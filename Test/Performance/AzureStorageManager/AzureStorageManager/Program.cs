﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Samples.ServiceHosting.StorageClient;
using System.Configuration;
using System.Data.Services.Client;

namespace AzureStorageManager
{
    class Program
    {
        static string account, key;
        static StorageAccountInfo accountInfo;
        static Uri baseUri;
        static bool? usePathStyleUris = false;

        static void Main(string[] args)
        {
            DeleteAllEntriesInTable("AzureMonitor");            
        }

        private static void DeleteAllQueuesInQueueContainer()
        {
            IEnumerable<MessageQueue> myQueues = null;
            string queueEndpoint;

            account = "airwatchqbar";
            key = "G/mvNGCSb0GvNACyz2Ulkw/6PlDZfOW45EdKNqHBTdjt+1fCHonNwcI0vCxm9EAXWRJNQeOpNZAn+ifRuHgu9Q==";
            queueEndpoint = "http://queue.core.windows.net";

            baseUri = new Uri(queueEndpoint);
            accountInfo = new StorageAccountInfo(baseUri, usePathStyleUris, account, key);

            QueueStorage myQueueStorage = QueueStorage.Create(accountInfo);

            myQueues = myQueueStorage.ListQueues();

            foreach (MessageQueue q in myQueues)
            {
                Console.WriteLine("Deleting queue with name {0}", q.Name);
                q.DeleteQueue();
            }
        }

        private static void DeleteAllBlobsInBlobContainter()
        {
            string blobEndpoint;
            IEnumerable<BlobContainer> myBlobs = null;

            account = "airwatchqbar";
            key = "G/mvNGCSb0GvNACyz2Ulkw/6PlDZfOW45EdKNqHBTdjt+1fCHonNwcI0vCxm9EAXWRJNQeOpNZAn+ifRuHgu9Q==";
            blobEndpoint = "http://blob.core.windows.net";

            baseUri = new Uri(blobEndpoint);
            accountInfo = new StorageAccountInfo(baseUri, usePathStyleUris, account, key);

            BlobStorage myBlobStorage = BlobStorage.Create(accountInfo);
            myBlobs = myBlobStorage.ListBlobContainers();

            foreach (BlobContainer b in myBlobs)
            {
                Console.WriteLine("Deleting blob container with name {0}", b.ContainerName);
                b.DeleteContainer();
            }

        }

        private static void DeleteAllEntriesInTable(string tableName)
        {
            account = "airwatchperf";
            key = "4bcxWp9HCrCagjce+Ctnvbyd/Lh1NIY0sXJxu+Jd4fJ77VZeC6qAqaNdyiGfiEohRnTPuf8q5sdDCbfdzIg7nw==";
            string tableEndpoint = "http://table.core.windows.net";

            baseUri = new Uri(tableEndpoint);
            accountInfo = new StorageAccountInfo(baseUri, usePathStyleUris, account, key);  

            TableStorage myTable = TableStorage.Create(accountInfo);
            //DataServiceContext svc = myTable.GetDataServiceContext();
            //DataServiceQuery<TableStorageTable> localQuery;
            //IEnumerable<TableStorageTable> tmp;

            //svc.MergeOption = MergeOption.NoTracking;
            //IQueryable<TableStorageTable> query = from t in svc.CreateQuery<TableStorageTable>("Tables") select t;

            if(myTable.DoesTableExist(tableName))
            {
                myTable.DeleteTable(tableName);
                
            }
            while (true)
            {
                try
                {
                    myTable.CreateTable(tableName);
                    break;
                }
                catch
                {
                }          
            }
        }
    }
}
