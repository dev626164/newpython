﻿
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Net.Mime;
using System.Configuration;
using System.IO;
using System.Net;
using System.Collections.Specialized;
using System.Data.Services.Common;
using System.Data.Services.Client;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using Microsoft.ServiceHosting.ServiceRuntime;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Neudesic.Azure;

namespace Neudesic.Azure
{
    // This class provides storage and retrieval services for cloud storage.

    public class StorageHelper
    {
        // Constants.

        private const string ContainerName = "test";        // Storage container name.

        // Entities needed for cloud storage.

        private StorageAccountInfo account;
        public StorageAccountInfo StorageAccountInfo
        {
            get
            {
                return account;
            }
        }

        private NameValueCollection containerMetadata = null;
        private BlobStorage blobStorage;
        private BlobContainer container;

        private QueueStorage queueStorage;

        private TableStorage tableStorage;

        // Variables to establish the current user context.

        private string containerName;                                   // The container name (unique to user) the user's profile is stored in.

        /// <summary>
        /// Create a lock manager instance.
        /// </summary>
        /// <param name="username"></param>

        public StorageHelper(string accountName, string accountKey, bool isLocal, string blobStorageEndpoint, string queueStorageEndpoint, string tableStorageEndpoint)
        {
            this.containerName = ContainerName;

            // Open blob storage.

            //account = StorageAccountInfo.GetDefaultBlobStorageAccountFromConfiguration();

            account = new StorageAccountInfo(new Uri(blobStorageEndpoint /* "http://blob.core.windows.net/" */), isLocal, accountName, accountKey);

            blobStorage = BlobStorage.Create(account);
            blobStorage.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));
            container = blobStorage.GetBlobContainer(containerName);

            //Create the container if it does not exist.
            container.CreateContainer(containerMetadata, ContainerAccessControl.Private);

            // Open queue storage.

            //StorageAccountInfo qaccount = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();

            StorageAccountInfo qaccount = new StorageAccountInfo(new Uri(queueStorageEndpoint /* "http://queue.core.windows.net/" */), isLocal /* true for dev storage, false */, accountName, accountKey);

            queueStorage = QueueStorage.Create(qaccount);
            queueStorage = QueueStorage.Create(qaccount);
            queueStorage.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));

            // Open table storage.

            //StorageAccountInfo taccount = StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration();
            StorageAccountInfo taccount = new StorageAccountInfo(new Uri(tableStorageEndpoint /* "http://table.core.windows.net/" */), isLocal /* false */, accountName, accountKey);

            tableStorage = TableStorage.Create(taccount);
            tableStorage = TableStorage.Create(taccount);
            tableStorage.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));
        }

        /// <summary>
        /// Return a list of BLOB containers.
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<BlobContainer> GetBlobContainers()
        {
            return blobStorage.ListBlobContainers();
        }

        public void CreateBlobContainer(string containerName)
        {
            BlobContainer container = blobStorage.GetBlobContainer(containerName);
            container.CreateContainer();
        }

        public IEnumerable<MessageQueue> GetQueues()
        {
            return queueStorage.ListQueues();
        }

        public void CreateQueue(string queueName)
        {
            MessageQueue queue = queueStorage.GetQueue(queueName);
            queue.CreateQueue();
        }

        public IEnumerable<string> GetTables()
        {
            return tableStorage.ListTables();
        }

        public void CreateTable(string tableName)
        {
            tableStorage.CreateTable(tableName);
        }


        /// <summary>
        /// Delete a table.
        /// </summary>
        /// <param name="tableName"></param>

        public void DeleteTable(string tableName)
        {
            tableStorage.DeleteTable(tableName);
        }


        /// <summary>
        /// Delete a table entity.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="entity"></param>

        public void DeleteTableRecord(string tableName, GenericEntity entity)
        {
            List<GenericEntity> entities = new List<GenericEntity>();

            TableStorageDataServiceContext svc = tableStorage.GetDataServiceContext();

            svc.IgnoreMissingProperties = true;
            svc.ReadingEntity += new EventHandler<ReadingWritingEntityEventArgs>(OnReadingEntity);

            var qResult2 = (from c in svc.CreateQuery<GenericEntity>(tableName)
                            where c.PartitionKey == entity.PartitionKey && c.RowKey == entity.RowKey
                            select c);

            TableStorageDataServiceQuery<GenericEntity> tableStorageQuery = new TableStorageDataServiceQuery<GenericEntity>(qResult2 as DataServiceQuery<GenericEntity>);
            IEnumerable<GenericEntity> res = tableStorageQuery.ExecuteAllWithRetries();
            foreach (GenericEntity entityFound in res)
            {
                svc.DeleteObject(entityFound);
            }
            svc.SaveChanges();
        }

        public void AddTableRecord(string tableName, GenericEntity entity)
        {
            TableStorageDataServiceContext svc = tableStorage.GetDataServiceContext();
            svc.IgnoreMissingProperties = true;
            svc.WritingEntity += new EventHandler<ReadingWritingEntityEventArgs>(svc_WritingEntity);
            svc.AddObject(tableName, entity);
            svc.SaveChanges();
        }

        void svc_WritingEntity(object sender, ReadingWritingEntityEventArgs e)
        {
            //<entry xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns="http://www.w3.org/2005/Atom">
            //  <title />
            //  <updated>2009-05-09T23:43:33.7292Z</updated>
            //  <author>
            //    <name />
            //  </author>
            //  <id />
            //  <content type="application/xml">
            //    <m:properties>
            //      <d:PartitionKey>P1</d:PartitionKey>
            //      <d:RowKey>R1</d:RowKey>
            //    </m:properties>
            //  </content>
            //</entry>

            // Add properties from generic entity.

            try
            {
                XNamespace ns = "http://www.w3.org/2005/Atom";
                XNamespace nsd = "http://schemas.microsoft.com/ado/2007/08/dataservices";
                XNamespace nsm = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";

                XElement entry = e.Data;

                string entryTS = entry.ToString();
                string entryV = entry.Value;

                XElement content = entry.Element(XName.Get("content", ns.NamespaceName));

                string contentTS = content.ToString();
                string contentV = content.Value;

                XElement properties = content.Element(XName.Get("properties", nsm.NamespaceName));

                string propertiesTS = properties.ToString();
                string propertiesV = properties.Value;

                //PropertyItem item;
                foreach (KeyValuePair<string, object> kvp in ((GenericEntity)e.Entity).Properties)
                {
                    properties.Add(new XElement(XName.Get(kvp.Key, nsd.NamespaceName), kvp.Value.ToString()));
                }
            }
            catch (Exception)
            {

            }
        }

        public IEnumerable<GenericEntity> GetTableRecords(string tableName)
        {
            List<GenericEntity> entities = new List<GenericEntity>();

            TableStorageDataServiceContext svc = tableStorage.GetDataServiceContext();

            svc.IgnoreMissingProperties = true;
            svc.ReadingEntity += new EventHandler<ReadingWritingEntityEventArgs>(OnReadingEntity);

            var qResult2 = (from c in svc.CreateQuery<GenericEntity>(tableName)
                            select c);

            TableStorageDataServiceQuery<GenericEntity> tableStorageQuery = new TableStorageDataServiceQuery<GenericEntity>(qResult2 as DataServiceQuery<GenericEntity>);
            IEnumerable<GenericEntity> res = tableStorageQuery.ExecuteAllWithRetries();
            foreach (GenericEntity entity in res)
            {
                entities.Add(entity);
            }

            return entities;
        }


        #region Blob storage

        /// <summary>
        /// Store a UTF-8 encoded string.
        /// </summary>
        private static void PutTextBlob(
            BlobContainer container,
            StringBlob s
            )
        {
            container.CreateBlob(
                s.Blob,
                new BlobContents(Encoding.UTF8.GetBytes(s.Value)),
                true
                );
        }

        private static void PutTextBlob(
            BlobContainer container,
            StringBlob s,
            bool overwrite
            )
        {
            container.CreateBlob(
                s.Blob,
                new BlobContents(Encoding.UTF8.GetBytes(s.Value)),
                overwrite
                );
        }

        private static void DeleteTextBlob(
            BlobContainer container,
            string blobName
            )
        {
            container.DeleteBlob(blobName);
        }

        /// <summary>
        /// Read a UTF-8 encoded string.
        /// </summary>
        /// <param name="blobName">Name of the blob</param>
        /// <returns>Contents of the blob.</returns>
        internal static StringBlob GetTextBlob(BlobContainer container, string blobName)
        {
            BlobContents contents = new BlobContents(new MemoryStream());
            BlobProperties blob = container.GetBlob(blobName, contents, false);
            if (blob.ContentType == null)
            {
                throw new FormatException("No content type set for blob.");
            }

            ContentType blobMIMEType = new ContentType(blob.ContentType);
            if (!blobMIMEType.Equals(StringBlob.TextBlobMIMEType))
            {
                throw new FormatException("Not a text blob.");
            }

            return new StringBlob
            {
                Blob = blob,
                Value = Encoding.UTF8.GetString(contents.AsBytes())
            };
        }

        internal static bool RefreshTextBlob(BlobContainer container, StringBlob stringBlob)
        {
            BlobContents contents = new BlobContents(new MemoryStream());
            BlobProperties blob = stringBlob.Blob;
            bool modified = container.GetBlobIfModified(blob, contents, false);

            if (!modified)
                return false;

            if (blob.ContentType == null)
            {
                throw new FormatException("No content type set for blob.");
            }

            ContentType blobMIMEType = new ContentType(blob.ContentType);
            if (!blobMIMEType.Equals(StringBlob.TextBlobMIMEType))
            {
                throw new FormatException("Not a text blob.");
            }

            stringBlob.Value = UnicodeEncoding.UTF8.GetString(contents.AsBytes());
            return modified;
        }

        internal static bool UpdateTextBlob(BlobContainer container, StringBlob stringBlob)
        {
            return container.UpdateBlobIfNotModified(stringBlob.Blob,
                new BlobContents(Encoding.UTF8.GetBytes(stringBlob.Value)));
        }

        #endregion
        // Credit goes to Pablo from ADO.NET Data Service team 
        public void OnReadingEntity(object sender, ReadingWritingEntityEventArgs args)
        {
            // TODO: Make these statics   
            XNamespace AtomNamespace = "http://www.w3.org/2005/Atom";
            XNamespace AstoriaDataNamespace = "http://schemas.microsoft.com/ado/2007/08/dataservices";
            XNamespace AstoriaMetadataNamespace = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";

            GenericEntity entity = args.Entity as GenericEntity;
            if (entity == null)
            {
                return;
            }

            // read each property, type and value in the payload   
            var properties = args.Entity.GetType().GetProperties();
            var q = from p in args.Data.Element(AtomNamespace + "content")
                                    .Element(AstoriaMetadataNamespace + "properties")
                                    .Elements()
                    where properties.All(pp => pp.Name != p.Name.LocalName)
                    select new
                    {
                        Name = p.Name.LocalName,
                        IsNull = string.Equals("true", p.Attribute(AstoriaMetadataNamespace + "null") == null ? null : p.Attribute(AstoriaMetadataNamespace + "null").Value, StringComparison.OrdinalIgnoreCase),
                        TypeName = p.Attribute(AstoriaMetadataNamespace + "type") == null ? null : p.Attribute(AstoriaMetadataNamespace + "type").Value,
                        p.Value
                    };

            foreach (var dp in q)
            {
                entity[dp.Name] = GetTypedEdmValue(dp.TypeName, dp.Value, dp.IsNull);
            }
        }


        private static object GetTypedEdmValue(string type, string value, bool isnull)
        {
            if (isnull) return null;

            if (string.IsNullOrEmpty(type)) return value;

            switch (type)
            {
                case "Edm.String": return value;
                case "Edm.Byte": return Convert.ChangeType(value, typeof(byte));
                case "Edm.SByte": return Convert.ChangeType(value, typeof(sbyte));
                case "Edm.Int16": return Convert.ChangeType(value, typeof(short));
                case "Edm.Int32": return Convert.ChangeType(value, typeof(int));
                case "Edm.Int64": return Convert.ChangeType(value, typeof(long));
                case "Edm.Double": return Convert.ChangeType(value, typeof(double));
                case "Edm.Single": return Convert.ChangeType(value, typeof(float));
                case "Edm.Boolean": return Convert.ChangeType(value, typeof(bool));
                case "Edm.Decimal": return Convert.ChangeType(value, typeof(decimal));
                case "Edm.DateTime": return XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.RoundtripKind);
                case "Edm.Binary": return Convert.FromBase64String(value);
                case "Edm.Guid": return new Guid(value);

                default: throw new NotSupportedException("Not supported type " + type);
            }
        }



    }

    [DataServiceKey("PartitionKey", "RowKey")]
    public class GenericEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }

        Dictionary<string, object> properties = new Dictionary<string, object>();

        public Dictionary<string, object> Properties
        {
            get
            {
                return properties;
            }
            set
            {
                properties = value;
            }
        }

        internal object this[string key]
        {
            get
            {
                return this.properties[key];
            }

            set
            {
                this.properties[key] = value;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            string value;
            int count = 0;
            if (properties != null)
            {
                foreach (KeyValuePair<string, object> kvp in properties)
                {
                    if (count > 0)
                        sb.Append("|");
                    if (kvp.Value == null)
                        value = string.Empty;
                    else
                        value = kvp.Value.ToString();
                    sb.Append(kvp.Key + "=" + value);
                    count++;
                }
            }
            return sb.ToString();
        }

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<entity>");       // RowKey=\"" + this.RowKey + "\">");
            sb.AppendLine("  <PartitionKey>" + this.PartitionKey + "</PartitionKey>");
            sb.AppendLine("  <RowKey>" + this.RowKey + "</RowKey>");

            string value;
            if (properties != null)
            {
                foreach (KeyValuePair<string, object> kvp in properties)
                {
                    if (kvp.Value == null)
                        value = string.Empty;
                    else
                        value = kvp.Value.ToString();
                    sb.AppendLine("  <" + kvp.Key + ">" + value + "</" + kvp.Key + ">");
                }
            }

            sb.AppendLine("</entity>");
            return sb.ToString();
        }

        public string ToXmlBinaryValues()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<entity>");       // RowKey=\"" + this.RowKey + "\">");
            sb.AppendLine("  <PartitionKey>" + this.PartitionKey + "</PartitionKey>");
            sb.AppendLine("  <RowKey>" + this.RowKey + "</RowKey>");

            string value;
            if (properties != null)
            {
                foreach (KeyValuePair<string, object> kvp in properties)
                {
                    if (kvp.Value == null)
                        value = string.Empty;
                    else
                        value = kvp.Value.ToString();

                    value = DisplayCharsAsBytes(value.ToCharArray());

                    sb.AppendLine("  <" + kvp.Key + ">" + value + "</" + kvp.Key + ">");
                }
            }

            sb.AppendLine("</entity>");
            return sb.ToString();
        }

        /// Convert a byte sequence into a displayable multi-line string showing the values.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>

        private string DisplayBytes(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();

            for (int b = 0; b < bytes.Length; b++)
                sb.Append(String.Format("{0:X2}", bytes[b]) + " ");
            return sb.ToString();
        }

        private string DisplayCharsAsBytes(char[] chars)
        {
            StringBuilder sb = new StringBuilder();

            for (int b = 0; b < chars.Length; b++)
                sb.Append(String.Format("{0:X4}", Convert.ToInt64(char.GetNumericValue(chars[b]))) + " ");
            return sb.ToString();
        }
    }
}
