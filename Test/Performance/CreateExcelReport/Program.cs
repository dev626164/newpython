﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Neudesic.Azure;
using System.IO;

namespace CreateExcelReport
{
    /// <summary>
    /// Create a CSV for each web role and worker role.
    /// </summary>
    class Program
    {
        static Dictionary<string, string> rolesType;   //Key - MachineName, Value- Roletype
        static Dictionary<string, List<GenericEntity>> allRolesPerfCounters; //Key - machine name, Value - All Counter values of that machine
        static List<GenericEntity> performanceCounterValues;
        static Dictionary<string, TextWriter> csvWriter = new Dictionary<string, TextWriter>(); //Key - machinename + roleType, Value - Initialized Text Writer
        
        static void Main(string[] args)
        {            
            GetCountersFromTable();
            GetDistinctWebAndWorkerRoles();
            WriteCountersToCSV();
        }

        /// <summary>
        /// Gets the samples of all performance counters from the AzureMonitor
        /// </summary>
        private static void GetCountersFromTable()
        {
            string account = "airwatchqbar";
            string key = "G/mvNGCSb0GvNACyz2Ulkw/6PlDZfOW45EdKNqHBTdjt+1fCHonNwcI0vCxm9EAXWRJNQeOpNZAn+ifRuHgu9Q==";
            string tableEndpoint = "http://table.core.windows.net";
            string queueEndpoint = "http://queue.core.windows.net";
            string blobEndpoint = "http://blob.core.windows.net";

            //Get All performance counter values from AzureMonitor Table
            StorageHelper storage = new StorageHelper(account, key, false, blobEndpoint, queueEndpoint, tableEndpoint);
            performanceCounterValues = new List<GenericEntity>(storage.GetTableRecords("AzureMonitor"));
        }

        /// <summary>
        /// Writes the Counters to CSV files - one file for each instance of role (web and worker)
        /// </summary>
        private static void WriteCountersToCSV()
        {                                             
            CreateTextWriters();
            WriteHeadersToCSV();
            WriteValuesUnderHeaders();
            CloseTextWriters();
        }

        /// <summary>
        /// Creates CSV Files with the following naming convention - workerRole_1.csv or webRole_4.csv
        /// </summary>
        private static void CreateTextWriters()
        {
            string csvFileName;
            Dictionary<string, string>.Enumerator enumerator = rolesType.GetEnumerator();

            while (enumerator.MoveNext())
            {
                string machineName = enumerator.Current.Key;
                string roleType = enumerator.Current.Value;               

                csvFileName = machineName + roleType + ".csv";
                TextWriter txtWriter = new StreamWriter(csvFileName);

                csvWriter.Add(enumerator.Current.Key, txtWriter);

            } 
        }

        /// <summary>
        /// Closes the Text Writers
        /// </summary>
        private static void CloseTextWriters()
        {
            Dictionary<string, string>.Enumerator enumerator = rolesType.GetEnumerator();

            while (enumerator.MoveNext())
            {
                csvWriter[enumerator.Current.Key].Close();   
            } 
        }

        /// <summary>
        /// Writes all the values below the corresponding header
        /// </summary>
        /// <param name="fields"></param>
        private static void WriteValuesUnderHeaders()
        {
            Dictionary<string, string>.Enumerator rolesEnumerator = rolesType.GetEnumerator();

            while (rolesEnumerator.MoveNext())
            {
                StringBuilder valuesBuilder = null;
                List<string> fields = null;          

                List<GenericEntity> perfCounterValues = allRolesPerfCounters[rolesEnumerator.Current.Key];

                foreach (GenericEntity perfCountersInstance in perfCounterValues)
                {
                    fields = perfCountersInstance.ToString().Split('|').ToList();

                    string timestamp = fields.Find(s => s.StartsWith("Timestamp")).Split('=')[1];
                    string machineName = fields.Find(s => s.StartsWith("MachineName")).Split('=')[1];
                    string roleType = fields.Find(s => s.StartsWith("RoleName")).Split('=')[1];
                    string values = string.Empty;

                    valuesBuilder = new StringBuilder();
                    valuesBuilder.Append(timestamp + "," + machineName + "_" + roleType + ",");
                    foreach (string field in fields)
                    {
                        //Don't add these to the values
                        if (field.Contains("Timestamp") || field.Contains("MachineName") || field.Contains("RoleName"))
                        {
                            continue;
                        }

                        //Don't add these as values for worker role
                        if ((field.Contains("ASP") || field.Contains("WCF")) && allRolesPerfCounters[rolesEnumerator.Current.Key][0]["RoleName"].ToString().Contains("WorkerRole"))
                        {
                            continue;
                        }

                        valuesBuilder.Append(field.Split('=')[1] + ",");
                    }

                    values = valuesBuilder.ToString();

                    csvWriter[rolesEnumerator.Current.Key].WriteLine(values);
                }
            } 
        }

        /// <summary>
        /// Writes Header to all the CSVs - Worker roles and web roles will not collect the same set of performance counters
        /// This method takes care of creating the right headers for each type of CSV report - be it for web role or a worker role
        /// </summary>
        /// <param name="fields"></param>
        private static void WriteHeadersToCSV()
        {
            Dictionary<string, string>.Enumerator rolesEnumerator = rolesType.GetEnumerator();

            while(rolesEnumerator.MoveNext())
            {
                List<string> fields = allRolesPerfCounters[rolesEnumerator.Current.Key][0].Properties.Keys.ToList();

                string timestamp = fields.Find(s => s.StartsWith("Timestamp"));
                string machineName = fields.Find(s => s.StartsWith("MachineName"));
                string roleType = fields.Find(s => s.StartsWith("RoleName"));
                string header = string.Empty;

                StringBuilder headerBuilder = new StringBuilder();
                headerBuilder.Append(timestamp + "," + machineName + "_" + roleType + ",");
                foreach (string field in fields)
                {
                    //Don't add these to the header 
                    if (field.Contains(timestamp) || field.Contains(machineName) || field.Contains(roleType))
                    {
                        continue;
                    }

                    //Don't add these as headers for worker role
                    if ((field.Contains("ASP") || field.Contains("WCF")) && allRolesPerfCounters[rolesEnumerator.Current.Key][0]["RoleName"].ToString().Contains("WorkerRole"))
                    {
                        continue;
                    }

                    headerBuilder.Append(field.Split('=')[0] + ",");
                }

                header = headerBuilder.ToString();
                csvWriter[rolesEnumerator.Current.Key].WriteLine(header);
            } 
        }

        /// <summary>
        /// Gets all the distinct web and worker roles and stores all performance counters related
        /// to a particular role against it's name in a dictionary
        /// </summary>
        private static void GetDistinctWebAndWorkerRoles()
        {
            rolesType = new Dictionary<string, string>();
            allRolesPerfCounters = new Dictionary<string, List<GenericEntity>>();

            foreach (GenericEntity perfCounterSnapshot in performanceCounterValues)
            {
                string roleName = perfCounterSnapshot.Properties["RoleName"].ToString();
                string machineName = perfCounterSnapshot.Properties["MachineName"].ToString();

                //Add an entry to allRolesPerfCounters if this role(machine name) is being enumerated for the first time
                if (!allRolesPerfCounters.ContainsKey(machineName))
                {
                    //Add the current performance counter snapshot for the machine name
                    List<GenericEntity> rolePerfCounters = new List<GenericEntity>();
                    rolePerfCounters.Add(perfCounterSnapshot);
                    allRolesPerfCounters.Add(machineName, rolePerfCounters);
                }
                else
                {
                    //Add the current performance counter snapshot for the machine name
                    allRolesPerfCounters[machineName].Add(perfCounterSnapshot);
                }

                if (!rolesType.ContainsKey(machineName))
                {
                    rolesType.Add(machineName, roleName);
                }
            }
        }
    }
}
