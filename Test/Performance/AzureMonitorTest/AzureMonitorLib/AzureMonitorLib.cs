﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading;
using Microsoft.ServiceHosting.ServiceRuntime;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Neudesic.Azure;
using System.Data.Services.Client;
using System.Net;
using System.Runtime.InteropServices;
using ComTypes = System.Runtime.InteropServices.ComTypes;

namespace Neudesic.Azure
{
    public static class AzureMonitor
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GetSystemTimes(out ComTypes.FILETIME lpIdleTime, out ComTypes.FILETIME lpKernelTime, out ComTypes.FILETIME lpUserTime);

        private const int DEFAULT_PERF_COUNTER_UPDATE_INTERVAL = 15 * 1000;     // Default update interval, overridable in .cscfg file settings.
        private const string accountName = "airwatchperf";
        private const string accountKey = "4bcxWp9HCrCagjce+Ctnvbyd/Lh1NIY0sXJxu+Jd4fJ77VZeC6qAqaNdyiGfiEohRnTPuf8q5sdDCbfdzIg7nw==";

        private static bool Initialized = false;                                // If true, initialization has been performed.
        private static bool TrackingActive = false;                             // If true, tracking is active.
        private static Thread TrackingThread = null;                            // Thread that performs the monitoring.
        private static int PerfCounterUpdateInterval;                           // Update interval.
        private static StorageAccountInfoMod account = null;                       // Storage account instance.
        private static TableStorageMod tableStorage;                               // Table storage instance.
        private static TableSamples.SampleDataServiceContext svc;               // Data service context instance.
        private static string sampleTableName;                                  // Table name.
        private static System.Diagnostics.Process p;                            // Current process.
        private static string PartitionKey;                                     // Partition key for a table entity.
        private static string MachineName;                                      // Machine name.
        private static long PID;                                                // Process ID.
        private static string AppName;                                          // Application name.
        private static string RoleName;                                         // Role name.

        private static List<PerformanceCounter> allPerfCounters;                // List of all required Performance Counters
        private static List<PerformanceCounter> rawPerfCounters;                // List of Performance Counters that give raw values
        private static List<PerformanceCounter> formulaBasedPerfCounters;          // List of Performance Counters that use a formula to calcuate values
        private static List<PerformanceCounter> allLazyPerfCounters;            // List of all required Perf Counters that initialized lazily.
        private static List<PerformanceCounter> rawLazyCounters;
        private static List<PerformanceCounter> formulaBasedLazyCounters;

        private static Uri baseUri = new Uri("http://queue.core.windows.net");
        private static StorageAccountInfoMod accountInfo;
        private static QueueStorageMod azureMonitorQueue;
        private static MessageQueueMod queue;
        private static Timer checkForCollectionStopage;
        
        static AzureMonitor()
        {
            baseUri = new Uri("http://queue.core.windows.net");
            accountInfo = new StorageAccountInfoMod(baseUri, false, accountName, accountKey);
            azureMonitorQueue = QueueStorageMod.Create(accountInfo);
            queue = azureMonitorQueue.GetQueue("azuremonitorqueue");
        }

        // Initialization of storage account.

        private static void Initialize()
        {
            if (Initialized) return;
            Initialized = true;

            //Initiate timer to check periodically if the stop flag has been set.
            checkForCollectionStopage = new Timer(new TimerCallback(Stop), null, 0, 60 * 1000);

            // Save some information that isn't going to be changing.

            MachineName = System.Net.Dns.GetHostName();
            PID = System.Diagnostics.Process.GetCurrentProcess().Id;
            PartitionKey = MachineName + "-" + PID;

            // Set up storage account access and create table if necessary.

            account = StorageAccountInfoMod.GetDefaultTableStorageAccountFromConfiguration();
            tableStorage = TableStorageMod.Create(account);
            tableStorage.RetryPolicy = RetryPoliciesMod.RetryN(3, TimeSpan.FromSeconds(1));

            account = StorageAccountInfoMod.GetDefaultTableStorageAccountFromConfiguration();
            svc = new TableSamples.SampleDataServiceContext(account);

            sampleTableName = TableSamples.SampleDataServiceContext.SampleTableName;
            tableStorage.TryCreateTable(sampleTableName);
        }

        // Start - start tracking performance counters.

        public static void Start(object StateInfo)
        {
            List<string> startParams = StateInfo as List<string>;
            string appName = startParams[0];
            string roleName = startParams[1];
            bool queueExists;
            bool startPerfCounterCollection = false;
       
            do  //Get the startPerfCounterCollection flag and wait till it is set to true. Start the perfCounterCollection only after that.
            {
                try
                {
                    queueExists = queue.DoesQueueExist();
                    if (!queueExists)
                    {
                        Thread.Sleep(5 * 1000);
                    }
                    else
                    {
                        if (queue.GetMessage().ContentAsString().Equals("start"))
                            startPerfCounterCollection = true;
                        if (!startPerfCounterCollection)
                            continue;
                    }
                }
                catch
                {
                    queueExists = false;
                }

            } while (!queueExists);           

            Start(appName, roleName, false);
        }

        public static void Start(string appName, string roleName, bool deleteOlderEntries)
        {
            if (TrackingActive) return;
            TrackingActive = true;

            // Get the update interval setting or default it.

            PerfCounterUpdateInterval = DEFAULT_PERF_COUNTER_UPDATE_INTERVAL;

            string value = TryGetConfigurationSetting("PerfCounterUpdateInterval");
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    PerfCounterUpdateInterval = Convert.ToInt32(value);
                }
                catch (FormatException)
                {

                }
            }

            AppName = appName;
            RoleName = roleName;

            Initialize();

            // If delete flag is set, delete older entries.

            if (deleteOlderEntries)
            {
                var entries =
                    from AzureMonitorEntity c in svc.CreateQuery<TableSamples.SampleDataServiceContext>("AzureMonitor")
                    select c;

                foreach (AzureMonitorEntity s in entries)
                {
                    // Delete any item older than today.
                    if (s.Timestamp < DateTime.Today)
                    if (s != null)
                    {
                        svc.DeleteObject(s);
                    }
                }
                svc.SaveChangesWithRetries();
            }

            // Store the initial snapsho. This allows storage to be tested as part of Start, informing the caller right away if
            // there are any access problems.

            StoreSnapshot();

            // Kick off the monitoring thread.

            TrackingThread = new Thread(new ThreadStart(TrackingThreadRoutine));
            TrackingThread.Start();

            // Echo to the log that we're successfully started.

            RoleManager.WriteToLog("Information", "AzureAzureMonitor: Started");
        }

        // Stop - stop tracking performance counters.

        public static void Stop(Object stateInfo)
        {
            bool queueExists;
            bool stopPerfCounterCollection = false;

            do  //Get the startPerfCounterCollection flag and wait till it is set to true. Start the perfCounterCollection only after that.
            {
                try
                {
                    queueExists = queue.DoesQueueExist();
                    if (!queueExists)
                    {
                        Thread.Sleep(5 * 1000);
                    }
                    else
                    {
                        if (queue.GetMessage().ContentAsString().Equals("stop"))
                            stopPerfCounterCollection = true;
                    }
                }
                catch
                {
                    queueExists = false;
                }

            } while (!queueExists);

            if (stopPerfCounterCollection)
            {
                checkForCollectionStopage.Dispose();
                TrackingActive = false;
                TrackingThread.Interrupt(); 
            }

            // Echo to the log that we're stopped.

            RoleManager.WriteToLog("Information", "AzureAzureMonitor: Stopped");
        }

        // Return all application metrics from cloud storage.

        public static AzureMonitorEntity[] GetMetrics()
        {
            Initialize();

            List<AzureMonitorEntity> results = new List<AzureMonitorEntity>();

            var entries =
                from AzureMonitorEntity c in svc.CreateQuery<TableSamples.SampleDataServiceContext>("AzureMonitor")
                where c.RoleName == "WorkerRole"
                select c
                ;

            foreach (AzureMonitorEntity s in entries)
            {
                results.Add(s);
            }
            return results.ToArray();
        }

        // Delete all entries.

        public static void DeleteAll()
        {
            Initialize();

            var entries =
                from AzureMonitorEntity c in svc.CreateQuery<TableSamples.SampleDataServiceContext>("AzureMonitor")
                select c;

            foreach (AzureMonitorEntity s in entries)
            {
                 svc.DeleteObject(s);
            }
            svc.SaveChangesWithRetries();
        }

        // Monitoring thread routine.

        public static void TrackingThreadRoutine()
        {
            try
            {
                while (TrackingActive)
                {
                    StoreSnapshot();
                    Thread.Sleep(PerfCounterUpdateInterval);
                }
            }
            catch (ThreadInterruptedException)
            {
                // Shutting down.
            }
            catch (Exception e)
            {
                RoleManager.WriteToLog("Information", "AzureAzureMonitor: " + e.ToString());
            }
        }

        // Get the latest process information and add/update a record in cloud storage.

        public static void StoreSnapshot()
        {
            p = System.Diagnostics.Process.GetCurrentProcess();

            // Delete previous entries for this machine/PID.

            var entries =
                 from AzureMonitorEntity c in svc.CreateQuery<TableSamples.SampleDataServiceContext>("AzureMonitor")
                 where c.PartitionKey == PartitionKey
                 select c;

            foreach (AzureMonitorEntity s in entries)
            {
                 svc.DeleteObject(s);
            }
            svc.SaveChangesWithRetries();

            // Capture process information.

            #region Azure Monitor Code
            int handleCount = p.HandleCount;
            long nonPagedMemorySize = p.NonpagedSystemMemorySize64;
            long pagedMemorySize = p.PagedMemorySize64;
            long pagedSystemMemorySize = p.PagedSystemMemorySize64;
            long peakPagedMemorySize = p.PeakPagedMemorySize64;
            long peakVirtualMemorySize = p.PeakVirtualMemorySize64;
            long peakWorkingSet = p.PeakWorkingSet64;
            long privateMemorySize = p.PrivateMemorySize64;
            string processName = p.ProcessName;
            int threads = p.Threads.Count;
            DateTime startTime = p.StartTime;
            long pageFileUsage = p.PagedMemorySize64;
            long peakPageFileUsage = p.PeakPagedMemorySize64;
            long nonpagedSystemMemorySize = p.NonpagedSystemMemorySize64;
            long totalProcessorTime = p.TotalProcessorTime.Ticks;
            long userProcessorTime = p.UserProcessorTime.Ticks;
            DateTime localMachineTime = DateTime.Now; 
            #endregion

            //Initialize performance counters
            bool arePerfCountersInitialized = false;
            if (!arePerfCountersInitialized)
            {
                arePerfCountersInitialized = true;
                InitializePerformanceCounters(processName);
            }

            // Determine if we have a new entry to add or an existing one to update.
            AzureMonitorEntity t = null;

            bool isNew = true;

            try
            {
                var cs =
                    from AzureMonitorEntity c in svc.CreateQuery<TableSamples.SampleDataServiceContext>("AzureMonitor")
                    where c.PartitionKey == MachineName && c.RowKey == PID.ToString()
                    select c;

                foreach (AzureMonitorEntity s in cs)
                {
                    isNew = false;
                    t = s;
                }
            }
            catch (Exception)
            {
                // This is not good coding practice but seems to be necessary to handle all scenarios.
            }

            if (isNew)
                t = new AzureMonitorEntity(MachineName, DateTime.Now.Ticks.ToString());

            // Store process information.

            #region Azure Monitor Code
            t.MachineName = MachineName;
            t.PID = PID;
            t.ProcessName = processName;
            t.AppName = AppName;
            t.RoleName = RoleName;
            t.ProcessHandleCount = handleCount;
            t.nonPagedMemorySize = nonPagedMemorySize;
            t.pagedMemorySize = pagedMemorySize;
            t.pagedSystemMemorySize = pagedSystemMemorySize;
            t.peakPagedMemorySize = peakPagedMemorySize;
            t.peakVirtualMemorySize = peakVirtualMemorySize;
            t.peakWorkingSet = peakWorkingSet;
            t.privateMemorySize = privateMemorySize;
            t.threads = threads;
            t.startTime = startTime;
            t.pageFileUsage = pagedMemorySize;
            t.peakPageFileUsage = peakPagedMemorySize;
            t.nonPagedPoolUsage = nonpagedSystemMemorySize;
            t.totalProcessorTime = totalProcessorTime;
            t.userProcessorTime = userProcessorTime;
            t.localMachineTime = DateTime.Now; 
            #endregion

            CaptureRawCounters(t);
            CaptureFormulaBasedCounters(t);
            CaptureRawLazyCounters(t);
            CaptureFormulaBasedLazyCounters(t);

            if (isNew)
                svc.AddObject(sampleTableName, t);
            else
                svc.UpdateObject(t);

            svc.SaveChangesWithRetries();
        }

        // Retrieve a value from cloud configuration settings.

        private static string TryGetConfigurationSetting(string configName)
        {
            string ret = null;
            try
            {
                ret = RoleManager.GetConfigurationSetting(configName);
            }
            catch (RoleException)
            {
                return null;
            }
            return ret;
        }

        /// <summary>
        /// Instantiates all the required PerformanceCounter objects. 
        /// </summary>
        private static void InitializePerformanceCounters(string processName)
        {
            allPerfCounters = new List<PerformanceCounter>();

            #region Instantiate All Performance Counter Categories

            PerformanceCounterCategory processorCounter = new PerformanceCounterCategory("Processor");
            string[] processorInstanceNames = processorCounter.GetInstanceNames();

            PerformanceCounterCategory networkCounter = new PerformanceCounterCategory("Network Interface");
            string[] networkInterfacesInstanceNames = networkCounter.GetInstanceNames();

            PerformanceCounterCategory physicalDiskCounter = new PerformanceCounterCategory("PhysicalDisk");
            string[] physicalDiskInstanceNames = physicalDiskCounter.GetInstanceNames();

            PerformanceCounterCategory aspNetAppCounter = new PerformanceCounterCategory("ASP.NET Applications");
            string[] aspNetAppInstanceNames = aspNetAppCounter.GetInstanceNames();

            PerformanceCounterCategory webServicesCounter = new PerformanceCounterCategory("Web Service");
            string[] webServiceInstanceNames = webServicesCounter.GetInstanceNames();

            PerformanceCounterCategory serviceModelOperationCounter = new PerformanceCounterCategory("ServiceModelOperation 3.0.0.0");
            List<string> wcfOperations = new List<string>();
            while (wcfOperations.Count <= 0)
            {
                wcfOperations = serviceModelOperationCounter.GetInstanceNames().ToList();
            }


            #endregion

            #region System Resources Counter

            #region Processor Counters

            PerformanceCounter processorTime = new PerformanceCounter("Processor", "% Processor Time");
            processorTime.InstanceName = processorInstanceNames[0];
            allPerfCounters.Add(processorTime);

            PerformanceCounter privilegedTime = new PerformanceCounter("Processor", "% Privileged Time");
            privilegedTime.InstanceName = processorInstanceNames[0];
            allPerfCounters.Add(privilegedTime);

            PerformanceCounter interruptTime = new PerformanceCounter("Processor", "% Interrupt Time");
            interruptTime.InstanceName = processorInstanceNames[0];
            allPerfCounters.Add(interruptTime);

            PerformanceCounter processorQueueLength = new PerformanceCounter("System", "Processor Queue Length");
            allPerfCounters.Add(processorQueueLength);

            PerformanceCounter contextSwitchesPerSecond = new PerformanceCounter("System", "Context Switches/sec");
            allPerfCounters.Add(contextSwitchesPerSecond);

            #endregion

            #region Add Memory Counters

            PerformanceCounter availableMbytes = new PerformanceCounter("Memory", "Available Mbytes");
            allPerfCounters.Add(availableMbytes);

            PerformanceCounter pageReadsPerSecond = new PerformanceCounter("Memory", "Page Reads/sec");
            allPerfCounters.Add(pageReadsPerSecond);

            PerformanceCounter pagesPerSecond = new PerformanceCounter("Memory", "Pages/sec");
            allPerfCounters.Add(pagesPerSecond);

            PerformanceCounter poolNonpagedBytes = new PerformanceCounter("Memory", "Pool Nonpaged Bytes");
            allPerfCounters.Add(poolNonpagedBytes);

            PerformanceCounter poolNonPagedFailures = new PerformanceCounter("Server", "Pool Nonpaged Failures");
            allPerfCounters.Add(poolNonPagedFailures);

            PerformanceCounter poolPagedFailures = new PerformanceCounter("Server", "Pool Paged Failures");
            allPerfCounters.Add(poolPagedFailures);

            PerformanceCounter poolNonpagedPeak = new PerformanceCounter("Server", "Pool Nonpaged Peak");
            allPerfCounters.Add(poolNonpagedPeak);

            PerformanceCounter cacheBytes = new PerformanceCounter("Memory", "Cache Bytes");
            allPerfCounters.Add(cacheBytes);

            PerformanceCounter cacheFaultsPerSecond = new PerformanceCounter("Memory", "Cache Faults/sec");
            allPerfCounters.Add(cacheFaultsPerSecond);

            PerformanceCounter mdlReadHitsPercentage = new PerformanceCounter("Cache", "MDL Read Hits %");
            allPerfCounters.Add(mdlReadHitsPercentage);

            #endregion

            #region Add Disk IO Counters

            PerformanceCounter avgDiskQueueLength = new PerformanceCounter("PhysicalDisk", "Avg. Disk Queue Length");
            avgDiskQueueLength.InstanceName = physicalDiskInstanceNames[0];
            allPerfCounters.Add(avgDiskQueueLength);

            PerformanceCounter avgDiskReadQueueLength = new PerformanceCounter("PhysicalDisk", "Avg. Disk Read Queue Length");
            avgDiskReadQueueLength.InstanceName = physicalDiskInstanceNames[0];
            allPerfCounters.Add(avgDiskReadQueueLength);

            PerformanceCounter avgDiskWriteQueueLength = new PerformanceCounter("PhysicalDisk", "Avg. Disk Write Queue Length");
            avgDiskWriteQueueLength.InstanceName = physicalDiskInstanceNames[0];
            allPerfCounters.Add(avgDiskWriteQueueLength);

            PerformanceCounter avgDiskSecPerRead = new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Read");
            avgDiskSecPerRead.InstanceName = physicalDiskInstanceNames[0];
            allPerfCounters.Add(avgDiskSecPerRead);

            PerformanceCounter avgDiskSecPerTransfer = new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Transfer");
            avgDiskSecPerTransfer.InstanceName = physicalDiskInstanceNames[0];
            allPerfCounters.Add(avgDiskSecPerTransfer);

            PerformanceCounter diskWritesPerSecond = new PerformanceCounter("PhysicalDisk", "Disk Writes/sec");
            diskWritesPerSecond.InstanceName = physicalDiskInstanceNames[0];
            allPerfCounters.Add(diskWritesPerSecond);

            #endregion

            #region Network IO

            PerformanceCounter ntwrkIntrfcBytesTotalPerSec = new PerformanceCounter("Network Interface", "Bytes Total/sec");
            ntwrkIntrfcBytesTotalPerSec.InstanceName = networkInterfacesInstanceNames[0];
            allPerfCounters.Add(ntwrkIntrfcBytesTotalPerSec);

            PerformanceCounter ntwrkIntrfcBytesReceivedPerSec = new PerformanceCounter("Network Interface", "Bytes Received/sec");
            ntwrkIntrfcBytesReceivedPerSec.InstanceName = networkInterfacesInstanceNames[0];
            allPerfCounters.Add(ntwrkIntrfcBytesReceivedPerSec);

            PerformanceCounter ntwrkIntrfcBytesSentPerSec = new PerformanceCounter("Network Interface", "Bytes Sent/sec");
            ntwrkIntrfcBytesSentPerSec.InstanceName = networkInterfacesInstanceNames[0];
            allPerfCounters.Add(ntwrkIntrfcBytesSentPerSec);

            PerformanceCounter serverBytesTotal = new PerformanceCounter("Server", "Bytes Total/sec");
            allPerfCounters.Add(serverBytesTotal);

            #endregion

            #endregion

            #region Add CLR and Managed Code Counters

            #region Add Memory Counters

            PerformanceCounter percentageTimeInGC = new PerformanceCounter(".NET CLR Memory", "% Time in GC");
            percentageTimeInGC.InstanceName = processName;
            allPerfCounters.Add(percentageTimeInGC);

            PerformanceCounter bytesInAllHeaps = new PerformanceCounter(".NET CLR Memory", "# Bytes in all Heaps");
            bytesInAllHeaps.InstanceName = processName;
            allPerfCounters.Add(bytesInAllHeaps);

            PerformanceCounter gen0Collections = new PerformanceCounter(".NET CLR Memory", "# Gen 0 Collections");
            gen0Collections.InstanceName = processName;
            allPerfCounters.Add(gen0Collections);

            PerformanceCounter gen1Collections = new PerformanceCounter(".NET CLR Memory", "# Gen 1 Collections");
            gen1Collections.InstanceName = processName;
            allPerfCounters.Add(gen1Collections);

            PerformanceCounter gen2Collections = new PerformanceCounter(".NET CLR Memory", "# Gen 2 Collections");
            gen2Collections.InstanceName = processName;
            allPerfCounters.Add(gen2Collections);

            PerformanceCounter pinnedObjects = new PerformanceCounter(".NET CLR Memory", "# of Pinned Objects");
            pinnedObjects.InstanceName = processName;
            allPerfCounters.Add(pinnedObjects);

            PerformanceCounter largeObjectHeapSize = new PerformanceCounter(".NET CLR Memory", "Large Object Heap Size");
            largeObjectHeapSize.InstanceName = processName;
            allPerfCounters.Add(largeObjectHeapSize);

            #endregion

            #region Add Exception Counters

            PerformanceCounter exceptionsThrownPerSec = new PerformanceCounter(".NET CLR Exceptions", "# of Exceps Thrown / sec");
            exceptionsThrownPerSec.InstanceName = processName;
            allPerfCounters.Add(exceptionsThrownPerSec);

            #endregion

            #region Add Threads and Contention Counters

            PerformanceCounter contentionRatePerSec = new PerformanceCounter(".NET CLR LocksAndThreads", "Contention Rate / sec");
            contentionRatePerSec.InstanceName = processName;
            allPerfCounters.Add(contentionRatePerSec);

            PerformanceCounter currentQueueLength = new PerformanceCounter(".NET CLR LocksAndThreads", "Current Queue Length");
            currentQueueLength.InstanceName = processName;
            allPerfCounters.Add(currentQueueLength);

            PerformanceCounter currentPhysicalThreads = new PerformanceCounter(".NET CLR LocksAndThreads", "# of current physical Threads");
            currentPhysicalThreads.InstanceName = processName;
            allPerfCounters.Add(currentPhysicalThreads);

            #endregion

            #endregion

            if(processName.Contains("RdRoleHost"))
            {
                #region Add ASP.Net Counters

                PerformanceCounter aspNetAppRequestsPerSec = new PerformanceCounter("ASP.NET Applications", "Requests/Sec");
                aspNetAppRequestsPerSec.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppRequestsPerSec);

                PerformanceCounter isapiExtensionRequestsPerSec = new PerformanceCounter("Web Service", "ISAPI Extension Requests/sec");
                isapiExtensionRequestsPerSec.InstanceName = webServiceInstanceNames[0];
                allPerfCounters.Add(isapiExtensionRequestsPerSec);

                PerformanceCounter aspNetRequestsCurrent = new PerformanceCounter("ASP.NET", "Requests Current");
                allPerfCounters.Add(aspNetRequestsCurrent);

                PerformanceCounter aspNetAppRequestsExecuting = new PerformanceCounter("ASP.NET Applications", "Requests Executing");
                aspNetAppRequestsExecuting.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppRequestsExecuting);

                PerformanceCounter aspNetAppRequestsTimedOut = new PerformanceCounter("ASP.NET Applications", "Requests Timed Out");
                aspNetAppRequestsTimedOut.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppRequestsTimedOut);

                PerformanceCounter aspNetRequestsQueued = new PerformanceCounter("ASP.NET", "Requests Queued");
                allPerfCounters.Add(aspNetRequestsQueued);

                PerformanceCounter aspNetAppRequestsInApplicationQueue = new PerformanceCounter("ASP.NET Applications", "Requests In Application Queue");
                aspNetAppRequestsInApplicationQueue.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppRequestsInApplicationQueue);

                PerformanceCounter aspNetRequestsRejected = new PerformanceCounter("ASP.NET", "Requests Rejected");
                allPerfCounters.Add(aspNetRequestsRejected);

                PerformanceCounter aspNetRequestsWaitTime = new PerformanceCounter("ASP.NET", "Request Wait Time");
                allPerfCounters.Add(aspNetRequestsWaitTime);

                PerformanceCounter aspNetAppCacheTotalEntries = new PerformanceCounter("ASP.NET Applications", "Cache Total Entries");
                aspNetAppCacheTotalEntries.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppCacheTotalEntries);

                PerformanceCounter aspNetAppCacheTotalHitRation = new PerformanceCounter("ASP.NET Applications", "Cache Total Hit Ratio");
                aspNetAppCacheTotalHitRation.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppCacheTotalHitRation);

                PerformanceCounter aspNetAppCacheTotalTurnoverRate = new PerformanceCounter("ASP.NET Applications", "Cache Total Turnover Rate");
                aspNetAppCacheTotalTurnoverRate.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppCacheTotalTurnoverRate);

                PerformanceCounter aspNetAppCacheAPIHitRation = new PerformanceCounter("ASP.NET Applications", "Cache API Hit Ratio");
                aspNetAppCacheAPIHitRation.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppCacheAPIHitRation);

                PerformanceCounter aspNetAppCacheAPITurnoverRate = new PerformanceCounter("ASP.NET Applications", "Cache API Turnover Rate");
                aspNetAppCacheAPITurnoverRate.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppCacheAPITurnoverRate);

                PerformanceCounter aspNetAppOutputCacheEntries = new PerformanceCounter("ASP.NET Applications", "Output Cache Entries");
                aspNetAppOutputCacheEntries.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppOutputCacheEntries);

                PerformanceCounter aspNetAppOutputCacheHitRatio = new PerformanceCounter("ASP.NET Applications", "Output Cache Hit Ratio");
                aspNetAppOutputCacheHitRatio.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppOutputCacheHitRatio);

                PerformanceCounter aspNetAppOutputCacheTurnoverRate = new PerformanceCounter("ASP.NET Applications", "Output Cache Turnover Rate");
                aspNetAppOutputCacheTurnoverRate.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppOutputCacheTurnoverRate);

                PerformanceCounter aspNetAppErrorsTotalPerSec = new PerformanceCounter("ASP.NET Applications", "Errors Total/sec");
                aspNetAppErrorsTotalPerSec.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppErrorsTotalPerSec);

                PerformanceCounter aspNetAppErrorDuringExection = new PerformanceCounter("ASP.NET Applications", "Errors During Execution");
                aspNetAppErrorDuringExection.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppErrorDuringExection);

                PerformanceCounter aspNetAppErrorsUnhandledDuringExecutionPerSecond = new PerformanceCounter("ASP.NET Applications", "Errors Unhandled During Execution/sec");
                aspNetAppErrorsUnhandledDuringExecutionPerSecond.InstanceName = aspNetAppInstanceNames[0];
                allPerfCounters.Add(aspNetAppErrorsUnhandledDuringExecutionPerSecond);

            #endregion
            }

            #region Process Counter

            PerformanceCounter priviligedTime = new PerformanceCounter("Process", "% Privileged Time");
            priviligedTime.InstanceName = processName;
            allPerfCounters.Add(priviligedTime);

            PerformanceCounter processProcessorTime = new PerformanceCounter("Process", "% Processor Time");
            processProcessorTime.InstanceName = processName;
            allPerfCounters.Add(processProcessorTime);

            PerformanceCounter userTime = new PerformanceCounter("Process", "% User Time");
            userTime.InstanceName = processName;
            allPerfCounters.Add(userTime);

            PerformanceCounter handleCount = new PerformanceCounter("Process", "Handle Count");
            handleCount.InstanceName = processName;
            allPerfCounters.Add(handleCount);

            PerformanceCounter ioDataOpsPerSec = new PerformanceCounter("Process", "IO Data Operations/sec");
            ioDataOpsPerSec.InstanceName = processName;
            allPerfCounters.Add(ioDataOpsPerSec);

            PerformanceCounter pageFaultsPerSec = new PerformanceCounter("Process", "Page Faults/sec");
            pageFaultsPerSec.InstanceName = processName;
            allPerfCounters.Add(pageFaultsPerSec);

            PerformanceCounter processPoolNonpagedBytes = new PerformanceCounter("Process", "Pool Nonpaged Bytes");
            processPoolNonpagedBytes.InstanceName = processName;
            allPerfCounters.Add(processPoolNonpagedBytes);

            PerformanceCounter poolPagedBytes = new PerformanceCounter("Process", "Pool Paged Bytes");
            poolPagedBytes.InstanceName = processName;
            allPerfCounters.Add(poolPagedBytes);

            PerformanceCounter privateBytes = new PerformanceCounter("Process", "Private Bytes");
            privateBytes.InstanceName = processName;
            allPerfCounters.Add(privateBytes);

            PerformanceCounter workingSet = new PerformanceCounter("Process", "Working Set");
            workingSet.InstanceName = processName;
            allPerfCounters.Add(workingSet);

            PerformanceCounter virtualBytes = new PerformanceCounter("Process", "Virtual Bytes");
            virtualBytes.InstanceName = processName;
            allPerfCounters.Add(virtualBytes);

            #endregion

            rawPerfCounters = new List<PerformanceCounter>();
            formulaBasedPerfCounters = new List<PerformanceCounter>();

            foreach (PerformanceCounter perfCounter in allPerfCounters)
            {
                if (perfCounter.CounterType == PerformanceCounterType.NumberOfItems32 ||
                    perfCounter.CounterType == PerformanceCounterType.NumberOfItems64 ||
                    perfCounter.CounterType == PerformanceCounterType.NumberOfItemsHEX32 ||
                    perfCounter.CounterType == PerformanceCounterType.NumberOfItemsHEX64)
                {
                    rawPerfCounters.Add(perfCounter);
                }
                else
                {
                    formulaBasedPerfCounters.Add(perfCounter);
                }
            }
        }

        private static void InitializeLazyPerformanceCounters(string processName)
        {
            #region Instantiate All Performance Counter Categories

            PerformanceCounterCategory serviceModelOperationCounter = new PerformanceCounterCategory("ServiceModelOperation 3.0.0.0");
            List<string> wcfOperations = new List<string>();
            while (wcfOperations.Count <= 0)
            {
                wcfOperations = serviceModelOperationCounter.GetInstanceNames().ToList();
            }

            #endregion

            #region Add WCF Counters

            if (processName.Contains("RdRoleHost"))
            {
                PerformanceCounter wcfCalls = new PerformanceCounter("SystemServiceModel 3.0.0.0", "Calls");
                PerformanceCounter wcfCallsDuration = new PerformanceCounter("SystemServiceModel 3.0.0.0", "Calls Duration");
                PerformanceCounter wcfCallsFailed = new PerformanceCounter("SystemServiceModel 3.0.0.0", "Calls Failed");
                PerformanceCounter wcfCallsPerSecond = new PerformanceCounter("SystemServiceModel 3.0.0.0", "Calls Per Second");

                foreach (string instanceName in wcfOperations)
                {
                    wcfCalls.InstanceName = instanceName;
                    allPerfCounters.Add(wcfCalls);

                    wcfCallsDuration.InstanceName = instanceName;
                    allPerfCounters.Add(wcfCallsDuration);

                    wcfCallsFailed.InstanceName = instanceName;
                    allPerfCounters.Add(wcfCallsFailed);

                    wcfCallsPerSecond.InstanceName = instanceName;
                    allPerfCounters.Add(wcfCallsPerSecond);
                }
            }


            #endregion

            rawLazyCounters = new List<PerformanceCounter>();
            formulaBasedLazyCounters = new List<PerformanceCounter>();

            foreach (PerformanceCounter perfCounter in allLazyPerfCounters)
            {
                if (perfCounter.CounterType == PerformanceCounterType.NumberOfItems32 ||
                    perfCounter.CounterType == PerformanceCounterType.NumberOfItems64 ||
                    perfCounter.CounterType == PerformanceCounterType.NumberOfItemsHEX32 ||
                    perfCounter.CounterType == PerformanceCounterType.NumberOfItemsHEX64)
                {
                    rawLazyCounters.Add(perfCounter);
                }
                else
                {
                    formulaBasedLazyCounters.Add(perfCounter);
                }
            }
        }

        private static void CaptureRawCounters(AzureMonitorEntity t)
        {
            string perfCounterName;
                
            foreach (PerformanceCounter perfCounter in rawPerfCounters)
            {
                perfCounterName = perfCounter.CategoryName + ":" + perfCounter.CounterName;
                switch (perfCounterName)
                {
                    #region Caputer Counters

                    #region Processor
                    case "Processor:% Processor Time": t.ProcessorTime = perfCounter.NextValue();
                        break;
                    case "Processor:% Privileged Time": t.PrivilegedTime = perfCounter.NextValue();
                        break;
                    case "Processor:% Interrupt Time": t.InterruptTime = perfCounter.NextValue();
                        break;
                    case "System:Processor Queue Length": t.ProcessorQueueLength = perfCounter.NextValue();
                        break;
                    case "System:Context Switches/sec": t.ContentionRatePerSecond = perfCounter.NextValue();
                        break;
                    #endregion

                    #region Memory
                    case "Memory:Available Mbytes": t.AvaliableMbytes = perfCounter.NextValue();
                        break;
                    case "Memory:Page Reads/sec": t.PageReadsPerSecond = perfCounter.NextValue();
                        break;
                    case "Memory:Pages/sec": t.PagesPerSecond = perfCounter.NextValue();
                        break;
                    case "Memory:Pool Nonpaged Bytes": t.PoolNonpagedBytes = perfCounter.NextValue();
                        break;
                    case "Server:Pool Nonpaged Failures": t.PoolNonpagedFailures = perfCounter.NextValue();
                        break;
                    case "Server:Pool Paged Failures": t.PoolPagedFailures = perfCounter.NextValue();
                        break;
                    case "Server:Pool Nonpaged Peak": t.PoolNonpagedPeak = perfCounter.NextValue();
                        break;
                    case "Memory:Cache Bytes": t.CacheBytes = perfCounter.NextValue();
                        break;
                    case "Memory:Cache Faults/sec": t.CacheFaultsPerSecond = perfCounter.NextValue();
                        break;
                    case "Cache:MDL Read Hits %": t.MDLReadHitsPercentage = perfCounter.NextValue();
                        break;
                    #endregion

                    #region Disk IO
                    case "PhysicalDisk:Avg. Disk Queue Length": t.AvgDiskQueueLength = perfCounter.NextValue();
                        break;
                    case "PhysicalDisk:Avg. Disk Read Queue Length": t.AvgDiskReadQueueLength = perfCounter.NextValue();
                        break;
                    case "PhysicalDisk:Avg. Disk Write Queue Length": t.AvgDiskWriteQueueLength = perfCounter.NextValue();
                        break;
                    case "PhysicalDisk:Avg. Disk sec/Read": t.AvgDiskSecPerRead = perfCounter.NextValue();
                        break;
                    case "PhysicalDisk:Avg. Disk sec/Transfer": t.AvgDiskSecPerTransfer = perfCounter.NextValue();
                        break;
                    case "PhysicalDisk:Disk Writes/sec": t.DiskWritesPerSecond = perfCounter.NextValue();
                        break;
                    #endregion

                    #region Network IO
                    case "Network Interface:Bytes Total/sec": t.NtwrkInterfaceBytesTotalPerSecond = perfCounter.NextValue();
                        break;
                    case "Network Interface:Bytes Received/sec": t.NtwrkInterfaceBytesRecievedPerSecond = perfCounter.NextValue();
                        break;
                    case "Network Interface:Bytes Sent/sec": t.NtwrkInterfaceBytesSentPerSecond = perfCounter.NextValue();
                        break;
                    case "Server:Bytes Total/sec": t.ServerBytesTotalPerSecond = perfCounter.NextValue();
                        break;
                    #endregion

                    #region CLR
                    case ".NET CLR Memory:% Time in GC": t.PercentageTimeInGC = perfCounter.NextValue();
                        break;
                    case ".NET CLR Memory:# Bytes in all Heaps": t.BytesInAllHeaps = perfCounter.NextValue();
                        break;
                    case ".NET CLR Memory:# Gen 0 Collections": t.Gen0Collections = perfCounter.NextValue();
                        break;
                    case ".NET CLR Memory:# Gen 1 Collections": t.Gen1Collections = perfCounter.NextValue();
                        break;
                    case ".NET CLR Memory:# Gen 2 Collections": t.Gen2Colelctions = perfCounter.NextValue();
                        break;
                    case ".NET CLR Memory:# of Pinned Objects": t.PinnedObjects = perfCounter.NextValue();
                        break;
                    case ".NET CLR Memory:Large Object Heap Size": t.LargeObjectHeapSize = perfCounter.NextValue();
                        break;
                    case ".NET CLR Exceptions:# of Exceps Thrown / sec": t.ExceptionsThrownPerSecond = perfCounter.NextValue();
                        break;
                    case ".NET CLR LocksAndThreads:Contention Rate / sec": t.ContentionRatePerSecond = perfCounter.NextValue();
                        break;
                    case ".NET CLR LocksAndThreads:Current Queue Length": t.CurrentQueueLength = perfCounter.NextValue();
                        break;
                    case ".NET CLR LocksAndThreads:# of current physical Threads": t.CurrentPhysicalThreads = perfCounter.NextValue();
                        break;
                    #endregion

                    #region ASP.NET
                    case "ASP.NET Applications:Requests/Sec": t.ASPNetAppRequestsPerSecond = perfCounter.NextValue();
                        break;
                    case "Web Service:ISAPI Extension Requests/sec": t.ISAPIExtensionRequestsPerSecond = perfCounter.NextValue();
                        break;
                    case "ASP.NET:Requests Current": t.ASPNetRequestsCurrent = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:ASP.NET Applications": t.ASPNetAppRequestsExecuting = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Requests Timed Out": t.ASPNetAppRequestsTimedOut = perfCounter.NextValue();
                        break;
                    case "ASP.NET:Requests Queued": t.ASPNetRequestsQueued = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Requests In Application Queue": t.ASPNetAppRequestsInApplicationQueue = perfCounter.NextValue();
                        break;
                    case "ASP.NET:Requests Rejected": t.ASPNetRequestsRejected = perfCounter.NextValue();
                        break;
                    case "ASP.NET:Request Wait Time": t.ASPNetRequestsWaitTime = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Cache Total Entries": t.ASPNetAppCacheTotalEntries = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Cache Total Hit Ratio": t.ASPNetAppCacheHitRation = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Cache Total Turnover Rate": t.ASPNetAppCacheTotalTurnoverRate = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Cache API Hit Ratio": t.ASPNetAppCacheAPIHitRatio = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Cache API Turnover Rate": t.ASPNetAppCacheAPITurnoverRate = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Output Cache Entries": t.ASPNetAppOutputCacheEntries = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Output Cache Hit Ratio": t.ASPNetAppOutputCachceHitRatio = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Output Cache Turnover Rate": t.ASPNetAppOutputCachceTurnoverRate = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Errors Total/sec": t.ASPNetAppErrorsTotalPerSecond = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Errors During Execution": t.ASPNetAppErrorsDuringExecution = perfCounter.NextValue();
                        break;
                    case "ASP.NET Applications:Errors Unhandled During Execution/sec": t.ASPNetAppErrorsUnhandledPerSecond = perfCounter.NextValue();
                        break;
                    #endregion

                    #region Process
                    case "Process:% Privileged Time": t.ProcessPrivilagedTime = perfCounter.NextValue();
                        break;
                    case "Process:% Processor Time": t.ProcessProcessorTime = perfCounter.NextValue();
                        break;
                    case "Process:% User Time": t.ProcessUserTime = perfCounter.NextValue();
                        break;
                    case "Process:Handle Count": t.ProcessHandleCount = perfCounter.NextValue();
                        break;
                    case "Process:IO Data Operations/sec": t.ProcessIODataOperationsPerSec = perfCounter.NextValue();
                        break;
                    case "Process:Page Faults/sec": t.ProcessPageFaultsPerSec = perfCounter.NextValue();
                        break;
                    case "Process:Pool Nonpaged Bytes": t.ProcessPoolNonpagedBytes = perfCounter.NextValue();
                        break;
                    case "Process:Pool Paged Bytes": t.ProcessPoolPagedBytes = perfCounter.NextValue();
                        break;
                    case "Process:Private Bytes": t.ProcessPrivateBytes = perfCounter.NextValue();
                        break;
                    case "Process:Working Set": t.ProcessWorkingSet = perfCounter.NextValue();
                        break;
                    case "Process:Virtual Bytes": t.ProcessVirtualBytes = perfCounter.NextValue();
                        break;
                    #endregion

                    #endregion
                    default:
                        break;
                }
            }
        }

        private static void CaptureFormulaBasedCounters(AzureMonitorEntity t)
        {
            string perfCounterName;
            int i = 0;

            while (i < 2)
            {
                i++;
                foreach (PerformanceCounter perfCounter in formulaBasedPerfCounters)
                {
                    perfCounterName = perfCounter.CategoryName + ":" + perfCounter.CounterName;
                    switch (perfCounterName)
                    {
                        #region Caputer Counters

                        #region Processor
                        case "Processor:% Processor Time": t.ProcessorTime = perfCounter.NextValue();
                            break;
                        case "Processor:% Privileged Time": t.PrivilegedTime = perfCounter.NextValue();
                            break;
                        case "Processor:% Interrupt Time": t.InterruptTime = perfCounter.NextValue();
                            break;
                        case "System:Processor Queue Length": t.ProcessorQueueLength = perfCounter.NextValue();
                            break;
                        case "System:Context Switches/sec": t.ContentionRatePerSecond = perfCounter.NextValue();
                            break;
                        #endregion

                        #region Memory
                        case "Memory:Available Mbytes": t.AvaliableMbytes = perfCounter.NextValue();
                            break;
                        case "Memory:Page Reads/sec": t.PageReadsPerSecond = perfCounter.NextValue();
                            break;
                        case "Memory:Pages/sec": t.PagesPerSecond = perfCounter.NextValue();
                            break;
                        case "Memory:Pool Nonpaged Bytes": t.PoolNonpagedBytes = perfCounter.NextValue();
                            break;
                        case "Server:Pool Nonpaged Failures": t.PoolNonpagedFailures = perfCounter.NextValue();
                            break;
                        case "Server:Pool Paged Failures": t.PoolPagedFailures = perfCounter.NextValue();
                            break;
                        case "Server:Pool Nonpaged Peak": t.PoolNonpagedPeak = perfCounter.NextValue();
                            break;
                        case "Memory:Cache Bytes": t.CacheBytes = perfCounter.NextValue();
                            break;
                        case "Memory:Cache Faults/sec": t.CacheFaultsPerSecond = perfCounter.NextValue();
                            break;
                        case "Cache:MDL Read Hits %": t.MDLReadHitsPercentage = perfCounter.NextValue();
                            break;
                        #endregion

                        #region Disk IO
                        case "PhysicalDisk:Avg. Disk Queue Length": t.AvgDiskQueueLength = perfCounter.NextValue();
                            break;
                        case "PhysicalDisk:Avg. Disk Read Queue Length": t.AvgDiskReadQueueLength = perfCounter.NextValue();
                            break;
                        case "PhysicalDisk:Avg. Disk Write Queue Length": t.AvgDiskWriteQueueLength = perfCounter.NextValue();
                            break;
                        case "PhysicalDisk:Avg. Disk sec/Read": t.AvgDiskSecPerRead = perfCounter.NextValue();
                            break;
                        case "PhysicalDisk:Avg. Disk sec/Transfer": t.AvgDiskSecPerTransfer = perfCounter.NextValue();
                            break;
                        case "PhysicalDisk:Disk Writes/sec": t.DiskWritesPerSecond = perfCounter.NextValue();
                            break;
                        #endregion

                        #region Network IO
                        case "Network Interface:Bytes Total/sec": t.NtwrkInterfaceBytesTotalPerSecond = perfCounter.NextValue();
                            break;
                        case "Network Interface:Bytes Received/sec": t.NtwrkInterfaceBytesRecievedPerSecond = perfCounter.NextValue();
                            break;
                        case "Network Interface:Bytes Sent/sec": t.NtwrkInterfaceBytesSentPerSecond = perfCounter.NextValue();
                            break;
                        case "Server:Bytes Total/sec": t.ServerBytesTotalPerSecond = perfCounter.NextValue();
                            break;
                        #endregion

                        #region CLR
                        case ".NET CLR Memory:% Time in GC": t.PercentageTimeInGC = perfCounter.NextValue();
                            break;
                        case ".NET CLR Memory:# Bytes in all Heaps": t.BytesInAllHeaps = perfCounter.NextValue();
                            break;
                        case ".NET CLR Memory:# Gen 0 Collections": t.Gen0Collections = perfCounter.NextValue();
                            break;
                        case ".NET CLR Memory:# Gen 1 Collections": t.Gen1Collections = perfCounter.NextValue();
                            break;
                        case ".NET CLR Memory:# Gen 2 Collections": t.Gen2Colelctions = perfCounter.NextValue();
                            break;
                        case ".NET CLR Memory:# of Pinned Objects": t.PinnedObjects = perfCounter.NextValue();
                            break;
                        case ".NET CLR Memory:Large Object Heap Size": t.LargeObjectHeapSize = perfCounter.NextValue();
                            break;
                        case ".NET CLR Exceptions:# of Exceps Thrown / sec": t.ExceptionsThrownPerSecond = perfCounter.NextValue();
                            break;
                        case ".NET CLR LocksAndThreads:Contention Rate / sec": t.ContentionRatePerSecond = perfCounter.NextValue();
                            break;
                        case ".NET CLR LocksAndThreads:Current Queue Length": t.CurrentQueueLength = perfCounter.NextValue();
                            break;
                        case ".NET CLR LocksAndThreads:# of current physical Threads": t.CurrentPhysicalThreads = perfCounter.NextValue();
                            break;
                        #endregion

                        #region ASP.NET
                        case "ASP.NET Applications:Requests/Sec": t.ASPNetAppRequestsPerSecond = perfCounter.NextValue();
                            break;
                        case "Web Service:ISAPI Extension Requests/sec": t.ISAPIExtensionRequestsPerSecond = perfCounter.NextValue();
                            break;
                        case "ASP.NET:Requests Current": t.ASPNetRequestsCurrent = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:ASP.NET Applications": t.ASPNetAppRequestsExecuting = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Requests Timed Out": t.ASPNetAppRequestsTimedOut = perfCounter.NextValue();
                            break;
                        case "ASP.NET:Requests Queued": t.ASPNetRequestsQueued = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Requests In Application Queue": t.ASPNetAppRequestsInApplicationQueue = perfCounter.NextValue();
                            break;
                        case "ASP.NET:Requests Rejected": t.ASPNetRequestsRejected = perfCounter.NextValue();
                            break;
                        case "ASP.NET:Request Wait Time": t.ASPNetRequestsWaitTime = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Cache Total Entries": t.ASPNetAppCacheTotalEntries = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Cache Total Hit Ratio": t.ASPNetAppCacheHitRation = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Cache Total Turnover Rate": t.ASPNetAppCacheTotalTurnoverRate = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Cache API Hit Ratio": t.ASPNetAppCacheAPIHitRatio = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Cache API Turnover Rate": t.ASPNetAppCacheAPITurnoverRate = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Output Cache Entries": t.ASPNetAppOutputCacheEntries = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Output Cache Hit Ratio": t.ASPNetAppOutputCachceHitRatio = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Output Cache Turnover Rate": t.ASPNetAppOutputCachceTurnoverRate = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Errors Total/sec": t.ASPNetAppErrorsTotalPerSecond = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Errors During Execution": t.ASPNetAppErrorsDuringExecution = perfCounter.NextValue();
                            break;
                        case "ASP.NET Applications:Errors Unhandled During Execution/sec": t.ASPNetAppErrorsUnhandledPerSecond = perfCounter.NextValue();
                            break;
                        #endregion

                        #region Process
                        case "Process:% Privileged Time": t.ProcessPrivilagedTime = perfCounter.NextValue();
                            break;
                        case "Process:% Processor Time": t.ProcessProcessorTime = perfCounter.NextValue();
                            break;
                        case "Process:% User Time": t.ProcessUserTime = perfCounter.NextValue();
                            break;
                        case "Process:Handle Count": t.ProcessHandleCount = perfCounter.NextValue();
                            break;
                        case "Process:IO Data Operations/sec": t.ProcessIODataOperationsPerSec = perfCounter.NextValue();
                            break;
                        case "Process:Page Faults/sec": t.ProcessPageFaultsPerSec = perfCounter.NextValue();
                            break;
                        case "Process:Pool Nonpaged Bytes": t.ProcessPoolNonpagedBytes = perfCounter.NextValue();
                            break;
                        case "Process:Pool Paged Bytes": t.ProcessPoolPagedBytes = perfCounter.NextValue();
                            break;
                        case "Process:Private Bytes": t.ProcessPrivateBytes = perfCounter.NextValue();
                            break;
                        case "Process:Working Set": t.ProcessWorkingSet = perfCounter.NextValue();
                            break;
                        case "Process:Virtual Bytes": t.ProcessVirtualBytes = perfCounter.NextValue();
                            break;
                        #endregion

                        #endregion
                        default:
                            break;
                    }
                }
                Thread.Sleep(1000);
            }
        }

        private static void CaptureRawLazyCounters(AzureMonitorEntity t)
        {
            string perfCounterName;
            const string wcfOp_GetStationDescriptionForMapBoundaries = "GetStationDescriptionsForMapBoundaries";
            const string wcfOp_GetClosestStation = "GetClosestStation";
            const string wcfOp_GetStationByCode = "GetStationByCode";
            const string wcfOp_GetPushPin = "GetPushpin";
            const string wcfOp_SetRating = "SetRating";
            const string wcfOp_GetLanguage = "GetLanguage";
            const string wcfOp_GetLanguages = "GetLanguages";
            const string wcfOp_DoWork = "DoWork";
            const string wcfOp_GetSearchResults = "GetSearchResults";

            foreach (PerformanceCounter perfCounter in rawLazyCounters)
            {
                perfCounterName = perfCounter.CategoryName + ":" + perfCounter.CounterName;
                switch (perfCounterName)
                {
                    #region Caputer Counters

                    #region WCF
                    #region Calls
                    case "ServiceModelOperation 3.0.0.0:Calls":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.Calls_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.Calls_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.Calls_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.Calls_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.Calls_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.Calls_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.Calls_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.Calls_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.Calls_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion

                    #region CallsDuration
                    case "ServiceModelOperation 3.0.0.0:CallsDuration":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.CallsDuration_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.CallsDuration_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.CallsDuration_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.CallsDuration_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.CallsDuration_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.CallsDuration_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.CallsDuration_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.CallsDuration_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.CallsDuration_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion

                    #region CallsFailed
                    case "ServiceModelOperation 3.0.0.0:CallsFailed":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.CallsFailed_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.CallsFailed_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.CallsFailed_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.CallsFailed_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.CallsFailed_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.CallsFailed_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.CallsFailed_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.CallsFailed_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.CallsFailed_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion

                    #region CallsPerSecond
                    case "ServiceModelOperation 3.0.0.0:CallsPerSecond":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.CallsPerSecond_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.CallsPerSecond_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.CallsPerSecond_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.CallsPerSecond_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.CallsPerSecond_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.CallsPerSecond_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.CallsPerSecond_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.CallsPerSecond_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.CallsPerSecond_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion
                    #endregion

                    #endregion
                    default:
                        break;
                }
            }
        }

        private static void CaptureFormulaBasedLazyCounters(AzureMonitorEntity t)
        {
            string perfCounterName;
            const string wcfOp_GetStationDescriptionForMapBoundaries = "GetStationDescriptionsForMapBoundaries";
            const string wcfOp_GetClosestStation = "GetClosestStation";
            const string wcfOp_GetStationByCode = "GetStationByCode";
            const string wcfOp_GetPushPin = "GetPushpin";
            const string wcfOp_SetRating = "SetRating";
            const string wcfOp_GetLanguage = "GetLanguage";
            const string wcfOp_GetLanguages = "GetLanguages";
            const string wcfOp_DoWork = "DoWork";
            const string wcfOp_GetSearchResults = "GetSearchResults";

            foreach (PerformanceCounter perfCounter in formulaBasedLazyCounters)
            {
                perfCounterName = perfCounter.CategoryName + ":" + perfCounter.CounterName;
                switch (perfCounterName)
                {
                    #region Caputer Counters

                    #region WCF
                    #region Calls
                    case "ServiceModelOperation 3.0.0.0:Calls":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.Calls_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.Calls_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.Calls_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.Calls_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.Calls_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.Calls_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.Calls_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.Calls_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.Calls_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion

                    #region CallsDuration
                    case "ServiceModelOperation 3.0.0.0:CallsDuration":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.CallsDuration_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.CallsDuration_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.CallsDuration_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.CallsDuration_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.CallsDuration_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.CallsDuration_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.CallsDuration_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.CallsDuration_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.CallsDuration_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion

                    #region CallsFailed
                    case "ServiceModelOperation 3.0.0.0:CallsFailed":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.CallsFailed_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.CallsFailed_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.CallsFailed_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.CallsFailed_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.CallsFailed_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.CallsFailed_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.CallsFailed_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.CallsFailed_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.CallsFailed_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion

                    #region CallsPerSecond
                    case "ServiceModelOperation 3.0.0.0:CallsPerSecond":
                        if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetClosestStation.ToLower()))
                        {
                            t.CallsPerSecond_GetClosestStation = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationByCode.ToLower()))
                        {
                            t.CallsPerSecond_GetStationByCode = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetStationDescriptionForMapBoundaries.ToLower()))
                        {
                            t.CallsPerSecond_GetStationDescriptionForMapBoundaries = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetPushPin.ToLower()))
                        {
                            t.CallsPerSecond_GetPushpin = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_SetRating.ToLower()))
                        {
                            t.CallsPerSecond_SetRating = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_DoWork.ToLower()))
                        {
                            t.CallsPerSecond_MappingService = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguage.ToLower()))
                        {
                            t.CallsPerSecond_GetLanguage = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetLanguages.ToLower()))
                        {
                            t.CallsPerSecond_GetLanguages = perfCounter.NextValue();
                        }
                        else if (perfCounter.InstanceName.ToLower().Contains(wcfOp_GetSearchResults.ToLower()))
                        {
                            t.CallsPerSecond_GetSearchResults = perfCounter.NextValue();
                        }
                        break;
                    #endregion
                    #endregion

                    #endregion
                    default:
                        break;
                }
            }
        }
    }

}
