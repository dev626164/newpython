﻿NOTE: Some code in this solution is the property of Microsoft.

The AzureMonitorLib project includes cloud storage code derived from code in the Microsoft Azure SDK StorageClient sample.

The Thumbnails projects are instrumented versions of the code in the Microsoft Azure SDK Thumbnails sample.

