//
// <copyright file="Errors.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//
using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Linq;
using System.Xml;
using System.Xml.Linq;


// disable the generation of warnings for missing documentation elements for 
// public classes/members in this file
// justification is that this file contains many public constants whose names 
// sufficiently reflect their intended usage 
#pragma warning disable 1591

namespace Microsoft.Samples.ServiceHosting.StorageClient
{
    /// <summary>
    /// Error codes that can be returned by the storage service or the client library.
    /// These are divided into server errors and client errors depending on which side
    /// the error can be attributed to.
    /// </summary>
    public enum StorageErrorCodeMod
    {
        None = 0,

        //Server errors
        ServiceInternalError = 1,
        ServiceTimeout,
        ServiceIntegrityCheckFailed,
        TransportError,
        ServiceBadResponse,

        //Client errors
        ResourceNotFound,
        AccountNotFound,
        ContainerNotFound,
        BlobNotFound,
        AuthenticationFailure,
        AccessDenied,
        ResourceAlreadyExists,
        ContainerAlreadyExists,
        BlobAlreadyExists,
        BadRequest,
        ConditionFailed,
        BadGateway
    }

    [Serializable]
    public class StorageExtendedErrorInformationMod
    {
        public string ErrorCode { get; internal set; }
        public string ErrorMessage { get; internal set; }
        public NameValueCollection AdditionalDetails { get; internal set; }
    }

    /// <summary>
    /// The base class for storage service exceptions
    /// </summary>
    [Serializable]
    public abstract class StorageExceptionMod : Exception
    {
        /// <summary>
        /// The Http status code returned by the storage service
        /// </summary>
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// The specific error code returned by the storage service
        /// </summary>
        public StorageErrorCodeMod ErrorCode { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public StorageExtendedErrorInformationMod ExtendedErrorInformation { get; private set; }

        protected StorageExceptionMod()
        {
        }

        protected StorageExceptionMod(
            StorageErrorCodeMod errorCode,
            string message,
            HttpStatusCode statusCode,
            StorageExtendedErrorInformationMod extendedErrorInfo,
            Exception innerException
            )
            : base(message, innerException)
        {
            this.ErrorCode = errorCode;
            this.StatusCode = statusCode;
            this.ExtendedErrorInformation = extendedErrorInfo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageException"/> class with
        /// serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> object that contains serialized object
        /// data about the exception being thrown</param>
        /// <param name="context">The <see cref="StreamingContext"/> object that contains contextual information
        /// about the source or destionation. </param>
        protected StorageExceptionMod(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (null == info)
            {
                throw new ArgumentNullException("info");
            }

            this.StatusCode = (HttpStatusCode)info.GetValue("StatusCode", typeof(HttpStatusCode));
            this.ErrorCode = (StorageErrorCodeMod)info.GetValue("ErrorCode", typeof(StorageErrorCodeMod));
            this.ExtendedErrorInformation = (StorageExtendedErrorInformationMod)info.GetValue(
                        "ExtendedErrorInformation", typeof(StorageExtendedErrorInformationMod));
        }

        /// <summary>
        /// Sets the <see cref="SerializationInfo"/> object with additional exception information
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> object that holds the 
        /// serialized object data.</param>
        /// <param name="context">The <see cref="StreamingContext"/> object that contains contextual information
        /// about the source or destionation. </param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (null == info)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("StatusCode", this.StatusCode);
            info.AddValue("ErrorCode", this.ErrorCode);
            info.AddValue("ExtendedErrorInformation", this.ExtendedErrorInformation);
            base.GetObjectData(info, context);
        }

    }

    /// <summary>
    /// Server exceptions are those due to server side problems.
    /// These may be transient and requests resulting in such exceptions
    /// can be retried with the same parameters.
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors",
        Justification = "Since this exception comes from the server, there must be an HTTP response code associated with it, hence we exclude the default constructor taking only a string but no status code.")]
    [Serializable]
    public class StorageServerExceptionMod : StorageExceptionMod
    {
        internal StorageServerExceptionMod(
            StorageErrorCodeMod errorCode,
            string message,
            HttpStatusCode statusCode,
            Exception innerException
            )
            : base(errorCode, message, statusCode, null, innerException)
        {
        }

        internal StorageServerExceptionMod(
            StorageErrorCodeMod errorCode,
            string message,
            HttpStatusCode statusCode,
            StorageExtendedErrorInformationMod extendedErrorInfo,
            Exception innerException
            )
            : base(errorCode, message, statusCode, extendedErrorInfo, innerException)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="StorageServerException"/> class with
        /// serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> object that contains serialized object
        /// data about the exception being thrown</param>
        /// <param name="context">The <see cref="StreamingContext"/> object that contains contextual information
        /// about the source or destionation. </param>
        protected StorageServerExceptionMod(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public StorageServerExceptionMod()
        {
        }
    }

    /// <summary>
    /// Client side exceptions are due to incorrect parameters to the request.
    /// These requests should not be retried with the same parameters
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors",
        Justification = "Since this exception comes from the server, there must be an HTTP response code associated with it, hence we exclude the default constructor taking only a string but no status code.")]
    [Serializable]
    public class StorageClientExceptionMod : StorageExceptionMod
    {
        internal StorageClientExceptionMod(
            StorageErrorCodeMod errorCode,
            string message,
            HttpStatusCode statusCode,
            StorageExtendedErrorInformationMod extendedErrorInfo,
            Exception innerException
            )
            : base(errorCode, message, statusCode, extendedErrorInfo, innerException)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="StorageClientException"/> class with
        /// serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> object that contains serialized object
        /// data about the exception being thrown</param>
        /// <param name="context">The <see cref="StreamingContext"/> object that contains contextual information
        /// about the source or destionation. </param>
        protected StorageClientExceptionMod(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public StorageClientExceptionMod()
        {
        }

    }

    #region Error code strings that can be returned in the StorageExtendedErrorInformation.ErrorCode
    /// <summary>
    /// Error code strings that are common to all storage services
    /// </summary>
    public static class StorageErrorCodeStringsMod
    {
        public const string UnsupportedHttpVerb = "UnsupportedHttpVerb";
        public const string MissingContentLengthHeader = "MissingContentLengthHeader";
        public const string MissingRequiredHeader = "MissingRequiredHeader";
        public const string MissingRequiredXmlNode = "MissingRequiredXmlNode";
        public const string UnsupportedHeader = "UnsupportedHeader";
        public const string UnsupportedXmlNode = "UnsupportedXmlNode";
        public const string InvalidHeaderValue = "InvalidHeaderValue";
        public const string InvalidXmlNodeValue = "InvalidXmlNodeValue";
        public const string MissingRequiredQueryParameter = "MissingRequiredQueryParameter";
        public const string UnsupportedQueryParameter = "UnsupportedQueryParameter";
        public const string InvalidQueryParameterValue = "InvalidQueryParameterValue";
        public const string OutOfRangeQueryParameterValue = "OutOfRangeQueryParameterValue";
        public const string InvalidUri = "InvalidUri";
        public const string InvalidHttpVerb = "InvalidHttpVerb";
        public const string EmptyMetadataKey = "EmptyMetadataKey";
        public const string RequestBodyTooLarge = "RequestBodyTooLarge";
        public const string InvalidXmlDocument = "InvalidXmlDocument";
        public const string InternalError = "InternalError";
        public const string AuthenticationFailed = "AuthenticationFailed";
        public const string Md5Mismatch = "Md5Mismatch";
        public const string InvalidMd5 = "InvalidMd5";
        public const string OutOfRangeInput = "OutOfRangeInput";
        public const string InvalidInput = "InvalidInput";
        public const string OperationTimedOut = "OperationTimedOut";
        public const string ResourceNotFound = "ResourceNotFound";
        public const string InvalidMetadata = "InvalidMetadata";
        public const string MetadataTooLarge = "MetadataTooLarge";
        public const string ConditionNotMet = "ConditionNotMet";
        public const string InvalidRange = "InvalidRange";
        public const string ContainerNotFound = "ContainerNotFound";
        public const string ContainerAlreadyExists = "ContainerAlreadyExists";
        public const string ContainerDisabled = "ContainerDisabled";
        public const string ContainerBeingDeleted = "ContainerBeingDeleted";
        public const string ServerBusy = "ServerBusy";
    }

    /// <summary>
    /// Error code strings that are specific to blob service
    /// </summary>
    public static class BlobErrorCodeStringsMod
    {
        public const string InvalidBlockId = "InvalidBlockId";
        public const string BlobNotFound = "BlobNotFound";
        public const string BlobAlreadyExists = "BlobAlreadyExists";
        public const string InvalidBlobOrBlock = "InvalidBlobOrBlock";
        public const string InvalidBlockList = "InvalidBlockList";
    }

    /// <summary>
    /// Error code strings that are specific to queue service
    /// </summary>
    public static class QueueErrorCodeStringsMod
    {
        public const string QueueNotFound = "QueueNotFound";
        public const string QueueDisabled = "QueueDisabled";
        public const string QueueAlreadyExists = "QueueAlreadyExists";
        public const string QueueNotEmpty = "QueueNotEmpty";
        public const string QueueBeingDeleted = "QueueBeingDeleted";
        public const string PopReceiptMismatch = "PopReceiptMismatch";
        public const string InvalidParameter = "InvalidParameter";
        public const string MessageNotFound = "MessageNotFound";
        public const string MessageTooLarge = "MessageTooLarge";
        public const string InvalidMarker = "InvalidMarker";
    }

    /// <summary>
    /// Error code strings that are specific to queue service
    /// </summary>
    ///     public static class TableErrorCodeStrings
    public static class TableErrorCodeStringsMod
    {
        public const string XMethodNotUsingPost = "XMethodNotUsingPost";
        public const string XMethodIncorrectValue = "XMethodIncorrectValue";
        public const string XMethodIncorrectCount = "XMethodIncorrectCount";

        public const string TableHasNoProperties = "TableHasNoProperties";
        public const string DuplicatePropertiesSpecified = "DuplicatePropertiesSpecified";
        public const string TableHasNoSuchProperty = "TableHasNoSuchProperty";
        public const string DuplicateKeyPropertySpecified = "DuplicateKeyPropertySpecified";
        public const string TableAlreadyExists = "TableAlreadyExists";
        public const string TableNotFound = "TableNotFound";
        public const string EntityNotFound = "EntityNotFound";
        public const string EntityAlreadyExists = "EntityAlreadyExists";
        public const string PartitionKeyNotSpecified = "PartitionKeyNotSpecified";
        public const string OperatorInvalid = "OperatorInvalid";
        public const string UpdateConditionNotSatisfied = "UpdateConditionNotSatisfied";
        public const string PropertiesNeedValue = "PropertiesNeedValue";

        public const string PartitionKeyPropertyCannotBeUpdated = "PartitionKeyPropertyCannotBeUpdated";
        public const string TooManyProperties = "TooManyProperties";
        public const string EntityTooLarge = "EntityTooLarge";
        public const string PropertyValueTooLarge = "PropertyValueTooLarge";
        public const string InvalidValueType = "InvalidValueType";
        public const string TableBeingDeleted = "TableBeingDeleted";
        public const string TableServerOutOfMemory = "TableServerOutOfMemory";
        public const string PrimaryKeyPropertyIsInvalidType = "PrimaryKeyPropertyIsInvalidType";
        public const string PropertyNameTooLong = "PropertyNameTooLong";
        public const string PropertyNameInvalid = "PropertyNameInvalid";

        public const string BatchOperationNotSupported = "BatchOperationNotSupported";
        public const string JsonFormatNotSupported = "JsonFormatNotSupported";
        public const string MethodNotAllowed = "MethodNotAllowed";
        public const string NotImplemented = "NotImplemented";
    }
    #endregion

    #region Helper functions dealing with errors
    internal static partial class Utilities
    {
        internal static void ProcessUnexpectedStatusCode(HttpWebResponse response)
        {
            throw new StorageServerExceptionMod(
                        StorageErrorCodeMod.ServiceBadResponse,
                        response.StatusDescription,
                        response.StatusCode,
                        null
                        );
        }

        internal static Exception TranslateWebException(Exception e)
        {
            WebException we = e as WebException;
            if (null == we)
            {
                return e;
            }

            // If the response is not null, let's first see what the status code is.
            if (we.Response != null)
            {
                HttpWebResponse response = ((HttpWebResponse)we.Response);

                StorageExtendedErrorInformationMod extendedError =
                    GetExtendedErrorDetailsFromResponse(
                            response.GetResponseStream(),
                            response.ContentLength
                            );
                Exception translatedException = null;
                if (extendedError != null)
                {
                    translatedException = TranslateExtendedError(
                                                    extendedError,
                                                    response.StatusCode,
                                                    response.StatusDescription,
                                                    e);
                    if (translatedException != null)
                        return translatedException;
                }
                translatedException = TranslateFromHttpStatus(
                                            response.StatusCode,
                                            response.StatusDescription,
                                            extendedError,
                                            we
                                            );
                if (translatedException != null)
                    return translatedException;

            }

            switch (we.Status)
            {
                case WebExceptionStatus.RequestCanceled:
                    return new StorageServerExceptionMod(
                        StorageErrorCodeMod.ServiceTimeout,
                        "The server request did not complete within the specified timeout",
                        HttpStatusCode.GatewayTimeout,
                        we);

                case WebExceptionStatus.ConnectFailure:
                    return we;

                default:
                    return new StorageServerExceptionMod(
                        StorageErrorCodeMod.ServiceInternalError,
                        "The server encountered an unknown failure: " + e.Message,
                        HttpStatusCode.InternalServerError,
                        we
                        );
            }
        }

        internal static Exception TranslateFromHttpStatus(
                    HttpStatusCode statusCode,
                    string statusDescription,
                    StorageExtendedErrorInformationMod details,
                    Exception inner
                    )
        {
            switch (statusCode)
            {
                case HttpStatusCode.Forbidden:
                    return new StorageClientExceptionMod(
                        StorageErrorCodeMod.AccessDenied,
                        statusDescription,
                        HttpStatusCode.Forbidden,
                        details,
                        inner
                        );

                case HttpStatusCode.Gone:
                case HttpStatusCode.NotFound:
                    return new StorageClientExceptionMod(
                        StorageErrorCodeMod.ResourceNotFound,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );

                case HttpStatusCode.BadRequest:
                    return new StorageClientExceptionMod(
                        StorageErrorCodeMod.BadRequest,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );

                case HttpStatusCode.PreconditionFailed:
                case HttpStatusCode.NotModified:
                    return new StorageClientExceptionMod(
                        StorageErrorCodeMod.BadRequest,
                        statusDescription,
                        statusCode,
                        details,
                        inner);

                case HttpStatusCode.Conflict:
                    return new StorageClientExceptionMod(
                        StorageErrorCodeMod.ResourceAlreadyExists,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );

                case HttpStatusCode.GatewayTimeout:
                    return new StorageServerExceptionMod(
                        StorageErrorCodeMod.ServiceTimeout,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );

                case HttpStatusCode.RequestedRangeNotSatisfiable:
                    return new StorageClientExceptionMod(
                        StorageErrorCodeMod.BadRequest,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );

                case HttpStatusCode.InternalServerError:
                    return new StorageServerExceptionMod(
                        StorageErrorCodeMod.ServiceInternalError,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );

                case HttpStatusCode.BadGateway:
                    return new StorageServerExceptionMod(
                        StorageErrorCodeMod.BadGateway,
                        statusDescription,
                        statusCode,
                        details,
                        inner
                        );
            }
            return null;
        }

        private static Exception TranslateExtendedError(
                    StorageExtendedErrorInformationMod details,
                    HttpStatusCode statusCode,
                    string statusDescription,
                    Exception inner
                    )
        {
            StorageErrorCodeMod errorCode = default(StorageErrorCodeMod);
            switch (details.ErrorCode)
            {
                case StorageErrorCodeStringsMod.UnsupportedHttpVerb:
                case StorageErrorCodeStringsMod.MissingContentLengthHeader:
                case StorageErrorCodeStringsMod.MissingRequiredHeader:
                case StorageErrorCodeStringsMod.UnsupportedHeader:
                case StorageErrorCodeStringsMod.InvalidHeaderValue:
                case StorageErrorCodeStringsMod.MissingRequiredQueryParameter:
                case StorageErrorCodeStringsMod.UnsupportedQueryParameter:
                case StorageErrorCodeStringsMod.InvalidQueryParameterValue:
                case StorageErrorCodeStringsMod.OutOfRangeQueryParameterValue:
                case StorageErrorCodeStringsMod.InvalidUri:
                case StorageErrorCodeStringsMod.InvalidHttpVerb:
                case StorageErrorCodeStringsMod.EmptyMetadataKey:
                case StorageErrorCodeStringsMod.RequestBodyTooLarge:
                case StorageErrorCodeStringsMod.InvalidXmlDocument:
                case StorageErrorCodeStringsMod.InvalidXmlNodeValue:
                case StorageErrorCodeStringsMod.MissingRequiredXmlNode:
                case StorageErrorCodeStringsMod.InvalidMd5:
                case StorageErrorCodeStringsMod.OutOfRangeInput:
                case StorageErrorCodeStringsMod.InvalidInput:
                case StorageErrorCodeStringsMod.InvalidMetadata:
                case StorageErrorCodeStringsMod.MetadataTooLarge:
                case StorageErrorCodeStringsMod.InvalidRange:
                    errorCode = StorageErrorCodeMod.BadRequest;
                    break;
                case StorageErrorCodeStringsMod.AuthenticationFailed:
                    errorCode = StorageErrorCodeMod.AuthenticationFailure;
                    break;
                case StorageErrorCodeStringsMod.ResourceNotFound:
                    errorCode = StorageErrorCodeMod.ResourceNotFound;
                    break;
                case StorageErrorCodeStringsMod.ConditionNotMet:
                    errorCode = StorageErrorCodeMod.ConditionFailed;
                    break;
                case StorageErrorCodeStringsMod.ContainerAlreadyExists:
                    errorCode = StorageErrorCodeMod.ContainerAlreadyExists;
                    break;
                case StorageErrorCodeStringsMod.ContainerNotFound:
                    errorCode = StorageErrorCodeMod.ContainerNotFound;
                    break;
                case BlobErrorCodeStringsMod.BlobNotFound:
                    errorCode = StorageErrorCodeMod.BlobNotFound;
                    break;
                case BlobErrorCodeStringsMod.BlobAlreadyExists:
                    errorCode = StorageErrorCodeMod.BlobAlreadyExists;
                    break;
            }

            if (errorCode != default(StorageErrorCodeMod))
                return new StorageClientExceptionMod(
                                errorCode,
                                statusDescription,
                                statusCode,
                                details,
                                inner
                                );

            switch (details.ErrorCode)
            {
                case StorageErrorCodeStringsMod.InternalError:
                case StorageErrorCodeStringsMod.ServerBusy:
                    errorCode = StorageErrorCodeMod.ServiceInternalError;
                    break;
                case StorageErrorCodeStringsMod.Md5Mismatch:
                    errorCode = StorageErrorCodeMod.ServiceIntegrityCheckFailed;
                    break;
                case StorageErrorCodeStringsMod.OperationTimedOut:
                    errorCode = StorageErrorCodeMod.ServiceTimeout;
                    break;
            }
            if (errorCode != default(StorageErrorCodeMod))
                return new StorageServerExceptionMod(
                                errorCode,
                                statusDescription,
                                statusCode,
                                details,
                                inner
                                );



            return null;
        }


        // This is the limit where we allow for the error message returned by the server.
        // Message longer than that will be truncated. 
        private const int ErrorTextSizeLimit = 8 * 1024;

        private static StorageExtendedErrorInformationMod GetExtendedErrorDetailsFromResponse(
            Stream httpResponseStream,
            long contentLength
            )
        {
            try
            {
                int bytesToRead = (int)Math.Max((long)contentLength, (long)ErrorTextSizeLimit);
                byte[] responseBuffer = new byte[bytesToRead];
                int bytesRead = CopyStreamToBuffer(httpResponseStream, responseBuffer, (int)bytesToRead);
                return GetErrorDetailsFromStream(
                            new MemoryStream(responseBuffer, 0, bytesRead, false)
                            );
            }
            catch (WebException)
            {
                //Ignore network errors when reading error details.
                return null;
            }
            catch (IOException)
            {
                return null;
            }
            catch (TimeoutException)
            {
                return null;
            }
        }

        private static StorageExtendedErrorInformationMod GetErrorDetailsFromStream(
            Stream inputStream
            )
        {
            StorageExtendedErrorInformationMod extendedError = new StorageExtendedErrorInformationMod();
            try
            {
                using (XmlReader reader = XmlReader.Create(inputStream))
                {
                    reader.Read();
                    reader.ReadStartElement(StorageHttpConstants.XmlElementNamesMod.ErrorRootElement);
                    extendedError.ErrorCode = reader.ReadElementString(StorageHttpConstants.XmlElementNamesMod.ErrorCode);
                    extendedError.ErrorMessage = reader.ReadElementString(StorageHttpConstants.XmlElementNamesMod.ErrorMessage);
                    extendedError.AdditionalDetails = new NameValueCollection();

                    // After error code and message we can have a number of additional details optionally followed
                    // by ExceptionDetails element - we'll read all of these into the additionalDetails collection
                    do
                    {
                        if (reader.IsStartElement())
                        {
                            if (string.Compare(reader.LocalName, StorageHttpConstants.XmlElementNamesMod.ErrorException, StringComparison.Ordinal) == 0)
                            {
                                // Need to read exception details - we have message and stack trace
                                reader.ReadStartElement(StorageHttpConstants.XmlElementNamesMod.ErrorException);
                                extendedError.AdditionalDetails.Add(StorageHttpConstants.XmlElementNamesMod.ErrorExceptionMessage,
                                    reader.ReadElementString(StorageHttpConstants.XmlElementNamesMod.ErrorExceptionMessage));
                                extendedError.AdditionalDetails.Add(StorageHttpConstants.XmlElementNamesMod.ErrorExceptionStackTrace,
                                    reader.ReadElementString(StorageHttpConstants.XmlElementNamesMod.ErrorExceptionStackTrace));
                                reader.ReadEndElement();
                            }
                            else
                            {
                                string elementName = reader.LocalName;
                                extendedError.AdditionalDetails.Add(elementName, reader.ReadString());
                            }
                        }
                    }
                    while (reader.Read());
                }
            }
            catch (XmlException)
            {
                //If there is a parsing error we cannot return extended error information
                return null;
            }
            return extendedError;
        }

        internal static StorageExtendedErrorInformationMod GetExtendedErrorFromXmlMessage(string xmlErrorMessage)
        {
            string message = null;
            string errorCode = null;

            XName xnErrorCode = XName.Get(StorageHttpConstants.XmlElementNamesMod.TableErrorCodeElement,
                StorageHttpConstants.XmlElementNamesMod.DataWebMetadataNamespace);
            XName xnMessage = XName.Get(StorageHttpConstants.XmlElementNamesMod.TableErrorMessageElement,
                StorageHttpConstants.XmlElementNamesMod.DataWebMetadataNamespace);

            using (StringReader reader = new StringReader(xmlErrorMessage))
            {
                XDocument xDocument = null;
                try
                {
                    xDocument = XDocument.Load(reader);
                }
                catch (XmlException)
                {
                    // The XML could not be parsed. This could happen either because the connection 
                    // could not be made to the server, or if the response did not contain the
                    // error details (for example, if the response status code was neither a failure
                    // nor a success, but a 3XX code such as NotModified.
                    return null;
                }

                XElement errorCodeElement =
                    xDocument.Descendants(xnErrorCode).FirstOrDefault();

                if (errorCodeElement == null)
                    return null;

                errorCode = errorCodeElement.Value;

                XElement messageElement =
                    xDocument.Descendants(xnMessage).FirstOrDefault();

                if (messageElement != null)
                {
                    message = messageElement.Value;
                }

            }

            StorageExtendedErrorInformationMod errorDetails = new StorageExtendedErrorInformationMod();
            errorDetails.ErrorMessage = message;
            errorDetails.ErrorCode = errorCode;
            return errorDetails;
        }
    }


    #endregion
}
