﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using Microsoft.ServiceHosting.ServiceRuntime;
using Neudesic.Azure;

namespace MyWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        public override void Start()
        {
            // This is a sample worker implementation. Replace with your logic.
            RoleManager.WriteToLog("Information", "Worker Process entry point called");
            //AzureMonitor.Start("Test", "MyWorkerRole");

            for (int i = 0; i < 10000; i++)
            {
                
                Thread.Sleep(2000);
            }
        }

        public override RoleStatus GetHealthStatus()
        {
            // This is a sample worker implementation. Replace with your logic.
            return RoleStatus.Healthy;
        }
    }
}
