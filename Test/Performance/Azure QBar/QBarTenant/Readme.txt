Precondition to compile the program.

1) install Windows Azure SDK
	http://www.microsoft.com/downloads/details.aspx?FamilyID=aa40f3e2-afc5-484d-b4e9-6a5227e73590&displaylang=en

2) install Windows Azure Visual Studio tools
	http://www.microsoft.com/azure/windowsazurefordevelopers/Default.aspx?path=DownloadToolsBtn

3) With Visual Studio closed, create a new environment variable called: _CSPACK_FORCE_NOENCRYPT_ set the value to true. You can access the environment variables dialog from the advanced tab on the System Properties from the Advanced system settings task button on the left of: Control Panel\System and Maintenance\System.

4) compile with 'publish'

