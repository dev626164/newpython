﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using Microsoft.Cis.E2E.QualityBar;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Cis.E2E.QBar
{
    public class DurationEntity
    {
        public string Name;
        public string Subkey;
        public DateTime Timestamp;
        public double Total;
        public double Sum;

        public DurationEntity()
        {
            Name = Subkey = string.Empty;
            Timestamp = DateTime.MinValue;
            Total = Sum = 0.0;
        }
    }

    public class Watcher
    {
        private static string E2EBlobEndpoint, E2EQueueEndpoint, E2ETableEndpoint;
        private static string E2EAccount, E2EAccountKey;
        private static string QBarRegressionContainer;
        private static string QBarSummaryContainer, QBarCompleteSummaryPrefix, QBarIncompleteSummaryPrefix;

        public static void Init()
        {
            E2EBlobEndpoint = RoleManager.GetConfigurationSetting("BlobEndpoint");
            E2EQueueEndpoint = RoleManager.GetConfigurationSetting("QueueEndpoint");
            E2ETableEndpoint = RoleManager.GetConfigurationSetting("TableEndpoint");
            E2EAccount = RoleManager.GetConfigurationSetting("E2EAccount");
            E2EAccountKey = RoleManager.GetConfigurationSetting("E2EAccountKey");
            CloudWork.SetAccount(E2EBlobEndpoint, E2EQueueEndpoint, E2ETableEndpoint, E2EAccount, E2EAccountKey);

            QBarRegressionContainer = RoleManager.GetConfigurationSetting("QualityBarReportRegressionContainer");            
            QBarSummaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            QBarCompleteSummaryPrefix = RoleManager.GetConfigurationSetting("QualityBarReportCompleteRunsPrefix");
            QBarIncompleteSummaryPrefix = RoleManager.GetConfigurationSetting("QualityBarReportIncompleteRunsPrefix");

            return;
        }

        //regression/finished/runid
        // <timestamp>...</timestamp><count>...</count><totalduration>...</totalduration>
        public static void SummarizeExistingRuns()
        {
            LinkedList<string> completeRuns, incompleteRuns;
            string runId;
            int len;

            completeRuns = CloudWork.GetBlobNames(QBarSummaryContainer, QBarCompleteSummaryPrefix);
            len = QBarCompleteSummaryPrefix.Length;
            foreach (string runIdBlobName in completeRuns)
            {
                //format "RunPrefix/[RunId]"
                runId = runIdBlobName.Substring(len + 1, runIdBlobName.Length - len - 1);
                if (CloudWork.DoesBlobExist(QBarRegressionContainer, "regression/finished/" + runId)) continue;

                SummarizeOneRun(runId);
                System.Threading.Thread.Sleep(500);
            }

            incompleteRuns = CloudWork.GetBlobNames(QBarSummaryContainer, QBarIncompleteSummaryPrefix);
            len = QBarIncompleteSummaryPrefix.Length;
            foreach (string runIdBlobName in incompleteRuns)
            {
                //format "RunPrefix/[RunId]"
                runId = runIdBlobName.Substring(len + 1, runIdBlobName.Length - len - 1);
                if (CloudWork.DoesBlobExist(QBarRegressionContainer, "regression/finished/" + runId)) continue;

                SummarizeOneRun(runId);
                System.Threading.Thread.Sleep(500);
            }

            return;
        }

        //regression/finished/runid
        //regression/scenario/[scenario name][subkey]/runid
        //regression/api/[api name]/runid
        // <timestamp>...</timestamp><count>...</count><totalduration>...</totalduration>
        static void SummarizeOneRun(string runId)
        {
            QBarReport report;
            SortedDictionary<string, ScenarioSummary> durationInfo;
            string blob, summary;

            report = new QBarReport(runId);
            report.ReadCloudResult();

            //scenarios, api
            durationInfo = new SortedDictionary<string,ScenarioSummary>();
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in report.ScenarioList)
            {
                if (durationInfo.ContainsKey(kvp.Key) == false)
                    durationInfo[kvp.Key] = new ScenarioSummary();
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp1 in kvp.Value)
                {
                    durationInfo[kvp.Key].DurationSum += kvp1.Value.DurationSum;
                    durationInfo[kvp.Key].Successed += kvp1.Value.Successed;
                    durationInfo[kvp.Key].Name = kvp1.Value.Name;
                    durationInfo[kvp.Key].ScenarioSubKey = kvp1.Value.ScenarioSubKey;
                }
            }
            foreach (KeyValuePair<string, ScenarioSummary> kvp in durationInfo)
            {
                if (kvp.Value.Successed <= 0) continue;

                blob = string.Format("regression/scenario/{0}/{1}", kvp.Key, runId);
                summary = ProtocolHelper.BuildMultipleFields(
                    new string[] { "name", "subkey", "timestamp", "total", "durationsum" },
                    new object[] { kvp.Value.Name, kvp.Value.ScenarioSubKey, report.StartTime, kvp.Value.Successed, kvp.Value.DurationSum });
                CloudWork.PutStringBlob(QBarRegressionContainer, blob, summary, true);
            }

            //api
            foreach (KeyValuePair<string, ApiSummary> kvp in report.ApiList)
            {
                if (kvp.Value.TestCount <= 0) continue;

                blob = string.Format("regression/api/{0}/{1}", kvp.Key.Replace(" - ", "").Replace("IServiceManagement:", ""), runId);
                summary = ProtocolHelper.BuildMultipleFields(
                    new string[] { "name", "timestamp", "total", "durationsum" },
                    new object[] { kvp.Value.Name, report.StartTime, kvp.Value.TestCount, kvp.Value.DurationSum });
                CloudWork.PutStringBlob(QBarRegressionContainer, blob, summary, true);
            }

            //finished.
            CloudWork.PutStringBlob(QBarRegressionContainer, "regression/finished/" + runId, string.Empty, true);

            return;
        }

        public static void SummarizeAll()
        {
            SummarizeScenarios();
            SummarizeAPIs();

            return;
        }

        static void SummarizeScenarios()
        {
            SortedDictionary<string, LinkedList<DurationEntity>> series;
            LinkedList<string> blobNames;
            string t, content;
            string name, subkey, key;
            double total, durationSum;
            DateTime time;

            series = new SortedDictionary<string, LinkedList<DurationEntity>>();
            blobNames = CloudWork.GetBlobNames(QBarRegressionContainer, "regression/scenario/");
            foreach (string blob in blobNames)
            {
                content = GetFromLocalCopy(blob);
                if (string.IsNullOrEmpty(content)) continue;

                name = ProtocolHelper.GetOneField(content, "name");
                subkey = ProtocolHelper.GetOneField(content, "subkey");
                time = DateTime.Parse(ProtocolHelper.GetOneField(content, "timestamp"));
                total = double.Parse(ProtocolHelper.GetOneField(content, "total"));
                durationSum = double.Parse(ProtocolHelper.GetOneField(content, "durationsum"));

                key = name + "+" + subkey;
                if (total <= 0.0) total = -1.0;
                if (series.ContainsKey(key) == false)
                    series[key] = new LinkedList<DurationEntity>();
                series[key].AddFirst(new DurationEntity(){ Name = name, Subkey = subkey, Timestamp = time, Total = total, Sum = durationSum });
            }

            foreach (KeyValuePair<string, LinkedList<DurationEntity>> kvp in series)
            {
                t = string.Format("regression/summary/scenario/{0}", kvp.Key);

                content = string.Empty;
                name = subkey = string.Empty;
                foreach (DurationEntity de in kvp.Value)
                {
                    name = de.Name;
                    subkey = de.Subkey;
                    content += ProtocolHelper.BuildHeadFields("entity",
                        new string[] { "timestamp", "total", "durationsum" },
                        new object[] { de.Timestamp, de.Total, de.Sum });
                }
                content += ProtocolHelper.BuildMultipleFields(
                    new string[] { "name", "subkey" },
                    new object[] { name, subkey });

                CloudWork.PutStringBlob(QBarRegressionContainer, t, content, true);
            }

            return;
        }

        static void SummarizeAPIs()
        {
            SortedDictionary<string, LinkedList<DurationEntity>> series;
            LinkedList<string> blobNames;
            string t, content;
            string name;
            double total, durationSum;
            DateTime time;

            series = new SortedDictionary<string, LinkedList<DurationEntity>>();
            blobNames = CloudWork.GetBlobNames(QBarRegressionContainer, "regression/api/");
            foreach (string blob in blobNames)
            {
                content = GetFromLocalCopy(blob);

                name = ProtocolHelper.GetOneField(content, "name");
                time = DateTime.Parse(ProtocolHelper.GetOneField(content, "timestamp"));
                total = double.Parse(ProtocolHelper.GetOneField(content, "total"));
                durationSum = double.Parse(ProtocolHelper.GetOneField(content, "durationsum"));

                if (total <= 0.0) total = -1.0;
                if (series.ContainsKey(name) == false)
                    series[name] = new LinkedList<DurationEntity>();
                series[name].AddFirst(new DurationEntity() { Name = name, Timestamp = time, Total = total, Sum = durationSum });
            }

            foreach (KeyValuePair<string, LinkedList<DurationEntity>> kvp in series)
            {
                t = string.Format("regression/summary/api/{0}", kvp.Key.Replace(" - ", "").Replace("IServiceManagement:", ""));

                content = string.Empty;
                name = string.Empty;
                foreach (DurationEntity de in kvp.Value)
                {
                    name = de.Name;
                    content += ProtocolHelper.BuildHeadFields("entity",
                        new string[] { "timestamp", "total", "durationsum" },
                        new object[] { de.Timestamp, de.Total, de.Sum });
                }
                content += ProtocolHelper.BuildMultipleFields(
                    new string[] { "name" },
                    new object[] { name });

                CloudWork.PutStringBlob(QBarRegressionContainer, t, content, true);
            }

            return;
        }

        static string GetFromLocalCopy(string blob)
        {
            string filename, content, regressionDir;

            regressionDir = ".\\regression";
            if (Directory.Exists(regressionDir) == false) Directory.CreateDirectory(regressionDir);

            content = string.Empty;
            filename = regressionDir + "\\" + blob.Replace('/', '+');
            if (File.Exists(filename))
            {
                content = File.ReadAllText(filename);
                if (string.IsNullOrEmpty(content))
                {
                    content = CloudWork.GetStringBlob(QBarRegressionContainer, blob);
                    File.WriteAllText(filename, content);
                }
            }
            else
            {
                content = CloudWork.GetStringBlob(QBarRegressionContainer, blob);
                try
                {
                    File.WriteAllText(filename, content);
                }
                catch
                {
                }
            }

            return content;
        }
    }

    public enum WorkItemType
    {
        Empty,
        Precondition,
        Concurrent,
        Average,
        Peak,
        Postcondition
    }

    public class ApiSummary
    {
        public string Name;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public long TestCount;
        public List<double> DurationList;

        public ApiSummary()
        {
            Name = string.Empty;
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            TestCount = 0;
            DurationList = new List<double>();
        }
    }

    public class ScenarioSummary
    {
        public string Name;
        public long NoResouceCount;
        public long Successed;
        public long Failed;
        public WorkItemType ItemType;
        public string ScenarioSubKey;
        public LinkedList<string> TestInfoIds;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public List<double> DurationList;

        public ScenarioSummary()
        {
            Name = string.Empty;
            NoResouceCount = 0;
            Successed = 0;
            Failed = 0;
            ItemType = WorkItemType.Empty;
            ScenarioSubKey = string.Empty;
            TestInfoIds = new LinkedList<string>();
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            DurationList = new List<double>();
        }
    }

    public class QBarReport
    {
        public string RunId;
        public DateTime StartTime;
        public DateTime EndTime;
        public Dictionary<string, ApiSummary> ApiList;
        public Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>> ScenarioList;
        public Dictionary<string, LinkedList<string>> ExceptionList;

        public QBarReport(string runid)
        {
            RunId = runid;
            StartTime = DateTime.MaxValue;
            EndTime = DateTime.MinValue;
            ApiList = new Dictionary<string, ApiSummary>();
            ScenarioList = new Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>>();
            ExceptionList = new Dictionary<string, LinkedList<string>>();
        }

        public void AddScenarioEntry(string scenario, WorkItemType type, string subKey, bool status, double duration, DateTime startedAt, bool isNoResource, string testInfoId)
        {
            Dictionary<WorkItemType, ScenarioSummary> wiss;
            ScenarioSummary ss;
            string key;

            if (StartTime > startedAt) StartTime = startedAt;
            if (EndTime < startedAt.AddMinutes(duration)) EndTime = startedAt;

            key = scenario.ToString() + subKey;
            if (ScenarioList.ContainsKey(key))
            {
                wiss = ScenarioList[key];
                if (wiss.ContainsKey(type))
                {
                    ss = wiss[type];
                }
                else
                {
                    ss = new ScenarioSummary();
                    wiss[type] = ss;
                }
            }
            else
            {
                wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                ScenarioList[key] = wiss;
                ss = new ScenarioSummary();
                wiss[type] = ss;
            }

            ss.Name = scenario;
            ss.ItemType = type;
            ss.ScenarioSubKey = subKey;
            ss.TestInfoIds.AddFirst(testInfoId);
            if (status == true)
            {
                ss.DurationSum += duration;
                ss.Successed++;
                if (ss.DurationMax < duration) ss.DurationMax = duration;
                if (ss.DurationMin > duration) ss.DurationMin = duration;

                ss.DurationList.Add(duration);
            }
            else if (isNoResource)
            {
                ss.NoResouceCount++;
            }
            else
            {
                ss.Failed++;
            }

            return;
        }

        public void AddApiEntry(string name, double duration)
        {
            ApiSummary apisum;

            if (ApiList.ContainsKey(name))
            {
                apisum = ApiList[name];
                apisum.DurationSum += duration;
                apisum.TestCount++;
                apisum.DurationList.Add(duration);
            }
            else
            {
                apisum = new ApiSummary()
                {
                    Name = name,
                    DurationSum = duration,
                    TestCount = 1
                };
                apisum.DurationList.Add(duration);
                ApiList[name] = apisum;
            }

            if (apisum.DurationMax < duration) apisum.DurationMax = duration;
            if (apisum.DurationMin > duration) apisum.DurationMin = duration;

            return;
        }

        public void AddExceptionEntry(string exceptionInfo, string testInfoId)
        {
            if (ExceptionList.ContainsKey(exceptionInfo) == false)
            {
                ExceptionList.Add(exceptionInfo, new LinkedList<string>());
            }

            ExceptionList[exceptionInfo].AddFirst(testInfoId);

            return;
        }

        public bool IsComplete()
        {
            //for compute, createsubscription == deletesubscription
            //for storage, CreateStorageAccount == DeleteStorageAccount
            long created, deleted;

            created = deleted = 0;
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key == WorkItemType.Precondition)
                    {
                        if (kvp2.Value.Name == xStoreScenarios.CreateStorageAccount.ToString()
                            || kvp2.Value.Name == RDFEScenarios.CreateSubscription.ToString())
                        {
                            created += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
                        }
                    }
                    else if (kvp2.Key == WorkItemType.Postcondition)
                    {
                        if (kvp2.Value.Name == xStoreScenarios.DeleteStorageAccount.ToString()
                            || kvp2.Value.Name == RDFEScenarios.DeleteSubscription.ToString())
                        {
                            deleted += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
                        }
                    }
                }
            }

            return (created != 0 && created == deleted);
        }

        public void Summarize()
        {
            int i;

            //find the median value for Scenarios
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Value.DurationList.Count <= 0) continue;

                    kvp2.Value.DurationList.Sort();
                    i = kvp2.Value.DurationList.Count / 2;
                    if (kvp2.Value.DurationList.Count % 2 == 0)
                    {
                        kvp2.Value.DurationMedian = (kvp2.Value.DurationList[i - 1] + kvp2.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        kvp2.Value.DurationMedian = kvp2.Value.DurationList[i];
                    }
                }
            }

            //find the median value for APIs
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                if (apisum.Value.DurationList.Count > 0)
                {
                    apisum.Value.DurationList.Sort();
                    i = apisum.Value.DurationList.Count / 2;
                    if (apisum.Value.DurationList.Count % 2 == 0)
                    {
                        apisum.Value.DurationMedian = (apisum.Value.DurationList[i - 1] + apisum.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        apisum.Value.DurationMedian = apisum.Value.DurationList[i];
                    }
                }
            }

            return;
        }

        public void CloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            CloudOverall();
            CloudOnePhase(WorkItemType.Precondition);
            CloudOnePhase(WorkItemType.Average);
            CloudOnePhase(WorkItemType.Peak);
            CloudOnePhase(WorkItemType.Postcondition);
            CloudAPIs();
            CloudExceptions();

            return;
        }

        private void CloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/Overall", RunId);

            content = ProtocolHelper.BuildHeadFields("overall",
                new string[] { "starttime", "endtime", "runid" },
                new object[] { StartTime.ToString(), EndTime.ToString(), RunId });
            CloudWork.PutStringBlob(summaryContainer, blobName, content, true);

            return;
        }

        private void CloudOnePhase(WorkItemType type)
        {
            //startTime, endTime, RunId
            StringBuilder sb;
            string summaryContainer, blobName, testIds;
            double median, max, min;

            sb = new StringBuilder();
            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/{1}", RunId, type.ToString());

            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key != type) continue;

                    median = (kvp2.Value.DurationMedian == double.MinValue) ? -1.0 : kvp2.Value.DurationMedian;
                    max = (kvp2.Value.DurationMax == double.MinValue) ? -1.0 : kvp2.Value.DurationMax;
                    min = (kvp2.Value.DurationMin == double.MaxValue) ? -1.0 : kvp2.Value.DurationMin;

                    testIds = string.Empty;
                    foreach (string id in kvp2.Value.TestInfoIds)
                    {
                        testIds += ProtocolHelper.GetOneField("id", id);
                    }

                    sb.Append(ProtocolHelper.BuildHeadFields("scenarioduration",
                        new string[] { "scenario", "subkey", "failed", "noresource", "succeed", "average", "median", "min", "max", "testids" },
                        new object[]{ kvp2.Value.Name, kvp2.Value.ScenarioSubKey, kvp2.Value.Failed, kvp2.Value.NoResouceCount, kvp2.Value.Successed,
                            kvp2.Value.Successed == 0 ? -1.0 : kvp2.Value.DurationSum / kvp2.Value.Successed, median, min, max, testIds}));
                }
            }

            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField(type.ToString(), sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        private void CloudAPIs()
        {
            StringBuilder sb;
            string summaryContainer, blobName;
            double median, max, min;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");

            sb = new StringBuilder();
            //API, Total, Average(s), Median(s), Min(s), Max(s)
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                median = (apisum.Value.DurationMedian == double.MinValue) ? -1.0 : apisum.Value.DurationMedian;
                max = (apisum.Value.DurationMax == double.MinValue) ? -1.0 : apisum.Value.DurationMax;
                min = (apisum.Value.DurationMin == double.MaxValue) ? -1.0 : apisum.Value.DurationMin;
                sb.Append(ProtocolHelper.BuildHeadFields("apiduration",
                    new string[] { "api", "count", "average", "median", "min", "max" },
                    new object[] { apisum.Key, apisum.Value.TestCount, apisum.Value.DurationSum / apisum.Value.TestCount, apisum.Value.DurationMedian, apisum.Value.DurationMin, apisum.Value.DurationMax }));
            }

            blobName = string.Format("{0}/APIs", RunId);
            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("apis", sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        private void CloudExceptions()
        {
            StringBuilder sb;
            string summaryContainer, blobName, testIds;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");

            sb = new StringBuilder();
            foreach (KeyValuePair<string, LinkedList<string>> kvp in ExceptionList)
            {
                testIds = string.Empty;
                foreach (string s in kvp.Value)
                {
                    testIds += ProtocolHelper.GetOneField("id", s);
                }

                sb.Append(ProtocolHelper.BuildHeadFields("exception",
                    new string[] { "info", "testids" },
                    new object[] { kvp.Key, testIds }));
            }

            blobName = string.Format("{0}/Exceptions", RunId);
            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("exceptions", sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        public void ReadCloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            ReadCloudOverall();
            ReadCloudOnePhase(WorkItemType.Precondition);
            ReadCloudOnePhase(WorkItemType.Average);
            ReadCloudOnePhase(WorkItemType.Peak);
            ReadCloudOnePhase(WorkItemType.Postcondition);
            ReadCloudAPIs();
            ReadCloudExceptions();

            return;
        }

        private void ReadCloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/Overall", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                StartTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "starttime"));
                EndTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "endtime"));
            }

            return;
        }

        private void ReadCloudOnePhase(WorkItemType type)
        {
            string summaryContainer, blobName, content, one, testIdsReport;
            LinkedList<string> testIds;
            ScenarioSummary sum;
            int succeed;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/{1}", RunId, type.ToString());
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "scenarioduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "scenarioduration", i);

                    testIdsReport = ProtocolHelper.GetOneField(one, "testids");
                    testIds = new LinkedList<string>();
                    for (int k = 0; k < ProtocolHelper.GetFieldCount(testIdsReport, "id"); k++)
                    {
                        testIds.AddFirst(ProtocolHelper.GetTimedField(testIdsReport, "id", k));
                    }

                    succeed = int.Parse(ProtocolHelper.GetOneField(one, "succeed"));
                    sum = new ScenarioSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "scenario"),
                        ScenarioSubKey = ProtocolHelper.GetOneField(one, "subkey"),
                        Failed = int.Parse(ProtocolHelper.GetOneField(one, "failed")),
                        NoResouceCount = int.Parse(ProtocolHelper.GetOneField(one, "noresource")),
                        Successed = succeed,
                        ItemType = type,
                        TestInfoIds = testIds,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * succeed,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };

                    string key;
                    Dictionary<WorkItemType, ScenarioSummary> wiss;

                    key = sum.Name + sum.ScenarioSubKey;
                    if (ScenarioList.ContainsKey(key))
                    {
                        wiss = ScenarioList[key];
                    }
                    else
                    {
                        wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                        ScenarioList[key] = wiss;
                    }

                    wiss[type] = sum;
                }
            }

            return;
        }

        private void ReadCloudAPIs()
        {
            string summaryContainer, blobName, content, one;
            ApiSummary sum;
            int count;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/APIs", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "apiduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "apiduration", i);

                    count = int.Parse(ProtocolHelper.GetOneField(one, "count"));
                    sum = new ApiSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "api"),
                        TestCount = count,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * count,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };
                    ApiList.Add(sum.Name, sum);
                }
            }

            return;
        }

        private void ReadCloudExceptions()
        {
            string summaryContainer, blobName, content, one, testIdsReport;
            LinkedList<string> testIds;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/Exceptions", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "exception"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "exception", i);

                    testIdsReport = ProtocolHelper.GetOneField(one, "testids");
                    testIds = new LinkedList<string>();
                    for (int k = 0; k < ProtocolHelper.GetFieldCount(testIdsReport, "id"); k++)
                    {
                        testIds.AddFirst(ProtocolHelper.GetTimedField(testIdsReport, "id", k));
                    }
                    ExceptionList.Add(ProtocolHelper.GetOneField(one, "info"), testIds);
                }
            }

            return;
        }
    }
}
