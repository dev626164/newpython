﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Cis.E2E.QBar
{
    public class ProtocolHelper
    {
        public static bool DoesFieldExist(string desc, string field)
        {
            bool exist;
            string pattern;
            
            exist = false;
            if (field != string.Empty)
            {
                pattern = string.Format("<{0}>(?'value'[^\0]*?)</{0}>", field);
                exist = Regex.IsMatch(desc, pattern);
            }

            return exist;
        }

        public static string GetOneField(string desc, string fieldName)
        {
            string arg;
            string pattern;
            Match m;

            arg = string.Empty;
            if (fieldName != string.Empty)
            {
                pattern = string.Format("<{0}>(?'value'[^\0]*?)</{0}>", fieldName);
                m = Regex.Match(desc, pattern);
                if (m.Success)
                {
                    arg = m.Groups["value"].Value;
                }
            }

            return arg;
        }

        public static string BuildOneField(string fieldName, object value)
        {
            return string.Format("<{0}>{1}</{0}>", fieldName, value);
        }

        public static string BuildMultipleFields(string[] fieldNames, object[] values)
        {
            string fields;

            fields = string.Empty;
            for (int i = 0; i < fieldNames.Length; i++)
            {
                fields += BuildOneField(fieldNames[i], values[i]);
            }

            return fields;
        }

        public static string BuildHeadFields(string head, string[] fieldNames, object[] values)
        {
            string fields;

            fields = string.Empty;
            for(int i=0; i<fieldNames.Length; i++)
            {
                fields += BuildOneField(fieldNames[i], values[i]);
            }

            return BuildOneField(head, fields);
        }

        public static string GetHeadedField(string desc, string head, string childField)
        {
            string value;

            value = GetOneField(desc, head);
            return GetOneField(value, childField);
        }

        public static int GetFieldCount(string desc, string field)
        {
            string pattern;
            int count;

            count = 0;
            if (field != string.Empty)
            {
                pattern = string.Format("<{0}>(?'value'[^\0]*?)</{0}>", field);
                count = Regex.Matches(desc, pattern).Count;
            }

            return count;
        }

        public static string GetTimedField(string desc, string field, int index)
        {
            string arg;
            string pattern;
            Match m;

            arg = string.Empty;
            if (field != string.Empty)
            {
                pattern = string.Format("<{0}>(?'value'[^\0]*?)</{0}>", field);
                m = Regex.Matches(desc, pattern)[index];
                if (m.Success)
                {
                    arg = m.Groups["value"].Value;
                }
            }

            return arg;
        }
    }
    
    public class DuplexQueue
    {
        public string SendQueue;
        public string RecvQueue;

        public DuplexQueue(string sendQueue, string recvQueue)
        {
            SendQueue = sendQueue;
            RecvQueue = recvQueue;
        }

        public DuplexQueue(string sendQueue)
        {
            SendQueue = sendQueue;
            RecvQueue = Guid.NewGuid().ToString("N");
            ClearQueue(SendQueue);
        }

        public string DoTransaction(string request, int waitSeconds, int checkIntervalInMilliseconds)
        {
            request += ProtocolHelper.BuildOneField("responsequeue", RecvQueue);
            CloudWork.SendMessage(SendQueue, request);
            return CloudWork.ResponseFromQueue(RecvQueue, waitSeconds, checkIntervalInMilliseconds);
        }

        public bool SendMessageOnly(string request)
        {
            if (request.Length > 6 * 1024)
            {
                QBarLogger.Log(DebugLevel.FATAL, "request length is too large, {0}", request.Length);
            }
            return CloudWork.SendMessage(SendQueue, request);
        }


        public string RecvMessageOnly(int waitSeconds, int checkIntervalInMilliseconds)
        {
            return CloudWork.ResponseFromQueue(RecvQueue, waitSeconds, checkIntervalInMilliseconds);
        }

        private void ClearQueue(string queueName)
        {
            Message msg;

            do
            {
                msg = CloudWork.GetAndDeleteOneMessage(queueName);
            } 
            while (msg != null);

            return;
        }
    }

    public class CloudWork
    {
        internal static string BlobEndpoint;
        internal static string QueueEndpoint;
        internal static string TableEndpoint;
        internal static string AccountName;
        internal static string AccountKey;

        static CloudWork()
        {
            BlobEndpoint =  RoleManager.GetConfigurationSetting("BlobEndpoint");
            QueueEndpoint = RoleManager.GetConfigurationSetting("QueueEndpoint");
            TableEndpoint = RoleManager.GetConfigurationSetting("TableEndpoint");
            AccountName = RoleManager.GetConfigurationSetting("E2EAccount");
            AccountKey = RoleManager.GetConfigurationSetting("E2EAccountKey");

        }

        public static bool PutBlob(string endpoint, string account, string key, string container, string blob, MemoryStream blobContent)
        {
            BlobStorage blobStorage;
            BlobContainer blobContainer;
            BlobContents content;
            BlobProperties blobProperties;

            QBarLogger.Log(DebugLevel.INFO, "PutBlob: {0}, {1}, {2}, {3}, {4}", endpoint, account, container, blob, blobContent.Length);
            try
            {
                blobStorage = BlobStorage.Create(new Uri(endpoint), false, account, key);
                blobContainer = blobStorage.GetBlobContainer(container);
                if (!blobContainer.DoesContainerExist())
                {
                    blobContainer.CreateContainer();
                }

                content = new BlobContents(blobContent);
                blobProperties = new BlobProperties(blob);
                blobContainer.CreateBlob(blobProperties, content, true);
            }
            catch (Exception ex)
            {
                QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());

                return false;
            }

            return true;
        }

        public static bool DoesBlobExist(string endpoint, string account, string key, string container, string blob)
        {
            BlobStorage blobStorage;
            BlobContainer blobContainer;
            bool exist;

            exist = false;
            try
            {
                blobStorage = BlobStorage.Create(new Uri(endpoint), false, account, key);
                blobContainer = blobStorage.GetBlobContainer(container);
                exist = blobContainer.DoesBlobExist(blob);
            }
            catch (Exception ex)
            {
                QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
            }

            return exist;
        }

        public static bool GetFileBlob(string containerName, string blobName, string fileName)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.GetFileBlob(blobName, fileName);
        }

        public static string GetStringBlob(string containerName, string blobName)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.GetStringBlob(blobName);
        }

        public static bool PutStringBlob(string containerName, string blobName, string strContent, bool overwriteOld)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.PutStringBlob(blobName, strContent, overwriteOld);
        }

        public static bool PutFileBlob(string containerName, string blobName, string fileName, bool overwriteOld)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.PutFileBlob(blobName, fileName, overwriteOld);
        }

        public static bool DeleteBlob(string containerName, string blobName)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.DeleteBlob(blobName);
        }

        public static LinkedList<string> GetBlobNames(string containerName, string prefix)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.GetBlobNames(prefix);
        }

        public static DateTime GetBlobTime(string containerName, string blobName)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.GetBlobTime(blobName);
        }

        public bool DoesBlobExist(string containerName, string blobName)
        {
            BlobWorker myWorker;

            myWorker = new BlobWorker(containerName);
            return myWorker.DoesBlobExist(blobName);
        }

        public static bool SendMessage(string queueName, string msgStr)
        {
            QueueWorker myWorker;

            myWorker = new QueueWorker(queueName);
            return myWorker.SendMessage(msgStr);
        }

        public static void DeleteQueue(string queueName)
        {
            QueueWorker myWorker;

            myWorker = new QueueWorker(queueName);
            myWorker.DeleteQueue(queueName);
            return;
        }

        public static Message GetAndDeleteOneMessage(string queueName)
        {
            Message msg;
            QueueWorker myWorker;

            msg = null;
            myWorker = new QueueWorker(queueName);
            try
            {
                msg = myWorker.GetMessage();
                if (msg != null)
                {
                    myWorker.DeleteMessage(msg);
                }
            }
            catch (Exception ex)
            {
                QBarLogger.Log(DebugLevel.INFO, ex.ToString());
            }

            return msg;
        }

        public static string GetAndDeleteOneMessageStr(string queueName)
        {
            Message msg;
            string msgStr;

            msgStr = string.Empty;
            msg = GetAndDeleteOneMessage(queueName);
            if (msg != null)
            {
                msgStr = msg.ContentAsString();
            }

            return msgStr;
        }

        public static string ResponseFromQueue(string queueName, int waitSeconds, int checkIntervalInMilliseconds)
        {
            Stopwatch watch;
            QueueWorker myWorker;
            Message msg;
            string response;

            if (queueName == string.Empty)
            {
                return string.Empty;
            }

            msg = null;
            try
            {
                myWorker = new QueueWorker(queueName);
            }
            catch (Exception)
            {
                return string.Empty;
            }

            watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalSeconds < waitSeconds)
            {
                try
                {
                    msg = myWorker.GetMessage();
                    if (msg != null) break;
                }
                catch (Exception)
                {
                }
                System.Threading.Thread.Sleep(checkIntervalInMilliseconds);
            }
            watch.Stop();
            
            response = string.Empty;
            if (msg != null)
            {
                response = msg.ContentAsString(); 
                myWorker.DeleteMessage(msg);
            }

            return response;
        }


        public class BlobWorker
        {
            BlobStorage blobStorage;
            BlobContainer blobContainer;

            #region constructor
            public BlobWorker()
            {
                blobStorage = BlobStorage.Create(new Uri(BlobEndpoint), false, AccountName, AccountKey);
                blobContainer = null;
            }

            public BlobWorker(string container)
            {
                bool exist;

                blobStorage = BlobStorage.Create(new Uri(BlobEndpoint), false, AccountName, AccountKey);
                blobContainer = blobStorage.GetBlobContainer(container);

                do
                {
                    try
                    {
                        exist = blobContainer.DoesContainerExist();
                        if (exist == false)
                        {
                            blobContainer.CreateContainer();
                            exist = true;
                        }

                    }
                    catch
                    {
                        exist = false;
                    }

                    if (!exist) System.Threading.Thread.Sleep(30 * 1000);
                }
                while (!exist);
            }
            #endregion //Constructor

            #region getblob
            public bool GetFileBlob(string container, string blobName, string fileName)
            {
                blobContainer = blobStorage.GetBlobContainer(container);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return GetFileBlob(blobName, fileName);
            }

            public bool GetFileBlob(string blobName, string fileName)
            {
                BlobProperties blobProperties;
                BlobContents contents;
                bool success;

                success = false;
                if (blobContainer.DoesContainerExist() && blobContainer.DoesBlobExist(blobName))
                {
                    using (FileStream fs = File.Open(fileName, FileMode.Create))
                    {
                        contents = new BlobContents(fs);
                        blobProperties = blobContainer.GetBlob(blobName, contents, true);
                        fs.Close();
                        //QBarLogger.Log(DebugLevel.INFO, "{0}.{1}(C.B) --> {2}(F)", blobContainer.ContainerName, blobName, fs.Name);
                        success = true;
                    }
                }

                return success;
            }
            public string GetStringBlob(string containerName, string blobName)
            {
                blobContainer = blobStorage.GetBlobContainer(containerName);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return GetStringBlob(blobName);
            }
            public string GetStringBlob(string blobName)
            {
                BlobContents content;
                string blobStr;

                blobStr = string.Empty;
                try
                {
                    if (blobContainer.DoesContainerExist() && blobContainer.DoesBlobExist(blobName))
                    {
                        content = new BlobContents(new MemoryStream());
                        blobContainer.GetBlob(blobName, content, false);
                        blobStr = System.Text.Encoding.UTF8.GetString(content.AsBytes());
                    }
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                }

                return blobStr;
            }
            #endregion //getblob

            #region //putblob
            public bool PutStringBlob(string containerName, string blobName, string strContent, bool overwriteOld)
            {
                blobContainer = blobStorage.GetBlobContainer(containerName);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return PutStringBlob(blobName, strContent, overwriteOld);
            }
            public bool PutStringBlob(string blobName, string strContent, bool overwriteOld)
            {
                MemoryStream contentStream;

                contentStream = new MemoryStream(System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(strContent));
                return PutBlob(blobName, new BlobContents(contentStream), overwriteOld);
            }
            public bool PutFileBlob(string containerName, string blobName, string fileName, bool overwriteOld)
            {
                blobContainer = blobStorage.GetBlobContainer(containerName);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return PutFileBlob(blobName, fileName, overwriteOld);
            }
            public bool PutFileBlob(string blobName, string fileName, bool overwriteOld)
            {
                bool success;

                success = false;
                using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    success = PutBlob(blobName, new BlobContents(fs), overwriteOld);
                }

                return success;
            }
            private bool PutBlob(string blobName, BlobContents blobContent, bool overwriteOld)
            {
                BlobProperties blobProperties;
                bool success;

                success = false;
                //QBarLogger.Log(DebugLevel.INFO, "PutBlob: {0}, {1}, {2}, {3}, {4}", endpoint, account, container, blob, blobContent.Length);
                try
                {
                    if (!blobContainer.DoesContainerExist())
                    {
                        blobContainer.CreateContainer();
                    }

                    blobProperties = new BlobProperties(blobName);
                    success = blobContainer.CreateBlob(blobProperties, blobContent, true);
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                }

                return success;
            }
            #endregion //putblob

            #region deleteblob
            public bool DeleteBlob(string containerName, string blobName)
            {
                blobContainer = blobStorage.GetBlobContainer(containerName);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return DeleteBlob(blobName);
            }
            public bool DeleteBlob(string blobName)
            {
                bool success;

                success = false;
                try
                {
                    if (blobContainer.DoesContainerExist())
                    {
                        success = blobContainer.DeleteBlob(blobName);
                    }
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                }

                return success;
            }
            #endregion //deleteblob

            #region listblobs
            public LinkedList<string> GetBlobNames(string containerName, string prefix)
            {
                blobContainer = blobStorage.GetBlobContainer(containerName);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return GetBlobNames(prefix);
            }
            public LinkedList<string> GetBlobNames(string prefix)
            {
                LinkedList<string> blobNames;

                blobNames = new LinkedList<string>();
                try
                {
                    if (blobContainer != null && blobContainer.DoesContainerExist())
                    {
                        foreach (object ob in blobContainer.ListBlobs(prefix, false))
                        {
                            blobNames.AddLast(((BlobProperties)ob).Name);
                        }
                    }
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                }

                return blobNames;
            }
            #endregion //listblobs

            #region blobexistence
            public bool DoesBlobExist(string containerName, string blobName)
            {
                blobContainer = blobStorage.GetBlobContainer(containerName);
                if (blobContainer.DoesContainerExist() == false)
                {
                    blobContainer.CreateContainer();
                }

                return DoesBlobExist(blobName);
            }
            public bool DoesBlobExist(string blobName)
            {
                bool exist;

                exist = false;
                try
                {
                    exist = blobContainer.DoesBlobExist(blobName);
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                }

                return exist;
            }
            #endregion //blobexistence

            #region blobproperties
            public DateTime GetBlobTime(string blobName)
            {
                DateTime blobTime;
                BlobProperties prop;

                blobTime = DateTime.MinValue;
                if (blobContainer.DoesBlobExist(blobName))
                {
                    prop = blobContainer.GetBlobProperties(blobName);
                    if (prop != null) blobTime = prop.LastModifiedTime;
                }

                return blobTime;
            }
            #endregion //blobproperties
        }

        public class QueueWorker
        {
            QueueStorage queueStorage;
            MessageQueue queue;

            #region constructor
            public QueueWorker()
            {
                queueStorage = QueueStorage.Create(new Uri(QueueEndpoint), false, AccountName, AccountKey);
                queue = null;
            }
            public QueueWorker(string queueName)
            {
                bool exist;

                queueStorage = QueueStorage.Create(new Uri(QueueEndpoint), false, AccountName, AccountKey);
                queue = queueStorage.GetQueue(queueName);
                do
                {
                    try
                    {
                        exist = queue.DoesQueueExist();
                        if (exist == false)
                        {
                            queue.CreateQueue();
                            exist = true;
                        }

                    }
                    catch
                    {
                        exist = false;
                    }

                    if (!exist) System.Threading.Thread.Sleep(30 * 1000);
                }
                while (!exist);
            }
            #endregion //constructor

            #region commonqueueoperatons
            public bool SendMessage(string queueName, string msgStr)
            {
                queue = queueStorage.GetQueue(queueName);
                if (queue.DoesQueueExist() == false)
                {
                    queue.CreateQueue();
                }

                return SendMessage(msgStr);
            }
            public bool SendMessage(string msgStr)
            {
                bool success;

                success = false;
                try
                {
                    //DEBUG LZW
                    success = queue.PutMessage(new Message(msgStr),1*60*60);
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                }

                if (success == false)
                {
                    QBarLogger.Log(DebugLevel.FATAL, "Cannot send the message to cloud");
                }

                return success;
            }
            public void DeleteQueue(string queueName)
            {
                queue = queueStorage.GetQueue(queueName);
                if (queue.DoesQueueExist())
                {
                    bool success;

                    success = queue.DeleteQueue();
                    if (!success)
                    {
                        throw new Exception(string.Format("Cannot delete queue {0}", queueName));
                    }
                }

                return;
            }
            public Message GetMessage(string queueName)
            {
                queue = queueStorage.GetQueue(queueName);
                if (!queue.DoesQueueExist())
                {
                    queue.CreateQueue();
                }

                return queue.GetMessage();
            }
            public Message GetMessage()
            {
                return queue.GetMessage();
            }
            public bool DeleteMessage(string queueName, Message msg)
            {
                queue = queueStorage.GetQueue(queueName);
                if (!queue.DoesQueueExist())
                {
                    queue.CreateQueue();
                }

                return queue.DeleteMessage(msg);
            }
            public bool DeleteMessage(Message msg)
            {
                return queue.DeleteMessage(msg);
            }
            #endregion //commonqueueoperations
        }

        public class TableWorker
        {
            public TableWorker()
            {
            }
        }
    }
}
