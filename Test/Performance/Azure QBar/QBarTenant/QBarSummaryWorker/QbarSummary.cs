﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.ServiceHosting.ServiceRuntime;
using Microsoft.Samples.ServiceHosting.StorageClient;
using System.Threading;

namespace Microsoft.Cis.E2E.QBar
{
    
    public class WarningMessages
    {
        public static readonly string UnknownError = "The exception is not expected.";
        public static readonly string FullDeploymentUnavailable = "Full deployment account is not available";
        public static readonly string ProductDeploymentUnavailable = "Product deployment is not available";
        public static readonly string StagingDeploymentUnavailable = "Staging deployment is not available";
        public static readonly string DeploymentUnavailable = "Deployment is not available";
        public static readonly string HostedServiceUnavailable = "Hosted service account is not available";
        public static readonly string StorageServiceUnavailable = "Storage service account is not available";
        public static readonly string SubscriptionUnavailable = "Subscription is not available";
        public static readonly string StorageAccountUnavailable = "Storage account is not available";
        public static readonly string StorageContainerUnavailable = "Storage container is not available";
        public static readonly string StorageBlobUnavailable = "Storage blob is not available";
    }

    public enum RDFEScenarios
    {
        Empty,
        CreateSubscription,
        CreateHostedService,
        CreateStorageService,
        DeleteSubscription,
        DeleteHostedService,
        DeleteStorageService,
        PutDeployment,
        DeleteDeployment,
        UpgradeDeployment,
        UpdateDeploymentConfig
    }

    public enum xStoreScenarios
    {
        Empty,
        CreateContainer,
        CreateStorageAccount,
        DeleteBlob,
        DeleteContainer,
        DeleteStorageAccount,
        GetBlob,
        PutBlob
    }

    //public class RunInfo
    //{
    //    public DateTime StartTime;
    //    public DateTime EndTime;
    //    public string RunId;
    //}

    public delegate void Summarize (string runId, string content);

    public class Watcher
    {
        private static string E2EBlobEndpoint, E2EQueueEndpoint, E2ETableEndpoint;
        private static string E2EAccount, E2EAccountKey;
        private static string QBarRawReportContainer, QBarActiveRunsPrefix;
        private static string QBarSummaryContainer, QBarCompleteSummaryPrefix, QBarIncompleteSummaryPrefix;

        static string account = "airwatchqbar", key = "G/mvNGCSb0GvNACyz2Ulkw/6PlDZfOW45EdKNqHBTdjt+1fCHonNwcI0vCxm9EAXWRJNQeOpNZAn+ifRuHgu9Q==";        
        static string queueEndpoint = "http://queue.core.windows.net";        
        static Uri baseUri = new Uri(queueEndpoint);
        static bool? usePathStyleUris = false;
        static StorageAccountInfo accountInfo = new StorageAccountInfo(baseUri, usePathStyleUris, account, key);
        static QueueStorage q = QueueStorage.Create(accountInfo);
        static MessageQueue mq = q.GetQueue("reportsqueue");
        static string runId = Guid.NewGuid().ToString();


        public static Queue<Message> deleteMessageQueue = new Queue<Message>();
        public static Queue<string> reportQueue = new Queue<string>();

        public static void Init()
        {
            E2EBlobEndpoint = RoleManager.GetConfigurationSetting("BlobEndpoint");
            E2EQueueEndpoint = RoleManager.GetConfigurationSetting("QueueEndpoint");
            E2ETableEndpoint = RoleManager.GetConfigurationSetting("TableEndpoint");
            E2EAccount = RoleManager.GetConfigurationSetting("E2EAccount");
            E2EAccountKey = RoleManager.GetConfigurationSetting("E2EAccountKey");

            QBarRawReportContainer = RoleManager.GetConfigurationSetting("QualityBarReportContainer");
            QBarActiveRunsPrefix = RoleManager.GetConfigurationSetting("QualityBarReportActiveRunsPrefix");

            QBarSummaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            QBarCompleteSummaryPrefix = RoleManager.GetConfigurationSetting("QualityBarReportCompleteRunsPrefix");
            QBarIncompleteSummaryPrefix = RoleManager.GetConfigurationSetting("QualityBarReportIncompleteRunsPrefix");

            return;            
        }

        public static void WatchSummary()
        {
            DateTime curr;
            //TimeSpan span;
            //QBarSummary summary;
            //string runId;

            CloudWork.BlobEndpoint = E2EBlobEndpoint;
            CloudWork.QueueEndpoint = E2EQueueEndpoint;
            CloudWork.TableEndpoint = E2ETableEndpoint;
            CloudWork.AccountName = E2EAccount;
            CloudWork.AccountKey = E2EAccountKey;

            curr = DateTime.Now.ToUniversalTime();
            //foreach (string oneRun in GetActiveRuns())
            //{
                //span = curr - CloudWork.GetBlobTime(QBarRawReportContainer, QBarActiveRunsPrefix + "/" + oneRun);
                //SummarizeQBarReports(Guid.NewGuid().ToString());//, span.TotalHours > 12);
            //}


            Thread summarizeReportsThread = new Thread(new ThreadStart(SummarizeQBarReports));
            Thread getReportsFromQueueThread = new Thread(new ThreadStart(GetReportFromQueue));

            //for (int i = 0; i < 10; i++)
            //{
            //    ThreadPool.QueueUserWorkItem(new WaitCallback(GetReportFromQueue));
            //    ThreadPool.QueueUserWorkItem(new WaitCallback(SummarizeQBarReports));
            //}

            getReportsFromQueueThread.Start();
            Thread.Sleep(5 * 1000);
            summarizeReportsThread.Start();

            Thread.CurrentThread.Join();

            return;
        }        

        public static LinkedList<string> GetActiveRuns()
        {
            LinkedList<string> allRuns, completeRuns, incompleteRuns, activeRuns;
            int len;
            string runId;
            
            allRuns = CloudWork.GetBlobNames(QBarRawReportContainer, QBarActiveRunsPrefix);
            completeRuns = CloudWork.GetBlobNames(QBarSummaryContainer, QBarCompleteSummaryPrefix);
            incompleteRuns = CloudWork.GetBlobNames(QBarSummaryContainer, QBarIncompleteSummaryPrefix);

            activeRuns = new LinkedList<string>();
            len = QBarActiveRunsPrefix.Length;
            foreach (string run in allRuns)
            {
                //format "RunPrefix/[RunId]"
                runId = run.Substring(len + 1, run.Length - len - 1);

                if (completeRuns.Contains(QBarCompleteSummaryPrefix + "/" + runId) == false
                    && incompleteRuns.Contains(QBarIncompleteSummaryPrefix + "/" + runId) == false)
                {
                    activeRuns.AddFirst(runId);
                }
            }

            return activeRuns;
        }

        //public static SortedDictionary<DateTime, RunInfo> GetExistingRuns()
        //{
        //    LinkedList<string> activeRunIds;
        //    string runId;
        //    DateTime startTime, endTime, t;
        //    SortedDictionary<DateTime, RunInfo> AllRuns;
        //    int len;

        //    AllRuns = new SortedDictionary<DateTime, RunInfo>();

        //    activeRunIds = CloudWork.GetBlobNames(QBarRawReportContainer, QBarActiveRunsPrefix);
        //    len = QBarActiveRunsPrefix.Length;
        //    foreach (string runIdBlobName in activeRunIds)
        //    {
        //        //format "RunPrefix/[RunId]"
        //        runId = runIdBlobName.Substring(len + 1, runIdBlobName.Length - len - 1);
                
        //        startTime = DateTime.MaxValue;
        //        endTime = DateTime.MinValue;
        //        foreach (string blob in CloudWork.GetBlobNames(QBarRawReportContainer, runId))
        //        {
        //            t = CloudWork.GetBlobTime(QBarRawReportContainer, blob);

        //            if (startTime > t) startTime = t;
        //            if (endTime < t) endTime = t;
        //        }

        //        if (startTime != DateTime.MaxValue)
        //        {
        //            AllRuns.Add(startTime, 
        //                new RunInfo() 
        //                { 
        //                    StartTime = startTime, 
        //                    EndTime = endTime, 
        //                    RunId = runId
        //                }
        //                );
        //        }
        //    }

        //    return AllRuns;
        //}

        //static bool IsSummarized(string runId)
        //{
        //    string summaryContainer;
        //    bool exist;

        //    exist = false;
        //    summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
        //    if (CloudWork.GetBlobNames(summaryContainer, runId).Count != 0)
        //    {
        //        exist = true;
        //    }

        //    return exist;
        //}

        public static void SummarizeQBarReports()
        {
            //LinkedList<string> reports;
            //string content;
            QBarSummary summary;
            TimeSpan span;
            

            //reports = CloudWork.GetBlobNames(QBarRawReportContainer, runId);
            //QBarLogger.Log(DebugLevel.INFO, "RunId-{0}: there are {1} reports", runId, reports.Count);

            //if (reports.Count == 0) return;
            
            
            //foreach (string r in reports)
            //{
                //content = GetReports();
                
            //}


            summary = new QBarSummary(runId);

            while (true)
	        {
                
                string content = string.Empty;
                try 
	            {
                    if (reportQueue.Count > 0)
                    {
                        content = reportQueue.Dequeue();
                    }
                    else
                        continue;
	            }
	            catch
	            {
                    continue;
	            }
                if (string.IsNullOrEmpty(content) == false)
                {
                    ParseReports(content, summary);
                }

                //summary.Summarize();
                //summary.CloudResult();

                span = DateTime.Now.ToUniversalTime() - summary.EndTime;
                if (summary.IsComplete()) //the run is over
                {
                    //CloudWork.PutStringBlob(QBarSummaryContainer, QBarCompleteSummaryPrefix + "/" + runId, string.Empty, true);
                    summary.Summarize();
                    summary.CloudResult();
                    break;
                }
                //else if (summary.EndTime != DateTime.MinValue && span.TotalHours > 12) //the run is very old
                //{
                //    // if there is no summary data at all, the run should be removed
                //    if (summary.ScenarioList.Count == 0 && summary.ExceptionList.Count == 0)
                //    {
                //        CloudWork.DeleteBlob(QBarRawReportContainer, QBarActiveRunsPrefix + "/" + runId);
                //        foreach (string blob in CloudWork.GetBlobNames(QBarRawReportContainer, runId))
                //        {
                //            CloudWork.DeleteBlob(QBarRawReportContainer, blob);
                //        }
                //        foreach (string blob in CloudWork.GetBlobNames(QBarSummaryContainer, runId))
                //        {
                //            CloudWork.DeleteBlob(QBarSummaryContainer, blob);
                //        }
                //    }
                //    else //the run should be put into incomplete list
                //    {
                //        CloudWork.PutStringBlob(QBarSummaryContainer, QBarIncompleteSummaryPrefix + "/" + runId, string.Empty, true);
                //    }
                //} 
	        }
        }

        private static void GetReportFromQueue()
        {
            while (true)
            {
                string s = string.Empty;
                Message msg = null;
                try
                {                    
                    msg = mq.GetMessage(5);
                }
                catch
                {}
                if (msg != null)
                {
                    s = msg.ContentAsString();
                    reportQueue.Enqueue(s);
                    try
                    {
                        mq.DeleteMessage(msg);
                    }
                    catch { }
                }
            }
            
        }

        public static void ParseReports(string reports, QBarSummary summary)
        {
            Match m;

            m = Regex.Match(reports, "<report>(?'report'[^\0]*?)</report>");
            while(m.Success)
            {
                ParseOneReport(m.Groups["report"].Value, summary);
                m = m.NextMatch();
            }

            return;
        }

        static void ParseOneReport(string report, QBarSummary summary)
        {
            string worker, scenario, subKey, parseResult;
            bool status, isNoResource;
            double duration, apiduration;
            DateTime startedAt, finishedAt;
            string apiname, exceptionInfo, testInfoId, temp;
            WorkItemType type;

            parseResult = string.Empty;

            worker = ProtocolHelper.GetOneField(report, "worker");
            scenario = ProtocolHelper.GetOneField(report, "scenario");
            type = WorkItemType.Empty;
            temp = ProtocolHelper.GetOneField(report, "qbartype");
            if (temp != string.Empty)
            {
                type = (WorkItemType)Enum.Parse(typeof(WorkItemType), temp);
            }

            subKey = ProtocolHelper.GetOneField(report, "subkey");
            status = bool.Parse(ProtocolHelper.GetOneField(report, "status"));
            duration = double.Parse(ProtocolHelper.GetOneField(report, "duration"));
            finishedAt = DateTime.Parse(ProtocolHelper.GetOneField(report, "finishedat"));
            exceptionInfo = ProtocolHelper.GetOneField(report, "exception");
            testInfoId = ProtocolHelper.GetOneField(report, "testinfoid");

            if (exceptionInfo != string.Empty)
            {
                parseResult += string.Format("Worker: {0}, {1,-25}, {2,-8}, {3,-8}, {4,-10}, {5,-12}, {6}",
                    worker, scenario, subKey, type.ToString(), status, duration, finishedAt);
            }

            isNoResource = (exceptionInfo == WarningMessages.DeploymentUnavailable
                || exceptionInfo == WarningMessages.ProductDeploymentUnavailable
                || exceptionInfo == WarningMessages.StagingDeploymentUnavailable
                || exceptionInfo == WarningMessages.FullDeploymentUnavailable
                || exceptionInfo == WarningMessages.HostedServiceUnavailable
                || exceptionInfo == WarningMessages.StorageServiceUnavailable
                || exceptionInfo == WarningMessages.SubscriptionUnavailable
                || exceptionInfo == WarningMessages.StorageAccountUnavailable
                || exceptionInfo == WarningMessages.StorageContainerUnavailable
                || exceptionInfo == WarningMessages.StorageBlobUnavailable);

            if (duration < 0)
            {
                parseResult += string.Format("Duration should be non-negative, but current value is {0}", duration);
                duration = 0;
            }
            startedAt = finishedAt.AddSeconds(-1 * duration);
            summary.AddScenarioEntry(scenario, type, subKey, status, duration, startedAt, isNoResource, testInfoId);

            for (int i = 0; i < ProtocolHelper.GetFieldCount(report, "apiperf"); i++)
            {
                temp = ProtocolHelper.GetTimedField(report, "apiperf", i);
                apiname = ProtocolHelper.GetOneField(temp, "name");// +" - " + subKey;
                apiduration = double.Parse(ProtocolHelper.GetOneField(temp, "duration"));
                //Console.WriteLine("{0} = {1}", apiname, apiduration);

                summary.AddApiEntry(apiname, apiduration);
            }

            if (exceptionInfo != string.Empty)
            {
                summary.AddExceptionEntry(exceptionInfo, testInfoId);
            }

            return;
        }
    }

    public enum WorkItemType
    {
        Empty,
        Precondition,
        Concurrent,
        Average,
        Peak,
        Postcondition
    }

    public class ApiSummary
    {
        public string Name;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public long TestCount;
        public List<double> DurationList;

        public ApiSummary()
        {
            Name = string.Empty;
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            TestCount = 0;
            DurationList = new List<double>();
        }
    }

    public class ScenarioSummary
    {
        public string Name;
        public long NoResouceCount;
        public long Successed;
        public long Failed;
        public WorkItemType ItemType;
        public string ScenarioSubKey;
        public LinkedList<string> TestInfoIds;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public List<double> DurationList;

        public ScenarioSummary()
        {
            Name = string.Empty;
            NoResouceCount = 0;
            Successed = 0;
            Failed = 0;
            ItemType = WorkItemType.Empty;
            ScenarioSubKey = string.Empty;
            TestInfoIds = new LinkedList<string>();
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            DurationList = new List<double>();
        }
    }

    public class QBarSummary
    {
        public string RunId;
        public DateTime StartTime;
        public DateTime EndTime;
        public Dictionary<string, ApiSummary> ApiList;
        public Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>> ScenarioList;
        public Dictionary<string, LinkedList<string>> ExceptionList;

        public QBarSummary(string runid)
        {
            RunId = runid;
            StartTime = DateTime.MaxValue;
            EndTime = DateTime.MinValue;
            ApiList = new Dictionary<string, ApiSummary>();
            ScenarioList = new Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>>();
            ExceptionList = new Dictionary<string, LinkedList<string>>();
        }

        public void AddScenarioEntry(string scenario, WorkItemType type, string subKey, bool status, double duration, DateTime startedAt, bool isNoResource, string testInfoId)
        {
            Dictionary<WorkItemType, ScenarioSummary> wiss;
            ScenarioSummary ss;
            string key;

            if (StartTime > startedAt) StartTime = startedAt;
            if (EndTime < startedAt.AddSeconds(duration)) EndTime = startedAt.AddSeconds(duration);

            key = scenario.ToString() + subKey;
            if (ScenarioList.ContainsKey(key))
            {
                wiss = ScenarioList[key];
                if (wiss.ContainsKey(type))
                {
                    ss = wiss[type];
                }
                else
                {
                    ss = new ScenarioSummary();
                    wiss[type] = ss;
                }
            }
            else
            {
                wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                ScenarioList[key] = wiss;
                ss = new ScenarioSummary();
                wiss[type] = ss;
            }

            ss.Name = scenario;
            ss.ItemType = type;
            ss.ScenarioSubKey = subKey;
            ss.TestInfoIds.AddFirst(testInfoId);
            if (status == true)
            {
                ss.DurationSum += duration;
                ss.Successed++;
                if (ss.DurationMax < duration) ss.DurationMax = duration;
                if (ss.DurationMin > duration) ss.DurationMin = duration;

                ss.DurationList.Add(duration);
            }
            else if (isNoResource)
            {
                ss.NoResouceCount++;
            }
            else
            {
                ss.Failed++;
            }

            return;
        }

        public void AddApiEntry(string name, double duration)
        {
            ApiSummary apisum;

            if (ApiList.ContainsKey(name))
            {
                apisum = ApiList[name];
                apisum.DurationSum += duration;
                apisum.TestCount++;
                apisum.DurationList.Add(duration);
            }
            else
            {
                apisum = new ApiSummary()
                {
                    Name = name,
                    DurationSum = duration,
                    TestCount = 1
                };
                apisum.DurationList.Add(duration);
                ApiList[name] = apisum;
            }

            if (apisum.DurationMax < duration) apisum.DurationMax = duration;
            if (apisum.DurationMin > duration) apisum.DurationMin = duration;

            return;
        }

        public void AddExceptionEntry(string exceptionInfo, string testInfoId)
        {
            if (ExceptionList.ContainsKey(exceptionInfo) == false)
            {
                ExceptionList.Add(exceptionInfo, new LinkedList<string>());
            }

            ExceptionList[exceptionInfo].AddFirst(testInfoId);
            
            return;
        }

        public bool IsComplete()
        {
            //for compute, createsubscription == deletesubscription
            //for storage, CreateStorageAccount == DeleteStorageAccount
            //long created, deleted;

            //created = deleted = 0;
            //foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            //{
            //    foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
            //    {
            //        if (kvp2.Key == WorkItemType.Precondition)
            //        {
            //            if (kvp2.Value.Name == xStoreScenarios.CreateStorageAccount.ToString()
            //                || kvp2.Value.Name == RDFEScenarios.CreateSubscription.ToString())
            //            {
            //                created += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
            //            }
            //        }
            //        else if (kvp2.Key == WorkItemType.Postcondition)
            //        {
            //            if (kvp2.Value.Name == xStoreScenarios.DeleteStorageAccount.ToString()
            //                || kvp2.Value.Name == RDFEScenarios.DeleteSubscription.ToString())
            //            {
            //                deleted += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
            //            }
            //        }
            //    }
            //}

            //return (created != 0 && created == deleted);

            string account = "airwatchqbar", key = "G/mvNGCSb0GvNACyz2Ulkw/6PlDZfOW45EdKNqHBTdjt+1fCHonNwcI0vCxm9EAXWRJNQeOpNZAn+ifRuHgu9Q==";        
            string queueEndpoint = "http://queue.core.windows.net";        
            Uri baseUri = new Uri(queueEndpoint);
            bool? usePathStyleUris = false;
            StorageAccountInfo accountInfo = new StorageAccountInfo(baseUri, usePathStyleUris, account, key);
            QueueStorage q = QueueStorage.Create(accountInfo);
            MessageQueue mq = q.GetQueue("loadtestover");
            MessageQueue queueCount = q.GetQueue("reportsqueue");
            string workersDone = string.Empty;
            bool workersFinished = false;
            bool reportsProcessed = false;
            bool testFinished = false;
            try
            {
                workersDone = mq.GetMessage().ContentAsString();
            }
            catch
            {
            }
            if (workersDone != string.Empty)
                workersFinished = Convert.ToBoolean(workersDone);

            if (queueCount.ApproximateCount() <= 0)
                reportsProcessed = true;

            if (reportsProcessed == true && workersFinished == true)
                testFinished = true;

            return testFinished;
        }

        public void Summarize()
        {
            int i;

            //find the median value for Scenarios
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Value.DurationList.Count <= 0) continue;

                    kvp2.Value.DurationList.Sort();
                    i = kvp2.Value.DurationList.Count / 2;
                    if (kvp2.Value.DurationList.Count % 2 == 0)
                    {
                        kvp2.Value.DurationMedian = (kvp2.Value.DurationList[i - 1] + kvp2.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        kvp2.Value.DurationMedian = kvp2.Value.DurationList[i];
                    }
                }
            }

            //find the median value for APIs
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                if (apisum.Value.DurationList.Count > 0)
                {
                    apisum.Value.DurationList.Sort();
                    i = apisum.Value.DurationList.Count / 2;
                    if (apisum.Value.DurationList.Count % 2 == 0)
                    {
                        apisum.Value.DurationMedian = (apisum.Value.DurationList[i - 1] + apisum.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        apisum.Value.DurationMedian = apisum.Value.DurationList[i];
                    }
                }
            }

            return;
        }

        public void CloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            CloudOverall();
            CloudOnePhase(WorkItemType.Precondition);
            CloudOnePhase(WorkItemType.Average);
            CloudOnePhase(WorkItemType.Peak);
            CloudOnePhase(WorkItemType.Postcondition);
            CloudAPIs();
            CloudExceptions();

            return;
        }

        private void CloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/Overall", RunId);

            content = ProtocolHelper.BuildHeadFields("overall",
                new string[] { "starttime", "endtime", "runid"},
                new object[] { StartTime.ToString(), EndTime.ToString(), RunId });

            CloudWork.PutStringBlob(summaryContainer, blobName, content, true);
           
            return;
        }

        private void CloudOnePhase(WorkItemType type)
        {
            //startTime, endTime, RunId
            StringBuilder sb;
            string summaryContainer, blobName, testIds;
            double median, max, min;

            sb = new StringBuilder();
            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/{1}", RunId, type.ToString());

            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key != type) continue;

                    median = (kvp2.Value.DurationMedian == double.MinValue) ? -1.0 : kvp2.Value.DurationMedian;
                    max = (kvp2.Value.DurationMax == double.MinValue) ? -1.0 : kvp2.Value.DurationMax;
                    min = (kvp2.Value.DurationMin == double.MaxValue) ? -1.0 : kvp2.Value.DurationMin;

                    testIds = string.Empty;
                    foreach (string id in kvp2.Value.TestInfoIds)
                    {
                        testIds += ProtocolHelper.BuildOneField("id", id);
                    }

                    sb.Append(ProtocolHelper.BuildHeadFields("scenarioduration",
                        new string[] { "scenario", "subkey", "failed", "noresource", "succeed", "average", "median", "min", "max", "testids"},
                        new object[]{ kvp2.Value.Name, kvp2.Value.ScenarioSubKey, kvp2.Value.Failed, kvp2.Value.NoResouceCount, kvp2.Value.Successed,
                            kvp2.Value.Successed == 0 ? -1.0 : kvp2.Value.DurationSum / kvp2.Value.Successed, median, min, max, testIds}));
                }
            }

            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField(type.ToString(), sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        private void CloudAPIs()
        {
            StringBuilder sb;
            string summaryContainer, blobName;
            double median, max, min;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");

            sb = new StringBuilder();
            //API, Total, Average(s), Median(s), Min(s), Max(s)
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                median = (apisum.Value.DurationMedian == double.MinValue) ? -1.0 : apisum.Value.DurationMedian;
                max = (apisum.Value.DurationMax == double.MinValue) ? -1.0 : apisum.Value.DurationMax;
                min = (apisum.Value.DurationMin == double.MaxValue) ? -1.0 : apisum.Value.DurationMin;
                sb.Append(ProtocolHelper.BuildHeadFields("apiduration",
                    new string[] { "api", "count", "average", "median", "min", "max" },
                    new object[] { apisum.Key, apisum.Value.TestCount, apisum.Value.DurationSum / apisum.Value.TestCount, apisum.Value.DurationMedian, apisum.Value.DurationMin, apisum.Value.DurationMax }));
            }

            blobName = string.Format("{0}/APIs", RunId);
            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("apis", sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        private void CloudExceptions()
        {
            StringBuilder sb;
            string summaryContainer, blobName, testIds;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");

            sb = new StringBuilder();
            foreach (KeyValuePair<string, LinkedList<string>> kvp in ExceptionList)
            {
                testIds = string.Empty;
                foreach (string s in kvp.Value)
                {
                    testIds += ProtocolHelper.BuildOneField("id", s);
                }

                sb.Append(ProtocolHelper.BuildHeadFields("exception",
                    new string[] { "info", "testids" },
                    new object[] { kvp.Key, testIds }));
            }

            blobName = string.Format("{0}/Exceptions", RunId);
            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("exceptions", sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        public void ReadCloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            ReadCloudOverall();
            ReadCloudOnePhase(WorkItemType.Precondition);
            ReadCloudOnePhase(WorkItemType.Average);
            ReadCloudOnePhase(WorkItemType.Peak);
            ReadCloudOnePhase(WorkItemType.Postcondition);
            ReadCloudAPIs();
            ReadCloudExceptions();

            return;
        }

        private void ReadCloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/Overall", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                StartTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "starttime"));
                EndTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "endtime"));
            }

            return;
        }

        private void ReadCloudOnePhase(WorkItemType type)
        {
            string summaryContainer, blobName, content, one, testIdsReport;
            LinkedList<string> testIds;
            ScenarioSummary sum;
            int succeed;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/{1}", RunId, type.ToString());
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "scenarioduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "scenarioduration", i);

                    testIdsReport = ProtocolHelper.GetOneField(one, "testids");
                    testIds = new LinkedList<string>();
                    for (int k = 0; k < ProtocolHelper.GetFieldCount(testIdsReport, "id"); k++)
                    {
                        testIds.AddFirst(ProtocolHelper.GetTimedField(testIdsReport, "id", k));
                    }

                    succeed = int.Parse(ProtocolHelper.GetOneField(one, "succeed"));
                    sum = new ScenarioSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "scenario"),
                        ScenarioSubKey = ProtocolHelper.GetOneField(one, "subkey"),
                        Failed = int.Parse(ProtocolHelper.GetOneField(one, "failed")),
                        NoResouceCount = int.Parse(ProtocolHelper.GetOneField(one, "noresource")),
                        Successed = succeed,
                        ItemType = type,
                        TestInfoIds = testIds,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * succeed,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };

                    string key;
                    Dictionary<WorkItemType, ScenarioSummary> wiss;

                    key = sum.Name + sum.ScenarioSubKey;
                    if (ScenarioList.ContainsKey(key))
                    {
                        wiss = ScenarioList[key];
                    }
                    else
                    {
                        wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                        ScenarioList[key] = wiss;
                    }

                    wiss[type] = sum;
                }
            }

            return;
        }

        private void ReadCloudAPIs()
        {
            string summaryContainer, blobName, content, one;
            ApiSummary sum;
            int count;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/APIs", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "apiduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "apiduration", i);

                    count = int.Parse(ProtocolHelper.GetOneField(one, "count"));
                    sum = new ApiSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "api"),
                        TestCount = count,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * count,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };
                    ApiList.Add(sum.Name, sum);
                }
            }

            return;
        }

        private void ReadCloudExceptions()
        {
            string summaryContainer, blobName, content, one, testIdsReport;
            LinkedList<string> testIds;

            summaryContainer = RoleManager.GetConfigurationSetting("QualityBarReportSummaryContainer");
            blobName = string.Format("{0}/Exceptions", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "exception"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "exception", i);

                    testIdsReport = ProtocolHelper.GetOneField(one, "testids");
                    testIds = new LinkedList<string>();
                    for(int k=0; k < ProtocolHelper.GetFieldCount(testIdsReport, "id"); k++)
                    {
                        testIds.AddFirst(ProtocolHelper.GetTimedField(testIdsReport, "id", k));
                    }
                    ExceptionList.Add(ProtocolHelper.GetOneField(one, "info"), testIds);
                }
            }

            return;
        }
    }
}
