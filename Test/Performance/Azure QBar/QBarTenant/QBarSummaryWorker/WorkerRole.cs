﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Cis.E2E.QBar
{
    public class WorkerRole : RoleEntryPoint
    {
        public override void Start()
        {
            // This is a sample worker implementation. Replace with your logic.
            RoleManager.WriteToLog("Information", "Worker Process entry point called");

            Watcher.Init();
            while (true)
            {
                Watcher.WatchSummary();
                Thread.Sleep(5 * 1000); //every 5 seconds
            }
        }

        public override RoleStatus GetHealthStatus()
        {
            // This is a sample worker implementation. Replace with your logic.
            return RoleStatus.Healthy;
        }
    }
}
