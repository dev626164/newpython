﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class WorkerRole : RoleEntryPoint
    {
        public override void Start()
        {
            // This is a sample worker implementation. Replace with your logic.
            RoleManager.WriteToLog("Information", "Worker Process entry point called");

            QBarInstance.QBarUpdate();

            //while (true)
            //{
            //    Thread.Sleep(10000);
            //    RoleManager.WriteToLog("Information", "Working");
            //}

            return;
        }

        public override RoleStatus GetHealthStatus()
        {
            // This is a sample worker implementation. Replace with your logic.
            return RoleStatus.Healthy;
        }
    }
}
