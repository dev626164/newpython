﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Cis.E2E.QualityBar
{
    class QBarInstance
    {
        public static LinkedList<QBarInstance> AllQBarProc = new LinkedList<QBarInstance>();
        public static string ListenQueue;
        public static Timer RoleRegisterTimer;

        public static void QBarUpdate()
        {
            string listenerContainer, updateMsg;
            QBarInstance exeUpdate;

            if (RoleManager.IsRoleManagerRunning == false)
            {
                RoleManager.WriteToLog("Error", "The role manager is not running");
            }

            CloudWork.BlobEndpoint = RoleManager.GetConfigurationSetting("BlobEndpoint");
            CloudWork.QueueEndpoint = RoleManager.GetConfigurationSetting("QueueEndpoint");
            CloudWork.TableEndpoint = RoleManager.GetConfigurationSetting("TableEndpoint");
            CloudWork.AccountName = RoleManager.GetConfigurationSetting("E2EAccount");
            CloudWork.AccountKey = RoleManager.GetConfigurationSetting("E2EAccountKey");

            listenerContainer = RoleManager.GetConfigurationSetting("E2EAppListenerContainer");
            ListenQueue = Guid.NewGuid().ToString().ToLower();
            RoleRegisterTimer = new Timer(new TimerCallback(RegisterRole), listenerContainer, 0, 10 * 60 * 1000);

            while (true)
            {
                updateMsg = CloudWork.GetAndDeleteOneMessageStr(ListenQueue);
                if (string.IsNullOrEmpty(updateMsg))
                {
                    Thread.Sleep(10 * 1000); //10 seconds
                    continue;
                }

                exeUpdate = new QBarInstance();
                exeUpdate.ProcessRequest(updateMsg);
                if (exeUpdate.Used == true)
                {
                    AllQBarProc.AddFirst(exeUpdate);
                }
            }

            //return;
        }

        private static void RegisterRole(object stateInfo)
        {
            string listenerContainer;

            listenerContainer = (string)stateInfo;

            CloudWork.PutStringBlob(listenerContainer, ListenQueue, string.Empty, true);

            return;
        }

        public Thread QBarThread;
        public Process QBarProcess;
        public string QBarDir;
        public string QBarExecutable;
        public string QBarParameters;
        public bool Used;
        
        public QBarInstance()
        {
            QBarThread = null;
            QBarProcess = null;
            QBarDir = string.Empty;
            QBarExecutable = string.Empty;
            QBarParameters = string.Empty;
            Used = false;
        }

        public void ProcessRequest(string request)
        {
            string action;

            action = ProtocolHelper.GetOneField(request, "action");
            QBarLogger.Log(DebugLevel.INFO, "Action is {0}", action);

            switch (action.ToLower())
            {
                case "live":
                    SendLiveness(request);
                    break;

                case "update":
                    UpdateQBarInstance(request);
                    break;
            }
        }

        private void SendLiveness(string request)
        {
            string replyQueue;

            replyQueue = ProtocolHelper.GetOneField(request, "responsequeue");
            CloudWork.SendMessage(replyQueue, "Online");

            return;
        }

        private void UpdateQBarInstance(string request)
        {
            string qbarVersion, executable, parameters, replyQueue;
            bool download;
            string result;

            //stop the old QBar processes and threads
            foreach (QBarInstance inst in AllQBarProc)
            {
                try
                {
                    inst.QBarProcess.Kill();
                    inst.QBarThread.Abort();
                }
                catch
                {
                }

            }
            AllQBarProc = new LinkedList<QBarInstance>();

            qbarVersion = ProtocolHelper.GetHeadedField(request, "QBarBin", "Version");
            executable = ProtocolHelper.GetHeadedField(request, "QBarBin", "Executable");
            parameters = ProtocolHelper.GetHeadedField(request, "QBarBin", "Params");
            if (string.IsNullOrEmpty(qbarVersion) || string.IsNullOrEmpty(executable))
            {
                QBarLogger.Log(DebugLevel.ERROR, "Illegal format in update request");
                return;
            }

            download = DownloadQBarBinary(qbarVersion);
            if (download == false)
            {
                result = "Cannot download all quality bar binary";
                //result += "    request:" + request;
                //result += string.Format("; {0},{1},{2}", qbarVersion, executable, parameters);
                //result += string.Format("; Localstore, {0}", RoleManager.GetLocalResource("QBarWorkspace").RootPath);
            }
            else
            {
                string qbarDir;

                qbarDir = RoleManager.GetLocalResource("QBarWorkspace").RootPath;
                if (qbarDir.EndsWith("\\") == false) qbarDir += "\\";
                qbarDir += qbarVersion;

                StartQBar(qbarDir, qbarDir+"\\"+executable, parameters);
                if (QBarProcess == null || QBarProcess.HasExited)
                {
                    result = "Cannot start the quality bar";
                }
                else
                {
                    result = "Start new quality bar instance successfully";
                }
            }

            replyQueue = ProtocolHelper.GetOneField(request, "responsequeue");
            result += "; " + RoleManager.GetLocalResource("QBarWorkspace").RootPath;
            CloudWork.SendMessage(replyQueue, result);

            return;
        }

        private bool DownloadQBarBinary(string version)
        {
            string qbarBinContainer, blobPrefix, filePath;
            LinkedList<string> blobs;
            bool success;
            
            qbarBinContainer = RoleManager.GetConfigurationSetting("E2EQBarBinaryContainer");
            blobPrefix = string.Format("{0}/QBarBin/", version);
            blobs = CloudWork.GetBlobNames(qbarBinContainer, blobPrefix);

            success = false;
            foreach (string blob in blobs)
            {
                try
                {
                    filePath = Blob2FilePath(blob);
                    success = CloudWork.GetFileBlob(qbarBinContainer, blob, filePath);
                    if (success == false)
                    {
                        QBarLogger.Log(DebugLevel.ERROR, "Cannot download blob {0} with filepath {1}", blob, filePath);
                        break;
                    }
                }
                catch
                {
                    RoleManager.WriteToLog("Information", "Cannot download the blob");
                }
            }

            return success;
        }

        private string Blob2FilePath(string blobName)
        {
            string filePath;
            string[] dirs;

            filePath = string.Empty;
            dirs = blobName.Split('/'); //[verion]/QBarBin/[dir]/.../[dir]/[file]
            if (dirs.Length < 3)
            {
                QBarLogger.Log(DebugLevel.ERROR, "the blob name {0} is illegal in QBar", blobName);
                return filePath;
            }

            filePath = RoleManager.GetLocalResource("QBarWorkspace").RootPath;
            if (filePath.EndsWith("\\") == false) filePath += "\\";
            filePath += dirs[0];
            if (Directory.Exists(filePath) == false)
            {
                Directory.CreateDirectory(filePath);
            }
            for (int i = 2; i < dirs.Length-1; i++)
            {
                filePath += "\\" + dirs[i];
                if (Directory.Exists(filePath) == false)
                {
                    Directory.CreateDirectory(filePath);
                }
            }
            filePath += "\\" + dirs[dirs.Length - 1];
        
            return filePath;
        }

        bool ProcessThreadEnd;
        private void StartQBar(string qbarDir, string executable, string parameters)
        {
            QBarDir = qbarDir;
            QBarExecutable = executable;
            QBarParameters = parameters;

            ProcessThreadEnd = false;
            QBarThread = new Thread(new ThreadStart(QBarExecution));
            QBarThread.Start();

            while (ProcessThreadEnd == false)
            {
                Thread.Sleep(5 * 1000);
            }

            if (QBarProcess != null)
            {
                Used = true;
            }

            return;
        }

        private void QBarExecution()
        {
            QBarProcess = new Process();

            QBarProcess.StartInfo.WorkingDirectory = QBarDir;
            QBarProcess.StartInfo.FileName = QBarExecutable;
            QBarProcess.StartInfo.Arguments = QBarParameters;
            QBarProcess.StartInfo.CreateNoWindow = true;
            QBarProcess.StartInfo.UseShellExecute = false;
            QBarProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            QBarProcess.StartInfo.RedirectStandardOutput = false;

            try
            {
                QBarProcess.Start();
                ProcessThreadEnd = true;
                RoleManager.WriteToLog("Information", "Start the process successfully in QBar");

                QBarProcess.WaitForExit();
                //File.WriteAllText(".\\QBarOutput.txt", QBarProcess.StandardOutput.ReadToEnd());
            }
            catch (Exception ex)
            {
                RoleManager.WriteToLog("Information", "Cannot start the process");
                RoleManager.WriteToLog("Information", ex.ToString());
                QBarProcess = null;
            }

            ProcessThreadEnd = true;

            return;
        }
    }
}
