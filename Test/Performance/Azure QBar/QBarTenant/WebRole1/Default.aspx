﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebRole1._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Homepage - Windows Azure Quality Bar</title>
    <style type="text/css">
        .style1
        {
            text-align: center;
            font-size: large;
        }
        .style2
        {
            font-family: "Times New Roman", Times, serif;
            font-size: xx-large;
            color: #FFFF99;
            background-color: #0000FF;
        }
        .style4
        {
            text-align: left;
            font-size: xx-large;
            font-weight: bold;
        }
        .style3
        {
            text-align: left;
        }
        #form1
        {
            height: 508px;
            width: 834px;
            margin-top: 70px;
        }
    </style>
</head>
<body bgcolor="#ccccff">
    <form id="form1" runat="server">
    <p class="style1" style="background-color: #CCCCFF">
        <b style="background-color: #66CCFF"><span class="style2">&nbsp;&nbsp; Windows Azure Quality Bar&nbsp; &nbsp;</span></b></p>
    <p class="style1" style="background-color: #CCCCFF">
        &nbsp;</p>
    <asp:Panel ID="TaskList" runat="server" Height="408px" style="text-align: center" 
        Width="452px" Direction="LeftToRight" HorizontalAlign="Center">
        <div style="background-color: #CCCCFF" class="style4">
            Available Tasks</div>
        <ul>
            <li>
                <p class="style3">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ReportList.aspx">QBar Reports</asp:HyperLink>
                </p>
            </li>
            <li>
                <p class="style3">
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/RegressionGraph.aspx" 
                        Target="_blank">Regression Analysis</asp:HyperLink>
                </p>
            </li>
            <li>
                <p class="style3">
                    <asp:HyperLink ID="DeleteIncomplete" runat="server" 
                        NavigateUrl="~/DeleteIncomplete.aspx" Target="_self">Delete Incomplete QBar Runs</asp:HyperLink>
                </p>
            </li>
            <li>
                <p class="style3">
                    Update QBar Binary</p>
            </li>
            <li>
                <p class="style3">
                    Start QBar Tests for Compute</p>
            </li>
            <li>
                <p class="style3">
                    Start QBar Tests for Storage</p>
            </li>
        </ul>
    </asp:Panel>
    </form>
    </body>
</html>
