﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public class ScenarioLabel
    {
        public WorkItemType Type;
        public string name;
        public string subKey;
    }

    public partial class Regression : System.Web.UI.Page
    {
        private string QBarRegressionContainer;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            CloudWork.SetAccount(ConfigurationSettings.AppSettings["BlobEndpoint"],
                ConfigurationSettings.AppSettings["QueueEndpoint"],
                ConfigurationSettings.AppSettings["TableEndpoint"],
                ConfigurationSettings.AppSettings["E2EAccount"],
                ConfigurationSettings.AppSettings["E2EAccountKey"]);            
            QBarRegressionContainer = ConfigurationSettings.AppSettings["QualityBarReportRegressionContainer"];

            AddScenarioRegressionChart(RDFEScenarios.PutDeployment.ToString());
            AddScenarioRegressionChart(RDFEScenarios.UpgradeDeployment.ToString());
            AddScenarioRegressionChart(RDFEScenarios.UpdateDeploymentConfig.ToString());
            AddScenarioRegressionChart(RDFEScenarios.DeleteDeployment.ToString());

            AddAPIRegressionChart("WaitPromoteDeploymentToProduction");
        
            return;
        }

        void AddScenarioRegressionChart(string scenario)
        {
            SortedDictionary<DateTime, double> series;
            object[] paramArray;
            int i;

            series = GetSeriesForScenario(scenario);
            if (series.Count != 0)
            {
                paramArray = new object[series.Count];

                i = 0;
                foreach (KeyValuePair<DateTime, double> kvp in series)
                {
                    paramArray[i] = kvp.Value;
                    i++;
                }

                AddOneChart(scenario, "inlinebar", paramArray);

                string str;
                str = string.Empty;
                foreach (KeyValuePair<DateTime, double> kvp in series)
                {
                    if (string.IsNullOrEmpty(str) == false) str += ",";
                    str += kvp.Key;
                }

                QBarRegressionTable.Rows.AddAt(0, AddTableCells(new object[] { scenario, str }));
            }
            
            return;
        }

        void AddAPIRegressionChart(string api)
        {
            SortedDictionary<DateTime, double> series;
            object[] paramArray;
            int i;

            series = GetSeriesForAPI(api);
            if (series.Count != 0)
            {
                paramArray = new object[series.Count];

                i = 0;
                foreach (KeyValuePair<DateTime, double> kvp in series)
                {
                    paramArray[i] = kvp.Value;
                    i++;
                }

                AddOneChart("API:" + api, "inlinebar", paramArray);
            }

            return;
        }

        SortedDictionary<DateTime, double> GetSeriesForScenario(string scenarioName)
        {
            SortedDictionary<DateTime, double> series, counts;
            LinkedList<string> blobNames;
            string content, name, entity;
            double total, durationSum;
            DateTime time;

            series = new SortedDictionary<DateTime, double>();
            counts = new SortedDictionary<DateTime,double>();
            blobNames = CloudWork.GetBlobNames(QBarRegressionContainer, "regression/summary/scenario/" + scenarioName);
            foreach (string blob in blobNames) //should be one? or multiple with subkeys
            {
                content = GetFromLocalCopy(blob);

                name = ProtocolHelper.GetOneField(content, "name");
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "entity"); i++)
                {
                    entity = ProtocolHelper.GetTimedField(content, "entity", i);

                    time = DateTime.Parse(ProtocolHelper.GetOneField(entity, "timestamp"));
                    total = double.Parse(ProtocolHelper.GetOneField(entity, "total"));
                    durationSum = double.Parse(ProtocolHelper.GetOneField(entity, "durationsum"));

                    if (series.ContainsKey(time) == false)
                    {
                        series.Add(time, 0.0);
                        counts.Add(time, 0.0);
                    }

                    if (total != -1.0)
                    {
                        series[time] += durationSum;
                        counts[time] += total;
                    }
                }
            }

            foreach (KeyValuePair<DateTime, double> kvp in counts)
            {
                if (kvp.Value == 0.0)
                {
                    series[kvp.Key] = -1.0;
                }
                else
                {
                    series[kvp.Key] /= kvp.Value;
                }
            }

            return series;
        }

        SortedDictionary<DateTime, double> GetSeriesForAPI(string api)
        {
            SortedDictionary<DateTime, double> series, counts;
            string blobName;
            string content, name, entity;
            double total, durationSum;
            DateTime time;

            series = new SortedDictionary<DateTime, double>();
            counts = new SortedDictionary<DateTime, double>();

            blobName = "regression/summary/api/" + api;        
            content = GetFromLocalCopy(blobName);
            name = ProtocolHelper.GetOneField(content, "name");
            for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "entity"); i++)
            {
                entity = ProtocolHelper.GetTimedField(content, "entity", i);

                time = DateTime.Parse(ProtocolHelper.GetOneField(entity, "timestamp"));
                total = double.Parse(ProtocolHelper.GetOneField(entity, "total"));
                durationSum = double.Parse(ProtocolHelper.GetOneField(entity, "durationsum"));

                if (series.ContainsKey(time) == false)
                {
                    series.Add(time, 0.0);
                    counts.Add(time, 0.0);
                }

                if (total != -1.0)
                {
                    series[time] += durationSum;
                    counts[time] += total;
                }
            }

            foreach (KeyValuePair<DateTime, double> kvp in counts)
            {
                if (kvp.Value == 0.0)
                {
                    series[kvp.Key] = -1.0;
                }
                else
                {
                    series[kvp.Key] /= kvp.Value;
                }
            }

            return series;
        }

        string GetFromLocalCopy(string blob)
        {
            string filename, content, regressionDir;

            regressionDir = ".\\regression";
            if (Directory.Exists(regressionDir) == false) Directory.CreateDirectory(regressionDir);

            content = string.Empty;
            filename = regressionDir + "\\" + blob.Replace('/', '+');
            if (File.Exists(filename))
            {
                content = File.ReadAllText(filename);
            }
            else
            {
                content = CloudWork.GetStringBlob(QBarRegressionContainer, blob);
                File.WriteAllText(filename, content);
            }

            return content;
        }

        void AddOneChart(string title, string graphStyle, object[] dataSeries)
        {
            string seriesStr, chartStr;

            seriesStr = string.Empty;
            foreach (object obj in dataSeries)
            {
                if(string.IsNullOrEmpty(seriesStr) == false) seriesStr += ",";
                seriesStr += obj;
            }

            chartStr = string.Format("<span class=\"{0}\">{1}</span>", graphStyle, seriesStr);
            QBarRegressionTable.Rows.AddAt(0, AddTableCells(new object[] { title, chartStr }));

            //Response.Write(chartStr);

            return;
        }

        private TableRow AddTableCells(object[] contents)
        {
            TableRow row;
            TableCell cell;

            row = new TableRow();
            foreach (object obj in contents)
            {
                cell = new TableCell();
                cell.Text = obj.ToString();
                row.Cells.Add(cell);
            }

            return row;
        }
    }
}
