﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public partial class Report : System.Web.UI.Page
    {
        private int Timeout;

        protected void Page_Load(object sender, EventArgs e)
        {
            string[] array;
            string runId, status;
            int year, month;

            Timeout = Server.ScriptTimeout;
            Server.ScriptTimeout = 3600;

            if (Request.QueryString.AllKeys.Contains<string>("Year"))
            {
                year = int.Parse(Request.QueryString["Year"].ToString());
            }
            else
            {
                year = DateTime.Now.Year;
            }

            if(Request.QueryString.AllKeys.Contains<string>("Month"))
            {
                month = int.Parse(Request.QueryString["Month"].ToString());
            }
            else
            {
                month = DateTime.Now.Month;
            }

            Watcher.Init();

            QBarRuns.Rows.Add(AddTableCells(new object[] { "<b>RunTime</b>", "<b>RunID</b>", "<b>Status</b>"}));
            foreach(KeyValuePair<DateTime, string> kvp in Watcher.GetExistingRuns(year, month))
            {
                foreach (string runIdStatus in kvp.Value.Split(';'))
                {
                    array = runIdStatus.Split('+');
                    runId = array[0];
                    status = array[1];

                    QBarRuns.Rows.AddAt(1, AddTableCells(
                        new object[] {kvp.Key, string.Format("<a href =\"RunReport.aspx?RunId={0}\">{0}</a>", runId), status}));
                }
            }

            return;
        }

        private void Page_Unload(object sender, System.EventArgs e)
        {
            Server.ScriptTimeout = Timeout;
        }

        private TableRow AddTableCells(object[] contents)
        {
            TableRow row;
            TableCell cell;

            row = new TableRow();
            foreach (object obj in contents)
            {
                cell = new TableCell();
                cell.Text = obj.ToString();
                row.Cells.Add(cell);
            }

            return row;
        }
    }
}
