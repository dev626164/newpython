﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteIncomplete.aspx.cs" Inherits="Microsoft.Cis.E2E.QBar.DeleteIncomplete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Delete Incomplete QBar Runs</title>
    <style type="text/css">

        .style5
        {
            color: #FFFFFF;
            font-family: "Times New Roman", Times, serif;
        }
        .style4
        {
            font-size: x-large;
            background-color: #0000CC;
        }
        .style6
        {
            text-align: center;
        }
        .style2
        {
            font-family: "Times New Roman", Times, serif;
            font-size: x-large;
            color: #FFFFFF;
            background-color: #0000CC;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="style6">
    
    &nbsp;<span 
            class="style5"><b class="style4"><br />
        </b></span>
    
    </div>
    <div class="style6">
        <span class="style5">
        <b class="style4">&nbsp;&nbsp; 
        </b>
        </span>
        <b style="background-color: #66CCFF"><span class="style2">Windows Azure Quality Bar 
        Maintenance</span></b><span 
            class="style5"><b class="style4"> 
        </b></span>
        <br />
        <br />
    </div>
    <asp:Table ID="IncompleteRunTable" runat="server" BackColor="#CCFFFF" 
        BorderColor="#FF3300" BorderStyle="Double" BorderWidth="2px" CellPadding="1" 
        CellSpacing="1" GridLines="Both" Height="26px" HorizontalAlign="Center" 
        style="text-align: center" Width="1036px">
    </asp:Table>
    </form>
</body>
</html>
