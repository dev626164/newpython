﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public partial class DeleteIncomplete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.HasKeys())
            {
                string runId;

                runId = Request.QueryString["RunId"].ToString();
                if (string.IsNullOrEmpty(runId))
                {
                    Response.Write(string.Format("RunId is empty"));
                }
                else
                {
                    Response.Write(string.Format("Delete a run with id {0}", runId));
                    DeleteRunInfo(runId);
                }
            }
            
            ListIncompleteRuns();
            
            return;
        }

        void DeleteRunInfo(string runId)
        {
            string container, runIdBlob;
            LinkedList<string> reports;

            container = ConfigurationSettings.AppSettings["QualityBarReportContainer"];
            runIdBlob = string.Format("{0}/{1}", ConfigurationSettings.AppSettings["QualityBarReportActiveRunsPrefix"], runId);
            
            CloudWork.DeleteBlob(container, runIdBlob);
            reports = CloudWork.GetBlobNames(container, runId);
            if (reports != null && reports.Count > 0)
            {
                foreach (string s in reports)
                {
                    CloudWork.DeleteBlob(container, s);
                }
            }

            container = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            reports = CloudWork.GetBlobNames(container, runId);
            if (reports != null && reports.Count > 0)
            {
                foreach (string s in reports)
                {
                    CloudWork.DeleteBlob(container, s);
                }
            }

            return;
        }

        void ListIncompleteRuns()
        {
            //IncompleteRunTable.Rows.Add(AddTableCells(new object[] { "<b>StartTime</b>", "<b>EndTime</b>", "<b>RunID</b>", "<b>Complete</b>", "<b>Deleted?</b>" }));
            //foreach (KeyValuePair<DateTime, string> kvp in Watcher.GetExistingRuns())
            //{
            //    if (kvp.Value.EndTime == DateTime.MinValue) continue;
            //    if ((DateTime.Now.ToUniversalTime() - kvp.Value.EndTime).TotalDays < 3) continue;
            //    if (kvp.Value.Complete == true) continue;

            //    IncompleteRunTable.Rows.AddAt(1, AddTableCells(
            //        new object[] { 
            //            kvp.Value.StartTime, 
            //            kvp.Value.EndTime,
            //            kvp.Value.RunId,
            //            kvp.Value.Complete,
            //            string.Format("<a href=\"DeleteIncomplete.aspx?RunId={0}\">DELETE!</a>", kvp.Value.RunId)}));
            //}

            return;
        }

        private TableRow AddTableCells(object[] contents)
        {
            TableRow row;
            TableCell cell;

            row = new TableRow();
            foreach (object obj in contents)
            {
                cell = new TableCell();
                cell.Text = obj.ToString();
                if (contents.Length == 1) cell.ColumnSpan = 9;
                row.Cells.Add(cell);
            }

            row.BorderStyle = BorderStyle.Inset;

            return row;
        }
    }
}
