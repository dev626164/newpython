﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public class Watcher
    {
        private static string E2EBlobEndpoint, E2EQueueEndpoint, E2ETableEndpoint;
        private static string E2EAccount, E2EAccountKey;
        private static string QBarRawReportContainer, QBarActiveRunsPrefix;
        private static string QBarSummaryContainer, QBarCompleteSummaryPrefix, QBarIncompleteSummaryPrefix;

        public static void Init()
        {
            E2EBlobEndpoint = ConfigurationSettings.AppSettings["BlobEndpoint"];
            E2EQueueEndpoint = ConfigurationSettings.AppSettings["QueueEndpoint"];
            E2ETableEndpoint = ConfigurationSettings.AppSettings["TableEndpoint"];
            E2EAccount = ConfigurationSettings.AppSettings["E2EAccount"];
            E2EAccountKey = ConfigurationSettings.AppSettings["E2EAccountKey"];

            QBarRawReportContainer = ConfigurationSettings.AppSettings["QualityBarReportContainer"];
            QBarActiveRunsPrefix = ConfigurationSettings.AppSettings["QualityBarReportActiveRunsPrefix"];

            QBarSummaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            QBarCompleteSummaryPrefix = ConfigurationSettings.AppSettings["QualityBarReportCompleteRunsPrefix"];
            QBarIncompleteSummaryPrefix = ConfigurationSettings.AppSettings["QualityBarReportIncompleteRunsPrefix"];

            return;
        }

        public static List<int> GetRunTimeInfo()
        {
            LinkedList<string> activeRunIds;
            DateTime blobTime;
            List<int> allRuns;
            int id;

            allRuns = new List<int>();
            activeRunIds = CloudWork.GetBlobNames(QBarRawReportContainer, QBarActiveRunsPrefix);
            foreach (string runIdBlobName in activeRunIds)
            {
                //format "RunPrefix/[RunId]"
                blobTime = CloudWork.GetBlobTime(QBarRawReportContainer, runIdBlobName);

                id = blobTime.Year * 100 + blobTime.Month;
                if (allRuns.Contains(id) == false) allRuns.Add(id);
            }

            allRuns.Sort();

            return allRuns;
        }

        public static SortedDictionary<DateTime, string> GetExistingRuns(int year, int month)
        {
            LinkedList<string> activeRunIds, completeRuns, incompleteRuns;
            string runId, status;
            DateTime blobTime;
            SortedDictionary<DateTime, string> AllRuns;
            int len;

            AllRuns = new SortedDictionary<DateTime, string>();
            
            activeRunIds = CloudWork.GetBlobNames(QBarRawReportContainer, QBarActiveRunsPrefix);
            completeRuns = CloudWork.GetBlobNames(QBarSummaryContainer, QBarCompleteSummaryPrefix);
            incompleteRuns = CloudWork.GetBlobNames(QBarSummaryContainer, QBarIncompleteSummaryPrefix);

            len = QBarActiveRunsPrefix.Length;
            foreach (string runIdBlobName in activeRunIds)
            {
                //format "RunPrefix/[RunId]"
                runId = runIdBlobName.Substring(len + 1, runIdBlobName.Length - len - 1);
                blobTime = CloudWork.GetBlobTime(QBarRawReportContainer, runIdBlobName);
                if (blobTime.Year != year || blobTime.Month != month) continue;

                status = "In Progress";
                if (completeRuns.Contains(QBarCompleteSummaryPrefix + "/" + runId))
                {
                    status = "Complete";
                }
                else if(incompleteRuns.Contains(QBarIncompleteSummaryPrefix + "/" + runId))
                {
                    status = "Incomplete";
                }

                if (status == "In Progress") UploadRunCompleteness(runId);
                
                if (AllRuns.ContainsKey(blobTime) == false)
                {
                    AllRuns.Add(blobTime, runId + "+" + status);
                }
                else
                {
                    AllRuns[blobTime] += ";" + runId + "+" + status;
                }
            }

            return AllRuns;
        }

        private static void UploadRunCompleteness(string runId)
        {
            QBarReport report;
            TimeSpan span;

            report = new QBarReport(runId);
            report.ReadCloudResult();

            span = DateTime.Now.ToUniversalTime() - report.EndTime;
            if (report.IsComplete()) //the run is over
            {
                CloudWork.PutStringBlob(QBarSummaryContainer, QBarCompleteSummaryPrefix + "/" + runId, string.Empty, true);
            }
            else if (report.EndTime != DateTime.MinValue && span.TotalHours > 12) //the run is very old
            {
                // if there is no summary data at all, the run should be removed
                if (report.ScenarioList.Count == 0 && report.ExceptionList.Count == 0)
                {
                    CloudWork.DeleteBlob(QBarRawReportContainer, QBarActiveRunsPrefix + "/" + runId);
                    foreach (string blob in CloudWork.GetBlobNames(QBarRawReportContainer, runId))
                    {
                        CloudWork.DeleteBlob(QBarRawReportContainer, blob);
                    }
                    foreach (string blob in CloudWork.GetBlobNames(QBarSummaryContainer, runId))
                    {
                        CloudWork.DeleteBlob(QBarSummaryContainer, blob);
                    }
                }
                else //the run should be put into incomplete list
                {
                    CloudWork.PutStringBlob(QBarSummaryContainer, QBarIncompleteSummaryPrefix + "/" + runId, string.Empty, true);
                }
            }
        }
    }

    public enum WorkItemType
    {
        Empty,
        Precondition,
        Concurrent,
        Average,
        Peak,
        Postcondition
    }

    public class ApiSummary
    {
        public string Name;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public long TestCount;
        public List<double> DurationList;

        public ApiSummary()
        {
            Name = string.Empty;
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            TestCount = 0;
            DurationList = new List<double>();
        }
    }

    public class ScenarioSummary
    {
        public string Name;
        public long NoResouceCount;
        public long Successed;
        public long Failed;
        public WorkItemType ItemType;
        public string ScenarioSubKey;
        public LinkedList<string> TestInfoIds;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public List<double> DurationList;

        public ScenarioSummary()
        {
            Name = string.Empty;
            NoResouceCount = 0;
            Successed = 0;
            Failed = 0;
            ItemType = WorkItemType.Empty;
            ScenarioSubKey = string.Empty;
            TestInfoIds = new LinkedList<string>();
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            DurationList = new List<double>();
        }
    }

    public class QBarReport
    {
        public string RunId;
        public DateTime StartTime;
        public DateTime EndTime;
        public Dictionary<string, ApiSummary> ApiList;
        public Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>> ScenarioList;
        public Dictionary<string, LinkedList<string>> ExceptionList;

        public QBarReport(string runid)
        {
            RunId = runid;
            StartTime = DateTime.MaxValue;
            EndTime = DateTime.MinValue;
            ApiList = new Dictionary<string, ApiSummary>();
            ScenarioList = new Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>>();
            ExceptionList = new Dictionary<string, LinkedList<string>>();
        }

        public void AddScenarioEntry(string scenario, WorkItemType type, string subKey, bool status, double duration, DateTime startedAt, bool isNoResource, string testInfoId)
        {
            Dictionary<WorkItemType, ScenarioSummary> wiss;
            ScenarioSummary ss;
            string key;

            if (StartTime > startedAt) StartTime = startedAt;
            if (EndTime < startedAt.AddMinutes(duration)) EndTime = startedAt;

            key = scenario.ToString() + subKey;
            if (ScenarioList.ContainsKey(key))
            {
                wiss = ScenarioList[key];
                if (wiss.ContainsKey(type))
                {
                    ss = wiss[type];
                }
                else
                {
                    ss = new ScenarioSummary();
                    wiss[type] = ss;
                }
            }
            else
            {
                wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                ScenarioList[key] = wiss;
                ss = new ScenarioSummary();
                wiss[type] = ss;
            }

            ss.Name = scenario;
            ss.ItemType = type;
            ss.ScenarioSubKey = subKey;
            ss.TestInfoIds.AddFirst(testInfoId);
            if (status == true)
            {
                ss.DurationSum += duration;
                ss.Successed++;
                if (ss.DurationMax < duration) ss.DurationMax = duration;
                if (ss.DurationMin > duration) ss.DurationMin = duration;

                ss.DurationList.Add(duration);
            }
            else if (isNoResource)
            {
                ss.NoResouceCount++;
            }
            else
            {
                ss.Failed++;
            }

            return;
        }

        public void AddApiEntry(string name, double duration)
        {
            ApiSummary apisum;

            if (ApiList.ContainsKey(name))
            {
                apisum = ApiList[name];
                apisum.DurationSum += duration;
                apisum.TestCount++;
                apisum.DurationList.Add(duration);
            }
            else
            {
                apisum = new ApiSummary()
                {
                    Name = name,
                    DurationSum = duration,
                    TestCount = 1
                };
                apisum.DurationList.Add(duration);
                ApiList[name] = apisum;
            }

            if (apisum.DurationMax < duration) apisum.DurationMax = duration;
            if (apisum.DurationMin > duration) apisum.DurationMin = duration;

            return;
        }

        public void AddExceptionEntry(string exceptionInfo, string testInfoId)
        {
            if (ExceptionList.ContainsKey(exceptionInfo) == false)
            {
                ExceptionList.Add(exceptionInfo, new LinkedList<string>());
            }

            ExceptionList[exceptionInfo].AddFirst(testInfoId);

            return;
        }

        public bool IsComplete()
        {
            //for compute, createsubscription == deletesubscription
            //for storage, CreateStorageAccount == DeleteStorageAccount
            long created, deleted;

            created = deleted = 0;
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key == WorkItemType.Precondition)
                    {
                        if (kvp2.Value.Name == xStoreScenarios.CreateStorageAccount.ToString()
                            || kvp2.Value.Name == RDFEScenarios.CreateSubscription.ToString())
                        {
                            created += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
                        }
                    }
                    else if (kvp2.Key == WorkItemType.Postcondition)
                    {
                        if (kvp2.Value.Name == xStoreScenarios.DeleteStorageAccount.ToString()
                            || kvp2.Value.Name == RDFEScenarios.DeleteSubscription.ToString())
                        {
                            deleted += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
                        }
                    }
                }
            }

            return (created != 0 && created == deleted);
        }

        public void Summarize()
        {
            int i;

            //find the median value for Scenarios
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Value.DurationList.Count <= 0) continue;

                    kvp2.Value.DurationList.Sort();
                    i = kvp2.Value.DurationList.Count / 2;
                    if (kvp2.Value.DurationList.Count % 2 == 0)
                    {
                        kvp2.Value.DurationMedian = (kvp2.Value.DurationList[i - 1] + kvp2.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        kvp2.Value.DurationMedian = kvp2.Value.DurationList[i];
                    }
                }
            }

            //find the median value for APIs
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                if (apisum.Value.DurationList.Count > 0)
                {
                    apisum.Value.DurationList.Sort();
                    i = apisum.Value.DurationList.Count / 2;
                    if (apisum.Value.DurationList.Count % 2 == 0)
                    {
                        apisum.Value.DurationMedian = (apisum.Value.DurationList[i - 1] + apisum.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        apisum.Value.DurationMedian = apisum.Value.DurationList[i];
                    }
                }
            }

            return;
        }

        public void CloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            CloudOverall();
            CloudOnePhase(WorkItemType.Precondition);
            CloudOnePhase(WorkItemType.Average);
            CloudOnePhase(WorkItemType.Peak);
            CloudOnePhase(WorkItemType.Postcondition);
            CloudAPIs();
            CloudExceptions();

            return;
        }

        private void CloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/Overall", RunId);

            content = ProtocolHelper.BuildHeadFields("overall",
                new string[] { "starttime", "endtime", "runid" },
                new object[] { StartTime.ToString(), EndTime.ToString(), RunId });
            CloudWork.PutStringBlob(summaryContainer, blobName, content, true);

            return;
        }

        private void CloudOnePhase(WorkItemType type)
        {
            //startTime, endTime, RunId
            StringBuilder sb;
            string summaryContainer, blobName, testIds;
            double median, max, min;

            sb = new StringBuilder();
            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/{1}", RunId, type.ToString());

            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key != type) continue;

                    median = (kvp2.Value.DurationMedian == double.MinValue) ? -1.0 : kvp2.Value.DurationMedian;
                    max = (kvp2.Value.DurationMax == double.MinValue) ? -1.0 : kvp2.Value.DurationMax;
                    min = (kvp2.Value.DurationMin == double.MaxValue) ? -1.0 : kvp2.Value.DurationMin;

                    testIds = string.Empty;
                    foreach (string id in kvp2.Value.TestInfoIds)
                    {
                        testIds += ProtocolHelper.GetOneField("id", id);
                    }

                    sb.Append(ProtocolHelper.BuildHeadFields("scenarioduration",
                        new string[] { "scenario", "subkey", "failed", "noresource", "succeed", "average", "median", "min", "max", "testids" },
                        new object[]{ kvp2.Value.Name, kvp2.Value.ScenarioSubKey, kvp2.Value.Failed, kvp2.Value.NoResouceCount, kvp2.Value.Successed,
                            kvp2.Value.Successed == 0 ? -1.0 : kvp2.Value.DurationSum / kvp2.Value.Successed, median, min, max, testIds}));
                }
            }

            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField(type.ToString(), sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        private void CloudAPIs()
        {
            StringBuilder sb;
            string summaryContainer, blobName;
            double median, max, min;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];

            sb = new StringBuilder();
            //API, Total, Average(s), Median(s), Min(s), Max(s)
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                median = (apisum.Value.DurationMedian == double.MinValue) ? -1.0 : apisum.Value.DurationMedian;
                max = (apisum.Value.DurationMax == double.MinValue) ? -1.0 : apisum.Value.DurationMax;
                min = (apisum.Value.DurationMin == double.MaxValue) ? -1.0 : apisum.Value.DurationMin;
                sb.Append(ProtocolHelper.BuildHeadFields("apiduration",
                    new string[] { "api", "count", "average", "median", "min", "max" },
                    new object[] { apisum.Key, apisum.Value.TestCount, apisum.Value.DurationSum / apisum.Value.TestCount, apisum.Value.DurationMedian, apisum.Value.DurationMin, apisum.Value.DurationMax }));
            }

            blobName = string.Format("{0}/APIs", RunId);
            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("apis", sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        private void CloudExceptions()
        {
            StringBuilder sb;
            string summaryContainer, blobName, testIds;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];

            sb = new StringBuilder();
            foreach (KeyValuePair<string, LinkedList<string>> kvp in ExceptionList)
            {
                testIds = string.Empty;
                foreach (string s in kvp.Value)
                {
                    testIds += ProtocolHelper.GetOneField("id", s);
                }

                sb.Append(ProtocolHelper.BuildHeadFields("exception",
                    new string[] { "info", "testids" },
                    new object[] { kvp.Key, testIds }));
            }

            blobName = string.Format("{0}/Exceptions", RunId);
            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("exceptions", sb.ToString()), true);
            }
            else
            {
                CloudWork.DeleteBlob(summaryContainer, blobName);
            }

            return;
        }

        public void ReadCloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            ReadCloudOverall();
            ReadCloudOnePhase(WorkItemType.Precondition);
            ReadCloudOnePhase(WorkItemType.Average);
            ReadCloudOnePhase(WorkItemType.Peak);
            ReadCloudOnePhase(WorkItemType.Postcondition);
            ReadCloudAPIs();
            ReadCloudExceptions();

            return;
        }

        private void ReadCloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/Overall", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                StartTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "starttime"));
                EndTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "endtime"));
            }

            return;
        }

        private void ReadCloudOnePhase(WorkItemType type)
        {
            string summaryContainer, blobName, content, one, testIdsReport;
            LinkedList<string> testIds;
            ScenarioSummary sum;
            int succeed;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/{1}", RunId, type.ToString());
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "scenarioduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "scenarioduration", i);

                    testIdsReport = ProtocolHelper.GetOneField(one, "testids");
                    testIds = new LinkedList<string>();
                    for (int k = 0; k < ProtocolHelper.GetFieldCount(testIdsReport, "id"); k++)
                    {
                        testIds.AddFirst(ProtocolHelper.GetTimedField(testIdsReport, "id", k));
                    }

                    succeed = int.Parse(ProtocolHelper.GetOneField(one, "succeed"));
                    sum = new ScenarioSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "scenario"),
                        ScenarioSubKey = ProtocolHelper.GetOneField(one, "subkey"),
                        Failed = int.Parse(ProtocolHelper.GetOneField(one, "failed")),
                        NoResouceCount = int.Parse(ProtocolHelper.GetOneField(one, "noresource")),
                        Successed = succeed,
                        ItemType = type,
                        TestInfoIds = testIds,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * succeed,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };

                    string key;
                    Dictionary<WorkItemType, ScenarioSummary> wiss;

                    key = sum.Name + sum.ScenarioSubKey;
                    if (ScenarioList.ContainsKey(key))
                    {
                        wiss = ScenarioList[key];
                    }
                    else
                    {
                        wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                        ScenarioList[key] = wiss;
                    }

                    wiss[type] = sum;
                }
            }

            return;
        }

        private void ReadCloudAPIs()
        {
            string summaryContainer, blobName, content, one;
            ApiSummary sum;
            int count;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/APIs", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "apiduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "apiduration", i);

                    count = int.Parse(ProtocolHelper.GetOneField(one, "count"));
                    sum = new ApiSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "api"),
                        TestCount = count,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * count,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };
                    ApiList.Add(sum.Name, sum);
                }
            }

            return;
        }

        private void ReadCloudExceptions()
        {
            string summaryContainer, blobName, content, one, testIdsReport;
            LinkedList<string> testIds;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/Exceptions", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "exception"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "exception", i);

                    testIdsReport = ProtocolHelper.GetOneField(one, "testids");
                    testIds = new LinkedList<string>();
                    for (int k = 0; k < ProtocolHelper.GetFieldCount(testIdsReport, "id"); k++)
                    {
                        testIds.AddFirst(ProtocolHelper.GetTimedField(testIdsReport, "id", k));
                    }
                    ExceptionList.Add(ProtocolHelper.GetOneField(one, "info"), testIds);
                }
            }

            return;
        }
    }
}
