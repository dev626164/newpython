﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Microsoft.Cis.E2E.QBar
{
    public partial class ReportList : System.Web.UI.Page
    {
        private int Timeout;

        protected void Page_Load(object sender, EventArgs e)
        {
            int year, month;

            Timeout = Server.ScriptTimeout;
            Server.ScriptTimeout = 3600;

            Watcher.Init();

            QBarReportList.Rows.Add(AddTableCells(new object[] { "<b>Months for QBar Runs</b>" }));
            foreach (int runTime in Watcher.GetRunTimeInfo())
            {
                year = runTime / 100;
                month = runTime % 100;
                
                QBarReportList.Rows.AddAt(1, AddTableCells(
                    new object[] { string.Format("<a href =\"Report.aspx?Year={0}&Month={1}\">{0}.{1}</a>", year, month)}));
            }
        }

        private void Page_Unload(object sender, System.EventArgs e)
        {
            Server.ScriptTimeout = Timeout;
        }

        private TableRow AddTableCells(object[] contents)
        {
            TableRow row;
            TableCell cell;

            row = new TableRow();
            foreach (object obj in contents)
            {
                cell = new TableCell();
                cell.Text = obj.ToString();
                row.Cells.Add(cell);
            }

            return row;
        }
    }
}
