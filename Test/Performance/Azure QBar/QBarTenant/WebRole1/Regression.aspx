﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Regression.aspx.cs" Inherits="Microsoft.Cis.E2E.QBar.Regression" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Regression Analysis</title>
    <script type="text/javascript" src="jquery-1.3.2-vsdoc2.js"></script>
    <script type="text/javascript" src="jquery.sparkline.js"></script>

    <script type="text/javascript">
    $(function() {
        /** This code runs when everything has been loaded on the page */
        /* Inline sparklines take their values from the contents of the tag */
        $('.inlinesparkline').sparkline(); 

        /* Sparklines can also take their values from the first argument 
        passed to the sparkline() function */
        var myvalues = [10,8,5,7,4,4,10];
        $('.dynamicsparkline').sparkline(myvalues, {height: '100', width: '500'});

        /* The second argument gives options such as chart type */
        $('.dynamicbar').sparkline(myvalues, { type: 'bar', barColor: 'green', height: '100', width: '500' });

        /* Use 'html' instead of an array of values to pass options 
        to a sparkline with data in the tag */
        $('.inlinebar').sparkline('html', { chartRangeMin: '0', type: 'bar', barColor: 'red', height: '50', width: '800' });
    });
    </script>
    <style type="text/css">

        .style3
        {
            text-align: center;
        }
        .style5
        {
            color: #FFFFFF;
        }
        .style4
        {
            font-size: xx-large;
            background-color: #0000CC;
        }
        .style2
        {
            font-family: "Times New Roman", Times, serif;
            font-size: xx-large;
            color: #FFFFFF;
            background-color: #0000CC;
        }
        </style>
</head>
<body bgcolor="#ccccff">
    <form id="form1" runat="server">
    <div>
    
    <h1 class="style3" style="background-color: #CCCCFF">
        <span class="style5">
        <b class="style4">&nbsp;&nbsp; 
        </b>
        </span>
        <b style="background-color: #66CCFF"><span class="style2">Windows Azure Quality Bar</span></b><span 
            class="style5"><b class="style4"> &nbsp;
        </b></span></h1>
    
    </div>
    <asp:Table ID="QBarRegressionTable" runat="server" Height="26px" Width="900px" 
        BackColor="#CCFFFF" BorderColor="#000099" BorderWidth="2px" 
        Caption="Regression Analysis " CaptionAlign="Top" CellPadding="1" 
        CellSpacing="1" ForeColor="Black" GridLines="Both" BorderStyle="Double" 
        HorizontalAlign="Center">
    </asp:Table>
    </form>
    
    
</body>
</html>
