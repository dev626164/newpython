﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public partial class TestInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string testId;

            if (Request.QueryString == null || Request.QueryString.Count == 0)
            {
                Response.Write(string.Format("TestId does not exist!!"));
                return;
            }

            testId = Request.QueryString["TestId"].ToString();
            if (string.IsNullOrEmpty(testId))
            {
                Response.Write(string.Format("TestId does not exist!!"));
                return;
            }

            this.Title += "-" + testId;
            //Response.Write(string.Format("<div>RUN ID: {0}</div>", testId));

            ShowTestInfo(testId);

            return;
        }

        private void ShowTestInfo(string testId)
        {
            string testInfoContainer, testInfo;

            testInfoContainer = ConfigurationSettings.AppSettings["QualityBarTestInfo"];
            testInfo = CloudWork.GetStringBlob(testInfoContainer, testId);
            if (string.IsNullOrEmpty(testInfo))
            {
                Response.Write("Test information is empty");
                return;
            }

            TestInfoTable.Rows.Add(AddTableCells(new object[] { "<b> TestInfo for " + testId + " </b>" }));
            TestInfoTable.Rows.Add(AddTableCells(new object[] { "<b>Name</b>", "<b>Value</b>"}));

            foreach(Match m in Regex.Matches(testInfo, "<(?'name'.+?)>(?'value'[^\0]*?)</(?'name'.+?)>"))
            {
                TestInfoTable.Rows.Add(AddTableCells(new object[]{m.Groups["name"], m.Groups["value"]}));
            }

            return;
        }

        private TableRow AddTableCells(object[] contents)
        {
            TableRow row;
            TableCell cell;

            row = new TableRow();
            foreach (object obj in contents)
            {
                cell = new TableCell();
                cell.Text = obj.ToString();
                if (contents.Length == 1) cell.ColumnSpan = 9;
                row.Cells.Add(cell);
            }

            row.BorderStyle = BorderStyle.Inset;

            return row;
        }
    }
}
