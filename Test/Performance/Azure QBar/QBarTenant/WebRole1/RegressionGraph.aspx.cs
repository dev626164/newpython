﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public partial class Test : System.Web.UI.Page
    {
        private string QBarRegressionContainer;
        string[][] scenariolabels = { 
            new string[]
            {
                RDFEScenarios.PutDeployment.ToString(), 
                RDFEScenarios.UpgradeDeployment.ToString(),
                RDFEScenarios.UpdateDeploymentConfig.ToString()
                                      
            },
            new string[]
            {
                RDFEScenarios.DeleteDeployment.ToString()
            },
            new string[]
            {
                RDFEScenarios.CreateHostedService.ToString(),
                RDFEScenarios.CreateStorageService.ToString()
            }
                                    };
        string[][] apilabels = {
             new string[]
             {
                 "WaitPromoteDeploymentToProduction"
             }
                             };

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void DynamicPlaceholders()
        {
            int num;

            num = scenariolabels.Length + apilabels.Length;
            for (int i = 0; i < num; i++)
            {
                Response.Write(string.Format("<div id=\"placeholder{0}\" style=\"width:900px;height:300px;\"></div>\r\n", i));
            }

            return;
        }

        protected void DynamicPlotWholeCommand(bool withMinMax)
        {
            int num;

            num = scenariolabels.Length + apilabels.Length;
            for (int i = 0; i < num; i++)
            {
                if (withMinMax)
                {

                    Response.Write(string.Format("$.plot($(\"#placeholder{0}\"), d{0}, {{ lines: {{ show: true }}, points: {{ show: true }}, yaxis: {{min:0}}, xaxis: {{ mode: \"time\", minTickSize: [1, \"day\"], min: min, max: max  }}}});\r\n", i));
                }
                else
                {
                    Response.Write(string.Format("$.plot($(\"#placeholder{0}\"), d{0}, {{ lines: {{ show: true }}, points: {{ show: true }}, yaxis: {{min:0}}, xaxis: {{ mode: \"time\" }}}});\r\n", i));
                }
            }

            return;
        }

        protected void DynamicGraphData()
        {
            CloudWork.SetAccount(ConfigurationSettings.AppSettings["BlobEndpoint"],
                ConfigurationSettings.AppSettings["QueueEndpoint"],
                ConfigurationSettings.AppSettings["TableEndpoint"],
                ConfigurationSettings.AppSettings["E2EAccount"],
                ConfigurationSettings.AppSettings["E2EAccountKey"]);
            QBarRegressionContainer = ConfigurationSettings.AppSettings["QualityBarReportRegressionContainer"];

            string data, t;
            int index;
            
            index = 0;
            foreach (string[] labelset in scenariolabels)
            {
                data = string.Empty;
                foreach(string label in labelset)
                {
                    t = AddScenarioRegressionData(label);
                    if (string.IsNullOrEmpty(t) == false)
                    {
                        if (string.IsNullOrEmpty(data) == false) data += ",";
                        data += t;
                    }
                }
                Response.Write(string.Format("var d{0} = [{1}];\r\n", index, data));
                index ++;
            }

            foreach (string[] labelset in apilabels)
            {
                data = string.Empty;
                foreach(string label in labelset)
                {
                    t = AddAPIRegressionData(label);
                    if (string.IsNullOrEmpty(t) == false)
                    {
                        if (string.IsNullOrEmpty(data) == false) data += ",";
                        data += t;
                    }
                }
                Response.Write(string.Format("var d{0} = [{1}];\r\n", index, data));
                index ++;
            }

            //Response.Write(string.Format("<script id=\"source\" language=\"javascript\" type=\"text/javascript\">\r\n" +
            //    "\t$(function (){{\r\n\t$.plot($(\"#placeholder\"), [ {0} ], {{ xaxis: {{mode: \"time\", timeformat: \"%y/%m/%d\", minTickSize: [10, \"day\"]}}, yaxis: {{ min: 0 }} }} );\r\n\t}});</script>", data));
            

            return;
        }

        string AddScenarioRegressionData(string scenario)
        {
            SortedDictionary<DateTime, double> series;
            string dataseries, content;

            content = string.Empty;
            series = GetSeriesForScenario(scenario);
            if (series.Count != 0)
            {
                dataseries = string.Empty;
                foreach (KeyValuePair<DateTime, double> kvp in series)
                {
                    if (string.IsNullOrEmpty(dataseries) == false) dataseries += ",";
                    dataseries += string.Format("[{0},{1}]", GetJavascriptTimestamp(kvp.Key), kvp.Value);
                }
                content = string.Format("{{label: \"{0}\", data: [{1}]}}", scenario, dataseries);
            }

            return content;
        }

        string AddAPIRegressionData(string api)
        {
            SortedDictionary<DateTime, double> series;
            string dataseries, content;

            content = string.Empty;
            series = GetSeriesForAPI(api);
            if (series.Count != 0)
            {
                dataseries = string.Empty;
                foreach (KeyValuePair<DateTime, double> kvp in series)
                {
                    if(string.IsNullOrEmpty(dataseries) == false) dataseries+=",";
                    dataseries += string.Format("[{0},{1}]", GetJavascriptTimestamp(kvp.Key), kvp.Value);
                }
                content = string.Format("{{label: \"{0}\", data: [{1}]}}", api, dataseries);
            }

            return content;
        }

        SortedDictionary<DateTime, double> GetSeriesForScenario(string scenarioName)
        {
            SortedDictionary<DateTime, double> series, counts;
            LinkedList<string> blobNames;
            string content, name, entity;
            double total, durationSum;
            DateTime time;

            series = new SortedDictionary<DateTime, double>();
            counts = new SortedDictionary<DateTime, double>();
            blobNames = CloudWork.GetBlobNames(QBarRegressionContainer, "regression/summary/scenario/" + scenarioName);
            foreach (string blob in blobNames) //should be one? or multiple with subkeys
            {
                content = GetFromLocalCopy(blob);

                name = ProtocolHelper.GetOneField(content, "name");
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "entity"); i++)
                {
                    entity = ProtocolHelper.GetTimedField(content, "entity", i);

                    time = DateTime.Parse(ProtocolHelper.GetOneField(entity, "timestamp"));
                    total = double.Parse(ProtocolHelper.GetOneField(entity, "total"));
                    durationSum = double.Parse(ProtocolHelper.GetOneField(entity, "durationsum"));

                    if (series.ContainsKey(time) == false)
                    {
                        series.Add(time, 0.0);
                        counts.Add(time, 0.0);
                    }

                    if (total != -1.0)
                    {
                        series[time] += durationSum;
                        counts[time] += total;
                    }
                }
            }

            foreach (KeyValuePair<DateTime, double> kvp in counts)
            {
                if (kvp.Value == 0.0)
                {
                    series[kvp.Key] = -1.0;
                }
                else
                {
                    series[kvp.Key] /= kvp.Value;
                }
            }

            return series;
        }

        SortedDictionary<DateTime, double> GetSeriesForAPI(string api)
        {
            SortedDictionary<DateTime, double> series, counts;
            string blobName;
            string content, name, entity;
            double total, durationSum;
            DateTime time;

            series = new SortedDictionary<DateTime, double>();
            counts = new SortedDictionary<DateTime, double>();

            blobName = "regression/summary/api/" + api;
            content = GetFromLocalCopy(blobName);
            name = ProtocolHelper.GetOneField(content, "name");
            for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "entity"); i++)
            {
                entity = ProtocolHelper.GetTimedField(content, "entity", i);

                time = DateTime.Parse(ProtocolHelper.GetOneField(entity, "timestamp"));
                total = double.Parse(ProtocolHelper.GetOneField(entity, "total"));
                durationSum = double.Parse(ProtocolHelper.GetOneField(entity, "durationsum"));

                if (series.ContainsKey(time) == false)
                {
                    series.Add(time, 0.0);
                    counts.Add(time, 0.0);
                }

                if (total != -1.0)
                {
                    series[time] += durationSum;
                    counts[time] += total;
                }
            }

            foreach (KeyValuePair<DateTime, double> kvp in counts)
            {
                if (kvp.Value == 0.0)
                {
                    series[kvp.Key] = -1.0;
                }
                else
                {
                    series[kvp.Key] /= kvp.Value;
                }
            }

            return series;
        }

        string GetFromLocalCopy(string blob)
        {
            string filename, content, regressionDir;

            regressionDir = ".\\regression";
            if (Directory.Exists(regressionDir) == false) Directory.CreateDirectory(regressionDir);

            content = string.Empty;
            filename = regressionDir + "\\" + blob.Replace('/', '+');
            if (File.Exists(filename))
            {
                content = File.ReadAllText(filename);
                if (string.IsNullOrEmpty(content) == true)
                {
                    content = CloudWork.GetStringBlob(QBarRegressionContainer, blob);
                    File.WriteAllText(filename, content);
                }
            }
            else
            {
                content = CloudWork.GetStringBlob(QBarRegressionContainer, blob);
                File.WriteAllText(filename, content);
            }

            return content;
        }

        public static long GetJavascriptTimestamp(System.DateTime input)
        {
            System.TimeSpan span = new System.TimeSpan(System.DateTime.Parse("1/1/1970").Ticks);
            System.DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }

    }
}
