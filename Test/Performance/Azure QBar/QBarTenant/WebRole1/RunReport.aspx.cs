﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public partial class RunReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string runId;
            QBarReport report;

            runId = Request.QueryString["RunId"].ToString();
            if (string.IsNullOrEmpty(runId))
            {
                Response.Write(string.Format("RunId does not exist: {0}", runId));
                return;
            }

            this.Title += runId;
            //Response.Write(string.Format("<div>RUN ID: {0}</div>", runId));

            report = new QBarReport(runId);
            report.ReadCloudResult();
            ShowOverall(report);
            ShowSummary(report, WorkItemType.Precondition);
            ShowSummary(report, WorkItemType.Average);
            ShowSummary(report, WorkItemType.Peak);
            ShowSummary(report, WorkItemType.Postcondition);
            ShowAPIs(report);
            ShowExceptions(report);

            return;
        }

        private void ShowOverall(QBarReport summary)
        {
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Run ID</b>", summary.RunId }));
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Completeness</b>", summary.IsComplete() }));
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Start Time (UTC)</b>", summary.StartTime }));
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>End Time (UTC)</b>", summary.EndTime }));
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>API Count</b>", summary.ApiList.Count }));
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Scenario Count</b>", summary.ScenarioList.Count }));
            SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Exception Count</b>", summary.ExceptionList.Count }));

            return;
        }

        private void ShowSummary(QBarReport summary, WorkItemType type)
        {
            QBarSummaryTable.Rows.Add(AddTableCells(new object[] { "---" }));
            QBarSummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Phase " + type.ToString() + "</b>" }));
            QBarSummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Scenario</b>", "<b>Subkey</b>", "<b>Failed</b>", "<b>Succeed</b>", "<b>NoResource</b>", "<b>Average</b>", "<b>Median</b>", "<b>Min</b>", "<b>Max</b>" }));
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in summary.ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key != type) continue;

                    QBarSummaryTable.Rows.Add(AddTableCells(new object[] 
                    { 
                        kvp2.Value.Name, 
                        kvp2.Value.ScenarioSubKey,
                        kvp2.Value.Failed, 
                        kvp2.Value.Successed, 
                        kvp2.Value.NoResouceCount, 
                        Math.Round(kvp2.Value.Successed == 0 ? -1.0 : kvp2.Value.DurationSum / kvp2.Value.Successed, 3, MidpointRounding.AwayFromZero), 
                        Math.Round(kvp2.Value.DurationMedian, 3, MidpointRounding.AwayFromZero),
                        Math.Round(kvp2.Value.DurationMin, 3, MidpointRounding.AwayFromZero),
                        Math.Round(kvp2.Value.DurationMax, 3, MidpointRounding.AwayFromZero)
                    }));
                }
            }

            return;
        }

        private void ShowAPIs(QBarReport summary)
        {
            //API, Total, Average(s), Median(s), Min(s), Max(s)
            QBarSummaryTable.Rows.Add(AddTableCells(new object[] { "---" }));
            QBarSummaryTable.Rows.Add(AddTableCells(new object[] { "<b>APIs</b>" }));
            QBarSummaryTable.Rows.Add(AddTableCells(new object[] { "<b>API</b>", "<b>Total</b>", "<b>Average</b>", "<b>Median</b>", "<b>Min</b>", "<b>Max</b>" }));
            foreach (KeyValuePair<string, ApiSummary> apisum in summary.ApiList)
            {
                QBarSummaryTable.Rows.Add(AddTableCells(new object[] 
                { 
                    apisum.Key, 
                    apisum.Value.TestCount, 
                    Math.Round(apisum.Value.DurationSum / apisum.Value.TestCount, 3, MidpointRounding.AwayFromZero), 
                    Math.Round(apisum.Value.DurationMedian, 3, MidpointRounding.AwayFromZero), 
                    Math.Round(apisum.Value.DurationMin, 3, MidpointRounding.AwayFromZero), 
                    Math.Round(apisum.Value.DurationMax, 3, MidpointRounding.AwayFromZero) 
                }));
            }

            return;
        }

        private void ShowExceptions(QBarReport summary)
        {
            string testIdReport;
            int nExpected, nUnexpected;

            nExpected = nUnexpected = 0;
            QBarUnexpectedExceptionTable.Rows.Add(AddTableCells(new object[] { "<b>Exception</b>", "<b>TestInfo</b>" }));
            QBarExpectedExceptionTable.Rows.Add(AddTableCells(new object[] { "<b>Exception</b>", "<b>TestInfo</b>" }));
            foreach (KeyValuePair<string, LinkedList<string>> kvp in summary.ExceptionList)
            {
                testIdReport = string.Empty;
                foreach (string id in kvp.Value)
                {
                    testIdReport += string.Format("<a href=\"TestInfo.aspx?TestId={0}\">{1}</a>, ", id, id);
                }
                if (string.IsNullOrEmpty(testIdReport)) testIdReport = "No test info";

                //exclude expected exceptions
                if (IsExpectedException(kvp.Key))
                {
                    QBarExpectedExceptionTable.Rows.Add(AddTableCells(new object[] { kvp.Key, testIdReport }));
                    nExpected++;
                }
                else
                {
                    QBarUnexpectedExceptionTable.Rows.Add(AddTableCells(new object[] { kvp.Key, testIdReport }));
                    nUnexpected++;
                }
            }

            if (nExpected == 0)
            {
                SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Expected exceptions</b>", nExpected }));
                SummaryTable.Rows.Add(AddTableCells(new object[] { "<b>Unexpected exceptions</b>", nUnexpected}));
            }

            return;
        }

        private bool IsExpectedException(string exceptionInfo)
        {
            bool expected;

            expected = (exceptionInfo.Contains(" is not available")
                    || exceptionInfo.Contains("System.IO.IOException: The process cannot access the file")
                    || exceptionInfo.Contains("FabricNoSettingsChange ")
                    || exceptionInfo.Contains("Microsoft.Samples.ServiceHosting.StorageClient.StorageServerException: Operation could not be completed within the specified time. ---> System.Net.WebException: The remote server returned an error: (500) Internal Server Error.")
                    || exceptionInfo.Contains("has exceeded the allotted timeout of 00:01:00. The time allotted to this operation may have been a portion of a longer timeout."));

            return expected;
        }

        private TableRow AddTableCells(object[] contents)
        {
            TableRow row;
            TableCell cell;

            row = new TableRow();
            foreach (object obj in contents)
            {
                cell = new TableCell();
                cell.Text = obj.ToString();
                if (contents.Length == 1) cell.ColumnSpan = 9;
                row.Cells.Add(cell);
            }

            row.BorderStyle = BorderStyle.Inset;

            return row;
        }
    }
}
