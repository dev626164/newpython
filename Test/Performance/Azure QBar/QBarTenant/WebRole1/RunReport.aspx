﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RunReport.aspx.cs" Inherits="Microsoft.Cis.E2E.QBar.RunReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Windows Azure Qualtiy Bar Report for RunId </title>
    <style type="text/css">

        .style5
        {
            color: #FFFFFF;
            font-family: "Times New Roman", Times, serif;
        }
        .style4
        {
            font-size: xx-large;
            background-color: #0000CC;
        }
        .style2
        {
            font-family: "Times New Roman", Times, serif;
            font-size: xx-large;
            color: #FFFFFF;
            background-color: #0000CC;
        }
        .style6
        {
            text-align: center;
        }
        </style>
</head>
<body bgcolor="#ccccff">
    <form id="form1" runat="server">
    <div>
    
    <h1 class="style6" style="background-color: #CCCCFF">
        <span class="style5">
        <b class="style4">&nbsp;&nbsp; 
        </b>
        </span>
        <b style="background-color: #66CCFF"><span class="style2">Windows Azure Quality Bar 
        Report</span></b><span 
            class="style5"><b class="style4"> &nbsp;
        </b></span></h1>
    </div>
    <br />
    <asp:Table ID="SummaryTable" runat="server" BackColor="Silver" 
        BorderColor="#CC9900" BorderStyle="Double" BorderWidth="4px" Caption="Summary" 
        CaptionAlign="Left" CellPadding="2" CellSpacing="2" ForeColor="#000066" 
        GridLines="Both" HorizontalAlign="Center" Width="900px">
    </asp:Table>
    <br />
    <asp:Table ID="QBarSummaryTable" runat="server" Caption="QBar Report" 
        CaptionAlign="Left" Height="23px" HorizontalAlign="Center" Width="1200px" 
        BackColor="#99FFCC" BorderColor="#000099" BorderWidth="4px" 
        BorderStyle="Double" CellPadding="2" CellSpacing="2" ForeColor="Black" 
        GridLines="Both">
    </asp:Table>
    <p>
        &nbsp;</p>
    <asp:Table ID="QBarUnexpectedExceptionTable" runat="server" Caption="QBar Unexpected Exceptions" 
        CaptionAlign="Left" Height="34px" HorizontalAlign="Center" Width="1200px" 
        BackColor="#FFCC99" BorderColor="Red" BorderWidth="4px" 
        BorderStyle="Double" CellPadding="2" CellSpacing="2" ForeColor="Black" 
        GridLines="Both">
    </asp:Table>
    <br />
    <br />
    <asp:Table ID="QBarExpectedExceptionTable" runat="server" Caption="QBar Expected Exceptions" 
        CaptionAlign="Left" Height="34px" HorizontalAlign="Center" Width="1200px" 
        BackColor="#FFCC99" BorderColor="Red" BorderWidth="4px" 
        BorderStyle="Double" CellPadding="2" CellSpacing="2" ForeColor="Black" 
        GridLines="Both">
    </asp:Table>
    </form>
</body>
</html>
