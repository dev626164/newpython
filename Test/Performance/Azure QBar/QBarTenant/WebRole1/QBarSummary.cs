﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using Microsoft.Cis.E2E.QualityBar;

namespace Microsoft.Cis.E2E.QBar
{
    public class RunInfo
    {
        public DateTime StartTime;
        public DateTime EndTime;
        public string RunId;
    }

    public class Watcher
    {
        static QBarSummary summary;
        
        public static SortedDictionary<DateTime, RunInfo> GetExistingRuns()
        {
            LinkedList<string> activeRunIds;
            string container, activeRunBlobPrefix, runId;
            DateTime startTime, endTime, t;
            Match match;
            SortedDictionary<DateTime, RunInfo> AllRuns;

            AllRuns = new SortedDictionary<DateTime, RunInfo>();
            
            container = ConfigurationSettings.AppSettings["QualityBarReportContainer"];
            activeRunBlobPrefix = ConfigurationSettings.AppSettings["QualityBarReportActiveRunsPrefix"];
            activeRunIds = CloudWork.GetBlobNames(container, activeRunBlobPrefix);
            foreach (string runIdBlobName in activeRunIds)
            {
                //format "RunPrefix/[RunId]"
                runId = runIdBlobName.Substring(activeRunBlobPrefix.Length + 1, runIdBlobName.Length - activeRunBlobPrefix.Length - 1);
                
                startTime = DateTime.MaxValue;
                endTime = DateTime.MinValue;
                foreach(string blob in CloudWork.GetBlobNames(container, runId))
                {
                    match = Regex.Match(blob, string.Format("{0}/(?'date'.*?)$", runId));
                    t = DateTime.Parse(match.Groups["date"].Value.Replace('+', ' '));
                    if (startTime > t) startTime = t;
                    if (endTime < t) endTime = t;
                }

                if (startTime != DateTime.MaxValue)
                {
                    AllRuns.Add(startTime, new RunInfo() { StartTime = startTime, EndTime = endTime, RunId = runId });
                }
            }

            return AllRuns;
        }

        public static void SummarizeQBarReports(string runId)
        {
            LinkedList<string> reports;
            string container;
            
            container = ConfigurationSettings.AppSettings["QualityBarReportContainer"];
            reports = CloudWork.GetBlobNames(container, runId);
            QBarLogger.Log(DebugLevel.INFO, "RunId-{0}: there are {1} reports", runId, reports.Count);

            if (reports.Count != 0)
            {
                summary = new QBarSummary(runId);
                foreach (string r in reports)
                {
                    //QBarLogger.Log(DebugLevel.INFO, "QBarReport: {0}", r);
                    ParseReports(CloudWork.GetStringBlob(container, r));
                }

                summary.Summarize();
                summary.CloudResult();
            }
        
            return;
        }

        public static void ParseReports(string reports)
        {
            string temp;

            for (int i = 0; i < ProtocolHelper.GetFieldCount(reports, "report"); i++)
            {
                temp = ProtocolHelper.GetTimedField(reports, "report", i);
                ParseOneReport(temp);
            }

            return;
        }

        static void ParseOneReport(string report)
        {
            string worker, scenario, subKey, parseResult;
            bool status, isNoResource;
            double duration, apiduration;
            DateTime finishedAt;
            string apiname, exceptionInfo, temp;
            WorkItemType type;

            parseResult = string.Empty;

            worker = ProtocolHelper.GetOneField(report, "worker");
            scenario = ProtocolHelper.GetOneField(report, "scenario");
            type = WorkItemType.Empty;
            temp = ProtocolHelper.GetOneField(report, "qbartype");
            if (temp != string.Empty)
            {
                type = (WorkItemType)Enum.Parse(typeof(WorkItemType), temp);
            }

            subKey = ProtocolHelper.GetOneField(report, "subkey");
            status = bool.Parse(ProtocolHelper.GetOneField(report, "status"));
            duration = double.Parse(ProtocolHelper.GetOneField(report, "duration"));
            finishedAt = DateTime.Parse(ProtocolHelper.GetOneField(report, "finishedat"));
            exceptionInfo = ProtocolHelper.GetOneField(report, "exception");

            if (exceptionInfo != string.Empty)
            {
                parseResult += string.Format("Worker: {0}, {1,-25}, {2,-8}, {3,-8}, {4,-10}, {5,-12}, {6}", 
                    worker, scenario, subKey, type.ToString(), status, duration, finishedAt);
            }

            isNoResource = (exceptionInfo == WarningMessages.DeploymentUnavailable
                || exceptionInfo == WarningMessages.ProductDeploymentUnavailable
                || exceptionInfo == WarningMessages.StagingDeploymentUnavailable
                || exceptionInfo == WarningMessages.FullDeploymentUnavailable
                || exceptionInfo == WarningMessages.HostedServiceUnavailable
                || exceptionInfo == WarningMessages.StorageServiceUnavailable
                || exceptionInfo == WarningMessages.SubscriptionUnavailable
                || exceptionInfo == WarningMessages.StorageAccountUnavailable
                || exceptionInfo == WarningMessages.StorageContainerUnavailable
                || exceptionInfo == WarningMessages.StorageBlobUnavailable);

            if (duration < 0)
            {
                parseResult += string.Format("Duration should be non-negative, but current value is {0}", duration);
                duration = 0;
            }
            summary.AddScenarioEntry(
                scenario, 
                type, 
                subKey, 
                status, 
                duration, 
                finishedAt.AddSeconds(-1 * duration), 
                isNoResource);

            for (int i = 0; i < ProtocolHelper.GetFieldCount(report, "apiperf"); i++)
            {
                temp = ProtocolHelper.GetTimedField(report, "apiperf", i);
                apiname = ProtocolHelper.GetOneField(temp, "name") + " - " + subKey;
                apiduration = double.Parse(ProtocolHelper.GetOneField(temp, "duration"));
                //Console.WriteLine("{0} = {1}", apiname, apiduration);

                summary.AddApiEntry(apiname, apiduration);
            }

            if (exceptionInfo != string.Empty)
            {
                summary.AddExceptionEntry(exceptionInfo);
            }

            return;
        }
    }

    public enum WorkItemType
    {
        Empty,
        Precondition,
        Concurrent,
        Average,
        Peak,
        Postcondition
    }

    public class ApiSummary
    {
        public string Name;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public long TestCount;
        public List<double> DurationList;

        public ApiSummary()
        {
            Name = string.Empty;
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            TestCount = 0;
            DurationList = new List<double>();
        }
    }

    public class ScenarioSummary
    {
        public string Name;
        public long NoResouceCount;
        public long Successed;
        public long Failed;
        public WorkItemType ItemType;
        public string ScenarioSubKey;
        public double DurationSum;
        public double DurationMin;
        public double DurationMax;
        public double DurationMedian;
        public List<double> DurationList;

        public ScenarioSummary()
        {
            Name = string.Empty;
            NoResouceCount = 0;
            Successed = 0;
            Failed = 0;
            ItemType = WorkItemType.Empty;
            ScenarioSubKey = string.Empty;
            DurationSum = 0;
            DurationMin = double.MaxValue;
            DurationMax = double.MinValue;
            DurationMedian = double.MinValue;
            DurationList = new List<double>();
        }
    }

    public class QBarSummary
    {
        public string RunId;
        public DateTime StartTime;
        public DateTime EndTime;
        public Dictionary<string, ApiSummary> ApiList;
        public Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>> ScenarioList;
        public Dictionary<string, int> ExceptionList;

        public QBarSummary(string runid)
        {
            RunId = runid;
            StartTime = DateTime.MaxValue;
            EndTime = DateTime.MinValue;
            ApiList = new Dictionary<string, ApiSummary>();
            ScenarioList = new Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>>();
            ExceptionList = new Dictionary<string, int>();
        }

        public void AddScenarioEntry(string scenario, WorkItemType type, string subKey, bool status, double duration, DateTime startedAt, bool isNoResource)
        {
            Dictionary<WorkItemType, ScenarioSummary> wiss;
            ScenarioSummary ss;
            string key;

            if (StartTime > startedAt) StartTime = startedAt;
            if (EndTime < startedAt.AddMinutes(duration)) EndTime = startedAt;

            key = scenario.ToString() + subKey;
            if (ScenarioList.ContainsKey(key))
            {
                wiss = ScenarioList[key];
                if (wiss.ContainsKey(type))
                {
                    ss = wiss[type];
                }
                else
                {
                    ss = new ScenarioSummary();
                    wiss[type] = ss;
                }
            }
            else
            {
                wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                ScenarioList[key] = wiss;
                ss = new ScenarioSummary();
                wiss[type] = ss;
            }

            ss.Name = scenario;
            ss.ItemType = type;
            ss.ScenarioSubKey = subKey;
            if (status == true)
            {
                ss.DurationSum += duration;
                ss.Successed++;
                if (ss.DurationMax < duration) ss.DurationMax = duration;
                if (ss.DurationMin > duration) ss.DurationMin = duration;

                ss.DurationList.Add(duration);
            }
            else if (isNoResource)
            {
                ss.NoResouceCount++;
            }
            else
            {
                ss.Failed++;
            }

            return;
        }

        public void AddApiEntry(string name, double duration)
        {
            ApiSummary apisum;

            if (ApiList.ContainsKey(name))
            {
                apisum = ApiList[name];
                apisum.DurationSum += duration;
                apisum.TestCount++;
                apisum.DurationList.Add(duration);
            }
            else
            {
                apisum = new ApiSummary()
                {
                    Name = name,
                    DurationSum = duration,
                    TestCount = 1
                };
                apisum.DurationList.Add(duration);
                ApiList[name] = apisum;
            }

            if (apisum.DurationMax < duration) apisum.DurationMax = duration;
            if (apisum.DurationMin > duration) apisum.DurationMin = duration;

            return;
        }

        public void AddExceptionEntry(string exceptionInfo)
        {
            if (ExceptionList.ContainsKey(exceptionInfo) == false)
            {
                ExceptionList.Add(exceptionInfo, 1);
            }
            else
            {
                ExceptionList[exceptionInfo]++;
            }

            return;
        }

        public bool IsComplete()
        {
            //for compute, createsubscription == deletesubscription
            //for storage, CreateStorageAccount == DeleteStorageAccount
            long created, deleted;

            created = deleted = 0;
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key == WorkItemType.Precondition)
                    {
                        if(kvp2.Value.Name == xStoreScenarios.CreateStorageAccount.ToString() 
                            || kvp2.Value.Name == RDFEScenarios.CreateSubscription.ToString())
                        {
                            created += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
                        }
                    }
                    else if (kvp2.Key == WorkItemType.Postcondition)
                    {
                        if (kvp2.Value.Name == xStoreScenarios.DeleteStorageAccount.ToString()
                            || kvp2.Value.Name == RDFEScenarios.DeleteSubscription.ToString())
                        {
                            deleted += kvp2.Value.Failed + kvp2.Value.Successed + kvp2.Value.NoResouceCount;
                        }
                    }
                }
            }

            return (created != 0 && created == deleted);
        }

        public void Summarize()
        {
            int i;

            //find the median value for Scenarios
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Value.DurationList.Count <= 0) continue;

                    kvp2.Value.DurationList.Sort();
                    i = kvp2.Value.DurationList.Count / 2;
                    if (kvp2.Value.DurationList.Count % 2 == 0)
                    {
                        kvp2.Value.DurationMedian = (kvp2.Value.DurationList[i - 1] + kvp2.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        kvp2.Value.DurationMedian = kvp2.Value.DurationList[i];
                    }
                }
            }

            //find the median value for APIs
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                if (apisum.Value.DurationList.Count > 0)
                {
                    apisum.Value.DurationList.Sort();
                    i = apisum.Value.DurationList.Count / 2;
                    if (apisum.Value.DurationList.Count % 2 == 0)
                    {
                        apisum.Value.DurationMedian = (apisum.Value.DurationList[i - 1] + apisum.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        apisum.Value.DurationMedian = apisum.Value.DurationList[i];
                    }
                }
            }

            return;
        }

        public void CloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            CloudOverall();
            CloudOnePhase(WorkItemType.Precondition);
            CloudOnePhase(WorkItemType.Average);
            CloudOnePhase(WorkItemType.Peak);
            CloudOnePhase(WorkItemType.Postcondition);
            CloudAPIs();
            CloudExceptions();

            return;
        }

        private void CloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/Overall", RunId);
            
            content = ProtocolHelper.BuildHeadFields("overall",
                new string[] { "starttime", "endtime", "runid" },
                new object[] { StartTime.ToString(), EndTime.ToString(), RunId});
            CloudWork.PutStringBlob(summaryContainer, blobName, content, true);

            return;
        }

        private void CloudOnePhase(WorkItemType type)
        {
            //startTime, endTime, RunId
            StringBuilder sb;
            string summaryContainer, blobName;
            double median, max, min;

            sb = new StringBuilder();
            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/{1}", RunId, type.ToString());

            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key != type) continue;

                    median = (kvp2.Value.DurationMedian == double.MinValue) ? -1.0 : kvp2.Value.DurationMedian;
                    max = (kvp2.Value.DurationMax == double.MinValue) ? -1.0 : kvp2.Value.DurationMax;
                    min = (kvp2.Value.DurationMin == double.MaxValue) ? -1.0 : kvp2.Value.DurationMin;
                    sb.Append(ProtocolHelper.BuildHeadFields("scenarioduration",
                        new string[]{ "scenario", "subkey", "failed", "noresource", "succeed", "average", "median", "min", "max" },
                        new object[]{ kvp2.Value.Name, kvp2.Value.ScenarioSubKey, kvp2.Value.Failed, kvp2.Value.NoResouceCount, kvp2.Value.Successed,
                            kvp2.Value.Successed == 0 ? -1.0 : kvp2.Value.DurationSum / kvp2.Value.Successed, median, min, max}));
                }
            }

            if (sb.Length != 0)
            {
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField(type.ToString(), sb.ToString()), true);
            }

            return;
        }

        private void CloudAPIs()
        {
            StringBuilder sb;
            string summaryContainer, blobName;
            double median, max, min;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];

            sb = new StringBuilder();
            //API, Total, Average(s), Median(s), Min(s), Max(s)
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                median = (apisum.Value.DurationMedian == double.MinValue) ? -1.0 : apisum.Value.DurationMedian;
                max = (apisum.Value.DurationMax == double.MinValue) ? -1.0 : apisum.Value.DurationMax;
                min = (apisum.Value.DurationMin == double.MaxValue) ? -1.0 : apisum.Value.DurationMin;
                sb.Append(ProtocolHelper.BuildHeadFields("apiduration",
                    new string[]{"api", "count", "average", "median", "min", "max"},
                    new object[]{apisum.Key, apisum.Value.TestCount, apisum.Value.DurationSum/apisum.Value.TestCount, apisum.Value.DurationMedian, apisum.Value.DurationMin, apisum.Value.DurationMax}));
            }
            if (sb.Length != 0)
            {
                blobName = string.Format("{0}/APIs", RunId);
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("apis", sb.ToString()), true);
            }

            return;
        }

        private void CloudExceptions()
        {
            StringBuilder sb;
            string summaryContainer, blobName;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];

            sb = new StringBuilder();
            foreach(KeyValuePair<string, int> kvp in ExceptionList)
            {
                sb.Append(ProtocolHelper.BuildHeadFields("exception",
                    new string[]{"info", "count"},
                    new object[]{kvp.Key, kvp.Value}));
            }

            if (sb.Length != 0)
            {
                blobName = string.Format("{0}/Exceptions", RunId);
                CloudWork.PutStringBlob(summaryContainer, blobName, ProtocolHelper.BuildOneField("exceptions", sb.ToString()), true);
            }

            return;
        }

        public void ReadCloudResult()
        {
            //runid/[Overall | Precondition | Postcondition | Peak | Average | APIs | Exceptions]
            ReadCloudOverall();
            ReadCloudOnePhase(WorkItemType.Precondition);
            ReadCloudOnePhase(WorkItemType.Average);
            ReadCloudOnePhase(WorkItemType.Peak);
            ReadCloudOnePhase(WorkItemType.Postcondition);
            ReadCloudAPIs();
            ReadCloudExceptions();

            return;
        }

        private void ReadCloudOverall()
        {
            //startTime, endTime, RunId
            string summaryContainer, blobName, content;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/Overall", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);
            
            if (string.IsNullOrEmpty(content) == false)
            {
                StartTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "starttime"));
                EndTime = DateTime.Parse(ProtocolHelper.GetOneField(content, "endtime"));
            }

            return;
        }

        private void ReadCloudOnePhase(WorkItemType type)
        {
            string summaryContainer, blobName, content, one;
            ScenarioSummary sum;
            int succeed;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/{1}", RunId, type.ToString());
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "scenarioduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "scenarioduration", i);

                    succeed = int.Parse(ProtocolHelper.GetOneField(one, "succeed"));
                    sum = new ScenarioSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "scenario"),
                        ScenarioSubKey = ProtocolHelper.GetOneField(one, "subkey"),
                        Failed = int.Parse(ProtocolHelper.GetOneField(one, "failed")),
                        NoResouceCount = int.Parse(ProtocolHelper.GetOneField(one, "noresource")),
                        Successed = succeed,
                        ItemType = type,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * succeed,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };

                    string key;
                    Dictionary<WorkItemType, ScenarioSummary> wiss;

                    key = sum.Name + sum.ScenarioSubKey;
                    if (ScenarioList.ContainsKey(key))
                    {
                        wiss = ScenarioList[key];
                    }
                    else
                    {
                        wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                        ScenarioList[key] = wiss;
                    }

                    wiss[type] = sum;
                }
            }

            return;
        }

        private void ReadCloudAPIs()
        {
            string summaryContainer, blobName, content, one;
            ApiSummary sum;
            int count;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/APIs", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "apiduration"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "apiduration", i);

                    count = int.Parse(ProtocolHelper.GetOneField(one, "count"));
                    sum = new ApiSummary()
                    {
                        Name = ProtocolHelper.GetOneField(one, "api"),
                        TestCount = count,
                        DurationList = null,
                        DurationSum = double.Parse(ProtocolHelper.GetOneField(one, "average")) * count,
                        DurationMedian = double.Parse(ProtocolHelper.GetOneField(one, "median")),
                        DurationMax = double.Parse(ProtocolHelper.GetOneField(one, "max")),
                        DurationMin = double.Parse(ProtocolHelper.GetOneField(one, "min"))
                    };
                    ApiList.Add(sum.Name, sum);
                }
            }

            return;
        }

        private void ReadCloudExceptions()
        {
            string summaryContainer, blobName, content, one;

            summaryContainer = ConfigurationSettings.AppSettings["QualityBarReportSummaryContainer"];
            blobName = string.Format("{0}/Exceptions", RunId);
            content = CloudWork.GetStringBlob(summaryContainer, blobName);

            if (string.IsNullOrEmpty(content) == false)
            {
                for (int i = 0; i < ProtocolHelper.GetFieldCount(content, "exception"); i++)
                {
                    one = ProtocolHelper.GetTimedField(content, "exception", i);
                    ExceptionList.Add(ProtocolHelper.GetOneField(one, "info"),
                        int.Parse(ProtocolHelper.GetOneField(one, "count")));
                }
            }

            return;
        }
    }
}
