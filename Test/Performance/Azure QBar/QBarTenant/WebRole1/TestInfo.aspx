﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestInfo.aspx.cs" Inherits="Microsoft.Cis.E2E.QBar.TestInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>QBar Test Information from Test Cases</title>
</head>
<body bgcolor="#ccccff">
    <form id="form1" runat="server">
    <asp:Table ID="TestInfoTable" runat="server" BackColor="#CCFFFF" 
        BorderColor="Lime" BorderStyle="Groove" BorderWidth="4px" 
        Caption="Test Information" CaptionAlign="Left" CellPadding="2" CellSpacing="2" 
        ForeColor="#000099" GridLines="Both" HorizontalAlign="Center" Width="1000px">
    </asp:Table>
    </form>
</body>
</html>
