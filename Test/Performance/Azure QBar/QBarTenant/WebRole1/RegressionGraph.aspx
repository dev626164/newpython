﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegressionGraph.aspx.cs" Inherits="Microsoft.Cis.E2E.QBar.Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Graphs for Regression Analysis</title>
    <!--[if IE]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="jquery-1.3.2-vsdoc2.js"></script>
    <script type="text/javascript" src="jquery.flot.js"></script>
</head>
<body>
    <p>
        <button id="whole">Whole Period</button>
        <button id="lastmonth">Last Month</button>
        <button id="thismonth">This Month</button>
        <button id="nextmonth">Next Month</button>
    </p>
    
    <%--<div id="placeholder" style="width:600px;height:300px;"></div>--%>
    <p>
    <% {DynamicPlaceholders();} %>
    </p>
    
    <script id="source"> 
    var showmonth, showyear;
    showyear = new Date().getFullYear();
    showmonth = new Date().getMonth()+1;
        
    $(function () {
        //var d = [<--% {DynamicGraphData();} %>];
        <% {DynamicGraphData();} %>;
        
        //$.plot($("#placeholder"), d, { xaxis: { mode: "time" }});
        <% {DynamicPlotWholeCommand(false);} %>;
        
 
    $("#whole").click(function () {
        //$.plot($("#placeholder"), d, { xaxis: { mode: "time" } });
        <% {DynamicPlotWholeCommand(false);} %>;
    });
 
    $("#lastmonth").click(function () 
    {
        var min, max, t;
        
        if(showmonth-1 == 0)
        {
            showyear = showyear - 1;
            showmonth = 12;
        }
        else
        {
            showmonth = showmonth - 1;
        }
        
        min = new Date(showyear + "/" + showmonth + "/01").getTime();        
        if(showmonth + 1 == 13)
        {
            t = showyear+1;
            max = new Date(t + "/01/01").getTime();
        }
        else
        {
            t = showmonth+1;
            max = new Date(showyear + "/" + t + "/01").getTime();
        }
        
        //$.plot($("#placeholder"), d, { xaxis: { mode: "time", minTickSize: [1, "day"], min: min, max: max } });
        <% {DynamicPlotWholeCommand(true);} %>;
    });
 
    $("#thismonth").click(function () 
    {
        var min, max, t;
        
        showmonth = new Date().getMonth()+1;
        showyear = new Date().getFullYear();
        
        min = new Date(showyear + "/" + showmonth + "/01").getTime();        
        if(showmonth + 1 == 13)
        {
            t = showyear+1;
            max = new Date(t + "/01/01").getTime();
        }
        else
        {
            t = showmonth+1;
            max = new Date(showyear + "/" + t + "/01").getTime();
        }
        
        //$.plot($("#placeholder"), d, { xaxis: { mode: "time", minTickSize: [1, "day"], min: min, max: max } });
        <% {DynamicPlotWholeCommand(true);} %>;
    });
    
    $("#nextmonth").click(function () 
    {
        var min, max, t;
        
        if(showmonth+1 == 13)
        {
            showyear = showyear + 1;
            showmonth = 1;
        }
        else
        {
            showmonth = showmonth + 1;
        }
        
        min = new Date(showyear + "/" + showmonth + "/01").getTime();        
        if(showmonth + 1 == 13)
        {
            t = showyear+1;
            max = new Date(t + "/01/01").getTime();
        }
        else
        {
            t = showmonth+1;
            max = new Date(showyear + "/" + t + "/01").getTime();
        }
        
        //$.plot($("#placeholder"), d, { xaxis: { mode: "time", minTickSize: [1, "day"], min: min, max: max } });
        <% {DynamicPlotWholeCommand(true);} %>;
    });
});
</script>

</body>
</html>
