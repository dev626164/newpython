﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="Microsoft.Cis.E2E.QBar.Report" EnableSessionState="True" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Windows Azure Quality Bar Reports</title>
    <style type="text/css">
        .style2
        {
            font-family: "Times New Roman", Times, serif;
            font-size: xx-large;
            color: #FFFFFF;
            background-color: #0000CC;
        }
        .style3
        {
            text-align: center;
        }
        .style4
        {
            font-size: xx-large;
            background-color: #0000CC;
        }
        #TextArea1
        {
            height: 450px;
            width: 1020px;
        }
        #form1
        {
            height: 343px;
        }
        .style5
        {
            color: #FFFFFF;
        }
    </style>
</head>
<body bgcolor="#ccccff">
    <div>
    
    <h1 class="style3" style="background-color: #CCCCFF">
        <span class="style5">
        <b class="style4">&nbsp;&nbsp; 
        </b>
        </span>
        <b style="background-color: #66CCFF"><span class="style2">Windows Azure Quality Bar</span></b><span 
            class="style5"><b class="style4"> &nbsp;
        </b></span></h1>
    
    </div>
    <asp:Table ID="QBarRuns" runat="server" Height="26px" Width="900px" 
        BackColor="#CCFFFF" BorderColor="#000099" BorderWidth="2px" 
        Caption="Quality Bar Measurement Results" CaptionAlign="Top" CellPadding="1" 
        CellSpacing="1" ForeColor="Black" GridLines="Both" BorderStyle="Double" 
        HorizontalAlign="Center">
    </asp:Table>
</body>
</html>
