﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using System.IO;

namespace Microsoft.Cis.E2E.QualityBar
{
    class UploadQBarBits
    {
        static void Main(string[] args)
        {
            LinkedList<string> activeRoles;

            //1. get the list of available instances
            Console.WriteLine("Get active roles");
            activeRoles = GetRoles();

            //2. put all executables into cloud
            Console.WriteLine("Cloud qbar executables");
            PutQBarExecutables(string.Empty);
            
            //3. send them update message
            Console.WriteLine("Send update messages");
            SendUpdateMessages(activeRoles);

            return;
        }

        static LinkedList<string> GetRoles()
        {
            string registerContainer, liveMsg;
            DuplexQueue liveQueue;
            LinkedList<string> roles;

            registerContainer = ConfigurationSettings.AppSettings["E2EAppListenerContainer"];

            //clean up the dead roles
            roles = CloudWork.GetBlobNames(registerContainer, "");
            Console.WriteLine("Total live/stale roles: {0}", roles.Count);
            foreach (string r in roles)
            {
                TimeSpan ts;

                ts = DateTime.Now.ToUniversalTime() - CloudWork.GetBlobTime(registerContainer, r);
                //if the role was replaced 2 hours ago, it is stale
                if (ts.TotalHours > 2)
                {
                    CloudWork.DeleteBlob(registerContainer, r);
                    continue;
                }

                liveQueue = new DuplexQueue(r);
                Console.WriteLine("Check role {0}", r);
                liveMsg = liveQueue.DoTransaction(ProtocolHelper.BuildOneField("action", "Live"), 60, 1000);
                if (string.IsNullOrEmpty(liveMsg) || liveMsg != "Online")
                {
                    CloudWork.DeleteBlob(registerContainer, r);
                    CloudWork.DeleteQueue(liveQueue.SendQueue);
                }
                
                CloudWork.DeleteQueue(liveQueue.RecvQueue);
            }

            //get all active roles
            roles = CloudWork.GetBlobNames(registerContainer, "");
            Console.WriteLine("There are {0} active roles", roles.Count);
            return roles;
        }

        static bool PutQBarExecutables(string subDir)
        {
            string qbarBinContainer, currDir, blobPrefix;
            bool success, flag;
            string version, rootDir, fileName, dirName;

            success = true;
            version = ConfigurationSettings.AppSettings["E2EQBarVersion"];
            rootDir = ConfigurationSettings.AppSettings["E2EQBarLocalDir"];
            qbarBinContainer = ConfigurationSettings.AppSettings["E2EQBarBinaryContainer"];

            currDir = rootDir;
            blobPrefix = string.Format("{0}/QBarBin/", version);
            if (string.IsNullOrEmpty(subDir) == false)
            {
                currDir += subDir;
                blobPrefix += subDir.Replace("\\", "/") + "/";
            }

            //blobPrefix = string.Format("{0}/QBarBin/{1}", version, subDir.Replace('\\', '/'));
            //if (blobPrefix.EndsWith("/") == false) blobPrefix += "/";
            foreach (string f in Directory.GetFiles(currDir))
            {
                fileName = new FileInfo(f).Name;
                Console.WriteLine("{0}  => {1}", currDir + "\\" + fileName, blobPrefix + fileName);
                
                flag = CloudWork.PutFileBlob(qbarBinContainer, blobPrefix + fileName, currDir + "\\" + fileName, true);
                if (flag == false) success = false;
            }
            
            foreach (string d in Directory.GetDirectories(currDir))
            {
                dirName = new DirectoryInfo(d).Name;
                flag = PutQBarExecutables(subDir + "\\" + dirName);
                if (flag == false) success = false;
            }

            return success;
        }

        static void SendUpdateMessages(LinkedList<string> activeRoles)
        {
            string updateMsg, updateReply;
            DuplexQueue updateQueue;

            updateMsg = BuildUpdateMessage();
            foreach (string role in activeRoles)
            {
                updateQueue = new DuplexQueue(role);
                updateReply = updateQueue.DoTransaction(updateMsg, 3 * 60, 3000);
                if (updateReply == string.Empty)
                {
                    updateReply = "Cannot get the reply in 5 minutes";
                }
                Console.WriteLine("Update {0}: {1}", updateQueue.SendQueue, updateReply);
            }

            return;
        }

        static string BuildUpdateMessage()
        {
            StringBuilder sb;
            string version, executable, parameters;

            version = ConfigurationSettings.AppSettings["E2EQBarVersion"];
            executable = ConfigurationSettings.AppSettings["E2EQBarCommand"];
            parameters = ConfigurationSettings.AppSettings["E2EQBarParameters"];

            sb = new StringBuilder();
            sb.Append(ProtocolHelper.BuildOneField("action", "Update"));
            sb.Append(ProtocolHelper.BuildHeadFields("QBarBin",
                new string[]{"Version", "Executable", "Params"},
                new string[]{version, executable, parameters}));

            return sb.ToString();
        }
    }
}
