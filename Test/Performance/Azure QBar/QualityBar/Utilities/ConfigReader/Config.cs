﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar.Utilites
{
    class Config
    {
        string LoadFile;
        public Config(string configFile)
        {
            LoadFile = configFile;
            Parse();
        }

        private void Parse()
        {
            XmlReader reader;
            XmlDocument doc;
            XmlNode rootNode, node;

            reader = XmlReader.Create(LoadFile);
            doc = new XmlDocument();
            doc.Load(reader);

            // Get the root node
            rootNode = doc.SelectSingleNode("QualityBar");

            node = rootNode.SelectSingleNode("Common");
            ParseCommon(node);

            node = rootNode.SelectSingleNode("Tenant");
            ParseTenant(node);

            node = rootNode.SelectSingleNode("Precondition");
            ParsePrecondition(node);

            node = rootNode.SelectSingleNode("Workload");
            ParseWorkload(node);

            return;
        }

        private void ParseCommon(XmlNode commonNode)
        {
            XmlNodeList nodes;

            Console.WriteLine("{0}", GetNodeValue(commonNode, "UnitTimeInSeconds", "600"));
            Console.WriteLine("{0}", GetNodeValue(commonNode, "AverageUnitsForMeasure", "1"));
            Console.WriteLine("{0}", GetNodeValue(commonNode, "PeakUnitsForMeasure", "0"));

            nodes = commonNode.SelectNodes("Resource");
            foreach (XmlNode node in nodes)
            {
                Console.WriteLine("{0}", GetNodeValue(node, "Type", ""));
                Console.WriteLine("{0}", GetNodeValue(node, "Name", ""));
                Console.WriteLine("{0}", GetNodeValue(node, "Container", ""));
                Console.WriteLine("{0}", GetNodeValue(node, "Blob", ""));
            }

            return;
        }

        private void ParseTenant(XmlNode tenantNode)
        {
            XmlNode node;

            Console.WriteLine("{0}", GetAttributeValue(tenantNode, "Type", ""));
            Console.WriteLine("{0}", GetAttributeValue(tenantNode, "FriendlyName", ""));

            Console.WriteLine("{0}", GetNodeValue(tenantNode, "AccessPoint", ""));
            Console.WriteLine("{0}", GetNodeValue(tenantNode, "InstanceCount", "1"));
            Console.WriteLine("{0}", GetNodeValue(tenantNode, "ClusterUsableNodes", "1"));
            Console.WriteLine("{0}", GetNodeValue(tenantNode, "Scale", "1.0"));

            node = tenantNode.SelectSingleNode("ComputePackageLocation");
            if (node != null)
            {
                Console.WriteLine("{0}", GetNodeValue(node, "BlobEndpoint", ""));
                Console.WriteLine("{0}", GetNodeValue(node, "AccountName", ""));
                Console.WriteLine("{0}", GetNodeValue(node, "AccountKey", ""));
            }

            return;
        }

        private void ParsePrecondition(XmlNode preconditionNode)
        {
            XmlNodeList nodeList, resourceList;

            nodeList = preconditionNode.SelectNodes("Phase");
            foreach (XmlNode node in nodeList)
            {
                Console.WriteLine("{0}", GetAttributeValue(node, "Index", "0"));

                resourceList = node.SelectNodes("Resource");
                foreach (XmlNode node2 in resourceList)
                {
                    Console.WriteLine("{0}", GetAttributeValue(node2, "Name", ""));
                    Console.WriteLine("{0}", GetNodeValue(node2, "Count", "0"));
                }
            }

            return;
        }

        private void ParseWorkload(XmlNode workloadNode)
        {
            XmlNode concurrentNode;
            XmlNodeList scenarioNodes, parameterNodes;

            concurrentNode = workloadNode.SelectSingleNode("Concurrent");
            Console.WriteLine("{0}", GetNodeValue(concurrentNode, "Total", "0"));
            Console.WriteLine("{0}", GetNodeValue(concurrentNode, "Account", "0"));

            scenarioNodes = workloadNode.SelectNodes("Scenario");
            foreach (XmlNode node in scenarioNodes)
            {
                Console.WriteLine("{0}", GetNodeValue(node, "Name", ""));
                Console.WriteLine("{0}", GetNodeValue(node, "AverageRequestNumber", "1"));
                Console.WriteLine("{0}", GetNodeValue(node, "PeakRequestNumber", "0"));
                Console.WriteLine("{0}", GetNodeValue(node, "DurationInSeconds", "1"));

                parameterNodes = node.SelectNodes("Parameter");
                foreach (XmlNode node2 in parameterNodes)
                {
                    Console.WriteLine("{0}", GetAttributeValue(node2, "Name", ""));
                    Console.WriteLine("{0}", node2.InnerText);
                }
            }
        }



        protected string GetNodeValue(XmlNode parentNode, string nodeName, object defaultValue)
        {
            XmlNode node;
            string value;

            value = defaultValue.ToString();
            node = parentNode.SelectSingleNode(nodeName);
            if (node != null)
            {
                value = node.InnerText;
            }

            return value;
        }

        protected string GetAttributeValue(XmlNode node, string atributeName, object defaultValue)
        {
            string value;
            XmlAttribute attribute;

            attribute = node.Attributes[atributeName];
            if (attribute != null)
            {
                value = attribute.Value;
            }
            else if (defaultValue != null)
            {
                value = defaultValue.ToString();
            }
            else
            {
                throw new Exception(String.Format("The Attribute '{0}' is missing from node '{1}'", atributeName, node.Name));
            }
            return value;
        }
    }
}
