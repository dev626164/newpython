﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Configuration;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class QBarCommunication
    {
        private Dictionary<Thread, string> ThreadList;
        private DuplexQueue ManagerQueue;
        private string StatusQueue;
        private Timer RoleRegisterTimer;
        private string RegisterContainer;
        public QBarCommunication()
        {
            ThreadList = new Dictionary<Thread, string>();
            StatusQueue = string.Empty;
        }

        public void AcceptManagerCommands(string manager)
        {
            bool exit;
            string msg;

            ManagerQueue = new DuplexQueue(manager);
            RegisterContainer = manager.ToLower();
            RoleRegisterTimer = new Timer(new TimerCallback(this.RegisterRole), RegisterContainer, 0, 5 * 60 * 1000);

            exit = false;
            while (!exit)
            {
                //receive the message from manager
                msg = ManagerQueue.RecvMessageOnly(2, 1000);
                if (msg == string.Empty) //no message
                {
                    Thread.Sleep(5000);
                    continue;
                }

                //process the manager command
                if (ProcessOneCommand(msg) == false)
                {
                    exit = true;
                }
            }

            QBarLogger.Log(DebugLevel.INFO, "Azure Quality Bar, exist");

            return;
        }

        private void RegisterRole(object stateInfo)
        {
            string registerContainer;
            bool success;

            success = false;
            registerContainer = (string)stateInfo;
            success = CloudWork.PutStringBlob(registerContainer, ManagerQueue.RecvQueue, "live", true);
            if (success == false)
            {
                QBarLogger.Log(DebugLevel.FATAL, "Failed to register the role");
            }

            return;
        }

        private void ClearDeadThreads()
        {
            LinkedList<Thread> notAlive;

            lock (ThreadList)
            {
                notAlive = new LinkedList<Thread>();
                foreach (Thread t in ThreadList.Keys)
                {
                    if (!t.IsAlive) notAlive.AddFirst(t);
                }
                foreach (Thread t in notAlive)
                {
                    ThreadList.Remove(t);
                }
            }

            return;
        }

        private void AddWorkThread(Thread thread, string masterQueue)
        {
            lock (ThreadList)
            {
                //clear dead threads first
                ClearDeadThreads();

                //add the new thread
                ThreadList.Add(thread, masterQueue);
            }

            return;
        }


        private bool ProcessOneCommand(string cmdMessage)
        {
            ManagerCommands cmd;
            bool success;
            string masterQueue;
            Thread thread;
            QBarServices service;
            string loadConfigFile;
            QBarLoad loadConfig;
            int groupSize;

            success = true;
            cmd = (ManagerCommands)Enum.Parse(typeof(ManagerCommands), ProtocolHelper.GetOneField(cmdMessage, "command"));
            QBarLogger.Log(DebugLevel.INFO, "Master: manager command {0}", cmd);
            switch (cmd)
            {
                case ManagerCommands.RoleRestart:
                    lock (ThreadList)
                    {
                        foreach (Thread t in ThreadList.Keys)
                        {
                            if (t.IsAlive) t.Abort();
                        }
                        ThreadList.Clear();
                    }
                    break;

                case ManagerCommands.AssignMaster:
                    QBarLogger.Log(DebugLevel.INFO, "Assigned master from manager");
                    masterQueue = ProtocolHelper.GetOneField(cmdMessage, "masterqueue");
                    StatusQueue = ProtocolHelper.GetOneField(cmdMessage, "statusqueue");
                    groupSize = int.Parse(ProtocolHelper.GetOneField(cmdMessage, "groupsize"));
                    QBarLogger.Log(DebugLevel.INFO, "Group size {0}", groupSize);
                    ManagerQueue.SendMessageOnly(ProtocolHelper.BuildMultipleFields(
                        new string[] { "request", "responsequeue" },
                        new object[] { RoleRequests.MasterConfirmed.ToString(), ManagerQueue.RecvQueue }));

                    success = GetQBarLoad(ManagerQueue, out service, out loadConfigFile);
                    if (success)
                    {
                        loadConfig = ParseConfigurations(service, loadConfigFile, ManagerQueue);
                        thread = QBarMaster.StartThread(masterQueue, StatusQueue, service, loadConfig, groupSize);
                        AddWorkThread(thread, masterQueue);
                    }
                    break;

                case ManagerCommands.AssignWorker:
                    QBarLogger.Log(DebugLevel.INFO, "Assigned master from manager");
                    masterQueue = ProtocolHelper.GetOneField(cmdMessage, "masterqueue");
                    StatusQueue = ProtocolHelper.GetOneField(cmdMessage, "statusqueue");
                    groupSize = int.Parse(ProtocolHelper.GetOneField(cmdMessage, "groupsize"));
                    ManagerQueue.SendMessageOnly(ProtocolHelper.BuildMultipleFields(
                        new string[] { "request", "responsequeue" },
                        new object[] { RoleRequests.WorkerConfirmed.ToString(), ManagerQueue.RecvQueue }));

                    thread = QBarWorker.StartThread(StatusQueue, masterQueue);
                    AddWorkThread(thread, string.Empty);
                    break;

                case ManagerCommands.GetRoleInfo:
                    string roleInfo;

                    roleInfo = BuildRoleInfo();
                    ManagerQueue.SendMessageOnly(roleInfo);
                    break;

                default:
                    QBarLogger.Log(DebugLevel.ERROR, "Unspecified manager command {0}", cmd.ToString());
                    break;
            }

            QBarLogger.Log(DebugLevel.INFO, "Command {0} is processed", cmd);

            return success;
        }

        private string BuildRoleInfo()
        {
            string roleInfo;
            string machineInfo;

            ClearDeadThreads();

            machineInfo = string.Format("{0}.{1}.{2}",
                System.Net.Dns.GetHostName(),
                Process.GetCurrentProcess().ProcessName,
                Process.GetCurrentProcess().Id);

            roleInfo = ProtocolHelper.BuildMultipleFields(
                new string[] { "request", "responsequeue", "machine", "threadnum", "statusqueue" },
                new object[] { RoleRequests.Information.ToString(), ManagerQueue.RecvQueue, machineInfo, ThreadList.Count, StatusQueue });

            foreach (string master in ThreadList.Values)
            {
                if (master == string.Empty) continue;
                roleInfo += ProtocolHelper.BuildOneField("masterqueue", master);
            }

            return roleInfo;
        }

        private bool GetQBarLoad(DuplexQueue manager, out QBarServices serviceName, out string loadConfigFileName)
        {
            string loadReq, response;
            string service, loadname, container, blob;
            bool success;

            serviceName = QBarServices.Empty;
            loadConfigFileName = string.Empty;
            loadReq = string.Format("<request>{0}</request>", RoleRequests.GetQBarLoad.ToString());
            response = manager.DoTransaction(loadReq, 10 * 60, 300); // 10 minutes

            //QBarLogger.Log(DebugLevel.INFO, "GetQBarLoad: {0}", response);

            success = false;
            if (response != string.Empty)
            {
                service = ProtocolHelper.GetOneField(response, "servicemeasured");
                serviceName = (QBarServices)Enum.Parse(typeof(QBarServices), service);

                loadname = ProtocolHelper.GetOneField(response, "loadname");
                container = ProtocolHelper.GetOneField(response, "container");
                blob = ProtocolHelper.GetOneField(response, "blob");
                QBarLogger.Log(DebugLevel.INFO, "Service {0} with load {1} at {2}/{3}", serviceName, loadname, container, blob);

                loadConfigFileName = string.Format(".\\masterloads\\{0}", loadname);
                if (System.IO.Directory.Exists(".\\masterloads") == false)
                {
                    System.IO.Directory.CreateDirectory(".\\masterloads");
                }
                success = CloudWork.GetFileBlob(container, blob, loadConfigFileName);
            }

            if (success == false)
            {
                QBarLogger.Log(DebugLevel.FATAL, "Cannot get the load");
            }

            return success;
        }

        private QBarLoad ParseConfigurations(QBarServices serviceName, string loadConfigFileName, DuplexQueue manager)
        {
            QBarLoad loadConfig;

            loadConfig = null;
            switch (serviceName)
            {
                case QBarServices.RDFE:
                    loadConfig = new RDFEQBarLoad(loadConfigFileName, manager);
                    break;

                case QBarServices.xStore:
                    loadConfig = new xStoreQBarLoad(loadConfigFileName, manager);
                    break;

                case QBarServices.AirWatch:
                    loadConfig = new AirWatchQBarLoad(loadConfigFileName, manager);
                    break;

                case QBarServices.Empty:
                    break;
            }

            return loadConfig;
        }
    }
}
