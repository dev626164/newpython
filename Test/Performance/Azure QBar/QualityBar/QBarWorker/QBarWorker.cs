﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class QBarWorker
    {
        public static Thread StartThread(string statusQueue, string masterQueue)
        {
            QBarWorker qbw;
            Thread worker;

            qbw = new QBarWorker(statusQueue, masterQueue);
            worker = new Thread(new ThreadStart(qbw.Start));
            worker.Start();

            return worker;
        }

        #region datavariables
        internal string StatusQueue;
        internal DuplexQueue MasterQueue;
        internal QBarServices ServiceName;
        internal QBarLoad LoadConfig;
        internal QBarBase ServiceLoad;
        internal StatusReporter StatusSender;
        #endregion //datavariables

        private QBarWorker(string statusQueue, string masterQueue)
        {
            StatusQueue = statusQueue;
            MasterQueue = new DuplexQueue(masterQueue);
            StatusSender = new StatusReporter(statusQueue, "QBWorker-" + MasterQueue.RecvQueue.Substring(0, 6));

            QBarLogger.Log(DebugLevel.INFO, "Master:{0}; Status:{1}", masterQueue, statusQueue);

            LoadConfig = null;

            return;
        }

        private void Start()
        {
            string start;

            //1. notify master on its liveness and workerqueue
            StatusSender.Send("Notify master about the worker queue");
            NotifyMaster();
            
            //2. wait for master start
            StatusSender.Send("Wait for master start");
            start = MasterQueue.RecvMessageOnly(30 * 60, 10000);
            if (start != "start")
            {
                StatusSender.Send("Cannot get master start within 30 minutes, {0}", start);
                return;
            }
            else
            {
                MasterQueue.SendMessageOnly(
                    ProtocolHelper.BuildOneField("workerreport", WorkerReports.WorkerStarted.ToString()) +
                    ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                    ProtocolHelper.BuildOneField("status", true));
            }

            //3. get load configuration from master
            StatusSender.Send("Get load settings, preconditions, unitloads");
            GetQBarLoads();

            //4. start measure quality bar
            StatusSender.Send("Start measure azure quality");
            StartMeasureQuality();

            StatusSender.Send("Task finished, worker exits");

            return;
        }

        private void NotifyMaster()
        {
            string workerDesc, response;
            
            workerDesc = string.Format("<workerqueue>{0}</workerqueue>", MasterQueue.RecvQueue);            
            response = string.Empty;
            while (true)
            {
                response = MasterQueue.DoTransaction(workerDesc, 2*60, 1000);
                if (response != string.Empty) break;
            }

            if(ProtocolHelper.GetOneField(response, "response") != RoleRequests.WorkerConfirmed.ToString())
            {
                QBarLogger.Log(DebugLevel.ERROR, "response={0}", response);
                QBarLogger.Log(DebugLevel.ERROR, "Cannot send worker message");
            }

            return;
        }

        private void GetQBarLoads()
        {
            bool success;

            //1. receive the settings
            StatusSender.Send("1. Get settings from master");
            success = GetSettings();
            if (success == false)
            {
                StatusSender.Send("Cannot get settings from master");
                return;
            }

            //2. receive the preconditions
            StatusSender.Send("2. Get preconditions from master");
            success = GetPreconditions();
            if (success == false)
            {
                StatusSender.Send("Cannot get preconditions from master");
                return;
            }

            //3. receive the workloads
            StatusSender.Send("3. Get unit loads from master");
            success = GetUnitLoads();
            if (success == false)
            {
                StatusSender.Send("Cannot get unit loads from master");
                return;
            }

            StatusSender.Send("Successfully get all load information from master");

            return;
        }

        private bool GetSettings()
        {
            string response;
            bool succ;

            succ = true;
            response = MasterQueue.RecvMessageOnly(20 * 60, 10000); //20 minutes
            if (response == string.Empty)
            {
                succ = false;
                QBarLogger.Log(DebugLevel.FATAL, "Cannot receive the overall settings");
            }
            else
            {
                ServiceName = (QBarServices)Enum.Parse(typeof(QBarServices), ProtocolHelper.GetHeadedField(response, "tenant", "type"));
                succ = CreateServiceLoadAndConfig(ServiceName);
                if (!succ)
                {
                    QBarLogger.Log(DebugLevel.FATAL, "Cannot create the load configuration");
                }
                else
                {
                    LoadConfig.ParseWorkerSettings(response);
                }

                MasterQueue.SendMessageOnly(
                    ProtocolHelper.BuildOneField("workerreport", WorkerReports.SettingReceived.ToString()) +
                    ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                    ProtocolHelper.BuildOneField("status", succ.ToString())
                    );
            }

            return succ;
        }

        private bool CreateServiceLoadAndConfig(QBarServices service)
        {
            bool success;

            success = true;
            switch (service)
            {
                case QBarServices.RDFE:
                    LoadConfig = new RDFEQBarLoad();
                    ServiceLoad = new RDFEQBar(LoadConfig, MasterQueue, StatusSender);
                    break;

                case QBarServices.xStore:
                    LoadConfig = new xStoreQBarLoad();
                    ServiceLoad = new xStoreQBar(LoadConfig, MasterQueue, StatusSender);
                    break;

                case QBarServices.AirWatch:
                    LoadConfig = new AirWatchQBarLoad();
                    ServiceLoad = new AirWatchQBar(LoadConfig, MasterQueue, StatusSender);
                    break;

                case QBarServices.Empty:
                    QBarLogger.Log(DebugLevel.ERROR, "Service name is unspecified");
                    success = false;
                    break;
            }

            return success;
        }

        private bool GetPreconditions()
        {
            string response;
            bool succ;

            response = MasterQueue.RecvMessageOnly(10 * 60, 10000); //10 minutes
            if (response == string.Empty)
            {
                succ = false;
                QBarLogger.Log(DebugLevel.FATAL, "Cannot receive the precondition information");
            }
            else
            {
                LoadConfig.ParseWorkerPreconditionLoad(response);
                succ = true;
            }

            MasterQueue.SendMessageOnly(
                ProtocolHelper.BuildOneField("workerreport", WorkerReports.PreconditionReceived.ToString()) +
                ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                ProtocolHelper.BuildOneField("status", succ.ToString()));

            return succ;
        }

        private bool GetUnitLoads()
        {
            string response;
            bool succ;

            response = MasterQueue.RecvMessageOnly(20 * 60, 10000); //20 minutes
            if (response == string.Empty)
            {
                succ = false;
                QBarLogger.Log(DebugLevel.FATAL, "Cannot receive the unitload information");
            }
            else
            {
                LoadConfig.ParseWorkerUnitLoad(response);
                succ = true;
            }

            MasterQueue.SendMessageOnly(
                ProtocolHelper.BuildOneField("workerreport", WorkerReports.WorkloadReceived.ToString()) +
                ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                ProtocolHelper.BuildOneField("status", succ.ToString()));

            return succ;
        }

        private void StartMeasureQuality()
        {
            //1. render the preconditions
            StatusSender.Send("Run preconditions");
            RunPreconditions();

            //2. render the workloads
            StatusSender.Send("Run unit loads");
            RunUnitLoads();

            //3. delete all testing resources
            StatusSender.Send("Delete all testing resources");
            RunPostcondition();

            StatusSender.Send("Finished measuring quality bar");

            return;
        }

        private void RunPreconditions()
        {
            ServiceLoad.DoPrecondition();

            MasterQueue.SendMessageOnly(
                ProtocolHelper.BuildOneField("workerreport", WorkerReports.PreconditionFinished.ToString()) +
                ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                ProtocolHelper.BuildOneField("status", "true"));

            return;
        }

        private void RunUnitLoads()
        {
            ServiceLoad.StartStress();

            MasterQueue.SendMessageOnly(
                ProtocolHelper.BuildOneField("workerreport", WorkerReports.WorkloadFinished.ToString()) +
                ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                ProtocolHelper.BuildOneField("status", "true"));

            return;
        }

        private void RunPostcondition()
        {
            ServiceLoad.DoPostcondition();

            MasterQueue.SendMessageOnly(
                ProtocolHelper.BuildOneField("workerreport", WorkerReports.PostconditionFinished.ToString()) +
                ProtocolHelper.BuildOneField("workerqueue", MasterQueue.RecvQueue) +
                ProtocolHelper.BuildOneField("status", "true"));

            return;
        }
    }
}
