﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class QBarSummary
    {
        public class ApiSummary
        {
            public string Name;
            public double DurationSum;
            public double DurationMin;
            public double DurationMax;
            public double DurationMedian;
            public long TestCount;
            public List<double> DurationList;

            public ApiSummary()
            {
                Name = string.Empty;
                DurationSum = 0;
                DurationMin = double.MaxValue;
                DurationMax = double.MinValue;
                DurationMedian = double.MinValue;
                TestCount = 0;
                DurationList = new List<double>();
            }
        }

        public class ScenarioSummary
        {
            public string Name;
            public long NoResouceCount;
            public long Successed;
            public long Failed;
            public WorkItemType ItemType;
            public string ScenarioSubKey;
            public double DurationSum;
            public double DurationMin;
            public double DurationMax;
            public double DurationMedian;
            public List<double> DurationList;
             
            public ScenarioSummary()
            {
                Name = string.Empty;
                NoResouceCount = 0;
                Successed = 0;
                Failed = 0;
                ItemType = WorkItemType.Empty;
                ScenarioSubKey = string.Empty;
                DurationSum = 0;
                DurationMin = double.MaxValue;
                DurationMax = double.MinValue;
                DurationMedian = double.MinValue;
                DurationList = new List<double>();
            }
        }

        public DateTime StartTime;
        public DateTime EndTime;
        public Dictionary<string, ApiSummary> ApiList;
        public Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>> ScenarioList;
        public QBarSummary()
        {
            StartTime = DateTime.MaxValue;
            EndTime = DateTime.MinValue;
            ApiList = new Dictionary<string, ApiSummary>();
            ScenarioList = new Dictionary<string, Dictionary<WorkItemType, ScenarioSummary>>();
        }

        public void AddScenarioEntry(string scenario, WorkItemType type, string subKey, bool status, double duration, DateTime startedAt, bool isNoResource)
        {
            Dictionary<WorkItemType, ScenarioSummary> wiss;
            ScenarioSummary ss;
            string key;

            if (StartTime > startedAt) StartTime = startedAt;
            if (EndTime < startedAt) EndTime = startedAt;

            key = scenario.ToString() + subKey;
            if (ScenarioList.ContainsKey(key))
            {
                wiss = ScenarioList[key];
                if (wiss.ContainsKey(type))
                {
                    ss = wiss[type];
                }
                else
                {
                    ss = new ScenarioSummary();
                    wiss[type] = ss;
                }
            }
            else
            {
                wiss = new Dictionary<WorkItemType, ScenarioSummary>();
                ScenarioList[key] = wiss;
                ss = new ScenarioSummary();
                wiss[type] = ss;
            }

            ss.Name = scenario;
            ss.ItemType = type;
            ss.ScenarioSubKey = subKey;
            if (status == true)
            {
                ss.DurationSum += duration;
                ss.Successed++;
                if (ss.DurationMax < duration) ss.DurationMax = duration;
                if (ss.DurationMin > duration) ss.DurationMin = duration;

                ss.DurationList.Add(duration);
            }
            else if (isNoResource)
            {
                ss.NoResouceCount++;
            }
            else
            {
                ss.Failed++;
            }

            return;
        }

        public void AddApiEntry(string name, double duration)
        {
            ApiSummary apisum;

            if (ApiList.ContainsKey(name))
            {
                apisum = ApiList[name];
                apisum.DurationSum += duration;
                apisum.TestCount++;
                apisum.DurationList.Add(duration);
            }
            else
            {
                apisum = new ApiSummary()
                {
                    Name = name,
                    DurationSum = duration,
                    TestCount = 1
                };
                apisum.DurationList.Add(duration);
                ApiList[name] = apisum;
            }

            if (apisum.DurationMax < duration) apisum.DurationMax = duration;
            if (apisum.DurationMin > duration) apisum.DurationMin = duration;

            return;
        }

        public bool IsComplete()
        {
            bool complete, allFailed;

            allFailed = false;
            complete = false;
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Value.Successed == 0 && kvp2.Value.Failed != 0)
                    {
                        allFailed = true;
                        break;
                    }
                }
                if (allFailed) break;
            }

            if (allFailed == false)
            {
                foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
                {
                    foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                    {
                        if (kvp2.Value.ItemType == WorkItemType.Postcondition)
                        {
                            complete = true;
                            break;
                        }
                    }

                    if (complete) break;
                }
            }

            return complete;
        }

        public void OutputResult(string runId)
        {
            string logFile;
            TextWriter tw;
            int i;

            if (Directory.Exists(@".\log") == false)
            {
                Directory.CreateDirectory(@".\log");
            }
            logFile = @".\log\" + runId +".csv";
            if (File.Exists(logFile)) File.Delete(logFile);
            tw = File.AppendText(logFile);
            
            OutputOnePhase(WorkItemType.Precondition, tw);
            OutputOnePhase(WorkItemType.Average, tw);
            OutputOnePhase(WorkItemType.Peak, tw);
            OutputOnePhase(WorkItemType.Postcondition, tw);

            tw.WriteLine("\r\nAPI, Total, Average(s), Median(s), Min(s), Max(s)");
            foreach (KeyValuePair<string, ApiSummary> apisum in ApiList)
            {
                if (apisum.Value.DurationList.Count > 0)
                {
                    apisum.Value.DurationList.Sort();
                    i = apisum.Value.DurationList.Count / 2;
                    if (apisum.Value.DurationList.Count % 2 == 0)
                    {
                        apisum.Value.DurationMedian = (apisum.Value.DurationList[i - 1] + apisum.Value.DurationList[i]) / 2;
                    }
                    else
                    {
                        apisum.Value.DurationMedian = apisum.Value.DurationList[i];
                    }
                }

                tw.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}", apisum.Key, apisum.Value.TestCount, 
                    apisum.Value.DurationSum / apisum.Value.TestCount, apisum.Value.DurationMedian, 
                    apisum.Value.DurationMin, apisum.Value.DurationMax);
            }

            tw.Close();

            return;
        }

        public void OutputOnePhase(WorkItemType phaseName, TextWriter writer)
        {
            int i;

            writer.WriteLine("Phase: {0}", phaseName.ToString());
            writer.WriteLine("Scenario, SubKey, Failed, NoResource, Successed, Average(s), Median(s), Min(s), Max(s)");
            foreach (KeyValuePair<string, Dictionary<WorkItemType, ScenarioSummary>> kvp in ScenarioList)
            {
                foreach (KeyValuePair<WorkItemType, ScenarioSummary> kvp2 in kvp.Value)
                {
                    if (kvp2.Key != phaseName) continue;

                    if (kvp2.Value.DurationList.Count > 0)
                    {
                        kvp2.Value.DurationList.Sort();
                        i = kvp2.Value.DurationList.Count / 2;
                        if (kvp2.Value.DurationList.Count % 2 == 0)
                        {
                            kvp2.Value.DurationMedian = (kvp2.Value.DurationList[i - 1] + kvp2.Value.DurationList[i]) / 2;
                        }
                        else
                        {
                            kvp2.Value.DurationMedian = kvp2.Value.DurationList[i];
                        }
                    }

                    writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8}", kvp2.Value.Name, kvp2.Value.ScenarioSubKey,
                        kvp2.Value.Failed, kvp2.Value.NoResouceCount, kvp2.Value.Successed,
                        kvp2.Value.Successed == 0 ? -1.0 : kvp2.Value.DurationSum / kvp2.Value.Successed,
                        kvp2.Value.DurationMedian, kvp2.Value.DurationMin, kvp2.Value.DurationMax);
                }
            }
            writer.WriteLine("\r\n");

            return;
        }
    }
}
