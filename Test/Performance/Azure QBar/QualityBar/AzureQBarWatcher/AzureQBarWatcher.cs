﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Threading;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class AzureQBarWatcher
    {
        static QBarSummary summary;
        static bool DeleteIncomplete;
        static void Main(string[] args)
        {
            LinkedList<string> reports, activeRunIds;
            string container, activeRunBlobPrefix, runId;

            DeleteIncomplete = (args != null && args.Length > 0 && args[0].ToLower() == "delete");
            
            container = ConfigurationSettings.AppSettings["QualityBarReportContainer"];
            activeRunBlobPrefix = ConfigurationSettings.AppSettings["QualityBarReportActiveRunsPrefix"];
            activeRunIds = CloudWork.GetBlobNames(container, activeRunBlobPrefix);
            foreach (string runIdBlobName in activeRunIds)
            {
                //format "RunPrefix/[RunId]"
                runId = runIdBlobName.Substring(activeRunBlobPrefix.Length + 1, runIdBlobName.Length - activeRunBlobPrefix.Length - 1);
                if (File.Exists(@".\log\" + runId + ".csv")) continue;

                reports = CloudWork.GetBlobNames(container, runId);
                QBarLogger.Log(DebugLevel.INFO, "RunId-{0}: there are {1} reports", runId, reports.Count);

                if (reports.Count != 0)
                {
                    summary = new QBarSummary();
                    foreach (string s in reports)
                    {
                        QBarLogger.Log(DebugLevel.INFO, "QbarReport: {0}", s);
                        ParseReports(CloudWork.GetStringBlob(container, s));
                    }
                    summary.OutputResult(runId);
                }
                if (DeleteIncomplete && (reports.Count == 0 || summary.IsComplete() == false))
                {
                    DeleteRunInfo(container, runIdBlobName, reports);
                }
            }

            return;
        }

        static void DeleteRunInfo(string container, string runIdBlob, LinkedList<string> reports)
        {
            CloudWork.DeleteBlob(container, runIdBlob);

            if (reports != null && reports.Count > 0)
            {
                foreach (string s in reports)
                {
                    CloudWork.DeleteBlob(container, s);
                }
            }

            return;
        }

        static void ParseReports(string reports)
        {
            string temp;

            for (int i = 0; i < ProtocolHelper.GetFieldCount(reports, "report"); i++)
            {
                temp = ProtocolHelper.GetTimedField(reports, "report", i);
                ParseOneReport(temp);
                //Console.WriteLine("\r\n{0}", temp);
            }

            return;
        }

        static void ParseOneReport(string report)
        {
            string worker;
            string scenario;
            string subKey;
            bool status, isNoResource;
            double duration;
            DateTime finishedAt;
            string apiname, exceptionInfo;
            double apiduration;
            string temp;
            WorkItemType type;

            worker = ProtocolHelper.GetOneField(report, "worker");
            scenario = ProtocolHelper.GetOneField(report, "scenario");

            type = WorkItemType.Empty;
            temp = ProtocolHelper.GetOneField(report, "qbartype");
            if (temp != string.Empty)
            {
                type = (WorkItemType)Enum.Parse(typeof(WorkItemType), temp);
            }

            subKey = ProtocolHelper.GetOneField(report, "subkey");
            status = bool.Parse(ProtocolHelper.GetOneField(report, "status"));
            duration = double.Parse(ProtocolHelper.GetOneField(report, "duration"));
            finishedAt = DateTime.Parse(ProtocolHelper.GetOneField(report, "finishedat"));
            exceptionInfo = ProtocolHelper.GetOneField(report, "exception");

            if (exceptionInfo != string.Empty)
            {
                QBarLogger.Log(DebugLevel.EXCEPTION, "Worker: {0}, {1,-25}, {2,-8}, {3,-8}, {4,-10}, {5,-12}, {6}", worker, scenario, subKey, type.ToString(), status, duration, finishedAt);
            }

            isNoResource = (exceptionInfo == WarningMessages.DeploymentUnavailable
                || exceptionInfo == WarningMessages.ProductDeploymentUnavailable
                || exceptionInfo == WarningMessages.StagingDeploymentUnavailable
                || exceptionInfo == WarningMessages.FullDeploymentUnavailable
                || exceptionInfo == WarningMessages.HostedServiceUnavailable
                || exceptionInfo == WarningMessages.StorageServiceUnavailable
                || exceptionInfo == WarningMessages.SubscriptionUnavailable
                || exceptionInfo == WarningMessages.StorageAccountUnavailable
                || exceptionInfo == WarningMessages.StorageContainerUnavailable
                || exceptionInfo == WarningMessages.StorageBlobUnavailable);
            
            if (duration < 0) duration = 0;
            summary.AddScenarioEntry(scenario, type, subKey, status, duration, finishedAt.AddSeconds(-1 * duration), isNoResource);
            
            for (int i = 0; i < ProtocolHelper.GetFieldCount(report, "apiperf"); i++)
            {
                temp = ProtocolHelper.GetTimedField(report, "apiperf", i);
                apiname = ProtocolHelper.GetOneField(temp, "name") + " - " + subKey;
                apiduration = double.Parse(ProtocolHelper.GetOneField(temp, "duration"));
                //Console.WriteLine("{0} = {1}", apiname, apiduration);

                summary.AddApiEntry(apiname, apiduration);
                if (exceptionInfo != string.Empty)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, "    1. API Name: {0}, Duration {1}", apiname, apiduration);
                }
            }

            if (exceptionInfo != string.Empty)
            {
                QBarLogger.Log(DebugLevel.EXCEPTION, "{0}", exceptionInfo);
            }

            return;
        }
    }
}
