﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public enum QBarServices
    {
        Empty,
        RDFE,
        xStore,
        AirWatch
    }

    public enum ManagerCommands
    {
        AssignMaster,
        AssignWorker,
        RoleRestart,
        GetRoleInfo
    }

    public enum RoleRequests
    {
        GetQBarLoad,
        WorkerCollectionFinished,
        Information,
        Smoking,
        WorkerConfirmed,
        MasterConfirmed
    }

    public enum StressSyncMessages
    {
        Empty,
        PreconditionFinished,
        ConcurrentLoadFinished,
        AverageLoadFinished,
        PeakLoadFinished,
        PostconditionFinished
    }

    public class WarningMessages
    {
        public static readonly string UnknownError = "The exception is not expected.";
        public static readonly string FullDeploymentUnavailable = "Full deployment account is not available";
        public static readonly string ProductDeploymentUnavailable = "Product deployment is not available";
        public static readonly string StagingDeploymentUnavailable = "Staging deployment is not available";
        public static readonly string DeploymentUnavailable = "Deployment is not available";
        public static readonly string HostedServiceUnavailable = "Hosted service account is not available";
        public static readonly string StorageServiceUnavailable = "Storage service account is not available";
        public static readonly string SubscriptionUnavailable = "Subscription is not available";
        public static readonly string StorageAccountUnavailable = "Storage account is not available";
        public static readonly string StorageContainerUnavailable = "Storage container is not available";
        public static readonly string StorageBlobUnavailable = "Storage blob is not available";
    }

    public enum ResourceTypes
    {
        Subscription,
        ComputePackage,
        CertFile,
        StorageAccount,
        ComputeAccount,
        ComputeDeployment,
        StorageContainer,
        StorageBlob
    }

    public enum ResourceStatus
    {
        Idle,
        Investigating,
        Deleting,
        Deploying,
        Updating,
        Upgrading,
        ContainerCreating,
        BlobPutting,
        BlobGetting
    }
        
    public enum RDFEScenarios
    {
        Empty,
        CreateSubscription,
        CreateHostedService,
        CreateStorageService,
        DeleteSubscription,
        DeleteHostedService,
        DeleteStorageService,
        PutDeployment,
        DeleteDeployment,
        UpgradeDeployment,
        UpdateDeploymentConfig
    }

    public enum xStoreScenarios
    {
        Empty,
        CreateContainer,
        CreateStorageAccount,
        DeleteBlob,
        DeleteContainer,
        DeleteStorageAccount,
        GetBlob,
        PutBlob
    }

    public enum AirWatchScenarios
    {
        Empty,
        AirWatchTextService,
        AirWatchSearch,
        AirWatchGetPushPin,
        AirWatchGetClosestStation,
        AirWatchGetStationByCode,
        AirWatchGetStationDescriptionsForMapBoundaries,
        AirWatchSetRating,
        AirWatchGetLanguages,
        AirWatchGetLanguage,
        AirWatchDoWork,
        AirWatchDataEntry
    }

    public enum WorkerReports
    {
        Empty,

        WorkerStarted,
        
        SettingReceived,
        PreconditionReceived,
        WorkloadReceived,
        
        PreconditionFinished,
        WorkloadFinished,
        PostconditionFinished,
        
        //ResourceCreated,
        //ResourceDeleted,
        
        //WorkItemFinished,
        //WorkItemException
    }

    public enum WorkItemType
    {
        Empty,
        Precondition,
        Concurrent,
        Average,
        Peak,
        Postcondition
    }

    public enum WorkItemExpectedException
    {
        NoSubscription,
        NoStorageAccount,
        NoComputeAccount,
        NoDeployment
    }
}
