﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class StatusReporter
    {
        internal string StatusQueue;
        internal string StatusPrefix;

        public StatusReporter(string statusQueue, string prefix)
        {
            StatusQueue = statusQueue;
            StatusPrefix = prefix;
        }

        public void Send(string format, params object[] args)
        {
            string status;

            status = StatusPrefix + ": " + string.Format(format, args);
            CloudWork.SendMessage(StatusQueue, status);

            return;
        }
    }
}
