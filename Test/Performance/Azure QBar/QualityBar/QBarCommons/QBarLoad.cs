﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Threading;

namespace Microsoft.Cis.E2E.QualityBar
{
    public abstract class QBarLoad
    {
        internal string LoadFile;
        public int WorkerSize;
        public int AllocatedWorkerSize;

        public QBarLoad()
        {
            LoadFile = string.Empty;
            WorkerSize = AllocatedWorkerSize = -1;
        }
        
        public QBarLoad(string loadfile, DuplexQueue manager)
        {
            WorkerSize = 0;
            AllocatedWorkerSize = 0;
            LoadFile = loadfile;
            Parse(manager);
        }

        private void Parse(DuplexQueue manager)
        {
            XmlReader reader;
            XmlDocument doc;
            XmlNode rootNode, node;

            reader = XmlReader.Create(LoadFile);
            doc = new XmlDocument();
            doc.Load(reader);

            // Get the root node
            rootNode = doc.SelectSingleNode("QualityBar");

            node = rootNode.SelectSingleNode("Common");
            ParseCommon(node);

            node = rootNode.SelectSingleNode("Tenant");
            ParseTenant(node);

            node = rootNode.SelectSingleNode("Precondition");
            ParsePrecondition(node);

            node = rootNode.SelectSingleNode("Workload");
            ParseWorkload(node);

            return;
        }

        //parse the load
        protected abstract void ParseCommon(XmlNode nodeCommon);
        protected abstract void ParseTenant(XmlNode nodeTenant);
        protected abstract void ParsePrecondition(XmlNode nodePrecondition);
        protected abstract void ParseWorkload(XmlNode nodeWorkload);

        //get the resources from manager
        public abstract string GetLoadSettings();
        public abstract void ParseWorkerSettings(string response);

        //partition the load into smaller piece
        public abstract string NextPreconditionLoad();
        public abstract void ParseWorkerPreconditionLoad(string response);
        public abstract string NextUnitLoad();
        public abstract void ParseWorkerUnitLoad(string response);

        protected int LoadSplit(int total, int groupSize, int allocatedWorkers)
        {
            int count, unit, allocated;

            unit = (int)Math.Ceiling(total * 1.0 / groupSize);
            allocated = unit * allocatedWorkers;
            if (allocated >= total)
            {
                count = 0;
            }
            else if (allocated + unit >= total)
            {
                count = total - allocated;
            }
            else
            {
                count = unit;
            }

            return count;
        }

        protected string GetNodeValue(XmlNode parentNode, string nodeName, object defaultValue)
        {
            XmlNode node;
            string value;

            value = defaultValue.ToString();
            node = parentNode.SelectSingleNode(nodeName);
            if (node != null)
            {
                value = node.InnerText;
            }

            return value;
        }
        protected string GetAttributeValue(XmlNode node, string atributeName, object defaultValue)
        {
            string value;
            XmlAttribute attribute;
            
            attribute = node.Attributes[atributeName];
            if (attribute != null)
            {
                value = attribute.Value;
            }
            else if (defaultValue != null)
            {
                value = defaultValue.ToString();
            }
            else
            {
                throw new Exception(String.Format("The Attribute '{0}' is missing from node '{1}'", atributeName, node.Name));
            }
            return value;
        }
    }

    
}
