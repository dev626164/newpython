﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace Microsoft.Cis.E2E.QualityBar
{
     /// <summary>
    /// This is the base class from which all classes which encapsulate a qbar measurement are derived
    /// </summary>
    public class QBarResults
    {
        public class WorkerActionStatus
        {
            public WorkerReports reportKind;
            public bool success;
        }

        public StatusReporter StatusSender;
        public Dictionary<string, WorkerActionStatus> SettingsReports, PreconditionReports, WorkloadReports, PostconditionReports;

        public QBarResults(StatusReporter statusSender)
        {
            StatusSender = statusSender;

            SettingsReports = new Dictionary<string, WorkerActionStatus>();
            PreconditionReports = new Dictionary<string, WorkerActionStatus>();
            WorkloadReports = new Dictionary<string, WorkerActionStatus>();
            PostconditionReports = new Dictionary<string, WorkerActionStatus>();
        }

        //public abstract bool WorkerReport(string report);
        public bool WorkerReport(string report)
        {
            WorkerReports reportKind;
            string workQueue;
            bool status, success;

            reportKind = (WorkerReports)Enum.Parse(typeof(WorkerReports), ProtocolHelper.GetOneField(report, "workerreport"));
            workQueue = ProtocolHelper.GetOneField(report, "workerqueue");
            status = bool.Parse(ProtocolHelper.GetOneField(report, "status"));

            success = true;
            switch (reportKind)
            {
                case WorkerReports.WorkerStarted:
                    AddSettingsReport(workQueue, reportKind, status);
                    break;

                case WorkerReports.SettingReceived:
                    AddSettingsReport(workQueue, reportKind, status);
                    break;

                case WorkerReports.PreconditionReceived:
                    AddPreconditionReport(workQueue, reportKind, status);
                    break;

                case WorkerReports.PreconditionFinished:
                    AddPreconditionReport(workQueue, reportKind, status);
                    break;

                case WorkerReports.WorkloadReceived:
                    AddWorkloadReport(workQueue, reportKind, status);
                    break;

                case WorkerReports.WorkloadFinished:
                    AddWorkloadReport(workQueue, reportKind, status);
                    break;

                //case WorkerReports.WorkItemFinished:
                //    ReportWorkItemResult(report);
                //    break;

                //case WorkerReports.WorkItemException:
                //    ReportWorkItemException(report);
                //    break;

                case WorkerReports.PostconditionFinished:
                    AddWorkloadReport(workQueue, reportKind, status);
                    break;

                default:
                    success = false;
                    QBarLogger.Log(DebugLevel.WARN, "Does not support report: {0}", report);
                    break;
            }

            //QBarLogger.Log(DebugLevel.INFO, "Report: {0}", report);
            return success;
        }

        public void AddSettingsReport(string worker, WorkerReports report, bool status)
        {
            SettingsReports[worker] = new WorkerActionStatus() { reportKind=report, success=status };
            return;
        }
        public void AddPreconditionReport(string worker, WorkerReports report, bool status)
        {
            PreconditionReports[worker] = new WorkerActionStatus() { reportKind = report, success = status };
            return;
        }
        public void AddWorkloadReport(string worker, WorkerReports report, bool status)
        {
            WorkloadReports[worker] = new WorkerActionStatus() { reportKind = report, success = status };
            return;
        }
        public void AddPostconditionReport(string worker, WorkerReports report, bool status)
        {
            PostconditionReports[worker] = new WorkerActionStatus() { reportKind=report,success=status };
            return;
        }

        public bool WaitWorkerReport(Dictionary<string, WorkerActionStatus> recvList, string item, WorkerReports report, int waitInSeconds)
        {
            Stopwatch watch;
            bool succ;

            succ = false;
            watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalSeconds < waitInSeconds)
            {
                if (recvList.ContainsKey(item) && recvList[item].reportKind == report)
                {
                    succ = recvList[item].success;
                    break;
                }
                Thread.Sleep(200);
            }

            if (succ == false)
            {
                QBarLogger.Log(DebugLevel.FATAL, "The worker action failed");
            }

            return succ;
        }
    }
}
