﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public abstract class QBarBase
    {
        protected QBarServices ServiceName;
        protected QBarWorkItemContainer ItemsContainer;
        protected StatusReporter StatusSender;
        protected DuplexQueue MasterQueue;

        public QBarBase(QBarServices service, DuplexQueue masterQueue, StatusReporter reporter)
        {
            MasterQueue = masterQueue;
            StatusSender = reporter;
            ServiceName = service;
            ItemsContainer = new QBarWorkItemContainer();
        }

        public abstract void DoPrecondition();
        public abstract void StartStress();
        public abstract void DoPostcondition(); //cleanup
    }
}
