﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Text;
using System.IO;
using System.Configuration;

namespace Microsoft.Cis.E2E.QualityBar
{
    public enum DebugLevel { DEBUG, INFO, WARN, ERROR, FATAL, EXCEPTION };

    //Logger Class 
    public static class QBarLogger
    {
        public static bool WriteToConsole;
        static string LogDir;
        static string FileName;
        static DateTime FileTime;
        const int LogFileInterval = 3600;   // In seconds

        static QBarLogger()
        {
            try
            {
                FileName = string.Empty;
                WriteToConsole = true;

                LogDir = @".\QBarLogs";
                if (Directory.Exists(LogDir) == false)
                {
                    Directory.CreateDirectory(LogDir);
                }
            }
            catch (Exception e)
            {
                QBarLogger.Log(DebugLevel.EXCEPTION, e.ToString());
            }
        }

        public static void Log(DebugLevel debugLevel, string format, params object[] args)
        {
            try
            {
                // Generate the log text
                string trace;

                trace = String.Format("{0} [{1,-3}]: {2}\r\n",
                    DateTime.Now.ToString("yyyyMMdd-HHmmss"), Thread.CurrentThread.ManagedThreadId, String.Format(format, args));

                if (WriteToConsole)
                {
                    Console.Write(trace);
                }

                // The log file will have the entire log text
                lock (typeof(QBarLogger))
                {
                    File.AppendAllText(GetLogFileName(), trace);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return;
        }

        static string GetLogFileName()
        {
            if (FileName == string.Empty || (DateTime.Now - FileTime).TotalSeconds > LogFileInterval)
            {
                if (FileName == string.Empty)
                {
                    FileTime = DateTime.Now;
                }
                else
                {
                    //self-cleaning
                    CloudWork.PutFileBlob("qbarlogs", FileTime.ToString()+Guid.NewGuid().ToString("N"), FileName, true);
                    File.Delete(FileName);

                    FileTime = FileTime.AddSeconds(LogFileInterval);
                }

                FileName = string.Format(@"{0}\{1}-{2}.log", LogDir, Process.GetCurrentProcess().Id, FileTime.ToString("yyyyMMdd_HHmmss"));
            }

            return FileName;
        }
    }
}
