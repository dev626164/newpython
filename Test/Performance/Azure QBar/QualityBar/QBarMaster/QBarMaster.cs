﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class QBarMaster
    {
        public static Thread StartThread(string masterQueue, string statusQueue, QBarServices serviceName, QBarLoad loadConfig, int groupSize)
        {
            QBarMaster qbm;
            Thread master;

            qbm = new QBarMaster(masterQueue, statusQueue, serviceName, loadConfig, groupSize);
            master = new Thread(new ThreadStart(qbm.Start));
            master.Start();

            return master;
        }

        #region datavariables
        internal string StatusQueue;
        internal string MasterQueue;
        internal Dictionary<DuplexQueue, string> Workers;
        internal int WorkerGroupSize;
        internal QBarServices ServiceName;
        internal QBarLoad LoadConfig;
        internal QBarResults Result;
        internal StatusReporter StatusSender;
        private bool OnStress;
        #endregion //datavariables

        private QBarMaster(string masterQueue, string statusQueue, QBarServices serviceName, QBarLoad loadConfig, int groupSize)
        {
            StatusQueue = statusQueue;
            StatusSender = new StatusReporter(statusQueue, "QBMaster-" + masterQueue.Substring(0, 6));

            MasterQueue = masterQueue;
            ServiceName = serviceName;
            LoadConfig = loadConfig;
            WorkerGroupSize = groupSize;
            Result = null;
            OnStress = true;

            Workers = new Dictionary<DuplexQueue, string>();

            return;
        }

        private void Start()
        {
            Thread workStatus;

            //1. manage the physical resources (account, container, blob, queue, table, deployment etc.)
            //listen on the message queues and get the resource created/deleted
            StatusSender.Send("Create result management for workers");
            Result = new QBarResults(StatusSender);

            //2. workers list
            StatusSender.Send("Collect running workers");
            Workers = GetRunningWorkers(MasterQueue, WorkerGroupSize);

            //3. allocate load to workers
            StatusSender.Send("Partition the overall quality bar workload for all workers");
            LoadConfig.WorkerSize = Workers.Count;

            //4. start a thread for result collection
            workStatus = new Thread(new ThreadStart(ProcessWorkerReports));
            workStatus.Start();

            //5. Tell workers to start work
            StartWorkers();

            //6. send the worker loads
            SendWorkerLoad();

            //7. do the measurement of azure quality bar
            StartMeasureQuality();

            return;
        }

        private Dictionary<DuplexQueue, string> GetRunningWorkers(string masterQueue, int groupSize)
        {
            string workerDesc, workerQueue;
            Dictionary<DuplexQueue, string> workerList;
            bool workerExist;
            Stopwatch watch;

            workerList = new Dictionary<DuplexQueue, string>();
            watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalMinutes < 20) //20 minutes
            {
                workerDesc = CloudWork.GetAndDeleteOneMessageStr(masterQueue);
                if (workerDesc == string.Empty)
                {
                    Thread.Sleep(5000);
                    continue;
                }

                //QBarLogger.Log(DebugLevel.INFO, "Worker: {0}", workerDesc);
                workerQueue = ProtocolHelper.GetOneField(workerDesc, "workerqueue");
                CloudWork.SendMessage(workerQueue, ProtocolHelper.BuildOneField("response", RoleRequests.WorkerConfirmed.ToString()));

                workerExist = false;
                foreach (KeyValuePair<DuplexQueue, string> kvp in workerList)
                {
                    if (kvp.Key.SendQueue == workerQueue)
                    {
                        workerExist = true;
                        break;
                    }
                }
                if (workerExist == false)
                {
                    workerList.Add(new DuplexQueue(workerQueue, masterQueue), string.Empty);
                    if (workerList.Count == groupSize - 1) break;
                }
                Thread.Sleep(500);
            }

            StatusSender.Send("There are {0} workers", workerList.Count);

            return workerList;
        }

        private void StartWorkers()
        {
            bool status;
            
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                StatusSender.Send("Start worker {0}", kvp.Key.SendQueue);
                status = kvp.Key.SendMessageOnly("start");
                if (!status)
                {
                    StatusSender.Send("Cannot start this worker");
                    break;
                }
                else
                {
                    status = Result.WaitWorkerReport(
                        Result.SettingsReports,
                        kvp.Key.SendQueue,
                        WorkerReports.WorkerStarted,
                        10 * 60); //10 minutes

                    if (!status)
                    {
                        StatusSender.Send("Cannot receive {0} within 10 minutes", WorkerReports.WorkerStarted);
                        break;
                    }
                }
            }

            return;
        }

        private void SendWorkerLoad()
        {
            string loadStr;
            bool success, status;

            success = true;

            //send the settings
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                StatusSender.Send("Send load settings to {0}", kvp.Key.SendQueue);

                loadStr = LoadConfig.GetLoadSettings();
                status = kvp.Key.SendMessageOnly(loadStr);
                if (!status)
                {
                    StatusSender.Send("Cannot send the settings");
                    success = false;
                    break;
                }
                else
                {
                    status = Result.WaitWorkerReport(
                        Result.SettingsReports,
                        kvp.Key.SendQueue,
                        WorkerReports.SettingReceived,
                        10 * 60); //10 minutes

                    if (!status)
                    {
                        StatusSender.Send("Cannot receive settings confirmation within 10 minutes");
                        success = false;
                        break;
                    }
                }
            }
            if (success == false)
            {
                StatusSender.Send("Exit, cannot get {0}", WorkerReports.SettingReceived);
                return;
            }

            //send the precondition and run
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                StatusSender.Send("Send the preconditions to {0}", kvp.Key.SendQueue);

                loadStr = LoadConfig.NextPreconditionLoad();
                status = kvp.Key.SendMessageOnly(loadStr);
                if (!status)
                {
                    StatusSender.Send("Cannot send the preconditions");
                    success = false;
                    break;
                }
                else
                {
                    status = Result.WaitWorkerReport(
                        Result.PreconditionReports,
                        kvp.Key.SendQueue,
                        WorkerReports.PreconditionReceived,
                        600);
                    if (!status)
                    {
                        StatusSender.Send("Cannot receive precondition confirmation within 10 minutes");
                        success = false;
                        break;
                    }
                }
            }
            if (success == false)
            {
                StatusSender.Send("Exit, cannot get {0}", WorkerReports.PreconditionReceived);
                return;
            }

            //send the unit load and run
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                StatusSender.Send("Send the unitloads to {0}", kvp.Key.SendQueue);

                loadStr = LoadConfig.NextUnitLoad();
                status = kvp.Key.SendMessageOnly(loadStr);
                if (!status)
                {
                    StatusSender.Send("Cannot send the unit load");
                    success = false;
                    break;
                }
                else
                {
                    status = Result.WaitWorkerReport(
                        Result.WorkloadReports,
                        kvp.Key.SendQueue,
                        WorkerReports.WorkloadReceived,
                        600);

                    if (status == false)
                    {
                        StatusSender.Send("Cannot receive workload confirmation within 10 minutes");
                        success = false;
                        break;
                    }
                }
            }
            
            return;
        }

        //TODO: sync in the stress traffic
        private void StartMeasureQuality()
        {
            bool success, status;

            success = true;

            //meet the preconditions
            QBarLogger.Log(DebugLevel.INFO, "Wait for the end of preconditions from workers");
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                status = Result.WaitWorkerReport(
                    Result.PreconditionReports, 
                    kvp.Key.SendQueue, 
                    WorkerReports.PreconditionFinished, 
                    10*60*60); //10 hour?
                if (status == false)
                {
                    success = false;
                    break;
                }

                //send to start next step
            }
            if (success == false)
            {
                StatusSender.Send("Exit, cannot get {0}", WorkerReports.PreconditionFinished);
                return;
            }

            //finish the workloads
            QBarLogger.Log(DebugLevel.INFO, "Wait for the end of unit loads from workers");
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                status = Result.WaitWorkerReport(
                    Result.WorkloadReports, 
                    kvp.Key.SendQueue, 
                    WorkerReports.WorkloadFinished, 
                    10*24*60*60); //2 days?

                if (status == false)
                {
                    success = false;
                    break;
                }
            }
            if (success == false)
            {
                StatusSender.Send("Exit, cannot get {0}", WorkerReports.WorkloadFinished);
                return;
            }

            //finish the workloads
            QBarLogger.Log(DebugLevel.INFO, "Wait for the end of postconditions from workers");
            foreach (KeyValuePair<DuplexQueue, string> kvp in Workers)
            {
                status = Result.WaitWorkerReport(
                    Result.PostconditionReports,
                    kvp.Key.SendQueue,
                    WorkerReports.PostconditionFinished,
                    10 * 60 * 60); //1 hour?

                if (status == false)
                {
                    success = false;
                    break;
                }
            }

            OnStress = false;
            if (success == false)
            {
                QBarLogger.Log(DebugLevel.FATAL, "Exit, cannot get {0}", WorkerReports.PostconditionFinished);
            }

            return;            
        }

        //listening on MasterQueue
        private void ProcessWorkerReports()
        {
            Stopwatch noReportDuration;
            string reportMsg;

            // Send result messages to resultmanagement, hear from manage/masterqueue
            StatusSender.Send("Retrieve the worker reports and process it");

            noReportDuration = Stopwatch.StartNew();
            while(noReportDuration.Elapsed.TotalMinutes < 30)
            {
                if (!OnStress) return;

                reportMsg =  CloudWork.GetAndDeleteOneMessageStr(MasterQueue);
                if (reportMsg != string.Empty)
                {
                    Result.WorkerReport(reportMsg);

                    //zero out the noreport duration
                    noReportDuration.Reset();
                    noReportDuration.Start();
                }
                else
                {
                    //sleep more if quiet
                    Thread.Sleep(1000);
                }
            }
            noReportDuration.Stop();

            return;
        }
    }
}
