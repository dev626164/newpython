﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.AirWatch.Core.Data
{
    using System.Runtime.Serialization;


    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Location", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class Location : System.Data.Objects.DataClasses.EntityObject
    {

        private Microsoft.AirWatch.Core.Data.CentreLocation[] CentreLocationField;

        private decimal LatitudeField;

        private long LocationIdField;

        private decimal LongitudeField;

        private Microsoft.AirWatch.Core.Data.Station[] StationField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.CentreLocation[] CentreLocation
        {
            get
            {
                return this.CentreLocationField;
            }
            set
            {
                this.CentreLocationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Latitude
        {
            get
            {
                return this.LatitudeField;
            }
            set
            {
                this.LatitudeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public long LocationId
        {
            get
            {
                return this.LocationIdField;
            }
            set
            {
                this.LocationIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Longitude
        {
            get
            {
                return this.LongitudeField;
            }
            set
            {
                this.LongitudeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Station[] Station
        {
            get
            {
                return this.StationField;
            }
            set
            {
                this.StationField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "CentreLocation", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class CentreLocation : System.Data.Objects.DataClasses.EntityObject
    {

        private long CentreLocationIdField;

        private Microsoft.AirWatch.Core.Data.CentreLocationMeasurement[] CentreLocationMeasurementField;

        private Microsoft.AirWatch.Core.Data.Location LocationField;

        private System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR LocationReferenceField;

        private System.Nullable<int> QualityIndexField;

        private System.Nullable<System.DateTime> TimestampField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public long CentreLocationId
        {
            get
            {
                return this.CentreLocationIdField;
            }
            set
            {
                this.CentreLocationIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.CentreLocationMeasurement[] CentreLocationMeasurement
        {
            get
            {
                return this.CentreLocationMeasurementField;
            }
            set
            {
                this.CentreLocationMeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Location Location
        {
            get
            {
                return this.LocationField;
            }
            set
            {
                this.LocationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR LocationReference
        {
            get
            {
                return this.LocationReferenceField;
            }
            set
            {
                this.LocationReferenceField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> QualityIndex
        {
            get
            {
                return this.QualityIndexField;
            }
            set
            {
                this.QualityIndexField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> Timestamp
        {
            get
            {
                return this.TimestampField;
            }
            set
            {
                this.TimestampField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "CentreLocationMeasurement", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class CentreLocationMeasurement : System.Data.Objects.DataClasses.EntityObject
    {

        private Microsoft.AirWatch.Core.Data.CentreLocation CentreLocationField;

        private long CentreLocationMeasurementIdField;

        private System.Data.Objects.DataClasses.EntityReferenceOfCentreLocationWMOmRWtR CentreLocationReferenceField;

        private Microsoft.AirWatch.Core.Data.Measurement MeasurementField;

        private System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR MeasurementReferenceField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.CentreLocation CentreLocation
        {
            get
            {
                return this.CentreLocationField;
            }
            set
            {
                this.CentreLocationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public long CentreLocationMeasurementId
        {
            get
            {
                return this.CentreLocationMeasurementIdField;
            }
            set
            {
                this.CentreLocationMeasurementIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfCentreLocationWMOmRWtR CentreLocationReference
        {
            get
            {
                return this.CentreLocationReferenceField;
            }
            set
            {
                this.CentreLocationReferenceField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Measurement Measurement
        {
            get
            {
                return this.MeasurementField;
            }
            set
            {
                this.MeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR MeasurementReference
        {
            get
            {
                return this.MeasurementReferenceField;
            }
            set
            {
                this.MeasurementReferenceField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Measurement", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class Measurement : System.Data.Objects.DataClasses.EntityObject
    {

        private Microsoft.AirWatch.Core.Data.CentreLocationMeasurement[] CentreLocationMeasurementField;

        private Microsoft.AirWatch.Core.Data.Component ComponentField;

        private System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR ComponentReferenceField;

        private int MeasurementIdField;

        private Microsoft.AirWatch.Core.Data.StationMeasurement[] StationMeasurementField;

        private System.Nullable<System.DateTime> TimestampField;

        private System.Nullable<decimal> ValueField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.CentreLocationMeasurement[] CentreLocationMeasurement
        {
            get
            {
                return this.CentreLocationMeasurementField;
            }
            set
            {
                this.CentreLocationMeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Component Component
        {
            get
            {
                return this.ComponentField;
            }
            set
            {
                this.ComponentField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR ComponentReference
        {
            get
            {
                return this.ComponentReferenceField;
            }
            set
            {
                this.ComponentReferenceField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MeasurementId
        {
            get
            {
                return this.MeasurementIdField;
            }
            set
            {
                this.MeasurementIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.StationMeasurement[] StationMeasurement
        {
            get
            {
                return this.StationMeasurementField;
            }
            set
            {
                this.StationMeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> Timestamp
        {
            get
            {
                return this.TimestampField;
            }
            set
            {
                this.TimestampField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<decimal> Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Component", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class Component : System.Data.Objects.DataClasses.EntityObject
    {

        private string CaptionField;

        private long ComponentIdField;

        private Microsoft.AirWatch.Core.Data.Measurement[] MeasurementField;

        private Microsoft.AirWatch.Core.Data.Threshold[] ThresholdField;

        private string UnitField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Caption
        {
            get
            {
                return this.CaptionField;
            }
            set
            {
                this.CaptionField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ComponentId
        {
            get
            {
                return this.ComponentIdField;
            }
            set
            {
                this.ComponentIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Measurement[] Measurement
        {
            get
            {
                return this.MeasurementField;
            }
            set
            {
                this.MeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Threshold[] Threshold
        {
            get
            {
                return this.ThresholdField;
            }
            set
            {
                this.ThresholdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Unit
        {
            get
            {
                return this.UnitField;
            }
            set
            {
                this.UnitField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Threshold", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class Threshold : System.Data.Objects.DataClasses.EntityObject
    {

        private Microsoft.AirWatch.Core.Data.Component ComponentField;

        private System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR ComponentReferenceField;

        private long ThresholdIdField;

        private decimal ValueField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Component Component
        {
            get
            {
                return this.ComponentField;
            }
            set
            {
                this.ComponentField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR ComponentReference
        {
            get
            {
                return this.ComponentReferenceField;
            }
            set
            {
                this.ComponentReferenceField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ThresholdId
        {
            get
            {
                return this.ThresholdIdField;
            }
            set
            {
                this.ThresholdIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "StationMeasurement", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class StationMeasurement : System.Data.Objects.DataClasses.EntityObject
    {

        private System.Nullable<int> AggregatedUserRatingField;

        private System.Nullable<int> AggregatedUserRatingCountField;

        private Microsoft.AirWatch.Core.Data.Measurement MeasurementField;

        private System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR MeasurementReferenceField;

        private Microsoft.AirWatch.Core.Data.Station StationField;

        private int StationMeasurementIdField;

        private System.Data.Objects.DataClasses.EntityReferenceOfStationWMOmRWtR StationReferenceField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> AggregatedUserRating
        {
            get
            {
                return this.AggregatedUserRatingField;
            }
            set
            {
                this.AggregatedUserRatingField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> AggregatedUserRatingCount
        {
            get
            {
                return this.AggregatedUserRatingCountField;
            }
            set
            {
                this.AggregatedUserRatingCountField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Measurement Measurement
        {
            get
            {
                return this.MeasurementField;
            }
            set
            {
                this.MeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR MeasurementReference
        {
            get
            {
                return this.MeasurementReferenceField;
            }
            set
            {
                this.MeasurementReferenceField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Station Station
        {
            get
            {
                return this.StationField;
            }
            set
            {
                this.StationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public int StationMeasurementId
        {
            get
            {
                return this.StationMeasurementIdField;
            }
            set
            {
                this.StationMeasurementIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfStationWMOmRWtR StationReference
        {
            get
            {
                return this.StationReferenceField;
            }
            set
            {
                this.StationReferenceField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Station", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data", IsReference = true)]
    public partial class Station : System.Data.Objects.DataClasses.EntityObject
    {

        private Microsoft.AirWatch.Core.Data.AverageRating AverageRatingField;

        private string CountryNameField;

        private string EuropeanCodeField;

        private string FriendlyAddressField;

        private Microsoft.AirWatch.Core.Data.Location LocationField;

        private System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR LocationReferenceField;

        private string NameField;

        private System.Nullable<int> QualityIndexField;

        private int StationIdField;

        private Microsoft.AirWatch.Core.Data.StationMeasurement[] StationMeasurementField;

        private string StationPurposeField;

        private string StreetTypeField;

        private System.Nullable<System.DateTime> TimestampField;

        private string TypeOfAreaField;

        private string TypeOfAreaSubcatField;

        private string TypeOfStationField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.AverageRating AverageRating
        {
            get
            {
                return this.AverageRatingField;
            }
            set
            {
                this.AverageRatingField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CountryName
        {
            get
            {
                return this.CountryNameField;
            }
            set
            {
                this.CountryNameField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EuropeanCode
        {
            get
            {
                return this.EuropeanCodeField;
            }
            set
            {
                this.EuropeanCodeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FriendlyAddress
        {
            get
            {
                return this.FriendlyAddressField;
            }
            set
            {
                this.FriendlyAddressField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Location Location
        {
            get
            {
                return this.LocationField;
            }
            set
            {
                this.LocationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR LocationReference
        {
            get
            {
                return this.LocationReferenceField;
            }
            set
            {
                this.LocationReferenceField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> QualityIndex
        {
            get
            {
                return this.QualityIndexField;
            }
            set
            {
                this.QualityIndexField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public int StationId
        {
            get
            {
                return this.StationIdField;
            }
            set
            {
                this.StationIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.StationMeasurement[] StationMeasurement
        {
            get
            {
                return this.StationMeasurementField;
            }
            set
            {
                this.StationMeasurementField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StationPurpose
        {
            get
            {
                return this.StationPurposeField;
            }
            set
            {
                this.StationPurposeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StreetType
        {
            get
            {
                return this.StreetTypeField;
            }
            set
            {
                this.StreetTypeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> Timestamp
        {
            get
            {
                return this.TimestampField;
            }
            set
            {
                this.TimestampField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TypeOfArea
        {
            get
            {
                return this.TypeOfAreaField;
            }
            set
            {
                this.TypeOfAreaField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TypeOfAreaSubcat
        {
            get
            {
                return this.TypeOfAreaSubcatField;
            }
            set
            {
                this.TypeOfAreaSubcatField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TypeOfStation
        {
            get
            {
                return this.TypeOfStationField;
            }
            set
            {
                this.TypeOfStationField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "AverageRating", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data")]
    public partial class AverageRating : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private System.Nullable<int> CountField;

        private System.Nullable<int> RatingField;

        private Microsoft.AirWatch.Core.Data.RatingQualifierIdentifiers[] RatingQualifierIdentifiersField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> Count
        {
            get
            {
                return this.CountField;
            }
            set
            {
                this.CountField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> Rating
        {
            get
            {
                return this.RatingField;
            }
            set
            {
                this.RatingField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.RatingQualifierIdentifiers[] RatingQualifierIdentifiers
        {
            get
            {
                return this.RatingQualifierIdentifiersField;
            }
            set
            {
                this.RatingQualifierIdentifiersField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "RatingQualifierIdentifiers", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data")]
    public partial class RatingQualifierIdentifiers : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private Microsoft.AirWatch.Core.Data.AverageRating AverageRatingField;

        private System.Nullable<double> PercentageField;

        private string WordIdentifierField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.AverageRating AverageRating
        {
            get
            {
                return this.AverageRatingField;
            }
            set
            {
                this.AverageRatingField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<double> Percentage
        {
            get
            {
                return this.PercentageField;
            }
            set
            {
                this.PercentageField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string WordIdentifier
        {
            get
            {
                return this.WordIdentifierField;
            }
            set
            {
                this.WordIdentifierField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Pushpin", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Core.Data")]
    public partial class Pushpin : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private Microsoft.AirWatch.Core.Data.AverageRating AverageRatingField;

        private Microsoft.AirWatch.Core.Data.CentreLocation CentreLocationField;

        private string FriendlyNameField;

        private Microsoft.AirWatch.Core.Data.Location LocationField;

        private int PushpinIdField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.AverageRating AverageRating
        {
            get
            {
                return this.AverageRatingField;
            }
            set
            {
                this.AverageRatingField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.CentreLocation CentreLocation
        {
            get
            {
                return this.CentreLocationField;
            }
            set
            {
                this.CentreLocationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FriendlyName
        {
            get
            {
                return this.FriendlyNameField;
            }
            set
            {
                this.FriendlyNameField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public Microsoft.AirWatch.Core.Data.Location Location
        {
            get
            {
                return this.LocationField;
            }
            set
            {
                this.LocationField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public int PushpinId
        {
            get
            {
                return this.PushpinIdField;
            }
            set
            {
                this.PushpinIdField = value;
            }
        }
    }
}
namespace System.Data
{
    using System.Runtime.Serialization;


    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityKey", Namespace = "http://schemas.datacontract.org/2004/07/System.Data", IsReference = true)]
    public partial class EntityKey : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private string EntityContainerNameField;

        private System.Data.EntityKeyMember[] EntityKeyValuesField;

        private string EntitySetNameField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EntityContainerName
        {
            get
            {
                return this.EntityContainerNameField;
            }
            set
            {
                this.EntityContainerNameField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.EntityKeyMember[] EntityKeyValues
        {
            get
            {
                return this.EntityKeyValuesField;
            }
            set
            {
                this.EntityKeyValuesField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EntitySetName
        {
            get
            {
                return this.EntitySetNameField;
            }
            set
            {
                this.EntitySetNameField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityKeyMember", Namespace = "http://schemas.datacontract.org/2004/07/System.Data")]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Station))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.AverageRating))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.RatingQualifierIdentifiers[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.RatingQualifierIdentifiers))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Location))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocation[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocation))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocationMeasurement[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocationMeasurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Measurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Component))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Measurement[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Threshold[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Threshold))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.StationMeasurement[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.StationMeasurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Station[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Common.StationContainer[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Common.StationContainer))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityObject))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.StructuralObject))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfCentreLocationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReference))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.RelatedEnd))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfStationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.EntityKey))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.EntityKeyMember[]))]
    public partial class EntityKeyMember : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private string KeyField;

        private object ValueField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Key
        {
            get
            {
                return this.KeyField;
            }
            set
            {
                this.KeyField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public object Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }
    }
}
namespace Microsoft.AirWatch.Common
{
    using System.Runtime.Serialization;


    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "StationContainer", Namespace = "http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.Common")]
    public partial class StationContainer : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private string EuropeanCodeField;

        private decimal LatitudeField;

        private decimal LongitudeField;

        private System.Nullable<int> QualityIndexField;

        private int StationIdField;

        private string StationPurposeField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EuropeanCode
        {
            get
            {
                return this.EuropeanCodeField;
            }
            set
            {
                this.EuropeanCodeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Latitude
        {
            get
            {
                return this.LatitudeField;
            }
            set
            {
                this.LatitudeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Longitude
        {
            get
            {
                return this.LongitudeField;
            }
            set
            {
                this.LongitudeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> QualityIndex
        {
            get
            {
                return this.QualityIndexField;
            }
            set
            {
                this.QualityIndexField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public int StationId
        {
            get
            {
                return this.StationIdField;
            }
            set
            {
                this.StationIdField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StationPurpose
        {
            get
            {
                return this.StationPurposeField;
            }
            set
            {
                this.StationPurposeField = value;
            }
        }
    }
}
namespace System.Data.Objects.DataClasses
{
    using System.Runtime.Serialization;


    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "StructuralObject", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses", IsReference = true)]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityObject))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Location))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocation))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocationMeasurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Measurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Component))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Threshold))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.StationMeasurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Station))]
    public partial class StructuralObject : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityObject", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses", IsReference = true)]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Location))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocation))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.CentreLocationMeasurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Measurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Component))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Threshold))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.StationMeasurement))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Microsoft.AirWatch.Core.Data.Station))]
    public partial class EntityObject : System.Data.Objects.DataClasses.StructuralObject
    {

        private System.Data.EntityKey EntityKeyField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.EntityKey EntityKey
        {
            get
            {
                return this.EntityKeyField;
            }
            set
            {
                this.EntityKeyField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityReferenceOfLocationWMOmRWtR", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    public partial class EntityReferenceOfLocationWMOmRWtR : System.Data.Objects.DataClasses.EntityReference
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityReferenceOfCentreLocationWMOmRWtR", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    public partial class EntityReferenceOfCentreLocationWMOmRWtR : System.Data.Objects.DataClasses.EntityReference
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityReference", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfStationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfCentreLocationWMOmRWtR))]
    public partial class EntityReference : System.Data.Objects.DataClasses.RelatedEnd
    {

        private System.Data.EntityKey EntityKeyField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Data.EntityKey EntityKey
        {
            get
            {
                return this.EntityKeyField;
            }
            set
            {
                this.EntityKeyField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "RelatedEnd", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReference))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfComponentWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfMeasurementWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfStationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfLocationWMOmRWtR))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(System.Data.Objects.DataClasses.EntityReferenceOfCentreLocationWMOmRWtR))]
    public partial class RelatedEnd : object, System.Runtime.Serialization.IExtensibleDataObject
    {

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityReferenceOfComponentWMOmRWtR", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    public partial class EntityReferenceOfComponentWMOmRWtR : System.Data.Objects.DataClasses.EntityReference
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityReferenceOfMeasurementWMOmRWtR", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    public partial class EntityReferenceOfMeasurementWMOmRWtR : System.Data.Objects.DataClasses.EntityReference
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EntityReferenceOfStationWMOmRWtR", Namespace = "http://schemas.datacontract.org/2004/07/System.Data.Objects.DataClasses")]
    public partial class EntityReferenceOfStationWMOmRWtR : System.Data.Objects.DataClasses.EntityReference
    {
    }
}
