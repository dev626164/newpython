﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3074
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Microsoft.AirWatch.AzureTables
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="LanguageDescription", Namespace="http://schemas.datacontract.org/2004/07/Microsoft.AirWatch.AzureTables")]
    public partial class LanguageDescription : object, System.Runtime.Serialization.IExtensibleDataObject
    {
        
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string LanguageCodeField;
        
        private string LanguageNameField;
        
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LanguageCode
        {
            get
            {
                return this.LanguageCodeField;
            }
            set
            {
                this.LanguageCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LanguageName
        {
            get
            {
                return this.LanguageNameField;
            }
            set
            {
                this.LanguageNameField = value;
            }
        }
    }
}


[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(ConfigurationName="ILanguageService")]
public interface ILanguageService
{
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ILanguageService/GetLanguages", ReplyAction="http://tempuri.org/ILanguageService/GetLanguagesResponse")]
    Microsoft.AirWatch.AzureTables.LanguageDescription[] GetLanguages();
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ILanguageService/GetLanguage", ReplyAction="http://tempuri.org/ILanguageService/GetLanguageResponse")]
    System.Collections.Generic.Dictionary<string, string> GetLanguage(string culture);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public interface ILanguageServiceChannel : ILanguageService, System.ServiceModel.IClientChannel
{
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public partial class LanguageServiceClient : System.ServiceModel.ClientBase<ILanguageService>, ILanguageService
{
    
    public LanguageServiceClient()
    {
    }
    
    public LanguageServiceClient(string endpointConfigurationName) : 
            base(endpointConfigurationName)
    {
    }
    
    public LanguageServiceClient(string endpointConfigurationName, string remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public LanguageServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public LanguageServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(binding, remoteAddress)
    {
    }
    
    public Microsoft.AirWatch.AzureTables.LanguageDescription[] GetLanguages()
    {
        return base.Channel.GetLanguages();
    }
    
    public System.Collections.Generic.Dictionary<string, string> GetLanguage(string culture)
    {
        return base.Channel.GetLanguage(culture);
    }
}
