﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class AirWatchTextService : QBarWorkItem
    {
        public AirWatchScenarios ItemName
        {
            get {return (AirWatchScenarios)Enum.Parse(typeof(AirWatchScenarios), ScenarioName);}
        }

        AirWatchQBarResources ActiveResources;
        private AirWatchTenantInfo TestTenant;

        public AirWatchTextService(AirWatchTenantInfo tenant, WorkItemType type, AirWatchQBarResources resources, DuplexQueue master)
            : base(AirWatchScenarios.AirWatchTextService.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            string userinfo = this.ToString();
            AirWatchTextWebServiceSoapClient proxy = null;
            msg mssg = null;

            try
            {
            }
            finally
            {
                if (proxy != null)
                {
                    proxy.Close();
                    proxy = null;
                }
            }

            return;
        }
    }
}
