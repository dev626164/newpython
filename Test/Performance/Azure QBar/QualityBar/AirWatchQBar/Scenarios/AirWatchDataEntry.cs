﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.IO;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class AirWatchDataEntry : QBarWorkItem
    {
        public AirWatchScenarios ItemName
        {
            get {return (AirWatchScenarios)Enum.Parse(typeof(AirWatchScenarios), ScenarioName);}
        }

        AirWatchQBarResources ActiveResources;
        private AirWatchTenantInfo TestTenant;
        
        public AirWatchDataEntry(AirWatchTenantInfo tenant, WorkItemType type, AirWatchQBarResources resources, DuplexQueue master)
            : base(AirWatchScenarios.AirWatchDataEntry.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            string userinfo = "DataEntry";

            Thread airStationThread = new Thread(new ThreadStart(EnterAirStationData));
            Thread waterStationThread = new Thread(new ThreadStart(EnterWaterStationData));
            Thread mapModelThread = new Thread(new ThreadStart(EnterMapModelData));

            airStationThread.Start();
            waterStationThread.Start();
            mapModelThread.Start();

            return;
        }

        private void EnterAirStationData()
        {
            DataEntryServiceClient proxy = null;
            try
            {
                proxy = new DataEntryServiceClient();

                PreAPICall();
                proxy.EnterAirStationData(null);
                PostAPICall("EnterAirStationData");
            }
            finally
            {
                if(proxy != null)
                {
                    proxy.Close();
                    proxy = null;
                }
            }
        }

        private void EnterWaterStationData()
        {
            DataEntryServiceClient proxy = null;
            try
            {
                proxy = new DataEntryServiceClient();

                PreAPICall();
                proxy.EnterWaterStationData(null);
                PostAPICall("EnterWaterStationData");
            }
            finally
            {
                if (proxy != null)
                {
                    proxy.Close();
                    proxy = null;
                }
            }
        }

        private void EnterMapModelData()
        {
            DataEntryServiceClient proxy = null;
            XmlDocument xmlDoc = null;

            try
            {
                xmlDoc = new XmlDocument();
                using (StreamReader sr = new StreamReader(@"\Data\MapModelInput.xml"))
                {
                    xmlDoc.LoadXml(sr.ReadToEnd());
                }
                proxy = new DataEntryServiceClient();

                PreAPICall();
                //proxy.EnterMapModelData(xmlDoc.GetElementById("data"));
                PostAPICall("EnterMapModelData");
            }
            finally
            {
                if (proxy != null)
                {
                    proxy.Close();
                    proxy = null;
                }
            }
        }
    }
}
