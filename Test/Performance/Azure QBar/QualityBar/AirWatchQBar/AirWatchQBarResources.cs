﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class AirWatchQBarResources
    {
        public class StorageAccount
        {
            public string Name;
            public string Key;
            public string Location;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public class Container
        {
            public string Name;
            public string Account;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }


    }
}
