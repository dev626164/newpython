﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Microsoft.Samples.ServiceHosting.StorageClient;


namespace Microsoft.Cis.E2E.QualityBar
{
    public class TestxStore
    {
        public static void StartTest()
        {
            TestxStore testEngine;

            QBarLogger.WriteToConsole = true;

            testEngine = new TestxStore();
            testEngine.TestCreateAccount();
            testEngine.TestCreateContainer();
            testEngine.TestPutBlob();
            testEngine.TestGetBlob();
            testEngine.TestDeleteBlob();
            testEngine.TestDeleteContainer();
            testEngine.TestDeleteAccount();
        }

        private TestxStore()
        {
            TestTenant = new XStoreTenantInfo()
            {
                Type = "xstore",
                FriendlyName = "QualityBar WorkItem Test",
                AccountEndpoint = @"https://" + ConfigurationSettings.AppSettings["XStoreEndpoint"] + "/",
                BlobEndpoint = @"http://" + ConfigurationSettings.AppSettings["XStoreBlobEndpoint"] + "/",
                QueueEndpoint = @"http://" + ConfigurationSettings.AppSettings["XStoreQueueEndpoint"] + "/",
                TableEndpoint = @"http://" + ConfigurationSettings.AppSettings["XStoreTableEndpoint"] + "/",
                AdminAccount = "lps",
                AdminAccountKey = "FjUfNl1KiJttbXlsdkMzBTC7WagvrRM9/g6UPBuy0ypCpAbYTL6/KA+dI/7gyoWvLFYmah3IviUP1jykOHHOlA==",
                NumInstances = 1,
                Scale = 1.0,
                Locations = "ussouth|uswest",
                Domains = "domainco2int1.1|domainco2int2.2"
            };

            foreach (string d in TestTenant.Domains.Split('|'))
            {
                TestTenant.GeoDomainList.Add(d);
            }

            foreach (string l in TestTenant.Locations.Split('|'))
            {
                TestTenant.GeoLocationList.Add(l);
            }

            ActiveTestResources = new xStoreQBarResources();
            TestQueue = new DuplexQueue(ConfigurationSettings.AppSettings["TestResultQueue"], Guid.NewGuid().ToString());
        }

        private void TestCreateAccount()
        {
            CreateStorageAccount csa = new CreateStorageAccount(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            csa.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", csa.TestInfo);

            return;
        }

        private void TestDeleteAccount()
        {
            DeleteStorageAccount dsa = new DeleteStorageAccount(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            dsa.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", dsa.TestInfo);
        }

        private void TestCreateContainer()
        {
            CreateContainer cc = new CreateContainer(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            cc.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", cc.TestInfo);
        }

        private void TestDeleteContainer()
        {
            DeleteContainer dc = new DeleteContainer(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            dc.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", dc.TestInfo);
        }

        private void TestPutBlob()
        {
            PutBlob pb = new PutBlob(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            pb.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", pb.TestInfo);
        }

        private void TestGetBlob()
        {
            GetBlob gb = new GetBlob(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            gb.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", gb.TestInfo);
        }

        private void TestDeleteBlob()
        {
            DeleteBlob db = new DeleteBlob(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            db.ExecuteScenario();

            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", db.TestInfo);
        }

        private XStoreTenantInfo TestTenant;
        private xStoreQBarResources ActiveTestResources;
        private DuplexQueue TestQueue;        
    }
}
