﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class TestRDFE
    {
        public static void StartTest()
        {
            TestRDFE testEngine;

            QBarLogger.WriteToConsole = true;

            testEngine = new TestRDFE();
            for (int i = 0; i < 3; i++)
            {
                testEngine.TestCreateSubscription();
                testEngine.TestCreateHostedService();
                testEngine.TestCreateStorageService();
                testEngine.TestPutDeployment();
                //testEngine.TestDeleteDeployment();
                //testEngine.TestDeleteStorageService();
                //testEngine.TestDeleteHostedService();
                //testEngine.TestUpdateDeploymentConfig();
                //testEngine.TestUpgradeDeployment();
            }

            return;
        }

        private TenantInfo TestTenant;
        private RDFEQBarResources ActiveTestResources;
        private DuplexQueue TestQueue;
        private TestRDFE()
        {
            TestTenant = new TenantInfo()
            {
                Endpoint = @"http://" + ConfigurationSettings.AppSettings["RDFEEndpoint"] + "/",
                FriendlyName = "QualityBar Workitem Test",
                CompPkgBlobEndpoint = "blob.e2e1.dnsdemo4.com",
                CompPkgAccount = "lpspackages",
                CompPkgKey = "k3Neh/d6sqd7OrKzvbQRmvvwBadqkGFnhEJgCiHQWsKfsHEFMQhTEspIA7rq4Ht2gXn3LZQUOBuNUqnVoFFq8A==",
                //CompPkgBlobEndpoint = "blob.core.windows.net",
                //CompPkgAccount = "rdfeprodtests",
                //CompPkgKey = "eZXlGVIp0lXjjvAi5YAcMDwA/ZJjWPwu7KdZPqFIoUMj3tVCN8FEBYNPjTKm5ZmN/dcm7lr0XLbpNIO7ErG0HA==",
                Scale = 1.0,
                NumInstances = 1,
                Type = "Average"
            };

            ActiveTestResources = new RDFEQBarResources();
            TestQueue = new DuplexQueue(ConfigurationSettings.AppSettings["TestResultQueue"], Guid.NewGuid().ToString());
        }

        private void TestCreateSubscription()
        {
            CreateSubscription cs;
            
            cs = new CreateSubscription(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            cs.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", cs.TestInfo);

            return;
        }

        private void TestCreateHostedService()
        {
            CreateHostedService chs;
            
            chs = new CreateHostedService(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            chs.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", chs.TestInfo);
            
            return;
        }

        private void TestCreateStorageService()
        {
            CreateStorageService css;
            
            css = new CreateStorageService(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            css.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", css.TestInfo);

            return;
        }

        private void TestPutDeployment()
        {
            PutDeployment pd;
            CommonInfo.ResourceInfo package;

            package = new CommonInfo.ResourceInfo()
            {
                Name = "HelloWorld",
                Type = ResourceTypes.ComputePackage,
                Container = "computepackages",
                Blob = "HelloWorld",
            };

            pd = new PutDeployment(TestTenant, package, 1, WorkItemType.Average, ActiveTestResources, TestQueue);
            pd.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", pd.TestInfo);

            return;
        }

        private void TestDeleteDeployment()
        {
            DeleteDeployment dd;
            
            dd = new DeleteDeployment(TestTenant, 1, WorkItemType.Average, ActiveTestResources, TestQueue);
            dd.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", dd.TestInfo);

            return;
        }

        private void TestDeleteStorageService()
        {
            DeleteStorageService dss;
            
            dss = new DeleteStorageService(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            dss.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", dss.TestInfo);

            return;
        }

        private void TestDeleteHostedService()
        {
            DeleteHostedService dhs;
            
            dhs = new DeleteHostedService(TestTenant, WorkItemType.Average, ActiveTestResources, TestQueue);
            dhs.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", dhs.TestInfo);

            return;
        }

        private void TestUpdateDeploymentConfig()
        {
            UpdateDeploymentConfig udc;
            
            udc = new UpdateDeploymentConfig(TestTenant, 1, 1, WorkItemType.Average, ActiveTestResources, TestQueue);
            udc.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", udc.TestInfo);

            return;
        }

        private void TestUpgradeDeployment()
        {
            UpgradeDeployment ud;
            CommonInfo.ResourceInfo newPackage;

            newPackage = new CommonInfo.ResourceInfo()
            {
                Name = "HelloWorld",
                Type = ResourceTypes.ComputePackage,
                Container = "computepackages",
                Blob = "HelloWorld",
            };

            ud = new UpgradeDeployment(TestTenant, 1, newPackage, WorkItemType.Average, ActiveTestResources, TestQueue);
            ud.ExecuteScenario();
            QBarLogger.Log(DebugLevel.INFO, "user info\r\n{0}", ud.TestInfo);

            return;
        }
        //End of TestRDFE
    }
}
