﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class RDFEQBarResources
    {
        public class Subscription
        {
            public string Puid;
            public string Name;
            public string Id;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public class StorageAccount
        {
            public string CreatorPuid;
            public string SubscriptionId;
            public string Name;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public class ComputeAccount
        {
            public class Deployment
            {
                public string Name;
                public string TenantId;
                public int NodeSize;
                public string PackageUrl;
                public DateTime CreatedAt;
            }
            public string CreatorPuid;
            public string SubscriptionId;
            public string Name;
            public Deployment StagingDeployment;
            public Deployment ProductDeployment;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public int SubscriptionCount
        {
            get
            {
                int count;
                lock (SubscriptionList)
                {
                    count = SubscriptionList.Count;
                }

                return count;
            }
        }

        public int ComputeAccountCount
        {
            get
            {
                int count;
                lock (ComputeAccountList)
                {
                    count = ComputeAccountList.Count;
                }

                return count;
            }
        }

        public int StorageAccountCount
        {
            get
            {
                int count;
                lock (StorageAccountList)
                {
                    count = StorageAccountList.Count;
                }

                return count;
            }
        }

        public int DeploymentCount
        {
            get
            {
                int count;

                count = 0;
                lock (ComputeAccountList)
                {
                    foreach(KeyValuePair<string, ComputeAccount> kvp in ComputeAccountList)
                    {
                        if (kvp.Value.StagingDeployment != null) count++;
                        if (kvp.Value.ProductDeployment != null) count++;
                    }
                }

                return count;
            }
        }

        Dictionary<string, Subscription> SubscriptionList;
        Dictionary<string, StorageAccount> StorageAccountList;
        Dictionary<string, ComputeAccount> ComputeAccountList;
        
        public RDFEQBarResources()
        {
            SubscriptionList = new Dictionary<string, Subscription>();
            StorageAccountList = new Dictionary<string, StorageAccount>();
            ComputeAccountList = new Dictionary<string, ComputeAccount>();
        }

        #region AddResources
        public void AddSubscription(string puid, string name, string id, DateTime createdAt)
        {
            lock (SubscriptionList)
            {
                SubscriptionList[id] = new Subscription() 
                { 
                    Puid = puid, 
                    Name = name, 
                    Id = id, 
                    CreatedAt = createdAt,
                    Status = ResourceStatus.Idle
                };
            }
        }

        public void AddStorageAccount(string creator, string subscription, string name, DateTime createdAt)
        {
            lock (StorageAccountList)
            {
                StorageAccountList[name] = new StorageAccount() 
                { 
                    CreatorPuid=creator, 
                    SubscriptionId=subscription, 
                    Name = name, 
                    CreatedAt = createdAt,
                    Status = ResourceStatus.Idle
                };
            }

            return;
        }

        public void AddComputeAccount(string creator, string subscription, string name, DateTime createdAt)
        {
            lock (ComputeAccountList)
            {
                ComputeAccountList[name] = new ComputeAccount() 
                { 
                    CreatorPuid = creator, 
                    SubscriptionId = subscription, 
                    Name = name, 
                    CreatedAt = createdAt, 
                    StagingDeployment = null,
                    ProductDeployment = null,
                    Status = ResourceStatus.Idle
                };
            }

            return;
        }

        public void AddStagingDeployment(string hostedServiceName, string deploymentName, int nodeSize, string packageUrl, DateTime createdAt)
        {
            lock (ComputeAccountList)
            {
                ComputeAccountList[hostedServiceName].StagingDeployment = new ComputeAccount.Deployment()
                {
                    Name = deploymentName,
                    NodeSize = nodeSize,
                    PackageUrl =packageUrl,
                    CreatedAt = createdAt,
                };
            }

            return;
        }

        public void AddProductionDeployment(string hostedServiceName, string deploymentName, int nodeSize, string packageUrl, DateTime createdAt)
        {
            lock (ComputeAccountList)
            {
                ComputeAccountList[hostedServiceName].ProductDeployment = new ComputeAccount.Deployment()
                {
                    Name = deploymentName,
                    NodeSize = nodeSize,
                    PackageUrl = packageUrl,
                    CreatedAt = createdAt,
                };
            }

            return;
        }

        public void SwitchProductionStaging(string hostedServiceName)
        {
            ComputeAccount.Deployment d;
            ComputeAccount ca;

            lock (ComputeAccountList)
            {
                ca = ComputeAccountList[hostedServiceName];
                d = ca.StagingDeployment;
                ca.StagingDeployment = ca.ProductDeployment;
                ca.ProductDeployment = d;
            }
            return;
        }
        #endregion //AddResources

        #region RandomGetResources
        public Subscription RandomGetSubscription(ResourceStatus status)
        {
            Subscription one;
            int i, index, idleCount;

            one = null;
            lock (SubscriptionList)
            {
                idleCount = 0;
                foreach (Subscription s in SubscriptionList.Values)
                {
                    if (s.Status == ResourceStatus.Idle) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (Subscription s in SubscriptionList.Values)
                    {
                        if (s.Status != ResourceStatus.Idle) continue;

                        if (i==index)
                        {
                            s.Status = status;
                            one = s;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.SubscriptionUnavailable);
            }

            return one;
        }

        public StorageAccount RandomGetStorageAccount(ResourceStatus status)
        {
            StorageAccount one;
            int i, index, idleCount;

            one = null;
            lock (StorageAccountList)
            {
                idleCount = 0;
                foreach (StorageAccount ca in StorageAccountList.Values)
                {
                    if (ca.Status != ResourceStatus.Idle) continue;
                    idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (StorageAccount ca in StorageAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle) continue;
                        
                        if (i == index)
                        {
                            ca.Status = status;
                            one = ca;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.StorageServiceUnavailable);
            }

            return one;
        }

        public ComputeAccount RandomGetComputeAccount(ResourceStatus status)
        {
            ComputeAccount one;
            int i, index, idleCount;

            one = null;
            lock (ComputeAccountList)
            {
                idleCount = 0;
                foreach (ComputeAccount ca in ComputeAccountList.Values)
                {
                    if (ca.Status == ResourceStatus.Idle) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (ComputeAccount ca in ComputeAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle) continue;

                        if (i == index)
                        {
                            ca.Status = status;
                            one = ca;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.HostedServiceUnavailable);
            }

            return one;
        }

        public ComputeAccount RandomGetComputeAccountWithStaging(int nodeSize, ResourceStatus status)
        {
            ComputeAccount one;
            int i, index, idleCount;

            one = null;
            lock (ComputeAccountList)
            {
                idleCount = 0;
                foreach (ComputeAccount ca in ComputeAccountList.Values)
                {
                    if (ca.Status == ResourceStatus.Idle 
                        && ca.StagingDeployment != null
                        && ca.StagingDeployment.NodeSize == nodeSize) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (ComputeAccount ca in ComputeAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle 
                            || ca.StagingDeployment == null
                            || ca.StagingDeployment.NodeSize != nodeSize) continue;

                        if (i == index)
                        {
                            ca.Status = status;
                            one = ca;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.StagingDeploymentUnavailable);
            }

            return one;
        }

        public ComputeAccount RandomGetComputeAccountWithProduction(int nodeSize, ResourceStatus status)
        {
            ComputeAccount one;
            int i, index, idleCount;

            one = null;
            lock (ComputeAccountList)
            {
                idleCount = 0;
                foreach (ComputeAccount ca in ComputeAccountList.Values)
                {
                    if (ca.Status == ResourceStatus.Idle 
                        && ca.ProductDeployment != null
                        && ca.ProductDeployment.NodeSize == nodeSize) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (ComputeAccount ca in ComputeAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle 
                            || ca.ProductDeployment == null
                            || ca.ProductDeployment.NodeSize != nodeSize) continue;

                        if (i == index)
                        {
                            ca.Status = status;
                            one = ca;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.ProductDeploymentUnavailable);
            }

            return one;
        }
        public ComputeAccount RandomGetFullDeployedComputeAccount(ResourceStatus status)
        {
            ComputeAccount one;
            int i, index, idleCount;

            one = null;
            lock (ComputeAccountList)
            {
                idleCount = 0;
                foreach (ComputeAccount ca in ComputeAccountList.Values)
                {
                    if (ca.Status == ResourceStatus.Idle
                        && ca.StagingDeployment != null
                        && ca.ProductDeployment != null) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (ComputeAccount ca in ComputeAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle 
                            || ca.StagingDeployment == null
                            || ca.ProductDeployment == null) continue;

                        if (i == index)
                        {
                            ca.Status = status;
                            one = ca;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.FullDeploymentUnavailable);
            }

            return one;
        }


        public ComputeAccount RandomGetComputeAccountWithDeployment(int nodeSize, ResourceStatus status)
        {
            ComputeAccount one;
            int i, index, idleCount;

            one = null;
            lock (ComputeAccountList)
            {
                idleCount = 0;
                foreach (ComputeAccount ca in ComputeAccountList.Values)
                {
                    if (ca.Status != ResourceStatus.Idle) continue;

                    if (ca.StagingDeployment != null && (nodeSize == -1 || ca.StagingDeployment.NodeSize == nodeSize))
                    {
                        idleCount++;
                    }
                    
                    if (ca.ProductDeployment != null && (nodeSize == -1 || ca.ProductDeployment.NodeSize == nodeSize))
                    {
                        idleCount++;
                    }
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (ComputeAccount ca in ComputeAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle) continue;

                        if(ca.StagingDeployment != null && (nodeSize == -1 || ca.StagingDeployment.NodeSize == nodeSize))
                        {
                            if(i==index)
                            {
                                ca.Status = status;
                                one = ca;
                                break;
                            }
                            i++;
                        }
                        
                        if(ca.ProductDeployment != null && (nodeSize == -1 || ca.ProductDeployment.NodeSize == nodeSize))
                        {
                            if(i==index)
                            {
                                ca.Status = status;
                                one = ca;
                                break;
                            }
                        }
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.DeploymentUnavailable);
            }

            return one;
        }

        public int RandomGetDeploymentSize()
        {
            ComputeAccount.Deployment d;
            int i, index, idleCount;

            d = null;
            lock (ComputeAccountList)
            {
                idleCount = 0;
                foreach (ComputeAccount ca in ComputeAccountList.Values)
                {
                    if (ca.Status != ResourceStatus.Idle) continue;

                    if (ca.StagingDeployment != null)
                    {
                        idleCount++;
                    }
                    
                    if (ca.ProductDeployment != null)
                    {
                        idleCount++;
                    }
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (ComputeAccount ca in ComputeAccountList.Values)
                    {
                        if (ca.Status != ResourceStatus.Idle) continue;

                        if (ca.StagingDeployment != null)
                        {
                            if (i == index)
                            {
                                d = ca.StagingDeployment;
                                break;
                            }
                            i++;
                        }
                        else if (ca.ProductDeployment != null)
                        {
                            if (i == index)
                            {
                                d = ca.ProductDeployment;
                                break;
                            }
                        }
                    }
                }
            }

            if (d == null)
            {
                return 0;
            }

            return d.NodeSize;
        }
        #endregion //RandomGetResources

        #region UpdateResourceStatus
        public void UpdateSubscriptionStatus(string subscriptionId, ResourceStatus status)
        {
            lock (SubscriptionList)
            {
                SubscriptionList[subscriptionId].Status = status;
            }
            return;
        }

        public void UpdateHostedServiceStatus(string hostedService, ResourceStatus status)
        {
            lock (ComputeAccountList)
            {
                ComputeAccountList[hostedService].Status = status;
            }
            return;
        }

        public void UpdateStorageServiceStatus(string storageService, ResourceStatus status)
        {
            lock (StorageAccountList)
            {
                StorageAccountList[storageService].Status = status;
            }
            return;
        }
        #endregion //UpdateResourceStatus

        #region DeleteResources
        public bool DeleteSubscription(string subscriptionId)
        {
            lock (SubscriptionList)
            {
                if (SubscriptionList.ContainsKey(subscriptionId))
                {
                    SubscriptionList.Remove(subscriptionId);
                }
            }

            return true;
        }

        public bool DeleteDeployment(string hostedService, string deploymentName)
        {
            bool success;
            ComputeAccount ca;

            success = false;
            lock (ComputeAccountList)
            {
                if (ComputeAccountList.ContainsKey(hostedService))
                {
                    ca = ComputeAccountList[hostedService];
                    if (ca != null)
                    {
                        if (ca.StagingDeployment != null
                            && ca.StagingDeployment.Name == deploymentName)
                        {
                            ca.StagingDeployment = null;
                            success = true;
                        }

                        if (ca.ProductDeployment != null
                            && ca.ProductDeployment.Name == deploymentName)
                        {
                            ca.ProductDeployment = null;
                            success = true;
                        }
                    }
                }
            }

            return success;
        }

        public bool DeleteHostedService(string hostedService)
        {
            lock (ComputeAccountList)
            {
                if (ComputeAccountList.ContainsKey(hostedService))
                {
                    ComputeAccountList.Remove(hostedService);
                }
            }

            return true;
        }

        public bool DeleteStorageService(string storageService)
        {
            lock (StorageAccountList)
            {
                if (StorageAccountList.ContainsKey(storageService))
                {
                    StorageAccountList.Remove(storageService);
                }
            }

            return true;
        }
        #endregion //DeleteResources
    }
}
