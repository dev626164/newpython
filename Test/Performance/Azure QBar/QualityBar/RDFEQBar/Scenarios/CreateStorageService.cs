﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class CreateStorageService:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;

        public CreateStorageService(TenantInfo tenant, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.CreateStorageService.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            StorageService actSS, expSS;
            string subscriptionId;

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            subscriptionId = ActiveResources.RandomGetSubscription(ResourceStatus.Idle).Id;
            AddTestInfoEntry("subscription", subscriptionId);
            QBarActionEnd();

            try
            {
                expSS = new StorageService()
                {
                    StorageServiceName = RDFECommons.RandomStorageServiceAccount(service, "rdfeqbar"),
                    StorageServiceLabel = RDFECommons.RandomString(50, true),
                    StorageServiceDescription = RDFECommons.RandomString(50, true),
                    GeoConstraint = new LocationConstraint() { Name = RDFECommons.RandomLocationConstraint(service, subscriptionId) }
                };
                AddTestInfoEntry("storageaccount", expSS.StorageServiceName);
                AddTestInfoEntry("location", ((LocationConstraint)expSS.GeoConstraint).Name);

                // Call to create Storage service
                PreAPICall();
                service.CreateStorageService(subscriptionId, expSS);
                PostAPICall("CreateStorageService");

                // Validation
                PreAPICall();
                actSS = service.GetStorageService(subscriptionId, expSS.StorageServiceName);
                PostAPICall("GetStorageService");
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateSubscriptionStatus(subscriptionId, ResourceStatus.Idle);
                throw ex;
            }

            DateTime createdAt;
            string puid;

            QBarActionStart();
            puid = service.GetSubscription(subscriptionId).Puid;
            createdAt = DateTime.Now.ToUniversalTime();
            ActiveResources.UpdateSubscriptionStatus(subscriptionId, ResourceStatus.Idle);
            ActiveResources.AddStorageAccount(puid, subscriptionId, expSS.StorageServiceName, createdAt);
            QBarActionEnd();
        
            return;
        }
    }
}
