﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class CreateHostedService:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;

        public CreateHostedService(TenantInfo tenant, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.CreateHostedService.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            HostedService actHS, expHS;
            string subscriptionId;

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            subscriptionId = ActiveResources.RandomGetSubscription(ResourceStatus.Idle).Id;
            AddTestInfoEntry("subscription", subscriptionId);
            QBarActionEnd();
            try
            {
                expHS = new HostedService()
                {
                    HostedServiceName = RDFECommons.RandomHostedServiceAccount(service, "rdfeqbar"),
                    HostedServiceLabel = RDFECommons.RandomString(50, true),
                    HostedServiceDescription = RDFECommons.RandomString(50, true),
                    GeoConstraint = new LocationConstraint() { Name = RDFECommons.RandomLocationConstraint(service, subscriptionId) }
                };
                AddTestInfoEntry("computeaccount", expHS.HostedServiceName);
                AddTestInfoEntry("location", ((LocationConstraint)expHS.GeoConstraint).Name);
                
                // Call to create Hosted service
                PreAPICall();
                service.CreateHostedService(subscriptionId, expHS);
                PostAPICall("CreateHostedService");

                // Validation 
                PreAPICall();
                actHS = service.GetHostedService(subscriptionId, expHS.HostedServiceName);
                PostAPICall("GetHostedService");
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateSubscriptionStatus(subscriptionId, ResourceStatus.Idle);
                throw ex;
            }
        
            DateTime createdAt;
            string puid;

            QBarActionStart();
            createdAt = DateTime.Now.ToUniversalTime();
            puid = service.GetSubscription(subscriptionId).Puid;
            ActiveResources.UpdateSubscriptionStatus(subscriptionId, ResourceStatus.Idle);
            ActiveResources.AddComputeAccount(puid, subscriptionId, expHS.HostedServiceName, createdAt);
            QBarActionEnd();
        
            return;        
        }
    }
}
