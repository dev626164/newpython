﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Net;
using System.Diagnostics;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class PutDeployment:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;
        private CommonInfo.ResourceInfo TestPackage; 
        private int NodeSize;

        public PutDeployment(TenantInfo tenant, CommonInfo.ResourceInfo package, int nodeSize, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.PutDeployment.ToString(), type, master)
        {
            TestTenant = tenant;
            TestPackage = package;
            NodeSize = nodeSize;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            CreateDeploymentInput input;
            string trackingId = null;
            //OperationState state;
            string status;

            IServiceManagement service;
            RDFEQBarResources.ComputeAccount compAcc;
            string temp;

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            compAcc = ActiveResources.RandomGetComputeAccount(ResourceStatus.Deploying);
            AddTestInfoEntry("subscription", compAcc.SubscriptionId);
            AddTestInfoEntry("computeaccount", compAcc.Name);
            ScenarioSubKey = TestPackage.Name;
            QBarActionEnd();

            try
            {
                QBarActionStart();
                if (compAcc.StagingDeployment != null)
                {
                    AddTestInfoEntry("oldstagingdeployment", service.GetDeployment(compAcc.SubscriptionId, compAcc.Name, compAcc.StagingDeployment.Name).Id);

                    RDFECommons.DeleteOneDeployment(service, compAcc.SubscriptionId, compAcc.Name, compAcc.StagingDeployment.Name);
                    ActiveResources.DeleteDeployment(compAcc.Name, compAcc.StagingDeployment.Name);
                }
                //2. deploy the package
                input = RDFECommons.CreateDeploymentInputInfo(TestPackage,
                    TestTenant.CompPkgBlobEndpoint, TestTenant.CompPkgAccount, TestTenant.CompPkgKey);
                QBarActionEnd();

                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    service.CreateDeployment(compAcc.SubscriptionId, compAcc.Name, input);
                    PostAPICall("CreateDeployment");
                    trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
                }
                PreAPICall();
                status = RDFECommons.WaitForAsyncOperation(service, compAcc.SubscriptionId, trackingId);
                PostAPICall("WaitCreateDeployment");
                AddTestInfoEntry("asyncstatus", status);
                QBarLogger.Log(DebugLevel.INFO, "CreateDeployment() with status {0}", status);
                if (status != OperationState.Succeeded.ToString())
                {
                    AddTestInfoEntry("errdetail", RDFECommons.GetTrackErrorDetail(service, compAcc.SubscriptionId, trackingId));
                    
                    temp = "PutDeployment: failed to create the deployment";
                    throw new Exception(temp);
                }
                QBarActionStart();
                ActiveResources.AddStagingDeployment(compAcc.Name, input.DeploymentName, NodeSize, input.PackageLocation, DateTime.Now.ToUniversalTime());
                AddTestInfoEntry("tenantid", service.GetDeployment(compAcc.SubscriptionId, compAcc.Name, input.DeploymentName).Id);
                QBarActionEnd();

                //3. run the deployment and get any status other than 'suspended'
                QBarLogger.Log(DebugLevel.INFO, "3. run the deployment");
                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    service.UpdateDeploymentStatus(compAcc.SubscriptionId, compAcc.Name, input.DeploymentName, DeploymentStatus.Running.ToString());
                    PostAPICall("UpdateDeploymentStatus");
                    trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
                }
                PreAPICall();
                status = RDFECommons.WaitForAsyncOperation(service, compAcc.SubscriptionId, trackingId);
                PostAPICall("WaitUpdateDeploymentStatus");
                AddTestInfoEntry("asyncstatus", status);
                QBarLogger.Log(DebugLevel.INFO, "TrackingId: {0} with status {1}", trackingId, status);
                if (status != OperationState.Succeeded.ToString())
                {
                    AddTestInfoEntry("errdetail", RDFECommons.GetTrackErrorDetail(service, compAcc.SubscriptionId, trackingId));

                    temp = "PutDeployment: failed to update the deployment to 'running'";
                    throw new Exception(temp);
                }

                //4. get all roles 'started'
                bool started;
                PreAPICall();
                started = RDFECommons.IsAllDeploymentInstancesStarted(service, compAcc.SubscriptionId, compAcc.Name, compAcc.StagingDeployment.Name, 120);
                PostAPICall("WaitAllStagingInstancesStarted");
                if (started == false)
                {
                    throw new Exception("PutDeployment: Cannot get all staging deployment instances into 'Started'");
                }

                //5. access compute resources in the staging
                QBarLogger.Log(DebugLevel.INFO, "5. access the index page in the stage");
                string stagingDns;
                stagingDns = RDFECommons.GetHostedServiceDns(service, compAcc.SubscriptionId, compAcc.Name, DeploymentKind.Staging);
                QBarLogger.Log(DebugLevel.INFO, "  stagingDns={0}", stagingDns);
                if (RDFECommons.IsTenantOnline(stagingDns, 120) == false)
                {
                    temp = "PutDeployment: Cannot get the index page from staging";
                    throw new Exception(temp);
                }

                //6. swap to product cluster
                QBarLogger.Log(DebugLevel.INFO, "6. swap to product cluster");
                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    service.PromoteDeploymentToProduction(compAcc.SubscriptionId, compAcc.Name, input.DeploymentName);
                    PostAPICall("PromoteDeploymentToProduction");
                    trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
                }
                PreAPICall();
                status = RDFECommons.WaitForAsyncOperation(service, compAcc.SubscriptionId, trackingId);
                PostAPICall("WaitPromoteDeploymentToProduction");
                AddTestInfoEntry("asyncstatus", status);
                if (status != OperationState.Succeeded.ToString())
                {
                    AddTestInfoEntry("errdetail", RDFECommons.GetTrackErrorDetail(service, compAcc.SubscriptionId, trackingId));

                    temp = "PutDeployment: Cannot promote deployment to production";
                    throw new Exception(temp);
                }
                QBarActionStart();
                ActiveResources.SwitchProductionStaging(compAcc.Name);
                QBarActionEnd();

                //7. get all roles 'started'
                PreAPICall();
                started = RDFECommons.IsAllDeploymentInstancesStarted(service, compAcc.SubscriptionId, compAcc.Name, compAcc.ProductDeployment.Name, 120);
                PostAPICall("WaitAllProductionInstancesStarted");
                if (started == false)
                {
                    throw new Exception("PutDeployment: Cannot get all production deployment instances into 'Started'");
                }

                //8. access compute resources in the staging
                QBarLogger.Log(DebugLevel.INFO, "8. access the index page in the production");
                string productionDns;
                productionDns = RDFECommons.GetHostedServiceDns(service, compAcc.SubscriptionId, compAcc.Name, DeploymentKind.Production);
                QBarLogger.Log(DebugLevel.INFO, "  productionDns={0}", productionDns);
                if (RDFECommons.IsTenantOnline(productionDns, 120) == false)
                {
                    temp = "PutDeployment: Cannot download the index page in production";
                    throw new Exception(temp);
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
            QBarActionEnd();

            return;
        }
    }
}
