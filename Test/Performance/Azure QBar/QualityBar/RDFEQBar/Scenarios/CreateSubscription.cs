﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class CreateSubscription:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;

        public CreateSubscription(TenantInfo tenant, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.CreateSubscription.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            string subscriptionId;
            Subscription sub;
            string puid;
            Int64 puidInt64;

            puidInt64 = (Int64)(((Int64)0xC8C8AAAA << 32) + new Random().Next(0,100000));
            puid = puidInt64.ToString("X");
            AddTestInfoEntry("puid", puid);

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            sub = new Subscription()
            {
                Puid = puid,
                SubscriptionName = Guid.NewGuid().ToString("N"),
                OfferType = "CTP"
            };
            PreAPICall();
            subscriptionId = service.CreateSubscription(sub);
            PostAPICall("CreateSubscription");
            sub.SubscriptionID = subscriptionId;
            AddTestInfoEntry("subscription", subscriptionId);

            QBarActionStart();
            ActiveResources.AddSubscription(puid, sub.SubscriptionName, subscriptionId, DateTime.Now.ToUniversalTime());
            QBarActionEnd();
        
            return;
        }
    }
}
