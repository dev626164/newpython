﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;
using System.ServiceModel.Web;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class DeleteDeployment:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        private RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;
        private int NodeSize;

        public DeleteDeployment(TenantInfo tenant, int nodeSize, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.DeleteDeployment.ToString(), type, master)
        {
            TestTenant = tenant;
            NodeSize = nodeSize;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            RDFEQBarResources.ComputeAccount compAcc;
            string trackingId, deploymentStatus, deploymentName;
            //OperationState state;
            string status;
            
            trackingId = string.Empty;
            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            compAcc = ActiveResources.RandomGetComputeAccountWithDeployment(NodeSize, ResourceStatus.Deleting);
            AddTestInfoEntry("computeaccount", compAcc.Name);
            AddTestInfoEntry("subscription", compAcc.SubscriptionId);
            QBarActionEnd();

            try
            {
                if (compAcc.ProductDeployment != null)
                {
                    deploymentName = compAcc.ProductDeployment.Name;
                    
                    ScenarioSubKey = compAcc.ProductDeployment.PackageUrl.Split('/')[4];
                    if (ScenarioSubKey.EndsWith(".cspkg")) ScenarioSubKey = ScenarioSubKey.Substring(0, ScenarioSubKey.Length - 6);
                    
                    AddTestInfoEntry("productiondeployment", deploymentName);
                }
                else if (compAcc.StagingDeployment != null)
                {
                    deploymentName = compAcc.StagingDeployment.Name;

                    ScenarioSubKey = compAcc.StagingDeployment.PackageUrl.Split('/')[4];
                    if (ScenarioSubKey.EndsWith(".cspkg")) ScenarioSubKey = ScenarioSubKey.Substring(0, ScenarioSubKey.Length - 6);
                    
                    AddTestInfoEntry("stagingdeployment", deploymentName);
                }
                else
                {
                    throw new Exception(WarningMessages.UnknownError);
                }
            
                // delete the deployment
                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    deploymentStatus = service.GetDeploymentStatus(compAcc.SubscriptionId, compAcc.Name, deploymentName);
                    PostAPICall("GetDeploymentStatus");

                    if (deploymentStatus == DeploymentStatus.Running.ToString())
                    {
                        PreAPICall();
                        service.UpdateDeploymentStatus(compAcc.SubscriptionId, compAcc.Name, deploymentName, DeploymentStatus.Suspended.ToString());
                        PostAPICall("UpdateDeploymentStatus");
                        trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
                    }
                    PreAPICall();
                    status = RDFECommons.WaitForAsyncOperation(service, compAcc.SubscriptionId, trackingId);
                    PostAPICall("WaitUpdateDeploymentStatus");
                    AddTestInfoEntry("asyncstatus", status);
                    if (status != OperationState.Succeeded.ToString())
                    {
                        AddTestInfoEntry("errdetail", RDFECommons.GetTrackErrorDetail(service, compAcc.SubscriptionId, trackingId));
                        throw new Exception("Cannot suspend the deployment");
                    }
                }
                QBarActionStart();
                AddTestInfoEntry("tenantid", service.GetDeployment(compAcc.SubscriptionId, compAcc.Name, deploymentName).Id);
                QBarActionEnd();

                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    service.DeleteDeployment(compAcc.SubscriptionId, compAcc.Name, deploymentName);
                    PostAPICall("DeleteDeployment");
                    trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
                }
                PreAPICall();
                status = RDFECommons.WaitForAsyncOperation(service, compAcc.SubscriptionId, trackingId);
                PostAPICall("WaitDeleteDeployment");
                AddTestInfoEntry("asyncstatus", status);
                if (status != OperationState.Succeeded.ToString()) 
                {
                    AddTestInfoEntry("errdetail", RDFECommons.GetTrackErrorDetail(service, compAcc.SubscriptionId, trackingId)); 
                    throw new Exception("Cannot delete the deployment");
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.DeleteDeployment(compAcc.Name, deploymentName);
            ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
            QBarActionEnd();

            return;
        }
    }
}
