﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;

namespace Microsoft.Cis.E2E.QualityBar
{
    class DeleteSubscription:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;

        public DeleteSubscription(TenantInfo tenant, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.DeleteSubscription.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            string subscriptionId;
            //string userInfo;

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            subscriptionId = ActiveResources.RandomGetSubscription(ResourceStatus.Deleting).Id;
            AddTestInfoEntry("subscription", subscriptionId);
            QBarActionEnd();

            //delete the subscription
            QBarActionStart();
            ActiveResources.DeleteSubscription(subscriptionId);
            QBarActionEnd();
        
            return;
        }
    }
}
