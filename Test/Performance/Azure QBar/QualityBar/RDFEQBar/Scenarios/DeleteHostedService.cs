﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;
using System.ServiceModel.Web;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class DeleteHostedService:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;

        public DeleteHostedService(TenantInfo tenant, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.DeleteHostedService.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            RDFEQBarResources.ComputeAccount compAcc;

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            compAcc = ActiveResources.RandomGetComputeAccount(ResourceStatus.Deleting);
            QBarLogger.Log(DebugLevel.INFO, "random get hosted service {0}", compAcc.Name);
            AddTestInfoEntry("subscription", compAcc.SubscriptionId);
            AddTestInfoEntry("computeaccount", compAcc.Name);
            QBarActionEnd();

            try
            {
                QBarActionStart();
                if (compAcc.StagingDeployment != null)
                {
                    AddTestInfoEntry("stagingdeployment", service.GetDeployment(compAcc.SubscriptionId, compAcc.Name, compAcc.StagingDeployment.Name).Id);

                    RDFECommons.DeleteOneDeployment(service, compAcc.SubscriptionId, compAcc.Name, compAcc.StagingDeployment.Name);
                    ActiveResources.DeleteDeployment(compAcc.Name, compAcc.StagingDeployment.Name);
                }
                if (compAcc.ProductDeployment != null)
                {
                    AddTestInfoEntry("productiondeployment", service.GetDeployment(compAcc.SubscriptionId, compAcc.Name, compAcc.ProductDeployment.Name).Id);

                    RDFECommons.DeleteOneDeployment(service, compAcc.SubscriptionId, compAcc.Name, compAcc.ProductDeployment.Name);
                    ActiveResources.DeleteDeployment(compAcc.Name, compAcc.ProductDeployment.Name);
                }
                QBarActionEnd();

                // Call to create Hosted service
                PreAPICall();
                service.DeleteHostedService(compAcc.SubscriptionId, compAcc.Name);
                PostAPICall("DeleteHostedService");
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.DeleteHostedService(compAcc.Name);
            QBarActionEnd();
        
            return;        
        }
    }
}
