﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Xml;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class UpdateDeploymentConfig:QBarWorkItem
    {
        public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        TenantInfo TestTenant;
        int NodeSize;
        int NewNodeSize;

        public UpdateDeploymentConfig(TenantInfo tenant, int nodeSize, int newNodeSize, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.UpdateDeploymentConfig.ToString(), type, master)
        {
            TestTenant = tenant;
            NodeSize = nodeSize;
            NewNodeSize = newNodeSize;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            RDFEQBarResources.ComputeAccount compAcc;
            string trackingId;
            //OperationState state;
            string status;
            Deployment deployment;

            trackingId = string.Empty;
            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            compAcc = ActiveResources.RandomGetComputeAccountWithProduction(NodeSize, ResourceStatus.Updating);
            AddTestInfoEntry("subscription", compAcc.SubscriptionId);
            AddTestInfoEntry("computeaccount", compAcc.Name);
            QBarActionEnd();

            try
            {
                ScenarioSubKey = compAcc.ProductDeployment.PackageUrl.Split('/')[4];
                if (ScenarioSubKey.EndsWith(".cspkg")) ScenarioSubKey = ScenarioSubKey.Substring(0, ScenarioSubKey.Length - 6);

                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    deployment = service.GetDeployment(compAcc.SubscriptionId, compAcc.Name, compAcc.ProductDeployment.Name);
                    PostAPICall("GetDeployment");

                    AddTestInfoEntry("tenantid", deployment.Id);
                }

                XmlDocument xmlSettings = new XmlDocument();
                xmlSettings.Load(new StringReader(deployment.Settings));
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlSettings.NameTable);
                nsMgr.AddNamespace("SC", "http://schemas.microsoft.com/ServiceHosting/2008/10/ServiceConfiguration");

                //Increase the number of tenants for the web role
                xmlSettings.SelectSingleNode("/SC:ServiceConfiguration/SC:Role[@name='WebRole']/SC:Instances/@count", nsMgr).Value = NewNodeSize.ToString();

                UpdateDeploymentInput input;
                input = new UpdateDeploymentInput()
                {
                    DeploymentDescription = "update this deployment for quality bar",
                    DeploymentLabel = "v2",
                    Settings = xmlSettings.OuterXml
                };

                //Get XML and Change the Config
                using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
                {
                    PreAPICall();
                    service.UpdateDeployment(compAcc.SubscriptionId, compAcc.Name, deployment.Name, input);
                    PostAPICall("UpdateDeployment");
                    trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
                }
                PreAPICall();
                status = RDFECommons.WaitForAsyncOperation(service, compAcc.SubscriptionId, trackingId);
                PostAPICall("WaitUpdateDeployment");
                AddTestInfoEntry("asyncstatus", status);
                if (status != OperationState.Succeeded.ToString())
                {
                    AddTestInfoEntry("errdetail", RDFECommons.GetTrackErrorDetail(service, compAcc.SubscriptionId, trackingId));

                    throw new Exception("Cannot update deployment");
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.UpdateHostedServiceStatus(compAcc.Name, ResourceStatus.Idle);
            QBarActionEnd();
        
            return;
        }
    }
}
