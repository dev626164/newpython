﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;
using System.ServiceModel.Web;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class DeleteStorageService:QBarWorkItem
    {
         public RDFEScenarios ItemName
        {
            get { return (RDFEScenarios)Enum.Parse(typeof(RDFEScenarios), ScenarioName); }
        }
        RDFEQBarResources ActiveResources;
        private TenantInfo TestTenant;

        public DeleteStorageService(TenantInfo tenant, WorkItemType type, RDFEQBarResources resources, DuplexQueue master)
            : base(RDFEScenarios.DeleteStorageService.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            IServiceManagement service;
            RDFEQBarResources.StorageAccount storageAcc;

            service = RDFECommons.GetServiceInstance(TestTenant.Endpoint, @".\temp\" + TestTenant.HttpsCertFile, TestTenant.HttpsCertPassword);
            QBarActionStart();
            storageAcc = ActiveResources.RandomGetStorageAccount(ResourceStatus.Deleting);
            AddTestInfoEntry("subscription", storageAcc.SubscriptionId);
            AddTestInfoEntry("storageaccount", storageAcc.Name);
            QBarActionEnd();

            try
            {
                // Call to create Hosted service
                PreAPICall();
                service.DeleteStorageService(storageAcc.SubscriptionId, storageAcc.Name);
                PostAPICall("DeleteStorageService");
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateStorageServiceStatus(storageAcc.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.DeleteStorageService(storageAcc.Name);
            QBarActionEnd();
    
            return;        
        }
    }
}
