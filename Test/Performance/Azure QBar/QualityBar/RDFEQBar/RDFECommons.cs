﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Microsoft.Cis.DevExp.Services.Rdfe.ServiceManagement;
using System.ServiceModel.Web;
using System.Diagnostics;
using System.ServiceModel;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class RDFECommons
    {
        public static string TrackingHeaderName = "operation-tracking-id";
        static ServiceManagementChannelFactory ResourceModelServiceFactory = null;
        //https://admin.e2e1.dnsdemo4.com
        public static IServiceManagement GetServiceInstance(string RDFEEndpoint, string certFile, string certPassword) 
        {
            IServiceManagement channel;
            Uri RDFEAdminUri;

            if (RDFEEndpoint.StartsWith("https://"))
            {
                X509Certificate2 certificate;
                WebHttpBinding binding;
                X509Store store;

                RDFEAdminUri = new Uri(RDFEEndpoint + "/ServiceManagementSSL.svc");

                binding = null;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(RemoteCertValidate);
                binding = new WebHttpBinding(WebHttpSecurityMode.Transport);
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

                store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadWrite);
                certificate = new X509Certificate2(certFile, certPassword);

                if (ResourceModelServiceFactory == null || ResourceModelServiceFactory.Endpoint.Address.Uri != RDFEAdminUri)
                {
                    ResourceModelServiceFactory = new ServiceManagementChannelFactory(binding, RDFEAdminUri);
                    ResourceModelServiceFactory.Credentials.ClientCertificate.Certificate = certificate;
                }
            }
            else if (RDFEEndpoint.StartsWith("http://"))
            {
                RDFEAdminUri = new Uri(RDFEEndpoint + "/ServiceManagement.svc");

                if (ResourceModelServiceFactory == null || ResourceModelServiceFactory.Endpoint.Address.Uri != RDFEAdminUri)
                {
                    ResourceModelServiceFactory = new ServiceManagementChannelFactory(RDFEAdminUri);
                }
            }
            //For various reasons, we want to return a new channel when this is called.  This allows tests to be atomic
            //and prevents faults in one test from affecting the service used by another
            channel = ResourceModelServiceFactory.CreateChannel();
            channel.ToContextChannel().OperationTimeout = TimeSpan.FromMinutes(3);
            
            if (channel == null)
            {
                throw new Exception(string.Format("Cannot get service instance from endpoint {0}", RDFEEndpoint));
            }

            return channel;
        }

        static bool RemoteCertValidate(object sender, X509Certificate cert, X509Chain chain, System.Net.Security.SslPolicyErrors error)
        {
            return true;
        }

        public static CreateDeploymentInput CreateDeploymentInputInfo(CommonInfo.ResourceInfo package, string pkgEndpoint, string pkgAccount, string pkgKey)
        {
            CreateDeploymentInput input;
            string /*tempDir, */container, computePackage, computeConfig, configXMLString;
            XmlDocument doc;
            string configXml;

            //tempDir = @".\temp";
            //if (!Directory.Exists(tempDir)) Directory.CreateDirectory(tempDir);

            container = package.Container;
            computePackage = package.Blob + ".cspkg";
            computeConfig = package.Blob + ".cscfg";
            //if (File.Exists(@".\temp\" + computePackage) == false || File.Exists(@".\temp\" + computeConfig) == false)
            //{
            //    try
            //    {
            //        CloudWork.GetFileBlob(container, computePackage, @".\temp\" + computePackage);
            //        CloudWork.GetFileBlob(container, computeConfig, @".\temp\" + computeConfig);
            //    }
            //    catch (IOException)
            //    {
            //        QBarLogger.Log(DebugLevel.INFO, "The file is downloaded by other worker thread");
            //    }
            //}
            configXMLString = CloudWork.GetStringBlob(container, computeConfig);

            doc = new XmlDocument();
            doc.LoadXml(configXMLString);
            //doc.Load(@".\temp\"+computeConfig);
            configXml = doc.DocumentElement.OuterXml;
            
            input = new CreateDeploymentInput()
            {
                DeploymentName = RandomAccountName("rdfegeo"),
                DeploymentLabel = "RDFEQualityBar", 
                DeploymentDescription = "refe quality bar",
                Settings = configXml,
                //PackageLocation = "http://rdfeprodtests.blob.core.windows.net/packages/" + computePackage
                //PackageLocation = PutComputePackage(computePackage, @".\temp\" + computePackage, pkgEndpoint, pkgAccount, pkgKey)
                PackageLocation = string.Format(@"http://{0}.{1}/{2}/{3}", pkgAccount, pkgEndpoint, "e2epackages", computePackage)
                //"http://lpspackages.blob.e2e1.dnsdemo4.com/f45f122d46884e338cdc70b247c82737/E2ETest.cspkg"
            };

            return input;
        }

        public static string PutComputePackage(string packageName, string packageFileName, string pkgEndpoint, string pkgAccount, string pkgKey)
        {
            string container, packageUrl;
            bool success;

            container = "e2epackages";
            success = CloudWork.DoesBlobExist("http://" + pkgEndpoint, pkgAccount, pkgKey, container, packageName);
            if(success == false)
            {
                try
                {
                    success = PutFileBlob("http://" + pkgEndpoint, pkgAccount, pkgKey, container, packageName, packageFileName);
                }
                catch (IOException) //other process will do this
                {
                    success = true;
                    QBarLogger.Log(DebugLevel.INFO, "The file is clouded by other worker thread");
                }
            }

            packageUrl = string.Empty;
            if (success)
            {
                packageUrl = string.Format(@"http://{0}.{1}/{2}/{3}", pkgAccount, pkgEndpoint, container, packageName);
                QBarLogger.Log(DebugLevel.INFO, "Compute package url: {0}", packageUrl);
            }
            return packageUrl;
        }

        public static bool PutFileBlob(string endpoint, string account, string key, string container, string blob, string file)
        {
            MemoryStream content;

            using (FileStream fs = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                byte[] buf;
                int size, len;

                len = (int)fs.Length;
                buf = new byte[fs.Length];
                size = fs.Read(buf, 0, len);
                content = new MemoryStream(buf);
            }

            return CloudWork.PutBlob(endpoint, account, key, container, blob, content);
        }

        public static string WaitForAsyncOperation(IServiceManagement service, string subscriptionId, string trackingId)
        {
            OperationTracking tracking;
            System.Diagnostics.Stopwatch watch;
            string status;

            if (trackingId == string.Empty) return "Tracking Id is empty";

            status = string.Empty;
            tracking = null;
            watch = System.Diagnostics.Stopwatch.StartNew();
            watch.Start();
            while (watch.Elapsed.TotalMinutes < 120)
            {
                System.Threading.Thread.Sleep(5*1000);
                try
                {
                    tracking = service.GetResult(subscriptionId, trackingId);
                    if (tracking.OperationStatus == OperationState.Failed.ToString()
                        || tracking.OperationStatus == OperationState.Succeeded.ToString())
                    {
                        status = tracking.OperationStatus;
                        break;
                    }

                    tracking = null;
                }
                catch (Exception ex)
                {
                    tracking = null;
                    status = ex.ToString();
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                    break;
                }
            }
            watch.Stop();
            //QBarLogger.Log(DebugLevel.INFO, "Elapsed seconds: {0}", watch.Elapsed.TotalSeconds);

            //time out
            if (tracking == null && string.IsNullOrEmpty(status) == true)
            {
                status = "Cannot finish within 120 minutes";
            }

            if (tracking != null && tracking.OperationStatus == OperationState.Failed.ToString())
            {
                QBarLogger.Log(DebugLevel.WARN, "Reason: {0}, {1}", tracking.ErrorDetail, tracking.ExtensionData);
            }

            return status; 
            //tracking == null ? OperationState.Failed : (OperationState)Enum.Parse(typeof(OperationState), tracking.OperationStatus);
        }

        public static string GetTrackErrorDetail(IServiceManagement service, string subscriptionId, string trackingId)
        {
            string temp;
            OperationTracking tracking;

            temp = string.Empty;
            try
            {
                tracking = service.GetResult(subscriptionId, trackingId);
                if (tracking != null) temp = tracking.ErrorDetail.ToString();
            }
            catch (Exception ex)
            {
                temp = ex.ToString();
            }

            return temp;
        }

        public static bool IsAllDeploymentInstancesStarted(IServiceManagement service, string
            subscriptionId, string hostedServiceName, string deploymentName,
            int waitInMinutes)
        {
            Deployment deploy;
            bool rolesReady, success;
            Stopwatch watch;

            success = true;
            watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalMinutes < waitInMinutes)
            {
                rolesReady = true;
                deploy = service.GetDeployment(subscriptionId, hostedServiceName, deploymentName);
                foreach (RoleInstance ri in deploy.RoleInstances)
                {
                    //Console.WriteLine("Role {0}: {1}", ri.RoleName, ri.RoleState);
                    if (RoleState.RoleStateStarted != ri.RoleState)
                    {
                        rolesReady = false;
                        break;
                    }

                    if (RoleState.RoleStateAborted == ri.RoleState
                        || RoleState.RoleStateUnresponsive == ri.RoleState
                        || RoleState.RoleStateUnhealthy == ri.RoleState
                        || RoleState.RoleStateDestroyed == ri.RoleState
                        || RoleState.RoleStateTeardown == ri.RoleState)
                    {
                        success = false;
                        break;
                    }
                }
                if (rolesReady == true)
                {
                    break;
                }

                if (success == false)
                {
                    break;
                }

                System.Threading.Thread.Sleep(20 * 1000);
            }

            return success;
        }

        public static bool IsTenantOnline(string tenantDns, int waitInMinutes)
        {
            Stopwatch watch;
            string content;
            WebClient webClient;
            bool success;

            if (tenantDns == string.Empty)
            {
                return false;
            }

            if (tenantDns.Contains("http://") == false)
            {
                tenantDns = "http://" + tenantDns;
            }

            QBarLogger.Log(DebugLevel.INFO, "DetectTenantOnline: {0}", tenantDns);

            success = false;
            webClient = new WebClient();
            watch = Stopwatch.StartNew();
            while (watch.Elapsed.TotalMinutes < waitInMinutes)
            {
                System.Threading.Thread.Sleep(10000);
                try
                {
                    content = webClient.DownloadString(tenantDns);
                    if (content != string.Empty && content.Length != 0)
                    {
                        success = true;
                    }
                    break;
                }
                catch (WebException)
                {
                    //QBarLogger.Log(DebugLevel.EXCEPTION, "WebException: try to download page {0}", webClient.BaseAddress);
                }
                catch (Exception ex)
                {
                    QBarLogger.Log(DebugLevel.EXCEPTION, ex.ToString());
                    break;
                }
            }
            watch.Stop();
            //QBarLogger.Log(DebugLevel.INFO, "Elapsed seconds: {0}", watch.Elapsed.TotalSeconds);
            if (watch.Elapsed.TotalMinutes >= waitInMinutes)
            {
                success = false;
            }

            return success;
        }

        public static string GetHostedServiceDns(IServiceManagement service, string subscriptionId, string hostedServiceName, DeploymentKind kind)
        {
            string dns;
            HostedService hs;

            dns = string.Empty;
            hs = service.GetHostedServiceWithDetails(subscriptionId, hostedServiceName, DetailLevel.DeploymentDetails.ToString());
            if (hs.Deployments != null && hs.Deployments.Count != 0)
            {
                foreach (Deployment d in hs.Deployments)
                {
                    if(d.Kind == kind.ToString())
                    {
                        dns = d.DnsName;
                        break;
                    }
                }
            }

            return dns;
        }

        public static string GetDeploymentName(IServiceManagement service, string subscriptionId, string hostedServiceName, DeploymentKind kind)
        {
            string deploymentName;
            HostedService hs;

            deploymentName = string.Empty;
            hs = service.GetHostedServiceWithDetails(subscriptionId, hostedServiceName, DetailLevel.DeploymentDetails.ToString());
            if (hs.Deployments != null && hs.Deployments.Count != 0)
            {
                foreach (Deployment d in hs.Deployments)
                {
                    if (d.Kind == kind.ToString())
                    {
                        deploymentName = d.Name;
                        break;
                    }
                }
            }

            return deploymentName;
        }

        public static bool DeleteOneDeployment(IServiceManagement service, string subscriptionId, string hostedServiceName, string deploymentName)
        {
            bool success;
            string trackingId;
            //OperationState state;
            string status;

            success = true;
            using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
            {
                service.UpdateDeploymentStatus(subscriptionId, hostedServiceName, deploymentName, DeploymentStatus.Suspended.ToString());
                trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
            }
            status = RDFECommons.WaitForAsyncOperation(service, subscriptionId, trackingId);
            if (status != OperationState.Succeeded.ToString())
            {
                throw new Exception(string.Format("Cannot suspend the deployment {0}", deploymentName));                
            }

            using (OperationContextScope scope = new OperationContextScope(service.ToContextChannel()))
            {
                service.DeleteDeployment(subscriptionId, hostedServiceName, deploymentName);
                trackingId = WebOperationContext.Current.IncomingResponse.Headers[RDFECommons.TrackingHeaderName];
            }
            status = RDFECommons.WaitForAsyncOperation(service, subscriptionId, trackingId);
            if (status != OperationState.Succeeded.ToString())
            {
                throw new Exception("Cannot delete the deployment");
            }

            return success;
        }

        public static bool DeleteDeploymentsForOneHostedService(IServiceManagement service, string subscriptionId, string hostedServiceName)
        {
            HostedService hs;
            bool success;
            
            success = true;
            hs = service.GetHostedServiceWithDetails(subscriptionId, hostedServiceName, DetailLevel.DeploymentDetails.ToString());
            if (hs.Deployments == null || hs.Deployments.Count == 0) return true;

            foreach (Deployment d in hs.Deployments)
            {
                DeleteOneDeployment(service, subscriptionId, hostedServiceName, d.Name);
            }
        
            return success;
        }

        public static string RandomLocationConstraint(IServiceManagement service, string subscriptionId)
        {
            string location;
            Subscription sub;
            LocationConstraintList locList;

            locList = null;
            location = string.Empty;
            sub = service.GetSubscription(subscriptionId);
            if(sub != null)
            {
               locList = sub.LocationConstraints;
            }

            if (locList != null)
            {
                int index;

                //QBarLogger.Log(DebugLevel.INFO, "There are {0} location constraints", locList.Count);
                index = new Random().Next(0, locList.Count);
                location = locList.ToArray()[index].Name;
            }

            return location;
        }

        public static string RandomAccountName(string prefix)
        {
            return (prefix + Guid.NewGuid().ToString("N").Substring(16, 10)).ToLower();
        }

        public static string RandomHostedServiceAccount(IServiceManagement service, string prefix)
        {
            string account;

            account = (prefix + Guid.NewGuid().ToString("N").Substring(16, 10)).ToLower();
            while(service.IsDNSAvailable(account) == false)
            {
                account = (prefix + Guid.NewGuid().ToString("N").Substring(16, 10)).ToLower();
            }
            
            return account;
        }

        public static string RandomStorageServiceAccount(IServiceManagement service, string prefix)
        {
            string account;

            account = (prefix + Guid.NewGuid().ToString("N").Substring(16, 10)).ToLower();
            while (service.DoesStorageServiceExist(account) == true)
            {
                account = (prefix + Guid.NewGuid().ToString("N").Substring(16, 10)).ToLower();
            }

            return account;
        }

        public static string RandomString(int len, bool lowerCase)
        {
            StringBuilder builder;
            char ch;
            Random rand;

            rand = new Random();
            builder = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(rand.Next(0, 26) + 65));
                builder.Append(ch);
            }
            if (lowerCase) return builder.ToString().ToLower();
            return builder.ToString();
        }
    }
}
