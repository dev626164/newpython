﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Configuration;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class ApiPerf
    {
        public string Name;
        public double Duration;
    }

    public class ReportEntry
    {
        public string Worker;
        public RDFEScenarios Scenario;
        public WorkItemType ItemType;
        public bool Status;
        public double Duration;
        public DateTime FinishedAt;
        public LinkedList<ApiPerf> APIs;
        public string ExceptionInfo;

        public string Report
        {
            get 
            {
                StringBuilder sb;

                sb = new StringBuilder();
                sb.Append(ProtocolHelper.BuildMultipleFields(
                    new string[] { "worker", "scenario", "qbartype", "status", "duration", "finishedat" },
                    new object[] { Worker, Scenario.ToString(), ItemType.ToString(), Status, Duration, FinishedAt }
                    ));
                foreach(ApiPerf api in APIs)
                {
                    sb.Append(ProtocolHelper.BuildHeadFields("apiperf",
                        new string[] { "name", "duration" },
                        new object[] { api.Name, api.Duration }
                        ));
                }

                if (ExceptionInfo != null && ExceptionInfo != string.Empty)
                {
                    sb.Append(ProtocolHelper.BuildOneField("exception", ExceptionInfo));
                }

                return ProtocolHelper.BuildOneField("report", sb.ToString());
            }
        }

        public ReportEntry(string worker, RDFEScenarios scenario, WorkItemType type, bool status, double duration, DateTime finishedAt, string exception)
        {
            Worker = worker;
            Scenario = scenario;
            ItemType = type;
            Status = status;
            Duration = duration;
            FinishedAt = finishedAt;
            APIs = new LinkedList<ApiPerf>();
            ExceptionInfo = exception;
        }

        public void AddApiPerf(string name, double duration)
        {
            APIs.AddFirst(new ApiPerf(){ Name = name, Duration = duration});
            return;
        }
    }

    public class RDFEQBarReport
    {
        LinkedList<ReportEntry> ActiveReports;
        private Timer CloudWorkTimer;
        private string RunId;

        public RDFEQBarReport()
        {
            ActiveReports = new LinkedList<ReportEntry>();
            RunId = Guid.NewGuid().ToString("N");
            PutCurrentRun(RunId);

            CloudWorkTimer = new Timer(
                new TimerCallback(this.PutDataIntoCloud),
                null,
                0,
                5 * 60 * 1000 //every 5 minutes
                );
        }

        private void PutCurrentRun(string runId)
        {
            string reportContainer, reportBlob;

            reportContainer = ConfigurationSettings.AppSettings["QualityBarReportContainer"];
            reportBlob = ConfigurationSettings.AppSettings["QualityBarReportActiveRunsPrefix"] + "/" + runId;

            CloudWork.PutStringBlob(reportContainer, reportBlob, string.Empty, true);

            return;
        }

        public void PutDataIntoCloud(Object stateInfo)
        {
            DateTime curr;
            TimeSpan span;
            LinkedList<ReportEntry> retired;
            StringBuilder report;
            string qbarcontainer;

            if (ActiveReports.Count == 0) return;

            curr = DateTime.Now.ToUniversalTime();
            QBarLogger.Log(DebugLevel.INFO, "Put measure reports to cloud at {0}", curr);

            report = new StringBuilder();
            retired = new LinkedList<ReportEntry>();
            lock (ActiveReports)
            {
                foreach (ReportEntry entry in ActiveReports)
                {
                    span = curr - entry.FinishedAt;
                    if (span.TotalSeconds >= 10)
                    {
                        retired.AddFirst(entry);
                        report.Append(entry.Report);
                    }
                }

                if (retired.Count != 0)
                {
                    foreach (ReportEntry entry in retired)
                    {
                        ActiveReports.Remove(entry);
                    }
                }
            }

            if (report.Length != 0)
            {
                qbarcontainer = System.Configuration.ConfigurationSettings.AppSettings["QualityBarReportContainer"];
                CloudWork.PutStringBlob(qbarcontainer, RunId + "/"+ curr.ToString(), report.ToString(), true);
            }

            return;
        }

        public void AddOneReportEntry(ReportEntry entry)
        {
            lock (ActiveReports)
            {
                ActiveReports.AddFirst(entry);
            }

            return;
        }
    }
}
