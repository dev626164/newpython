﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class CommonInfo
    {
        public class ResourceInfo
        {
            public string Name;
            public ResourceTypes Type;
            public string Container;
            public string Blob;
        }

        public int UnitTimeInSeconds;
        public int AverageUnitsForMeasure;
        public int PeakUnitsForMeasure;
        public ResourceInfo HttpsCert;
        public LinkedList<ResourceInfo> ComputePackages;

        public CommonInfo()
        {
            ComputePackages = new LinkedList<ResourceInfo>();
        }

        public void AddResource(string type, string name, string container, string blob)
        {
            ResourceTypes resType;
            ResourceInfo info;

            resType = (ResourceTypes)Enum.Parse(typeof(ResourceTypes), type);
            info = new ResourceInfo()
                    {
                        Type = resType,
                        Name = name,
                        Container = container,
                        Blob = blob
                    };

            if (resType == ResourceTypes.ComputePackage)
            {
                ComputePackages.AddFirst(info);
            }
            else if (resType == ResourceTypes.CertFile)
            {
                HttpsCert = info;
            }
        }

        public ResourceInfo RandomGetOnePackage()
        {
            ResourceInfo ret;
            int i, r;

            r = new Random().Next(1, ComputePackages.Count);

            i = 1;
            ret = null;
            foreach (ResourceInfo ri in ComputePackages)
            {
                if (i == r)
                {
                    ret = ri;
                    break;
                }

                i++;
            }

            return ret;
        }

        public ResourceInfo GetPackage(string packageName)
        {
            ResourceInfo ret;

            ret = null;
            foreach (ResourceInfo ri in ComputePackages)
            {
                if (ri.Name == packageName)
                {
                    ret = ri;
                    break;
                }
            }

            return ret;
        }

        public void DownloadResources()
        {
            string tempDir;
            string container, computePackage, computeConfig;

            tempDir = @".\temp";
            if (!Directory.Exists(tempDir)) Directory.CreateDirectory(tempDir);

            foreach (ResourceInfo ri in ComputePackages)
            {
                container = ri.Container;
                computePackage = ri.Blob + ".cspkg";
                computeConfig = ri.Blob + ".cscfg";
                CloudWork.GetFileBlob(container, computePackage, @".\temp\" + computePackage);
                CloudWork.GetFileBlob(container, computeConfig, @".\temp\" + computeConfig);
            }

            CloudWork.GetFileBlob(HttpsCert.Container, HttpsCert.Blob, @".\temp\" + HttpsCert.Name);

            return;
        }

        public void PutComputePackages(string blobEndpoint, string account, string accountKey)
        {
            string container, computePackage;

            foreach (ResourceInfo ri in ComputePackages)
            {
                container = ri.Container;
                computePackage = ri.Blob + ".cspkg";
                
                RDFECommons.PutComputePackage(computePackage, @".\temp\" + computePackage, blobEndpoint, account, accountKey);
            }

            return;
        }
    }

    public class TenantInfo
    {
        public string Type;
        public string FriendlyName;
        public string Endpoint;
        public string HttpsCertFile;
        public string HttpsCertPassword;
        public string CompPkgBlobEndpoint;
        public string CompPkgAccount;
        public string CompPkgKey;
        public int NumInstances;
        public int ClusterUsableNodes;
        public double Scale;
    }

    public class PrePhase
    {
        //<name, count> pair
        public int Index;
        public Dictionary<string, int> PreResources;

        public PrePhase()
        {
            Index = -1;
            PreResources = new Dictionary<string, int>();
        }
    }

    public class WorkloadScenario
    {
        public string Name;
        public int AverageRequestNumber;
        public int PeakRequestNumber;
        public Dictionary<string, string> ParamList;

        public WorkloadScenario()
        {
            Name = string.Empty;
            AverageRequestNumber = 0;
            PeakRequestNumber = 0;
            ParamList = new Dictionary<string, string>();
        }
    }

    public class WorkloadInfo
    {
        public int TotalConcurrent;
        public int AccountConcurrent;
        public LinkedList<WorkloadScenario> ScenarioList;

        public WorkloadInfo()
        {
            TotalConcurrent = 0;
            AccountConcurrent = 0;
            ScenarioList = new LinkedList<WorkloadScenario>();
        }

        public void AddScenario(string name, int average, int peak, Dictionary<string,string> parameters)
        {
            ScenarioList.AddLast(new WorkloadScenario()
            {
                Name = name,
                AverageRequestNumber = average,
                PeakRequestNumber = peak,
                ParamList = parameters
            });
        }
    }

    public class RDFEQBarLoad : QBarLoad
    {
        public CommonInfo Common;
        public TenantInfo Tenant;
        public LinkedList<PrePhase> PrePhaseList;
        public WorkloadInfo Workload;

        public RDFEQBarLoad()
            : base()
        {
            Common = null;
            Tenant = null;
            PrePhaseList = null;
            Workload = null;
        }

        public RDFEQBarLoad(string loadfile, DuplexQueue manager)
            : base(loadfile, manager)
        {
        }

        protected override void ParseCommon(XmlNode nodeCommon)
        {
            XmlNodeList resourceNodes;
            
            Common = new CommonInfo()
            {
                UnitTimeInSeconds = int.Parse(GetNodeValue(nodeCommon, "UnitTimeInSeconds", "600")),
                AverageUnitsForMeasure = int.Parse(GetNodeValue(nodeCommon, "AverageUnitsForMeasure", "1")),
                PeakUnitsForMeasure = int.Parse(GetNodeValue(nodeCommon, "PeakUnitsForMeasure", "0"))
            };

            resourceNodes = nodeCommon.SelectNodes("Resource");
            foreach (XmlNode node in resourceNodes)
            {
                Common.AddResource(
                    GetNodeValue(node, "Type", ""),
                    GetNodeValue(node, "Name", ""),
                    GetNodeValue(node, "Container", ""),
                    GetNodeValue(node, "Blob", ""));
            }

            return;
        }

        protected override void ParseTenant(XmlNode nodeTenant)
        {
            XmlNode node;

            node = nodeTenant.SelectSingleNode("ComputePackageLocation");
            Tenant = new TenantInfo()
            {
                Type = GetAttributeValue(nodeTenant, "Type", ""),
                FriendlyName = GetAttributeValue(nodeTenant, "FriendlyName", ""),
                Endpoint = GetNodeValue(nodeTenant, "AccessPoint", ""),
                HttpsCertFile = GetNodeValue(nodeTenant, "SecureCert", ""),
                HttpsCertPassword = GetNodeValue(nodeTenant, "SecureCertPassword", ""),
                NumInstances = int.Parse(GetNodeValue(nodeTenant, "InstanceCount", "1")),
                ClusterUsableNodes = int.Parse(GetNodeValue(nodeTenant, "ClusterUsableNodes", "1")),
                Scale = double.Parse(GetNodeValue(nodeTenant, "Scale", "1.0")),

                CompPkgBlobEndpoint = GetNodeValue(node, "BlobEndpoint", ""),
                CompPkgAccount = GetNodeValue(node, "AccountName", ""),
                CompPkgKey = GetNodeValue(node, "AccountKey", "")
            };

            Common.DownloadResources();
            Common.PutComputePackages(Tenant.CompPkgBlobEndpoint, Tenant.CompPkgAccount, Tenant.CompPkgKey);

            return;
        }
        protected override void ParsePrecondition(XmlNode nodePrecondition)
        {
            XmlNodeList nodeList, resourceList;
            int count;

            PrePhaseList = new LinkedList<PrePhase>();

            nodeList = nodePrecondition.SelectNodes("Phase");
            foreach (XmlNode node in nodeList)
            {
                PrePhase phase;

                phase = new PrePhase();
                phase.Index = int.Parse(GetAttributeValue(node, "Index", "0"));

                resourceList = node.SelectNodes("Resource");
                foreach (XmlNode node2 in resourceList)
                {
                    count = (int)(int.Parse(GetNodeValue(node2, "Count", "0")) * Tenant.Scale);
                    phase.PreResources.Add(GetAttributeValue(node2, "Name", ""), count);
                }
                PrePhaseList.AddFirst(phase);
            }

            //Console.WriteLine("There are {0} phases", PrePhaseList.Count);

            return;
        }
        protected override void ParseWorkload(XmlNode nodeWorkload)
        {
            XmlNode concurrentNode;
            XmlNodeList scenarioNodes, parameterNodes;

            Workload = new WorkloadInfo();

            concurrentNode = nodeWorkload.SelectSingleNode("Concurrent");
            Workload.TotalConcurrent = (int)(int.Parse(GetNodeValue(concurrentNode, "Total", "0")) * Tenant.Scale);
            Workload.AccountConcurrent = int.Parse(GetNodeValue(concurrentNode, "Account", "0"));
            
            scenarioNodes = nodeWorkload.SelectNodes("Scenario");
            foreach (XmlNode node in scenarioNodes)
            {
                Dictionary<string,string> parameters;
                
                parameters = new Dictionary<string,string>();
                parameterNodes = node.SelectNodes("Parameter");
                foreach (XmlNode node2 in parameterNodes)
                {
                    parameters.Add(GetAttributeValue(node2, "Name", ""), node2.InnerText);
                }

                Workload.AddScenario(GetNodeValue(node, "Name", ""),
                    (int)(int.Parse(GetNodeValue(node, "AverageRequestNumber", "1")) * Tenant.Scale),
                    (int)(int.Parse(GetNodeValue(node, "PeakRequestNumber", "0")) * Tenant.Scale),
                    parameters);
            }

            return;
        }

        public override string GetLoadSettings()
        {
            string common, packages, tenant;

            common = ProtocolHelper.BuildHeadFields("common", 
                new string[]{"unittime","averageunits", "peakunits"},
                new object[] { Common.UnitTimeInSeconds, Common.AverageUnitsForMeasure, Common.PeakUnitsForMeasure });

            packages = string.Empty;
            foreach(CommonInfo.ResourceInfo ri in Common.ComputePackages)
            {
                packages += ProtocolHelper.BuildHeadFields("package", 
                    new string[]{"name","container","blob"}, 
                    new object[]{ri.Name,ri.Container,ri.Blob});
            }
            packages += ProtocolHelper.BuildHeadFields("cert",
                    new string[] { "name", "container", "blob" },
                    new object[] { Common.HttpsCert.Name, Common.HttpsCert.Container, Common.HttpsCert.Blob });

            tenant = ProtocolHelper.BuildHeadFields("tenant", 
                new string[]{"type", "name", "endpoint", "cert", "certpassword", "cpkgendpoint", "cpkgaccount", "cpkgkey"}, 
                new object[]{Tenant.Type, Tenant.FriendlyName, Tenant.Endpoint, Tenant.HttpsCertFile, Tenant.HttpsCertPassword, 
                    Tenant.CompPkgBlobEndpoint, Tenant.CompPkgAccount, Tenant.CompPkgKey});

            return common + packages + tenant;
        }

        public override void ParseWorkerSettings(string response)
        {
            string temp;

            if(Common == null) Common = new CommonInfo();
            if(Tenant == null) Tenant = new TenantInfo();

            Common.UnitTimeInSeconds = int.Parse(ProtocolHelper.GetHeadedField(response, "common", "unittime"));
            Common.AverageUnitsForMeasure = int.Parse(ProtocolHelper.GetHeadedField(response, "common", "averageunits"));
            Common.PeakUnitsForMeasure = int.Parse(ProtocolHelper.GetHeadedField(response, "common", "peakunits"));
            
            for(int i=0; i<ProtocolHelper.GetFieldCount(response, "package"); i++)
            {
                temp = ProtocolHelper.GetTimedField(response, "package", i);

                Common.AddResource(ResourceTypes.ComputePackage.ToString(),
                    ProtocolHelper.GetOneField(temp, "name"), 
                    ProtocolHelper.GetOneField(temp, "container"), 
                    ProtocolHelper.GetOneField(temp, "blob"));
            }
            Common.AddResource(ResourceTypes.CertFile.ToString(),
                ProtocolHelper.GetHeadedField(response, "cert", "name"),
                ProtocolHelper.GetHeadedField(response, "cert", "container"),
                ProtocolHelper.GetHeadedField(response, "cert", "blob"));

            //download the cert
            Common.DownloadResources();
            
            Tenant.Type = ProtocolHelper.GetHeadedField(response, "tenant", "type");
            Tenant.FriendlyName = ProtocolHelper.GetHeadedField(response, "tenant", "name");
            Tenant.Endpoint = ProtocolHelper.GetHeadedField(response, "tenant", "endpoint");
            Tenant.HttpsCertFile = ProtocolHelper.GetHeadedField(response, "tenant", "cert");
            Tenant.HttpsCertPassword = ProtocolHelper.GetHeadedField(response, "tenant", "certpassword");
            Tenant.CompPkgBlobEndpoint = ProtocolHelper.GetHeadedField(response, "tenant", "cpkgendpoint");
            Tenant.CompPkgAccount = ProtocolHelper.GetHeadedField(response, "tenant", "cpkgaccount");
            Tenant.CompPkgKey = ProtocolHelper.GetHeadedField(response, "tenant", "cpkgkey");

            return;
        }

        public override string NextPreconditionLoad()
        {
            int newCount;
            StringBuilder sb, t;
            
            sb = new StringBuilder();
            foreach (PrePhase phase in PrePhaseList)
            {
                t = new StringBuilder();
                t.Append(ProtocolHelper.BuildOneField("index", phase.Index));
                foreach (KeyValuePair<string, int> kvp in phase.PreResources)
                {
                    newCount = LoadSplit(kvp.Value, WorkerSize, AllocatedWorkerSize);
                    t.Append(ProtocolHelper.BuildHeadFields("resource",
                        new string[] { "name", "count" },
                        new object[] { kvp.Key, newCount }));
                }
                sb.Append(ProtocolHelper.BuildOneField("phase", t.ToString()));
            }

            AllocatedWorkerSize++;
            if (AllocatedWorkerSize == WorkerSize)
            {
                AllocatedWorkerSize = 0;
            }

            return ProtocolHelper.BuildOneField("precondition", sb.ToString());
        }

        public override void ParseWorkerPreconditionLoad(string response)
        {
            string onePhase, oneResource;
            PrePhase newPhase;

            if (PrePhaseList == null) PrePhaseList = new LinkedList<PrePhase>();

            for (int i = 0; i < ProtocolHelper.GetFieldCount(response, "phase"); i++)
            {
                onePhase = ProtocolHelper.GetTimedField(response, "phase", i);

                newPhase = new PrePhase();
                newPhase.Index = int.Parse(ProtocolHelper.GetOneField(onePhase, "index"));
                for (int k = 0; k < ProtocolHelper.GetFieldCount(onePhase, "resource"); k++)
                {
                    oneResource = ProtocolHelper.GetTimedField(onePhase, "resource", k);
                    newPhase.PreResources.Add(ProtocolHelper.GetOneField(oneResource, "name"),
                        int.Parse(ProtocolHelper.GetOneField(oneResource, "count")));
                }
                PrePhaseList.AddFirst(newPhase);
            }

            return;
        }

        public override string NextUnitLoad()
        {
            StringBuilder unitLoad, scenario;
            int newCount;
            
            unitLoad = new StringBuilder();
            newCount = LoadSplit(Workload.TotalConcurrent, WorkerSize, AllocatedWorkerSize);
            unitLoad.Append(ProtocolHelper.BuildHeadFields("concurrent",
                new string[] { "total", "account" },
                new object[] { newCount, Workload.AccountConcurrent }));

            foreach (WorkloadScenario s in Workload.ScenarioList)
            {
                scenario = new StringBuilder();
                scenario.Append(ProtocolHelper.BuildOneField("name", s.Name));

                newCount = LoadSplit(s.AverageRequestNumber, WorkerSize, AllocatedWorkerSize);
                scenario.Append(ProtocolHelper.BuildOneField("average", newCount));

                newCount = LoadSplit(s.PeakRequestNumber, WorkerSize, AllocatedWorkerSize);
                scenario.Append(ProtocolHelper.BuildOneField("peak", newCount));
                
                foreach (KeyValuePair<string, string> kvp in s.ParamList)
                {
                    scenario.Append(ProtocolHelper.BuildHeadFields("parameter",
                        new string[] { "name", "value" },
                        new object[] { kvp.Key, kvp.Value }));
                }

                unitLoad.Append(ProtocolHelper.BuildOneField("scenario", scenario.ToString()));
            }

            return unitLoad.ToString();
        }

        public override void ParseWorkerUnitLoad(string response)
        {
            string t, t1;
            WorkloadScenario scenario;

            if (Workload == null) Workload = new WorkloadInfo();

            t = ProtocolHelper.GetOneField(response, "concurrent");
            Workload.TotalConcurrent = int.Parse(ProtocolHelper.GetOneField(t, "total"));
            Workload.AccountConcurrent = int.Parse(ProtocolHelper.GetOneField(t, "account"));

            for (int i = 0; i < ProtocolHelper.GetFieldCount(response, "scenario"); i++)
            {
                t = ProtocolHelper.GetTimedField(response, "scenario", i);

                scenario = new WorkloadScenario();
                scenario.Name = ProtocolHelper.GetOneField(t, "name");
                scenario.AverageRequestNumber = int.Parse(ProtocolHelper.GetOneField(t, "average"));
                scenario.PeakRequestNumber = int.Parse(ProtocolHelper.GetOneField(t, "peak"));

                scenario.ParamList = new Dictionary<string, string>();
                for (int k = 0; k < ProtocolHelper.GetFieldCount(t, "parameter"); k++)
                {
                    t1 = ProtocolHelper.GetTimedField(t, "parameter", k);

                    scenario.ParamList.Add(ProtocolHelper.GetOneField(t1, "name"),
                        ProtocolHelper.GetOneField(t1, "value"));
                }
                Workload.ScenarioList.AddFirst(scenario);
            }

            return;
        }
    }
}
