﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Configuration;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class RDFEQBar:QBarBase
    {
        RDFEQBarLoad LoadConfig;
        RDFEQBarResources RDFEActiveResources;

        public RDFEQBar(QBarLoad loadConfig, DuplexQueue masterQueue, StatusReporter reporter)
            : base(QBarServices.RDFE, masterQueue, reporter)
        {
            LoadConfig = (RDFEQBarLoad)loadConfig;
            RDFEActiveResources = new RDFEQBarResources();
        }

        public override void DoPrecondition()
        {
            int count, min, max;
           
            //sort first
            min = int.MaxValue;
            max = int.MinValue;
            foreach (PrePhase phase in LoadConfig.PrePhaseList)
            {
                if (phase.Index < min) min = phase.Index;
                if (phase.Index > max) max = phase.Index;
            }

            count = LoadConfig.PrePhaseList.Count;
            //StatusSender.Send("phase min={0}, max={1}, count={2}", min, max, count); 
            for (int i = min; i <= max; i++)
            {
                foreach (PrePhase phase in LoadConfig.PrePhaseList)
                {
                    if (phase.Index == i)
                    {
                        StatusSender.Send("Phase {0}", phase.Index);
                        foreach (KeyValuePair<string, int> kvp in phase.PreResources)
                        {
                            AddPreconditionWorkItem(kvp.Key, kvp.Value);
                        }
                        ItemsContainer.StartNow();
                        ItemsContainer.WaitAllTestsFinished();

                        break;
                    }
                }
            }

            StatusSender.Send("Finish the preconditions");

            //concurrent. TODO

            return;
        }

        private void AddPreconditionWorkItem(string name, int count)
        {
            Random rand;

            rand = new Random();
            //StatusSender.Send("Create {0} resource '{1}'", count, name);
            for (int i = 0; i < count; i++)
            {
                switch (name)
                {
                    case "Subscription":
                        ItemsContainer.AddWorkItem(
                            new CreateSubscription(LoadConfig.Tenant, WorkItemType.Precondition, RDFEActiveResources, MasterQueue), new Random().Next(300));
                        break;

                    case "StorageAccount":
                        ItemsContainer.AddWorkItem(
                            new CreateStorageService(LoadConfig.Tenant, WorkItemType.Precondition, RDFEActiveResources, MasterQueue), rand.Next(300));
                        break;

                    case "ComputeAccount":
                        ItemsContainer.AddWorkItem(
                            new CreateHostedService(LoadConfig.Tenant, WorkItemType.Precondition, RDFEActiveResources, MasterQueue), rand.Next(300));
                        break;

                    case "ComputeDeployment":
                        ItemsContainer.AddWorkItem(
                            new PutDeployment(LoadConfig.Tenant, LoadConfig.Common.RandomGetOnePackage(), 1, WorkItemType.Precondition, RDFEActiveResources, MasterQueue), rand.Next(300));
                        break;

                    default:
                        Console.WriteLine("Unknown precondition resource name");
                        break;
                }
            }

            return;
        }

        public override void StartStress()
        {
            int unitTime, numUnits;
            Random rand;

            rand = new Random();
            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            numUnits = LoadConfig.Common.AverageUnitsForMeasure;
            for (int i = 0; i < numUnits; i++)
            {
                foreach (WorkloadScenario sc in LoadConfig.Workload.ScenarioList)
                {
                    AddUnitWorkItem(WorkItemType.Average, sc.Name, sc.AverageRequestNumber, sc.ParamList);
                }
                ItemsContainer.StartNow();
                ItemsContainer.WaitAllTestsFinished();
                StatusSender.Send("Finish {0}th unit time average stress test", i);
            }

            numUnits = LoadConfig.Common.PeakUnitsForMeasure;
            for (int i = 0; i < numUnits; i++)
            {
                foreach (WorkloadScenario sc in LoadConfig.Workload.ScenarioList)
                {
                    AddUnitWorkItem(WorkItemType.Peak, sc.Name, sc.PeakRequestNumber, sc.ParamList);
                }
                ItemsContainer.StartNow();
                ItemsContainer.WaitAllTestsFinished();
                StatusSender.Send("Finish {0}th unit time peak stress test", i);
            }

            StatusSender.Send("Finish all unit loads tests");

            return;
        }


        private void AddUnitWorkItem(WorkItemType type, string name, int count, Dictionary<string,string> parameters)
        {
            int nodeSize, newNodeSize;
            string packageName, newPackageName;

            //StatusSender.Send("Create {0} resource '{1}'", count, name);
            for (int i = 0; i < count; i++)
            {
                switch (name)
                {
                    case "PutComputeDeployment":
                        packageName = string.Empty;
                        nodeSize = 0;
                        if (parameters.ContainsKey("Package")) packageName = parameters["Package"];
                        if (parameters.ContainsKey("NodeSize")) nodeSize = int.Parse(parameters["NodeSize"]);
                        UnitPutDeployments(type, packageName, nodeSize);
                        break;

                    case "DeleteComputeDeployment":
                        nodeSize = 0;
                        if (parameters.ContainsKey("NodeSize")) nodeSize = int.Parse(parameters["NodeSize"]);
                        UnitDeleteDeployments(type, nodeSize);
                        break;

                    case "UpdateDeploymentConfig":
                        nodeSize = 0;
                        newNodeSize = 0;
                        if (parameters.ContainsKey("NodeSize")) nodeSize = int.Parse(parameters["NodeSize"]);
                        if (parameters.ContainsKey("NewNodeSize")) newNodeSize = int.Parse(parameters["NewNodeSize"]);
                        UnitUpdateDeploymentConfigs(type, nodeSize, newNodeSize);
                        break;

                    case "UpgradeDeploymentConfig":
                        nodeSize = 0;
                        if (parameters.ContainsKey("NodeSize")) nodeSize = int.Parse(parameters["NodeSize"]);
                        newPackageName = string.Empty;
                        if (parameters.ContainsKey("NewPackage")) newPackageName = parameters["NewPackage"];
                        UnitUpgradeDeployments(type, nodeSize, newPackageName);
                        break;

                    default:
                        Console.WriteLine("Unknown scenario name");
                        break;
                }
            }

            return;
        }

        private void UnitPutDeployments(WorkItemType type, string packageName, int nodeSize)
        {
            int unitTime;
            CommonInfo.ResourceInfo package;
            
            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            package = string.IsNullOrEmpty(packageName) ? LoadConfig.Common.RandomGetOnePackage() : LoadConfig.Common.GetPackage(packageName);
            ItemsContainer.AddWorkItem(
                new PutDeployment(
                    LoadConfig.Tenant, 
                    package, 
                    nodeSize, 
                    type, 
                    RDFEActiveResources, 
                    MasterQueue
                    ),
                new Random().Next(unitTime)
                );
        
            return;
        }


        private void UnitDeleteDeployments(WorkItemType type, int nodeSize)
        {
            int unitTime;
            int sz;
            
            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            sz = nodeSize;
            if (sz == 0) sz = RDFEActiveResources.RandomGetDeploymentSize();

            if (sz != 0)
            {
                ItemsContainer.AddWorkItem(
                    new DeleteDeployment(
                        LoadConfig.Tenant,
                        sz,
                        type,
                        RDFEActiveResources,
                        MasterQueue
                        ),
                    new Random().Next(unitTime)
                    );
            }
        
            return;
        }

        private void UnitUpgradeDeployments(WorkItemType type, int nodeSize, string newPackageName)
        {
            int unitTime;
            int sz;
            CommonInfo.ResourceInfo newPackage;

            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            sz = nodeSize;
            newPackage = string.IsNullOrEmpty(newPackageName) ? LoadConfig.Common.RandomGetOnePackage() : LoadConfig.Common.GetPackage(newPackageName);
            
            if (sz == 0) sz = RDFEActiveResources.RandomGetDeploymentSize();
            if (sz != 0)
            {
                ItemsContainer.AddWorkItem(
                    new UpgradeDeployment(
                        LoadConfig.Tenant,
                        sz,
                        newPackage,
                        type,
                        RDFEActiveResources,
                        MasterQueue
                        ),
                     new Random().Next(unitTime)
                     );
            }

            return;
        }

        private void UnitUpdateDeploymentConfigs(WorkItemType type, int nodeSize, int newNodeSize)
        {
            int unitTime;
            int sz, sz1;
            
            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            
            sz = nodeSize;
            sz1 = newNodeSize;

            if (sz == 0) sz = RDFEActiveResources.RandomGetDeploymentSize();

            if (sz != 0)
            {
                if (sz1 == 0 || sz == sz1) sz1 = sz + 1;
                ItemsContainer.AddWorkItem(
                    new UpdateDeploymentConfig(
                        LoadConfig.Tenant,
                        sz,
                        sz1,
                        type,
                        RDFEActiveResources,
                        MasterQueue
                        ),
                    new Random().Next(unitTime)
                    );
            }
         
            return;
        }

        //release all resources in RDFEActiveResources
        public override void DoPostcondition()
        {
            int unitTime;
            Random rand;
            
            unitTime = LoadConfig.Common.UnitTimeInSeconds;

            rand = new Random();
            //1. delete all deployments;
            StatusSender.Send("Delete all deployments");
            for (int i = 0; i < RDFEActiveResources.DeploymentCount; i++)
            {
              
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteDeployment(
                                LoadConfig.Tenant, -1,
                                WorkItemType.Postcondition,
                                RDFEActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting compute account\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();


            //1. delete all compute accounts and deployments;
            StatusSender.Send("Delete {0} hosted service accounts", RDFEActiveResources.ComputeAccountCount);
            for (int i = 0; i < RDFEActiveResources.ComputeAccountCount; i++)
            {
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteHostedService(
                                LoadConfig.Tenant,
                                WorkItemType.Postcondition,
                                RDFEActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting compute account\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();

            //2. delete storage accounts
            StatusSender.Send("Delete {0} storage service accounts", RDFEActiveResources.StorageAccountCount);
            for (int i = 0; i < RDFEActiveResources.StorageAccountCount; i++)
            {
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteStorageService(
                                LoadConfig.Tenant,
                                WorkItemType.Postcondition,
                                RDFEActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting storage account\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();

            //3. delete all test subscriptions
            StatusSender.Send("Delete {0} subscriptions", RDFEActiveResources.SubscriptionCount);
            for(int i=0; i<RDFEActiveResources.SubscriptionCount; i++)
            {
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteSubscription(
                                LoadConfig.Tenant,
                                WorkItemType.Postcondition,
                                RDFEActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting subscriptions\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();

            return;
        }
    }
}
