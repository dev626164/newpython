﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class xStoreQBar:QBarBase
    {
        xStoreQBarLoad LoadConfig;
        xStoreQBarResources xStoreActiveResources;

        public xStoreQBar(QBarLoad loadConfig, DuplexQueue masterQueue, StatusReporter reporter)
            : base(QBarServices.xStore, masterQueue, reporter)
        {
            LoadConfig = (xStoreQBarLoad)loadConfig;
            xStoreActiveResources = new xStoreQBarResources();
        }

        public override void DoPrecondition()
        {
            int count, min, max;

            //sort first
            min = int.MaxValue;
            max = int.MinValue;
            foreach (PrePhase phase in LoadConfig.PrePhaseList)
            {
                if (phase.Index < min) min = phase.Index;
                if (phase.Index > max) max = phase.Index;
            }

            count = LoadConfig.PrePhaseList.Count;
            //StatusSender.Send("phase min={0}, max={1}, count={2}", min, max, count);
            for (int i = min; i <= max; i++)
            {
                foreach (PrePhase phase in LoadConfig.PrePhaseList)
                {
                    if (phase.Index == i)
                    {
                        StatusSender.Send("Phase {0}", phase.Index);
                        foreach (KeyValuePair<string, int> kvp in phase.PreResources)
                        {
                            AddPreconditionWorkItem(kvp.Key, kvp.Value);
                        }
                        ItemsContainer.StartNow();
                        ItemsContainer.WaitAllTestsFinished();

                        break;
                    }
                }
            }

            StatusSender.Send("Finish the preconditions");

            //concurrent. TODO

            return;
        }

        private void AddPreconditionWorkItem(string name, int count)
        {
            Random rand;

            rand = new Random();
            //StatusSender.Send("Create {0} resource '{1}'", count, name);
            for (int i = 0; i < count; i++)
            {
                switch (name)
                {
                    case "Account":
                        ItemsContainer.AddWorkItem(
                            new CreateStorageAccount(LoadConfig.Tenant, WorkItemType.Precondition, xStoreActiveResources, MasterQueue), rand.Next(300));
                        break;

                    case "Container":
                        ItemsContainer.AddWorkItem(
                            new CreateContainer(LoadConfig.Tenant, WorkItemType.Precondition, xStoreActiveResources, MasterQueue), rand.Next(300));
                        break;

                    case "Blob":
                        ItemsContainer.AddWorkItem(
                            new PutBlob(LoadConfig.Tenant, WorkItemType.Precondition, xStoreActiveResources, MasterQueue), rand.Next(300));
                        break;

                    default:
                        Console.WriteLine("Unknown precondition resource name");
                        break;
                }
            }

            return;
        }

        public override void StartStress()
        {
            int unitTime, numUnits;
            Random rand;

            rand = new Random();
            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            numUnits = LoadConfig.Common.AverageUnitsForMeasure;
            for (int i = 0; i < numUnits; i++)
            {
                foreach (WorkloadScenario sc in LoadConfig.Workload.ScenarioList)
                {
                    AddUnitWorkItem(WorkItemType.Average, sc.Name, sc.AverageRequestNumber, sc.ParamList);
                }
                ItemsContainer.StartNow();
                ItemsContainer.WaitAllTestsFinished();
                StatusSender.Send("Finish {0}th unit time average stress test", i);
            }

            numUnits = LoadConfig.Common.PeakUnitsForMeasure;
            for (int i = 0; i < numUnits; i++)
            {
                foreach (WorkloadScenario sc in LoadConfig.Workload.ScenarioList)
                {
                    AddUnitWorkItem(WorkItemType.Peak, sc.Name, sc.PeakRequestNumber, sc.ParamList);
                }
                ItemsContainer.StartNow();
                ItemsContainer.WaitAllTestsFinished();
                StatusSender.Send("Finish {0}th unit time peak stress test", i);
            }

            StatusSender.Send("Finish all unit loads tests");

            return;
        }


        private void AddUnitWorkItem(WorkItemType type, string name, int count, Dictionary<string, string> parameters)
        {
            int unitTime;
            Random rand;

            unitTime = LoadConfig.Common.UnitTimeInSeconds;

            rand = new Random();
            //StatusSender.Send("Create {0} resource '{1}'", count, name);
            for (int i = 0; i < count; i++)
            {
                switch (name)
                {
                    case "PutBlob":
                        ItemsContainer.AddWorkItem(
                            new PutBlob(LoadConfig.Tenant, type, xStoreActiveResources, MasterQueue), rand.Next(unitTime));
                        break;

                    case "GetBlob":
                        ItemsContainer.AddWorkItem(
                            new GetBlob(LoadConfig.Tenant, type, xStoreActiveResources, MasterQueue), rand.Next(unitTime));
                        break;

                    case "DeleteBlob":
                        ItemsContainer.AddWorkItem(
                            new DeleteBlob(LoadConfig.Tenant, type, xStoreActiveResources, MasterQueue), rand.Next(unitTime));
                        break;

                    case "CreateContainer":
                        ItemsContainer.AddWorkItem(
                            new CreateContainer(LoadConfig.Tenant, type, xStoreActiveResources, MasterQueue), rand.Next(unitTime));
                        break;

                    case "DeleteContainer":
                        ItemsContainer.AddWorkItem(
                            new DeleteContainer(LoadConfig.Tenant, type, xStoreActiveResources, MasterQueue), rand.Next(unitTime));
                        break;

                    default:
                        Console.WriteLine("Unknown scenario name");
                        break;
                }
            }

            return;
        }

        public override void DoPostcondition()
        {
            int unitTime;
            Random rand;

            if (LoadConfig.Common.PostCondition == false) return;

            unitTime = LoadConfig.Common.UnitTimeInSeconds;
            rand = new Random();
            //1. delete all blobs;
            StatusSender.Send("Delete {0} blobs", xStoreActiveResources.BlobCount);
            for (int i = 0; i < xStoreActiveResources.BlobCount; i++)
            {
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteBlob(
                                LoadConfig.Tenant,
                                WorkItemType.Postcondition,
                                xStoreActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting blobs\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();

            //2. delete containers
            StatusSender.Send("Delete {0} containers", xStoreActiveResources.ContainerCount);
            for (int i = 0; i < xStoreActiveResources.ContainerCount; i++)
            {
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteContainer(
                                LoadConfig.Tenant,
                                WorkItemType.Postcondition,
                                xStoreActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting container\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();

            //3. delete all test accounts
            StatusSender.Send("Delete {0} storage accounts", xStoreActiveResources.AccountCount);
            for (int i = 0; i < xStoreActiveResources.AccountCount; i++)
            {
                try
                {
                    ItemsContainer.AddWorkItem(
                            new DeleteStorageAccount(
                                LoadConfig.Tenant,
                                WorkItemType.Postcondition,
                                xStoreActiveResources,
                                MasterQueue
                                ),
                             rand.Next(0, unitTime)
                             );
                }
                catch (Exception ex)
                {
                    StatusSender.Send("Exception in deleting storage account\r\n{0}", ex.ToString());
                }
            }
            ItemsContainer.StartNow();
            ItemsContainer.WaitAllTestsFinished();

            return;
        }
    }
}
