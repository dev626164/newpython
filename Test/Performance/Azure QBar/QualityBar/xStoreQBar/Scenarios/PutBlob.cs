﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;
using System.IO;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class PutBlob : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public PutBlob(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.PutBlob.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            string temp;
            QBarActionStart();
            string blob = "qbarb"+xStoreCommons.RandomString(20, true);
            xStoreQBarResources.Container container = ActiveResources.RandomGetContainer(ResourceStatus.BlobPutting);
            string account = container.Account;
            string accountKey = ActiveResources.GetAccountKey(account);

            BlobContents content = xStoreCommons.RandomGetOneBlobContent();
            long blobSize = content.AsStream.Length;
            this.ScenarioSubKey = string.Format("{0}MB", blobSize / (1024 * 1024.0));
            QBarActionEnd();

            ResourceUriComponents uriComponents = new ResourceUriComponents(account, container.Name, blob);
            Uri uri = HttpRequestAccessor.ConstructResourceUri(new Uri(TestTenant.BlobEndpoint), uriComponents, false);
            int timeout = 30000;
            PutBlobProperties properties = new PutBlobProperties() { BlobType = BlobTypes.StreamBlob };

            try
            {
                PreAPICall();
                HttpWebRequest request = BlobRequest.Put(uri, timeout, properties, string.Empty);
                BlobRequest.SignRequest(request, new Credentials(account, accountKey));

                xStoreCommons.AddBody(request, content.AsBytes());

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                }
                PostAPICall("BlobRequest.Create");
            }
            catch (WebException ex)
            {
                ActiveResources.UpdateContainerStatus(container.Name, ResourceStatus.Idle);
                using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                {
                    if (response != null && (response.StatusCode == HttpStatusCode.NotFound ||
                        response.StatusCode == HttpStatusCode.Gone ||
                        response.StatusCode == HttpStatusCode.Conflict))
                    {
                        temp = response.StatusDescription;
                        throw new Exception(temp);
                    }
                    throw RestApiException.GenerateException(response, ex);
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateContainerStatus(container.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.AddBlob(blob, blobSize, account, container.Name, DateTime.Now);
            ActiveResources.UpdateContainerStatus(container.Name, ResourceStatus.Idle);
            QBarActionEnd();

            return;
        }
    }
}
