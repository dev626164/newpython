﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class CreateContainer : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public CreateContainer(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.CreateContainer.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            string temp;
            string containerName = "qbarc" + xStoreCommons.RandomString(20, true);

            QBarActionStart();
            xStoreQBarResources.StorageAccount sa = ActiveResources.RandomGetAccount(ResourceStatus.ContainerCreating);
            QBarActionEnd();

            string account = sa.Name;
            string accountKey = sa.Key;

            int timeout = 30000;
            ResourceUriComponents uriComponents = new ResourceUriComponents(account, containerName);
            Uri uri = HttpRequestAccessor.ConstructResourceUri(new Uri(TestTenant.BlobEndpoint), uriComponents, false);

            try
            {
                PreAPICall();
                HttpWebRequest request = ContainerRequest.Create(uri, timeout);
                ContainerRequest.SignRequest(request, new Credentials(account, accountKey));

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                }
                PostAPICall("ContainerRequest.Create");
            }
            catch (WebException ex)
            {
                ActiveResources.UpdateAccountStatus(account, ResourceStatus.Idle);
                using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                {
                    if (response != null && response.StatusCode == HttpStatusCode.Conflict)
                    {
                        temp = response.StatusDescription;
                        throw new Exception(temp);
                    }
                    throw RestApiException.GenerateException(response, ex);
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateAccountStatus(account, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.AddContainer(containerName, account, DateTime.Now);
            ActiveResources.UpdateAccountStatus(account, ResourceStatus.Idle);
            QBarActionEnd();

            return;
        }
    }
}
