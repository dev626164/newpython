﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class DeleteBlob : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public DeleteBlob(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.DeleteBlob.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            QBarActionStart();
            xStoreQBarResources.Blob blob = ActiveResources.RandomGetBlob(ResourceStatus.BlobGetting);
            string account = blob.Account;
            string accountKey = ActiveResources.GetAccountKey(account);
            string container = blob.Container;
            this.ScenarioSubKey = string.Format("{0}MB", blob.Size / (1024 * 1024.0));
            QBarActionEnd();

            string temp;

            ResourceUriComponents uriComponents = new ResourceUriComponents(account, container, blob.Name);
            Uri uri = HttpRequestAccessor.ConstructResourceUri(new Uri(TestTenant.BlobEndpoint), uriComponents, false);
            int timeout = 30000;

           try
            {
                PreAPICall();

                HttpWebRequest request = BlobRequest.Delete(uri, timeout, null, null, string.Empty);
                BlobRequest.SignRequest(request, new Credentials(account, accountKey));

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {   
                }
                PostAPICall("BlobRequest.Delete");
            }
            catch (WebException ex)
            {
                ActiveResources.UpdateBlobStatus(blob.Name, ResourceStatus.Idle);
                using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                {
                    if (response != null & (response.StatusCode == HttpStatusCode.NotFound ||
                      response.StatusCode == HttpStatusCode.Gone ||
                      response.StatusCode == HttpStatusCode.Conflict))
                    {
                        temp = response.StatusDescription;
                        throw new Exception(temp);
                    }
                    throw RestApiException.GenerateException(response, ex);
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateBlobStatus(blob.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.DeleteBlob(blob.Name);
            QBarActionEnd();

            return;
        }
    }
}
