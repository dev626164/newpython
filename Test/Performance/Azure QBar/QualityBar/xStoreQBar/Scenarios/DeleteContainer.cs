﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class DeleteContainer : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public DeleteContainer(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.DeleteContainer.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
/*
            string account, accountKey, containerName;
            string userInfo;
            BlobStorage blobStorage;
            BlobContainer blobContainer;
            xStoreQBarResources.Container container;
            bool flag;
*/

            string temp;

            QBarActionStart();
            xStoreQBarResources.Container container = ActiveResources.RandomGetContainer(ResourceStatus.Deleting);
            string containerName = container.Name;
            string account = container.Account;
            string accountKey = ActiveResources.GetAccountKey(account);
            QBarActionEnd();

            ResourceUriComponents uriComponents = new ResourceUriComponents(account, containerName);
            Uri uri = HttpRequestAccessor.ConstructResourceUri(new Uri(TestTenant.BlobEndpoint), uriComponents, false);
            int timeout = 30000;

            try
            {
                PreAPICall();
                HttpWebRequest request = ContainerRequest.Delete(uri, timeout);
                ContainerRequest.SignRequest(request, new Credentials(account, accountKey));

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                }
                PostAPICall("ContainerRequest.Delete");
            }
            catch (WebException ex)
            {
                ActiveResources.UpdateContainerStatus(containerName, ResourceStatus.Idle);
                using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                {
                    if (response != null && (response.StatusCode == HttpStatusCode.NotFound ||
                        response.StatusCode == HttpStatusCode.Gone ||
                        response.StatusCode == HttpStatusCode.Conflict))
                    {
                        temp = response.StatusDescription;
                        throw new Exception(temp);
                    }
                    throw RestApiException.GenerateException(response, ex);
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateContainerStatus(containerName, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.DeleteContainer(containerName);
            QBarActionEnd();

            return;
        }
    }
}
