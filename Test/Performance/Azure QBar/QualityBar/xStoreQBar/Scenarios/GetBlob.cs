﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class GetBlob : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public GetBlob(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.GetBlob.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            string userInfo = string.Empty;
            QBarActionStart();
            xStoreQBarResources.Blob blob = ActiveResources.RandomGetBlob(ResourceStatus.BlobGetting);
            string account = blob.Account;
            string accountKey = ActiveResources.GetAccountKey(account);
            string container = blob.Container;
            this.ScenarioSubKey = string.Format("{0}MB", blob.Size / (1024 * 1024.0));
            QBarActionEnd();


            ResourceUriComponents uriComponents = new ResourceUriComponents(account, container, blob.Name);
            Uri uri = HttpRequestAccessor.ConstructResourceUri(new Uri(TestTenant.BlobEndpoint), uriComponents, false);
            int timeout = 30000;

            try
            {
                PreAPICall();
                HttpWebRequest request = BlobRequest.Get(uri, timeout, null, string.Empty);
                BlobRequest.SignRequest(request, new Credentials(account, accountKey));

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.ContentLength == 0)
                    {
                        userInfo = "Blob length is 0";
                        throw new Exception(userInfo);
                    }

                    using (Stream stream = response.GetResponseStream())
                    {
                    }
                }
                PostAPICall("BlobRequest.Get");
            }
            catch (WebException ex)
            {
                ActiveResources.UpdateBlobStatus(blob.Name, ResourceStatus.Idle);
                using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                {
                    if (response != null && (response.StatusCode == HttpStatusCode.NotFound ||
                      response.StatusCode == HttpStatusCode.Gone ||
                      response.StatusCode == HttpStatusCode.Conflict))
                    {
                        userInfo = response.StatusDescription;
                        throw new Exception(userInfo);
                    }
                    throw RestApiException.GenerateException(response, ex);
                }
            }
            catch (Exception ex)
            {
                ActiveResources.UpdateBlobStatus(blob.Name, ResourceStatus.Idle);
                throw ex;
            }

            QBarActionStart();
            ActiveResources.UpdateBlobStatus(blob.Name, ResourceStatus.Idle);
            QBarActionEnd();

            return;
        }
    }
}
