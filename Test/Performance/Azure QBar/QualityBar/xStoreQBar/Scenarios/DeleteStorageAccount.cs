﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class DeleteStorageAccount : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public DeleteStorageAccount(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.DeleteStorageAccount.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            HttpWebRequest request;
            int timeout;
            string accountName;

            timeout = 30000;
            QBarActionStart();
            accountName = ActiveResources.RandomGetAccount(ResourceStatus.Deleting).Name;
            QBarActionEnd();
            try
            {
                request = xStoreCommons.CreateAccountServiceRequest(
                    xStoreCommons.BuildAccountUri(TestTenant.AccountEndpoint, timeout/1000, null, accountName), 
                    HttpVerbs.Delete, TestTenant.AdminAccount, TestTenant.AdminAccountKey, TimeSpan.FromMilliseconds(timeout));

                PreAPICall();
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) 
                { 
                }
                PostAPICall("DeleteStorageAccount");
            }
            catch (WebException e)
            {
                ActiveResources.UpdateAccountStatus(accountName, ResourceStatus.Idle);
                using (HttpWebResponse response = (HttpWebResponse)e.Response)
                {
                    throw RestApiException.GenerateException(response, e);
                }
            }
            catch (Exception e)
            {
                ActiveResources.UpdateAccountStatus(accountName, ResourceStatus.Idle);
                throw RestApiException.GenerateException(null, e);
            }

            QBarActionStart();
            ActiveResources.DeleteAccount(accountName);
            QBarActionEnd();

            return;
        }
    }
}
