﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class CreateStorageAccount : QBarWorkItem
    {
        public xStoreScenarios ItemName
        {
            get { return (xStoreScenarios)Enum.Parse(typeof(xStoreScenarios), ScenarioName); }
        }
        xStoreQBarResources ActiveResources;
        private XStoreTenantInfo TestTenant;

        public CreateStorageAccount(XStoreTenantInfo tenant, WorkItemType type, xStoreQBarResources resources, DuplexQueue master)
            : base(xStoreScenarios.CreateStorageAccount.ToString(), type, master)
        {
            TestTenant = tenant;
            ActiveResources = resources;
        }

        public override void ExecuteScenario()
        {
            string accountName, key, geoLocation, geoDomain;
            int timeout;
            HttpWebRequest request;
            string createAccountXmlRequest;

            timeout = 30000;

            accountName = xStoreCommons.RandomStorageAccount("qbara");
            geoLocation = TestTenant.GetRandomLocation();
            geoDomain = TestTenant.GetDomain(geoLocation);
            QBarLogger.Log(DebugLevel.INFO, "Create account {0} at {1}/{2}", accountName, geoLocation, geoDomain);
           
            try
            {
                request = xStoreCommons.CreateAccountServiceRequest(
                    xStoreCommons.BuildAccountUri(TestTenant.AccountEndpoint, timeout/1000, null), 
                    HttpVerbs.Post, TestTenant.AdminAccount, TestTenant.AdminAccountKey, 
                    TimeSpan.FromMilliseconds(timeout));

                // Generate the request body xml
                key = xStoreCommons.GetRandomStorageKey();
                createAccountXmlRequest = xStoreCommons.GetCreateAccountRequestBody(
                    accountName, "FULL", geoLocation, geoDomain, key, xStoreCommons.GetRandomStorageKey());
                xStoreCommons.AddBody(request, Encoding.UTF8.GetBytes(createAccountXmlRequest));

                PreAPICall();
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                }
                PostAPICall("CreateStorageAccount");
            }
            catch (WebException ex)
            {
                using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                {
                    throw RestApiException.GenerateException(response, ex);
                }
            }
            catch (Exception ex)
            {
                throw RestApiException.GenerateException(null, ex);
            }

            QBarActionStart();
            ActiveResources.AddAccount(accountName, key, geoLocation, DateTime.Now);
            QBarActionEnd();

            return;
        }
    }
}
