﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Linq;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class XStoreCommonInfo
    {
        public int UnitTimeInSeconds;
        public int AverageUnitsForMeasure;
        public int PeakUnitsForMeasure;
        public bool PostCondition;
    }

    public class XStoreTenantInfo
    {
        public string Type;
        public string FriendlyName;
        public string AccountEndpoint;
        public string BlobEndpoint;
        public string QueueEndpoint;
        public string TableEndpoint;
        public string AdminAccount;
        public string AdminAccountKey;
        public int NumInstances;
        public double Scale;
        public string Locations;
        public string Domains;
        public List<string> GeoLocationList;
        public List<string> GeoDomainList;

        public XStoreTenantInfo()
        {
            GeoLocationList = new List<string>();
            GeoDomainList = new List<string>();
        }

        public string GetRandomLocation()
        {
            string location;
            int index;

            location = string.Empty;
            if(GeoLocationList.Count > 0)
            {
                index = new Random().Next(GeoLocationList.Count);
                location = GeoLocationList[index];
            }
            
            return location;
        }

        public string GetDomain(string location)
        {
            string domain;
            int index;

            domain = string.Empty;
            if (GeoDomainList.Count > 0)
            {
                index = GeoLocationList.IndexOf(location);
                domain = GeoDomainList[index];
            }

            return domain;
        }
    }

    public class PrePhase
    {
        //<name, count> pair
        public int Index;
        public Dictionary<string, int> PreResources;

        public PrePhase()
        {
            Index = -1;
            PreResources = new Dictionary<string, int>();
        }
    }

    public class WorkloadScenario
    {
        public string Name;
        public int AverageRequestNumber;
        public int PeakRequestNumber;
        public Dictionary<string, string> ParamList;

        public WorkloadScenario()
        {
            Name = string.Empty;
            AverageRequestNumber = 0;
            PeakRequestNumber = 0;
            ParamList = new Dictionary<string, string>();
        }
    }

    public class WorkloadInfo
    {
        public int TotalConcurrent;
        public int AccountConcurrent;
        public LinkedList<WorkloadScenario> ScenarioList;

        public WorkloadInfo()
        {
            TotalConcurrent = 0;
            AccountConcurrent = 0;
            ScenarioList = new LinkedList<WorkloadScenario>();
        }

        public void AddScenario(string name, int average, int peak, Dictionary<string, string> parameters)
        {
            ScenarioList.AddLast(new WorkloadScenario()
            {
                Name = name,
                AverageRequestNumber = average,
                PeakRequestNumber = peak,
                ParamList = parameters
            });

            return;
        }
    }

    public class xStoreQBarLoad:QBarLoad
    {
        public XStoreCommonInfo Common;
        public XStoreTenantInfo Tenant;
        public LinkedList<PrePhase> PrePhaseList;
        public WorkloadInfo Workload;

        public xStoreQBarLoad():base()
        {
            Common = null;
            Tenant = null;
            PrePhaseList = null;
            Workload = null;
        }

        public xStoreQBarLoad(string loadfile, DuplexQueue manager)
            : base(loadfile, manager)
        {
        }

        protected override void ParseCommon(XmlNode nodeCommon)
        {
            Common = new XStoreCommonInfo()
            {
                UnitTimeInSeconds = int.Parse(GetNodeValue(nodeCommon, "UnitTimeInSeconds", "600")),
                AverageUnitsForMeasure = int.Parse(GetNodeValue(nodeCommon, "AverageUnitsForMeasure", "1")),
                PeakUnitsForMeasure = int.Parse(GetNodeValue(nodeCommon, "PeakUnitsForMeasure", "0")),
                PostCondition = bool.Parse(GetNodeValue(nodeCommon, "PostCondition", true))
            };

            return;
        }
        protected override void ParseTenant(XmlNode nodeTenant)
        {
            Tenant = new XStoreTenantInfo()
            {
                Type = GetAttributeValue(nodeTenant, "Type", ""),
                FriendlyName = GetAttributeValue(nodeTenant, "FriendlyName", ""),
                AccountEndpoint = @"https://" + GetNodeValue(nodeTenant, "AccountEndpoint", "") + "/",
                BlobEndpoint = @"http://" + GetNodeValue(nodeTenant, "BlobEndpoint", "") + "/",
                QueueEndpoint = @"http://" + GetNodeValue(nodeTenant, "QueueEndpoint", "") + "/",
                TableEndpoint = @"http://" + GetNodeValue(nodeTenant, "TableEndpoint", "") + "/",
                AdminAccount = GetNodeValue(nodeTenant, "AdminAccount", ""),
                AdminAccountKey = GetNodeValue(nodeTenant, "AdminAccountKey", ""),
                NumInstances = int.Parse(GetNodeValue(nodeTenant, "InstanceCount", "1")),
                Scale = double.Parse(GetNodeValue(nodeTenant, "Scale", "1.0")),
            };
            Tenant.Domains = GetNodeValue(nodeTenant, "Domains", "");
            foreach (string d in Tenant.Domains.Split('|'))
            {
                Tenant.GeoDomainList.Add(d);
            }
            Tenant.Locations = GetNodeValue(nodeTenant, "Locations", "");
            foreach (string l in Tenant.Locations.Split('|'))
            {
                Tenant.GeoLocationList.Add(l);
            }

            return;
        }
        protected override void ParsePrecondition(XmlNode nodePrecondition)
        {
            XmlNodeList nodeList, resourceList;
            int count;

            PrePhaseList = new LinkedList<PrePhase>();

            nodeList = nodePrecondition.SelectNodes("Phase");
            foreach (XmlNode node in nodeList)
            {
                PrePhase phase;

                phase = new PrePhase();
                phase.Index = int.Parse(GetAttributeValue(node, "Index", "0"));

                resourceList = node.SelectNodes("Resource");
                foreach (XmlNode node2 in resourceList)
                {
                    count = (int)(int.Parse(GetNodeValue(node2, "Count", "0")) * Tenant.Scale);
                    phase.PreResources.Add(GetAttributeValue(node2, "Name", ""), count);
                }
                PrePhaseList.AddFirst(phase);
            }

            //Console.WriteLine("There are {0} phases", PrePhaseList.Count);

            return;
        }
        protected override void ParseWorkload(XmlNode nodeWorkload)
        {
            XmlNode concurrentNode;
            XmlNodeList scenarioNodes, parameterNodes;

            Workload = new WorkloadInfo();

            concurrentNode = nodeWorkload.SelectSingleNode("Concurrent");
            Workload.TotalConcurrent = (int)(int.Parse(GetNodeValue(concurrentNode, "Total", "0")) * Tenant.Scale);
            Workload.AccountConcurrent = int.Parse(GetNodeValue(concurrentNode, "Account", "0"));

            scenarioNodes = nodeWorkload.SelectNodes("Scenario");
            foreach (XmlNode node in scenarioNodes)
            {
                Dictionary<string, string> parameters;

                parameters = new Dictionary<string, string>();
                parameterNodes = node.SelectNodes("Parameter");
                if (parameterNodes != null && parameterNodes.Count > 0)
                {
                    foreach (XmlNode node2 in parameterNodes)
                    {
                        parameters.Add(GetAttributeValue(node2, "Name", ""), node2.InnerText);
                    }
                }

                Workload.AddScenario(GetNodeValue(node, "Name", ""),
                    (int)(int.Parse(GetNodeValue(node, "AverageRequestNumber", "1")) * Tenant.Scale),
                    (int)(int.Parse(GetNodeValue(node, "PeakRequestNumber", "0")) * Tenant.Scale),
                    parameters);
            }

            return;
        }

        public override string GetLoadSettings()
        {
            string common, tenant;

            common = ProtocolHelper.BuildHeadFields("common",
                new string[] { "unittime", "averageunits", "peakunits", "postcondition" },
                new object[] { Common.UnitTimeInSeconds, Common.AverageUnitsForMeasure, Common.PeakUnitsForMeasure, Common.PostCondition });

            tenant = ProtocolHelper.BuildHeadFields("tenant",
                new string[] { "type", "name", "accountendpoint", "blobendpoint", "queueendpoint", "tableendpoint", 
                    "adminaccount", "adminaccountkey", "domains", "locations"},
                new object[]{Tenant.Type, Tenant.FriendlyName, Tenant.AccountEndpoint, Tenant.BlobEndpoint, 
                    Tenant.QueueEndpoint, Tenant.TableEndpoint, Tenant.AdminAccount, Tenant.AdminAccountKey,
                    Tenant.Domains, Tenant.Locations});

            return common + tenant;
        }

        public override void ParseWorkerSettings(string response)
        {
            if (Common == null) Common = new XStoreCommonInfo();
            if (Tenant == null) Tenant = new XStoreTenantInfo();

            Common.UnitTimeInSeconds = int.Parse(ProtocolHelper.GetHeadedField(response, "common", "unittime"));
            Common.AverageUnitsForMeasure = int.Parse(ProtocolHelper.GetHeadedField(response, "common", "averageunits"));
            Common.PeakUnitsForMeasure = int.Parse(ProtocolHelper.GetHeadedField(response, "common", "peakunits"));
            Common.PostCondition = bool.Parse(ProtocolHelper.GetHeadedField(response, "common", "postcondition"));

            Tenant.Type = ProtocolHelper.GetHeadedField(response, "tenant", "type");
            Tenant.FriendlyName = ProtocolHelper.GetHeadedField(response, "tenant", "name");
            Tenant.AccountEndpoint = ProtocolHelper.GetHeadedField(response, "tenant", "accountendpoint");
            Tenant.BlobEndpoint = ProtocolHelper.GetHeadedField(response, "tenant", "blobendpoint");
            Tenant.QueueEndpoint = ProtocolHelper.GetHeadedField(response, "tenant", "queueendpoint");
            Tenant.TableEndpoint = ProtocolHelper.GetHeadedField(response, "tenant", "tableendpoint");
            Tenant.AdminAccount = ProtocolHelper.GetHeadedField(response, "tenant", "adminaccount");
            Tenant.AdminAccountKey = ProtocolHelper.GetHeadedField(response, "tenant", "adminaccountkey");

            Tenant.Domains = ProtocolHelper.GetHeadedField(response, "tenant", "domains");
            foreach (string d in Tenant.Domains.Split('|'))
            {
                Tenant.GeoDomainList.Add(d);
            }
            Tenant.Locations = ProtocolHelper.GetHeadedField(response, "tenant", "locations");
            foreach (string l in Tenant.Locations.Split('|'))
            {
                Tenant.GeoLocationList.Add(l);
            }
            
            return;
        }

        public override string NextPreconditionLoad()
        {
            int newCount;
            StringBuilder sb, t;
            
            sb = new StringBuilder();
            foreach (PrePhase phase in PrePhaseList)
            {
                t = new StringBuilder();
                t.Append(ProtocolHelper.BuildOneField("index", phase.Index));
                foreach (KeyValuePair<string, int> kvp in phase.PreResources)
                {
                    newCount = LoadSplit(kvp.Value, WorkerSize, AllocatedWorkerSize);
                    t.Append(ProtocolHelper.BuildHeadFields("resource",
                        new string[] { "name", "count" },
                        new object[] { kvp.Key, newCount }));
                }
                sb.Append(ProtocolHelper.BuildOneField("phase", t.ToString()));
            }

            AllocatedWorkerSize++;
            if (AllocatedWorkerSize == WorkerSize)
            {
                AllocatedWorkerSize = 0;
            }

            return ProtocolHelper.BuildOneField("precondition", sb.ToString());
        }

        public override void ParseWorkerPreconditionLoad(string response)
        {
            string onePhase, oneResource;
            PrePhase newPhase;

            if (PrePhaseList == null) PrePhaseList = new LinkedList<PrePhase>();

            for (int i = 0; i < ProtocolHelper.GetFieldCount(response, "phase"); i++)
            {
                onePhase = ProtocolHelper.GetTimedField(response, "phase", i);

                newPhase = new PrePhase();
                newPhase.Index = int.Parse(ProtocolHelper.GetOneField(onePhase, "index"));
                for (int k = 0; k < ProtocolHelper.GetFieldCount(onePhase, "resource"); k++)
                {
                    oneResource = ProtocolHelper.GetTimedField(onePhase, "resource", k);
                    newPhase.PreResources.Add(ProtocolHelper.GetOneField(oneResource, "name"),
                        int.Parse(ProtocolHelper.GetOneField(oneResource, "count")));
                }
                PrePhaseList.AddFirst(newPhase);
            }

            return;
        }

        public override string NextUnitLoad()
        {
            StringBuilder unitLoad, scenario;
            int newCount;
            
            unitLoad = new StringBuilder();
            newCount = LoadSplit(Workload.TotalConcurrent, WorkerSize, AllocatedWorkerSize);
            unitLoad.Append(ProtocolHelper.BuildHeadFields("concurrent",
                new string[] { "total", "account" },
                new object[] { newCount, Workload.AccountConcurrent }));

            foreach (WorkloadScenario s in Workload.ScenarioList)
            {
                scenario = new StringBuilder();
                scenario.Append(ProtocolHelper.BuildOneField("name", s.Name));
                
                newCount = LoadSplit(s.AverageRequestNumber, WorkerSize, AllocatedWorkerSize);
                scenario.Append(ProtocolHelper.BuildOneField("average", newCount));

                newCount = LoadSplit(s.PeakRequestNumber, WorkerSize, AllocatedWorkerSize);
                scenario.Append(ProtocolHelper.BuildOneField("peak", newCount));
                
                foreach (KeyValuePair<string, string> kvp in s.ParamList)
                {
                    scenario.Append(ProtocolHelper.BuildHeadFields("parameter",
                        new string[] { "name", "value" },
                        new object[] { kvp.Key, kvp.Value }));
                }

                unitLoad.Append(ProtocolHelper.BuildOneField("scenario", scenario.ToString()));
            }

            return unitLoad.ToString();
        }

        public override void ParseWorkerUnitLoad(string response)
        {
            string t, t1;
            WorkloadScenario scenario;

            if (Workload == null) Workload = new WorkloadInfo();

            t = ProtocolHelper.GetOneField(response, "concurrent");
            Workload.TotalConcurrent = int.Parse(ProtocolHelper.GetOneField(t, "total"));
            Workload.AccountConcurrent = int.Parse(ProtocolHelper.GetOneField(t, "account"));

            for (int i = 0; i < ProtocolHelper.GetFieldCount(response, "scenario"); i++)
            {
                t = ProtocolHelper.GetTimedField(response, "scenario", i);

                scenario = new WorkloadScenario();
                scenario.Name = ProtocolHelper.GetOneField(t, "name");
                scenario.AverageRequestNumber = int.Parse(ProtocolHelper.GetOneField(t, "average"));
                scenario.PeakRequestNumber = int.Parse(ProtocolHelper.GetOneField(t, "peak"));

                scenario.ParamList = new Dictionary<string, string>();
                for (int k = 0; k < ProtocolHelper.GetFieldCount(t, "parameter"); k++)
                {
                    t1 = ProtocolHelper.GetTimedField(t, "parameter", k);

                    scenario.ParamList.Add(ProtocolHelper.GetOneField(t1, "name"),
                        ProtocolHelper.GetOneField(t1, "value"));
                }
                Workload.ScenarioList.AddFirst(scenario);
            }

            return;
        }
    }
}
