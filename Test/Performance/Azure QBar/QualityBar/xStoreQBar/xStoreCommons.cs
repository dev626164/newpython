﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using System.Net;
using System.Globalization;
using System.Web;
using System.Security.Cryptography;
using Microsoft.Samples.ServiceHosting.StorageClient;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class HeaderNames
    {
        public const string NephosPrefix = "x-ms-";
        public const string NephosMetaPrefix = "x-ms-meta-";

        public const string ContentMD5 = "Content-MD5";
        public const string Authorization = "Authorization";
        public const string NephosDate = NephosPrefix + "date";
        public const string Version = "x-ms-version";
    }
    public class HttpVerbs
    {
        public const string Get = "GET";
        public const string Put = "PUT";
        public const string Post = "POST";
        public const string Delete = "DELETE";
        public const string Head = "HEAD";
    }

    public class QueryParamNames
    {
        public const string Timeout = "timeout";
        public const string Component = "comp";
        public const string PreFix = "prefix";
        public const string Marker = "marker";
        public const string MaxResults = "maxresults";
        public const string PeekOnly = "peekonly";
        public const string PopReceipt = "popreceipt";
        public const string NumberOfMessages = "numofmessages";
        public const string MessageTTL = "messagettl"; // Message Time To Live in seconds.
    }


    class xStoreCommons
    {
        const string SignatureDelimitter = "\n";

        public static BlobContents RandomGetOneBlobContent()
        {
            int scope, size;

            scope = new Random().Next(100);
            if(scope < 60)
            {
                size = 512*1024; //512KB
            }
            else if (scope < 90)
            {
                size = 3 * 1024 * 1024; //3MB
            }
            else
            {
                size = 10 * 1024 * 1024; //10MB
            }
            
            return new BlobContents(
                new MemoryStream(
                    System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(
                    RandomBlobContent(size/1024))));
        }

        public static string GetProperties(string accountEndpoint, string accountName, string adminAccount, string adminAccountKey, int timeoutMilliSeconds)
        {
            Uri uri;
            HttpWebRequest request;
            try
            {
                uri = BuildUri(accountEndpoint, timeoutMilliSeconds / 1000, null, accountName);
                request = CreateAccountServiceRequest(uri, HttpVerbs.Get, adminAccount, adminAccountKey, TimeSpan.FromMilliseconds(timeoutMilliSeconds));

                request.ContentLength = 0;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                using (HttpWebResponse response = (HttpWebResponse)e.Response)
                {
                    throw RestApiException.GenerateException(response, e);
                }
            }
            catch (Exception e)
            {
                throw RestApiException.GenerateException(null, e);
            }
        }


        public static Uri BuildUri(string host, int timeoutInSeconds, NameValueCollection inputQueryParams, params string[] objectNames)
        {
            // Build URL
            Uri uri = null;
            if (objectNames.Length <= 0)
            {
                uri = new Uri(host);
            }
            else
            {
                StringBuilder uriPath = new StringBuilder();
                Uri serviceUri = new Uri(host);
                uriPath.Append(string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}.{3}:{4}", serviceUri.Scheme, Uri.SchemeDelimiter, objectNames[0], serviceUri.Host, serviceUri.Port));
                for (int i = 1; i < objectNames.Length; i++)
                {
                    uriPath.Append("/");
                    uriPath.Append(objectNames[i]);
                }
                uri = new Uri(uriPath.ToString());
            }
            UriBuilder builder = new UriBuilder(uri);

            // Build query params collection. As of now we only add timeoutInSeconds
            NameValueCollection queryParams;
            if (inputQueryParams != null)
                queryParams = new NameValueCollection(inputQueryParams);
            else
                queryParams = new NameValueCollection();
            queryParams.Add(QueryParamNames.Timeout, timeoutInSeconds.ToString(CultureInfo.InvariantCulture));

            // Build query string
            StringBuilder buffer = new StringBuilder();
            foreach (string key in queryParams.AllKeys)
                buffer.Append(key + "=" + HttpUtility.UrlEncode(queryParams[key]) + "&");  // UrlEncode does the same thing.
            if (buffer.Length > 0)
                buffer.Remove(buffer.Length - 1, 1);    // Remove the last trailing "&"
            builder.Query = buffer.ToString();

            return builder.Uri;
        }

        public static HttpWebRequest CreateAccountServiceRequest(Uri uri, string verb, string adminAccount, string adminAccountKey, TimeSpan timeout)
        {
            // accountName can be null
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Method = verb;
            request.Timeout = (int)timeout.TotalMilliseconds;

            // Add date header
            string dateValue = DateTime.UtcNow.ToString("R", CultureInfo.InvariantCulture);
            request.Headers.Add(HeaderNames.NephosDate, dateValue);

            request.Headers.Add(HeaderNames.Authorization,
                GenerateAccountAuthorizationHeader(request, adminAccount, adminAccountKey));

            return request;
        }


        protected static string GenerateAccountAuthorizationHeader(HttpWebRequest request, string authenticatingAccount, string authenticatingAccountKey)
        {
            // Generate the canoncialized string to sign
            // We start with the HTTP method
            StringBuilder canonicalizedString = new StringBuilder(request.Method);
            
            // MD5 header value comes next
            string contentMD5 = null;
            ArrayList contentMD5Values = GetHeaderValues(request.Headers, HeaderNames.ContentMD5);
            if (contentMD5Values.Count > 0)
            {
                if (contentMD5Values.Count == 1)
                {
                    contentMD5 = (string)contentMD5Values[0];
                }
                else
                {
                    // If we have multiple values of content MD5, throw an exception.
                    throw new Exception("The given HTTP request header contains more than one Content-MD5 values");
                }
            }
            canonicalizedString.Append(SignatureDelimitter);
            canonicalizedString.Append(contentMD5);

            // Date header (more precisely, the Nephos-specific date header)
            canonicalizedString.Append(SignatureDelimitter);
            canonicalizedString.Append(request.Headers[HeaderNames.NephosDate]);

            // URI Path (undecoded)
            canonicalizedString.Append(SignatureDelimitter);
            canonicalizedString.Append(request.RequestUri.AbsolutePath);
            
            // The comp=xxx param, if any
            NameValueCollection queryVariables = HttpUtility.ParseQueryString(request.Address.Query);
            string compQueryParameterValue = queryVariables[QueryParamNames.Component];
            if (compQueryParameterValue != null)
            {
                canonicalizedString.Append("?");
                canonicalizedString.Append(QueryParamNames.Component);
                canonicalizedString.Append("=");
                canonicalizedString.Append(compQueryParameterValue);
            }

            // Now compute the hash value
            string signature;
            using (HMACSHA256 hmacsha256 = new HMACSHA256(Convert.FromBase64String(authenticatingAccountKey)))
            {
                signature = Convert.ToBase64String(hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(canonicalizedString.ToString())));
            }

            return string.Format(CultureInfo.InvariantCulture, "{0} {1}:{2}", "SharedKey", authenticatingAccount, signature);
        }

        protected static ArrayList GetHeaderValues(NameValueCollection headers, string headerName)
        {
            ArrayList arrayOfValues = new ArrayList();
            string[] values = headers.GetValues(headerName);
            if (values != null)
            {
                foreach (string value in values)
                {
                    arrayOfValues.Add(value.TrimStart());
                }
            }
            return arrayOfValues;
        }

        public static string GetCreateAccountRequestBody(
            string accountName, string permission, string georegion, string geodomain,
            string sharedKey, string sharedKey2)
        {
            string createAccountXmlRequest;
            
            createAccountXmlRequest = String.Format("<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Account>" +
                "  <Name>{0}</Name>" +
                "  <RDAccountName>{1}</RDAccountName>" +
                "  <Permissions>{2}</Permissions>" +
                "  <GeoRegion>{3}</GeoRegion>" +
                "  <GeoDomain>{4}</GeoDomain>" +
                "  <Keys>" +
                "    <Key>" +
                "      <Name>KeyName1</Name>" +
                "      <Value>{5}</Value>" +
                "    </Key>" +
                "    <Key>" +
                "      <Name>KeyName2</Name>" +
                "      <Value>{6}</Value>" +
                "    </Key>" +
                "  </Keys>" +
                "</Account>",
                accountName, "reddog", permission, georegion, geodomain, sharedKey, sharedKey2);

            //Console.WriteLine(createAccountXmlRequest);

            return createAccountXmlRequest;
        }

        public static string GetRandomStorageKey()
        {
            byte[] sharedKeyBytes;
            RNGCryptoServiceProvider rng;
            string sharedKey;

            // Generate a random shared key
            sharedKeyBytes = new byte[64];
            rng = new RNGCryptoServiceProvider();
            rng.GetBytes(sharedKeyBytes);
            sharedKey = Convert.ToBase64String(sharedKeyBytes);

            return sharedKey;
        }


        public static Uri BuildAccountUri(string host, int timeoutInSeconds, NameValueCollection inputQueryParams, params string[] objectNames)
        {
            // Build URL
            Uri uri = null;
            if (objectNames.Length <= 0)
            {
                uri = new Uri(host);
            }
            else
            {
                string relativePath = string.Join("/", objectNames);
                uri = new Uri(new Uri(host), relativePath);
            }

            //System.Console.WriteLine("uri: {0}", uri.ToString());

            UriBuilder builder = new UriBuilder(uri);

            // Build query params collection. As of now we only add timeoutInSeconds
            NameValueCollection queryParams;
            if (inputQueryParams != null)
                queryParams = new NameValueCollection(inputQueryParams);
            else
                queryParams = new NameValueCollection();
            queryParams.Add(QueryParamNames.Timeout, timeoutInSeconds.ToString(CultureInfo.InvariantCulture));

            // Build query string
            StringBuilder buffer = new StringBuilder();
            foreach (string key in queryParams.AllKeys)
                buffer.Append(key + "=" + HttpUtility.UrlEncode(queryParams[key]) + "&");  // UrlEncode does the same thing.
            if (buffer.Length > 0)
                buffer.Remove(buffer.Length - 1, 1);    // Remove the last trailing "&"
            builder.Query = buffer.ToString();

            return builder.Uri;
        }


        public static void AddBody(HttpWebRequest request, byte[] body)
        {
            request.ContentLength = body.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(body, 0, body.Length);
            }
        }

        public static string RandomStorageAccount(string prefix)
        {
            return (prefix + Guid.NewGuid().ToString("N").Substring(16, 10)).ToLower();
        }

        public static string RandomString(int len, bool lowerCase)
        {
            StringBuilder builder;
            char ch;
            Random rand;

            rand = new Random();
            builder = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(rand.Next(0, 26) + 65));
                builder.Append(ch);
            }
            if (lowerCase) return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string RandomBlobContent(int numBlock)
        {
            StringBuilder sb;
            string pad;

            pad = RandomString(1024, false);
            sb = new StringBuilder();
            for (int i = 0; i < numBlock; i++)
            {
                sb.Append(pad);
            }

            return sb.ToString();
        }
    }

    public class RestApiException : Exception
    {
        protected HttpStatusCode statusCode;
        protected string msRequestID;

        internal RestApiException(HttpStatusCode statusCode, string message, Exception innerException, string msRequestID)
            : base(message, innerException)
        {
            this.statusCode = statusCode;
            this.msRequestID = msRequestID;
        }

        public HttpStatusCode StatusCode
        {
            get { return statusCode; }
        }

        public string MsRequestID
        {
            get { return msRequestID; }
        }

        public override string ToString()
        {
            string result = base.ToString();
            if (msRequestID != null)
            {
                result = "MS-Request-ID: " + msRequestID + Environment.NewLine + result;
            }
            return result;
        }

        public static RestApiException GenerateException(HttpWebResponse response, Exception innerException)
        {
            string requestID = null;
            if (response != null)
            {
                //get MS-request-id from response header
                if (response.Headers != null && response.Headers["x-ms-request-id"] != null)
                {
                    requestID = response.Headers.Get("x-ms-request-id");
                }
                // Usually it is the responsibility of the creator/receiver to dispose an object but we are doing it here anyways, just in case someone overlooked it in the future.
                response.Close();
                return new RestApiException(response.StatusCode, response.StatusDescription, innerException, requestID);
            }
            else if (innerException != null)
                return new RestApiException(0, innerException.Message, innerException, null);
            else
                return new RestApiException(0, string.Empty, null, null);
        }
    }
}
