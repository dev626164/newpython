﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Cis.E2E.QualityBar
{
    public class xStoreQBarResources
    {
        public class StorageAccount
        {
            public string Name;
            public string Key;
            public string Location;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public class Container
        {
            public string Name;
            public string Account;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public class Blob
        {
            public string Name;
            public long Size;
            public string Account;
            public string Container;
            public DateTime CreatedAt;
            public ResourceStatus Status;
        }

        public int AccountCount
        {
            get
            {
                int count;
                lock (AccountList)
                {
                    count = AccountList.Count;
                }

                return count;
            }
        }

        public int ContainerCount
        {
            get
            {
                int count;
                lock (ContainerList)
                {
                    count = ContainerList.Count;
                }

                return count;
            }
        }

        public int BlobCount
        {
            get
            {
                int count;
                lock (BlobList)
                {
                    count = BlobList.Count;
                }

                return count;
            }
        }

        Dictionary<string, StorageAccount> AccountList;
        Dictionary<string, Container> ContainerList;
        Dictionary<string, Blob> BlobList;

        public xStoreQBarResources()
        {
            AccountList = new Dictionary<string, StorageAccount>();
            ContainerList = new Dictionary<string, Container>();
            BlobList = new Dictionary<string, Blob>();
        }

        #region AddResources
        public void AddAccount(string name, string key, string location, DateTime createdAt)
        {
            lock (AccountList)
            {
                AccountList[name] = new StorageAccount()
                {
                    Name = name,
                    Key = key,
                    Location = location,
                    CreatedAt = createdAt,
                    Status = ResourceStatus.Idle
                };
            }
        }

        public void AddContainer(string name, string storageAccount, DateTime createdAt)
        {
            lock (ContainerList)
            {
                ContainerList[name] = new Container()
                {
                    Name = name,
                    Account = storageAccount,
                    CreatedAt = createdAt,
                    Status = ResourceStatus.Idle
                };
            }

            return;
        }

        public void AddBlob(string name, long size, string storageAccount, string container, DateTime createdAt)
        {
            lock (BlobList)
            {
                BlobList[name] = new Blob()
                {
                    Name = name,
                    Size = size,
                    Account = storageAccount,
                    Container = container,
                    CreatedAt = createdAt,
                    Status = ResourceStatus.Idle
                };
            }

            return;
        }
        #endregion //AddResources

        #region get resource properties
        public string GetAccountKey(string account)
        {
            string key;

            key = string.Empty;
            lock (AccountList)
            {
                key = AccountList[account].Key;
            }

            return key;
        }
        #endregion //get resource properties

        #region RandomGetResources
        public StorageAccount RandomGetAccount(ResourceStatus status)
        {
            StorageAccount one;
            int i, index, idleCount;

            one = null;
            lock (AccountList)
            {
                idleCount = 0;
                foreach (StorageAccount s in AccountList.Values)
                {
                    if (s.Status == ResourceStatus.Idle) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (StorageAccount s in AccountList.Values)
                    {
                        if (s.Status != ResourceStatus.Idle) continue;

                        if (i == index)
                        {
                            s.Status = status;
                            one = s;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.StorageAccountUnavailable);
            }

            return one;
        }

        public Container RandomGetContainer(ResourceStatus status)
        {
            Container one;
            int i, index, idleCount;

            one = null;
            lock (ContainerList)
            {
                idleCount = 0;
                foreach (Container c in ContainerList.Values)
                {
                    if (c.Status != ResourceStatus.Idle) continue;
                    idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (Container c in ContainerList.Values)
                    {
                        if (c.Status != ResourceStatus.Idle) continue;

                        if (i == index)
                        {
                            c.Status = status;
                            one = c;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.StorageContainerUnavailable);
            }

            return one;
        }

        public Blob RandomGetBlob(ResourceStatus status)
        {
            Blob one;
            int i, index, idleCount;

            one = null;
            lock (BlobList)
            {
                idleCount = 0;
                foreach (Blob b in BlobList.Values)
                {
                    if (b.Status == ResourceStatus.Idle) idleCount++;
                }

                if (idleCount != 0)
                {
                    i = 0;
                    index = new Random().Next(0, idleCount);
                    foreach (Blob b in BlobList.Values)
                    {
                        if (b.Status != ResourceStatus.Idle) continue;

                        if (i == index)
                        {
                            b.Status = status;
                            one = b;
                            break;
                        }
                        i++;
                    }
                }
            }

            if (one == null)
            {
                throw new Exception(WarningMessages.StorageBlobUnavailable);
            }

            return one;
        }
        #endregion //RandomGetResources

        #region UpdateResourceStatus
        public void UpdateAccountStatus(string account, ResourceStatus status)
        {
            lock (AccountList)
            {
                AccountList[account].Status = status;
            }
            return;
        }

        public void UpdateContainerStatus(string container, ResourceStatus status)
        {
            lock (ContainerList)
            {
                ContainerList[container].Status = status;
            }
            return;
        }

        public void UpdateBlobStatus(string blob, ResourceStatus status)
        {
            lock (BlobList)
            {
                BlobList[blob].Status = status;
            }
            return;
        }
        #endregion //UpdateResourceStatus

        #region DeleteResources
        public bool DeleteAccount(string account)
        {
            lock (AccountList)
            {
                if (AccountList.ContainsKey(account))
                {
                    AccountList.Remove(account);
                }
            }

            return true;
        }

        public bool DeleteContainer(string container)
        {
            lock (ContainerList)
            {
                if (ContainerList.ContainsKey(container))
                {
                    ContainerList.Remove(container);
                }
            }

            return true;
        }

        public bool DeleteBlob(string blob)
        {
            lock (BlobList)
            {
                if (BlobList.ContainsKey(blob))
                {
                    BlobList.Remove(blob);
                }
            }

            return true;
        }
        #endregion //DeleteResources
    }
}
