﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.IO;
using System.ServiceModel;
using Microsoft.AirWatch.PerformanceTests.Constants;

namespace Microsoft.AirWatch.PerformanceTests
{
    /// <summary>
    /// Summary description for MapModelDataEntry
    /// </summary>
    [TestClass]
    public class MapModelDataEntryTest
    {
        static XmlDocument xmlDoc = null;
        static IMapModelDataEntryService service = null;

        public MapModelDataEntryTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
         [ClassInitialize()]
        public static void MapModelDataEntryTestInitialize(TestContext testContext) 
        {
            //Load XML document
            xmlDoc = new XmlDocument();
            using (StreamReader sr = new StreamReader(@"..\..\..\Microsoft.AirWatch.PerformanceTests\Data\input.xml"))
            {
                xmlDoc.LoadXml(sr.ReadToEnd());
            }

            //Initialize WCF Client
            service = ChannelFactory<IMapModelDataEntryService>.CreateChannel(new BasicHttpBinding(), 
                                            new EndpointAddress(TestConstants.AirWatchServer + "/Services/MapModelDataEntryService.svc"));
        }
        //
        // Use ClassCleanup to run code after all tests in a class have run
         [ClassCleanup()]
        public static void MapModelDataEntryTestCleanup() 
        { 
             
        }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void MapModelDataEntryService()
        {
            //Begin Timer
            this.TestContext.BeginTimer("MapModelDataEntryService");

            //Invoke the Web Method
            service.EnterData(xmlDoc.GetElementById("data"));

            //End Timer
            this.TestContext.EndTimer("MapModelDataEntryService");
        }
    }
}
