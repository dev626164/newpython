﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.AirWatch.PerformanceTests.Constants
{
    public class TestConstants
    {
        public const string AirWatchServer = @"http://airwatchtest.cloudapp.net";
        public const string AirWatchProxy = "default";
        public const string BingToken_TestContext_Key = "BingToken";
        public const string BingToken_ClientIPAddress = "127.0.0.1";
    }
}
