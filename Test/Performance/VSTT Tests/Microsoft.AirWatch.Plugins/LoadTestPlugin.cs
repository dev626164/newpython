﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.LoadTesting;
using Microsoft.VisualStudio.TestTools.WebTesting;
using Microsoft.AirWatch.PerformanceTests.Constants;

namespace Microsoft.AirWatch.PerformanceTests.Plugins
{
    public class LoadTestPlugin : ILoadTestPlugin
    {
        LoadTest awLoadTest = new LoadTest();
        static readonly object lockObject = new object();
        
        #region ILoadTestPlugin Members

        public void Initialize(LoadTest loadTest)
        {
            //TODO: Currently Hardcoding the BingToken for first time use. This is not the best way to do it. Need to come up with a way such that
            //the load test engine executes the GetBingToken test first.
            //loadTest.Context.Add(TestConstants.BingToken_TestContext_Key, "eJkaHmB7YQg3mhfAAgQ7bRnNdMBgytBT_TMth_sECu-4XhTklihUZpmbHUfx0aDOsNu-sj_M0YNim_B5inzVRA2");
            awLoadTest = loadTest;
            loadTest.TestFinished += new EventHandler<TestFinishedEventArgs>(loadTest_TestFinished);
            loadTest.TestSelected += new EventHandler<TestSelectedEventArgs>(loadTest_TestSelected);
        }

        void loadTest_TestSelected(object sender, TestSelectedEventArgs e)
        {
            //For the SearchLocation Test
            if (e.TestName.Equals("SearchLocation"))
            {
                // Pick up the bing token from the load test context and place it in the test's context
                lock (lockObject)
                {
                    if (!e.UserContext.Keys.Contains(TestConstants.BingToken_TestContext_Key))
                    {
                        e.UserContext.Add(TestConstants.BingToken_TestContext_Key, awLoadTest.Context[TestConstants.BingToken_TestContext_Key]);
                    }
                    else
                    {
                        e.UserContext[TestConstants.BingToken_TestContext_Key] = awLoadTest.Context[TestConstants.BingToken_TestContext_Key];

                    }
                }
            }
        }

        void loadTest_TestFinished(object sender, TestFinishedEventArgs e)
        {
            //For the GetBingToken Test
            if (e.TestName.Equals("GetBingToken"))
            {
                //The GetBingToken test, extracts the BingToken from the response and stores it in it's test context.
                //Copy the value from it's test context and place it in the load test context so that 'SearchLocation' can pick it up.
                lock (lockObject)
                {
                    string bingToken = e.UserContext[TestConstants.BingToken_TestContext_Key].ToString();
                    if (awLoadTest.Context.ContainsKey(TestConstants.BingToken_TestContext_Key))
                    {
                        awLoadTest.Context[TestConstants.BingToken_TestContext_Key] = bingToken;
                    }
                    else
                    {
                        awLoadTest.Context.Add(TestConstants.BingToken_TestContext_Key, bingToken);
                    }
                }
            }
        }

        #endregion
    }

    public class AirWatchLoadTestPlugin : ILoadTestPlugin
    {
        //This load profile will be used by the "Regular AirWatch" Scenario
        LoadTestStepLoadProfile airWatchRegularLoadProfile = null;

        //This load profile will be used by the "Data Feed Services" Scenario
        LoadTestConstantLoadProfile airWatchDataFeedLoadProfile = null;

        LoadTest m_loadTest = null;

        #region ILoadTestPlugin Members

        public void Initialize(LoadTest loadTest)
        {
            m_loadTest = loadTest;

            m_loadTest.TestStarting += new EventHandler<TestStartingEventArgs>(m_loadTest_TestStarting);
            m_loadTest.TestFinished += new EventHandler<TestFinishedEventArgs>(m_loadTest_TestFinished);
        }

        void m_loadTest_TestFinished(object sender, TestFinishedEventArgs e)
        {
            if(e.TestName.Equals("GetBingToken"))
            {
                string bingToken = string.Empty;
                if (e.UserContext.ContainsKey(TestConstants.BingToken_TestContext_Key))
                {
                    bingToken = e.UserContext[TestConstants.BingToken_TestContext_Key].ToString();
                    if (m_loadTest.Context.ContainsKey(TestConstants.BingToken_TestContext_Key))
                    {
                        m_loadTest.Context[TestConstants.BingToken_TestContext_Key] = bingToken;
                    }
                    else
                    {
                        m_loadTest.Context.Add(TestConstants.BingToken_TestContext_Key, bingToken);

                        this.intializeAirWatchDataFeedsLoadProfile();
                        this.intializeAirWatchRegularLoadProfile();

                        m_loadTest.Scenarios[1].LoadProfile = airWatchRegularLoadProfile;
                        m_loadTest.Scenarios[2].LoadProfile = airWatchDataFeedLoadProfile;
                    }
                }          
            }
        }

        void m_loadTest_TestStarting(object sender, TestStartingEventArgs e)
        {
            //For SearchLocation Test
            if(e.TestName.Equals("SearchLocation"))
            {
                string bingToken = m_loadTest.Context[TestConstants.BingToken_TestContext_Key].ToString();
                if (e.TestContextProperties.ContainsKey(TestConstants.BingToken_TestContext_Key))
                {
                    e.TestContextProperties[TestConstants.BingToken_TestContext_Key] = bingToken;
                }
                else
                {
                    e.TestContextProperties.Add(TestConstants.BingToken_TestContext_Key, bingToken);
                }
            }
        }

        #endregion

        private void intializeAirWatchRegularLoadProfile()
        {
            //Initialize airWatchRegularLoadProfile
            airWatchRegularLoadProfile = new LoadTestStepLoadProfile();
            airWatchRegularLoadProfile.InitialUserCount = 50;
            airWatchRegularLoadProfile.MaxUserCount = 1600;
            airWatchRegularLoadProfile.StepDuration = 120;
            airWatchRegularLoadProfile.StepRampTime = 30;
            airWatchRegularLoadProfile.StepUserCount = 100;
        }

        private void intializeAirWatchDataFeedsLoadProfile()
        {
            //Initialize airWatchDataFeedLoadProfile
            airWatchDataFeedLoadProfile = new LoadTestConstantLoadProfile();
            airWatchDataFeedLoadProfile.MaxUserCount = 1;
        }
    }
}
