﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.VisualStudio.TestTools.WebTesting;
using Microsoft.VisualStudio.TestTools.LoadTesting;
using Microsoft.AirWatch.PerformanceTests.Constants;

namespace Microsoft.AirWatch.PerformanceTests.Plugins
{
    public class AirWatchWebTestPlugin : WebTestPlugin
    {
        [DisplayName("New Timeout Value")]
        [Description("Request timeout value to apply to all requests in every test")]
        [DefaultValue(600)]
        int Timeout { get; set; }

        //Increase Default Timeout period to 600 seconds
        public override void PreRequest(object sender, PreRequestEventArgs e)
        {
            e.Request.Timeout = Timeout;
        }        
    }

    public class GetBingTokenWebTestPlugin : WebTestPlugin
    {
        //After the response has been recieved, extract the bingToken from the TestContext and pass it on to
        //the user's loadTestUserContext - so that the bing token can be used by other tests.
        public override void PostRequest(object sender, PostRequestEventArgs e)
        {
            LoadTestUserContext loadTestUserContext = null;
           
            if (e.WebTest.Context.ContainsKey("$LoadTestUserContext"))
            {
                loadTestUserContext = e.WebTest.Context["$LoadTestUserContext"] as LoadTestUserContext;
                //If loadTestUserContext already contains the bing token, update it's value
                if (loadTestUserContext.ContainsKey(TestConstants.BingToken_TestContext_Key))
                {
                    loadTestUserContext[TestConstants.BingToken_TestContext_Key] = e.WebTest.Context[TestConstants.BingToken_TestContext_Key].ToString();
                }
                //Else, add the bing token to the loadTestUserContext
                else
                {
                    loadTestUserContext.Add(TestConstants.BingToken_TestContext_Key, e.WebTest.Context[TestConstants.BingToken_TestContext_Key].ToString());
                }
            }
        }
    }
}
