﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Microsoft.AirWatch.Utilities
{
    class RandomNumberGenerator
    {
        static Random randomIntGenerator = null;
        static Random randomDoubleGenerator = null;

        private static int GetNextRandomInt()
        {
            randomIntGenerator = new Random(DateTime.Now.Millisecond);
            //Thread is made to sleep for 1 ms so that the seed for the random number generator is 
            //as varied as possible, each time this path is executed.
            Thread.Sleep(1);
            return randomIntGenerator.Next();
        }

        public static double GetNextRandomDouble()
        {
            randomDoubleGenerator = new Random(GetNextRandomInt());
            return randomDoubleGenerator.NextDouble();
        }
    }
}
