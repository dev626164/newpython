﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.AirWatch.Utilities
{
    class Program
    {
        public static void Main(string[] args)
        {
            CoordinateGenerator cGen = new CoordinateGenerator();
            cGen.GenerateCoordinatesForEurope();
        }
    }
}
