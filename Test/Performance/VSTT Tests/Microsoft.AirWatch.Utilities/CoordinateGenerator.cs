﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Microsoft.AirWatch.Utilities
{
    public class CoordinateGenerator
    {
        public CoordinateGenerator()
        {

        }

        public void GenerateCoordinatesForEurope()
        {
            TextReader coordinatesReader = null;
            TextWriter coordinatesWriter = null;
            string fileContents = string.Empty;
            string[] coordinates = null;
            string latitude = string.Empty;
            string longitude = string.Empty;

            try
            {
                coordinatesReader = new StreamReader(@"..\..\Coordinates.txt");
                fileContents = coordinatesReader.ReadToEnd();
                coordinates = fileContents.Split(new char[] { '\n' });

                //Create a file and write the header to it.
                coordinatesWriter = new StreamWriter(@"..\..\GeneratedCoordinates.txt");
                coordinatesWriter.WriteLine("Longitude,Latitude");

                foreach (string coordinate in coordinates)
                {
                    string[] tempStringArray = coordinate.Split(new char[] { '\t' });
                    longitude = tempStringArray[0];
                    latitude = tempStringArray[1].Remove(tempStringArray[1].Length - 1);

                    for (int i = 0; i < 10; i++)
                    {
                        double e_longitude = RandomNumberGenerator.GetNextRandomDouble();
                        double e_latitude = RandomNumberGenerator.GetNextRandomDouble();

                        double tempLongitude = Convert.ToDouble(longitude);
                        double tempLatitude = Convert.ToDouble(latitude);
                        if (tempLongitude < 0)
                            tempLongitude -= e_longitude;
                        else
                            tempLongitude += e_longitude;
                        tempLatitude += e_latitude;

                        coordinatesWriter.WriteLine(string.Format(@"{0},{1}", tempLongitude, tempLatitude));

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                coordinatesWriter.Close();
                coordinatesReader.Close();
            }

            
        }
    }
}
