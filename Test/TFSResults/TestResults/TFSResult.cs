﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.Configuration;

namespace TestResults
{
      
        class TFSTestResult
        {

            private void TFSLogInit()
            {
            }
            private void TFSLogClose()
            {
            }


            public static void TFSLogResult(string title, string testid, string result, string iteration, string bugid)
            {

                try
                {
                    string tfsserver = ConfigSettings.getConfigvalue("TFSSERVER");
                    TeamFoundationServer tfs = new TeamFoundationServer(tfsserver);
                    WorkItemStore wis = (WorkItemStore)tfs.GetService(typeof(WorkItemStore));

                    Project teamProject = null;
                    string project = ConfigSettings.getConfigvalue("projectname");
                    // if project is null or 
                    for (int projectcnt = 0; projectcnt < wis.Projects.Count; projectcnt++)
                    {

                        teamProject = wis.Projects[projectcnt];
                        if (teamProject.Equals(project))
                        {
                            break;
                        }

                    }

                    //Iterate the loop and match name is Airwatch, assign the teamproject as Airwatch
                    //Console.WriteLine(teamProject.Name);
                    //foreach (WorkItemType wit in teamProject.WorkItemTypes)
                    // Console.WriteLine(wit.Name);

                    //WorkItemCollection wic = wis.Query("Project='Test' AND Type='Bug'");
                    //foreach (WorkItem wiEntry in wic)
                    //{
                    // }

                    //WorkItemType witBug = teamProject.WorkItemTypes["Bug"];
                    WorkItemType witTR = teamProject.WorkItemTypes["Test Result"];

                    if (witTR != null)
                    {
                        //Console.WriteLine("Adding new Test result to Team Project {0}", teamProject.Name);

                        WorkItem wi = new WorkItem(witTR);
                        /*FieldCollection fc = wi.Fields;

                        for (int i = 0; i < fc.Count; i++) {
                            Console.WriteLine(fc[i].Name);
                        }
                        Console.ReadLine();
                        */
                         wi.Title = title;
                         wi.IterationPath = ConfigSettings.getConfigvalue("Iteration");
                         wi.Fields["Test Browser"].Value = ConfigSettings.getConfigvalue("Test Browser");
                         wi.Fields["Test OS"].Value = ConfigSettings.getConfigvalue("Test OS"); ;
                         wi.Fields["Value"].Value = result;
                         wi.Fields["ResultType"].Value = "Manual";
                         wi.Fields["Test Id"].Value = testid;
                         if (bugid.Equals("")) {
                         }
                         else{
                          wi.Fields["Microsoft.Test.BugId"].Value= bugid;
                          }
                         wi.Save();
                         
                        
                        int id = wi.Id;
                        Console.WriteLine(id);
                        

                       Console.WriteLine(title);
                       Console.WriteLine("AirWatch");
                       Console.WriteLine(ConfigSettings.getConfigvalue("Test Browser"));
                       Console.WriteLine(ConfigSettings.getConfigvalue("Test OS"));
                       Console.WriteLine(result);
                       Console.WriteLine("Manual");
                       Console.WriteLine(testid);
                       Console.WriteLine(iteration);
                       if (bugid.Equals("")) {
                       }
                       else{
                          Console.WriteLine(bugid);
                       }
                        
                        

                    }
                }
                catch (Exception ex)
                {
                   // ExceptionLogger.LogExceptionInfo(ex, "Check the server exists and correct in config file" + "\n" + "Server is up and running \\n " + "All Mandatory fields have value or reuired values for the fields are missing");
                    Console.WriteLine(ex.ToString());
                    Console.ReadLine();
                }


            }

        }
    }


