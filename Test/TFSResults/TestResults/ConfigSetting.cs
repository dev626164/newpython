﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestResults
{
    class ConfigSettings
    {
        /// <summary>
        /// Allows to assign and retrive the  config values
        /// </summary>
        /// <param name="inputstr"></param>
        /// <returns></returns>
        public static string getConfigvalue(string inputstr)
        {
            string retval = "";

            Dictionary<string, string> configvalues = new Dictionary<string, string>();
            try
            {
                // Storing the config values
                configvalues.Add("testcasefile", "C:\\Users\\ashisbha\\Desktop\\sampletestcase.xlsx");
                configvalues.Add("TFSSERVER", "tkbgitvstfat08");
                configvalues.Add("Test OS", "Windows Vista");
                configvalues.Add("Test Browser", "IE 7.0");
                configvalues.Add("Iteration", "AirWatch\\Iteration 1");
                configvalues.Add("projectname", "AirWatch");
                

                if (inputstr.Equals(null) || inputstr.Equals(""))
                {
                    //inputstr is empty
                }
                else
                {
                    if (configvalues.ContainsKey(inputstr))
                    {
                        retval = configvalues[inputstr];
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
             }
            return retval;
        }
    }
}
