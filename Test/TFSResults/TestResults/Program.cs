﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;

namespace TestResults
{
    class test {

        public static void Main() {

            Testcase tc = new Testcase();
            List<string> testcases;
            testcases =  tc.GetTestcases(ConfigSettings.getConfigvalue("testcasefile"));
                       
            foreach (string tcase in testcases){
                
                Console.WriteLine(tcase);

                if (tcase.Equals("^Title^Iteration Path^Value^BugId"))
                { 
                    //Skip The first Line
                }
                else
                {
                    string[] values;
                    values = tcase.Split('^');
                    TFSTestResult.TFSLogResult (values[1],values[0], values[3], values[2],values[4]);
                }
            
            }
            Console.ReadLine();
        }
    
    }
     
}