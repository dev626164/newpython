﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;

namespace DriverTest
{
    class Testcase
    {
        static String connectionstring;
        static OleDbConnection objCon;
        static OleDbDataReader readexecl;
        string scenarioid;
        string title;
        string execute;
        string stepname;
        string arguments;
        public List<string> testcases;
        /// <summary>
        /// Retruns the testcases
        /// </summary>
        /// <param name="testcasefile"></param>
        /// <returns>List<string></returns>
        public List<string> GetTestcases(string testcasefile)
        {
            TestCases(testcasefile);
            return testcases;
        }
        /// <summary>
        ///  Retrives the test cases form the scenario sheet
        /// </summary>
        /// <param name="testcasefile"></param>
        /// <returns>void</returns>
        void TestCases(string testcasefile)
        {
            try
            {   // Connection string
                connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + testcasefile + ";Extended Properties=Excel 12.0;";
                objCon = new OleDbConnection(connectionstring);
                objCon.Open();
                //Retrives all the rows form the sheet[Sheet1]
                OleDbCommand objCmdSel = new OleDbCommand("SELECT * FROM [Sheet1$]", objCon);
                readexecl = objCmdSel.ExecuteReader();
                testcases = new List<string>();
                // Read while end of the file not reached 
                while (readexecl.Read())
                {
                    scenarioid = "";
                    title = "";
                    execute = "";
                    stepname = "";
                    arguments = "";
                    //scenarioid  scenarion name  scenariosheet
                    scenarioid = readexecl.GetValue(0).ToString();
                    title = readexecl.GetValue(1).ToString();
                    execute = readexecl.GetValue(2).ToString();
                    stepname = readexecl.GetValue(3).ToString();
                    arguments = readexecl.GetValue(4).ToString();
                    // Adding the test case to list
                    testcases.Add(scenarioid + "^" + title + "^" + execute + "^" + stepname + "^" + arguments + "\n");
                }
                // Calling dispose to clean up 
                Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in getting the tescases form excel sheet");
            }
            finally
            {
                Dispose();
            }
        }
        /// <summary>
        /// Close the files cleanly
        /// </summary>
        public static void Dispose()
        {
            if (readexecl != null)
            {
                readexecl.Close();
            }
            else
            {
                readexecl = null;
            }

            if (objCon != null)
            {
                objCon.Close();
            }
            else
            {
                objCon = null;
            }
        }
    }
}