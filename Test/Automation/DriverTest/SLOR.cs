﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.KAF.UIElements;
using Microsoft.KAF.Factory;
using Microsoft.KAF.UIDriver;
using Microsoft.KAF.Agent.Browser;
using Microsoft.Internal.WLX.WLXLogger;
using Microsoft.KAF.Agent;
using System.Configuration;


namespace DriverTest
{
    class SLOR
    {
      // Declartion of  the Objects 
        #region Maplayers
        public SilverlightUIElement map;
        #endregion 
        #region Home Flyout Panel
        public SilverlightUIElement Home_LockFlyOutPanel;
        public SilverlightUIElement Home_flyout;
        public SilverlightTextBox Homeflyout_Search; 
        public SilverlightTextBlock Homeflyout_changelocation; 
        public SilverlightButton Homeflyout_SearchButton; 
        public SilverlightButton Homeflyout_SearchCancelButton; 
        public SilverlightComboBox Homeflyout_ResultsList; 
        public SilverlightButton Homeflyout_Home;  
        public SilverlightButton Homeflyout_SMS; 
        public SilverlightButton Homeflyout_About; 
        public SilverlightButton Homeflyout_Help;
        public SilverlightTextBlock Homeflyout_homelocation;
        #endregion
        #region search
        public SilverlightTextBox SearchControl_SearchText;
        //public SilverlightButton Home_Search_Go;
        public SilverlightButton SearchControl_SearchButton;
        #endregion
        #region searchresults Objects
        public SilverlightComboBox SearchResults_ResultsList;
        public SilverlightButton SearchResults_NewSearchButton;
        public SilverlightTextBox SearchResults_NewSearchBox;
        public SilverlightButton SearchResults_CloseButton;
        public SilverlightTextBlock Search_ResultsWarning;
        #endregion
        # region Mapcontrol Objects
        public SilverlightButton MapNavigationBar_ToggleMenuButton;
        public SilverlightButton Home_ZoomInButton;
        public SilverlightButton Home_ZoomOutButton;

        public SilverlightButton Home_AerialButton;
        public SilverlightButton Home_RoadButton;
        public SilverlightButton Home_LabelsButton;

        public SilverlightUIElement MapPanControl;
        public SilverlightUIElement pushpinStatic;
        public SilverlightUIElement MapControl;
        public SilverlightButton Mapcontrol_Deletepushpin;
        public SilverlightComboBox Mapcontrol_views;
        public SilverlightButton Mapcontrol_viewToggle;

        # endregion
        # region Pushpin Flyout objects
        public SilverlightButton RateQaulity;
        public SilverlightButton QualifyingWord_1;
        public SilverlightButton QualifyingWord_2;
        public SilverlightButton QualifyingWord_3;
        public SilverlightButton QualifyingWord_4;
        public SilverlightButton QualifyingWord_5;
        public SilverlightButton QualifyingWord_6;
        public SilverlightButton QualifyingWord_7;
        public SilverlightButton QualifyingWord_8;

        public SilverlightButton pushpin_rate; // to rate the pushpin  flyoput panel
        public SilverlightUIElement air_ratingVBad;
        public SilverlightUIElement air_ratingBad;
        public SilverlightUIElement air_ratingMod;
        public SilverlightUIElement air_ratingGood;
        public SilverlightUIElement air_ratingVGood;
        public SilverlightButton pushpin_rate_next;

        public SilverlightUIElement pushpin;
        public SilverlightButton pushpin_delbutton;
        public SilverlightButton pushpin_closebutton;
        #endregion 
        
        #region Generic Objects
        public SilverlightButton FlyOutPanelToggle;
        public SilverlightTextBlock MileInfo;
        public SilverlightTextBox LatitudeVal;
        public SilverlightTextBox LongituteVal;
        #endregion 
        # region Home
        public SilverlightButton Home_Disclaimer; 
        #endregion Home

        

        //public override void  createOR()
        /// <summary>
        /// Create the objects with known properties
        /// </summary>
        public void createOR()
        {
            
            //Search Objects
            SearchControl_SearchText        = UIElementFactory.GetUIElement<SilverlightTextBox>("AutomationId", "SearchControl_SearchText");
            SearchControl_SearchButton      = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SearchControl_SearchButton");
            SearchResults_ResultsList       = UIElementFactory.GetUIElement<SilverlightComboBox>("AutomationId", "SearchResults_ResultsList");
            SearchResults_NewSearchButton   = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SearchResults_NewSearchButton");
            SearchResults_CloseButton       = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SearchResults_CloseButton");
            Search_ResultsWarning           = UIElementFactory.GetUIElement<SilverlightTextBlock>("AutomationId", "ResultsFlyOut_ResultsWarning");
            
            // SearchResults_NewSearchBox   = UIElementFactory.GetUIElement<SilverlightTextBox>("AutomationId","SearchResults_NewSearchTextBox");
            // Home Objects
            Home_Disclaimer = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "AcceptDisclaimer");
            Home_AerialButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "MapNavigationBar_AerialButton");
            Home_RoadButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "MapNavigationBar_RoadButton");
            Home_LabelsButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "MapNavigationBar_LabelsButton");
            //Home_ZoomInButton   = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "VerticalTrackLargeChangeIncreaseRepeatButton");
            //Home_ZoomOutButton  = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "VerticalTrackLargeChangeDecreaseRepeatButton");
            Home_ZoomOutButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "MapNavigationBar_ZoomOutButton");
            Home_ZoomInButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "MapNavigationBar_ZoomInButton");
            MapPanControl = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "MapPanControl");
            pushpinStatic = UIElementFactory.GetUIElement<SilverlightUIElement>("AutomationId", "NavBar_DraggablePushpin");
            // Map Objects(Generic)
            //map = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "VEMap");
            map = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "MapControl");
            MapControl = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "MapControl");
            MileInfo = UIElementFactory.GetUIElement<SilverlightTextBlock>("AutomationId", "Text1", 1);
            Mapcontrol_Deletepushpin = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "DeleteAllPushpins");
            Mapcontrol_views = UIElementFactory.GetUIElement<SilverlightComboBox>("AutomationId", "MapNavigationBar_ChangeViewcombobox");
            Mapcontrol_viewToggle =  UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "DropDownToggle");

            LatitudeVal = UIElementFactory.GetUIElement<SilverlightTextBox>("AutomationId", "txtLatitude");
            LongituteVal = UIElementFactory.GetUIElement<SilverlightTextBox>("AutomationId", "txtLongitude");
            // Home flyout Objects
            Home_flyout = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "FlyOutPanelContainer");
            Homeflyout_Search = UIElementFactory.GetUIElement<SilverlightTextBox>("AutomationId", "SearchResults_NewSearchBox", 1);
            //Homeflyout_changelocation = UIElementFactory.GetUIElement<SilverlightTextBlock>("AutomationId", "ChangeLocation");
            Homeflyout_changelocation = UIElementFactory.GetUIElement<SilverlightTextBlock>("AutomationId", "FlyOutPanel_ChangeLocation");
            Homeflyout_SearchButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SearchResults_NewSearchButton", 1);
            Homeflyout_SearchCancelButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SearchResults_CancelSearchButton");
            Homeflyout_ResultsList = UIElementFactory.GetUIElement<SilverlightComboBox>("AutomationId", "ResultsList");
            Homeflyout_Home = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "HomeRadioButton");
            Homeflyout_SMS = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SmsRadioButton");
            Homeflyout_About = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "AboutRadioButton");
            Homeflyout_Help = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "HelpRadioButton");
            //Genrics Objects                     
            FlyOutPanelToggle = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FlyOutPanelToggle");
            Home_LockFlyOutPanel = UIElementFactory.GetUIElement<SilverlightUIElement>("AutomationId", "LockFlyOutPanel");
            Homeflyout_homelocation = UIElementFactory.GetUIElement<SilverlightTextBlock>("AutomationId", "FlyOutPanel_HomeName");

           // pushpin_cancel = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "Cancel");

            pushpin = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "DataIcon");
            pushpin_delbutton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "PART_DeleteButton");
            pushpin_closebutton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "PART_CloseButton");
            //pushpin flyout 
            RateQaulity = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "Rate");
            QualifyingWord_1 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FirstWord");
            QualifyingWord_2 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SecondWord");
            QualifyingWord_3 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "ThirdWord");
            QualifyingWord_4 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FourthWord");
            QualifyingWord_5 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FifthWord");
            QualifyingWord_6 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SixthWord");
            QualifyingWord_7 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SeventhWord");
            QualifyingWord_8 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "EighthWord");

            //Rate pushpin
            pushpin_rate = UIElementFactory.GetUIElement<SilverlightButton>("Name", "PART_RateRadioButton");
            air_ratingVBad = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_FirstIcon");
            air_ratingBad = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_SecondIcon");
            air_ratingMod = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_ThirdIcon");
            air_ratingGood = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_FourthIcon");
            air_ratingVGood = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_FifthIcon");
            pushpin_rate_next = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "NextStep");
        }
    }
}