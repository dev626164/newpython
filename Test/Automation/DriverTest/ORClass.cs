﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.KAF.UIElements;
using Microsoft.KAF.Factory;
using Microsoft.KAF.UIDriver;
using Microsoft.KAF.Agent.Browser;
using Microsoft.Internal.WLX.WLXLogger;
using Microsoft.KAF.Agent;
using System.Configuration;


namespace DriverTest
{
     class ORClass
    {
        // Declartion of the Objects 

    #region Maplayers
        public SilverlightUIElement map;
    #endregion 
    #region HomePage
        public SilverlightButton Login_Home_Login;
        public SilverlightButton Login_Home_Logout;
        public SilverlightButton Search_Home_Go;
        public SilverlightButton Login_Home_Sendafriend;
    #endregion 
    #region MyPeference
        public SilverlightButton Mypef_Home_Cancel;
        public SilverlightButton Mypef_Home_CompleteRegistration;
        //public SilverlightCheckBox Mypef_Home_acceptTnC;
        public SilverlightComboBox Mypef_Home_Language;
        public SilverlightButton Mypef_Home_Search;
    #endregion
    # region Mapcontrol Objects
        public SilverlightTextBox Search_Home_Textbox;
        public SilverlightButton Home_Search_Go;
        public SilverlightComboBox SearchResults_ResultsList;
        public SilverlightButton SearchResults_NewSearchButton;
    #endregion
    # region Mapcontrol Objects
        public SilverlightButton MapNavigationBar_ToggleMenuButton;
        public SilverlightButton Home_ZoomInButton;
        public SilverlightButton Home_ZoomOutButton;

        public SilverlightButton Home_AerialButton;
        public SilverlightButton Home_RoadButton;
        public SilverlightButton Home_LabelsButton;
    # endregion 
    # region Login Objects
        public HtmlButton  Login_btsignin;
        public HtmlTextBox Login_txpasswd;
        public HtmlTextBox Login_txlogin;
        public HtmlCheckBox Login_chkbxRemMe;
     #endregion
    # region Pushpin and  pushpin Flyout objects
       public SilverlightButton RateQaulity ;
       public SilverlightButton QualifyingWord_1; 
       public SilverlightButton QualifyingWord_2; 
       public SilverlightButton QualifyingWord_3; 
       public SilverlightButton QualifyingWord_4; 
       public SilverlightButton QualifyingWord_5; 
       public SilverlightButton QualifyingWord_6;
       public SilverlightButton QualifyingWord_7; 
       public SilverlightButton QualifyingWord_8;

       public SilverlightButton pushpin_rate; // to rate the pushpin  flyoput panel
       public SilverlightUIElement air_ratingVBad; 
       public SilverlightUIElement air_ratingBad; 
       public SilverlightUIElement air_ratingMod; 
       public SilverlightUIElement air_ratingGood; 
       public SilverlightUIElement air_ratingVGood;
       public SilverlightButton pushpin_rate_next;

       public SilverlightUIElement pushpin;
       public SilverlightButton    pushpin_delbutton;

            
      #endregion 

        public void createOR()
        {

            

            //UI SL elements of the Home page
            Login_Home_Login = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "LogOnControl_LogonButton"); 
            Search_Home_Textbox = UIElementFactory.GetUIElement<SilverlightTextBox>("AutomationId", "SearchControl_SearchText");
            Home_Search_Go = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SearchControl_SearchButton");
            Home_AerialButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId","MapNavigationBar_AerialButton");
            Home_RoadButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId","MapNavigationBar_RoadButton");
            Home_LabelsButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "MapNavigationBar_LabelsButton");
            Home_ZoomInButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "VerticalTrackLargeChangeIncreaseRepeatButton");
            Home_ZoomOutButton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "VerticalTrackLargeChangeDecreaseRepeatButton");
            
            //UI elements of the Live Login page
            Login_txlogin = UIElementFactory.GetUIElement<HtmlTextBox>("input", "name", "login");
            Login_txpasswd = UIElementFactory.GetUIElement<HtmlTextBox>("input", "name", "passwd");
            Login_btsignin = UIElementFactory.GetUIElement<HtmlButton>("input", "name", "SI");
            Login_chkbxRemMe = UIElementFactory.GetUIElement<HtmlCheckBox>("name", "remMe");
            
                 
            map = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "MapControl");

            pushpin = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "DataIcon");
            pushpin_delbutton = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "PART_DeleteButton");
            //pushpin flyout 
             RateQaulity = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "Rate");
             QualifyingWord_1 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FirstWord");
             QualifyingWord_2 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SecondWord");
             QualifyingWord_3 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "ThirdWord");
             QualifyingWord_4 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FourthWord");
             QualifyingWord_5 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "FifthWord");
             QualifyingWord_6 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SixthWord");
             QualifyingWord_7 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "SeventhWord");
             QualifyingWord_8 = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "EighthWord");

            //Rate pushpin
            pushpin_rate = UIElementFactory.GetUIElement<SilverlightButton>("Name", "PART_RateRadioButton");
            air_ratingVBad = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_FirstIcon");
            air_ratingBad = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_SecondIcon");
            air_ratingMod = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_ThirdIcon");
            air_ratingGood = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_FourthIcon");
            air_ratingVGood = UIElementFactory.GetUIElement<SilverlightUIElement>("Name", "PART_FifthIcon");
            pushpin_rate_next = UIElementFactory.GetUIElement<SilverlightButton>("AutomationId", "NextStep");
            
            //AirPushpinLayer (Type:Microsoft.VirtualEarth.MapControl.MapLayer) = "Microsoft.VirtualEarth.MapControl.MapLayer"
            //AutomationId (Type:System.String) = "SearchResults_ResultsList"
            //AutomationId (Type:System.String) = "UserFavorites_ResultsList"
        }
    }
}