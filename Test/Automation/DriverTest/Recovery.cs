﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.KAF.UIElements;
using Microsoft.KAF.Factory;
using Microsoft.KAF.UIDriver;
using Microsoft.KAF.Agent.Browser;
using Microsoft.Internal.WLX.WLXLogger;
using Microsoft.KAF.Agent;
using System.Configuration;

namespace DriverTest
{
    enum recoveryaction {SKIPCONTINUE, CLOSEBROWSER, CLOSERESTART};
    class Recovery
    {
    
        public void StopBrowser()
        {
             BrowserAgentFactory.BrowserAgent.ToolBar.Stop.Click();
        }

        public void CloseBrowser()
        {
            BrowserAgentFactory.BrowserAgent.CloseWebBrowser();
            BrowserAgentFactory.BrowserAgent.Process.Kill();
        }

        public void FinalCleanUp() {
            Logger.Dispose();
            ExceptionLogger.Dispose();
            Testcase.Dispose();
        }

        public void RestartTest() { 
        }
    }
}
