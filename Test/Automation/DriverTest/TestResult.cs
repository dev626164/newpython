﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;

namespace DriverTest
{
    class TestResult
    {


        private static bool stepresult;
        static ExcelLog elog;
        

        // add for TFS

        public static void LogInit()
        {
            //if(Excel)
            //string excelfile = ConfigurationSettings.AppSettings["excellogfile"].ToString();
            string excelfile= ConfigSettings.getConfigvalue("excellogfile");
            Logger.LogInfo("Result excel file " + excelfile);
            elog = new ExcelLog(excelfile);
            elog.CreateExcel();
            //else(TFS)
            
        }
        public static void LogClose()
        {
            elog.closeSheet();
            elog.CleanUp();
        }

        public static void WriteLog( string stepname, string result, string message)
        {
            elog.AddResultrow(stepname,result,message);
           
        }
        
    }

}