﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.KAF.UIElements;
using Microsoft.KAF.Factory;
using Microsoft.KAF.UIDriver;
using Microsoft.KAF.Agent.Browser;
using Microsoft.Internal.WLX.WLXLogger;
using Microsoft.KAF.Agent;
using System.Reflection;

namespace DriverTest
{
    [TestClass]
    public class Framework
    {
        /// <summary>
        /// Runs before every test case
        /// Declares and retrives all the values from 
        /// </summary>
        /// 
        #region varibale declartion
        public static string tfsserver;
        public string testos;
        public string testbrowser;
        public string testtitle;
        public string testid;
        public string testcasefile;
        public string loggerfilename;
        public string Url;
        public string debug;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            try
            {
                #region cleanup
                // Close the Logger files if any
                Logger.Dispose();
                #endregion
                #region getconfigsetting
                testbrowser = ConfigSettings.getConfigvalue("Test Browser");
                testos = ConfigSettings.getConfigvalue("Test OS");
                testcasefile = ConfigSettings.getConfigvalue("testcasefile");
                loggerfilename = ConfigSettings.getConfigvalue("logfilename");
                debug = ConfigSettings.getConfigvalue("DEBUG");
                Url = ConfigSettings.getConfigvalue("URL");
                String browser = ConfigSettings.getConfigvalue("Currentbrowser");
                #endregion
                // Intialize the Logger
                Logger.InitializeLogger(loggerfilename, debug);
                ExceptionLogger.InitializeExLogger();

                Logger.LogInfo("=======================================");
                Logger.LogInfo("The value of the browser is " + browser);

                switch (browser)
                {
                    case "IE70":
                        Microsoft.KAF.Agent.Browser.BrowserAgentFactory.BrowserType = BrowserType.IE70;
                        break;
                    case "IE80":
                        Microsoft.KAF.Agent.Browser.BrowserAgentFactory.BrowserType = BrowserType.IE80;
                        break;
                    case "Firefox20":
                        Microsoft.KAF.Agent.Browser.BrowserAgentFactory.BrowserType = BrowserType.Firefox20;
                        break;
                    default:
                        break;
                }
                BrowserAgentFactory.BrowserInstance = "Default";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
                ExceptionLogger.LogExceptionInfo(ex, "There is problem in accessing the config file and /or any of the required value is missing field(s) does not exists");
            }
        }
        /// <summary>
        /// Run the Driver Script
        /// </summary>
        [TestProperty("JobId", "1")]
        [TestMethod]
        public void Test()
        {
            // Enables the SilverLight
            KafConfiguration.EnableSilverlightDriver = true;
            Logger.LogInfo("Starting browser...");
            BrowserAgentFactory.BrowserAgent.StartWebBrowser(Url, true, true);
            //Wait to load the page
            CommonLib.Wait(50000);
            // BrowserAgentFactory.BrowserAgent.AttachTo("AirWatch.UI");

            // Retrive the testcase form the excel sheet
            Testcase ts = new Testcase();
            List<string> testcases = ts.GetTestcases(testcasefile);
            // Make it null intenionally so that 
            // testcases = null;
            
            //if (Loggertype.equals("Excel")) 
            //{
            //TestResult.LogInit();
            // }
            TestResult.LogInit();
            if (testcases != null)
            {
                string testcaseid = "";
                string title="";
                bool testcaseresult = true;
                Logger.LogInfo("Starting Testcase excution");
                foreach (string testcase in testcases)
                {
                    //testcaseid = "";
                    //testcaseresult = true;
                    bool stepresult = false;
                    string testcaseend = "^^^^\n";

                    if (testcase.Equals(testcaseend)) // testcase ends
                    {
                        
                        Logger.LogInfo("Test case ends " + testcaseid);
                        Logger.LogInfo(testcaseid + "   " + testcaseresult.ToString());
                        TestResult.WriteLog(testcaseid, testcaseresult.ToString(), "");
                        string result;
                        if (testcaseresult == true) {
                            result = "Pass";
                        }
                        else{
                            result = "Fail";
                        }
                        if(title.Equals("Accept Disclaimer")){
                        }
                        else{
                        TFSTestResult.TFSLogResult(title, testcaseid, result, testos, testbrowser);
                        }
                        testcaseresult = true;
                    }
                    else
                    {
                        string[] fields = testcase.Trim().Split('^');
                        if (fields[1].Equals("testcase ends"))
                        {
                            Logger.LogInfo("test case execution completes");
                            break;
                        }
                        //confirms if it step
                        if (fields[1].Contains("Step"))
                        {
                            string stepid = fields[1];
                            string stepname = fields[3];
                            string arguments = fields[4];
                            if (arguments == null || arguments.Length == 0)
                            {
                                arguments = "";
                            }
                            Logger.LogInfo("Executing step ...");
                            Logger.LogInfo("Execute(" + stepid + "," + stepname.Trim() + "," + arguments.Trim() + ")");
                            stepresult = Execute(stepid, stepname.Trim(), arguments.Trim());

                            if (stepresult) //If step Pass
                            {
                                //testcaseresult = testcaseresult & stepresult;
                                Logger.LogInfo(stepid + " " + stepname.Trim() + "Step is " + " Passed");
                            }
                            else //step fails
                            {
                                if (testcaseresult == false)
                                {
                                }
                                else {
                                    testcaseresult = testcaseresult & stepresult;
                                } 
                                Logger.LogInfo(stepid + " " + stepname.Trim() + "Step is " + "Failed");
                            }
                        }
                        else //if (!fields[0].Equals("") && !fields[1].Equals("^") && !fields[2].Equals("^")) //testcase
                        {

                            testcaseid = fields[0];
                            title = fields[1];
                            Logger.LogInfo("Test case started for Testcase id " + testcaseid);
                        }
                    }
                }
                TestResult.LogClose();
            }
            else // If There is no test cases available in the sheet
            {
                Logger.LogInfo("There is no testcass available for the testing");
            }
        }
        /// <summary>
        /// Executes the test cases using the reflection
        /// step name reprents the function in functionallib
        /// </summary>
        /// <param name="stepid"></param>
        /// <param name="stepname"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public bool Execute(string stepid, string stepname, string arguments)
        {
            // if (stepname.Length == 0 ) return false;
            bool success = false;
            // steps argumnets
            object[] args;
            if (string.IsNullOrEmpty(arguments))
            {
                args = null;
            }
            else
            {
                args = new object[arguments.Length];
                args = arguments.Split(',');
            }

            object retval;
            object classInstance = Activator.CreateInstance(typeof(FunctionalLib));
            Type type = classInstance.GetType();
            try
            {
                MethodInfo mi = type.GetMethod(stepname);
                CommonLib.Wait(1000);
                retval = mi.Invoke(classInstance, args);
                success = Convert.ToBoolean(retval.ToString());
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, stepname + " Problem in Invoking method / getting method");
            }
            return success;
        }
        /// <summary>
        /// Perform any necessary cleanup between tests
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            Logger.Dispose();
            ExceptionLogger.Dispose();
            DriverFactory.CleanupDrivers();
        }
    }
}