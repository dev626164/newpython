﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DriverTest
{
    class ConfigSettings
    {
        /// <summary>
        /// Allows to assign and retrive the  config values
        /// </summary>
        /// <param name="inputstr"></param>
        /// <returns></returns>
        public static string getConfigvalue(string inputstr)
        {
            string retval = "";

            Dictionary<string, string> configvalues = new Dictionary<string, string>();
            try
            {
                // Storing the cinfig values
                configvalues.Add("Drive", "D:");
                configvalues.Add("Basedir", "\\Airwatch\\Microsoft.AirWatch\\Main\\Test\\Automation");
                configvalues.Add("URL", "http://airwatchtest.cloudapp.net/");
                //configvalues.Add("URL", "http://myapp:81/");
                configvalues.Add("logfilename", configvalues["Drive"] + configvalues["Basedir"] + "\\Results\\TCLog.txt");
                configvalues.Add("Currentbrowser", "IE70");
                //configvalues.Add("Currentbrowser", "Firefox20");
                //<!-- testcasefile" "D:\\Airwatchautomation\\Scenarios\\scenarios.xlsx"-->
                configvalues.Add("testcasefile", configvalues["Drive"] + configvalues["Basedir"] + "\\Data Tables\\scenarios.xlsx");
                configvalues.Add("TFSSERVER", "tkbgitvstfat08");
                configvalues.Add("Test OS", "Windows Vista");
                configvalues.Add("Test Browser", "IE 7.0");
                configvalues.Add("DEBUG", "ON");
                //Console.WriteLine(configvalues["Drive"]+configvalues["Basedir"]+ "\\Results\\ResultExcel");
                //Console.ReadLine();
                configvalues.Add("excellogfile", configvalues["Drive"]+configvalues["Basedir"]+ "\\Results\\ResultExcel");
                configvalues.Add("projectname", "AirWatch");
                configvalues.Add("ORTYPE", "SL");

                if (inputstr.Equals(null) || inputstr.Equals(""))
                {
                    //ExceptionLogger.LogExceptionInfo(null, "Input value is null or empty");
                }
                else
                {
                    if (configvalues.ContainsKey(inputstr))
                    {
                        retval = configvalues[inputstr];
                    }
                    //retval = ConfigurationSettings.AppSettings[inputstr.Trim()].ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
                ExceptionLogger.LogExceptionInfo(ex, "Cannot retrive values form config file for " + inputstr);
            }
            return retval;
        }
    }
}