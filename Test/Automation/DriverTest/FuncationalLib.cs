﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.KAF.UIElements;
using Microsoft.KAF.Factory;
using Microsoft.KAF.UIDriver;
using Microsoft.KAF.Agent.Browser;
using Microsoft.Internal.WLX.WLXLogger;
using Microsoft.KAF.Agent;
using System.Reflection;


namespace DriverTest
{
    class FunctionalLib
    {
        SLOR slor;
        ASPOR aspor;
        public FunctionalLib()
        {
            // Retrive the value of the OR type form config settings
            string ortype = ConfigSettings.getConfigvalue("ORTYPE");
            if (ortype.Equals("SL"))
            {
                //classInstance = Activator.CreateInstance(typeof(SLOR));
                slor = new SLOR();
                slor.createOR();
            }
            else
            {
                //classInstance = Activator.CreateInstance(typeof(ASPOR));
                aspor = new ASPOR();
                //slor.createOR();
            }
            //Type type = classInstance.GetType();
            //MethodInfo mi = type.GetMethod("createOR");
            //mi.Invoke(classInstance, null);
        }

        public bool VerifyMapMode(string viewtype)
        {
            // Obtain map mode view
            CommonLib.Wait(4000);
            string mapMode = slor.map.GetAttribute("Mode");
            Logger.LogInfo(mapMode);
            if (mapMode.Equals(viewtype))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetAerialView()
        {

            bool success = false;
            try
            {
                if (slor.Home_AerialButton.GetAttribute("IsChecked").Equals("True"))
                {
                    success = true;
                }
                else
                {
                    slor.Home_AerialButton.Click();
                    Logger.LogInfo("View Mode Set to Aerial");
                    success = true;
                }
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Aerial button");
            }
            return success;
        }
        public bool SetRoadView()
        {

            bool success = false;
            try
            { 
                if (slor.Home_RoadButton.GetAttribute("IsChecked").Equals("True")){
                }
                else{
                slor.Home_RoadButton.Click();
                Logger.LogInfo("View Mode Set to Road");
                success = true;
                }
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Road button");
            }
            return success;
        }

        public bool ClickLabel()
        {
            bool success = false;
            try
            {
                int i = 0;
                while (i < 10)
                {
                    string enable = slor.Home_LabelsButton.GetAttribute("IsEnabled");
                    CommonLib.Wait(500);
                    if (enable.Equals("True"))
                    {
                        Logger.LogInfo("Label button is enabled");
                        break;
                    }
                    i++;
                }
                //Logger.LogInfo("Clicked Labels Button");
                slor.Home_LabelsButton.Click();
                Logger.LogInfo("Clicked Labels Button");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Labelbutton");
            }
            return success;
        }

        public bool VerifyLabelChecked(string attrchecked)
        {

            //String islabelchecked = slor.Home_LabelsButton.GetAttribute("CHECKED");
            String islabelchecked = slor.Home_LabelsButton.GetAttribute("IsChecked");
            if (islabelchecked.Equals(attrchecked))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ZoomIn()
        {

            bool success = false;
            try
            {
                //Logger.LogInfo("Clicked Labels Button");
                string zoomlevel = slor.MapControl.GetAttribute("ZoomLevel");
                double level = double.Parse(zoomlevel);

                slor.Home_ZoomInButton.Click(ClickType.SingleClick);

                Logger.LogInfo("Zoom In  has been Clicked at level" + level);
                CommonLib.Wait(20000);
                string zoomlevel1 = slor.MapControl.GetAttribute("ZoomLevel");
                double level1 = double.Parse(zoomlevel1);

                Logger.LogInfo("Zoom In  has been Clicked at level" + level1);
                if (level1 > level)
                {
                    success = true;
                    Logger.LogInfo("Zoom in  at level " + zoomlevel);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking ZoomIn");
            }
            return success;
        }

        public bool ZoomOut()
        {
            bool success = false;
            try
            {
                //Logger.LogInfo("Clicked Labels Button");
                string zoomlevel = slor.MapControl.GetAttribute("ZoomLevel");
                double level = double.Parse(zoomlevel);
                Logger.LogInfo("Zoom out  has been Clicked at level" + level);
                
                slor.Home_ZoomOutButton.Click();

                Logger.LogInfo("Zoom out  has been Clicked");
                CommonLib.Wait(20000);

                string zoomlevel1 = slor.MapControl.GetAttribute("ZoomLevel");
                double level1 = double.Parse(zoomlevel1);

                if (level > level1)
                {
                    success = true;
                } 
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking ZoomOut");
            }
            return success;
        }

        public void ClickSearchButton()
        {

            slor.SearchControl_SearchButton.Click();
        }
        public void setSearchText(string location)
        {
            slor.SearchControl_SearchText.Click();
            slor.SearchControl_SearchText.Text = location;
        }
        public bool SearchUniqueLocation(string location)
        {
            bool success = false;
            try
            {
                string latitute = slor.LatitudeVal.GetAttribute("Text");
                string longitute = slor.LongituteVal.GetAttribute("Text");
                Logger.LogInfo("latitute and longitutes are  " + longitute + " and" + latitute);

                setSearchText(location);
                Logger.LogInfo("Set the value for search..." + location);

                ClickSearchButton();
                CommonLib.Wait(20000);
                string latitutenew = slor.LatitudeVal.GetAttribute("Text");
                string longitutenew = slor.LongituteVal.GetAttribute("Text");


                if (latitute.Equals(latitutenew) && longitute.Equals(longitutenew))
                {

                    Logger.LogInfo("Pushpin add at " + longitutenew + " and" + latitutenew);
                }
                else
                {
                    success = true;
                    Logger.LogInfo("Pushpin add at " + longitutenew + " and" + latitutenew);
                }


            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in search or clicking the button");
            }
            return success;
        }

        public bool SearchNonUniqueLocation(string location)
        {
            Logger.LogInfo("Search " + location);

            bool success = false;
            try
            {
                setSearchText(location);
                Logger.LogInfo("Set the value for search..." + location);

                ClickSearchButton();
                CommonLib.Wait(10000);
                int cnt = slor.SearchResults_ResultsList.Count;
                Logger.LogInfo("count is " + cnt);
                System.Random rand = new Random();
                int index = rand.Next(cnt + 1);
                Logger.LogInfo("Index is " + index);
                slor.SearchResults_ResultsList.SelectedIndex = index-1;
                string option = slor.SearchResults_ResultsList.SelectedItem;
                int startIndex = option.IndexOf("<DisplayName type=\"System.String\">");
                startIndex = startIndex + 34;
                int endindex = option.IndexOf("</DisplayName>");
                string seloption = option.Substring(startIndex, endindex - startIndex);
                slor.SearchResults_ResultsList.Select(seloption.Trim());
                Logger.LogInfo("Clicked on option" + seloption);
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in search results");
            }
            return success;
        }

        public void AttachedTo(string Title)
        {
            //need allthe references
        }

        public bool VerifySuggestLoc()
        {
            bool success = false;
            try
            {
                int cnt = slor.SearchResults_ResultsList.Count;
                if (cnt > 0)
                {
                    success = true;
                    Logger.LogInfo("Multiple options are avilable");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "problem in verify location from list box");
            }
            return success;
        }

        public bool SearchNonExistingLocation(string location)
        {
            bool success = false;
            try
            {

                setSearchText(location);
                Logger.LogInfo("Set the value for search..." + location);

                ClickSearchButton();
                string warnmsg = slor.Search_ResultsWarning.GetAttribute("Text");
                if (warnmsg.Equals("There was more than one match for your location"))
                {
                    Logger.LogInfo("The new search is available = " + success);
                    success = true;
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in verify new search");
            }
            return success;
        }
        public bool AddPushpin()
        {
            bool success = false;
            try
            {
                string latitute = slor.LatitudeVal.GetAttribute("Text");
                string longitute = slor.LongituteVal.GetAttribute("Text");
                Logger.LogInfo("latitute and longitutes are  " + longitute + " and" + latitute);
                System.Random rand = new Random();
                int cor = rand.Next() * 700;
                slor.pushpinStatic.DragAndDrop(200, cor);
                Logger.LogInfo("PushPin has been Draged and dropped");

                //slor.pushpin_delbutton.Click();
                CommonLib.Wait(20000);
                slor.map.Click();
                string latitutenew = slor.LatitudeVal.GetAttribute("Text");
                string longitutenew = slor.LongituteVal.GetAttribute("Text");


                if (latitute.Equals(latitutenew) && longitute.Equals(longitutenew))
                {

                    Logger.LogInfo("Pushpin add at " + longitutenew + " and" + latitutenew);
                }
                else
                {
                    success = true;
                    Logger.LogInfo("Pushpin add at " + longitutenew + " and" + latitutenew);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking DragnDrop");
            }
            return success;
        }
        public void deletepushpin()
        {
        }
        public void DragPushpin()
        {
        }
        public bool CloseSearch()
        {
            bool success = false;
            try
            {
                slor.SearchResults_CloseButton.Click();
                success = true;
                Logger.LogInfo("Close the search flyout");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Closing the search flyout panel");
            }
            return success;
        }

        public bool SetUniqueHomeLocation(string location)
        {
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                    Logger.LogInfo("flyout is visible");
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }

                slor.Homeflyout_changelocation.Click();
                Logger.LogInfo("Clicked on chnage location");
                CommonLib.Wait(4000);
                slor.Homeflyout_Search.Text = location;
                Logger.LogInfo("Location is " + location);
                slor.Homeflyout_SearchButton.Click();

                CommonLib.Wait(10000);
               string homelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
               Logger.LogInfo("Value set to " + homelocation);
               if (homelocation.ToUpper().Contains(location.ToUpper()))
               {
                   Logger.LogInfo("Value set to " + homelocation);
                   success = true;
                    
               }
               return success;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the home location");
            }
            return success;
        }
        public bool SearchNonUniqueHomeLocation(string location){
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                string homelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                Logger.LogInfo("The existing location is " + homelocation);
                
                slor.Homeflyout_changelocation.Click();
                slor.Homeflyout_Search.Click();
                slor.Homeflyout_Search.Text = location;
                slor.Homeflyout_SearchButton.Click();

                       
                // slor.Homeflyout_SearchCancelButton.Click();
                CommonLib.Wait(20000);
                //bool visible = slor.Homeflyout_ResultsList.Visible;
                //Logger.LogInfo("is ResultsList visible " + visible);
                string seloption = "";
                int cnt = slor.Homeflyout_ResultsList.Count;
                
                //if (visible)
                if(cnt > 0)
                {
                    //int cnt = slor.Homeflyout_ResultsList.Count;
                    Logger.LogInfo("count is " + cnt);
                    System.Random rand = new Random();
                    int index = rand.Next(cnt + 1);
                    //Logger.LogInfo("Index is " + index);
                    slor.Homeflyout_ResultsList.SelectedIndex = index-1 ;
                    string option = slor.Homeflyout_ResultsList.SelectedItem;
                    int startIndex = option.IndexOf("<DisplayName type=\"System.String\">");
                    startIndex = startIndex + 34;
                    int endindex = option.IndexOf("</DisplayName>");
                    seloption = option.Substring(startIndex, endindex - startIndex);
                    slor.Homeflyout_ResultsList.Select(seloption.Trim());
                }
                else
                {
                    // Check the search message and  "No results found"
                    //Text block does not have automation Ids
                    slor.Homeflyout_SearchCancelButton.Click();
                }
                string newhomelocation = slor.Homeflyout_homelocation.GetAttribute("Text");

                if (newhomelocation.Contains(seloption))
                {
                    Logger.LogInfo("The new location is");
                    success = true;

                }
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the Non unique home location");
            }
            return success;
        }

        public bool NonExistingHomeLocation(string location)
        {
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                string homelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                Logger.LogInfo("Value set to " + homelocation);

                slor.Homeflyout_changelocation.Click();
                slor.Homeflyout_Search.Text = location;
                slor.Homeflyout_SearchButton.Click();

                CommonLib.Wait(10000);

                string newhomelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                if (homelocation.Equals(newhomelocation))
                {
                    Logger.LogInfo("Value of homelocation " + homelocation);
                    success = true;

                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the home location");
            }
            slor.Homeflyout_SearchCancelButton.Click();
            return success;
            
        }
        
        public bool VerifyMiles(string expectedmiles)
        {
            bool success = false;
            try
            {
                string miles = slor.MileInfo.GetAttribute("Text");
                Logger.LogInfo("Miles are " + miles);

                if (miles.Equals(expectedmiles))
                {
                    success = true;
                    Logger.LogInfo("Miles are " + miles + " matches with " + expectedmiles);
                }
                else
                {
                    Logger.LogInfo("Miles are " + miles + " not matches with " + expectedmiles);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "problem in verify location list boxs");
            }
            return success;
        }
        public bool PressKey(string key)
        {
            bool success = false;
            try
            {
                //slor.map.Focus();
                DriverFactory.DriverGroups["Web"].PressKey(key);
                //Keyboard.SendKeys("1001806050");
                Logger.LogInfo(key + " Pressed");
                if (key.Equals("+"))
                {

                    string zoomlevel = slor.MapControl.GetAttribute("ZoomLevel");
                    int level = int.Parse(zoomlevel);

                    DriverFactory.DriverGroups["Web"].PressKey(key);
                    //Keyboard.SendKeys("1001806050");

                    string zoomlevel1 = slor.MapControl.GetAttribute("ZoomLevel");
                    int level1 = int.Parse(zoomlevel);

                    if (level1 > level)
                    {
                        Logger.LogInfo(key + " Pressed" + " with zoom level " + zoomlevel);
                        success = true;
                    }
                }//end key press +
                if (key.Equals("-"))
                {

                    string zoomlevel = slor.MapControl.GetAttribute("ZoomLevel");
                    int level = int.Parse(zoomlevel);

                    DriverFactory.DriverGroups["Web"].PressKey(key);
                    //Keyboard.SendKeys("1001806050");

                    string zoomlevel1 = slor.MapControl.GetAttribute("ZoomLevel");
                    int level1 = int.Parse(zoomlevel);

                    if (level1 < level)
                    {
                        Logger.LogInfo(key + " Pressed");
                        Logger.LogInfo(key + " Pressed" + " with zoom level " + zoomlevel);
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking " + key);
            }
            return success;
        }
        public bool HoverPushpin()
        {
            bool success = false;
            try
            {
                slor.pushpinStatic.Hover();
                string tip = slor.pushpinStatic.GetAttribute("ToolTip");
                if (tip.Equals("Drag & Drop"))
                {
                    Logger.LogInfo("PushPin Hover");
                    success = true;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Label Button");
            }
            return success;
        }


        public bool ClickHome()
        {
            bool success = false;
            try
            {
                Logger.LogInfo("Clicked on Home");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Clicking Home");
            }
            return success;
        }
        public bool ClickSMS()
        {
            bool success = false;
            try
            {
                slor.Homeflyout_SMS.Click();
                Logger.LogInfo("Clicked on SMS");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Clicking SMS");
            }
            return success;
        }
        public bool ClickHelp()
        {
            bool success = false;
            try
            {
                Logger.LogInfo("Clicked on Help");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Help");
            }
            return success;
        }
        public bool ClickAbout()
        {
            bool success = false;
            try
            {
                Logger.LogInfo("Clicked on About");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Clicking About");
            }
            return success;
        }

        public bool ChangeHomeLocation(string location)
        {
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                string homelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                Logger.LogInfo("The existing location is " + homelocation);
                CommonLib.Wait(10000);
                slor.Homeflyout_changelocation.Click();
               
                slor.Homeflyout_Search.Text = location;
                slor.Homeflyout_SearchButton.Click();


                // slor.Homeflyout_SearchCancelButton.Click();
                CommonLib.Wait(10000);
                //bool visible = slor.Homeflyout_ResultsList.Visible;
                //Logger.LogInfo("is ResultsList visible " + visible);
                string seloption = "";
                int cnt = slor.Homeflyout_ResultsList.Count;

                //if (visible)
                if(cnt > 0)
                {
                    //int cnt = slor.Homeflyout_ResultsList.Count;
                    Logger.LogInfo("count is " + cnt);
                    System.Random rand = new Random();
                    int index = rand.Next(cnt + 1);
                    //Logger.LogInfo("Index is " + index);
                    slor.Homeflyout_ResultsList.SelectedIndex = index -1;
                    string option = slor.Homeflyout_ResultsList.SelectedItem;
                    int startIndex = option.IndexOf("<DisplayName type=\"System.String\">");
                    startIndex = startIndex + 34;
                    int endindex = option.IndexOf("</DisplayName>");
                    seloption = option.Substring(startIndex, endindex - startIndex);
                    slor.Homeflyout_ResultsList.Select(seloption.Trim());
                    
                    string newhomelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                    
                    if(newhomelocation.Contains(seloption)){
                        Logger.LogInfo("New location is " + newhomelocation );
                        success = true;
                    }


                }
                else
                {
                    // Check the search message and  "No results found"
                    //Text block does not have automation Ids
                    //slor.Homeflyout_SearchCancelButton.Click();
                    string newhomelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                                        
                    if ( newhomelocation.Contains(homelocation))
                    {
                    Logger.LogInfo("The new location is" + newhomelocation);
                    success = true;

                    }
                }
                

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the home location");
            }
            return success;
        }

        public bool IsHomeLocExists()
        {
            bool success = false;
            try
            {
                Logger.LogInfo("Clicked on About");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in IsHomeLocExists");
            }
            return success;
        }
        public bool SearchChangeLocation()
        {
            bool success = false;
            try
            {
                Logger.LogInfo("Clicked on About");
                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Clicking About");
            }
            return success;
        }
        public bool CancelHomeChangeLocation(string location)
        {
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                

                slor.Homeflyout_changelocation.Click();
                slor.Homeflyout_Search.Text = location;
                slor.Homeflyout_SearchButton.Click();

                CommonLib.Wait(10000);

                  success = true;
           
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the home location");
            }
            slor.Homeflyout_SearchCancelButton.Click();
            return success;
            
        }
        public bool VerifyTabs()
        {
            bool success = false;
            try
            {
                if (slor.Home_flyout.Visible)
                {
                    // Do nothing just want ot check
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                if (slor.Homeflyout_SMS.Visible && slor.Homeflyout_About.Visible && slor.Homeflyout_Home.Visible && slor.Homeflyout_Help.Visible)
                {
                    Logger.LogInfo("Buttons availales on the Home Fly out Panel");
                    success = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in finding button Home, SMS, Help");
            }
            return success;
        }

        public bool VerifyLabelStatus(string status)
        {
            bool success = false;
            try
            {
                string enable = slor.Home_LabelsButton.GetAttribute("IsEnabled");
                Logger.LogInfo("Label is enabled " + enable);
                if (enable.Equals(status))
                {
                    Logger.LogInfo(" is  Label enabled ? " + enable);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in VerifyLabelStatus");
            }
            return success;
        }

        public bool IsHomeFlyoutVisible()
        {

            bool success = false;
            try
            {
                //string enable = slor.Home_LabelsButton.GetAttribute("IsEnabled");

                success = slor.Home_flyout.Visible;
                Logger.LogInfo("Home fly out panel is visible " + success);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in find Homeflyout");
            }
            return success;
        }

        public bool IsPushPinVisible(string prop, string value)
        {
            bool success = false;
            try
            {
                SilverlightUIElement pushpin = UIElementFactory.GetUIElement<SilverlightUIElement>(prop, value);
                if (pushpin.Visible)
                {
                    string val = pushpin.GetAttribute("Name");
                    Logger.LogInfo("Pushpin is visible with the " + value);
                    if (val.Equals("Pushpin"))
                    {
                        success = true;
                        Logger.LogInfo("Value matches with pushpin");
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Closing the search flyout panel");
            }
            return success;
        }

        public bool ClickMapControl()
        {
            bool success = false;
            try
            {
                if (!IsHomeFlyoutVisible())
                {
                    slor.FlyOutPanelToggle.Click();
                }
                CommonLib.Wait(4000);

                slor.MapPanControl.Click();

                CommonLib.Wait(4000);
                if (! slor.Home_flyout.Visible)
                {
                    success = true;
                    Logger.LogInfo("Clicked on the mapcontrol and flyout panel becomes invisible");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Clicking in pancontrol");
            }
            return success;
        }
        public bool VerifyZoomLevel(string level)
        {
            bool success = false;
            try
            {
                string zoomlevel = slor.MapControl.GetAttribute("ZoomLevel");
                if (zoomlevel.Equals(level.Trim()))
                {
                    success = true;
                    Logger.LogInfo("Zoom Level is " + zoomlevel);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Colud not able to retrive the Zoom Level");
            }
            return success;
        }
        private int GetZoomLevel()
        {
            int level = -1;
            try
            {
                string zoomlevel = slor.MapControl.GetAttribute("ZoomLevel");
                level = int.Parse(zoomlevel);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Cloud not able to retrive the Zoom Level");
            }
            return level;
        }

        public bool AcceptDisclaimer()
        {
            bool success = false;
            try
            {
                slor.Home_Disclaimer.Click();
                success = true;

                Logger.LogInfo("Accepted Disclaimer ");

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Colud not able to retrive the Zoom Level");
            }
            return success;
        }
        public bool DeleteAllPushpin()
        {
            bool success = false;
            try
            {
                slor.Mapcontrol_Deletepushpin.Click();
                success = true;
               Logger.LogInfo("All pushpin are deleted...");
                
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Colud not able to retrive the Zoom Level");
            }
            return success;
        }
        public bool setviews(string view)
        {
            bool success = false;
            try
            {
                slor.Mapcontrol_views.Click();
                /* slor.Mapcontrol_views.SelectedIndex = 1;
                //List<SilverlightUIElement> options = slor.Mapcontrol_views.GetComboBoxItems();
                string option = slor.Mapcontrol_views.SelectedItem;
                int startIndex = option.IndexOf("<Identifier type=\"System.String\">");
                startIndex = startIndex + 34;
                int endindex = option.IndexOf("</Identifier>");
                endindex = endindex + 1;
                string seloption = option.Substring(startIndex-1, endindex - startIndex);
                Logger.LogInfo(option);
                Logger.LogInfo(seloption);
                slor.Mapcontrol_views.Select(seloption);
                 * */
                //slor.Mapcontrol_views.Select("Air Stations");
                slor.Mapcontrol_views.Select(view.Trim());
                slor.Mapcontrol_views.Click();
                success = true;

                Logger.LogInfo("View set to " + view);

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Cloud not able to retrive the Zoom Level");
            }
            return success;
        }

        public bool LockFlyout()
        {
            bool success = false;
            try
            {

                slor.Home_LockFlyOutPanel.Click();
                //clicktogle
                //flyout should visible
                success = true;
                Logger.LogInfo("flyout is locked");

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Colud not able to retrive the Zoom Level");
            }
            return success;
        }
        public bool DefaultHomeLocation()
        {
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                string homelocation = slor.Homeflyout_homelocation.GetAttribute("Text");
                Logger.LogInfo("Home Location is " + homelocation);
                if (homelocation.Contains("Copenhagen"))
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the home location");
            }
            return success;
        }
        public bool CancelHomeLocation(string location)
        {
            bool success = false;
            try
            {
                if (slor.Homeflyout_About.Visible)
                {
                }
                else
                {
                    slor.FlyOutPanelToggle.Click();
                }
                slor.Homeflyout_changelocation.Click();
                slor.Homeflyout_Search.Text = location;
                CommonLib.Wait(2000);
                slor.Homeflyout_SearchCancelButton.Click();

                success = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in Setting the home location");
            }
            return success;
        }

        public bool ClickPushpin()
        {
            bool success = false;
            try
            {
                
                slor.map.Click();
                
                slor.pushpin.Click();
                CommonLib.Wait(10000);
                if (slor.pushpin_delbutton.GetAttribute("Visibility").Equals("Visible"))
                {
                    Logger.LogInfo("PushPin Flyout is visible");
                    success = true;
                }
                CommonLib.Wait(2000);
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Label Button");
            }
            return success;
        }
        public bool ClosePushpin()
        {
            bool success = false;
            try
            {
              
               
                CommonLib.Wait(10000);
                if (slor.pushpin_delbutton.GetAttribute("Visibility").Equals("Visible"))
                {
                    Logger.LogInfo("PushPin Flyout is visible");
                    slor.pushpin.Click();
                    Logger.LogInfo("PushPin Flyout is closed");
                    success = true;
                }
                CommonLib.Wait(2000);

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Label Button");
            }
            return success;
        }

        public bool VerifyPushpinRate_Next()
        {
            bool success = false;
            try
            {
                //CommonLib.Wait(20000);
                slor.pushpin_rate.Click();
                if (slor.pushpin_rate_next.GetAttribute("IsEnabled").Equals("False"))
                {
                    Logger.LogInfo("Next button initially is disabled");
                    success = true;
                }
                else
                {
                    Logger.LogInfo("Next button initially is enabled");
                    success = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Label Button");
            }
            return success;
        }

        public bool Click_Air_Rate(string rating)
        {
            bool success = false;
            try
            {
                CommonLib.Wait(5000);
                switch (rating)
                {
                    case "1":
                        slor.air_ratingVBad.Click();
                        break;
                    case "2":
                        slor.air_ratingBad.Click();
                        break;
                    case "3":
                        slor.air_ratingMod.Click();
                        break;
                    case "4":
                        slor.air_ratingGood.Click();
                        break;
                    case "5":
                        slor.air_ratingVGood.Click();
                        break;
                    default:
                        break;
                }
                if (slor.pushpin_rate_next.GetAttribute("IsEnabled").Equals("True"))
                {
                    Logger.LogInfo("Next button is now Enabled");
                    success = true;
                }
                else
                {
                    Logger.LogInfo("Next button is still disabled");
                    success = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking Label Button");
            }
            return success;
        }

        public bool ClickMap() {
            bool success = false;
            try
            {
                CommonLib.Wait(1000);
                slor.map.Click();   
                
                    Logger.LogInfo("Clicked on map");
                    success = true;
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogExceptionInfo(ex, "Problem in clicking on map");
            }
            return success;
        }

    }
}