﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;
using System.Configuration;

namespace DriverTest
{
    public class Logger
    {
        private static StreamWriter streamWriter;
        private static string filename;
        private static string DEBUG="ON";


        public static void InitializeLogger(string logfilename,string debug )
        {
            filename = logfilename;
            DEBUG = debug;
        }
        public static void LogInfo(String message)
        {

            try
            {
                if (DEBUG.Equals("ON"))
                {
                    // Creates or append the Log file
                    streamWriter = new StreamWriter(filename, true);
                    String logmessage = DateTime.Now.ToString() + " " + message;
                    streamWriter.WriteLine(logmessage);
                    streamWriter.Close();
                }
                else { 
                  //Ignore It  
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Exception has occur while writing to the Log file " + ex);
                Dispose();
            }

        }


        public static void Dispose()
        {


            if (streamWriter != null)
            {
                streamWriter.Close();
                streamWriter.Dispose();
                streamWriter = null;
            }

        }
    }
}
