﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;

namespace DriverTest
{
    class ExceptionLogger
    {
        private static StreamWriter streamWriter;
        private static String filename = "";
        // Initialize the logger
        public static void InitializeExLogger()
        {
            filename = ConfigSettings.getConfigvalue("Drive") + ConfigSettings.getConfigvalue("Basedir") + "\\Results\\ExceptionLog.txt";
        }
        /// <summary>
        /// Logs the custom error message and exception messages to the exception Logger
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="customerrmessage"></param>
        public static void LogExceptionInfo(Exception ex, string customerrmessage)
        {
            try
            {
                // Creates or append the Log file
                streamWriter = new StreamWriter(filename, true);
                string errMessage = "The Error is " + customerrmessage + "\n" + ex.Message.ToString();
                streamWriter.WriteLine(System.DateTime.Now+ " " + errMessage);
                streamWriter.Close();
            }
            catch (Exception logex)
            {
                // If any error occurs verify the trace in VS
                System.Diagnostics.Trace.WriteLine("Exception has occur while writing to the Log file " + logex);
                Dispose();
            }
            finally
            {
                Dispose();
            }
        }
        /// <summary>
        ///  Cleanlly closes the streams
        /// </summary>
        public static void Dispose()
        {
            if (streamWriter != null)
            {
                streamWriter.Close();
                streamWriter.Dispose();
                streamWriter = null;
            }
        }
     }
}