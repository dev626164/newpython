﻿using System;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;

namespace DriverTest
{
    class ExcelLog
    {
        Excel.Application app = new Excel.Application();
        Excel.Workbook workbook;
        Excel.Worksheet sheet;
        string filename = "";
        int rowid = 0;
        object lockobj = new object();

        public ExcelLog(string filename)
        {
             StringBuilder sb = new StringBuilder(DateTime.Now.ToString());
            // Replacing the  : and / as they are not allowed in the file name
            sb = sb.Replace(':', '-');
            sb = sb.Replace('/', '-');
            sb = sb.Replace(' ', '_');
            filename = filename + sb.ToString(); // +".xlsx";
            this.filename = filename;
            Logger.LogInfo(filename);
             
        }

        /// <summary>
        /// Creates work sheet 
        /// </summary>
        public void CreateExcel()
        {
            app.DisplayAlerts = false;
            // add workbook 
            workbook = app.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
            // the workbook workbook already has 1 worksheet 
            
            // add worksheet  
            workbook.Worksheets.Add(Type.Missing, workbook.Worksheets[workbook.Worksheets.Count], Type.Missing, Type.Missing);
            // get the first worksheet 
            sheet = (Excel.Worksheet)workbook.Worksheets[1];
            // change worksheet's name 
            sheet.Name = "ResultSheet";
            // Create Header
            //sheet.Cells.CurrentRegion.Areas(
            sheet.Cells[1, 1] = "TestcaseId";
            sheet.Cells[1, 2] = "Result";
            sheet.Cells[1, 3] = "Message";
        }
        /// <summary>
        /// Add the results to the row
        /// </summary>
        /// <param name="stepname"></param>
        /// <param name="result"></param>
        /// <param name="message"></param>
        public void AddResultrow(string stepname, string result, string message)
        {
            Increment();
            //sheet.Cells.CurrentRegion.AutoFit();
            sheet.Cells[rowid, 1] = stepname;
            if (result.Equals("True"))
            {
                //sheet.Cells.CurrentRegion.Font.Color = "Green";
                sheet.Cells[rowid, 2] = "Pass";
            }
            else
            {
                //sheet.Cells.CurrentRegion.Font.Color = "Red";
                sheet.Cells[rowid, 2] = "Fail";
            }

            sheet.Cells[rowid, 3] = message;
        }
        /// <summary>
        /// Close the excel sheet
        /// </summary>
        public void closeSheet()
        {
            // close and save the workbook 
            try
            {
                
                workbook.Close(true, filename, null);
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                //Console.ReadLine();
                ExceptionLogger.LogExceptionInfo(ex, "There is problem in writing to excel file");
                CleanUp();
            }

        }
        /// <summary>
        /// Close the file
        /// </summary>
        public void CleanUp()
        {
            // release the worksheet resources 
            if (sheet != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(sheet);
                sheet = null;
            }
            // release the workbook resources 
            if (workbook != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
                workbook = null;
            }
            // release the excel instance resources 
            if (app != null)
            {
                app.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                app = null;
            }
            // Clean up memory so Excel can shut down. 
            GC.Collect();
            GC.WaitForPendingFinalizers();
            // The GC needs to be called twice in order to get the 
            // Finalizers called - the first time in, it simply makes 
            // a list of what is to be finalized, the second time in, 
            // it actually the finalizing. Only then will the 
            // object do its automatic ReleaseComObject. 
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        /// <summary>
        /// To Increment the rowid and lock it for concurrent users
        /// </summary>
        private void Increment()
        {
            lock (lockobj)
            {
                rowid++;
            }
        }
    }
}