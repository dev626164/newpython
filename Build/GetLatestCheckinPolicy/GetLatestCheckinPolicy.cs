﻿// <copyright file="GetLatestCheckinPolicy.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-05-2009</date>
// <summary>GetLatestCheckinPolicy</summary>

using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.TeamFoundation;
using Microsoft.TeamFoundation.VersionControl;
using Microsoft.TeamFoundation.VersionControl.Client;

/// <summary>
/// Check in policy 
/// </summary>
[Serializable]
public class GetLatestCheckinPolicy : PolicyBase
{
    /// <summary>
    /// Gets the description.
    /// </summary>
    /// <value>The description.</value>
    public override string Description
    {
        get { return "Remind users to perform a get latest before a check-in"; } 
    }

    /// <summary>
    /// Gets is a string that is stored with the policy definition on the source
    /// control server. If a user does not have the policy plug-in installed, this string
    /// is displayed.  You can use this to explain to the user how they should 
    /// install the policy plug-in.
    /// </summary>
    /// <value>The installation instructions.</value>
    public override string InstallationInstructions
    {
        get { return "To install this policy, read InstallInstructions.txt."; }
    }

    /// <summary>
    /// Gets string identifies the type of policy. It is displayed in the 
    /// policy list when you add a new policy to a Team Project.
    /// </summary>
    /// <value>The type of policy.</value>
    public override string Type
    {
        get { return "Get Latest"; }
    }

    /// <summary>
    /// Gets string is a description of the type of policy. It is displayed 
    /// when you select the policy in the Add Check-in Policy dialog box.
    /// </summary>
    /// <value>The type description.</value>
    public override string TypeDescription
    {
        get { return "This policy will prompt the user to decide whether or not they should be allowed to check in."; }
    }

    /// <summary>
    /// This method is called by the policy framework when you create 
    /// a new check-in policy or edit an existing check-in policy.
    /// You can use this to display a UI specific to this policy type 
    /// allowing the user to change the parameters of the policy.
    /// </summary>
    /// <param name="args">The arguments.</param>
    /// <returns>A boolean.</returns>
    public override bool Edit(IPolicyEditArgs args)
    {
        // Do not need any custom configuration
        return true;
    }

    /// <summary>
    /// This method performs the actual policy evaluation. 
    /// It is called by the policy framework at various points in time
    /// when policy should be evaluated. In this example, the method 
    /// is invoked when various asyc events occur that may have 
    /// invalidated the current list of failures.
    /// </summary>
    /// <returns>The policy failure message</returns>
    public override PolicyFailure[] Evaluate()
    {
       return PendingCheckin.PendingChanges.Workspace.Get(VersionSpec.Latest, GetOptions.Preview).NumOperations == 0 ? new PolicyFailure[] { } : new PolicyFailure[] { new PolicyFailure("Please perform a Get Latest before checking in.", this) };
    }
        
    /// <summary>
    /// This method is called if the user double-clicks on 
    /// a policy failure in the UI. In this case a message telling the user 
    /// to supply some comments is displayed.
    /// </summary>
    /// <param name="failure">The failure.</param>
    public override void Activate(PolicyFailure failure)
    {
        MessageBox.Show("Please perform a get latest before checking in.", "How to fix your policy failure");
    }

    /// <summary>
    /// This method is called if the user presses F1 when a policy failure 
    /// is active in the UI. In this example, a message box is displayed
    /// </summary>
    /// <param name="failure">The failure.</param>
    public override void DisplayHelp(PolicyFailure failure)
    {
        MessageBox.Show("This policy helps you to remember to get latest before checking in.", "Prompt Policy Help");
    }
}