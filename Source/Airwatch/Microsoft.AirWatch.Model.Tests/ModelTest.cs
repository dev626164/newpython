﻿// <copyright file="ModelTest.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>30-04-2009</date>
// <summary>Unit tests for the AirWatch Model</summary>
namespace Microsoft.AirWatch.Model.Tests
{
    using System;
    using System.Linq;
    using Microsoft.AirWatch.Model;
    using Microsoft.VisualStudio.TestTools.UnitTesting;    

    /// <summary>
    /// Unit tests for the AirWatch Model 
    /// </summary>
    [TestClass]
    public class ModelTests
    {
        /// <summary>
        /// The Model interface for AirWatch
        /// </summary>
        private IAirWatchModel model;

        /// <summary>
        /// The context the test is run in
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the ModelTests class
        /// </summary>
        public ModelTests()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// You can use the following additional attributes as you write your tests:
        ////
        //// Use ClassInitialize to run code before running the first test in the class
        //// [ClassInitialize()]
        //// public static void MyClassInitialize(TestContext testContext) { }
        ////
        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }
        ////
        #endregion        

        /// <summary>
        /// Tests the Mock Model Layer with get specific locations
        /// </summary>
        [TestMethod]
        public void LogOn()
        {
            this.model = new AirWatchModel();

            this.model.UserLoaded += new EventHandler<Microsoft.AirWatch.Model.UserLoadedEventArgs>(
                (sender, args) =>
                {
                    Assert.IsNotNull(args.User, "null came back");

                    Console.WriteLine("UserId {0} : MobileNumber {1} : IsRegistered {2}", args.User.UserId, args.User.MobileNumber, args.User.IsRegistered);
                });

            this.model.LogOn("00000000000000000000000000000000");
        }

        /// <summary>
        /// Tests the GetToken Method.
        /// </summary>
        [TestMethod]
        public void GetTokenTest()
        {
            this.model = new AirWatchModelMock();

            this.model.TokenRetrieved += new EventHandler<TokenRetrievedEventArgs>(
                (sender, args) =>
                {
                    Assert.IsFalse(String.IsNullOrEmpty(args.BingToken));
                });

            this.model.GetToken();
        }

        /// <summary>
        /// Tests the GetSearchResults Method.
        /// </summary>
        [TestMethod]
        public void GetSearchResultsTest()
        {
            this.model = new AirWatchModelMock();

            this.model.SearchResultsRetrieved += new EventHandler<SearchResultsEventArgs>(
                (sender, args) =>
                {
                    Assert.IsTrue(args.Results.Count() == 2);
                });

            this.model.GetSearchResults("testing", "test");
        }
    }
}
