//-----------------------------------------------------------------------
// <copyright file="GlobalSuppresion.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>30/07/09</date>
// <summary>Global Suppresions</summary>
//----------------------------------------------------------------------- 

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Microsoft.Samples.ServiceHosting.StorageClient.StorageHttpConstants")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1016:MarkAssembliesWithAssemblyVersion")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1017:MarkAssembliesWithComVisible")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Portability", "CA1903:UseOnlyApiFromTargetedFramework", MessageId = "System.Data.Services.Client, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.BlobContainer.#DeleteBlobIfNotModified(Microsoft.Samples.ServiceHosting.StorageClient.BlobProperties,System.Boolean&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.BlobContainerRest.#GetBlockList(System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Scope = "type", Target = "Microsoft.Samples.ServiceHosting.StorageClient.BlobContents")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.BlobProperties.#Metadata")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1820:TestForEmptyStringsUsingStringLength", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.Message.#SetContentFromBase64String(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix", Scope = "type", Target = "Microsoft.Samples.ServiceHosting.StorageClient.MessageQueue")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "0#", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.MessageQueue.#CreateQueue(System.Boolean&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.MessageQueue.#GetProperties()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1003:UseGenericEventHandlerInstances", Scope = "type", Target = "Microsoft.Samples.ServiceHosting.StorageClient.MessageReceivedEventHandler")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.QueueProperties.#Metadata")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.QueueStorageRest+ListQueueResult.#Urls")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Backoff", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.RetryPolicies.#RetryExponentialN(System.Int32,System.TimeSpan)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Backoff", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.RetryPolicies.#RetryExponentialN(System.Int32,System.TimeSpan,System.TimeSpan,System.TimeSpan)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Backoff", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.RetryPolicies.#StandardMaxBackoff")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Backoff", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.RetryPolicies.#StandardMinBackoff")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.StorageAccountInfo.#GetAccountInfoFromConfiguration(System.String,System.String,System.String,System.String,System.Boolean)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Md", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.StorageErrorCodeStrings.#InvalidMd5")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Md", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.StorageErrorCodeStrings.#Md5Mismatch")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Json", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableErrorCodeStrings.#JsonFormatNotSupported")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "Microsoft.Samples.ServiceHosting.StorageClient.ContextRef", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorage.#Attach(System.Data.Services.Client.DataServiceContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorage.#GetDataServiceContext()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1405:ComVisibleTypeBaseTypesShouldBeComVisible", Scope = "type", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorageDataServiceContext")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorageEntity.#.ctor(System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorageHelpers.#EvaluateException(System.Exception,System.Net.HttpStatusCode&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorageHelpers.#EvaluateException(System.Exception,System.Net.HttpStatusCode&,Microsoft.Samples.ServiceHosting.StorageClient.StorageExtendedErrorInformation&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.Utilities.#TranslateExtendedError(Microsoft.Samples.ServiceHosting.StorageClient.StorageExtendedErrorInformation,System.Net.HttpStatusCode,System.String,System.Exception)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#", Scope = "member", Target = "Microsoft.Samples.ServiceHosting.StorageClient.TableStorageHelpers.#EvaluateException(System.Exception,System.Net.HttpStatusCode&,Microsoft.Samples.ServiceHosting.StorageClient.StorageExtendedErrorInformation&)")]
