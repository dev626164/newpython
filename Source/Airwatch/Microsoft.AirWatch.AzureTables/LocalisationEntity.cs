﻿// <copyright file="LocalisationEntity.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>LocalisationEntity 
// </summary>
namespace Microsoft.AirWatch.AzureTables
{
    using System;
    using System.Globalization;
    using Microsoft.WindowsAzure.StorageClient;
    
    /// <summary>
    /// Localisation table entity
    /// </summary>
    public class LocalisationEntity : TableServiceEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalisationEntity"/> class.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="airReplyString">The air reply string.</param>
        /// <param name="waterReplyString">The water reply string.</param>
        /// <param name="airName">Name of the air.</param>
        /// <param name="waterName">Name of the water.</param>
        /// <param name="incorrectRequestString">The incorrect request string.</param>
        /// <param name="airUnknownLocationErrorString">The air unknown location error string.</param>
        /// <param name="waterUnknownLocationErrorString">The water unknown location error string.</param>
        /// <param name="noMeasurementsForLocationString">The no measurements for location string.</param>
        [CLSCompliantAttribute(true)]
        public LocalisationEntity(string locale, string countryCode, string airReplyString, string waterReplyString, string airName, string waterName, string incorrectRequestString, string airUnknownLocationErrorString, string waterUnknownLocationErrorString, string noMeasurementsForLocationString)
        {
            PartitionKey = "loc";
            RowKey = locale;
            this.CountryCode = countryCode;
            this.AirReplyString = airReplyString;
            this.WaterReplyString = waterReplyString;
            this.AirName = airName;
            this.WaterName = waterName;
            this.IncorrectRequestString = incorrectRequestString;
            this.AirUnknownLocationErrorString = airUnknownLocationErrorString;
            this.WaterUnknownLocationErrorString = waterUnknownLocationErrorString;
            this.NoMeasurementsForLocationString = noMeasurementsForLocationString;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalisationEntity"/> class.
        /// </summary>
        public LocalisationEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the air reply string.
        /// </summary>
        /// <value>The air reply string.</value>
        public string AirReplyString { get; set; }

        /// <summary>
        /// Gets or sets the water reply string.
        /// </summary>
        /// <value>The water reply string.</value>
        public string WaterReplyString { get; set; }

        /// <summary>
        /// Gets or sets the name of the air request.
        /// </summary>
        /// <value>The name of the air.</value>
        public string AirName { get; set; }

        /// <summary>
        /// Gets or sets the name of the water request.
        /// </summary>
        /// <value>The name of the water.</value>
        public string WaterName { get; set; }

        /// <summary>
        /// Gets or sets the string format which will explain that the request text message is incorrect
        /// </summary>
        /// <value>The incorrect request string.</value>
        public string IncorrectRequestString { get; set; }

        /// <summary>
        /// Gets or sets the air error string.
        /// </summary>
        /// <value>The air error string.</value>
        public string AirUnknownLocationErrorString { get; set; }

        /// <summary>
        /// Gets or sets the water error string.
        /// </summary>
        /// <value>The water error string.</value>
        public string WaterUnknownLocationErrorString { get; set; }

        /// <summary>
        /// Gets or sets the no measurements for location string.
        /// </summary>
        /// <value>The no measurements for location string.</value>
        public string NoMeasurementsForLocationString { get; set; }
    }
}
