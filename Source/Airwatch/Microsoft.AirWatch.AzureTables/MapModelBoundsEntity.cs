﻿//-----------------------------------------------------------------------
// <copyright file="MapModelBoundsEntity.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>2009-09-02</date>
// <summary>Map Model Bounds Entity Definition.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AzureTables
{
    #region Using

    using System;
    using System.Globalization;
    using Microsoft.WindowsAzure.StorageClient;

    #endregion

    /// <summary>
    /// Map Model Bounnds Entity Definition.
    /// </summary>
    [CLSCompliant(false)]
    public class MapModelBoundsEntity : TableServiceEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapModelBoundsEntity"/> class.
        /// </summary>
        /// <param name="partitionKey">The partition key.</param>
        /// <param name="rowKey">The row key.</param>
        /// <param name="minimumLatitude">The minimum latitude.</param>
        /// <param name="minimumLongitude">The minimum longitude.</param>
        /// <param name="maximumLatitude">The maximum latitude.</param>
        /// <param name="maximumLongitude">The maximum longitude.</param>
        public MapModelBoundsEntity(string partitionKey, string rowKey, double minimumLatitude, double minimumLongitude, double maximumLatitude, double maximumLongitude)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;

            this.MinimumLatitude = minimumLatitude;
            this.MinimumLongitude = minimumLongitude;
            this.MaximumLatitude = maximumLatitude;
            this.MaximumLongitude = maximumLongitude;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MapModelBoundsEntity"/> class.
        /// </summary>
        public MapModelBoundsEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the minimum latitude.
        /// </summary>
        /// <value>The minimum latitude.</value>
        public double MinimumLatitude { get; set; }

        /// <summary>
        /// Gets or sets the minimum longitude.
        /// </summary>
        /// <value>The minimum longitude.</value>
        public double MinimumLongitude { get; set; }

        /// <summary>
        /// Gets or sets the maximum latitude.
        /// </summary>
        /// <value>The maximum latitude.</value>
        public double MaximumLatitude { get; set; }

        /// <summary>
        /// Gets or sets the maximum longitude.
        /// </summary>
        /// <value>The maximum longitude.</value>
        public double MaximumLongitude { get; set; }
    }
}
