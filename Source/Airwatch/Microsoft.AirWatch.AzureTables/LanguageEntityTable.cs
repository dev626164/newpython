﻿//-----------------------------------------------------------------------
// <copyright file="LanguageEntityTable.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>10/07/09</date>
// <summary>Language Entity Table</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AzureTables
{
    using System.Data.Services.Client;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure;
    
    /// <summary>
    /// Language Entity table
    /// </summary>
    public class LanguageEntityTable : TableServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageEntityTable"/> class.
        /// </summary>
        public LanguageEntityTable(string baseAddress, StorageCredentials credentials)
            : base(baseAddress, credentials)
        {
        }

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <value>The languages.</value>
        public DataServiceQuery<LanguageEntity> Languages
        {
            get
            {
                return CreateQuery<LanguageEntity>("Languages");
            }
        }        
    }
}
