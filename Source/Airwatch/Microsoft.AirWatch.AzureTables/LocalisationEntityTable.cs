﻿// <copyright file="LocalisationEntityTable.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>LocalisationEntityTable 
// </summary>
namespace Microsoft.AirWatch.AzureTables
{
    using System.Data.Services.Client;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure;
    
    /// <summary>
    /// Localisation table
    /// </summary>
    public class LocalisationEntityTable : TableServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalisationEntityTable"/> class.
        /// </summary>
        public LocalisationEntityTable(string baseAddress, StorageCredentials credentials)
            : base(baseAddress, credentials)
        {
        }

        /// <summary>
        /// Gets the localisations.
        /// </summary>
        /// <value>The localisations.</value>
        public DataServiceQuery<LocalisationEntity> Localisations
        {
            get
            {
                return CreateQuery<LocalisationEntity>("Localisations");
            }
        }
    }
}
