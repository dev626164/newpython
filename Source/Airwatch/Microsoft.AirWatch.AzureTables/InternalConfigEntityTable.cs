﻿// <copyright file="InternalConfigEntityTable.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>InternalConfigEntityTable 
// </summary>
namespace Microsoft.AirWatch.AzureTables
{
    using System.Data.Services.Client;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure;

    /// <summary>
    /// Internal config table
    /// </summary>
    public class InternalConfigEntityTable : TableServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalConfigEntityTable"/> class.
        /// </summary>
        public InternalConfigEntityTable(string baseAddress, StorageCredentials credentials)
            : base(baseAddress, credentials)
        {
        }

        /// <summary>
        /// Gets the internal configuration.
        /// </summary>
        /// <value>The internal configuration.</value>
        public DataServiceQuery<InternalConfigEntity> InternalConfiguration
        {
            get
            {
                return CreateQuery<InternalConfigEntity>("InternalConfiguration");
            }
        }
    }
}
