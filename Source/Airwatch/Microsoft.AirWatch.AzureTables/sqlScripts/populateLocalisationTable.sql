﻿INSERT INTO [MicrosoftAirWatchCloudService].[dbo].[Localisations]
           ([AirReplyString]
           ,[WaterReplyString]
           ,[Timestamp]
           ,[PartitionKey]
           ,[CountryCode]
           ,[RowKey]
           ,[AirName]
           ,[WaterName]
           ,[IncorrectRequestString]
           ,[AirUnknownLocationErrorString]
           ,[WaterUnknownLocationErrorString]
           ,[NoMeasurementsForLocationString])
     VALUES
           ('The air quality index for {0} is {1}',
           'The water quality index for {0} is {1}',
           null,
           'loc',
           '44',
           'en-GB',
           'air',
           'water',
           'ERROR – please use the following format: AIR/WATER Reading, UK etc...',
           'ERROR – The location received has not been recognised. Please use the following format: AIR Reading, UK etc...',
           'ERROR – The location received has not been recognised. Please use the following format: WATER Reading, UK etc...',
           'ERROR - There is no data for this location. Please note that Eye on Earth currently only provides data for the 32 EEA member countries')
GO

INSERT INTO [MicrosoftAirWatchCloudService].[dbo].[Localisations]
           ([AirReplyString]
           ,[WaterReplyString]
           ,[Timestamp]
           ,[PartitionKey]
           ,[CountryCode]
           ,[RowKey]
           ,[AirName]
           ,[WaterName]           
           ,[IncorrectRequestString]
           ,[AirUnknownLocationErrorString]
           ,[WaterUnknownLocationErrorString]
           ,[NoMeasurementsForLocationString])
     VALUES
           ('Der Luftqualitatsindex fur {0} ist {1}',
           'Der Wasserqualitatsindex fur {0} ist {1}',
           null,
           'loc',
           '49',
           'de-DE',
           'luft',
           'wasser',
           'STÖRUNG - verwenden Sie bitte dieses Format: LUFT/WASSER Wiesbaden, Hessen',
           'STÖRUNG - Die Position wurde nicht erkannt. Verwenden Sie bitte dieses Format: LUFT Wiesbaden, Hessen',
           'STÖRUNG - Die Position wurde nicht erkannt. Verwenden Sie bitte dieses Format: WASSER Wiesbaden, Hessen',
           'STÖRUNG - Es gibt keine Daten für diesen Standort. Bitte beachten Sie, dass Eye on Earth derzeit nur Daten für den 32 EEA Länder')
GO


INSERT INTO [MicrosoftAirWatchCloudService].[dbo].[Localisations]
           ([AirReplyString]
           ,[WaterReplyString]
           ,[Timestamp]
           ,[PartitionKey]
           ,[CountryCode]
           ,[RowKey]
           ,[AirName]
           ,[WaterName]           
           ,[IncorrectRequestString]
           ,[AirUnknownLocationErrorString]
           ,[WaterUnknownLocationErrorString]
           ,[NoMeasurementsForLocationString])
     VALUES
           ('Jakosc powietrza dla {0} wynosi {1}',
           'Jakosc wody dla {0} wynosi {1} ',
           null,
           'loc',
           '48',
           'pl-PL',
           'powietrze',
           'woda',
           'BLAD - prosze uzyc nastepujacego formatu: POWIETRZE/WODA Wroclaw, Dolnoslaskie',
           'BLAD - Lokalizacja nie zostala rozpoznana. prosze uzyc nastepujacego formatu: POWIETRZE Wroclaw, Dolnoslaskie',
           'BLAD - Lokalizacja nie zostala rozpoznana. prosze uzyc nastepujacego formatu: WODA Wroclaw, Dolnoslaskie',
           'BLAD - Brak danych dla tej lokalizacji. Prosze pamietac, ze obecnie Eye on Earth dostarcza danych jedynie dla 32 panstw czlonkowskich EEA')
GO

INSERT INTO [MicrosoftAirWatchCloudService].[dbo].[InternalConfiguration]
           ([BingToken]
           ,[Timestamp]
           ,[PartitionKey]
           ,[RowKey])
     VALUES
           ('',
           null,
           'InternalConfig',
           'configuration1')
GO
