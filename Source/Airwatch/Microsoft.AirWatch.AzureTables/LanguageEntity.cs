﻿//-----------------------------------------------------------------------
// <copyright file="LanguageEntity.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>09/07/09</date>
// <summary>Language Entity</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AzureTables
{
    using System;
    using System.Globalization;
    using Microsoft.WindowsAzure.StorageClient;
    
    /// <summary>
    /// Language entity for azure tables
    /// </summary>
    public class LanguageEntity : TableServiceEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageEntity"/> class.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <param name="stringId">The string ID.</param>
        /// <param name="value">The value.</param>
        [CLSCompliantAttribute(true)]
        public LanguageEntity(string culture, string stringId, string value)
        {
            PartitionKey = culture;
            RowKey = stringId;
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageEntity"/> class.
        /// </summary>
        public LanguageEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }
    }
}
