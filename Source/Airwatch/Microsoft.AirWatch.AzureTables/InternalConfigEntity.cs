﻿// <copyright file="InternalConfigEntity.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>InternalConfigEntity 
// </summary>
namespace Microsoft.AirWatch.AzureTables
{
    using System;
    using System.Globalization;
    using Microsoft.WindowsAzure.StorageClient;
    
    /// <summary>
    /// Internal config table entity
    /// </summary>
    [CLSCompliant(false)]
    public class InternalConfigEntity : TableServiceEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalConfigEntity"/> class.
        /// </summary>
        /// <param name="partitionKey">The partition key.</param>
        /// <param name="rowKey">The row key.</param>
        /// <param name="bingToken">The bing token.</param>
        public InternalConfigEntity(string partitionKey, string rowKey, string bingToken)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
            this.BingToken = bingToken;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternalConfigEntity"/> class.
        /// </summary>
        public InternalConfigEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the bing token.
        /// </summary>
        /// <value>The bing token.</value>
        public string BingToken { get; set; }
    }
}
