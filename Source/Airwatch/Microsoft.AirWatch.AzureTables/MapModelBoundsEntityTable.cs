﻿//-----------------------------------------------------------------------
// <copyright file="MapModelBoundsEntityTable.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>2009-09-02</date>
// <summary>Table Entity definition for Map Model data.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AzureTables
{
    #region Using

    using System.Data.Services.Client;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure;

    #endregion

    /// <summary>
    /// Table Entity definition for Map Model data.
    /// </summary>
    public class MapModelBoundsEntityTable : TableServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapModelBoundsEntityTable"/> class.
        /// </summary>
        public MapModelBoundsEntityTable(string baseAddress, StorageCredentials credentials)
            : base(baseAddress, credentials)
        {
        }

        /// <summary>
        /// Gets the map model.
        /// </summary>
        /// <value>The map model.</value>
        public DataServiceQuery<MapModelBoundsEntity> MapModelBounds
        {
            get
            {
                return CreateQuery<MapModelBoundsEntity>("MapModelBounds");
            }
        }
    }
}
