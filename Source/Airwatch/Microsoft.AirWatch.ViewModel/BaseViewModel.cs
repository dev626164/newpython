﻿//-----------------------------------------------------------------------
// <copyright file="BaseViewModel.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>18-06-2009</date>
// <summary>Base class for all ViewModels</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.ViewModel
{
    using System.ComponentModel;

    /// <summary>
    /// Base class for all ViewModels
    /// </summary>
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        } 
    }
}
