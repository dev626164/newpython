﻿//-----------------------------------------------------------------------
// <copyright file="ClientServices.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>18-08-2009</date>
// <summary>Client Services singleton object container.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.ViewModel
{
    /// <summary>
    /// Client Services singleton object container.
    /// </summary>
    public class ClientServices
    {
        /// <summary>
        /// Backing field for <see cref="Instance" /> property.
        /// </summary>
        private static ClientServices instance;

        /// <summary>
        /// Backing property for FlyoutViewModel property.
        /// </summary>
        private FlyOutViewModel flyoutViewModel;

        /// <summary>
        /// Backing property for MainViewModel property.
        /// </summary>
        private MainViewModel mainViewModel;

        /// <summary>
        /// Backing property for MapViewModel property.
        /// </summary>
        private MapViewModel mapViewModel;

        /// <summary>
        /// Backing property for SearchViewModel property.
        /// </summary>
        private SearchViewModel searchViewModel;

        /// <summary>
        /// Prevents a default instance of the <see cref="ClientServices"/> class from being created.
        /// </summary>
        private ClientServices()
        {
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static ClientServices Instance
        {
            get
            {
                if (ClientServices.instance == null)
                {
                    ClientServices.instance = new ClientServices();
                }

                return ClientServices.instance;
            }
        }

        /// <summary>
        /// Gets the flyout view model.
        /// </summary>
        /// <value>The flyout view model.</value>
        public FlyOutViewModel FlyOutViewModel
        {
            get
            {
                if (this.flyoutViewModel == null)
                {
                    this.flyoutViewModel = new FlyOutViewModel();
                }

                return this.flyoutViewModel;
            }
        }

        /// <summary>
        /// Gets the main view model.
        /// </summary>
        /// <value>The main view model.</value>
        public MainViewModel MainViewModel
        {
            get
            {
                if (this.mainViewModel == null)
                {
                    this.mainViewModel = new MainViewModel();
                }

                return this.mainViewModel;
            }
        }

        /// <summary>
        /// Gets the map view model.
        /// </summary>
        /// <value>The map view model.</value>
        public MapViewModel MapViewModel
        {
            get
            {
                if (this.mapViewModel == null)
                {
                    this.mapViewModel = new MapViewModel();
                }

                return this.mapViewModel;
            }
        }

        /// <summary>
        /// Gets the search view model.
        /// </summary>
        /// <value>The search view model.</value>
        public SearchViewModel SearchViewModel
        {
            get
            {
                if (this.searchViewModel == null)
                {
                    this.searchViewModel = new SearchViewModel();
                }

                return this.searchViewModel;
            }
        }
    }
}
