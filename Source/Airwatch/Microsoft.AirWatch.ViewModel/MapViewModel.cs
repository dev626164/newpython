﻿// <copyright file="MapViewModel.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>12-06-2009</date>
// <summary>ViewModel for map</summary>
namespace Microsoft.AirWatch.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Model;
    using Microsoft.AirWatch.Model.Data;
    using Microsoft.AirWatch.UICommon;
    using Microsoft.Maps.MapControl;
    using Microsoft.Maps.MapControl.Core;
    using AirwatchData = Microsoft.AirWatch.Core.Data;
    using Map = Microsoft.Maps.MapControl;

    /// <summary>
    /// View model for map
    /// </summary>
    public class MapViewModel : BaseViewModel
    {
        /// <summary>
        /// bing maps token
        /// </summary>
        private static string bingToken;

        /// <summary>
        /// Number of pushpin ids allocated
        /// </summary>
        private int pushpinIdCount;

        /// <summary>
        /// Current view map is displaying
        /// </summary>
        private MapMode mapMode;

        /// <summary>
        /// Zoom level of the map
        /// </summary>
        private double mapZoomLevel;

        /// <summary>
        /// Minimum zoom level for the map
        /// </summary>
        private double mapZoomMin;

        /// <summary>
        /// Maximum zoom level for the map
        /// </summary>
        private double mapZoomMax;

        /// <summary>
        /// Backing field for draggablePushPinVisibility property
        /// </summary>
        private bool dragPushpinVisible;

        /// <summary>
        /// Backing field for StationLayerVisible property
        /// </summary>
        private bool stationLayerVisible;

        /// <summary>
        /// Backing field for StationsVisible property
        /// </summary>
        private bool stationsVisible;

        /// <summary>
        /// Backing field for WaterStationsVisible property
        /// </summary>
        private bool waterStationsVisible;

        /// <summary>
        /// Backing field for AirStationsVisible property
        /// </summary>
        private bool airStationsVisible;

        /// <summary>
        /// Backing field for LightMapLayerVisible property
        /// </summary>
        private bool lightMapLayerVisible;

        /// <summary>
        /// Backing field for airModelKeyVisible property
        /// </summary>
        private bool airModelKeyVisible;

        /// <summary>
        /// Backing field for userFeedbackKeyVisible property
        /// </summary>
        private bool userFeedbackKeyVisible;

        /// <summary>
        /// Backing field for userFeedbackUnavailable property
        /// </summary>
        private bool userFeedbackUnavailable;

        /// <summary>
        /// Backing field for stationKeyVisible property
        /// </summary>
        private bool stationKeyVisible;

        /// <summary>
        /// For tracking pushpin Ids
        /// </summary>
        private Dictionary<int, AirwatchData.Pushpin> pushpinDictionary = new Dictionary<int, AirwatchData.Pushpin>();

        /// <summary>
        /// For tracking station Ids
        /// </summary>
        private Dictionary<int, AirwatchData.Station> stationDictionary = new Dictionary<int, AirwatchData.Station>();

        /// <summary>
        /// Backing field for PushpinCollection property
        /// </summary>
        private ObservableCollection<AirwatchData.Pushpin> pushpinCollection = new ObservableCollection<AirwatchData.Pushpin>();
        
        /// <summary>
        /// Backing field for StationCollection property
        /// </summary>
        private ObservableCollection<AirwatchData.Station> stationCollection = new ObservableCollection<AirwatchData.Station>();
        
        /// <summary>
        /// Backing field for TileLayerCollection property
        /// </summary>
        private ObservableCollection<TileLayer> tileLayerCollection = new ObservableCollection<TileLayer>();

        /// <summary>
        /// Collection of user ratings so we don't allow duplicate ratings
        /// </summary>
        private ObservableCollection<Rating> userRatingsCollection = new ObservableCollection<Rating>();        

        /// <summary>
        /// Initializes a new instance of the MapViewModel class
        /// </summary>
        public MapViewModel()
        {
            this.MapMode = new Map.AerialMode();
            AirWatchModel.Instance.PushpinLoaded += new EventHandler<PushpinLoadedEventArgs>(this.PushpinLoaded);
            AirWatchModel.Instance.AddressResultsRetrieved += new EventHandler<AddressResultsEventArgs>(this.AddressResultsRetrieved);
            AirWatchModel.Instance.StationAreaLoaded += new EventHandler<StationAreaLoadedEventArgs>(this.StationAreaLoaded);
            AirWatchModel.Instance.StationLoaded += new EventHandler<StationLoadedEventArgs>(this.StationLoaded);
            AirWatchModel.Instance.IsolatedStoragePushpinsRetrieved += new EventHandler<IsolatedStoragePushpinEventArgs>(this.IsolatedStoragePushpinsRetrieved);
            AirWatchModel.Instance.IsolatedStorageRatingsRetrieved += new EventHandler<IsolatedStorageRatingEventArgs>(this.IsolatedStorageRatingsRetrieved);
            AirWatchModel.Instance.IsolatedStorageProbed += new EventHandler<ProbeIsolatedStorageEventArgs>(this.Instance_IsolatedStorageProbed);
            AirWatchModel.Instance.GetTileLayersCompleted += new EventHandler<GetTileLayersCompletedEventArgs>(this.GetTileLayersCompleted);
            AirWatchModel.Instance.MapTokenRetrieved += new EventHandler<TokenRetrievedEventArgs>(this.MapTokenRetrieved);
            AirWatchModel.Instance.PushpinRated += new EventHandler<PushpinLoadedEventArgs>(this.PushpinRated);
            AirWatchModel.Instance.StationRated += new EventHandler<StationLoadedEventArgs>(this.StationRated);

            if (!DesignerProperties.IsInDesignTool)
            {
                AirWatchModel.Instance.GetTileLayers();
            }

            // initialise borders and pushpin flyout positions
            MapHelper.TopMargin = 315;
            MapHelper.RightMargin = 765;
        }

        #region EventHandlers

        /// <summary>
        /// Occurs when map location changes
        /// </summary>
        public event EventHandler MapLocationChanged;

        /// <summary>
        /// Occurs when pins closed.
        /// </summary>
        public event EventHandler ClosePins;

        /// <summary>
        /// Occurs when the user clicks the cancel button
        /// </summary>
        public event EventHandler CancelFlyOutEvent;

        /// <summary>
        /// Occurs when the user clicks rate (return home)
        /// </summary>
        public event EventHandler DoneRatingEvent;

        /// <summary>
        /// Occurs when [tile layer changed].
        /// </summary>
        public event EventHandler<EventArgs> OverlayChanged;

        /// <summary>
        /// Occurs when [stations updated].
        /// </summary>
        public event EventHandler<EventArgs> StationsUpdated;

        /// <summary>
        /// Occurs when [pushpins updated].
        /// </summary>
        public event EventHandler<EventArgs> PushpinsUpdated;

        /// <summary>
        /// Occurs when [add pushpin cancelled].
        /// </summary>
        public event EventHandler<EventArgs> AddPushpinCanceled;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the zoom level for turning on stations.
        /// </summary>
        /// <value>The map station zoom level.</value>
        public int MapStationZoomLevel { get; set; }

        /// <summary>
        /// Gets or sets the zoom level where stations will be shown regardless of count
        /// </summary>
        /// <value>The map station max zoom level.</value>
        public int MapStationMaxZoomLevel { get; set; }

        /// <summary>
        /// Gets or sets the buffer (in lat/long) around the map view that we will request for stations
        /// </summary>
        /// <value>The map station buffer.</value>
        public double MapStationBuffer { get; set; }

        /// <summary>
        /// Gets or sets the max stations to show at a time.
        /// </summary>
        /// <value>The map station max stations to show.</value>
        public int MapStationMaxStationsToShow { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [isolated storage unavailable].
        /// </summary>
        /// <value>
        /// <c>true</c> if [isolated storage unavailable]; otherwise, <c>false</c>.
        /// </value>
        public bool IsolatedStorageUnavailable { get; set; }
         
        /// <summary>
        /// Gets or sets the map mode.
        /// </summary>
        /// <value>The map mode.</value>
        public MapMode MapMode
        {
            get
            {
                return this.mapMode;
            }

            set
            {
                if (this.mapMode != value)
                {
                    this.mapMode = value;
                    this.NotifyPropertyChanged("MapMode");
                }
            }
        }

        /// <summary>
        /// Gets or sets the map zoom level.
        /// </summary>
        /// <value>The map zoom level.</value>
        public double MapZoomLevel
        {
            get
            {
                return this.mapZoomLevel;
            }

            set
            {
                if (this.mapZoomLevel != value)
                {
                    ////if (value >= this.mapZoomMin && value <= this.mapZoomMax)
                    ////{
                    ////    this.mapZoomLevel = value;
                    ////}
                    this.mapZoomLevel = value;

                    this.NotifyPropertyChanged("MapZoomLevel");
                }
            }
        }

        /// <summary>
        /// Gets or sets the map zoom min.
        /// </summary>
        /// <value>The map zoom min.</value>
        public double MapZoomMin
        {
            get
            {
                return this.mapZoomMin;
            }

            set
            {
                if (this.mapZoomMin != value)
                {
                    this.mapZoomMin = value;
                    this.NotifyPropertyChanged("MapZoomMin");
                }
            }
        }

        /// <summary>
        /// Gets or sets the map view max.
        /// </summary>
        /// <value>The map view max.</value>
        public double MapZoomMax
        {
            get
            {
                return this.mapZoomMax;
            }

            set
            {
                if (this.mapZoomMax != value)
                {
                    this.mapZoomMax = value;
                    this.NotifyPropertyChanged("MapZoomMax");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the draggable push pin is visilbe
        /// </summary>
        public bool DragPushpinVisible
        {
            get
            {
                return this.dragPushpinVisible;
            }

            set
            {
                this.dragPushpinVisible = value;
                this.NotifyPropertyChanged("DragPushpinVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [station layer visible].
        /// </summary>
        /// <value><c>true</c> if [station layer visible]; otherwise, <c>false</c>.</value>
        public bool StationLayerVisible
        {
            get
            {
                return this.stationLayerVisible;
            }

            set
            {
                this.stationLayerVisible = value;
                this.NotifyPropertyChanged("StationLayerVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [stations visible].
        /// </summary>
        /// <value><c>true</c> if [stations visible]; otherwise, <c>false</c>.</value>
        public bool StationsVisible
        {
            get
            {
                return this.stationsVisible;
            }

            set
            {
                this.stationsVisible = value;
                this.NotifyPropertyChanged("StationsVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [air stations visible].
        /// </summary>
        /// <value><c>true</c> if [air stations visible]; otherwise, <c>false</c>.</value>
        public bool AirStationsVisible
        {
            get
            {
                return this.airStationsVisible;
            }

            set
            {
                this.airStationsVisible = value;
                this.NotifyPropertyChanged("AirStationsVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [water stations visible].
        /// </summary>
        /// <value>
        /// <c>true</c> if [water stations visible]; otherwise, <c>false</c>.
        /// </value>
        public bool WaterStationsVisible
        {
            get
            {
                return this.waterStationsVisible;
            }

            set
            {
                this.waterStationsVisible = value;
                this.NotifyPropertyChanged("WaterStationsVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [light map layer visible].
        /// </summary>
        /// <value>
        /// <c>true</c> if [light map layer visible]; otherwise, <c>false</c>.
        /// </value>
        public bool LightMapLayerVisible
        {
            get
            {
                return this.lightMapLayerVisible;
            }

            set
            {
                this.lightMapLayerVisible = value;
                this.NotifyPropertyChanged("LightMapLayerVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [air model key visible].
        /// </summary>
        /// <value>
        /// <c>true</c> if [air model key visible]; otherwise, <c>false</c>.
        /// </value>
        public bool AirModelKeyVisible
        {
            get
            {
                return this.airModelKeyVisible;
            }

            set
            {
                this.airModelKeyVisible = value;
                this.NotifyPropertyChanged("AirModelKeyVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [user feedback unavailable].
        /// </summary>
        /// <value>
        /// <c>true</c> if [user feedback unavailable]; otherwise, <c>false</c>.
        /// </value>
        public bool UserFeedbackUnavailable
        {
            get
            {
                return this.userFeedbackUnavailable;
            }

            set
            {
                this.userFeedbackUnavailable = value;
                this.NotifyPropertyChanged("UserFeedbackUnavailable");
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether [userfeedback key visible].
        /// </summary>
        /// <value>
        /// <c>true</c> if [userfeedback key visible]; otherwise, <c>false</c>.
        /// </value>
        public bool UserFeedbackKeyVisible
        {
            get
            {
                return this.userFeedbackKeyVisible;
            }

            set
            {
                this.userFeedbackKeyVisible = value;
                this.NotifyPropertyChanged("UserFeedbackKeyVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [station key visible].
        /// </summary>
        /// <value>
        /// <c>true</c> if [station key visible]; otherwise, <c>false</c>.
        /// </value>
        public bool StationKeyVisible
        {
            get
            {
                return this.stationKeyVisible;
            }

            set
            {
                this.stationKeyVisible = value;
                this.NotifyPropertyChanged("StationKeyVisible");
            }
        }

        /// <summary>
        /// Gets the pushpin collection.
        /// </summary>
        /// <value>The pushpin collection.</value>
        public ObservableCollection<AirwatchData.Pushpin> PushpinCollection
        {
            get
            {
                return this.pushpinCollection;
            }
        }

        /// <summary>
        /// Gets the tile layer collection.
        /// </summary>
        /// <value>The tile layer collection.</value>
        public ObservableCollection<TileLayer> TileLayerCollection
        {
            get
            {
                return this.tileLayerCollection;
            }
        }

        /// <summary>
        /// Gets the station collection.
        /// </summary>
        /// <value>The station collection.</value>
        public ObservableCollection<AirwatchData.Station> StationCollection
        {
            get
            {
                return this.stationCollection;
            }
        }

        /// <summary>
        /// Gets or sets the map culture.
        /// </summary>
        /// <value>The map culture.</value>
        public string MapCulture
        {
            get
            {
                return AirWatchModel.Instance.CurrentCulture;
            }

            set
            {
                AirWatchModel.Instance.CurrentCulture = value;
                this.NotifyPropertyChanged("MapCulture");
            }
        }

        /// <summary>
        /// Gets or sets the rating area.
        /// </summary>
        /// <value>The rating area.</value>
        public decimal RatingArea { get; set; }

        #endregion

        #region Stations

        /// <summary>
        /// Gets the station by code.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        public static void GetStationById(int stationId)
        {
            AirWatchModel.Instance.GetStationById(stationId);
        }

        /// <summary>
        /// Gets the pushpin.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="pushpinId">The pushpin id.</param>
        public static void GetPushpin(double latitude, double longitude, int pushpinId)
        {
            AirWatchModel.Instance.GetPushpin((decimal)latitude, (decimal)longitude, pushpinId);
        }

        /// <summary>
        /// Gets the station descriptions for map boundaries.
        /// </summary>
        /// <param name="topLeft">The top left.</param>
        /// <param name="bottomRight">The bottom right.</param>
        public static void GetStationDescriptionsForMapBoundaries(Location topLeft, Location bottomRight)
        {
            double north = topLeft.Latitude;
            double south = bottomRight.Latitude;
            double west = topLeft.Longitude;
            double east = bottomRight.Longitude;

            if (ClientServices.Instance.MapViewModel.MapZoomLevel >= ClientServices.Instance.MapViewModel.MapStationMaxZoomLevel)
            {
                AirWatchModel.Instance.GetStationDescriptionsForMapBoundaries(north, east, south, west, 0);
            }
            else
            {
                AirWatchModel.Instance.GetStationDescriptionsForMapBoundaries(north, east, south, west, ClientServices.Instance.MapViewModel.MapStationMaxStationsToShow);
            }
        }

        #endregion

        #region Ratings

        /// <summary>
        /// Saves a rating for a location
        /// </summary>
        /// <param name="stationId">The station id.</param>
        /// <param name="latitude">latitude of pin</param>
        /// <param name="longitude">longitude of pin</param>
        /// <param name="rating">rating value</param>
        /// <param name="ratingTarget">air or water</param>
        /// <param name="userAddress">user IP address</param>
        /// <param name="wordQualifiers">describing words</param>
        public void SaveStationRating(int stationId, double latitude, double longitude, int rating, string ratingTarget, string userAddress, bool[] wordQualifiers)
        {
            if (this.DoneRatingEvent != null)
            {
                this.DoneRatingEvent(this, null);
            }

            AirWatchModel.Instance.SetStationRating(stationId, latitude, longitude, rating, ratingTarget, userAddress, wordQualifiers);
        }

        /// <summary>
        /// Saves the pushpin rating.
        /// </summary>
        /// <param name="pushpinId">The pushpin id.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="rating">The rating.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userAddress">The user address.</param>
        /// <param name="wordQualifiers">The word qualifiers.</param>
        public void SavePushpinRating(int pushpinId, double latitude, double longitude, int rating, string ratingTarget, string userAddress, bool[] wordQualifiers)
        {
            if (this.DoneRatingEvent != null)
            {
                this.DoneRatingEvent(this, null);
            }

            AirWatchModel.Instance.SetPushpinRating(pushpinId, latitude, longitude, rating, ratingTarget, userAddress, wordQualifiers);
        }

        /// <summary>
        /// Clears the stations.
        /// </summary>
        public void ClearStations()
        {
            foreach (AirwatchData.Station station in this.StationCollection)
            {
                station.EuropeanCode = null;
            }
        }

        /// <summary>
        /// Refreshes the pushpins.
        /// </summary>
        public void RefreshPushpins()
        {
            foreach (AirwatchData.Pushpin pushpin in this.PushpinCollection)
            {
                AirWatchModel.Instance.GetPushpin(pushpin.Location.Latitude, pushpin.Location.Longitude, pushpin.PushpinId); 
            }
        }

        /// <summary>
        /// Determines whether the specified location is rateable.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="pinType">Type of the pin.</param>
        /// <returns>
        /// <c>true</c> if the specified location is rateable; otherwise, <c>false</c>.
        /// </returns>
        public bool CanRate(decimal longitude, decimal latitude, string pinType)
        {
            foreach (Rating rating in this.userRatingsCollection)
            {
                if (pinType.Equals("station"))
                {
                    if (rating.Longitude == longitude && rating.Latitude == latitude && rating.Date.AddDays(6).CompareTo(DateTime.Today) > 0)
                    {
                        return false;
                    }
                }
                else if (pinType.Equals("pushpin") && rating.PinType != null && rating.PinType.Equals("Air"))
                {
                    if ((rating.Longitude >= (longitude - this.RatingArea)) && (rating.Longitude <= (longitude + this.RatingArea)) && (rating.Latitude >= (latitude - this.RatingArea)) && (rating.Latitude <= (latitude + this.RatingArea)) && rating.Date.AddDays(6).CompareTo(DateTime.Today) > 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion

        #region Map Control

        /// <summary>
        /// Loads the defaults.
        /// </summary>
        public void LoadDefaults()
        {
            this.MapZoomMax = 17.0;
            this.MapZoomMin = 3.0;
            this.MapZoomLevel = 5.0;
            this.MoveMap(50, 25);
            this.MapStationBuffer = 100;
            this.MapStationZoomLevel = 8;
            this.MapStationMaxZoomLevel = 11;
            this.MapStationMaxStationsToShow = 30;
            this.StationLayerVisible = false;
            this.RatingArea = 0.03M;
            this.StationKeyVisible = true;
            this.StationsVisible = true;
            this.TileLayerCollection[1].Visible = true;
            this.TileLayerCollection[2].Visible = true;
            this.WaterStationsVisible = true;
            this.AirStationsVisible = true;
            this.TileLayerChanged();
            
            AirWatchModel.Instance.GetIsolatedStoragePushpins();
            AirWatchModel.Instance.GetIsolatedStorageRatings();
        }

        /// <summary>
        /// Moves the map.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        public void MoveMap(double latitude, double longitude)
        {
            if (this.MapLocationChanged != null)
            {
                this.MapLocationChanged.Invoke(this, new MapLocationEventArgs(new Map.Location(latitude, longitude)));
            }
        }

        #endregion

        #region Pushpins

        /// <summary>
        /// Adds a pushpin where a new one was dropped on the map control.
        /// </summary>
        /// <param name="pinX">pixel x value</param>
        /// <param name="pinY">pixel y value</param>
        public void DropPushpin(double pinX, double pinY)
        {
            Location loc = MapHelper.ControlPixelXYToLatLong(pinX, pinY);
            this.AddPushpin(loc.Latitude, loc.Longitude);
        }

        /// <summary>
        /// Adds the pushpin cancel.
        /// </summary>
        public void AddPushpinCancel()
        {
            if (this.AddPushpinCanceled != null)
            {
                this.AddPushpinCanceled.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Adds the pushpin.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        public void AddPushpin(double latitude, double longitude)
        {            
            bool duplicateFound = false;

            foreach (AirwatchData.Pushpin dataPushpin in this.pushpinCollection)
            {
                if (dataPushpin != null && dataPushpin.Location.Longitude == (decimal)longitude && dataPushpin.Location.Latitude == (decimal)latitude)
                {
                    duplicateFound = true;
                    break;
                }
            }

            if (duplicateFound == false)
            {
                AirwatchData.Pushpin newPushpin = new AirwatchData.Pushpin();
                newPushpin.PushpinId = -1;
                newPushpin.Location = new AirwatchData.Location();
                newPushpin.Location.Latitude = (decimal)latitude;
                newPushpin.Location.Longitude = (decimal)longitude;
                newPushpin.CanRate = true;

                int id = this.pushpinIdCount++;
                this.pushpinDictionary.Add(id, newPushpin);
                this.pushpinCollection.Add(newPushpin);
                this.NotifyPropertyChanged("PushpinCollection");

                if (this.PushpinsUpdated != null)
                {
                    this.PushpinsUpdated.Invoke(this, new EventArgs());
                }

                GetPushpin(latitude, longitude, id);
                AirWatchModel.Instance.GetAddress(longitude, latitude, bingToken, AirWatchModel.Instance.CurrentCulture, id, "pushpin");

                if (!AirWatchModel.Instance.ProbeIsolatedStorage(this.PushpinCollection.Count * 30000, false) && !this.IsolatedStorageUnavailable)
                {
                    AirWatchModel.Instance.SaveIsolatedStoragePushpins(this.PushpinCollection);                    
                }
            }
        }

        /// <summary>
        /// Saves the pushpin collection to isolated storeage.
        /// </summary>
        public void SavePushpinCollection()
        {
            if (this.pushpinDictionary != null)
            {
                AirWatchModel.Instance.SaveIsolatedStoragePushpins(this.pushpinCollection);
            }
        }

        /// <summary>
        /// Saves the rating collection.
        /// </summary>
        public void SaveRatingCollection()
        {
            if (this.userRatingsCollection != null)
            {
                AirWatchModel.Instance.SaveIsolatedStorageRatings(this.userRatingsCollection);
            }
        }

        /// <summary>
        /// Deletes the pushpin.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        public void DeletePushpin(object dataContext)
        {
            AirwatchData.Pushpin dataPushpinToDelete = dataContext as AirwatchData.Pushpin;

            if (this.PushpinCollection.Contains(dataPushpinToDelete))
            {
                this.PushpinCollection.Remove(dataPushpinToDelete);
            }
        }

        /// <summary>
        /// Deletes all the pushpins.
        /// </summary>
        public void DeleteAllPushpins()
        {
            this.pushpinDictionary.Clear();
            this.pushpinCollection.Clear();
            this.NotifyPropertyChanged("PushpinCollection");
        }

        #endregion

        #region Misc

        /// <summary>
        /// Fires the event to cancel a flyout process
        /// </summary>
        public void CancelFlyOut()
        {
            if (this.CancelFlyOutEvent != null)
            {
                this.CancelFlyOutEvent(this, null);
            }
        }

        /// <summary>
        /// Closes all Pins.
        /// </summary>
        public void CloseAllPins()
        {
            if (this.ClosePins != null)
            {
                this.ClosePins(this, new EventArgs());
            }
        }

        /// <summary>
        /// Tiles the layer changed.
        /// </summary>
        public void TileLayerChanged()
        {
            if (this.OverlayChanged != null)
            {
                this.OverlayChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// Copies the data pushpin.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        private static void CopyDataPushpin(AirwatchData.Pushpin source, AirwatchData.Pushpin target)
        {
            target.Address = source.Address;
            target.AverageRating = source.AverageRating;
            target.CanRate = source.CanRate;
            target.CentreLocation = source.CentreLocation;
            target.FriendlyName = source.FriendlyName;
            target.Location = source.Location;
            target.PushpinId = source.PushpinId;
        }

        /// <summary>
        /// Copies the data station.
        /// </summary>;
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        private static void CopyDataStation(AirwatchData.Station source, AirwatchData.Station target)
        {
            target.AverageRating = source.AverageRating;
            target.CanRate = source.CanRate;
            target.CountryName = source.CountryName;
            target.EuropeanCode = source.EuropeanCode;
            ////target.FriendlyAddress = source.FriendlyAddress;
            target.Location = source.Location;
            target.Name = source.Name;
            target.QualityIndex = source.QualityIndex;
            target.StationId = source.StationId;
            target.StationMeasurement = source.StationMeasurement;
            target.StationPurpose = source.StationPurpose;
            target.StreetType = source.StreetType;
            target.Timestamp = source.Timestamp;
            target.TypeOfStation = source.TypeOfStation;
        }

        /// <summary>
        /// Retrieved map token
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.TokenRetrievedEventArgs"/> instance containing the event data.</param>
        private void MapTokenRetrieved(object sender, TokenRetrievedEventArgs e)
        {
            bingToken = e.BingToken;
        }

        #endregion

        #region Station/Pushpin/Ratings Callbacks

        /// <summary>
        /// Handles the PushpinLoaded event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.PushpinLoadedEventArgs"/> instance containing the event data.</param>
        private void PushpinLoaded(object sender, PushpinLoadedEventArgs e)
        {
            if (this.pushpinDictionary.ContainsKey(e.Pushpin.PushpinId))
            {
                e.Pushpin.CanRate = this.CanRate(e.Pushpin.Location.Longitude, e.Pushpin.Location.Latitude, "pushpin");
                int index = this.pushpinCollection.IndexOf(this.pushpinDictionary[e.Pushpin.PushpinId]);
                if (index >= 0 && this.PushpinCollection[index] != null)
                {
                    e.Pushpin.Address = this.pushpinCollection[index].Address;
                    CopyDataPushpin(e.Pushpin, this.pushpinCollection[index]);
                    this.NotifyPropertyChanged("PushpinCollection");
                }
            }
        }

        /// <summary>
        /// Addresses the results retrieved.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.AddressResultsEventArgs"/> instance containing the event data.</param>
        private void AddressResultsRetrieved(object sender, AddressResultsEventArgs e)
        {
            if (e != null && e.Result != null && e.Result.Results.Count > 0)
            {
                if (e.PinType.Equals("pushpin"))
                {
                    if (this.pushpinDictionary.ContainsKey(e.PinId))
                    {
                        int index = this.pushpinCollection.IndexOf(this.pushpinDictionary[e.PinId]);
                        if (index >= 0)
                        {
                            this.pushpinCollection[index].Address = e.Result.Results[0].Address.FormattedAddress;
                        }
                    }
                }
                else if (e.PinType.Equals("station"))
                {
                    if (this.stationDictionary.ContainsKey(e.PinId))
                    {
                        this.stationCollection.SingleOrDefault(p => p.StationId == e.PinId).FriendlyAddress = e.Result.Results[0].Address.FormattedAddress;
                    }
                }
            }
        }

        /// <summary>
        /// Handles station area loaded event
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.StationAreaLoadedEventArgs"/> instance containing the event data.</param>
        private void StationAreaLoaded(object sender, StationAreaLoadedEventArgs e)
        {
            foreach (AirwatchData.Station station in this.stationDictionary.Values)
            {
                if ((double)station.Location.Latitude > MapHelper.TopLeftBoundary.Latitude ||
                    (double)station.Location.Longitude < MapHelper.TopLeftBoundary.Longitude ||
                    (double)station.Location.Latitude < MapHelper.BottomRightBoundary.Latitude ||
                    (double)station.Location.Longitude > MapHelper.BottomRightBoundary.Longitude ||
                    (station.StationPurpose == "Water" && this.WaterStationsVisible == false) ||
                    (station.StationPurpose == "Air" && this.AirStationsVisible == false))
                {
                    this.stationCollection.Remove(station);
                }
            }

            this.stationDictionary.Clear();

            foreach (AirwatchData.Station station in this.stationCollection)
            {
                this.stationDictionary.Add(station.StationId, station);
            }

            int count = e.GetStations().Count;

            if (count <= this.MapStationMaxStationsToShow || this.MapZoomLevel > this.MapStationMaxZoomLevel)
            {
                foreach (StationContainer stationContainer in e.GetStations())
                {
                    if (this.stationDictionary.ContainsKey(stationContainer.StationId))
                    {
                        continue;
                    }

                    if (stationContainer.StationPurpose == "Water" && this.WaterStationsVisible == false)
                    {
                        continue;
                    }
                    
                    if (stationContainer.StationPurpose == "Air" && this.AirStationsVisible == false)
                    {
                        continue;
                    }

                    AirwatchData.Station station = new AirwatchData.Station();
                    station.StationId = stationContainer.StationId;
                    station.StationPurpose = stationContainer.StationPurpose;
                    station.QualityIndex = stationContainer.QualityIndex;
                    station.Location = new AirwatchData.Location { Latitude = stationContainer.Latitude, Longitude = stationContainer.Longitude };
                    station.CanRate = true;

                    this.stationCollection.Add(station);
                    this.stationDictionary.Add(station.StationId, station);
                }

                this.LightMapLayerVisible = false;
                this.StationLayerVisible = true;
            }
            else
            {
                this.CloseAllPins();
                this.LightMapLayerVisible = true;
                this.StationLayerVisible = false;
            }
            
            this.NotifyPropertyChanged("StationCollection");

            if (this.StationsUpdated != null)
            {
                this.StationsUpdated.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Fired event when station is loaded
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.StationLoadedEventArgs"/> instance containing the event data.</param>
        private void StationLoaded(object sender, StationLoadedEventArgs e)
        {
            if (this.stationDictionary.ContainsKey(e.Station.StationId))
            {
                e.Station.CanRate = this.CanRate(e.Station.Location.Longitude, e.Station.Location.Latitude, "station");
                int index = this.stationCollection.IndexOf(this.stationDictionary[e.Station.StationId]);

                if (index >= 0 && this.stationCollection[index] != null)
                {
                    e.Station.FriendlyAddress = this.stationCollection[index].FriendlyAddress;
                    CopyDataStation(e.Station, this.stationCollection[index]);
                }

                if (e.Station.StationPurpose == "Water")
                {
                    AirWatchModel.Instance.GetAddress((double)e.Station.Location.Longitude, (double)e.Station.Location.Latitude, bingToken, AirWatchModel.Instance.CurrentCulture, e.Station.StationId, "station");
                }
            }
        }

        /// <summary>
        /// Handles the IsolatedStoragePushpinsRetrieved event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.IsolatedStoragePushpinEventArgs"/> instance containing the event data.</param>
        private void IsolatedStoragePushpinsRetrieved(object sender, IsolatedStoragePushpinEventArgs e)
        {
            if (e.IsolatedStoragePushpins != null && e.IsolatedStoragePushpins.PushpinCollection != null)
            {
                foreach (AirwatchData.Pushpin p in e.IsolatedStoragePushpins.PushpinCollection)
                {
                    if (p != null)
                    {
                        this.AddPushpin((double)p.Location.Latitude, (double)p.Location.Longitude);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the IsolatedStorageRatingsRetrieved event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.IsolatedStorageRatingEventArgs"/> instance containing the event data.</param>
        private void IsolatedStorageRatingsRetrieved(object sender, IsolatedStorageRatingEventArgs e)
        {
            if (e.IsolatedStorageRatings != null && e.IsolatedStorageRatings.RatingCollection != null)
            {
                this.userRatingsCollection = e.IsolatedStorageRatings.RatingCollection;
            }
        }

        /// <summary>
        /// Stations the rated.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void StationRated(object sender, StationLoadedEventArgs e)
        {
            this.userRatingsCollection.Add(new Rating { Date = DateTime.Now, Latitude = e.Station.Location.Latitude, Longitude = e.Station.Location.Longitude, PinType = e.Station.StationPurpose });

            this.ClearStations();
            AirWatchModel.Instance.GetStationById(e.Station.StationId);
            this.RefreshPushpins();
            ClientServices.Instance.FlyOutViewModel.RefreshHomeLocations();
        }

        /// <summary>
        /// Pushpins the rated.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void PushpinRated(object sender, PushpinLoadedEventArgs e)
        {
            this.userRatingsCollection.Add(new Rating { Date = DateTime.Now, Latitude = e.Pushpin.Location.Latitude, Longitude = e.Pushpin.Location.Longitude, PinType = "Air" });

            AirWatchModel.Instance.GetPushpin(e.Pushpin.Location.Latitude, e.Pushpin.Location.Longitude, e.Pushpin.PushpinId);

            this.ClearStations();
            this.RefreshPushpins();
            ClientServices.Instance.FlyOutViewModel.RefreshHomeLocations();
        }

        /// <summary>
        /// Handles the IsolatedStorageProbed event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.ProbeIsolatedStorageEventArgs"/> instance containing the event data.</param>
        private void Instance_IsolatedStorageProbed(object sender, ProbeIsolatedStorageEventArgs e)
        {
            this.IsolatedStorageUnavailable = !e.Success;
        }

        #endregion

        #region Layers Callbacks

        /// <summary>
        /// Gets the tile layers completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.GetTileLayersCompletedEventArgs"/> instance containing the event data.</param>
        private void GetTileLayersCompleted(object sender, GetTileLayersCompletedEventArgs e)
        {
            if (e != null && e.TileLayers != null)
            {
                this.tileLayerCollection = e.TileLayers;
            }
        }

        #endregion
    }
}
