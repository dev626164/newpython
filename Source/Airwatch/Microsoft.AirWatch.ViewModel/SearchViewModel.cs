﻿//-----------------------------------------------------------------------
// <copyright file="SearchViewModel.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author> Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>19/06/09</date>
// <summary>Search ViewModel</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Text.RegularExpressions;
    using System.Windows;
    using dev.virtualearth.net.webservices.v1.common;
    using Microsoft.AirWatch.Model;
    using BingMaps = Microsoft.Maps.MapControl;

    /// <summary>
    /// Search ViewModel
    /// </summary>
    public class SearchViewModel : BaseViewModel
    {
        /// <summary>
        /// Bing token
        /// </summary>
        private static string bingToken;

        /// <summary>
        /// Results Collection
        /// </summary>
        private ObservableCollection<GeocodeResult> resultsCollection = new ObservableCollection<GeocodeResult>();

        /// <summary>
        /// Visibility of the flyout
        /// </summary>
        private bool resultsFlyOutVisible;

        /// <summary>
        /// Visibility of the error
        /// </summary>
        private bool errorVisible;

        /// <summary>
        /// Visibility of the results
        /// </summary>
        private bool resultsVisible;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchViewModel"/> class.
        /// </summary>
        public SearchViewModel()
        {
           AirWatchModel.Instance.MapTokenRetrieved += new EventHandler<TokenRetrievedEventArgs>(this.TokenRetrieved);
           AirWatchModel.Instance.GetMapToken("127.0.0.1");
        }

        /// <summary>
        /// Event handler to change status of home fly out panel
        /// </summary>
        public event EventHandler FlyOutStatusChangeEvent;

        /// <summary>
        /// Gets or sets a value indicating whether [results fly out visibility].
        /// </summary>
        /// <value>
        /// <c>true</c> if [results fly out visibility]; otherwise, <c>false</c>.
        /// </value>
        public bool ResultsFlyOutVisible
        {
            get 
            { 
                return this.resultsFlyOutVisible; 
            }

            set 
            {
                this.resultsFlyOutVisible = value;
                this.NotifyPropertyChanged("ResultsFlyOutVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [error visibility].
        /// </summary>
        /// <value><c>true</c> if [error visibility]; otherwise, <c>false</c>.</value>
        public bool ErrorVisible
        {
            get 
            { 
                return this.errorVisible; 
            }
            
            set 
            { 
                this.errorVisible = value;
                this.NotifyPropertyChanged("ErrorVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [results visibility].
        /// </summary>
        /// <value><c>true</c> if [results visibility]; otherwise, <c>false</c>.</value>
        public bool ResultsVisible
        {
            get
            {
                return this.resultsVisible;
            }

            set
            {
                this.resultsVisible = value;
                this.NotifyPropertyChanged("ResultsVisible");
            }
        }

        /// <summary>
        /// Gets or sets the results collection.
        /// </summary>
        /// <value>The results collection.</value>
        public ObservableCollection<GeocodeResult> ResultsCollection
        {
            get
            {
                return this.resultsCollection;
            }

            set
            {
                this.resultsCollection = value;
                this.NotifyPropertyChanged("ResultsCollection");
            }
        }

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="query">The query.</param>
        public void GetSearchResults(string query)
        {
            AirWatchModel.Instance.SearchResultsRetrieved += new EventHandler<SearchResultsEventArgs>(this.SearchResultsRetrieved);

            // TODO: Get the culture
            string culture = AirWatchModel.Instance.CurrentCulture;

            if (String.IsNullOrEmpty(bingToken))
            {
                AirWatchModel.Instance.MapTokenRetrieved += new EventHandler<TokenRetrievedEventArgs>((sender, eventArgs) =>
                {
                    bingToken = eventArgs.BingToken;

                    if (!String.IsNullOrEmpty(bingToken) && String.IsNullOrEmpty(query) == false)
                    {
                        AirWatchModel.Instance.GetSearchResults(query, bingToken, culture);
                    }
                });
                AirWatchModel.Instance.GetMapToken("127.0.0.1");
            }
            else if (String.IsNullOrEmpty(query) == false)
            {
                if (Regex.IsMatch(query, @"[a-zA-Z0-9\u00C0-\u0513]+"))
                {
                    AirWatchModel.Instance.GetSearchResults(query, bingToken, culture);
                }
                else
                {
                    this.SearchResultsRetrieved(this, null);
                }
            }
        }

        /// <summary>
        /// Adds the pushpin.
        /// </summary>
        /// <param name="index">The index.</param>
        public void AddPushpin(int index)
        {
            if (index >= 0 && index < this.ResultsCollection.Count)
            {
                GeocodeResult result = this.ResultsCollection[index];
                ClientServices.Instance.MapViewModel.AddPushpin(result.Locations[0].Latitude, result.Locations[0].Longitude);
                ClientServices.Instance.MapViewModel.MoveMap(result.Locations[0].Latitude, result.Locations[0].Longitude);

                BingMaps.Location topLeft = new BingMaps.Location { Longitude = result.BestView.Southwest.Longitude, Latitude = result.BestView.Northeast.Latitude };
                BingMaps.Location bottomRight = new BingMaps.Location { Longitude = result.BestView.Northeast.Longitude, Latitude = result.BestView.Southwest.Latitude };
                ClientServices.Instance.MapViewModel.MapZoomLevel = MapHelper.GetOptimalZoomForArea(topLeft, bottomRight, 1024, 768);

                if (this.FlyOutStatusChangeEvent != null)
                {
                    this.FlyOutStatusChangeEvent(this, null);
                }
            }
        }

        /// <summary>
        /// Gets the token and store in static.
        /// </summary>
        private static void GetTokenAndStoreInStatic()
        {
            AirWatchModel.Instance.GetMapToken("127.0.0.1");
        }

        /// <summary>
        /// Handles the TokenRetrieved event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.TokenRetrievedEventArgs"/> instance containing the event data.</param>
        private void TokenRetrieved(object sender, TokenRetrievedEventArgs e)
        {
            bingToken = e.BingToken;
        }

        /// <summary>
        /// Instances the search results retrieved.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.SearchResultsEventArgs"/> instance containing the event data.</param>
        private void SearchResultsRetrieved(object sender, SearchResultsEventArgs e)
        {
            AirWatchModel.Instance.SearchResultsRetrieved -= new EventHandler<SearchResultsEventArgs>(this.SearchResultsRetrieved);

            if (e == null || e.Results == null || e.Results.Count == 0)
            {
                this.ResultsCollection.Clear();
                this.ResultsFlyOutVisible = true;
                this.ErrorVisible = true;
                this.ResultsVisible = false;
            } 
            else
            {
                this.ErrorVisible = false;
                this.ResultsVisible = true;

                ObservableCollection<GeocodeResult> results = new ObservableCollection<GeocodeResult>();

                foreach (GeocodeResult result in e.Results)
                {
                    results.Add(result);
                }
                
                this.ResultsCollection = results;

                if (e.Results.Count == 1)
                {                
                    this.AddPushpin(0);
                    this.ResultsFlyOutVisible = false;
                }
                else if (e.Results.Count > 1)
                {
                    this.ResultsFlyOutVisible = true;
                }
            }
        }
    }
}
