﻿//-----------------------------------------------------------------------
// <copyright file="FlyOutViewModel.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>17-06-2009</date>
// <summary>View Model to control FlyOut View.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using dev.virtualearth.net.webservices.v1.common;
    using Microsoft.AirWatch.Model;
    using Microsoft.AirWatch.UICommon;
    using AirwatchData = Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// View Model to control FlyOut View.
    /// </summary>
    public class FlyOutViewModel : BaseViewModel
    {
        /// <summary>
        /// Bing token
        /// </summary>
        private static string bingToken;

        /// <summary>
        /// Backing field for the HomePanelVisible property
        /// </summary>
        private bool homePanelVisible;

        /// <summary>
        /// Backing field for the SmsPanelVisible property
        /// </summary>
        private bool smsPanelVisible;

        /// <summary>
        /// Backing field for the AboutPanelVisible property
        /// </summary>
        private bool aboutPanelVisible;

        /// <summary>
        /// Backing field for the HelpPanelVisible property
        /// </summary>
        private bool helpPanelVisible;

        /// <summary>
        /// Backing field for the FlyOutPanelVisible property
        /// </summary>
        private bool flyOutPanelVisible;

        /// <summary>
        /// Backing field for homeName property
        /// </summary>
        private string homeName;

        /// <summary>
        /// Backing field for homePushpin
        /// </summary>
        private AirwatchData.Pushpin homePushpin = new AirwatchData.Pushpin { PushpinId = -1 };

        /// <summary>
        /// Backing field for homeWaterStation
        /// </summary>
        private AirwatchData.Station homeWaterStation = new AirwatchData.Station();

        /// <summary>
        /// Results Collection
        /// </summary>
        private ObservableCollection<GeocodeResult> resultsCollection = new ObservableCollection<GeocodeResult>();

        /// <summary>
        /// Visibility of the search results
        /// </summary>
        private bool resultsVisible;

        /// <summary>
        /// Is Isolated storage available
        /// </summary>
        private bool isolatedStorageUnavailable;

        /// <summary>
        /// Visibility of the search box
        /// </summary>
        private bool searchVisible;

        /// <summary>
        /// Visibility of the error
        /// </summary>
        private bool errorVisible;

        /// <summary>
        /// Initializes a new instance of the <see cref="FlyOutViewModel"/> class.
        /// </summary>
        public FlyOutViewModel()
        {
            AirWatchModel.Instance.MapTokenRetrieved += new EventHandler<TokenRetrievedEventArgs>(this.MapTokenRetrieved);
            AirWatchModel.Instance.HomePushpinLoaded += new EventHandler<PushpinLoadedEventArgs>(this.HomePushpinLoaded);
            AirWatchModel.Instance.HomeWaterStationLoaded += new EventHandler<StationLoadedEventArgs>(this.HomeWaterStationLoaded);
            AirWatchModel.Instance.AddressResultsRetrieved += new EventHandler<AddressResultsEventArgs>(this.AddressResultsRetrieved);
            AirWatchModel.Instance.IsolatedStorageProbed += new EventHandler<ProbeIsolatedStorageEventArgs>(this.Instance_IsolatedStorageProbed);
            AirWatchModel.Instance.GetMapToken("127.0.0.1");
            this.homePanelVisible = true;

            AirWatchModel.Instance.IsolatedStorageUserLoaded += new EventHandler<UserLoadedEventArgs>(this.IsolatedStorageUserLoaded);
        }

        /// <summary>
        /// Event to hide fly out if one result is returned from search
        /// </summary>
        public event EventHandler SingleResultHomeLocationChangeEvent;        

        /// <summary>
        /// Gets or sets a value indicating whether [home panel visible].
        /// </summary>
        /// <value><c>true</c> if [home panel visible]; otherwise, <c>false</c>.</value>
        public bool HomePanelVisible
        {
            get
            {
                return this.homePanelVisible;
            }

            set
            {
                this.homePanelVisible = value;
                this.NotifyPropertyChanged("HomePanelVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [SMS panel visible].
        /// </summary>
        /// <value><c>true</c> if [SMS panel visible]; otherwise, <c>false</c>.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Sms", Justification = "If correct spelling to SMS then an error occurs relating to case")]
        public bool SmsPanelVisible
        {
            get
            {
                return this.smsPanelVisible;
            }

            set
            {
                this.smsPanelVisible = value;
                this.NotifyPropertyChanged("SmsPanelVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [about panel visible].
        /// </summary>
        /// <value><c>true</c> if [about panel visible]; otherwise, <c>false</c>.</value>
        public bool AboutPanelVisible
        {
            get
            {
                return this.aboutPanelVisible;
            }

            set
            {
                this.aboutPanelVisible = value;
                this.NotifyPropertyChanged("AboutPanelVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [help panel visible].
        /// </summary>
        /// <value><c>true</c> if [help panel visible]; otherwise, <c>false</c>.</value>
        public bool HelpPanelVisible
        {
            get
            {
                return this.helpPanelVisible;
            }

            set
            {
                this.helpPanelVisible = value;
                this.NotifyPropertyChanged("HelpPanelVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [fly out panel visible].
        /// </summary>
        /// <value><c>true</c> if [fly out panel visible]; otherwise, <c>false</c>.</value>
        public bool FlyOutPanelVisible
        {
            get
            {
                return this.flyOutPanelVisible;
            }

            set
            {
                this.flyOutPanelVisible = value;
                this.NotifyPropertyChanged("FlyOutPanelVisible");
            }
        }

        /// <summary>
        /// Gets or sets the name of the home.
        /// </summary>
        /// <value>The name of the home.</value>
        public string HomeName
        {
            get
            {
                if (this.homeName != null)
                {
                    this.homeName = this.homeName.ToUpper(CultureInfo.InvariantCulture);
                }

                return this.homeName;
            }

            set
            {
                this.homeName = value;
                this.NotifyPropertyChanged("HomeName");
            }
        }

        /// <summary>
        /// Gets or sets the home pushpin.
        /// </summary>
        /// <value>The home pushpin.</value>
        public AirwatchData.Pushpin HomePushpin
        {
            get
            {
                return this.homePushpin;
            }

            set
            {
                this.homePushpin = value;
                this.NotifyPropertyChanged("HomePushpin");
            }
        }

        /// <summary>
        /// Gets or sets the home water station.
        /// </summary>
        /// <value>The home water station.</value>
        public AirwatchData.Station HomeWaterStation
        {
            get
            {
                return this.homeWaterStation;
            }

            set
            {
                this.homeWaterStation = value;
                this.NotifyPropertyChanged("HomeWaterStation");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [results visibility].
        /// </summary>
        /// <value><c>true</c> if [results visibility]; otherwise, <c>false</c>.</value>
        public bool ResultsVisible
        {
            get
            {
                return this.resultsVisible;
            }

            set
            {
                this.resultsVisible = value;
                this.NotifyPropertyChanged("ResultsVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [error visibility].
        /// </summary>
        /// <value><c>true</c> if [error visibility]; otherwise, <c>false</c>.</value>
        public bool ErrorVisible
        {
            get
            {
                return this.errorVisible;
            }

            set
            {
                this.errorVisible = value;
                this.NotifyPropertyChanged("ErrorVisible");
            }
        }

        /// <summary>
        /// Gets or sets the results collection.
        /// </summary>
        /// <value>The results collection.</value>
        public ObservableCollection<GeocodeResult> ResultsCollection
        {
            get
            {
                return this.resultsCollection;
            }

            set
            {
                this.resultsCollection = value;
                this.NotifyPropertyChanged("ResultsCollection");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [search visible].
        /// </summary>
        /// <value><c>true</c> if [search visible]; otherwise, <c>false</c>.</value>
        public bool SearchVisible
        {
            get
            {
                return this.searchVisible;
            }

            set
            {
                this.searchVisible = value;
                this.NotifyPropertyChanged("SearchVisible");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [isolated storage available].
        /// </summary>
        /// <value>
        /// <c>true</c> if [isolated storage available]; otherwise, <c>false</c>.
        /// </value>
        public bool IsolatedStorageUnavailable
        {
            get
            {
                return this.isolatedStorageUnavailable;
            }

            set
            {
                this.isolatedStorageUnavailable = value;
                this.NotifyPropertyChanged("IsolatedStorageUnavailable");
            }
        }

        /// <summary>
        /// Load the data from isolated storage
        /// </summary>
        public static void LoadIsolatedStorageSettings()
        {
            AirWatchModel.Instance.GetIsolatedStorageEntry();
        }

        /// <summary>
        /// Probes the isolated storage.
        /// </summary>
        public static void ProbeIsolatedStorage()
        {
            AirWatchModel.Instance.ProbeIsolatedStorage(AirWatchModel.MinStorageSize, true);
        }

        #region SetHomeLocation

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="query">The query.</param>
        public void GetSearchResults(string query)
        {
            // add event handler and remove on return so that the search results flyout doesn't open when we search for a home location
            AirWatchModel.Instance.SearchResultsRetrieved += new EventHandler<SearchResultsEventArgs>(this.SearchResultsRetrieved);

            string culture = AirWatchModel.Instance.CurrentCulture;

            if (String.IsNullOrEmpty(bingToken))
            {
                AirWatchModel.Instance.MapTokenRetrieved += new EventHandler<TokenRetrievedEventArgs>((sender, eventArgs) =>
                {
                    bingToken = eventArgs.BingToken;

                    if (!String.IsNullOrEmpty(bingToken) && String.IsNullOrEmpty(query) == false)
                    {
                        AirWatchModel.Instance.GetSearchResults(query, bingToken, culture);
                    }
                });
                AirWatchModel.Instance.GetMapToken("127.0.0.1");
            }
            else if (String.IsNullOrEmpty(query) == false)
            {
                if (Regex.IsMatch(query, @"[a-zA-Z0-9\u00C0-\u0513]+"))
                {
                    AirWatchModel.Instance.GetSearchResults(query, bingToken, culture);
                }
                else
                {
                    this.SearchResultsRetrieved(this, null);
                }
            }
        }

        /// <summary>
        /// Confirms the location.
        /// </summary>
        /// <param name="index">The index.</param>
        public void ConfirmLocation(int index)
        {
            if (index >= 0 && index < this.ResultsCollection.Count)
            {
                GeocodeResult result = this.ResultsCollection[index];
                this.HomeName = result.DisplayName;
                decimal latitude = (decimal)this.ResultsCollection[index].Locations[0].Latitude;
                decimal longitude = (decimal)this.ResultsCollection[index].Locations[0].Longitude;
                this.HomePushpin = new AirwatchData.Pushpin { PushpinId = -1 };
                this.HomeWaterStation = new AirwatchData.Station();
                AirWatchModel.Instance.GetHomePushpin(latitude, longitude);
                AirWatchModel.Instance.GetHomeWaterStation(latitude, longitude);
                AirWatchModel.Instance.SaveIsolatedStorageEntry(longitude, latitude, this.homeName);
                this.AddPushpin(index);
            }

            this.ResultsVisible = false;
        }

        /// <summary>
        /// Refreshes the home locations.
        /// </summary>
        public void RefreshHomeLocations()
        {
            if (this.HomePushpin.Location != null)
            {
                AirWatchModel.Instance.GetHomePushpin(this.HomePushpin.Location.Latitude, this.HomePushpin.Location.Longitude);
            }
            
            if (this.HomeWaterStation.Location != null)
            {
                AirWatchModel.Instance.GetHomeWaterStation(this.HomeWaterStation.Location.Latitude, this.HomeWaterStation.Location.Longitude);
            }
        }

        /// <summary>
        /// Adds the pushpin.
        /// </summary>
        /// <param name="index">The index.</param>
        public void AddPushpin(int index)
        {
            if (index >= 0 && index < this.ResultsCollection.Count)
            {
                GeocodeResult result = this.ResultsCollection[index];
                ClientServices.Instance.MapViewModel.AddPushpin(result.Locations[0].Latitude, result.Locations[0].Longitude);
                ClientServices.Instance.MapViewModel.MoveMap(result.Locations[0].Latitude, result.Locations[0].Longitude);
            }
        }

        #endregion

        #region Isolated Storage callbacks

        /// <summary>
        /// Event fires when isolated storage is loaded
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">UserLoadedEventArgs e</param>
        private void IsolatedStorageUserLoaded(object sender, UserLoadedEventArgs e)
        {
            if (e.User.UserHomePushpin.Pushpin.FriendlyName != null)
            {
                this.HomeName = e.User.UserHomePushpin.Pushpin.FriendlyName;
                AirWatchModel.Instance.GetHomePushpin(e.User.UserHomePushpin.Pushpin.Location.Latitude, e.User.UserHomePushpin.Pushpin.Location.Longitude);
                AirWatchModel.Instance.GetHomeWaterStation(e.User.UserHomePushpin.Pushpin.Location.Latitude, e.User.UserHomePushpin.Pushpin.Location.Longitude);
            }
            else
            {
                this.HomeName = "Copenhagen, Denmark";
                AirWatchModel.Instance.GetHomePushpin((decimal)55.676, (decimal)12.569);
                AirWatchModel.Instance.GetHomeWaterStation((decimal)55.676, (decimal)12.569);
            }
        }

        /// <summary>
        /// Handles the HomePushpinLoaded event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.PushpinLoadedEventArgs"/> instance containing the event data.</param>
        private void HomePushpinLoaded(object sender, PushpinLoadedEventArgs e)
        {            
            this.HomePushpin = e.Pushpin;
            ClientServices.Instance.MapViewModel.AddPushpin((double)e.Pushpin.Location.Latitude, (double)e.Pushpin.Location.Longitude);
        }

        /// <summary>
        /// Handles the HomeWaterStationLoaded event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.UICommon.StationLoadedEventArgs"/> instance containing the event data.</param>
        private void HomeWaterStationLoaded(object sender, StationLoadedEventArgs e)
        {
            if (e.Station != null)
            {
                this.HomeWaterStation = e.Station;
                AirWatchModel.Instance.GetAddress((double)e.Station.Location.Longitude, (double)e.Station.Location.Latitude, bingToken, AirWatchModel.Instance.CurrentCulture, e.Station.StationId, "home");
            }
            else
            {
                this.HomeWaterStation = new AirwatchData.Station { EuropeanCode = "NONE" };
            }
        }

        #endregion

        #region SetHomeLocation callbacks

        /// <summary>
        /// Addresses the results retrieved.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.AddressResultsEventArgs"/> instance containing the event data.</param>
        private void AddressResultsRetrieved(object sender, AddressResultsEventArgs e)
        {
            if (e != null && e.Result != null && e.Result.Results.Count > 0)
            {
                if (e.PinType.Equals("home"))
                {
                    this.HomeWaterStation.FriendlyAddress = e.Result.Results[0].Address.FormattedAddress;
                }
            }
        }

        /// <summary>
        /// Recieved search results
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.SearchResultsEventArgs"/> instance containing the event data.</param>
        private void SearchResultsRetrieved(object sender, SearchResultsEventArgs e)
        {
            AirWatchModel.Instance.SearchResultsRetrieved -= new EventHandler<SearchResultsEventArgs>(this.SearchResultsRetrieved);

            this.ResultsVisible = false;

            if (e == null || e.Results == null || e.Results.Count == 0)
            {
                this.ErrorVisible = true;
            }
            else
            {
                ObservableCollection<GeocodeResult> results = new ObservableCollection<GeocodeResult>();

                foreach (GeocodeResult result in e.Results)
                {
                    results.Add(result);
                }

                this.ResultsCollection = results;

                if (e.Results.Count == 0)
                {
                    this.ErrorVisible = true;
                }
                else if (e.Results.Count == 1)
                {
                    this.HomeName = results[0].DisplayName;
                    decimal latitude = (decimal)this.ResultsCollection[0].Locations[0].Latitude;
                    decimal longitude = (decimal)this.ResultsCollection[0].Locations[0].Longitude;
                    this.HomePushpin = new AirwatchData.Pushpin { PushpinId = -1 };
                    this.HomeWaterStation = new AirwatchData.Station();
                    AirWatchModel.Instance.GetHomePushpin(latitude, longitude); 
                    AirWatchModel.Instance.GetHomeWaterStation(latitude, longitude);
                    AirWatchModel.Instance.SaveIsolatedStorageEntry(longitude, latitude, this.homeName);
                    this.AddPushpin(0);

                    if (this.SingleResultHomeLocationChangeEvent != null)
                    {
                        this.SingleResultHomeLocationChangeEvent(this, null);
                    }

                    this.ErrorVisible = false;
                }
                else if (e.Results.Count > 1)
                {
                    this.ResultsVisible = true;
                    this.ErrorVisible = false;
                }
            }
        }
        #endregion        

        /// <summary>
        /// Handles the IsolatedStorageProbed event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.ProbeIsolatedStorageEventArgs"/> instance containing the event data.</param>
        private void Instance_IsolatedStorageProbed(object sender, ProbeIsolatedStorageEventArgs e)
        {
            this.IsolatedStorageUnavailable = !e.Success;
        }

        /// <summary>
        /// Retrieved map token
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.TokenRetrievedEventArgs"/> instance containing the event data.</param>
        private void MapTokenRetrieved(object sender, TokenRetrievedEventArgs e)
        {
            bingToken = e.BingToken;
        }
    }
}
