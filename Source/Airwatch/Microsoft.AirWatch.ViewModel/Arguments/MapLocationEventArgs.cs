﻿// <copyright file="MapLocationEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>25-06-2009</date>
// <summary>Custom event args to pass location</summary>
namespace Microsoft.AirWatch.ViewModel
{
    using System;
    using Microsoft.Maps.MapControl;

    /// <summary>
    /// Custom event args to pass location
    /// </summary>
    public class MapLocationEventArgs : EventArgs
    {
        /// <summary>
        /// Location object
        /// </summary>
        private Location location;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapLocationEventArgs"/> class.
        /// </summary>
        /// <param name="newLocation">The new location.</param>
        public MapLocationEventArgs(Location newLocation)
        {
            this.Location = newLocation;
        }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        public Location Location
        {
            get { return this.location; }
            set { this.location = value; }
        }
    }
}
