﻿// <copyright file="AddPushpinEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>19-06-2009</date>
// <summary>Custom EventArgs for adding pin to map</summary>

namespace Microsoft.AirWatch.ViewModel
{
    using System;
    using System.Windows.Shapes;
    using Microsoft.Maps.MapControl;

    /// <summary>
    /// Class for custom EventArgs for adding pin
    /// </summary>
    public class AddPushpinEventArgs : EventArgs
    {
        /// <summary>
        /// String name for new location
        /// </summary>
        private Location newLocation;
        
        /// <summary>
        /// Initializes a new instance of the AddPushpinEventArgs class
        /// </summary>
        /// <param name="value">Location value</param>
        public AddPushpinEventArgs(Location value)
        {
            this.NewLocation = value;
        }

        /// <summary>
        /// Gets or sets the new location
        /// </summary>
        public Location NewLocation
        {
            get
            {
                return this.newLocation;
            }

            set
            {
                this.newLocation = value;
            }
        }
    }
}
