﻿//-----------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>18-06-2009</date>
// <summary>View Model to control Main View.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.ViewModel
{
    #region Using

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Browser;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Model;

    #endregion

    /// <summary>
    /// View Model to control Main View.
    /// </summary>
    public class MainViewModel : BaseViewModel
    {
        /// <summary>
        /// Indicates if the language has loaded.
        /// </summary>
        private bool languageLoaded = false;

        /// <summary>
        /// Backing field for FlyOutPanelUnlocked property.
        /// </summary>
        private bool flyOutPanelUnlocked;

        /// <summary>
        /// Is isolated storage available
        /// </summary>
        private bool isolatedStorageUnavailable;

        /// <summary>
        /// Represents whether the flyout has loaded all of its data so we can signal the ui to show.
        /// </summary>
        private bool flyoutWaterStationLoaded;

        /// <summary>
        /// Represents whether the flyout has loaded all of its data so we can signal the ui to show.
        /// </summary>
        private bool flyoutHomePushpinStationLoaded;    
  
        /// <summary>
        /// Backing field for Languages property.
        /// </summary>
        private Collection<LanguageDescription> languages = new Collection<LanguageDescription>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            AirWatchModel.Instance.IsolatedStorageProbed += new EventHandler<ProbeIsolatedStorageEventArgs>(this.Instance_IsolatedStorageProbed);
            AirWatchModel.Instance.GetCulturesCompleted += new EventHandler<LanguagesReceivedEventArgs>(this.Instance_LanguagesLoaded);
            AirWatchModel.Instance.ChangeCultureCompleted += new EventHandler(this.GetLanguageCompleted);
            AirWatchModel.Instance.ErrorOccurred += new EventHandler(this.AirWatchModelErrorOccured);
            AirWatchModel.Instance.HomePushpinLoaded += new EventHandler<Microsoft.AirWatch.UICommon.PushpinLoadedEventArgs>(this.HomePushpinLoaded);
            AirWatchModel.Instance.HomeWaterStationLoaded += new EventHandler<Microsoft.AirWatch.UICommon.StationLoadedEventArgs>(this.HomeWaterStationLoaded);
        }

        /// <summary>
        /// Occurs when [hide loading animation].
        /// </summary>
        public event EventHandler HideLoadingAnimation;

        /// <summary>
        /// Occurs when [language received].
        /// </summary>
        public event EventHandler LanguageReceived;

        /// <summary>
        /// Occurs when [languages received].
        /// </summary>
        public event EventHandler LanguagesReceived;

        /// <summary>
        /// Occurs when [map moved].
        /// </summary>
        public event EventHandler MapMoved;

        /// <summary>
        /// Occurs when user clips more info on key
        /// </summary>
        public event EventHandler UserFeedbackMoreInfo;

        /// <summary>
        /// Occurs when user clicks more info station key
        /// </summary>
        public event EventHandler StationsMoreInfo;

        /// <summary>
        /// Occurs when an [error occured] in the AirWatchModel.
        /// </summary>
        public event EventHandler ErrorOccurred;        

        /// <summary>
        /// Gets or sets a value indicating whether [isolated storage available].
        /// </summary>
        /// <value>
        /// <c>true</c> if [isolated storage available]; otherwise, <c>false</c>.
        /// </value>
        public bool IsolatedStorageUnavailable
        {
            get
            {
                return this.isolatedStorageUnavailable;
            }

            set
            {
                this.isolatedStorageUnavailable = value;
                this.NotifyPropertyChanged("IsolatedStorageUnavailable");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [fly out panel unlocked].
        /// </summary>
        /// <value><c>true</c> if [fly out panel unlocked]; otherwise, <c>false</c>.</value>
        public bool FlyOutPanelUnlocked
        {
            get
            {
                return this.flyOutPanelUnlocked;
            }

            set
            {
                this.flyOutPanelUnlocked = value;
                this.NotifyPropertyChanged("FlyOutPanelUnlocked");
            }
        }

        /// <summary>
        /// Gets the current language dictionary.
        /// </summary>
        /// <value>The current language dictionary.</value>
        public Dictionary<string, string> CurrentLanguageDictionary
        {
            get
            {
                return AirWatchModel.Instance.CurrentLanguageDictionary;
            }     
        }

        /// <summary>
        /// Gets or sets the languages.
        /// </summary>
        /// <value>The languages.</value>
        public Collection<LanguageDescription> Languages
        {
            get
            {
                return this.languages;
            }

            set
            {
                this.languages = value;
                this.NotifyPropertyChanged("Languages");
            }
        }

        /// <summary>
        /// Gets or sets the map culture.
        /// </summary>
        /// <value>The map culture.</value>
        public string CurrentCulture
        {
            get
            {
                return AirWatchModel.Instance.CurrentCulture;
            }

            set
            {
                AirWatchModel.Instance.CurrentCulture = value;
                this.NotifyPropertyChanged("CurrentCulture");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the  Language Loaded state is true.
        /// </summary>
        /// <value>The Language Loaded state.</value> 
        public bool LanguageLoaded
        {
            get
            {
                return this.languageLoaded || (AirWatchModel.Instance.CurrentLanguageDictionary != null && AirWatchModel.Instance.CurrentLanguageDictionary.Count != 0);
            }
        }

        /// <summary>
        /// Gets the selected language.
        /// </summary>
        /// <param name="culture">The culture.</param>
        public void GetSelectedLanguage(string culture)
        {
            AirWatchModel.Instance.GetLanguage(culture);
        }

        /// <summary>
        /// More Info clicked
        /// </summary>
        public void UserFeedbackGetMoreInfo()
        {
            if (this.UserFeedbackMoreInfo != null)
            {
                this.UserFeedbackMoreInfo.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Station More Info clicked
        /// </summary>
        public void StationsGetMoreInfo()
        {
            if (this.StationsMoreInfo != null)
            {
                this.StationsMoreInfo.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Maps the moved.
        /// </summary>
        public void MapMove()
        {
            if (this.MapMoved != null)
            {
                this.MapMoved.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Changes the page tile.
        /// </summary>
        public void ChangePageTile()
        {
            ScriptObjectCollection soc = HtmlPage.Document.GetElementsByTagName("Title");

            if (soc != null && soc.Count > 0)
            {
                HtmlElement titleElement = (HtmlElement)soc[0];

                string title = (string)titleElement.GetProperty("innerHTML");

                if (ClientServices.Instance.MainViewModel.CurrentLanguageDictionary != null && ClientServices.Instance.MainViewModel.CurrentLanguageDictionary.ContainsKey("COMMON_TITLE"))
                {
                    HtmlPage.Document.SetProperty("title", ClientServices.Instance.MainViewModel.CurrentLanguageDictionary["COMMON_TITLE"]);
                }
            }
        }

        /// <summary>
        /// Homes the water station loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.UICommon.StationLoadedEventArgs"/> instance containing the event data.</param>
        private void HomeWaterStationLoaded(object sender, Microsoft.AirWatch.UICommon.StationLoadedEventArgs e)
        {
            this.flyoutWaterStationLoaded = true;

            if (this.LanguageLoaded && this.flyoutHomePushpinStationLoaded && this.HideLoadingAnimation != null)
            {
                this.HideLoadingAnimation(this, new EventArgs());
            }
        }

        /// <summary>
        /// Homes the pushpin loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.UICommon.PushpinLoadedEventArgs"/> instance containing the event data.</param>
        private void HomePushpinLoaded(object sender, Microsoft.AirWatch.UICommon.PushpinLoadedEventArgs e)
        {
            this.flyoutHomePushpinStationLoaded = true;

            if (this.LanguageLoaded && this.flyoutWaterStationLoaded && this.HideLoadingAnimation != null)
            {
                this.HideLoadingAnimation(this, new EventArgs());
            }
        }  

        /// <summary>
        /// Handles the IsolatedStorageProbed event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.ProbeIsolatedStorageEventArgs"/> instance containing the event data.</param>
        private void Instance_IsolatedStorageProbed(object sender, ProbeIsolatedStorageEventArgs e)
        {
            this.IsolatedStorageUnavailable = !e.Success;
        }

        /// <summary>
        /// Handles the LanguageLoaded event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.LanguageReceivedEventArgs"/> instance containing the event data.</param>
        private void GetLanguageCompleted(object sender, EventArgs e)
        {
            this.languageLoaded = true;

            if (this.flyoutHomePushpinStationLoaded && this.flyoutWaterStationLoaded && this.HideLoadingAnimation != null)
            {
                this.HideLoadingAnimation(this, new EventArgs());
            }

            this.LanguageReceived(this, new EventArgs());
        }

        /// <summary>
        /// Handles the LanguagesLoaded event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.LanguagesReceivedEventArgs"/> instance containing the event data.</param>
        private void Instance_LanguagesLoaded(object sender, LanguagesReceivedEventArgs e)
        {
            // TODO: Add Error handling.
            this.Languages = e.LanguageDescriptions;
            this.LanguagesReceived(this, new EventArgs());
        }

        /// <summary>
        /// Airs the watch model error occured.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AirWatchModelErrorOccured(object sender, EventArgs e)
        {
            if (this.ErrorOccurred != null)
            {
                this.ErrorOccurred(this, new EventArgs());
            }
        }
    }
}
