﻿// <copyright file="MapHelper.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>03-07-2009</date>
// <summary>Provides methods to help with the map control</summary>
namespace Microsoft.AirWatch.ViewModel
{
    using System;
    using System.Windows;
    using Microsoft.Maps.MapControl;
    using AirwatchData = Microsoft.AirWatch.Model;

    /// <summary>
    /// Provides methods to help with the map control
    /// </summary>
    public static class MapHelper
    {
        /// <summary>
        /// Gets or sets the current map.
        /// </summary>
        /// <value>The current map.</value>
        public static Map CurrentMap { get; set; }

        /// <summary>
        /// Gets or sets the top margin.
        /// </summary>
        /// <value>The top margin.</value>
        public static double TopMargin { get; set; }

        /// <summary>
        /// Gets or sets the right margin.
        /// </summary>
        /// <value>The right margin.</value>
        public static double RightMargin { get; set; }

        /// <summary>
        /// Gets or sets the top left boundary.
        /// </summary>
        /// <value>The top left boundary.</value>
        public static Location TopLeftBoundary { get; set; }

        /// <summary>
        /// Gets or sets the bottom right boundary.
        /// </summary>
        /// <value>The bottom right boundary.</value>
        public static Location BottomRightBoundary { get; set; }

        /// <summary>
        /// Converts Latitude and Longitude values into Pixel X and Y values (of whole map).
        /// </summary>
        /// <param name="latitude">latitude value</param>
        /// <param name="longitude">longitude value</param>
        /// <returns>double[] pixel</returns>
        public static double[] LatLongToMapPixelXY(double latitude, double longitude)
        {
            return LatLongToMapPixelXY(latitude, longitude, (int)CurrentMap.ZoomLevel);
        }

        /// <summary>
        /// Converts Latitude and Longitude values into Pixel X and Y values (of whole map).
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="mapZoomLevel">The map zoom level.</param>
        /// <returns>double[] pixel</returns>
        public static double[] LatLongToMapPixelXY(double latitude, double longitude, int mapZoomLevel)
        {
            double x = (longitude + 180) / 360;
            double sinLatitude = Math.Sin(latitude * Math.PI / 180);
            double y = 0.5 - (Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI));
            uint mapSize = MapSize(mapZoomLevel);

            double[] pixel = new double[2];

            pixel[0] = (double)(x * mapSize);
            pixel[1] = (double)(y * mapSize);

            return pixel;
        }

        /// <summary>
        /// Converts Pixel X and Y values (of whole map) to a Latitude and Longitude
        /// </summary>
        /// <param name="pixelX">pixel x value</param>
        /// <param name="pixelY">pixel y value</param>
        /// <returns>Location loc</returns>
        public static Location MapPixelXYToLatLong(double pixelX, double pixelY)
        {
            return MapPixelXYToLatLong(pixelX, pixelY, (int)CurrentMap.ZoomLevel);
        }

        /// <summary>
        /// Converts Pixel X and Y values (of whole map) to a Latitude and Longitude
        /// </summary>
        /// <param name="pixelX">The pixel X.</param>
        /// <param name="pixelY">The pixel Y.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>The lat/long as a location</returns>
        public static Location MapPixelXYToLatLong(double pixelX, double pixelY, int zoomLevel)
        {
            uint mapSize = MapSize(zoomLevel);

            double x = (double)pixelX / (double)mapSize;
            double y = (double)pixelY / (double)mapSize;

            double a = 2 * Math.PI * (1 - (2 * y));
            double b = (Math.Pow(Math.E, a) - 1) / (Math.Pow(Math.E, a) + 1);

            Location loc = new Location();

            loc.Latitude = (180 / Math.PI) * Math.Asin(b);
            loc.Longitude = (360 * x) - 180;

            return loc;
        }

        /// <summary>
        /// Converts pixel XY relative to the map control to lat long.
        /// </summary>
        /// <param name="positionX">The position X.</param>
        /// <param name="positionY">The position Y.</param>
        /// <returns>The lat/long as a location</returns>
        public static Location ControlPixelXYToLatLong(double positionX, double positionY)
        {
            return ControlPixelXYToLatLong(positionX, positionY, (int)CurrentMap.ZoomLevel);
        }

        /// <summary>
        /// Converts pixel XY relative to the map control to lat long.
        /// </summary>
        /// <param name="positionX">The position X.</param>
        /// <param name="positionY">The position Y.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>The lat/long as a location</returns>
        public static Location ControlPixelXYToLatLong(double positionX, double positionY, int zoomLevel)
        {
            double[] pixel = LatLongToMapPixelXY(CurrentMap.Center.Latitude, CurrentMap.Center.Longitude, zoomLevel);

            pixel[0] = pixel[0] - ((CurrentMap.ActualWidth / 2) - positionX);
            pixel[1] = pixel[1] - ((CurrentMap.ActualHeight / 2) - positionY);

            Location location = MapPixelXYToLatLong(pixel[0], pixel[1], zoomLevel);

            return location;
        }

        /// <summary>
        /// Gets the flyout position.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="pinWidth">Width of the pin.</param>
        /// <param name="pinHeight">Height of the pin.</param>
        /// <param name="flyOutWidth">Width of the fly out.</param>
        /// <param name="flyOutHeight">Height of the fly out.</param>
        /// <returns>Thickness to position the flyout</returns>
        public static Thickness GetFlyOutPosition(double longitude, double latitude, double pinWidth, double pinHeight, double flyOutWidth, double flyOutHeight)
        {
            double flyOutOffset = 10;
            
            double mapHeight = CurrentMap.ActualHeight;
            double mapWidth = CurrentMap.ActualWidth;

            double[] pixelCenter = LatLongToMapPixelXY(CurrentMap.Center.Latitude, CurrentMap.Center.Longitude);

            double right = pixelCenter[0] + (mapWidth / 2);
            double top = pixelCenter[1] - (mapHeight / 2);
           
            double[] pushpin = LatLongToMapPixelXY(latitude, longitude);
            
            if (pushpin[1] < (top + MapHelper.TopMargin))
            {
                if (pushpin[0] < (right - MapHelper.RightMargin))
                {
                    return new Thickness(pinWidth - flyOutOffset, -flyOutOffset, 0, 0); // FlyOutPositionBottomLeft
                }
                else
                {
                    return new Thickness(-flyOutWidth + flyOutOffset, -flyOutOffset, 0, 0); // FlyOutPositionBottomRight;
                }
            }
            else if (pushpin[0] < (right - MapHelper.RightMargin))
            {
                return new Thickness(pinWidth - flyOutOffset, -(flyOutHeight + pinHeight - flyOutOffset), 0, 0); // FlyOutPositionTopRight;
            }
            else
            {
                return new Thickness(-flyOutWidth + flyOutOffset, -(flyOutHeight + pinHeight - flyOutOffset), 0, 0); // FlyOutPositionTopLeft;
            }
        }

        /// <summary>
        /// Gets the optimal zoom for area.
        /// </summary>
        /// <param name="topLeft">The top left.</param>
        /// <param name="bottomRight">The bottom right.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>zoom level</returns>
        public static double GetOptimalZoomForArea(Location topLeft, Location bottomRight, double width, double height)
        { 
            for (double zoom = ClientServices.Instance.MapViewModel.MapZoomMax; zoom >= ClientServices.Instance.MapViewModel.MapZoomMin; zoom--)
            {
                double[] topLeftPixels = LatLongToMapPixelXY(topLeft.Latitude, topLeft.Longitude, (int)zoom);
                double[] bottomRightPixels = LatLongToMapPixelXY(bottomRight.Latitude, bottomRight.Longitude, (int)zoom);

                if (bottomRightPixels[0] - topLeftPixels[0] <= width && bottomRightPixels[1] - topLeftPixels[1] <= height)
                {
                    return zoom;
                }
            }

            return ClientServices.Instance.MapViewModel.MapZoomMin;
        }
        
        /// <summary>
        /// Sets the map boundaries.
        /// </summary>
        /// <param name="northPixel">The north pixel.</param>
        /// <param name="southPixel">The south pixel.</param>
        /// <param name="eastPixel">The east pixel.</param>
        /// <param name="westPixel">The west pixel.</param>
        public static void SetMapBoundaries(double northPixel, double southPixel, double eastPixel, double westPixel)
        {
            TopLeftBoundary = ControlPixelXYToLatLong(westPixel, northPixel);
            BottomRightBoundary = ControlPixelXYToLatLong(eastPixel, southPixel);
        }

        /// <summary>
        /// returns map zoom level
        /// </summary>
        /// <param name="levelOfDetail">level of detail on map</param>
        /// <returns>map zoom level</returns>
        private static uint MapSize(int levelOfDetail)
        {
            return (uint)256 << levelOfDetail;
        }
    }
}
