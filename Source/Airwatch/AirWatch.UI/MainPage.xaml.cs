﻿// <copyright file="MainPage.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-04-2009</date>
// <summary>Partial class for the Silverlight Application's Main Page.</summary>
namespace AirWatch.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Browser;
    using System.Windows.Controls;
    using System.Windows.Input;    
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.ViewModel;
    
    /// <summary>
    /// Partial Class for the Silverlight Application's Main Page
    /// </summary>
    public partial class MainPage : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the MainPage class.
        /// </summary>
        public MainPage()
        {
            ClientServices.Instance.MainViewModel.ChangePageTile();

            System.Windows.Interop.SilverlightHost host = Application.Current.Host;

            // The Settings object, which represents Web browser settings.
            System.Windows.Interop.Settings settings = host.Settings;

            // Read/write properties of the Settings object.
            settings.EnableFrameRateCounter = false;
            settings.EnableRedrawRegions = false;
            settings.MaxFrameRate = 60;
            settings.EnableAutoZoom = false;

            this.InitializeComponent();
            this.Loaded += new RoutedEventHandler(this.MainPageLoaded);
            this.DataContext = ClientServices.Instance.MainViewModel;
            this.Banner.MouseLeftButtonUp += new MouseButtonEventHandler(this.BannerMouseLeftButtonUp);

            this.FlyOutPanelToggle.Click += new RoutedEventHandler(this.FlyOutPanelToggleClick);

            this.VEMap.MouseLeftButtonDown += new MouseButtonEventHandler(this.VEMapMouseLeftButtonDown);

            this.VEMap.MouseWheel += new MouseWheelEventHandler(this.VEMapMouseWheel);

            ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked = true;

            this.FlyOutPanelDisappear.Completed += new EventHandler(this.FlyOutPanelDisappearCompleted);

            ClientServices.Instance.SearchViewModel.FlyOutStatusChangeEvent += new EventHandler(this.InstanceFlyOutStatusChangeEvent);
            ClientServices.Instance.MainViewModel.LanguageReceived += new EventHandler(this.MainViewModel_LanguageReceived);
            ClientServices.Instance.MainViewModel.LanguagesReceived += new EventHandler(this.MainViewModel_LanguagesReceived);
            ClientServices.Instance.MainViewModel.UserFeedbackMoreInfo += new EventHandler(this.MainViewModelUserFeedbackMoreInfo);
            ClientServices.Instance.MainViewModel.StationsMoreInfo += new EventHandler(this.MainViewModelStationsMoreInfo);
            ClientServices.Instance.MainViewModel.ErrorOccurred += new EventHandler(this.MainViewModelErrorOccured);
            ClientServices.Instance.MainViewModel.HideLoadingAnimation += new EventHandler(this.MainViewModelHideLoadingAnimation);

            this.ChangeLanguageButton.Click += new RoutedEventHandler(this.ChangeLanguageButtonClick);            
            this.DevelopersButton.Click += new RoutedEventHandler(this.DevelopersButtonClick);
            this.DevelopersCloseButton.Click += new RoutedEventHandler(this.DevelopersCloseButtonClick);
            this.LanguageCancelButton.Click += new RoutedEventHandler(this.LanguageCancelButtonClick);
            this.LanguageOkButton.Click += new RoutedEventHandler(this.LanguageOkButton_Click);
            this.ChangeLanguageCloseButton.Click += new RoutedEventHandler(this.ChangeLanguageCloseButtonClick);
            this.ProvidersButton.Click += new RoutedEventHandler(this.ProvidersButtonClick);
            this.ProvidersCloseButton.Click += new RoutedEventHandler(this.ProvidersCloseButtonClick);
            this.DisclaimerButton.Click += new RoutedEventHandler(this.DisclaimerButtonClick);
            this.DisclaimerCloseButton.Click += new RoutedEventHandler(this.DisclaimerCloseButtonClick);            
        }              

        /// <summary>
        /// Checks for parameters for sharing and the gadget.
        /// </summary>
        private void CheckForParameters()
        {
            IDictionary<string, string> parameters = HtmlPage.Document.QueryString;

            // for sharing
            string locationString;
            
            if (parameters.TryGetValue("location", out locationString))
            {
                string[] splitLocationString = locationString.Split(';');
                
                double longitude;
                double latitude;
                string locationType = string.Empty;

                if (splitLocationString.Length >= 3)
                {
                    locationType = splitLocationString[2].ToString();
                }

                if (splitLocationString.Length >= 2 && double.TryParse(splitLocationString[0], out longitude) && double.TryParse(splitLocationString[1], out latitude))
                {
                    if (longitude >= -180 && longitude <= 180 && latitude >= -90 && latitude <= 90)
                    {
                        if (!locationType.Equals("station"))
                        {
                            ClientServices.Instance.MapViewModel.AddPushpin(latitude, longitude);
                        }

                        ClientServices.Instance.MapViewModel.MoveMap(latitude, longitude);
                        ClientServices.Instance.MapViewModel.MapZoomLevel = 12;
                    }
                }
            }

            // for the gadget
            string longitudeString;
            string latitudeString;
            string zoomString;
            string showPushpinString;

            double latitudeValue;
            double longitudeValue;
            bool showPushpin = false;
            double zoomValue;

            if (parameters.TryGetValue("zoom", out zoomString))
            {
                if (double.TryParse(zoomString, out zoomValue) == true && zoomValue >= 3 && zoomValue <= 24)
                {
                    ClientServices.Instance.MapViewModel.MapZoomLevel = zoomValue;
                }
            }

            if (parameters.TryGetValue("showpushpin", out showPushpinString))
            {
                showPushpin = bool.Parse(showPushpinString); 
            }

            if (parameters.TryGetValue("latitude", out latitudeString) && parameters.TryGetValue("longitude", out longitudeString))
            {
                if (double.TryParse(latitudeString, out latitudeValue) && double.TryParse(longitudeString, out longitudeValue))
                {
                    if (longitudeValue >= -180 && longitudeValue <= 180 && latitudeValue >= -90 && latitudeValue <= 90)
                    {
                        if (showPushpin == true)
                        {
                            ClientServices.Instance.MapViewModel.AddPushpin(latitudeValue, longitudeValue); 
                        }

                        ClientServices.Instance.MapViewModel.MoveMap(latitudeValue, longitudeValue);
                        this.FlyOutPanelDisappear.Begin();
                        MapHelper.RightMargin = 765;
                    }
                }
            } 
        }

        /// <summary>
        /// Mains the view model hide loading animation.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MainViewModelHideLoadingAnimation(object sender, EventArgs e)
        {
            this.SecondaryLoadingPage.Visibility = Visibility.Collapsed;
            this.SecondaryLoadingAnimation.AnimationVisibility = Visibility.Collapsed;
        } 

        /// <summary>
        /// Mains the view model error occured.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MainViewModelErrorOccured(object sender, EventArgs e)
        {
            this.ErrorPage.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the UserfeedbackMoreInfo Button
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MainViewModelUserFeedbackMoreInfo(object sender, EventArgs e)
        {
            ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible = true;
            this.FlyOutPanelAppear.Begin();
            MapHelper.RightMargin = 765;
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the StationsMoreInfo Button
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MainViewModelStationsMoreInfo(object sender, EventArgs e)
        {
            ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible = true;
            this.FlyOutPanelAppear.Begin();
            MapHelper.RightMargin = 765;
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the Banner control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void BannerMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ClientServices.Instance.MapViewModel.AddPushpinCancel();
        }   

        /// <summary>
        /// Handles the LanguageReceived event of the MainViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.ViewModel.LanguageDictionaryRetrievedEventArgs"/> instance containing the event data.</param>
        private void MainViewModel_LanguageReceived(object sender, EventArgs e)
        {            
            this.LanguageCancelButton.IsEnabled = true;
            this.LanguageOkButton.IsEnabled = true;
            this.LanguageComboBox.IsEnabled = true;            
            this.WaitingAnimation.AnimationVisibility = Visibility.Collapsed;
            this.ChangeLanguageOverlay.Visibility = Visibility.Collapsed;
            this.GadgetHyperlinkButton.NavigateUri = new Uri("/gadgetinfo.aspx?culture=" + ClientServices.Instance.MainViewModel.CurrentCulture, UriKind.Relative);

            ClientServices.Instance.MainViewModel.ChangePageTile();
        }        

        /// <summary>
        /// Handles the LanguagesReceived event of the MainViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MainViewModel_LanguagesReceived(object sender, EventArgs e)
        {
            if (this.LanguageComboBox.Items.Count > 0)
            {               
                this.LanguageComboBox.SelectedItem = ClientServices.Instance.MainViewModel.Languages.Single(p => p.LanguageCode == ClientServices.Instance.MainViewModel.CurrentCulture);
                this.ChangeLanguageButton.Visibility = Visibility.Visible;
            }            
        } 

        /// <summary>
        /// Event to close all of the panels in the main page
        /// </summary>
        private void CloseAllPanels()
        {
            this.ChangeLanguageDisappear.Begin();
            this.DevelopersDissappear.Begin();
            this.ProvidersDissappear.Begin();
            this.DisclaimerPanelDisappear.Begin();
        }

        /// <summary>
        /// Handles the Click event of the LanguageOkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void LanguageOkButton_Click(object sender, RoutedEventArgs e)
        {
            this.LanguageCancelButton.IsEnabled = false; 
            this.LanguageOkButton.IsEnabled = false;
            this.LanguageComboBox.IsEnabled = false;
            this.WaitingAnimation.AnimationVisibility = Visibility.Visible;
            ClientServices.Instance.MainViewModel.GetSelectedLanguage((this.LanguageComboBox.SelectedItem as LanguageDescription).LanguageCode);
            ClientServices.Instance.MapViewModel.MapCulture = (this.LanguageComboBox.SelectedItem as LanguageDescription).LanguageCode;
        }

        /// <summary>
        /// Event to open the disclaimer panel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void DisclaimerButtonClick(object sender, RoutedEventArgs e)
        {
            this.CloseAllPanels();
            this.DisclaimerPanelAppear.Begin();
        }

        /// <summary>
        /// Event to close the disclaimer panel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void DisclaimerCloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.DisclaimerPanelDisappear.Begin();
        }

        /// <summary>
        /// Event to change language and show panel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void LanguageCancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.ChangeLanguageDisappear.Begin();
        }

        /// <summary>
        /// Event to change language and show panel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void ChangeLanguageButtonClick(object sender, RoutedEventArgs e)
        {
            this.CloseAllPanels();
            this.ChangeLanguageAppear.Begin();
            this.ProvidersDissappear.Begin();
        }

        /// <summary>
        /// Event to cclose the change language page
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void ChangeLanguageCloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.ChangeLanguageDisappear.Begin();
        }

        /// <summary>
        /// Event to close the providers page
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void ProvidersCloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.ProvidersDissappear.Begin();
        }

        /// <summary>
        /// Event to open the providers page
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void ProvidersButtonClick(object sender, RoutedEventArgs e)
        {
            this.CloseAllPanels();
            this.ProvidersAppear.Begin();
        }

        /// <summary>
        /// Event to open the developers page
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void DevelopersButtonClick(object sender, RoutedEventArgs e)
        {
            this.CloseAllPanels();
            this.DevelopersAppear.Begin();
        }

        /// <summary>
        /// Event to close the developers page
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
         private void DevelopersCloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.DevelopersDissappear.Begin();
        }     

        /// <summary>
        /// Event to hide flyout panel on user mouse wheel zoom
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseWheelEventArgs e</param>
        private void VEMapMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible == true && ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked == true)
            {
                this.FlyOutPanelDisappear.Begin();
                MapHelper.RightMargin = 380;
            }

            ClientServices.Instance.MainViewModel.MapMove();
        }

        /// <summary>
        /// Event that causes the fly out panel to close
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        private void InstanceFlyOutStatusChangeEvent(object sender, EventArgs e)
        {
            if (ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible == true && ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked == true)
            {
                this.FlyOutPanelDisappear.Begin();
                MapHelper.RightMargin = 765;
            }
        }
        
        /// <summary>
        /// Event to hide home flyout if user interacts with map
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseButtonEventArgs e</param>
        private void VEMapMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible == true && ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked == true)
            {
                this.FlyOutPanelDisappear.Begin();
                MapHelper.RightMargin = 380;
            }

            ClientServices.Instance.MainViewModel.MapMove();
        }

        /// <summary>
        /// Change the visibility of the flyout panel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void FlyOutPanelToggleClick(object sender, RoutedEventArgs e)
        {
            if (ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible && ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked == true)
            {
                this.FlyOutPanelDisappear.Begin();
                MapHelper.RightMargin = 410;
            }
            else
            {
                ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible = true;
                this.FlyOutPanelAppear.Begin();
                MapHelper.RightMargin = 765;
            }
        }

        /// <summary>
        /// Event to collapse the fly out after hide animation completes
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        private void FlyOutPanelDisappearCompleted(object sender, EventArgs e)
        {
            ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible = false;
        }

        /// <summary>
        /// Mains the page loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MainPageLoaded(object sender, RoutedEventArgs e)
        {
            this.VEMap.Focus();
            ClientServices.Instance.FlyOutViewModel.FlyOutPanelVisible = true;
            this.CheckForParameters();
        }        
    }
}
