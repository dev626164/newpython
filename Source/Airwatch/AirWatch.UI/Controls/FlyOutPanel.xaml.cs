﻿// <copyright file="FlyOutPanel.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>11-06-2009</date>
// <summary>Partial class code-behind for the flyout panel.</summary>

namespace AirWatch.UI
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Partial class code-behind for the flyout panel
    /// </summary>
    public partial class FlyOutPanel : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FlyOutPanel"/> class.
        /// </summary>
        public FlyOutPanel()
        {
            this.InitializeComponent();

            this.DataContext = ClientServices.Instance.FlyOutViewModel;

            this.SizeChanged += new SizeChangedEventHandler(this.FlyOutPanelSizeChanged);
            this.MouseLeftButtonUp += new MouseButtonEventHandler(this.FlyOutPanelMouseLeftButtonUp);

            this.HomeRadioButton.Checked += new RoutedEventHandler(this.HomeRadioButton_Checked);
            this.SmsRadioButton.Checked += new RoutedEventHandler(this.SmsRadioButton_Checked);
            this.AboutRadioButton.Checked += new RoutedEventHandler(this.AboutRadioButton_Checked);
            this.HelpRadioButton.Checked += new RoutedEventHandler(this.HelpRadioButton_Checked);

            this.SMSMoreInfoButton.Click += new RoutedEventHandler(this.SMSMoreInfoButtonClick);

            this.LockFlyOutPanel.Checked += new RoutedEventHandler(this.LockFlyOutPanelChecked);
            this.LockFlyOutPanel.Unchecked += new RoutedEventHandler(this.LockFlyOutPanelUnchecked);

            this.ChangeLocation.MouseLeftButtonUp += new MouseButtonEventHandler(this.ChangeLocation_MouseLeftButtonUp);
            this.SearchButton.Click += new System.Windows.RoutedEventHandler(this.SearchButtonClick);
            this.ResultsList.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.ResultsListMouseLeftButtonUp);
            this.SearchBox.KeyUp += new System.Windows.Input.KeyEventHandler(this.SearchBoxKeyUp);
            this.CancelButton.Click += new RoutedEventHandler(this.CancelButtonClick);

            this.changeLocationFadeOut.Completed += new System.EventHandler(this.ChangeLocationFadeOutCompleted);

            ClientServices.Instance.FlyOutViewModel.SingleResultHomeLocationChangeEvent += new System.EventHandler(this.FlyOutViewModelSingleResultHomeLocationChangeEvent);
            ClientServices.Instance.MainViewModel.UserFeedbackMoreInfo += new System.EventHandler(this.MainViewModelUserFeedbackMoreInfo);
            ClientServices.Instance.MainViewModel.StationsMoreInfo += new System.EventHandler(this.MainViewModelStationsMoreInfo);
            
            FlyOutViewModel.LoadIsolatedStorageSettings();
        }

        /// <summary>
        /// Hides the panels.
        /// </summary>
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the FlyOutPanel control.
        /// </summary>
        private static void HidePanels()
        {
            ClientServices.Instance.FlyOutViewModel.HomePanelVisible = false;
            ClientServices.Instance.FlyOutViewModel.SmsPanelVisible = false;
            ClientServices.Instance.FlyOutViewModel.AboutPanelVisible = false;
            ClientServices.Instance.FlyOutViewModel.HelpPanelVisible = false;
        }

        /// <summary>
        /// Handles the userfeedbackmoreinfo event of the key control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MainViewModelUserFeedbackMoreInfo(object sender, System.EventArgs e)
        {
            this.HelpRadioButton.IsChecked = true;
            this.UserFeedbackAnimatedExpander.IsExpanded = true;
        }

        /// <summary>
        /// Handles the StationsMoreInfo event of the key control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MainViewModelStationsMoreInfo(object sender, System.EventArgs e)
        {
            this.HelpRadioButton.IsChecked = true;
            this.CurrentBathingSeasonAnimatedExpander.IsExpanded = true;
            this.AirQualityIndexAnimatedExpander.IsExpanded = true;
        }

        /// <summary>
        /// Hides the panels.
        /// </summary>
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the FlyOutPanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void FlyOutPanelMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ClientServices.Instance.MapViewModel.AddPushpinCancel();
        }

        /// <summary>
        /// Handles the SizeChanged event of the FlyOutPanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.SizeChangedEventArgs"/> instance containing the event data.</param>
        private void FlyOutPanelSizeChanged(object sender, SizeChangedEventArgs e)
        {
            double newSize = e.NewSize.Height - 85; 

            if (newSize < 460)
            {
                this.ScrollViewer.Height = newSize;
            }
            else
            {
                this.ScrollViewer.Height = 460;
            }

            if (newSize > 600)
            {
                this.RSSBorder.Visibility = Visibility.Visible;
            }
            else
            {
                this.RSSBorder.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Update status after animation
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">System.EventArgs e</param>
        private void ChangeLocationFadeOutCompleted(object sender, System.EventArgs e)
        {
            ClientServices.Instance.FlyOutViewModel.SearchVisible = false;
            ClientServices.Instance.FlyOutViewModel.ErrorVisible = false;
            ClientServices.Instance.FlyOutViewModel.ResultsVisible = false;
        }

        /// <summary>
        /// Hide flyout from single result search
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">System.EventArgs e</param>
        private void FlyOutViewModelSingleResultHomeLocationChangeEvent(object sender, System.EventArgs e)
        {
            this.changeLocationFadeOut.Begin();
        }

        /// <summary>
        /// Cancels the save location and returns to home view
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.SearchBox.Text = string.Empty;
            this.changeLocationFadeOut.Begin();
        }

        /// <summary>
        /// Opens the SMS tab
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void SMSMoreInfoButtonClick(object sender, RoutedEventArgs e)
        {
            this.SmsRadioButton.IsChecked = true;
        }

        /// <summary>
        /// Event to unlock the fly out panel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void LockFlyOutPanelUnchecked(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked = true;
        }

        /// <summary>
        /// Event to lock the fly out panel in place
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void LockFlyOutPanelChecked(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MainViewModel.FlyOutPanelUnlocked = false;
        }

        /// <summary>
        /// Handles the Checked event of the HelpRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HelpRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            FlyOutPanel.HidePanels();
            ClientServices.Instance.FlyOutViewModel.HelpPanelVisible = true;
        }

        /// <summary>
        /// Handles the Checked event of the AboutRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void AboutRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            FlyOutPanel.HidePanels();
            ClientServices.Instance.FlyOutViewModel.AboutPanelVisible = true;
        }

        /// <summary>
        /// Handles the Checked event of the SmsRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SmsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            FlyOutPanel.HidePanels();
            ClientServices.Instance.FlyOutViewModel.SmsPanelVisible = true;
        }

        /// <summary>
        /// Handles the Checked event of the HomeRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HomeRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            FlyOutPanel.HidePanels();
            ClientServices.Instance.FlyOutViewModel.HomePanelVisible = true;
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the ChangeLocation control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ChangeLocation_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ClientServices.Instance.FlyOutViewModel.SearchVisible == false)
            {
                if (!ClientServices.Instance.FlyOutViewModel.IsolatedStorageUnavailable)
                {
                    FlyOutViewModel.ProbeIsolatedStorage();
                }

                ClientServices.Instance.FlyOutViewModel.SearchVisible = true;
                ////this.SearchBox.Focus();
                this.changeLocationFadeIn.Begin();
            }
        }

        /// <summary>
        /// Search box key up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void SearchBoxKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ClientServices.Instance.FlyOutViewModel.GetSearchResults(this.SearchBox.Text.ToString());
                this.SearchBox.Text = string.Empty;
            }
        }

        /// <summary>
        /// Searches the button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SearchButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ClientServices.Instance.FlyOutViewModel.GetSearchResults(this.SearchBox.Text.ToString());
            this.SearchBox.Text = string.Empty;
        }

        /// <summary>
        /// Results list mouse left button up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ResultsListMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            int selectedIndex = this.ResultsList.SelectedIndex;
            ClientServices.Instance.FlyOutViewModel.ConfirmLocation(selectedIndex);
            this.changeLocationFadeOut.Begin();
        }
    }
}
