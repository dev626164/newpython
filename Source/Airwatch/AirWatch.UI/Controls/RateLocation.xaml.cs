﻿// <copyright file="RateLocation.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>13-07-2009</date>
// <summary>User control to contain rating functions</summary>

namespace AirWatch.UI
{
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using Microsoft.AirWatch.UI.Controls;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// User control for rating a location
    /// </summary>
    public partial class RateLocation : UserControl
    {
        /// <summary>
        /// Dependency Property indicating if the push pin is showing air or water data
        /// </summary>
        public static readonly DependencyProperty TargetTypeProperty =
            DependencyProperty.Register("TargetType", typeof(string), typeof(RateLocation), new PropertyMetadata(new PropertyChangedCallback(TargetTypePropertyChanged)));

        /// <summary>
        /// Using a DependencyProperty as the backing store for CanRate.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty CanRateProperty =
            DependencyProperty.Register("CanRate", typeof(bool), typeof(RateLocation), new PropertyMetadata(new PropertyChangedCallback(OnRatingValuePropertyChanged)));

        /// <summary>
        /// Check if next has been clicked
        /// </summary>
        private bool nextClicked;

        /// <summary>
        /// The number qualifying words that are checked
        /// </summary>
        private int numberWordsChecked;

        /// <summary>
        /// Initializes a new instance of the RateLocation class
        /// </summary>
        public RateLocation()
        {
            this.InitializeComponent(); 

            this.NextStep.Click += new RoutedEventHandler(this.NextStepClick);
            this.Rate.Click += new RoutedEventHandler(this.RateClick);
            this.PreviousStep.Click += new RoutedEventHandler(this.PreviousStepClick);
            this.Cancel1.Click += new RoutedEventHandler(this.CancelClick);
            this.Cancel2.Click += new RoutedEventHandler(this.CancelClick);

            this.RatingButton1.Checked += new RoutedEventHandler(this.RatingButton1Checked);
            this.RatingButton2.Checked += new RoutedEventHandler(this.RatingButton2Checked);
            this.RatingButton3.Checked += new RoutedEventHandler(this.RatingButton3Checked);
            this.RatingButton4.Checked += new RoutedEventHandler(this.RatingButton4Checked);
            this.RatingButton5.Checked += new RoutedEventHandler(this.RatingButton5Checked);

            this.FirstWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.SecondWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.ThirdWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.FourthWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.FifthWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.SixthWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.SeventhWord.Checked += new RoutedEventHandler(this.RatingWordChecked);
            this.EighthWord.Checked += new RoutedEventHandler(this.RatingWordChecked);

            this.FirstWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.SecondWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.ThirdWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.FourthWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.FifthWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.SixthWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.SeventhWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
            this.EighthWord.Unchecked += new RoutedEventHandler(this.RatingWordUnchecked);
       }

        /// <summary>
        /// Gets or sets the rating scale.
        /// </summary>
        /// <value>The rating scale.</value>
        public int RatingValue { get; set; }

        /// <summary>
        /// Gets or sets the type of data in the pin
        /// </summary>
        public string TargetType
        {
            get { return (string)GetValue(TargetTypeProperty); }
            set { SetValue(TargetTypeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance can rate.
        /// </summary>
        /// <value><c>true</c> if this instance can rate; otherwise, <c>false</c>.</value>
        public bool CanRate
        {
            get { return (bool)GetValue(CanRateProperty); }
            set { SetValue(CanRateProperty, value); }
        }

        /// <summary>
        /// Targets the type property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void TargetTypePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            UpdateWords(sender);
        }

        /// <summary>
        /// Called when [rating value property changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnRatingValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            RateLocation element = sender as RateLocation;
            bool? value = args.NewValue as bool?;

            if (element != null && value.HasValue)
            {
                if (value.Value == true)
                {
                    element.RateGrid.Visibility = Visibility.Visible;
                    element.NoRateGrid.Visibility = Visibility.Collapsed;
                }
                else
                {
                    element.RateGrid.Visibility = Visibility.Collapsed;
                    element.NoRateGrid.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Updates the words.
        /// </summary>
        /// <param name="sender">The sender.</param>
        private static void UpdateWords(DependencyObject sender)
        {
            RateLocation rateLocation = sender as RateLocation;
            if (rateLocation.FirstWord != null && rateLocation.SecondWord != null && rateLocation.ThirdWord != null && rateLocation.FourthWord != null && rateLocation.FifthWord != null && rateLocation.SixthWord != null && rateLocation.SeventhWord != null && rateLocation.EighthWord != null)
            {
                string bindingTerm = string.Format(CultureInfo.InvariantCulture, "RATELOCATION_{0}_", rateLocation.TargetType.ToUpper(CultureInfo.InvariantCulture));

                rateLocation.FirstWord.Content   = bindingTerm + "0";
                rateLocation.SecondWord.Content  = bindingTerm + "1";
                rateLocation.ThirdWord.Content   = bindingTerm + "2";
                rateLocation.FourthWord.Content  = bindingTerm + "3";
                rateLocation.FifthWord.Content   = bindingTerm + "4";
                rateLocation.SixthWord.Content   = bindingTerm + "5";
                rateLocation.SeventhWord.Content = bindingTerm + "6";
                rateLocation.EighthWord.Content  = bindingTerm + "7";
            }
        }

        /// <summary>
        /// Handles the Checked event of the word controls.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingWordChecked(object sender, RoutedEventArgs e)
        {
            this.numberWordsChecked++;
            this.Rate.IsEnabled = true;
        }

        /// <summary>
        /// Handles the Unchecked event of the word controls.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingWordUnchecked(object sender, RoutedEventArgs e)
        {
            this.numberWordsChecked--;

            if (this.numberWordsChecked <= 0)
            {
                this.Rate.IsEnabled = false;
            }
        }

        /// <summary>
        /// Handles the Checked event of the RatingButton1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingButton1Checked(object sender, RoutedEventArgs e)
        {
            this.RatingValue = 1;
            this.NextStep.IsEnabled = true;
        }

        /// <summary>
        /// Handles the Checked event of the RatingButton2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingButton2Checked(object sender, RoutedEventArgs e)
        {
            this.RatingValue = 2;
            this.NextStep.IsEnabled = true;
        }

        /// <summary>
        /// Handles the Checked event of the RatingButton3 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingButton3Checked(object sender, RoutedEventArgs e)
        {
            this.RatingValue = 3;
            this.NextStep.IsEnabled = true;
        }

        /// <summary>
        /// Handles the Checked event of the RatingButton4 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingButton4Checked(object sender, RoutedEventArgs e)
        {
            this.RatingValue = 4;
            this.NextStep.IsEnabled = true;
        }

        /// <summary>
        /// Handles the Checked event of the RatingButton5 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RatingButton5Checked(object sender, RoutedEventArgs e)
        {
            this.RatingValue = 5;
            this.NextStep.IsEnabled = true;
        }

        /// <summary>
        /// Cancels the rating process for a location
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void CancelClick(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MapViewModel.CancelFlyOut();
            this.Reset();
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        private void Reset()
        {
            this.PreviousStepClick(this, null);

            this.RatingButton1.IsChecked = false;
            this.RatingButton2.IsChecked = false;
            this.RatingButton3.IsChecked = false;
            this.RatingButton4.IsChecked = false;
            this.RatingButton5.IsChecked = false;
            
            this.NextStep.IsEnabled = false;

            if (this.nextClicked)
            {
                this.FirstWord.IsChecked = false;
                this.SecondWord.IsChecked = false;
                this.ThirdWord.IsChecked = false;
                this.FourthWord.IsChecked = false;
                this.FifthWord.IsChecked = false;
                this.SixthWord.IsChecked = false;
                this.SeventhWord.IsChecked = false;
                this.EighthWord.IsChecked = false;
            }
        }

        /// <summary>
        /// Return to previous rating step
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e)</param>
        private void PreviousStepClick(object sender, RoutedEventArgs e)
        {
            this.RatePreviousStep.Completed += new System.EventHandler(this.RatePreviousStepCompleted);
            this.RatePreviousStep.Begin();
        }

        /// <summary>
        /// Fires when animation completes
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">System.EventArgs e</param>
        private void RatePreviousStepCompleted(object sender, System.EventArgs e)
        {
            this.NextStep.Visibility = Visibility.Visible;
            this.Rate.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Confirm user rating and send out details to database
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void RateClick(object sender, RoutedEventArgs e)
        {
            App ap = App.Current as App;

            bool[] words = new bool[8];

            for (int i = 0; i < 4; i++)
            {
                CheckBox cb = this.Words1.Children[i] as CheckBox;

                if (cb.IsChecked == true)
                {
                    words[i] = true;
                }

                cb = this.Words2.Children[i] as CheckBox;

                if (cb.IsChecked == true)
                {
                    words[i + 4] = true;
                }
            }

            if (this.DataContext is Microsoft.AirWatch.Core.Data.Pushpin)
            {
                Microsoft.AirWatch.Core.Data.Pushpin currentPin = this.DataContext as Microsoft.AirWatch.Core.Data.Pushpin;
                ClientServices.Instance.MapViewModel.SavePushpinRating(currentPin.PushpinId, System.Convert.ToDouble(currentPin.Location.Latitude), System.Convert.ToDouble(currentPin.Location.Longitude), this.RatingValue, this.TargetType, ap.UserAddress, words);
            }
            else if (this.DataContext is Microsoft.AirWatch.Core.Data.Station)
            {
                Microsoft.AirWatch.Core.Data.Station currentStation = this.DataContext as Microsoft.AirWatch.Core.Data.Station;
                ClientServices.Instance.MapViewModel.SaveStationRating(currentStation.StationId, System.Convert.ToDouble(currentStation.Location.Latitude), System.Convert.ToDouble(currentStation.Location.Longitude), this.RatingValue, this.TargetType, ap.UserAddress, words);
            }

            this.Reset();
        }

        /// <summary>
        /// Handles the next button click event to move to next step
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void NextStepClick(object sender, RoutedEventArgs e)
        {
            this.RateNextStep.Completed += new System.EventHandler(this.RateNextStepCompleted);
            this.RateNextStep.Begin();
        }

        /// <summary>
        /// Fires when animation completes
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">System.EventArgs e</param>
        private void RateNextStepCompleted(object sender, System.EventArgs e)
        {
            this.NextStep.Visibility = Visibility.Collapsed;
            this.Rate.Visibility = Visibility.Visible;

            this.nextClicked = true;
        }
    }
}