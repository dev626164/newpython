﻿// <copyright file="PinIcon.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>03-07-2009</date>
// <summary>Icon for toolbars/dragging</summary>
namespace AirWatch.UI
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// User control class for PinIcon
    /// </summary>
    public partial class PinIcon : UserControl
    {
        /// <summary>
        /// Dependency property for whether the pushpin has data or not
        /// </summary>
        public static readonly DependencyProperty HasDataProperty = DependencyProperty.Register("HasData", typeof(bool), typeof(PinIcon), null);

        /// <summary>
        /// Initializes a new instance of the PinIcon class
        /// </summary>
        public PinIcon()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has data.
        /// </summary>
        /// <value><c>true</c> if this instance has data; otherwise, <c>false</c>.</value>
        public bool HasData
        {
            get
            {
                return (bool)GetValue(HasDataProperty);
            }

            set
            {
                SetValue(HasDataProperty, value);
                this.UpdateIcon();
            }
        }

        /// <summary>
        /// Updates the icon.
        /// </summary>
        private void UpdateIcon()
        {
            if (this.HasData)
            {
                this.DataIcon.Visibility = Visibility.Visible;
                this.NoDataIcon.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.DataIcon.Visibility = Visibility.Collapsed;
                this.NoDataIcon.Visibility = Visibility.Visible;
            }
        }
    }
}
