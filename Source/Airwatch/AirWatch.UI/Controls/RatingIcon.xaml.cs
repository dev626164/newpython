﻿// <copyright file="RatingIcon.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>09-10-2009</date>
// <summary>Icon for Ratings</summary>
namespace AirWatch.UI
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// User control class for RatingIcon
    /// </summary>
    public partial class RatingIcon : UserControl
    {
        /// <summary>
        /// DP for rating value
        /// </summary>
        public static readonly DependencyProperty RatingValueProperty =
            DependencyProperty.Register("RatingValue", typeof(int), typeof(RatingIcon), new PropertyMetadata(new PropertyChangedCallback(OnRatingValuePropertyChanged)));

        /// <summary>
        /// Initializes a new instance of the RatingIcon class
        /// </summary>
        public RatingIcon()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the Rating Value
        /// </summary>
        public int RatingValue
        {
            get
            {
                return (int)GetValue(RatingValueProperty);
            }

            set
            {
                SetValue(RatingValueProperty, value);
            }
        }

        /// <summary>
        /// Called when [rating value property changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnRatingValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            RatingIcon ratingIcon = sender as RatingIcon;

            switch (ratingIcon.RatingValue)
            {
                case 1:
                    {
                        ratingIcon.HeadColor1.Color = new Color { A = 255, B = 7, G = 7, R = 255 };
                        ratingIcon.HeadColor2.Color = new Color { A = 255, B = 5, G = 5, R = 173 };
                        ratingIcon.StrokeColor.Color = new Color { A = 255, B = 2, G = 2, R = 178 };
                        break;
                    }

                case 2:
                    {
                        ratingIcon.HeadColor1.Color = new Color { A = 255, B = 123, G = 190, R = 255 };
                        ratingIcon.HeadColor2.Color = new Color { A = 255, B = 3, G = 159, R = 232 };
                        ratingIcon.StrokeColor.Color = new Color { A = 255, B = 27, G = 120, R = 213 };
                        break;
                    }

                case 3:
                    {
                        ratingIcon.HeadColor1.Color = new Color { A = 255, B = 4, G = 234, R = 233 };
                        ratingIcon.HeadColor2.Color = new Color { A = 255, B = 4, G = 223, R = 222 };
                        ratingIcon.StrokeColor.Color = new Color { A = 255, B = 0, G = 207, R = 206 };
                        break;
                    }

                case 4:
                    {
                        ratingIcon.HeadColor1.Color = new Color { A = 255, B = 145, G = 255, R = 193 };
                        ratingIcon.HeadColor2.Color = new Color { A = 255, B = 8, G = 255, R = 193 };
                        ratingIcon.StrokeColor.Color = new Color { A = 255, B = 8, G = 255, R = 133 };
                        break;
                    }

                case 5:
                    {
                        ratingIcon.HeadColor1.Color = new Color { A = 255, B = 35, G = 220, R = 72 };
                        ratingIcon.HeadColor2.Color = new Color { A = 255, B = 6, G = 169, R = 54 };
                        ratingIcon.StrokeColor.Color = new Color { A = 255, B = 11, G = 162, R = 7 };
                        break;
                    }
            }
        }
    }
}
