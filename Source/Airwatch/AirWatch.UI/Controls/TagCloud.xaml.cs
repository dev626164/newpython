﻿// <copyright file="TagCloud.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>16-07-2009</date>
// <summary>Tagcloud code-behind.</summary>
namespace AirWatch.UI
{
    using System.Collections.Generic;
    using System.Windows.Controls;
    using System.Windows.Media;
    using Microsoft.AirWatch.UI.Controls;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Tagcloud code-behind
    /// </summary>
    public partial class TagCloud : UserControl
    {
        /// <summary>
        /// Maximum text width in cloud
        /// </summary>
        private double maxTextWidth;

        /// <summary>
        /// keeps track of the localized text blocks
        /// </summary>
        private List<LocalizedTextBlock> localizedTextBlockList;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="TagCloud"/> class.
        /// </summary>
        public TagCloud()
        {
            this.InitializeComponent();

            this.maxTextWidth = this.Width;
            this.localizedTextBlockList = new List<LocalizedTextBlock>();

            this.Loaded += new System.Windows.RoutedEventHandler(this.TagCloudLoaded);
            ClientServices.Instance.MainViewModel.LanguageReceived += new System.EventHandler(this.MainViewModelLanguageReceived);
        }

        /// <summary>
        /// Handles the Loaded event of the TagCloud control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void TagCloudLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.ResizeCheck(); 
        }

        /// <summary>
        /// Handles the LanguageReceived event of the MainViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MainViewModelLanguageReceived(object sender, System.EventArgs e)
        {
            this.ResizeCheck();
        }

        /// <summary>
        /// Resizes the check.
        /// </summary>
        private void ResizeCheck()
        {
            WrapPanel wrapPanel = this.TagCloudItems.GetItemsHost() as WrapPanel;

            foreach (LocalizedTextBlock textBlock in this.localizedTextBlockList)
            {
                if (textBlock != null)
                {
                    double widthCheck = textBlock.ActualWidth + 30;

                    if (widthCheck > this.maxTextWidth)
                    {
                        this.maxTextWidth = widthCheck;
                        wrapPanel.Width = this.maxTextWidth;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Loaded event of the LocalizedTextBlock control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void LocalizedTextBlock_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            LocalizedTextBlock textBlock = sender as LocalizedTextBlock;
            this.localizedTextBlockList.Add(textBlock);

            if (textBlock != null)
            {
                double widthCheck = textBlock.ActualWidth + 30;

                if (widthCheck > this.maxTextWidth)
                {
                    this.maxTextWidth = widthCheck;

                    WrapPanel wrapPanel = this.TagCloudItems.GetItemsHost() as WrapPanel;
                    wrapPanel.Width = this.maxTextWidth;
                }
            }
        }
    }
}
