﻿// <copyright file="MapViews.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>11-06-2009</date>
// <summary>Partial class code-behind for the map views control</summary>

namespace AirWatch.UI
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.UICommon;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Partal class for MapViews control
    /// </summary>
    public partial class MapViews : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapViews"/> class.
        /// </summary>
        public MapViews()
        {
            this.InitializeComponent();

            this.MouseLeftButtonDown += new MouseButtonEventHandler(this.OnMouseLeftButtonDown);

            ClientServices.Instance.MainViewModel.MapMoved += new System.EventHandler(this.MainViewModelMapMoved);

            this.MapViewsButton.Checked += new RoutedEventHandler(this.MapViewsButton_Checked);
            this.MapViewsButton.Unchecked += new RoutedEventHandler(this.MapViewsButton_Checked);

            this.AirHeatMap.Checked += new RoutedEventHandler(this.MapViewChecked);
            this.StationData.Checked += new RoutedEventHandler(this.StationDataChecked);
            this.UserFeedback.Checked += new RoutedEventHandler(this.UserFeedbackChecked);

            this.AirHeatMap.Unchecked += new RoutedEventHandler(this.MapViewUnchecked);
            this.StationData.Unchecked += new RoutedEventHandler(this.StationDataUnchecked);
            this.UserFeedback.Unchecked += new RoutedEventHandler(this.UserFeedbackUnchecked);

            this.StationDataAll.Checked += new RoutedEventHandler(this.MapViewChecked);
            this.StationDataAir.Checked += new RoutedEventHandler(this.MapViewChecked);
            this.StationDataWater.Checked += new RoutedEventHandler(this.MapViewChecked);
            this.UserFeedbackAll.Checked += new RoutedEventHandler(this.MapViewChecked);
            this.UserFeedbackAir.Checked += new RoutedEventHandler(this.MapViewChecked);
            this.UserFeedbackWater.Checked += new RoutedEventHandler(this.MapViewChecked);
        }

        /// <summary>
        /// Mouses the left button up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Handles the MapMoved event of the MainViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MainViewModelMapMoved(object sender, System.EventArgs e)
        {
            this.Popup.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles the Checked event of the MapView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MapViewsButton_Checked(object sender, RoutedEventArgs e)
        {
            if (this.Popup.Visibility == Visibility.Visible)
            {
                this.Popup.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.Popup.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Handles the Checked event of the MapView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MapViewChecked(object sender, RoutedEventArgs e)
        {
            this.UpdateViews();
        }

        /// <summary>
        /// Handles the Unchecked event of the MapView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MapViewUnchecked(object sender, RoutedEventArgs e)
        {
            this.UpdateViews();
        }

        /// <summary>
        /// Handles the Checked event of the UserFeedback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserFeedbackChecked(object sender, RoutedEventArgs e)
        {
            if (this.UserFeedbackAir.IsChecked == false && this.UserFeedbackWater.IsChecked == false)
            {
                this.UserFeedbackAll.IsChecked = true;
            }
        }

        /// <summary>
        /// Handles the Checked event of the StationData control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void StationDataChecked(object sender, RoutedEventArgs e)
        {
            if (this.StationDataAir.IsChecked == false && this.StationDataWater.IsChecked == false)
            {
                this.StationDataAll.IsChecked = true;
            }
        }

        /// <summary>
        /// Handles the Unchecked event of the UserFeedback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserFeedbackUnchecked(object sender, RoutedEventArgs e)
        {
            this.UserFeedbackAll.IsChecked = false;
            this.UserFeedbackAir.IsChecked = false;
            this.UserFeedbackWater.IsChecked = false;
            this.UpdateViews();
        }

        /// <summary>
        /// Handles the Unchecked event of the StationData control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void StationDataUnchecked(object sender, RoutedEventArgs e)
        {
            this.StationDataAll.IsChecked = false;
            this.StationDataAir.IsChecked = false;
            this.StationDataWater.IsChecked = false;
            this.UpdateViews();
        }

        /// <summary>
        /// Updates the views.
        /// </summary>
        private void UpdateViews()
        {
            foreach (TileLayer tileLayer in ClientServices.Instance.MapViewModel.TileLayerCollection)
            {
                if (tileLayer.Visible == true)
                {
                    tileLayer.Visible = false;
                }
            }

            ClientServices.Instance.MapViewModel.AirModelKeyVisible = false;
            ClientServices.Instance.MapViewModel.UserFeedbackKeyVisible = false;
            ClientServices.Instance.MapViewModel.StationKeyVisible = false;
            ClientServices.Instance.MapViewModel.StationsVisible = false;
            ClientServices.Instance.MapViewModel.AirStationsVisible = false;
            ClientServices.Instance.MapViewModel.WaterStationsVisible = false;

            if (this.AirHeatMap.IsChecked == true)
            {
                ClientServices.Instance.MapViewModel.TileLayerCollection[0].Visible = true;
                ClientServices.Instance.MapViewModel.AirModelKeyVisible = true;
            }

            if (this.StationDataAll.IsChecked == true)
            {
                this.StationData.IsChecked = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[1].Visible = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[2].Visible = true;
                ClientServices.Instance.MapViewModel.StationKeyVisible = true;
                ClientServices.Instance.MapViewModel.StationsVisible = true;
                ClientServices.Instance.MapViewModel.AirStationsVisible = true;
                ClientServices.Instance.MapViewModel.WaterStationsVisible = true;
            }

            if (this.StationDataAir.IsChecked == true)
            {
                this.StationData.IsChecked = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[1].Visible = true;
                ClientServices.Instance.MapViewModel.StationKeyVisible = true;
                ClientServices.Instance.MapViewModel.StationsVisible = true;
                ClientServices.Instance.MapViewModel.AirStationsVisible = true;
            }

            if (this.StationDataWater.IsChecked == true)
            {
                this.StationData.IsChecked = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[2].Visible = true;
                ClientServices.Instance.MapViewModel.StationKeyVisible = true;
                ClientServices.Instance.MapViewModel.StationsVisible = true;
                ClientServices.Instance.MapViewModel.WaterStationsVisible = true;
            }

            if (this.UserFeedbackAll.IsChecked == true)
            {
                this.UserFeedback.IsChecked = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[3].Visible = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[4].Visible = true;
                ClientServices.Instance.MapViewModel.UserFeedbackKeyVisible = true;
            }

            if (this.UserFeedbackAir.IsChecked == true)
            {
                this.UserFeedback.IsChecked = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[3].Visible = true;
                ClientServices.Instance.MapViewModel.UserFeedbackKeyVisible = true;
            }

            if (this.UserFeedbackWater.IsChecked == true)
            {
                this.UserFeedback.IsChecked = true;
                ClientServices.Instance.MapViewModel.TileLayerCollection[4].Visible = true;
                ClientServices.Instance.MapViewModel.UserFeedbackKeyVisible = true;
            }

            ClientServices.Instance.MapViewModel.TileLayerChanged();
        }
    }
}
