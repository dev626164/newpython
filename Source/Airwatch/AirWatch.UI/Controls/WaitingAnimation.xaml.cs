﻿// <copyright file="WaitingAnimation.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>24-08-2009</date>
// <summary>WaitingAnimation code-behind.</summary>

namespace AirWatch.UI
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// WaitingAnimation code-behind
    /// </summary>
    public partial class WaitingAnimation : UserControl
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for AnimationVisibility.
        /// </summary>
        public static readonly DependencyProperty AnimationVisibilityProperty =
            DependencyProperty.Register("AnimationVisibility", typeof(Visibility), typeof(WaitingAnimation), new PropertyMetadata(Visibility.Collapsed, new PropertyChangedCallback(VisibilityPropertyChanged)));

        /// <summary>
        /// Using a DependencyProperty as the backing store for TextVisibility.
        /// </summary>
        public static readonly DependencyProperty TextVisibilityProperty =
            DependencyProperty.Register("TextVisibility", typeof(Visibility), typeof(WaitingAnimation), new PropertyMetadata(Visibility.Visible, new PropertyChangedCallback(TextPropertyChanged)));

        /// <summary>
        /// Initializes a new instance of the <see cref="WaitingAnimation"/> class.
        /// </summary>
        public WaitingAnimation()
        {
            this.InitializeComponent();
            this.Visibility = Visibility.Collapsed;
            this.AnimationStoryboard.Completed += new System.EventHandler(this.AnimationStoryboardCompleted);
        }

        /// <summary>
        /// Gets or sets the animation visibility.
        /// </summary>
        /// <value>The animation visibility.</value>
        public Visibility AnimationVisibility
        {
            get { return (Visibility)GetValue(AnimationVisibilityProperty); }
            set { SetValue(AnimationVisibilityProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text visibility.
        /// </summary>
        /// <value>The text visibility.</value>
        public Visibility TextVisibility
        {
            get { return (Visibility)GetValue(TextVisibilityProperty); }
            set { SetValue(TextVisibilityProperty, value); }
        }

        /// <summary>
        /// Visibilities the property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void VisibilityPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            WaitingAnimation element = sender as WaitingAnimation;

            Visibility newVisibility = (Visibility) args.NewValue;

            if (newVisibility == Visibility.Visible)
            {
                element.Visibility = Visibility.Visible;
                element.AnimationStoryboard.Begin();
            }
            else
            {
                element.Visibility = Visibility.Collapsed;
                element.AnimationStoryboard.Stop();
            }
        }

        /// <summary>
        /// Texts the property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void TextPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            WaitingAnimation element = sender as WaitingAnimation;

            Visibility newVisibility = (Visibility)args.NewValue;

            if (newVisibility == Visibility.Visible)
            {
                element.AnimationText.Visibility = Visibility.Visible;
            }
            else
            {
                element.AnimationText.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Handles the Completed event of the AnimationStoryboard control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AnimationStoryboardCompleted(object sender, System.EventArgs e)
        {
            this.AnimationVisibility = Visibility.Collapsed;
        }
    }
}
