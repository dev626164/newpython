﻿// <copyright file="ResultsFlyOut.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>17-06-2009</date>
// <summary>Custom control for search results flyout panel.</summary>
namespace AirWatch.UI
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Custom control for search results flyout panel.
    /// </summary>
    public partial class ResultsFlyOut : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultsFlyOut"/> class.
        /// </summary>
        public ResultsFlyOut()
        {
            this.InitializeComponent();

            this.DataContext = ClientServices.Instance.SearchViewModel;

            this.CloseButton.Click += new RoutedEventHandler(this.CloseButtonClick);
            this.LocationSuggestionsListBox.MouseLeftButtonUp += new MouseButtonEventHandler(this.LocationSuggestionsListBoxMouseLeftButtonUp);
            this.SearchTextBox.KeyUp += new System.Windows.Input.KeyEventHandler(this.SearchTextBoxKeyUp);
            this.SearchButton.Click += new RoutedEventHandler(this.SearchButtonClick);
        }

        /// <summary>
        /// Locations the suggestions mouse left button up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void LocationSuggestionsListBoxMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int selectedIndex = this.LocationSuggestionsListBox.SelectedIndex;
            ClientServices.Instance.SearchViewModel.AddPushpin(selectedIndex);
            ClientServices.Instance.SearchViewModel.ResultsFlyOutVisible = false;
        }

        /// <summary>
        /// Handles the Click event of the CloseButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.SearchViewModel.ResultsFlyOutVisible = false;
        }

        /// <summary>
        /// Button to start search based on query string
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.SearchViewModel.GetSearchResults(this.SearchTextBox.Text.ToString());
            this.SearchTextBox.Text = String.Empty;
        }

        /// <summary>
        /// Enter key press to start search based on query
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">System.Windows.Input.KeyEventArgs e</param>
        private void SearchTextBoxKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ClientServices.Instance.SearchViewModel.GetSearchResults(this.SearchTextBox.Text.ToString());
                this.SearchTextBox.Text = String.Empty;
            }
        }
    }
}
