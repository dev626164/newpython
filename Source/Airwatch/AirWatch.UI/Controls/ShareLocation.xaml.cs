﻿// <copyright file="ShareLocation.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>21-09-2009</date>
// <summary>User control to contain sharing functions</summary>

namespace AirWatch.UI
{
    using System;
    using System.Text;
    using System.Windows;
    using System.Windows.Browser;
    using System.Windows.Controls;
    using Microsoft.AirWatch.UI.Controls;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// User control for sharing a location
    /// </summary>
    public partial class ShareLocation : UserControl
    {
        /// <summary>
        /// DependencyProperty as the backing store for Latitude. 
        /// </summary>
        public static readonly DependencyProperty LatitudeProperty =
            DependencyProperty.Register("Latitude", typeof(double), typeof(ShareLocation), new PropertyMetadata(0.0));

        /// <summary>
        /// DependencyProperty as the backing store for Longitude. 
        /// </summary>
        public static readonly DependencyProperty LongitudeProperty =
            DependencyProperty.Register("Longitude", typeof(double), typeof(ShareLocation), new PropertyMetadata(0.0));

        /// <summary>
        /// Using a DependencyProperty as the backing store for PinType.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty PinTypeProperty =
            DependencyProperty.Register("PinType", typeof(string), typeof(ShareLocation), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Initializes a new instance of the ShareLocation class
        /// </summary>
        public ShareLocation()
        {
            this.InitializeComponent();

            this.CancelButton.Click += new System.Windows.RoutedEventHandler(this.CancelButtonClick);
            this.FacebookImage.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.FacebookImageMouseLeftButtonUp);
            this.TwitterImage.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.TwitterImageMouseLeftButtonUp);
            this.LiveSpacesImage.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.LiveSpacesImageMouseLeftButtonUp);
        }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude
        {
            get { return (double)GetValue(LatitudeProperty); }
            set { SetValue(LatitudeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude
        {
            get { return (double)GetValue(LongitudeProperty); }
            set { SetValue(LongitudeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the type of the pin.
        /// </summary>
        /// <value>The type of the pin.</value>
        public string PinType
        {
            get { return (string)GetValue(PinTypeProperty); }
            set { SetValue(PinTypeProperty, value); }
        }

        /// <summary>
        /// Opens the browser.
        /// </summary>
        /// <param name="urlValue">The URL value.</param>
        /// <param name="parameters">The parameters.</param>
        public static void OpenBrowser(string urlValue, string parameters)
        {
            if (HtmlPage.BrowserInformation.UserAgent.Contains("Safari"))
            {
                HtmlElement anchor = HtmlPage.Document.GetElementById("externalAnchor");
                anchor.SetProperty("href", urlValue);
                HtmlElement button = HtmlPage.Document.GetElementById("externalButton");
                button.Invoke("click", null);
            }
            else
            {
                HtmlPage.Window.Navigate(new Uri(urlValue, UriKind.RelativeOrAbsolute), "_newWindow", parameters);
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the FacebookImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void FacebookImageMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            StringBuilder url = new StringBuilder();
            url.Append("http://www.facebook.com/sharer.php?u=http://");
            url.Append(HtmlPage.Document.DocumentUri.Host);
            url.Append("/Default.aspx?location=");
            url.Append(Math.Round(this.Longitude, 4));
            url.Append(";");
            url.Append(Math.Round(this.Latitude, 4));

            if (this.PinType == "Water" || this.PinType == "Air")
            {
                url.Append(";station");
            }

            url.Append("&t=View the latest air and water quality at EyeOnEarth...");

            ShareLocation.OpenBrowser(url.ToString(), "toolbar=0,menubar=0,resizable=1,scrollbars=0,top=100,left=100,width=600,height=350");

            ClientServices.Instance.MapViewModel.CancelFlyOut();
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the TwitterImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void TwitterImageMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            StringBuilder url = new StringBuilder();
            url.Append("http://twitter.com/home?status=View+the+latest+air+and+water+quality+at+EyeOnEarth:+http://");
            url.Append(HtmlPage.Document.DocumentUri.Host);
            url.Append("/Default.aspx?location=");
            url.Append(Math.Round(this.Longitude, 4));
            url.Append(";");
            url.Append(Math.Round(this.Latitude, 4));

            if (this.PinType == "Water" || this.PinType == "Air")
            {
                url.Append(";station");
            }

            ShareLocation.OpenBrowser(url.ToString(), "toolbar=0,menubar=0,resizable=1,scrollbars=1,top=100,left=100,width=800,height=600");

            ClientServices.Instance.MapViewModel.CancelFlyOut();
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the LiveSpacesImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void LiveSpacesImageMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            StringBuilder url = new StringBuilder();
            url.Append("http://spaces.live.com/blogit.aspx?title=the+latest+air+and+water+quality+at+EyeOnEarth&sourceurl=http://");
            url.Append(HtmlPage.Document.DocumentUri.Host);
            url.Append("/Default.aspx?location=");
            url.Append(Math.Round(this.Longitude, 4));
            url.Append(";");
            url.Append(Math.Round(this.Latitude, 4));

            if (this.PinType == "Water" || this.PinType == "Air")
            {
                url.Append(";station");
            }

            url.Append("&description=A+location+has+been+shared+using+EyeOnEarth,+from+the+European+Environment+Agency.");

            ShareLocation.OpenBrowser(url.ToString(), "toolbar=0,menubar=0,resizable=1,scrollbars=1,top=100,left=100,width=900,height=550");

            ClientServices.Instance.MapViewModel.CancelFlyOut();
        }

        /// <summary>
        /// Return to main flyout panel 
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e)</param>
        private void CancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ClientServices.Instance.MapViewModel.CancelFlyOut();
        }
    }
}
