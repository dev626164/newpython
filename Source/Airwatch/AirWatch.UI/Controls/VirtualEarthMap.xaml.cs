﻿// <copyright file="VirtualEarthMap.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>09-06-2009</date>
// <summary>Instance of the VE Map control</summary>
namespace AirWatch.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.UI.Controls;
    using Microsoft.AirWatch.UICommon;
    using Microsoft.AirWatch.ViewModel;
    using Microsoft.Maps.MapControl;

    /// <summary>
    /// Partial Class for the VirtualEarthMap
    /// </summary>
    public partial class VirtualEarthMap : UserControl
    {
        /// <summary>
        /// Constants for map boundary checking
        /// </summary>
        private const double LatitudeMax = 85;

        /// <summary>
        /// Constants for map boundary checking
        /// </summary>
        private const double LatitudeMin = -81;

        /// <summary>
        /// Check to see if push pin icon is moving/being dragged
        /// </summary>
        private bool newPushpinMoving;

        /// <summary>
        /// X and Y co-ordinates for initial position of push pin
        /// </summary>
        private double pushpinStartX, pushpinStartY;

        /// <summary>
        /// X and Y co-ordinates for most up-to-date position of push pin
        /// </summary>
        private double pushPinLastX, pushpinLastY, pushpinX, pushpinY;

        /// <summary>
        /// List of pushpins in UI
        /// </summary>
        private List<Microsoft.AirWatch.UI.Controls.Pushpin> pushpinList = new List<Microsoft.AirWatch.UI.Controls.Pushpin>();

        /// <summary>
        /// List of stations in UI
        /// </summary>
        private List<Station> stationList = new List<Station>();

        /// <summary>
        /// tracks buffer for getting stations
        /// </summary>
        private double[] lastLocation = new double[2];

        /// <summary>
        /// for tracking direction of zoom
        /// </summary>
        private double lastZoomLevel = double.MaxValue;

        /// <summary>
        /// bool for if mouse is currently handling a scroll event
        /// </summary>
        private bool isScrolling;

        /// <summary>
        /// bool for if we should be checking boundaries
        /// </summary>
        private bool checkMapBoundries;

        /// <summary>
        /// timer to start boundary checking (fix for Mac error)
        /// </summary>
        private System.Windows.Threading.DispatcherTimer checkMapBoundariesTimer;

        /// <summary>
        /// Initializes a new instance of the VirtualEarthMap class.
        /// </summary>
        public VirtualEarthMap()
        {
            this.InitializeComponent();

            this.Loaded += new RoutedEventHandler(this.VirtualEarthMapLoaded);
            this.DataContext = ClientServices.Instance.MapViewModel;

            this.checkMapBoundariesTimer = new System.Windows.Threading.DispatcherTimer();
            this.checkMapBoundariesTimer.Interval = new TimeSpan(2000);
            this.checkMapBoundariesTimer.Tick += new EventHandler(this.CheckMapBoundariesTimerTick);
            
            this.MapControl.NavigationVisibility = Visibility.Collapsed;
            this.MapControl.ViewChangeOnFrame += new EventHandler<MapEventArgs>(this.ViewChangeOnFrame);
            this.MapControl.ViewChangeEnd += new EventHandler<MapEventArgs>(this.MapControlViewChangeEnd);
            this.MapControl.Mode = StaticMapMode.Instance.AerialMode;
            this.MapControl.MouseWheel += new MouseWheelEventHandler(this.MapControlMouseWheel);
            this.MapControl.KeyPress += new EventHandler<MapKeyPressEventArgs>(this.MapControlKeyPress);
            this.MapControl.KeyDown += new KeyEventHandler(this.MapControlKeyDown);
            this.MapControl.KeyUp += new KeyEventHandler(this.MapControlKeyUp);
            this.MapControl.KeyHeld += new EventHandler<MapKeyHeldEventArgs>(this.MapControlKeyHeld);
            this.MapControl.MousePan += new EventHandler<MapMouseDragEventArgs>(this.MapControlMousePan);
            this.UserFeedbackMoreInfo.Click += new RoutedEventHandler(this.UserFeedbackMoreInfoClick);
            this.StationsMoreInfo.Click += new RoutedEventHandler(this.StationsMoreInfoClick);
            MapHelper.CurrentMap = this.MapControl;

            this.NavigationGrid.MouseLeftButtonUp += new MouseButtonEventHandler(this.MapNavigationGridMouseLeftButtonUp);
            this.MapNavigationBar.PushpinPickedUpEvent += new System.Windows.Input.MouseButtonEventHandler(this.MapNavigationBarPushpinPickedUpEvent);
            this.MapNavigationBar.ZoomInEvent += new EventHandler(this.MapNavigationBarZoomInEvent);
            this.MapNavigationBar.ZoomOutEvent += new EventHandler(this.MapNavigationBarZoomOutEvent);

            this.LayoutRoot.MouseMove += new System.Windows.Input.MouseEventHandler(this.LayoutRootMouseMove);
            this.LayoutRoot.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.LayoutRootMouseLeftButtonUp);

            this.MapPanControl.MapMove += new EventHandler(this.MapPanControlMapMove);

            ClientServices.Instance.MapViewModel.MapLocationChanged += new EventHandler(this.MapLocationChanged);
            ClientServices.Instance.MapViewModel.ClosePins += new EventHandler(this.ClosePins);
            ClientServices.Instance.MapViewModel.DragPushpinVisible = false;
            ClientServices.Instance.MapViewModel.OverlayChanged += new EventHandler<EventArgs>(this.OverlayChanged);
            ClientServices.Instance.MapViewModel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.MapViewModelPropertyChanged);
            ClientServices.Instance.MapViewModel.StationsUpdated += new EventHandler<EventArgs>(this.MapViewModelStationsUpdated);
            ClientServices.Instance.MapViewModel.PushpinsUpdated += new EventHandler<EventArgs>(this.MapViewModelPushpinsUpdated);
            ClientServices.Instance.MapViewModel.AddPushpinCanceled += new EventHandler<EventArgs>(this.MapViewModelAddPushpinCancelled);
        }

        #region Map Overlays

        /// <summary>
        /// Creates the CAQI layer.
        /// </summary>
        /// <param name="uri">The URI to get the Tile Layer from.</param>
        /// <returns>MapTileLayer for CAQI</returns>
        private static MapTileLayer CreateTileLayerFromUri(Uri uri)
        {
            // Create a Tile Layer that will display our custom Map Imagery Tiles 
            MapTileLayer customTileLayer = new MapTileLayer();

            // Define the Bounding Rectangle 
            // LocationRect boundingRect = new LocationRect(
            //    new Location(75, -40),
            //    new Location(30, 65));

            // Create a LocationRectTileSource 
            LocationRectTileSource customTileSource = new LocationRectTileSource();

            // Set the Uri for the custom Map Imagery Tiles 
            customTileSource.UriFormat = uri.ToString();

            // Set the Min and Max Zoom Levels that the imagery is to be visible within 
            customTileSource.ZoomRange = new Range<double>(1, 31);

            // The bounding rectangle area that the tile overaly is valid in. 
            // customTileSource.BoundingRectangle = boundingRect;

            // Add the Tile Source to the Tile Layer 
            customTileLayer.TileSources.Add(customTileSource);
            customTileLayer.TileHeight = 256;
            customTileLayer.TileWidth = 256;

            // Set the Tile Layer Opacity to a desired value 
            customTileLayer.Opacity = 0.7;

            return customTileLayer;
        }

        /// <summary>
        /// Handles the click event on UserfeedbackMoreInfo button
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserFeedbackMoreInfoClick(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MainViewModel.UserFeedbackGetMoreInfo();
        }

        /// <summary>
        /// Handles the click event on StationsMoreInfo button
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void StationsMoreInfoClick(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MainViewModel.StationsGetMoreInfo();
        }

        #endregion

        #region Class methods

        /// <summary>
        /// Handles the Loaded event of the VirtualEarthMap control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void VirtualEarthMapLoaded(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MapViewModel.LoadDefaults();
            this.MapControl.Focus();
            this.checkMapBoundariesTimer.Start();
        }

        /// <summary>
        /// Handles the Tick event of the checkMapBoundariesTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void CheckMapBoundariesTimerTick(object sender, EventArgs e)
        {
            this.checkMapBoundries = true;
            this.checkMapBoundariesTimer.Stop();

            //// SMCCAR - FIX FOR MAC LIGHT MAPS
            ClientServices.Instance.MapViewModel.MapZoomLevel--;
        }

        #endregion

        #region Controlling Map

        /// <summary>
        /// Handles the PropertyChanged event of the mapViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void MapViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "MapMode":
                    {
                        if (ClientServices.Instance.MapViewModel.MapMode == StaticMapMode.Instance.RoadMode && this.MapControl.Mode != StaticMapMode.Instance.RoadMode)
                        {
                            this.MapControl.Mode = StaticMapMode.Instance.RoadMode;
                        }
                        else if (ClientServices.Instance.MapViewModel.MapMode == StaticMapMode.Instance.AerialMode && this.MapControl.Mode != StaticMapMode.Instance.AerialMode)
                        {
                            this.MapControl.Mode = StaticMapMode.Instance.AerialMode;
                        }
                        else if (ClientServices.Instance.MapViewModel.MapMode == StaticMapMode.Instance.AerialWithLabelsMode && this.MapControl.Mode != StaticMapMode.Instance.AerialWithLabelsMode)
                        {
                            this.MapControl.Mode = StaticMapMode.Instance.AerialWithLabelsMode;
                        }

                        this.UpdateMaxZoomFromMap();
                        break;
                    }

                case "WaterStationsVisible":
                    {
                        this.UpdateStations(true);
                        break;
                    }

                case "AirStationsVisible":
                    {
                        this.UpdateStations(true);
                        break;
                    }
            }
        }

        /// <summary>
        /// Handles the StationsUpdated event of the MapViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapViewModelStationsUpdated(object sender, EventArgs e)
        {
            this.UpdateLayers();
        }

        /// <summary>
        /// Handles the PushpinsUpdated event of the MapViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapViewModelPushpinsUpdated(object sender, EventArgs e)
        {
            this.UpdateLayers();
        }

        /// <summary>
        /// Updates the layers.
        /// </summary>
        private void UpdateLayers()
        {
            MapLayer mapLayer = this.PushpinItemsControl.GetItemsHost() as MapLayer;

            if (mapLayer != null && mapLayer.Children.Count > 0)
            {
                foreach (ContentPresenter contentPresenter in mapLayer.Children)
                {
                    contentPresenter.MouseEnter += new MouseEventHandler(this.PushpinMouseEnter);
                    Canvas.SetZIndex(contentPresenter, 0);
                }
            }

            mapLayer = this.StationItemsControl.GetItemsHost() as MapLayer;

            if (mapLayer != null && mapLayer.Children.Count > 0)
            {
                foreach (ContentPresenter contentPresenter in mapLayer.Children)
                {
                    contentPresenter.MouseEnter += new MouseEventHandler(this.StationMouseEnter);
                    Canvas.SetZIndex(contentPresenter, 0);
                }
            }
        }

        /// <summary>
        /// Handles the ZoomOutEvent event of the MapNavigationBar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapNavigationBarZoomOutEvent(object sender, EventArgs e)
        {
            this.MapControl.ZoomLevel--; 
        }

        /// <summary>
        /// Handles the ZoomInEvent event of the MapNavigationBar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapNavigationBarZoomInEvent(object sender, EventArgs e)
        {
            this.MapControl.ZoomLevel++;
        }

        /// <summary>
        /// Handles the MousePan event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Maps.MapControl.MapMouseDragEventArgs"/> instance containing the event data.</param>
        private void MapControlMousePan(object sender, MapMouseDragEventArgs e)
        {
            if (this.MapBoundaryCheck() == true)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// checks boundaries
        /// </summary>
        /// <returns>bool indicating if a boundary check failed</returns>
        private bool MapBoundaryCheck()
        {
            if (this.checkMapBoundries == true && this.MapControl.ZoomLevel >= ClientServices.Instance.MapViewModel.MapZoomMin)
            {
                Location topLeft = MapHelper.ControlPixelXYToLatLong(0, 0);
                Location bottomRight = MapHelper.ControlPixelXYToLatLong(this.ActualWidth, this.ActualHeight);

                if (topLeft.Latitude > VirtualEarthMap.LatitudeMax)
                {
                    double[] boundary = MapHelper.LatLongToMapPixelXY(VirtualEarthMap.LatitudeMax, this.MapControl.Center.Longitude);
                    boundary[1] += (this.MapControl.ActualHeight / 2) + 10;
                    Location newCenter = MapHelper.MapPixelXYToLatLong(boundary[0], boundary[1]);
                    ClientServices.Instance.MapViewModel.MoveMap(newCenter.Latitude, newCenter.Longitude);

                    return true;
                }
                else if (bottomRight.Latitude < VirtualEarthMap.LatitudeMin)
                {
                    double[] boundary = MapHelper.LatLongToMapPixelXY(VirtualEarthMap.LatitudeMin, this.MapControl.Center.Longitude);
                    boundary[1] -= (this.MapControl.ActualHeight / 2) + 10;
                    Location newCenter = MapHelper.MapPixelXYToLatLong(boundary[0], boundary[1]);
                    ClientServices.Instance.MapViewModel.MoveMap(newCenter.Latitude, newCenter.Longitude);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Handles the MouseWheel event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseWheelEventArgs"/> instance containing the event data.</param>
        private void MapControlMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ////if (this.isScrolling && ((this.MapControl.ZoomLevel >= ClientServices.Instance.MapViewModel.MapZoomMax - 1 && e.Delta > 0) || (this.MapControl.ZoomLevel <= ClientServices.Instance.MapViewModel.MapZoomMin + 1 && e.Delta < 0)))
            if (this.isScrolling && (this.MapControl.ZoomLevel >= ClientServices.Instance.MapViewModel.MapZoomMax - 1 || this.MapControl.ZoomLevel <= ClientServices.Instance.MapViewModel.MapZoomMin + 1))
            {
                e.Handled = true;
            }
            else
            {
                if (e.Delta > 240)
                {
                    e.Handled = true;
                }
                else if (e.Delta > 120 && this.MapControl.ZoomLevel >= ClientServices.Instance.MapViewModel.MapZoomMax - 1)
                {
                    e.Handled = true;
                }
                else if (e.Delta > 0 && this.MapControl.ZoomLevel >= ClientServices.Instance.MapViewModel.MapZoomMax)
                {
                    e.Handled = true;
                }
                else if (e.Delta < -240)
                {
                    e.Handled = true;
                }
                else if (e.Delta < -120 && this.MapControl.ZoomLevel <= ClientServices.Instance.MapViewModel.MapZoomMin + 1)
                {
                    e.Handled = true;
                }
                else if (e.Delta < 0 && this.MapControl.ZoomLevel <= ClientServices.Instance.MapViewModel.MapZoomMin)
                {
                    e.Handled = true;
                }
                else
                {
                    this.isScrolling = true;
                }
            }
        }

        /// <summary>
        /// Handles the KeyPress event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Maps.MapControl.MapKeyPressEventArgs"/> instance containing the event data.</param>
        private void MapControlKeyPress(object sender, MapKeyPressEventArgs e)
        {
            if (e.KeyPressed == Key.Subtract)
            {
                e.Handled = true;
                if (this.MapControl.ZoomLevel - 1 >= ClientServices.Instance.MapViewModel.MapZoomMin)
                {
                    this.MapControl.ZoomLevel--;
                }
            }
            else if (e.KeyPressed == Key.Add)
            {
                e.Handled = true;
                if (this.MapControl.ZoomLevel + 1 <= ClientServices.Instance.MapViewModel.MapZoomMax)
                {
                    this.MapControl.ZoomLevel++;
                }
            }
        }

        /// <summary>
        /// Handles the KeyHeld event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Maps.MapControl.MapKeyHeldEventArgs"/> instance containing the event data.</param>
        private void MapControlKeyHeld(object sender, MapKeyHeldEventArgs e)
        {
            if (e.KeysHeld.Count > 0 && (e.KeysHeld.Contains(Key.Subtract) || e.KeysHeld.Contains(Key.Add)))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the KeyUp event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void MapControlKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Subtract || e.Key == Key.Add)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void MapControlKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Subtract || e.Key == Key.Add)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Pushpins the mouse enter.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void PushpinMouseEnter(object sender, MouseEventArgs e)
        {
            MapLayer mapLayer = this.PushpinItemsControl.GetItemsHost() as MapLayer;

            if (mapLayer != null && mapLayer.Children.Count > 0)
            {
                foreach (ContentPresenter contentPresenter in mapLayer.Children)
                {
                    Canvas.SetZIndex(contentPresenter, 0);
                }
            }

            ContentPresenter targetContentPresenter = sender as ContentPresenter;
            Canvas.SetZIndex(targetContentPresenter, 100);

            Canvas.SetZIndex(this.PushpinMapLayer, 100);
            Canvas.SetZIndex(this.StationMapLayer, 0);
        }

        /// <summary>
        /// Stations the mouse enter.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void StationMouseEnter(object sender, MouseEventArgs e)
        { 
            MapLayer mapLayer = this.StationItemsControl.GetItemsHost() as MapLayer;

            if (mapLayer != null && mapLayer.Children.Count > 0)
            {
                foreach (ContentPresenter contentPresenter in mapLayer.Children)
                {
                    Canvas.SetZIndex(contentPresenter, 0); 
                }
            }

            ContentPresenter targetContentPresenter = sender as ContentPresenter;
            Canvas.SetZIndex(targetContentPresenter, 100);

            Canvas.SetZIndex(this.PushpinMapLayer, 0);
            Canvas.SetZIndex(this.StationMapLayer, 100);
        }

        /// <summary>
        /// Handles the MapMove event of the MapPanControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapPanControlMapMove(object sender, EventArgs e)
        {
            // get the move ratio and multiply it by a the zoom level (so zoomed in will move less than zoomed out)
            MapPanEventArgs mapPanEventArgs = (MapPanEventArgs)e;

            double log = Math.Log10(ClientServices.Instance.MapViewModel.MapZoomLevel);
            double zoomRatio = 1 / Math.Pow(5, log);

            Location currentLocation = this.MapControl.Center;
            Location newLocation = new Location(currentLocation.Latitude - (mapPanEventArgs.YMoveRatio * zoomRatio), currentLocation.Longitude + (mapPanEventArgs.XMoveRatio * zoomRatio));

            if (newLocation.Latitude - 1 > LatitudeMin && newLocation.Latitude + 1 < LatitudeMax)
            {
                this.MapControl.Center = newLocation;
            }

            this.MapBoundaryCheck();
        }

        /// <summary>
        /// Handles the MapLocationChanged event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapLocationChanged(object sender, EventArgs e)
        {
            MapLocationEventArgs mapLocationEventArgs = e as MapLocationEventArgs;
            this.MapControl.Center = mapLocationEventArgs.Location;
        }

        /// <summary>
        /// Handles the ViewChangeOnFrame event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Maps.MapControl.MapEventArgs"/> instance containing the event data.</param>
        private void ViewChangeOnFrame(object sender, MapEventArgs e)
        {
            if (ClientServices.Instance.MapViewModel.LightMapLayerVisible == false && (int)this.MapControl.ZoomLevel <= ClientServices.Instance.MapViewModel.MapStationMaxZoomLevel && this.lastZoomLevel > this.MapControl.ZoomLevel)
            {
                ClientServices.Instance.MapViewModel.CloseAllPins();
                ClientServices.Instance.MapViewModel.LightMapLayerVisible = true;
                ClientServices.Instance.MapViewModel.StationLayerVisible = false;
            }

            this.lastZoomLevel = this.MapControl.ZoomLevel;
        }

        /// <summary>
        /// Handles the ViewChangeEnd event of the MapControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Maps.MapControl.MapEventArgs"/> instance containing the event data.</param>
        private void MapControlViewChangeEnd(object sender, MapEventArgs e)
        {
            this.MapBoundaryCheck();

            this.UpdateStations(false);

            if (this.MapControl.ZoomLevel != Math.Round(this.MapControl.ZoomLevel))
            {
                this.MapControl.ZoomLevel = Math.Round(this.MapControl.ZoomLevel);
            }

            if (this.MapControl.ZoomLevel <= 11)
            {
                ClientServices.Instance.MapViewModel.UserFeedbackUnavailable = false;
            }
            else
            {
                ClientServices.Instance.MapViewModel.UserFeedbackUnavailable = true;
            }

            this.UpdateMaxZoomFromMap();

            this.isScrolling = false;
        }

        /// <summary>
        /// Updates the stations.
        /// </summary>
        /// <param name="forceUpdate">if set to <c>true</c> [force update].</param>
        private void UpdateStations(bool forceUpdate)
        {
            if (this.MapControl.ZoomLevel >= ClientServices.Instance.MapViewModel.MapStationZoomLevel && ClientServices.Instance.MapViewModel.StationsVisible == true)
            {
                double buffer = ClientServices.Instance.MapViewModel.MapStationBuffer;

                double westPixel = -buffer;
                double northPixel = -buffer;

                double southPixel = this.MapControl.ActualHeight + buffer;
                double eastPixel = this.MapControl.ActualWidth + buffer;

                MapHelper.SetMapBoundaries(northPixel, southPixel, eastPixel, westPixel);

                double[] topLeftPixels = MapHelper.LatLongToMapPixelXY(MapHelper.TopLeftBoundary.Latitude, MapHelper.TopLeftBoundary.Longitude);

                if (this.lastLocation[0] == 0 || this.lastLocation[1] == 0 || forceUpdate == true)
                {
                    this.lastLocation = topLeftPixels;
                    MapViewModel.GetStationDescriptionsForMapBoundaries(MapHelper.TopLeftBoundary, MapHelper.BottomRightBoundary);
                } 
                else if (Math.Abs(topLeftPixels[1] - this.lastLocation[1]) > buffer || Math.Abs(topLeftPixels[0] - this.lastLocation[0]) > buffer)
                {
                    this.lastLocation = topLeftPixels;
                    MapViewModel.GetStationDescriptionsForMapBoundaries(MapHelper.TopLeftBoundary, MapHelper.BottomRightBoundary);
                }
            }
        }

        /// <summary>
        /// Updates the max zoom from map.
        /// </summary>
        private void UpdateMaxZoomFromMap()
        {
            Range<double> zoomRange = this.MapControl.Mode.ZoomRange;
            ClientServices.Instance.MapViewModel.MapZoomMax = zoomRange.To;
        }

        #endregion

        #region Adding Pushpins

        /// <summary>
        /// Handles user "picking up" a push pin from the map navigation bar
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseButtonEventArgs e</param>
        private void MapNavigationBarPushpinPickedUpEvent(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;

            ClientServices.Instance.MapViewModel.CloseAllPins();
            ClientServices.Instance.MapViewModel.DragPushpinVisible = true;

            this.pushpinStartX = e.GetPosition(LayoutRoot).X;
            this.pushpinStartY = e.GetPosition(LayoutRoot).Y;

            Canvas.SetLeft(this.PushPin, this.pushpinStartX);
            Canvas.SetTop(this.PushPin, this.pushpinStartY - this.PushPin.ActualHeight);

            this.newPushpinMoving = true;
        }

        /// <summary>
        /// Handles user dragging the push pin across the map
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseEventArgs e</param>
        private void LayoutRootMouseMove(object sender, MouseEventArgs e)
        {
            if (this.newPushpinMoving)
            {
                this.pushpinX = e.GetPosition(LayoutRoot).X;
                this.pushpinY = e.GetPosition(LayoutRoot).Y;

                this.pushPinLastX = this.pushpinX;
                this.pushpinLastY = this.pushpinY;

                Canvas.SetLeft(this.PushPin, this.pushPinLastX);
                Canvas.SetTop(this.PushPin, this.pushpinLastY - this.PushPin.ActualHeight);
            }
        }

        /// <summary>
        /// Handles user "releasing" the push pin to place on the map
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseButtonEventArgs e</param>
        private void LayoutRootMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.newPushpinMoving)
            {
                ClientServices.Instance.MapViewModel.DropPushpin(this.pushpinX + (this.PushPin.ActualWidth / 2), this.pushpinY);
                this.ResetPushPinDrag();
            }
        }

        /// <summary>
        /// Reset status of draggable push pin
        /// </summary>
        private void ResetPushPinDrag()
        {
            if (this.newPushpinMoving)
            {
                ClientServices.Instance.MapViewModel.DragPushpinVisible = false;
                this.Cursor = Cursors.Arrow;
                this.newPushpinMoving = false;
            }
        }

        /// <summary>
        /// Allow the user to "not" place a PushPin by releasing the mouse on the navigation
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseButtonEventArgs e</param>
        private void MapNavigationGridMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.ResetPushPinDrag();
        }

        /// <summary>
        /// Handles the AddPushpinCancelled event of the MapViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MapViewModelAddPushpinCancelled(object sender, EventArgs e)
        {
            this.ResetPushPinDrag();
        }

        #endregion

        #region Pushpins / Stations

       /// <summary>
        /// Closes all the Pin flyouts
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ClosePins(object sender, EventArgs e)
        {
            foreach (Microsoft.AirWatch.UI.Controls.Pushpin pushpin in this.pushpinList)
            {
                if (pushpin != null)
                {
                    pushpin.Hide();
                    pushpin.SetValue(Canvas.ZIndexProperty, 0);
                }
            }

            foreach (Station station in this.stationList)
            {
                if (station != null)
                {
                    station.Hide();
                    station.SetValue(Canvas.ZIndexProperty, 0);
                }
            }
        }

        /// <summary>
        /// Pushpins the loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is referenced in XAML")]
        private void PushpinLoaded(object sender, RoutedEventArgs e)
        {
            Microsoft.AirWatch.UI.Controls.Pushpin pushpin = sender as Microsoft.AirWatch.UI.Controls.Pushpin;
            this.pushpinList.Add(pushpin);
            this.UpdateLayers();
        }

        /// <summary>
        /// Stations the loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is referenced in XAML")]
        private void StationLoaded(object sender, RoutedEventArgs e)
        {
            Station station = sender as Station;
            this.stationList.Add(station);
            this.UpdateLayers();
        }

        /// <summary>
        /// Overlays the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OverlayChanged(object sender, EventArgs e)
        {
            this.HeatMapLayer.Children.Clear();
            this.UserFeedbackMapLayer.Children.Clear();
            this.StationLightMapLayer.Children.Clear();

            for (int i = 0; i < ClientServices.Instance.MapViewModel.TileLayerCollection.Count; i++)
            {
                TileLayer tileLayer = ClientServices.Instance.MapViewModel.TileLayerCollection[i];

                if (tileLayer.Visible)
                {
                    if (i == 0)
                    {
                        this.HeatMapLayer.Children.Add(VirtualEarthMap.CreateTileLayerFromUri(tileLayer.Uri));
                    }
                    else if (i == 1 || i == 2)
                    {
                        this.StationLightMapLayer.Children.Add(VirtualEarthMap.CreateTileLayerFromUri(tileLayer.Uri));
                    }
                    else
                    {
                        this.UserFeedbackMapLayer.Children.Add(VirtualEarthMap.CreateTileLayerFromUri(tileLayer.Uri));
                    }
                }
            }

            this.UpdateStations(true);
        }

        #endregion
    }
}
