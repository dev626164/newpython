﻿// <copyright file="SearchControl.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>11-06-2009</date>
// <summary>Search User Control.</summary>
namespace AirWatch.UI
{
    #region Using

    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Microsoft.AirWatch.ViewModel;

    #endregion

    /// <summary>
    /// Search User Control.
    /// </summary>
    public partial class SearchControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchControl"/> class.
        /// </summary>
        public SearchControl()
        {
            this.InitializeComponent();

            this.SearchTextBox.KeyUp += new KeyEventHandler(this.SearchTextBoxKeyUp);
            this.SearchButton.Click += new RoutedEventHandler(this.SearchButtonClick);
        }     

        /// <summary>
        /// Button to start search based on query string
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {   
            ClientServices.Instance.SearchViewModel.GetSearchResults(this.SearchTextBox.Text.ToString());            
        }

        /// <summary>
        /// Enter key press to start search based on query
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">System.Windows.Input.KeyEventArgs e</param>
        private void SearchTextBoxKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ClientServices.Instance.SearchViewModel.GetSearchResults(this.SearchTextBox.Text.ToString());
                this.SearchTextBox.Text = String.Empty;
            }
        }
    }
}
