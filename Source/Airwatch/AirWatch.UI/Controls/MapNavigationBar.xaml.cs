﻿// <copyright file="MapNavigationBar.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>11-06-2009</date>
// <summary>Custom navigation bar for VE Map</summary>
namespace AirWatch.UI
{
    #region Using

    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.ViewModel;

    #endregion
    
    /// <summary>
    /// Custom navigation bar for VE Map
    /// </summary>
    public partial class MapNavigationBar : UserControl
    {
        /// <summary>
        /// Remembers the setting of labels checkbox
        /// </summary>
        private bool? labelsSetting = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapNavigationBar"/> class.
        /// </summary>
        public MapNavigationBar()
        {
            this.InitializeComponent();

            this.DataContext = ClientServices.Instance.MapViewModel;

            this.RoadButton.Checked += new RoutedEventHandler(this.RoadRadioButtonChecked);
            this.AerialButton.Checked += new RoutedEventHandler(this.AerialRadioButtonChecked);
            this.LabelsButton.Click += new RoutedEventHandler(this.LabelsButtonClick);

            this.ZoomInButton.Click += new RoutedEventHandler(this.ZoomInClick);
            this.ZoomOutButton.Click += new RoutedEventHandler(this.ZoomOutClick);
            this.ZoomSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(this.ZoomSliderValueChanged);

            this.Pushpin.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.PushpinMouseLeftButtonDown);
            this.DeleteAllPushpins.Click += new RoutedEventHandler(this.DeleteAllPushpinsClick);

            this.RoadButton.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.AerialButton.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.LabelsButton.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.ZoomInButton.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.ZoomOutButton.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.ZoomSlider.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.Pushpin.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);
            this.DeleteAllPushpins.MouseLeftButtonUp += new MouseButtonEventHandler(this.NavigationMouseLeftButtonUp);

            ClientServices.Instance.MapViewModel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.MapViewModelPropertyChanged);
            ClientServices.Instance.MainViewModel.MapMoved += new EventHandler(this.MainViewModelMapMoved);
        }

        /// <summary>
        /// Occurs when a push pin is "picked up" (mouse down)
        /// </summary>
        public event MouseButtonEventHandler PushpinPickedUpEvent;

        /// <summary>
        /// Occurs when [zoom in event].
        /// </summary>
        public event EventHandler ZoomInEvent;

        /// <summary>
        /// Occurs when [zoom out event].
        /// </summary>
        public event EventHandler ZoomOutEvent;

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void NavigationMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ClientServices.Instance.MapViewModel.AddPushpinCancel(); 
        }

        /// <summary>
        /// Handles the MapMoved event of the MapViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.EventArgs"/> instance containing the event data.</param>
        private void MainViewModelMapMoved(object sender, EventArgs e)
        {
            this.PushPinToolTipFadeOutQuick.Begin();
        }

        /// <summary>
        /// Handles the PropertyChanged event of the MapViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void MapViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "MapZoomMin" || e.PropertyName == "MapZoomMax")
            {
                this.UpdateSliderButtons();
            }
        }

        /// <summary>
        /// Handles the ValueChanged event of the ZoomSlider control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Double&gt;"/> instance containing the event data.</param>
        private void ZoomSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.UpdateSliderButtons();
        }

        /// <summary>
        /// Updates the slider buttons.
        /// </summary>
        private void UpdateSliderButtons()
        {
            if ((int)this.ZoomSlider.Value <= (int)ClientServices.Instance.MapViewModel.MapZoomMin)
            {
                this.ZoomOutButton.IsEnabled = false;
            }
            else
            {
                this.ZoomOutButton.IsEnabled = true;
            }

            if (Math.Round(this.ZoomSlider.Value, 0) >= (int)ClientServices.Instance.MapViewModel.MapZoomMax)
            {
                this.ZoomInButton.IsEnabled = false;
            }
            else
            {
                this.ZoomInButton.IsEnabled = true;
            }
        }

        /// <summary>
        /// Handles user "picking up" the push pin by fireing custom event
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">MouseButtonEventArgs e</param>
        private void PushpinMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.PushpinPickedUpEvent != null)
            {
                this.PushpinPickedUpEvent(this, e);
            }
        }

        /// <summary>
        /// Handles the Click event of the DeleteAllPushpins control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteAllPushpinsClick(object sender, RoutedEventArgs e)
        {
            ClientServices.Instance.MapViewModel.DeleteAllPushpins();
        }

        /// <summary>
        /// Handles the Click event of the LabelsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void LabelsButtonClick(object sender, RoutedEventArgs e)
        {
            this.UpdateMapMode();
        }

        /// <summary>
        /// Handles the Checked event of the AerialRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void AerialRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.LabelsButton.IsChecked = this.labelsSetting;
            this.LabelsButton.IsEnabled = true;
            this.UpdateMapMode();
        }

        /// <summary>
        /// Handles the Checked event of the RoadRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RoadRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.labelsSetting = this.LabelsButton.IsChecked;
            this.LabelsButton.IsChecked = true;
            this.LabelsButton.IsEnabled = false;
            this.UpdateMapMode();
        }

        /// <summary>
        /// Updates the map mode.
        /// </summary>
        private void UpdateMapMode()
        {
            if (this.AerialButton.IsChecked == true)
            {
                if (this.LabelsButton.IsChecked == true)
                {
                    ClientServices.Instance.MapViewModel.MapMode = StaticMapMode.Instance.AerialWithLabelsMode;
                }
                else
                {
                    ClientServices.Instance.MapViewModel.MapMode = StaticMapMode.Instance.AerialMode;
                }
            }
            else if (this.RoadButton.IsChecked == true)
            {
                ClientServices.Instance.MapViewModel.MapMode = StaticMapMode.Instance.RoadMode;
            }
        }

        /// <summary>
        /// Decrease the value of the zoom slider (zoom out)
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void ZoomOutClick(object sender, RoutedEventArgs e)
        {
            if (this.ZoomOutEvent != null)
            {
                this.ZoomOutEvent.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Increase the value of the zoom slider (zoom in)
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void ZoomInClick(object sender, RoutedEventArgs e)
        {
            if (this.ZoomInEvent != null)
            {
                this.ZoomInEvent.Invoke(this, new EventArgs());
            }
        }
    }
}
