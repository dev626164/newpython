﻿// <copyright file="GadgetDetails.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>11-06-2009</date>
// <summary>Partial class code-behind for the flyout panel.</summary>

namespace AirWatch.UI
{
    using System.Windows.Browser;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Partial class code-behind for the flyout panel
    /// </summary>
    public partial class GadgetDetails : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GadgetDetails"/> class.
        /// </summary>
        public GadgetDetails()
        {
            this.InitializeComponent();

            this.DataContext = ClientServices.Instance.FlyOutViewModel;

            this.MouseLeftButtonUp += new MouseButtonEventHandler(this.FlyOutPanelMouseLeftButtonUp);
            ClientServices.Instance.FlyOutViewModel.SingleResultHomeLocationChangeEvent += new System.EventHandler(this.FlyOutViewModel_SingleResultHomeLocationChangeEvent);
                       
            if (HtmlPage.Document.QueryString.ContainsKey("location"))
            {
                ClientServices.Instance.FlyOutViewModel.GetSearchResults(HtmlPage.Document.QueryString["location"]);
            }
            else
            {
                ClientServices.Instance.FlyOutViewModel.GetSearchResults("Copengagen, DK");
            }
        }

        /// <summary>
        /// Fires when we get the home location back.
        /// </summary>
        /// <param name="sender">The originating sender</param>
        /// <param name="e">The empty event args</param>
        private void FlyOutViewModel_SingleResultHomeLocationChangeEvent(object sender, System.EventArgs e)
        {
            if (HtmlPage.Document.QueryString.ContainsKey("header"))
            {
                ClientServices.Instance.FlyOutViewModel.HomeName = HtmlPage.Document.QueryString["header"];
            }
        }              

        /// <summary>
        /// Hides the panels.
        /// </summary>
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the FlyOutPanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void FlyOutPanelMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ClientServices.Instance.MapViewModel.AddPushpinCancel();
        }        
    }
}
