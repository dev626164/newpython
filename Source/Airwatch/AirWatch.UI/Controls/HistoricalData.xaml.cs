﻿// <copyright file="HistoricalData.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>13-07-2009</date>
// <summary>User control to contain Historical Data</summary>

namespace AirWatch.UI
{
    using System.ComponentModel;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// User control for Historical Data
    /// </summary>
    public partial class HistoricalData : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the HistoricalData class
        /// </summary>
        public HistoricalData()
        {
            this.InitializeComponent();

            this.CancelButton.Click += new System.Windows.RoutedEventHandler(this.CancelButtonClick);
        }

        /// <summary>
        /// Return to main flyout panel 
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e)</param>
        private void CancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ClientServices.Instance.MapViewModel.CancelFlyOut();
        }
    }
}
