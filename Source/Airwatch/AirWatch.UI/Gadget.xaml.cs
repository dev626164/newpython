﻿// <copyright file="Gadget.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>12-06-2009</date>
// <summary>Gadget container</summary>
namespace AirWatch.UI
{
    using System.Windows.Controls;
    
    /// <summary>
    /// Gadget container
    /// </summary>
    public partial class Gadget : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Gadget"/> class.
        /// </summary>
        public Gadget()
        {
            this.InitializeComponent();
        }
    }
}
