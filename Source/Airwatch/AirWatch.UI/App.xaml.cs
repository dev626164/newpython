﻿// <copyright file="App.xaml.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-04-2009</date>
// <summary>Partial class for the Silverlight Application.</summary>
namespace AirWatch.UI
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Browser;
    using Microsoft.AirWatch.UI.Controls;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Partial class for the Silverlight Application
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Backing field for user IP
        /// </summary>
        private string userAddress;   

        /// <summary>
        /// Initializes a new instance of the App class.
        /// </summary>
        public App()
        {
            this.Startup += this.Application_Startup;
            this.Exit += this.Application_Exit;
            this.UnhandledException += this.Application_UnhandledException;

            // Overwrite the service uri
            ////string serviceUri = "http://VEINTPLAT2.live-int.com/webservices/v1/MapControlConfigurationService/MapControlConfigurationService.svc/binaryHttp";
            ////Microsoft.Maps.MapControl.Core.MapConfiguration.SetServiceUri(new Uri(serviceUri));

            ClientServices.Instance.MainViewModel.LanguageReceived += new EventHandler(this.MainViewModel_LanguageReceived);
            LocalizedTextBlock.CurrentLanguageDictionary = ClientServices.Instance.MainViewModel.CurrentLanguageDictionary;

            this.InitializeComponent();
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged; 

        /// <summary>
        /// Gets or sets the user IP
        /// </summary>
        /// <value>The user address.</value>
        public string UserAddress
        {
            get
            {
                return this.userAddress;
            }

            set
            {
                this.userAddress = value;                             
            }
        }
        
        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }        

        /// <summary>
        /// Report an error to the DOM
        /// </summary>
        /// <param name="e">The event arguments of the unhandled exception</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031",
            Justification = "Without the exception handling, silverlight exceptions would break the application rather than prompting the user")]
        private static void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e)
        {
            try
            {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                System.Windows.Browser.HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg + "\");");
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Handles the LanguageReceived event of the MainViewModel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.ViewModel.LanguageDictionaryRetrievedEventArgs"/> instance containing the event data.</param>
        private void MainViewModel_LanguageReceived(object sender, EventArgs e)
        {
            LocalizedTextBlock.CurrentLanguageDictionary = ClientServices.Instance.MainViewModel.CurrentLanguageDictionary;      
        }

        /// <summary>
        /// Event handler for Application startup
        /// </summary>
        /// <param name="sender">The object which fired this event</param>
        /// <param name="e">The event arguments to be passed into this event</param>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (HtmlPage.Document.QueryString.ContainsKey("gadget"))
            {
                this.RootVisual = new Gadget();
            }
            else
            {
                this.RootVisual = new MainPage();
            }

            this.UserAddress = e.InitParams["ipAddress"];
        }

        /// <summary>
        /// Event handler for Application exit
        /// </summary>
        /// <param name="sender">The object which fired this event</param>
        /// <param name="e">The event arguments to be passed into this event</param>
        private void Application_Exit(object sender, EventArgs e)
        {
            Microsoft.AirWatch.ViewModel.ClientServices.Instance.MapViewModel.SavePushpinCollection();
            Microsoft.AirWatch.ViewModel.ClientServices.Instance.MapViewModel.SaveRatingCollection();
        }

        /// <summary>
        /// Event handler for an unhandled exception being thrown
        /// </summary>
        /// <param name="sender">The object which fired this event</param>
        /// <param name="e">The event arguments to be passed into this event</param>
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using
            // the browser's exception mechanism. On IE this will display it a yellow alert 
            // icon in the status bar and Firefox will display a script error.
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });
            }
        }
    }
}
