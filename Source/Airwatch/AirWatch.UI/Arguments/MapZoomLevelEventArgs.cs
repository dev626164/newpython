﻿// <copyright file="MapZoomLevelEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>16-06-2009</date>
// <summary>Custom event args to ascertain zoom level mode change</summary>
namespace AirWatch.UI
{
    using System;

    /// <summary>
    /// Custom event args for zoom level change events in MapNavigationBar.cs
    /// </summary>
    public class MapZoomLevelEventArgs : EventArgs
    {
        /// <summary>
        /// The zoom level of the map
        /// </summary>
        private double zoomLevel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapZoomLevelEventArgs"/> class.
        /// </summary>
        /// <param name="zoomLevel">The zoom level.</param>
        public MapZoomLevelEventArgs(double zoomLevel)
        {
            this.ZoomLevel = zoomLevel;
        }

        /// <summary>
        /// Gets or sets the zoom level.
        /// </summary>
        /// <value>The zoom level.</value>
        public double ZoomLevel
        {
            get { return this.zoomLevel; }
            set { this.zoomLevel = value; }
        }
    }
}
