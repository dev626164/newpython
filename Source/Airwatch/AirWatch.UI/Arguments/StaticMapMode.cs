﻿// <copyright file="StaticMapMode.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-06-2009</date>
// <summary>Static map modes</summary>

namespace AirWatch.UI
{
    using Microsoft.Maps.MapControl;

    /// <summary>
    /// Custom event args for mode change events in MapNavigationBar.cs
    /// </summary>
    public class StaticMapMode
    {
        /// <summary>
        /// The instance
        /// </summary>
        private static StaticMapMode instance;

        /// <summary>
        /// Single instance of the road map mode
        /// </summary>
        private RoadMode roadMode = new RoadMode();

        /// <summary>
        /// Single instance of the aerial map mode
        /// </summary>
        private AerialMode aerialMode = new AerialMode();

        /// <summary>
        /// Single instance of the aerial with labels map mode
        /// </summary>
        private AerialMode aerialWithLabelsMode = new AerialMode();

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticMapMode"/> class.
        /// </summary>
        public StaticMapMode()
        {
            this.aerialWithLabelsMode.Labels = true;
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static StaticMapMode Instance
        {
            get
            {
                if (StaticMapMode.instance == null)
                {
                    StaticMapMode.instance = new StaticMapMode();
                }

                return StaticMapMode.instance;
            }
        }

        /// <summary>
        /// Gets or sets the road mode.
        /// </summary>
        /// <value>The road mode.</value>
        public RoadMode RoadMode
        {
            get { return this.roadMode; }
            set { this.roadMode = value; }
        }

        /// <summary>
        /// Gets or sets the aerial mode.
        /// </summary>
        /// <value>The aerial mode.</value>
        public AerialMode AerialMode
        {
            get { return this.aerialMode; }
            set { this.aerialMode = value; }
        }

        /// <summary>
        /// Gets or sets the  aerial with labels mode.
        /// </summary>
        /// <value>The aerial with labels mode.</value>
        public AerialMode AerialWithLabelsMode
        {
            get { return this.aerialWithLabelsMode; }
            set { this.aerialWithLabelsMode = value; }
        }
    }
}
