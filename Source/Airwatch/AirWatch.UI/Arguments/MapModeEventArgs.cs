﻿// <copyright file="MapModeEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>16-06-2009</date>
// <summary>Custom event args to ascertain map mode change</summary>
namespace AirWatch.UI
{
    using System;
    using Microsoft.Maps.MapControl.Core;

    /// <summary>
    /// Custom event args for mode change events in MapNavigationBar.cs
    /// </summary>
    public class MapModeEventArgs : EventArgs
    {
        /// <summary>
        /// the current display mode of the map
        /// </summary>
        private MapMode mapMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapModeEventArgs"/> class.
        /// </summary>
        /// <param name="mapMode">The map mode.</param>
        public MapModeEventArgs(MapMode mapMode)
        {
            this.MapMode = mapMode;
        }

        /// <summary>
        /// Gets or sets the map mode.
        /// </summary>
        /// <value>The map mode.</value>
        public MapMode MapMode
        {
            get { return this.mapMode; }
            set { this.mapMode = value; }
        }
     }
}
