﻿// <copyright file="SearchStringEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>18-06-2009</date>
// <summary>Custom EventArgs for search string</summary>
namespace AirWatch.UI
{
    using System;

    /// <summary>
    /// Class for custom EventArgs for passing search string
    /// </summary>
    public class SearchStringEventArgs : EventArgs
    {
        /// <summary>
        /// String name for user search
        /// </summary>
        private string searchString;
        
        /// <summary>
        /// Initializes a new instance of the SearchStringEventArgs class
        /// </summary>
        /// <param name="value">string value</param>
        public SearchStringEventArgs(string value)
        {
            this.SearchString = value;
        }

        /// <summary>
        /// Gets or sets the user search string 
        /// </summary>
        public string SearchString
        {
            get
            {
                return this.searchString;
            }

            set
            {
                this.searchString = value;
            }
        }
    }
}
