﻿// <copyright file="DataLocationToMapLocationConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>07-07-2009</date>
// <summary>Converts Location to Map Location</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;
    using Microsoft.VirtualEarth.MapControl;

    /// <summary>
    /// Converts a boolean value to a Visibility
    /// </summary>
    public class DataLocationToMapLocationConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Microsoft.AirWatch.Core.Data.Location dataLocation = value as Microsoft.AirWatch.Core.Data.Location;

            if (dataLocation != null)
            {
                double latitude = (double) dataLocation.Latitude;
                double longitude = (double) dataLocation.Longitude;

                Microsoft.VirtualEarth.MapControl.Location velocation = new Microsoft.VirtualEarth.MapControl.Location();
                velocation.Latitude = latitude;
                velocation.Longitude = longitude;

                return velocation;
            }

            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
