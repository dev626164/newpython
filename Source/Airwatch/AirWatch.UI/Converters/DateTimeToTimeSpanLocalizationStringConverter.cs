﻿// <copyright file="DateTimeToTimeSpanLocalizationStringConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Converts a DateTime to the 'updated {0} Month ago' format. Once done, ElementName bind a normal textblock to the localizedTextBlock TranslatedTerm property and use the DateTimeToTimeSpanStringConverter to replace the format palceholder.</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Converts a DateTime to the 'updated {0} Month ago' format. 
    /// Once done, ElementName bind a normal textblock to the localizedTextBlock TranslatedTerm property and use the 
    /// DateTimeToTimeSpanStringConverter to replace the format palceholder.
    /// </summary>
    public class DateTimeToTimeSpanLocalizationStringConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type"/> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="culture">The culture of the conversion.</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string localizedString = string.Empty;

            DateTime? dateTime = value as DateTime?;

            if (dateTime == null || dateTime.HasValue == false)
            {
                localizedString = "COMMON_NODATE"; 
            }
            else
            {
                TimeSpan timeSpan = DateTime.UtcNow.Subtract((DateTime)dateTime);

                if (timeSpan.Days > 365)
                {
                    localizedString = "COMMON_YEARS";                    
                }
                else if (timeSpan.Days > 273)
                {
                    localizedString = "COMMON_YEAR";                    
                }
                else if (timeSpan.Days > 30)
                {
                    localizedString = "COMMON_MONTHS";                     
                }
                else if (timeSpan.Days > 15)
                {
                    localizedString = "COMMON_MONTH";
                }
                else if (timeSpan.Days >= 1)
                {
                   localizedString = "COMMON_DAYS";                      
                }
                else if (timeSpan.Hours > 12)
                {
                    localizedString = "COMMON_DAY";
                }
                else if (timeSpan.Hours >= 1)
                {
                    localizedString = "COMMON_HOURS";
                }
                else
                {
                    localizedString = "COMMON_HOUR";
                }
            }
            
            return localizedString;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay"/> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type"/> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="culture">The culture of the conversion.</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
