﻿// <copyright file="VisibilityIfPluralConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Converts to visibility if 0 or greater than 1.</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Converts a boolean value to a Visibility
    /// </summary>
    public class VisibilityIfPluralConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                return Visibility.Visible;
            }

            if (value == null)
            {
                return Visibility.Collapsed;
            }

            bool vis = true;
            int? number = value as int?;
            bool negate = false;

            if (parameter != null)
            {
                negate = bool.Parse(parameter.ToString());
            }

            if (number.HasValue)
            {
                if (number.Value == 1)
                {
                    vis = false;
                }
            }
            else
            {
                return Visibility.Collapsed;
            }

            if (negate == true)
            {
                vis = !vis;
            }

            return (bool)vis ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
