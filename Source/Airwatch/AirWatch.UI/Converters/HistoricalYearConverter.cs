﻿// <copyright file="HistoricalYearConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Filters historical data for year</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Data;
    using Microsoft.AirWatch.Core.Data;
    using AirwatchData = Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Filters historical data for year
    /// </summary>
    public class HistoricalYearConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Collection<AirwatchData.StationMeasurement> sourceCollection = value as Collection<AirwatchData.StationMeasurement>;
            Collection<AirwatchData.StationMeasurement> targetCollection = new Collection<AirwatchData.StationMeasurement>();

            int sourceCount = 0;

            if (sourceCollection != null)
            {
                sourceCount = sourceCollection.Count;
            }

            int startYear = DateTime.Now.Year - 1;

            if (sourceCount > 0 && sourceCollection[sourceCount - 1].Measurement.Timestamp.Value.Year == startYear)
            {
                // if the latest reading is other than 31/12 or if the latest is 31/12 but there is another from same year
                if (sourceCollection[sourceCount - 1].Measurement.Timestamp.Value.Day != 31
                && sourceCollection[sourceCount - 1].Measurement.Timestamp.Value.Month != 12)
                {
                    startYear--;
                } 
                else if (sourceCount > 1 && sourceCollection[sourceCount - 2].Measurement.Timestamp.Value.Year == startYear)
                {
                    startYear--;
                }
            }

            for (int currentYear = startYear; currentYear >= 1990; currentYear--)
            {
                bool found = false;

                if (sourceCollection != null)
                {
                    for (int i = 0; i < sourceCount; i++)
                    {
                        if (sourceCollection[i].Measurement.Timestamp.HasValue && sourceCollection[i].Measurement.Timestamp.Value.Year == currentYear)
                        {
                            targetCollection.Add(sourceCollection[i]);
                            found = true;
                            break;
                        }
                    }
                }

                if (found == false)
                {
                    targetCollection.Add(new AirwatchData.StationMeasurement
                    {
                        Measurement = new AirwatchData.Measurement
                        {
                            Timestamp = new DateTime?(new DateTime(currentYear, 12, 31))
                        },
                        AggregatedUserRatingCount = 0
                    });
                }
            }

            return targetCollection;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
