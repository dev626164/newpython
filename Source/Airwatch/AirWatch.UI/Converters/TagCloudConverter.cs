﻿// <copyright file="TagCloudConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>20-10-2009</date>
// <summary>Reorders so biggest tag words in center</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Data;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Converts a boolean value to a Visibility
    /// </summary>
    public class TagCloudConverter : IValueConverter
    {      
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Collection<RatingQualifierIdentifiers> ratings = value as Collection<RatingQualifierIdentifiers>;

            if (ratings == null)
            {
                return value;
            }

            Collection<RatingQualifierIdentifiers> newRatings = new Collection<RatingQualifierIdentifiers>();
            
            while (ratings.Count > 0)
            {
                double maxValue = ratings[0].Percentage.Value;
                int maxIndex = 0;
                
                for (int i = 1; i < ratings.Count; i++)
                {
                    if (ratings[i].Percentage > maxValue)
                    {
                        maxValue = ratings[i].Percentage.Value;
                        maxIndex = i;
                    }
                }

                if (ratings.Count % 2 == 0)
                {
                    newRatings.Add(ratings[maxIndex]);
                }
                else
                {
                    newRatings.Insert(0, ratings[maxIndex]);
                }

                ratings.RemoveAt(maxIndex);
            }

            return newRatings;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
