﻿// <copyright file="RatingToBrushConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-07-2009</date>
// <summary>Converts agency rating to brush of certian color</summary>

namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Convertion class for rating to brush
    /// </summary>
    public class RatingToBrushConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts rating to brush
        /// </summary>
        /// <param name="value">Agency rating</param>
        /// <param name="targetType">Type targetType</param>
        /// <param name="parameter">object parameter</param>
        /// <param name="culture">CultureInfo culture</param>
        /// <returns>brush with appropriate rating color</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            LinearGradientBrush brush = new LinearGradientBrush();
            brush.StartPoint = new Point(0.5, 0);
            brush.EndPoint = new Point(0.5, 1);

            decimal decimalValue;
            
            if (value != null && decimal.TryParse(value.ToString(), out decimalValue))
            {
                int rating = decimal.ToInt32(decimalValue);

                switch (rating)
                {
                    case 1:
                        GradientStop oneBottomGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 18, 80, 0),
                            Offset = 1
                        };
                        GradientStop oneTopGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 91, 219, 1),
                            Offset = 0.0
                        };

                        brush.GradientStops.Add(oneTopGS);
                        brush.GradientStops.Add(oneBottomGS);
                        break;
                    case 2:
                        GradientStop twoBottomGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 37, 190, 0),
                            Offset = 1
                        };
                        GradientStop twoTopGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 175, 233, 9),
                            Offset = 0.0
                        };

                        brush.GradientStops.Add(twoTopGS);
                        brush.GradientStops.Add(twoBottomGS);
                        break;
                    case 3:
                        GradientStop threeBottomGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 201, 202, 5),
                            Offset = 1
                        };
                        GradientStop threeTopGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 254, 255, 3),
                            Offset = 0.0
                        };

                        brush.GradientStops.Add(threeTopGS);
                        brush.GradientStops.Add(threeBottomGS);
                        break;
                    case 4:
                        GradientStop fourBottomGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 180, 90, 0),

                            Offset = 1
                        };
                        GradientStop fourTopGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 254, 150, 45),
                            Offset = 0.0
                        };

                        brush.GradientStops.Add(fourTopGS);
                        brush.GradientStops.Add(fourBottomGS);
                        break;
                    case 5:
                        GradientStop fiveBottomGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 129, 0, 0),
                            Offset = 1
                        };
                        GradientStop fiveTopGS = new GradientStop()
                        {
                            Color = Color.FromArgb(255, 255, 4, 4),
                            Offset = 0.0
                        };

                        brush.GradientStops.Add(fiveTopGS);
                        brush.GradientStops.Add(fiveBottomGS);
                        break;
                }
            }
            else
            {
                GradientStop oneBottomGS = new GradientStop()
                {
                    Color = Color.FromArgb(255, 40, 40, 40),
                    Offset = 1
                };
                GradientStop oneTopGS = new GradientStop()
                {
                    Color = Color.FromArgb(255, 140, 140, 140),
                    Offset = 0.0
                };

                brush.GradientStops.Add(oneTopGS);
                brush.GradientStops.Add(oneBottomGS);
            }

            return brush;
        }

        /// <summary>
        /// Converts brush to agency rating value
        /// </summary>
        /// <param name="value">object value</param>
        /// <param name="targetType">Type targetType</param>
        /// <param name="parameter">object parameter</param>
        /// <param name="culture">CultureInfo culture</param>
        /// <returns>Agency rating</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
