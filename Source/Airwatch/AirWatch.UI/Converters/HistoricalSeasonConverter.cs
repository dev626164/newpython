﻿// <copyright file="HistoricalSeasonConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Filters historical data for year</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Data;
    using AirwatchData = Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Filters historical data for year
    /// </summary>
    public class HistoricalSeasonConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Collection<AirwatchData.StationMeasurement> sourceCollection = value as Collection<AirwatchData.StationMeasurement>;
            Collection<AirwatchData.StationMeasurement> targetCollection = new Collection<AirwatchData.StationMeasurement>();

            int sourceCount = 0;

            if (sourceCollection != null)
            {
                sourceCount = sourceCollection.Count;
            }

            if (sourceCount == 0 || sourceCollection[0].Measurement.Timestamp.HasValue == false)
            {
                return targetCollection;
            }

            int year = sourceCollection[sourceCount - 1].Measurement.Timestamp.Value.Year;

            for (int i = sourceCount - 1; i >= 0; i--)
            {
                DateTime? sourceTimestamp = sourceCollection[i].Measurement.Timestamp;

                if (sourceTimestamp.HasValue && sourceTimestamp.Value.Year == year 
                    && !(sourceTimestamp.Value.Month == 12 && sourceTimestamp.Value.Day == 31))
                {
                    targetCollection.Add(sourceCollection[i]);
                }
            }

            return targetCollection;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
