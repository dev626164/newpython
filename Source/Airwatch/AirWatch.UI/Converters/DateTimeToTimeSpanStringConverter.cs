﻿// <copyright file="DateTimeToTimeSpanStringConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Converts a DateTime to the '1' part of 'updated 1 Month ago' format.</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// DateTime To TimeSpan String Converter.
    /// </summary>
    public class DateTimeToTimeSpanStringConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type"/> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="culture">The culture of the conversion.</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string parameterString = string.Empty;

            DateTime? dateTime = value as DateTime?;

            if (dateTime == null || dateTime.HasValue == false)
            {
                parameterString = dateTime.GetValueOrDefault().ToShortDateString();
            }
            else
            {
                TimeSpan timeSpan = DateTime.UtcNow.Subtract((DateTime)dateTime);

                if (timeSpan.Days > 365)
                {
                    parameterString = (timeSpan.Days / 365).ToString(CultureInfo.InvariantCulture);
                }
                else if (timeSpan.Days > 273)
                {
                    parameterString = null;
                }
                else if (timeSpan.Days > 30)
                {
                    parameterString = ((timeSpan.Days / 30) + 1).ToString(CultureInfo.InvariantCulture);                   
                }
                else if (timeSpan.Days > 15)
                {
                    parameterString = null;
                }
                else if (timeSpan.Days >= 1)
                {
                    parameterString = (timeSpan.Days + 1).ToString(CultureInfo.InvariantCulture);                    
                }
                else if (timeSpan.Hours > 12)
                {
                    parameterString = null;                    
                }
                else if (timeSpan.Hours >= 1)
                {
                    parameterString = (timeSpan.Hours + 1).ToString(CultureInfo.InvariantCulture);                    
                }
                else
                {
                    parameterString = null;
                }
            }
            
            return parameterString;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay"/> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type"/> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="culture">The culture of the conversion.</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
