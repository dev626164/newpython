﻿//-----------------------------------------------------------------------
// <copyright file="QualityIndexToBindingTermConverter.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>13-10-2009</date>
// <summary>Converts a Quality Index to a localised binding term.</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.UI.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converts a Quality Index to a localised binding term.
    /// </summary>
    public class QualityIndexToBindingTermConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string bindingTerm = string.Empty;
            string param = (string)parameter;

            if (param != null)
            {
                param = param.ToLower(CultureInfo.InvariantCulture);
            }

            if (value == null)
            {
                return "COMMON_NODATA";
            }

            int qualityIndex = 0;
            decimal decimalValue;
            int intValue;

            if (decimal.TryParse(value.ToString(), out decimalValue))
            {
                qualityIndex = (int) decimalValue;
            } 
            else if (int.TryParse(value.ToString(), out intValue))
            {
                qualityIndex = (int) intValue;
            }

            if (qualityIndex == 0)
            {
                bindingTerm = "COMMON_NODATA";
            }

            if (param == "invert")
            {
                switch (qualityIndex)
                {                    
                    case 1:
                        bindingTerm = "COMMON_VERYBAD";
                        break;
                    case 2:
                        bindingTerm = "COMMON_BAD";
                        break;
                    case 3:
                        bindingTerm = "COMMON_MODERATE";
                        break;
                    case 4:
                        bindingTerm = "COMMON_GOOD";
                        break;
                    case 5:
                        bindingTerm = "COMMON_VERYGOOD";
                        break;
                }
            }
            else if (param == "water")
            {
                switch (qualityIndex)
                {                    
                    case 0:
                        bindingTerm = "COMMON_NODATA";
                        break;
                    case 1:
                        bindingTerm = "COMMON_GOOD";
                        break;
                    case 2:
                        bindingTerm = "COMMON_MODERATE";
                        break;
                    case 3:
                        bindingTerm = "COMMON_BAD";
                        break;
                }
            }
            else
            {
                switch (qualityIndex)
                {
                    case 1:
                        bindingTerm = "COMMON_VERYGOOD";
                        break;
                    case 2:
                        bindingTerm = "COMMON_GOOD";
                        break;
                    case 3:
                        bindingTerm = "COMMON_MODERATE";
                        break;
                    case 4:
                        bindingTerm = "COMMON_BAD";
                        break;
                    case 5:
                        bindingTerm = "COMMON_VERYBAD";
                        break;
                }
            }

            return bindingTerm;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
