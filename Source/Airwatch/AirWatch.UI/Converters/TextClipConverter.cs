﻿// <copyright file="TextClipConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Clips text to a certain length</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows.Data;

    /// <summary>
    /// Clips text to a certain length
    /// </summary>
    public class TextClipConverter : IValueConverter
    {  
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string extenderText = "...";

            if (value == null || parameter == null)
            {
                return value;
            }

            int length;
            string text = value.ToString();

            if (string.IsNullOrEmpty(text) || int.TryParse(parameter.ToString(), out length) == false || text.Length <= length)
            {
                return value;
            }

            int subStringLength = length - (1 + extenderText.Length);

            if (subStringLength > text.Length)
            {
                return value;
            }

            return text.Substring(0, subStringLength) + extenderText;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
