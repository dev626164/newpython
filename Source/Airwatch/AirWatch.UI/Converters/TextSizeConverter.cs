﻿// <copyright file="TextSizeConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>23-06-2009</date>
// <summary>Converts percentage and max size to a text size</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows.Data;

    /// <summary>
    /// Converts a boolean value to a Visibility
    /// </summary>
    public class TextSizeConverter : IValueConverter
    {
        /// <summary>
        /// Minimum value for the text
        /// </summary>
        private const int MinValue = 8;
        
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int maxValue; 
            double? percentage = value as double?;
            bool parseSucceeded = int.TryParse(parameter.ToString(), out maxValue);

            if (parseSucceeded == false || percentage == null)
            {
                return value;
            }

            // defines the text size to be a percentage of the max size. Removes / adds minValue to ensure not zero.
            int textSize = (int)(((maxValue - MinValue) * percentage) + MinValue);

            return textSize;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
