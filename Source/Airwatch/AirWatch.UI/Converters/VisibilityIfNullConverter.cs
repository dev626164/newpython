﻿// <copyright file="VisibilityIfNullConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>17-08-2009</date>
// <summary>Returns visible if null data</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Returns visible if null data
    /// </summary>
    public class VisibilityIfNullConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool vis = false;

            if (value == null)
            {
                vis = true;
            }
            else
            {
                // tests for null as -1 in an int
                int valueAsInt;
                if (Int32.TryParse(value.ToString(), out valueAsInt) == true)
                {
                    if (valueAsInt == -1)
                    {
                        vis = true;
                    }
                } 
                else if (string.IsNullOrEmpty(value.ToString()))
                {
                    vis = true;
                }
            }

            bool negate = false;

            if (parameter != null)
            {
                negate = bool.Parse(parameter.ToString());
            }

            if (negate == true)
            {
                vis = !vis;
            }

            return (bool)vis ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
