﻿// <copyright file="VisibilityIfGreaterThanConverter.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>17-08-2009</date>
// <summary>Returns visible if value is greater than parameter</summary>
namespace AirWatch.UI.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Returns visible if null data
    /// </summary>
    public class VisibilityIfGreaterThanConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool vis = false;
            double? paramValue = parameter as double?;
            double? compareValue = value as double?;

            if (paramValue.HasValue && compareValue.HasValue && compareValue.Value >= paramValue.Value)
            {
                vis = true;
            }

            return (bool)vis ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
