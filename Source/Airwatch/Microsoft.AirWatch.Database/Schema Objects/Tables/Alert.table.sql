﻿CREATE TABLE [Alert] (
    [AlertId]     BIGINT IDENTITY (0, 1) NOT NULL,
    [Threshold]   INT    NULL,
    [NotifiedBad] BIT    NOT NULL
);

