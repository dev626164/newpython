﻿CREATE TABLE [StationMeasurement] (
    [StationMeasurementId]		INT          IDENTITY (0, 1) NOT NULL,
    [MeasurementId]				BIGINT		 NOT NULL,
    [StationId]					INT			 NOT NULL,
    [AggregatedUserRating]		INT		     NULL,
	[AggregatedUserRatingCount] INT			 NULL,
);
GO

CREATE INDEX IDX_StationMeasurementToStation ON StationMeasurement(StationId)
GO

CREATE INDEX IDX_StationMeasurementToMeasurement ON StationMeasurement(MeasurementId)
GO