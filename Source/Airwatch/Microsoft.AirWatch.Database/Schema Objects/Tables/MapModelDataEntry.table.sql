﻿CREATE TABLE [MapModelDataEntry] (
    [DataEntryKey]        BIGINT          IDENTITY (0, 1) NOT NULL,
    [Longitude]           FLOAT			  NOT NULL,
    [Latitude]            FLOAT			  NOT NULL,
    [QualityValue]        FLOAT           NULL,
    [CAQI]                FLOAT			  NULL,
    [Ozone]               FLOAT			  NULL,
    [NitrogenDioxide]     FLOAT			  NULL,
    [ParticulateMatter10] FLOAT			  NULL,
    [Timestamp]           DATETIME        NOT NULL
);

