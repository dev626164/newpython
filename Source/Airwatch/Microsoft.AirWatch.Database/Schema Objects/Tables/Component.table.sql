﻿CREATE TABLE [Component] (
    [ComponentId] BIGINT        IDENTITY (0, 1) NOT NULL,
    [Caption]     VARCHAR (50) NULL,
    [Unit]        VARCHAR (20) NULL
);

