﻿CREATE TABLE [dbo].[MapModelData](
	[MapModelDataId] [int] IDENTITY(0,1) NOT NULL,
	[Resolution] [float] NULL,
	[Type] [nvarchar](10) NULL
);