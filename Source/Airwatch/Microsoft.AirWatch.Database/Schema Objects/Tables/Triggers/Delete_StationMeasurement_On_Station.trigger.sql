﻿-- =============================================
-- Deletes relevant StationMeasurements when Station is deleted
-- =============================================
CREATE TRIGGER [dbo].[Delete_StationMeasurement_On_Station]
   ON  [dbo].[Station]
   FOR DELETE
AS 
DELETE FROM StationMeasurement WHERE StationMeasurement.StationId IN
	(SELECT del.StationId FROM deleted del)
GO