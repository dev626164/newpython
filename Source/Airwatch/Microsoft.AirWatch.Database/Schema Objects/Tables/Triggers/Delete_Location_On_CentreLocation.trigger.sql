﻿-- =============================================
-- Deletes relevant Locations when CentreLocation is deleted
-- =============================================
CREATE TRIGGER Delete_Location_On_CentreLocation
   ON  [CentreLocation]
   FOR DELETE
AS 
DELETE FROM Location WHERE Location.LocationId IN
	(SELECT del.Location FROM deleted del)
GO
