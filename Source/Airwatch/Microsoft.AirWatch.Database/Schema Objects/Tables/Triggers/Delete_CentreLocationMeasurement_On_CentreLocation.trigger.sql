﻿-- =============================================
-- Deletes relevant CentreLocationMeasurements when CentreLocation is deleted
-- =============================================
CREATE TRIGGER  Delete_CentreLocationMeasurement_On_CentreLocation
   ON  [CentreLocation]
   FOR DELETE
AS 
DELETE FROM CentreLocationMeasurement WHERE CentreLocationMeasurement.CentreLocationId IN
	(SELECT del.CentreLocationId FROM deleted del)
GO