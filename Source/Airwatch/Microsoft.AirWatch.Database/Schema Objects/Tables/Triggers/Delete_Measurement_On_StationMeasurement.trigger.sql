﻿-- =============================================
-- Deletes relevant Measurements when StationMeasurement is deleted
-- =============================================
CREATE TRIGGER Delete_Measurement_On_StationMeasurement
   ON  [StationMeasurement]
   FOR DELETE
AS 
DELETE FROM Measurement WHERE Measurement.MeasurementId IN
	(SELECT del.MeasurementId FROM deleted del)
GO