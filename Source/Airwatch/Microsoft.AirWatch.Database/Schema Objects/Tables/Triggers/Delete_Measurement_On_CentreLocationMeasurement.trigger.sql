﻿-- =============================================
-- Deletes relevant Measurements when CentreLocationMeasurement is deleted
-- =============================================
CREATE TRIGGER Delete_Measurement_On_CentreLocationMeasurement
   ON  [CentreLocationMeasurement]
   FOR DELETE
AS 
DELETE FROM Measurement WHERE Measurement.MeasurementId IN
	(SELECT del.MeasurementId FROM deleted del)
GO