﻿CREATE TABLE [StationDataEntry] (
    [DataEntryKey]        BIGINT          IDENTITY (0, 1) NOT NULL,
    [Longitude]           DECIMAL (10, 7) NOT NULL,
    [Latitude]            DECIMAL (10, 7) NOT NULL,
    [CAQI]                DECIMAL (6, 3)  NULL,
    [Ozone]               DECIMAL (10, 5) NULL,
    [NitrogenDioxide]     DECIMAL (10, 5) NULL,
    [ParticulateMatter10] DECIMAL (10, 5) NULL,
    [Timestamp]           DATETIME        NOT NULL
);

