﻿CREATE TABLE [Location] (
    [LocationId] BIGINT          IDENTITY (0, 1) NOT NULL,
    [Longitude]  DECIMAL (10, 7) NOT NULL,
    [Latitude]   DECIMAL (10, 7) NOT NULL
);

