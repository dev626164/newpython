﻿ALTER TABLE [CentreLocation]
    ADD CONSTRAINT [FK_CentreLocation_Location] FOREIGN KEY ([Location]) REFERENCES [Location] ([LocationId]) ON DELETE CASCADE ON UPDATE NO ACTION;

