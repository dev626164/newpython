﻿ALTER TABLE [Measurement]
    ADD CONSTRAINT [FK_Measurement_Component] FOREIGN KEY ([ComponentId]) REFERENCES [Component] ([ComponentId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

