﻿ALTER TABLE [CentreLocationMeasurement]
    ADD CONSTRAINT [FK_CentreLocationMeasurement_CentreLocation] FOREIGN KEY ([CentreLocationId]) REFERENCES [CentreLocation] ([CentreLocationId]) ON DELETE CASCADE ON UPDATE NO ACTION;

