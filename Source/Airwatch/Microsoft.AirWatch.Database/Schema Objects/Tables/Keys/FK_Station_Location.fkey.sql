﻿ALTER TABLE [Station]
    ADD CONSTRAINT [FK_Station_Location] FOREIGN KEY ([LocationId]) REFERENCES [Location] ([LocationId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

