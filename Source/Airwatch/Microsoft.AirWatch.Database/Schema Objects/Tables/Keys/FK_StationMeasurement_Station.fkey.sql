﻿ALTER TABLE [StationMeasurement]
    ADD CONSTRAINT [FK_StationMeasurement_Station] FOREIGN KEY ([StationId]) REFERENCES [Station] ([StationId]) ON DELETE CASCADE ON UPDATE NO ACTION;

