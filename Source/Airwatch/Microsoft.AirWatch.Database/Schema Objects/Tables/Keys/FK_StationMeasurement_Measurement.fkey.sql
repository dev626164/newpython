﻿ALTER TABLE [StationMeasurement]
    ADD CONSTRAINT [FK_StationMeasurement_Measurement] FOREIGN KEY ([MeasurementId]) REFERENCES [Measurement] ([MeasurementId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

