﻿ALTER TABLE [RatingQualifier]  WITH CHECK ADD  CONSTRAINT [FK_RatingQualifier_UserRating] FOREIGN KEY([UserRatingId])
REFERENCES [UserRating] ([UserRatingId])
ON DELETE CASCADE

