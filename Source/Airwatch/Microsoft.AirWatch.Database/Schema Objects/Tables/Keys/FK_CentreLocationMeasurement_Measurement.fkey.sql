﻿ALTER TABLE [CentreLocationMeasurement]
    ADD CONSTRAINT [FK_CentreLocationMeasurement_Measurement] FOREIGN KEY ([MeasurementId]) REFERENCES [Measurement] ([MeasurementId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

