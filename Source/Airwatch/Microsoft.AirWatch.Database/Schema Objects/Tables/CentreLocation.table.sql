﻿CREATE TABLE [CentreLocation] (
    [CentreLocationId] BIGINT   IDENTITY (0, 1) NOT NULL,
    [QualityValue]     FLOAT    NULL,
    [QualityIndex]     INT      NULL,
    [Location]         BIGINT   NOT NULL,
    [Timestamp]        DATETIME NULL
);
GO

CREATE INDEX IDX_CentreLocationToLocation ON CentreLocation(Location)
GO