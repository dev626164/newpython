﻿CREATE TABLE [CentreLocationMeasurement] (
    [CentreLocationMeasurementId] BIGINT IDENTITY (0, 1) NOT NULL,
    [CentreLocationId]            BIGINT NOT NULL,
    [MeasurementId]               BIGINT NOT NULL
);
GO

CREATE INDEX IDX_CentreLocationMeasurementToCentreLocation ON CentreLocationMeasurement(CentreLocationId)
GO

CREATE INDEX IDX_CentreLocationMeasurementToMeasurement ON CentreLocationMeasurement(MeasurementId)
GO