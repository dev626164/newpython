﻿CREATE TABLE [RatingQualifier]
(
	[RatingQualifierId] BIGINT	IDENTITY (0,1) NOT NULL, 
	[UserRatingId]	BIGINT,
	[Identifier] varchar(100) NOT NULL
)
