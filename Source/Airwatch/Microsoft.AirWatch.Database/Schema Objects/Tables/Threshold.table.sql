﻿CREATE TABLE [Threshold]
(
	[ThresholdId]	BIGINT		IDENTITY (0, 1) NOT NULL, 
	[ComponentId]	BIGINT		NOT NULL,
	[Value]			DECIMAL (10, 5)	NOT NULL
)
