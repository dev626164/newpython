﻿CREATE TABLE [Station] (
	[StationId]        INT IDENTITY (0, 1)		NOT NULL,   
    [EuropeanCode]     VARCHAR (50)				NOT NULL,
    [Name]             NVARCHAR (MAX)			NULL,
    [CountryName]      NVARCHAR (30)			NULL,
    [FriendlyAddress]  NVARCHAR (MAX)			NULL,
    [TypeOfStation]    VARCHAR (20)				NULL,
    [TypeOfArea]       VARCHAR (20)				NULL,
    [TypeOfAreaSubcat] VARCHAR (20)				NULL,
    [StreetType]       VARCHAR (30)				NULL,
    [QualityIndex]     INT						NULL,
    [LocationId]       BIGINT					NOT NULL,
    [StationPurpose]   VARCHAR (5)				NULL,
    [Timestamp]        DATETIME					NULL
);
GO

CREATE INDEX IDX_StationToLocation ON Station(LocationId)
GO

ALTER TABLE [dbo].[Station] ADD UNIQUE (EuropeanCode, StationPurpose)
GO