﻿CREATE TABLE [Measurement] (
    [MeasurementId] BIGINT          IDENTITY (0, 1) NOT NULL,
    [ComponentId]   BIGINT          NULL,
    [Value]         DECIMAL (10, 5) NULL,
    [Timestamp]     DATETIME        NULL
);

