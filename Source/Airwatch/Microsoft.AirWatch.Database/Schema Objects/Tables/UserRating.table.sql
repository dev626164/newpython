﻿CREATE TABLE [UserRating](
	[UserRatingId] [bigint] IDENTITY(0,1) NOT NULL,
	[Rating] [int] NOT NULL,
	[Longitude]  DECIMAL (10, 7) NOT NULL,
    [Latitude]   DECIMAL (10, 7) NOT NULL,
	[RatingTime] [datetime] NOT NULL,
	[RatingTarget] [varchar](5) NOT NULL,
	[IPAddress] [varchar](50) NULL
);

