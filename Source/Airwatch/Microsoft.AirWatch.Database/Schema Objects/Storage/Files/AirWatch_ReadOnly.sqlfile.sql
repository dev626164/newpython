﻿-- Do not change the database name.
-- It will be properly coded for build and deployment
-- This is using sqlcmd variable substitution
ALTER DATABASE [$(DatabaseName)]
    ADD FILE 
    (
    	NAME = [AirWatch_ReadOnly], 
    	FILENAME = '$(DefaultDataPath)$(DatabaseName)_AirWatch_ReadOnly.ndf', 
        SIZE = 3072 KB, 
        MAXSIZE = UNLIMITED, 
        FILEGROWTH = 1024 KB
    ) 
	TO FILEGROUP [AW_READONLY]

