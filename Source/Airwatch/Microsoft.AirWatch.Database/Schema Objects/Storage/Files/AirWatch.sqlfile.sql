﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [AirWatch], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', MAXSIZE = UNLIMITED, FILEGROWTH = 10 %) TO FILEGROUP [PRIMARY];

