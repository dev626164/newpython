﻿-- =============================================
-- Author:		Dave Thompson
-- Description:	Transfers data in the MapModelDataEntry table
--				to the real tables
-- =============================================
CREATE PROCEDURE [dbo].[sp_TransferMapModelData]
	@batchSize Int = 100,
	@TraceInfo Bit = 0
AS
BEGIN

SET DEADLOCK_PRIORITY 10;

SET NOCOUNT ON;

DECLARE @batch Table
(
	id Int identity(1,1) PRIMARY KEY,
	DataEntryKey BigInt NOT NULL
);

DECLARE @duration VarChar(200);
DECLARE @startTime DateTime;

DECLARE @Longitude float;
DECLARE @Latitude float;
DECLARE @CAQI float;
DECLARE @QualityValue float;
DECLARE @Ozone float;
DECLARE @NitrogenDioxide float;
DECLARE @ParticulateMatter10 float;
DECLARE @Timestamp DateTime;

DECLARE @LocationId BigInt,
		@MeasurementId BigInt,
		@CentreLocationId BigInt,
		@ComponentIdO3 BigInt,
		@ComponentIdNO2 BigInt,
		@ComponentIdPM10 BigInt;

DECLARE @Continue Bit;
DECLARE @FirstRun BIT;
DECLARE @Stop BIT;
DECLARE @Count INT;

DECLARE @CurrentID BigInt, @MaxID BigInt, @CurrKey BigInt;
DECLARE @MinOriginalID BigInt, @MaxOriginalID Bigint;

SELECT @startTime = getutcdate();

SELECT @Continue = 1, @CurrentID = 1, @FirstRun = 1, @Stop = 0;

select @MinOriginalID = MIN(centrelocationid) from CentreLocation
select @MaxOriginalID = MAX(CentreLocationId) from CentreLocation

SELECT @ComponentIdO3 = [ComponentId]
FROM Component
WHERE [Caption] = 'O3';

SELECT @ComponentIdNO2 = [ComponentId]
FROM Component
WHERE [Caption] = 'NO2';

SELECT @ComponentIdPM10 = [ComponentId]
FROM Component
WHERE [Caption] = 'PM10';

BEGIN TRY

	-- DELETE FROM [CentreLocation];
	
	WHILE (@Continue = 1)
	BEGIN
	
		DELETE FROM @batch;
		
		INSERT INTO @batch
		(
			DataEntryKey
		)
		SELECT TOP(@batchSize)
			DataEntryKey
		FROM MapModelDataEntry
		ORDER BY DataEntryKey;
		
		IF @@ROWCOUNT = 0
		BEGIN
			IF @FirstRun = 1 -- if it is the first run and there are no entries, do not continue
				SELECT @Stop = 1;
				
			BREAK;
		END
			
		SELECT @FirstRun = 0;

		SELECT @CurrentID = MIN(id), @MaxID = MAX(id) FROM @batch;
		
		BEGIN TRANSACTION Batch
		        
		WHILE (@CurrentID <= @MaxID)
		BEGIN

			SELECT
				@CurrKey = DataEntryKey
			FROM @batch
			WHERE id = @CurrentID;
			
			SELECT
				@Longitude = Longitude,
				@Latitude = Latitude,
				@QualityValue = QualityValue,
				@CAQI = CAQI,
				@Ozone = Ozone,
				@NitrogenDioxide = NitrogenDioxide,
				@ParticulateMatter10 = ParticulateMatter10,
				@Timestamp = [Timestamp]
			FROM MapModelDataEntry
			WHERE DataEntryKey = @CurrKey;
			
			INSERT INTO Location
			(
				[Longitude],
				[Latitude]
			)
			VALUES
			(
				@Longitude,
				@Latitude
			);
			
			SELECT @LocationId = SCOPE_IDENTITY();
			
			INSERT INTO CentreLocation
			(
				[QualityIndex],
				[QualityValue],
				[Location],
				[Timestamp]
			)
			VALUES
			(
				@CAQI,
				@QualityValue,
				@LocationId,
				@Timestamp
			);
			
			SELECT @CentreLocationId = SCOPE_IDENTITY();
			
			IF @Ozone IS NOT NULL
			BEGIN
			
				INSERT INTO [Measurement]
				(
					[ComponentId],
					[Value],
					[Timestamp]
				)
				VALUES
				(
					@ComponentIdO3,
					@Ozone,
					@Timestamp
				);
				
				SELECT @MeasurementId = SCOPE_IDENTITY();
				
				INSERT INTO [CentreLocationMeasurement]
				(
					[CentreLocationId],
					[MeasurementId]
				)
				VALUES
				(
					@CentreLocationId,
					@MeasurementId
				);
				
			END;				
			
			IF @NitrogenDioxide IS NOT NULL
			BEGIN
			
				INSERT INTO [Measurement]
				(
					[ComponentId],
					[Value],
					[Timestamp]
				)
				VALUES
				(
					@ComponentIdNO2,
					@NitrogenDioxide,
					@Timestamp
				);
				
				SELECT @MeasurementId = SCOPE_IDENTITY();
				
				INSERT INTO [CentreLocationMeasurement]
				(
					[CentreLocationId],
					[MeasurementId]
				)
				VALUES
				(
					@CentreLocationId,
					@MeasurementId
				);
				
			END;
			
			IF @ParticulateMatter10 IS NOT NULL
			BEGIN
			
				INSERT INTO [Measurement]
				(
					[ComponentId],
					[Value],
					[Timestamp]
				)
				VALUES
				(
					@ComponentIdPM10,
					@ParticulateMatter10,
					@Timestamp
				);
				
				SELECT @MeasurementId = SCOPE_IDENTITY();
				
				INSERT INTO [CentreLocationMeasurement]
				(
					[CentreLocationId],
					[MeasurementId]
				)
				VALUES
				(
					@CentreLocationId,
					@MeasurementId
				);
				
			END;
		
			SELECT @CurrentID = @CurrentID + 1;
			
			IF @TraceInfo = 1 AND @CurrentID % 500 = 0
			BEGIN
				RAISERROR('%I64d completed so far...', 0, 1, @CurrentID) WITH NOWAIT;
			END;

		END; -- WHILE (@CurrentID <= @MaxID)
		
		DELETE MapModelDataEntry
		FROM MapModelDataEntry m
		INNER JOIN @batch b ON b.DataEntryKey = m.DataEntryKey;
		
		COMMIT TRANSACTION Batch;
		
	END; -- WHILE (@Continue = 1)
	
	IF @Stop = 0
	BEGIN
		WHILE @Continue = 1
		BEGIN
		
			IF @MaxOriginalID is null
				BREAK;
			
			SELECT @Count = COUNT(*) FROM CentreLocation WHERE CentreLocationId > @MaxOriginalId;
			IF @Count < 1000 -- if there are less than 1000 items higher than @MaxOriginalId
			BEGIN
				IF @TraceInfo = 1
				BEGIN
					SELECT @CurrentID = @CurrentID - 1;
					RAISERROR('There are %d items greater than @MaxOriginalId (%I64d) in CentreLocation', 0, 1, @Count, @MaxOriginalId) WITH NOWAIT;
				END;
				
				BREAK;
			END;
				
			IF (SELECT TOP(1) CentreLocationId FROM CentreLocation ORDER BY CentreLocationId) > @MaxOriginalID
				BREAK;
				
			BEGIN TRANSACTION Batch
			
				DELETE TOP(@batchSize) FROM CentreLocation WHERE CentreLocationId >= @MinOriginalID and CentreLocationId <= @MaxOriginalID;
			
			COMMIT TRANSACTION Batch;
			
			IF @TraceInfo = 1
			BEGIN
				RAISERROR('Deleted %d from CentreLocation (Batch)', 0, 1, @Count, @MaxOriginalId) WITH NOWAIT;
			END;
		
		END; -- WHLE (@Continue = 1)
	END; -- IF @Stop = 0

	IF @TraceInfo = 1
	BEGIN
		SELECT @CurrentID = @CurrentID - 1;
		RAISERROR('%I64d completed so far...', 0, 1, @CurrentID) WITH NOWAIT;
	END;
		
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(max)
	DECLARE @ErrorProc nvarchar(max)
	DECLARE @ErrorNum int
	DECLARE @ErrorLine int

	SELECT
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorProc = ERROR_PROCEDURE(),
		@ErrorNum = ERROR_NUMBER(),
		@ErrorLine = ERROR_LINE();

	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION Batch;

	-- Tell our caller what happened...
	RAISERROR('Procedure %s raised error number %d: ''%s'' at proc line %d', 11, 1,
				@ErrorProc,
				@ErrorNum,
				@ErrorMessage,
				@ErrorLine);
				
END CATCH;

SELECT
	@duration = CAST (CAST(DATEDIFF(ms, @startTime, getutcdate()) AS REAL)/CAST(1000 as REAL) AS varchar(200));

RAISERROR('Completed processing in %s seconds', 0, 1, @duration);

END

