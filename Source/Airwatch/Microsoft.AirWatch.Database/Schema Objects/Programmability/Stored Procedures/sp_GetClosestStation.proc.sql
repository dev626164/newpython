﻿/*
Name:  sp_GetClosestStation
Description:  Gets the closest station of type to a longitude and latitude
Author:  Dave Thompson
Modification Log: Change
*/
CREATE PROCEDURE [sp_GetClosestStation]
@Longitude DECIMAL (6, 3), @Latitude DECIMAL (6, 3), @stationType varchar(5)
AS

SELECT *, (POWER(Location.Longitude - @Longitude, 2) + POWER(Location.Latitude - @Latitude, 2)) AS distance
FROM [Station]
INNER JOIN [Location] ON [Station].[LocationId] = [Location].[LocationId] 
WHERE 
[Station].[StationPurpose] = @stationType
ORDER BY distance ASC