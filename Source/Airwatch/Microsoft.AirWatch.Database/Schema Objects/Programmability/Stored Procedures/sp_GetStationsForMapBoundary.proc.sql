﻿/*
Name:  sp_GetStationsForMapBoundary
Description:  Gets all the stations located in a given boundary
Author:  Fil Karnicki
Modification Log: Change

Description                  Date         Changed By
Created procedure            06/08/2009   Fil Karnicki
*/
CREATE PROCEDURE [sp_GetStationsForMapBoundary]
	@north	decimal(6,3), 
	@east	decimal(6,3),
	@south	decimal(6,3),
	@west	decimal(6,3)
AS
BEGIN
SET NOCOUNT ON;

SELECT [Station].[StationId], [Station].[EuropeanCode], [Location].[Latitude], [Location].[Longitude], [Station].[StationPurpose], [Station].[QualityIndex] FROM [Station] INNER JOIN [Location] ON [Station].[LocationId] = [Location].[LocationId] 
WHERE [Location].[Latitude] <= @north 
AND [Location].[Latitude] >= @south
AND [Location].[Longitude] <= @east
AND [Location].[Longitude] >= @west

END
GO