﻿/*
Name:  sp_GetStationsForMapBoundaryAndType
Description:  Gets all the stations located in a given boundary
Author:  Fil Karnicki
Override: Dave Thompson
Modification Log: Change

Description                  Date         Changed By
Modified procedure           01/09/2009   Dave Thompson
*/

CREATE PROCEDURE [sp_GetStationsForMapBoundaryAndType]
@north DECIMAL (6, 3), @east DECIMAL (6, 3), @south DECIMAL (6, 3), @west DECIMAL (6, 3), @stationPurpose VARCHAR (5)
AS
BEGIN
SET NOCOUNT ON;

SELECT [Station].[StationId], [Station].[EuropeanCode], [Location].[Latitude], [Location].[Longitude], [Station].[StationPurpose], [Station].[QualityIndex] FROM [Station] INNER JOIN [Location] ON [Station].[LocationId] = [Location].[LocationId] 
WHERE [Location].[Latitude] <= @north 
AND [Location].[Latitude] >= @south
AND [Location].[Longitude] <= @east
AND [Location].[Longitude] >= @west
AND [Station].[StationPurpose] = @stationPurpose

END
GO