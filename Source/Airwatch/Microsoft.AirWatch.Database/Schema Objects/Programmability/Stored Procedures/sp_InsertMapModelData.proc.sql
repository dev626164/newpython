﻿-- =============================================
-- Author:		Dave Thompson
-- Description: insert data into map model staging table
-- =============================================
CREATE PROCEDURE [sp_InsertMapModelData]
	@Longitude float,
	@Latitude float,
	@QualityValue float,
	@CAQI float,
	@Ozone float,
	@NitrogenDioxide float,
	@ParticulateMatter10 float,
	@Timestamp datetime
AS
BEGIN

INSERT INTO [MapModelDataEntry](
[Longitude],
[Latitude],
[QualityValue],
[CAQI],
[Ozone],
[NitrogenDioxide],
[ParticulateMatter10],
[Timestamp])
VALUES
(
	@Longitude,
	@Latitude,
	@QualityValue,
	@CAQI,
	@Ozone,
	@NitrogenDioxide,
	@ParticulateMatter10,
	@Timestamp
);

END

GO