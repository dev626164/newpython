﻿-- =============================================
-- Author:		Dave Thompson
-- Description:	Deletes all items in MapModelDataEntry
-- =============================================
CREATE PROCEDURE [sp_DeleteMapModelDataEntry] 
AS
BEGIN
	DELETE FROM [MapModelDataEntry]
END

GO