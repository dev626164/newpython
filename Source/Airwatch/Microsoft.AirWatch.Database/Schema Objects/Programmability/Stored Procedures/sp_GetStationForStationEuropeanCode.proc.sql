﻿/*
Name:  sp_GetStationForStationEuropeanCode
Description:  Gets a station for a given station european code
Author:  Fil Karnicki
Modification Log: Change

Description                  Date         Changed By
Created procedure            12/06/2009   Fil Karnicki
*/

CREATE PROCEDURE sp_GetStationForStationEuropeanCode

@stationid int

AS

SELECT *
FROM Station
WHERE Station.StationId = @stationid