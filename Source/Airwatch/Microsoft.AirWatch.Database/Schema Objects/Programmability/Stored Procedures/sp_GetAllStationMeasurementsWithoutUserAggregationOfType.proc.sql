﻿/*
Name:  sp_GetAllStationMeasurementsWithoutUserAggregationOfType
Description:  Gets all station measurements of given target type where there is no aggregated user rating
				For use when archiving data
Author:  Dave Thompson
Modification Log: Change

Description                  Date         Changed By
Created procedure            22/06/2009   Dave Thompson
*/

CREATE PROCEDURE sp_GetAllStationMeasurementsWithoutUserAggregationOfType
	@Target NVARCHAR(5)
	AS
BEGIN
	SELECT StationMeasurement.StationMeasurementId, StationMeasurement.StationId, StationMeasurement.MeasurementId, StationMeasurement.AggregatedUserRating, StationMeasurement.AggregatedUserRatingCount FROM StationMeasurement
	INNER JOIN Station ON Station.StationId = StationMeasurement.StationId
	WHERE Station.StationPurpose = @Target AND StationMeasurement.AggregatedUserRating IS NULL
END
GO