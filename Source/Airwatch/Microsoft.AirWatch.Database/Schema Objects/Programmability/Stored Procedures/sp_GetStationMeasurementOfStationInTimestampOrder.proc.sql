﻿/*
Name:  sp_GetStationMeasurementOfStationInTimestampOrder
Description:  Gets all station measurements of given station in timestamp order
Author:  Dave Thompson
Modification Log: Change

Description                  Date         Changed By
Created procedure            12/10/2009   Dave Thompson
*/

CREATE PROCEDURE sp_GetStationMeasurementOfStationInTimestampOrder
	@StationId BigInt,
	@Timestamp DateTime
	AS
BEGIN
	SELECT	[StationMeasurement].[StationMeasurementId], 
			[StationMeasurement].[StationId],
			[StationMeasurement].[MeasurementId],
			[StationMeasurement].[AggregatedUserRating],
			[StationMeasurement].[AggregatedUserRatingCount]
	FROM [StationMeasurement]
	INNER JOIN [Measurement] on [Measurement].[MeasurementId] = [StationMeasurement].[MeasurementId]
	WHERE	[Measurement].[Timestamp] > @Timestamp AND
			[StationMeasurement].[StationId] = @StationId
	ORDER BY [Measurement].[Timestamp] DESC
END
GO