﻿/*
Name:  sp_GetAllStationMeasurementsWithoutUserAggregationOfType
Description:  Gets all previous station measurements from a given station id where the timestamp
				is less than the timestamp given.  Ordered so the top record will be the closest time
Author:  Dave Thompson
Modification Log: Change

Description                  Date         Changed By
Created procedure            22/06/2009   Dave Thompson
*/

CREATE PROCEDURE [dbo].[sp_GetPreviousStationMeasurement]
	@StationId BIGINT,
	@Timestamp DateTime
	AS
BEGIN
	SELECT StationMeasurement.StationMeasurementId, StationMeasurement.StationId, StationMeasurement.MeasurementId, StationMeasurement.AggregatedUserRating, StationMeasurement.AggregatedUserRatingCount FROM StationMeasurement
	INNER JOIN Measurement ON StationMeasurement.MeasurementId = Measurement.MeasurementId
	WHERE StationMeasurement.StationId = @StationId AND Measurement.[Timestamp] < @Timestamp
	ORDER BY Measurement.[Timestamp] DESC
END