﻿/*
Name:  sp_GetIndexForPoint
Description:  Gets current quality index for a given point on the map
Author:  Simon Middlemiss, Dave Thompson, Fil Karnicki
Modification Log: Change

Description                  Date         Changed By
Created procedure            11/06/2009   Fil Karnicki
*/

CREATE PROCEDURE sp_GetIndexForPoint

@longitude			decimal(6,3),
@latitude			decimal(6,3),
@offset				decimal(6,3),
@componentCaption	varchar(15)

AS

SELECT *
FROM Component
INNER JOIN Measurement ON Measurement.MeasurementId IN 
(
	SELECT Measurement.MeasurementId 
		FROM CentreLocationMeasurement 
			JOIN Measurement 
				ON Measurement.MeasurementId 
				IN
					(SELECT CentreLocationMeasurement.MeasurementId 
						FROM CentreLocationMeasurement 
							WHERE CentreLocationMeasurement.CentreLocationId = 
								(SELECT CentreLocation.CentreLocationId
											FROM CentreLocation 
												INNER JOIN Location 
												ON CentreLocation.Location = Location.LocationId
												WHERE ((Location.Latitude >= @latitude) AND (Location.Latitude < (@latitude + @offset))
												AND (Location.Longitude <= @longitude) AND (Location.Longitude > (@longitude - @offset)))))
)
WHERE Measurement.ComponentId = Component.ComponentId
AND Component.Caption = @componentCaption