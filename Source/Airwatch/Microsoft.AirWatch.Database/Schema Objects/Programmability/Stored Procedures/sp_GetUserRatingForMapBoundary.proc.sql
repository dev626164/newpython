﻿/*
Name:  sp_GetUserRatingForMapBoundary
Description:  Gets all the User Ratings located in a given boundary
Author:  Simon Middlemiss
Modification Log: Change

Description                  Date         Changed By
Created procedure            28/08/2009   Simon Middlemiss
*/
CREATE PROCEDURE [sp_GetUserRatingForMapBoundary]
	@north	decimal(6,3), 
	@east	decimal(6,3),
	@south	decimal(6,3),
	@west	decimal(6,3)
AS
BEGIN
SET NOCOUNT ON;

SELECT [UserRating].UserRatingId, [UserRating].[Rating], [UserRating].[Latitude], [UserRating].[Longitude], [UserRating].[IPAddress], [UserRating].RatingTarget, [UserRating].RatingTime, [UserRating].UserRatingId
FROM [UserRating]
WHERE [UserRating].[Latitude] <= @north 
AND [UserRating].[Latitude] >= @south
AND [UserRating].[Longitude] <= @east
AND [UserRating].[Longitude] >= @west

END
GO