﻿/*
Name:  sp_GetCentreLocationForPoint
Description:  Gets current centre location for a given point on the map
Author:  Fil Karnicki
Modification Log: Change

Description                  Date         Changed By
Created procedure            12/06/2009   Fil Karnicki
*/

CREATE PROCEDURE [sp_GetCentreLocationForPoint]

@longitude		decimal(6,3),
@latitude		decimal(6,3),
@offset			decimal(6,3)

AS

SELECT *
FROM CentreLocation 
INNER JOIN Location 
ON CentreLocation.Location = Location.LocationId
WHERE ((Location.Latitude >= @latitude) AND (Location.Latitude - @offset < @latitude)) 
AND ((Location.Longitude <= @longitude) AND (Location.Longitude + @offset > @longitude))