﻿/*
Name:  sp_GetUserRatingsInRadiusForTime
Description:  Gets all the User Ratings in a Radius up to a given time
Author:  Dave Thompson
Modification Log: Change
*/
CREATE PROCEDURE [sp_GetUserRatingsInRadiusForTime]
	@Longitude Decimal(10, 7),
	@Latitude Decimal(10, 7),
	@Target NVARCHAR(5),
	@Radius DECIMAL(10,7),
	@BaseTime DateTime,
	@EndTime DateTime
AS
BEGIN
	SELECT * FROM UserRating
		WHERE SQRT(POWER(UserRating.Longitude - @Longitude, 2) + POWER(UserRating.Latitude - @Latitude, 2)) < @Radius
		AND UserRating.RatingTime < @EndTime
		AND UserRating.RatingTime > @BaseTime
		AND UserRating.RatingTarget = @Target
END
GO
