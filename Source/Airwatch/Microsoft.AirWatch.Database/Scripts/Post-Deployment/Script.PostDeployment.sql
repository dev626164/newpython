﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------

Note all the txt files for bulk import start with primary key 0 on all rows, this is because the rows
should all be calcuated automatically and so ignore this anyway, however bulk insert is not flexible
enough to ignore this column
*/
PRINT 'Deploying Sample Data to the Database';
:r .\Component.sql
:r .\Threshold.sql
/*
 :r .\Measurement.sql
:r .\Location.sql
:r .\CentreLocation.sql
:r .\CentreLocationMeasurement.sql
:r .\Station.sql
:r .\StationMeasurement.sql
*/
PRINT 'Deployment Complete';