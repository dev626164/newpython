PRINT 'Inserting into Location';
BULK INSERT [Microsoft.AirWatch.Database].[Location]
    FROM '\\AWBUILD01\D$\Builds\Sources\AirWatch.Build.Continuous\Source\Airwatch\Microsoft.AirWatch.Database\Scripts\Post-Deployment\Location.txt'
    WITH
    (
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n'
    )
GO