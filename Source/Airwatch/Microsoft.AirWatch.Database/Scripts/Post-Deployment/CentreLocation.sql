PRINT 'Inserting into CentreLocation';
BULK INSERT [Microsoft.AirWatch.Database].[CentreLocation]
	FROM '\\AWBUILD01\D$\Builds\Sources\AirWatch.Build.Continuous\Source\Airwatch\Microsoft.AirWatch.Database\Scripts\Post-Deployment\CentreLocation.txt'
	WITH
    (
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n'
    )
GO