PRINT 'Inserting into CentreLocationMeasurement';
BULK INSERT [Microsoft.AirWatch.Database].[CentreLocationMeasurement]
	FROM '\\AWBUILD01\D$\Builds\Sources\AirWatch.Build.Continuous\Source\Airwatch\Microsoft.AirWatch.Database\Scripts\Post-Deployment\CentreLocationMeasurement.txt'
	WITH
    (
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n'
    )
GO