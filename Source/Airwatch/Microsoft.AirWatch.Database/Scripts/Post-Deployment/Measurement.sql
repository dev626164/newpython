PRINT 'Inserting into Measurement';
BULK INSERT [Microsoft.AirWatch.Database].[Measurement]
    FROM '\\AWBUILD01\D$\Builds\Sources\AirWatch.Build.Continuous\Source\Airwatch\Microsoft.AirWatch.Database\Scripts\Post-Deployment\Measurement.txt'
    WITH
    (
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n'
    )
GO