﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>15/07/09</date>
// <summary>Parses EOE xml and puts its contents in azure tables</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Tools.AzureTablesExcelParser
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Xml;
    using System.Xml.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure.ServiceRuntime;

    /// <summary>
    /// Parses EOE xml and puts its contents in azure tables 
    /// </summary>
    public sealed class Program
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Program"/> class from being created.
        /// </summary>
        private Program()
        {
        }

        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The argsuments passed</param>
        public static void Main(string[] args)
        {
            int count = 0;
            ////int totalCount = 0;

            bool automatedBuild = false;

            Console.WriteLine(args);
            Console.WriteLine(args.Length);
            if (args.Length != 0)
            {
                if (args.Length == 1)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Please run the development storage and press any key");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.ReadKey();
                }
            }
            else
            {
                Console.WriteLine("This should not appear during the MS build tasks, we must specify the correct xml.");
                Console.WriteLine(@"Using default xml file: ..\..\Microsoft.AirWatch.AzureTables\languagesXLSX\eoe.xml from context {0}", Environment.CurrentDirectory);
                args = new string[] { @"..\..\Microsoft.AirWatch.AzureTables\languagesXLSX\eoe.xml" };
            }

            if (args.Length == 1 || args.Length == 2)
            {
                if (args.Length == 2)
                {
                    Console.WriteLine("TFS build deploying to the cloud.");
                    automatedBuild = true;
                }

                ServicePointManager.DefaultConnectionLimit = 500;

                using (XmlReader reader = XmlReader.Create(File.OpenRead(args.ElementAt(0))))
                {
                    XElement element = XElement.Load(reader);

                    var allContents = from el in element.Elements().Elements()
                                      select new
                                      {
                                          Culture = el.Name.LocalName,
                                          StringId = el.Parent.Attribute("id").Value,
                                          Value = el.Value
                                      };

                    CloudStorageAccount account = CloudStorageAccount.DevelopmentStorageAccount;
                    // StorageAccountInfo account = StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration();

                    CloudTableClient t = account.CreateCloudTableClient();
                    // TableStorage t = TableStorage.Create(account);
                    
                    t.RetryPolicy = RetryPolicies.RetryExponential(100, RetryPolicies.DefaultClientBackoff.Subtract(RetryPolicies.DefaultMinBackoff));
                    // t.RetryPolicy = RetryPolicies.RetryExponentialN(100, RetryPolicies.StandardMaxBackoff.Subtract(RetryPolicies.StandardMinBackoff));

                    try
                    {
                        List<LanguageEntity> culture_ids = new List<LanguageEntity>();

                        var langEntityTable = new LanguageEntityTable(account.TableEndpoint.ToString(), account.Credentials);

                        foreach (var languageString in allContents)
                        {
                            if (languageString.StringId == "CULTURE_ID")
                            {
                                culture_ids.Add(new LanguageEntity(languageString.Culture, languageString.StringId, languageString.Value));
                            }

                            var l = from c in langEntityTable.Languages
                                    where c.PartitionKey == languageString.Culture && c.RowKey == languageString.StringId
                                    select c;

                            List<LanguageEntity> currentEntities = null;

                            try
                            {
                                currentEntities = l.ToList();
                            }
                            catch
                            {
                                // need to do something with this error once caught
                            }

                            if (currentEntities != null && currentEntities.Count == 1)
                            {
                                LanguageEntity languageEntity = currentEntities[0] as LanguageEntity;

                                if (languageString.Value != languageEntity.Value)
                                {
                                    languageEntity.Value = languageString.Value;

                                    langEntityTable.UpdateObject(languageEntity);

                                    Console.WriteLine("Updated: [" + string.Format(CultureInfo.InvariantCulture, "{0}%", (int)(((double)count++ / (double)allContents.Count()) * 100)) + "]\t" + languageString.StringId + " (" + languageString.Culture + ")");

                                    langEntityTable.SaveChanges();
                                    Thread.Sleep(0);
                                }
                                else
                                {
                                    Console.WriteLine("Skipped - Up to date: [" + string.Format(CultureInfo.InvariantCulture, "{0}%", (int)(((double)count++ / (double)allContents.Count()) * 100)) + "]\t" + languageString.StringId + " (" + languageString.Culture + ")");
                                }
                            }
                            else
                            {
                                if (currentEntities == null || currentEntities.Count == 0)
                                {
                                    langEntityTable.AddObject("Languages", new LanguageEntity(languageString.Culture, languageString.StringId, languageString.Value));
                                    Console.WriteLine("Added: [" + string.Format(CultureInfo.InvariantCulture, "{0}%", (int)(((double)count++ / (double)allContents.Count()) * 100)) + "]\t" + languageString.StringId + " (" + languageString.Culture + ")");

                                    langEntityTable.SaveChanges();
                                    Thread.Sleep(0);
                                }
                                else
                                {
                                    Console.WriteLine("Skipping - Non unique entry found: [" + string.Format(CultureInfo.InvariantCulture, "{0}%", (int)(((double)count++ / (double)allContents.Count()) * 100)) + "]\t" + languageString.StringId + " (" + languageString.Culture + ")");
                                }
                            }                            
                        }

                        foreach (LanguageEntity cultureId in culture_ids)
                        {
                            var l = from c in langEntityTable.Languages
                                    where c.PartitionKey == cultureId.PartitionKey && c.RowKey == cultureId.RowKey
                                    select c;

                            List<LanguageEntity> currentEntities = l.ToList();

                            if (currentEntities != null && currentEntities.Count == 1)
                            {
                                LanguageEntity languageEntity = currentEntities[0] as LanguageEntity;

                                languageEntity.Value = cultureId.Value;

                                langEntityTable.DeleteObject(languageEntity);
                                langEntityTable.SaveChanges();
                                Thread.Sleep(0);
                                langEntityTable.AddObject("Languages", languageEntity);
                                langEntityTable.SaveChanges();

                                Console.WriteLine("Touched CULTURE_ID: [" + string.Format(CultureInfo.InvariantCulture, "New Time Stamp {0}", languageEntity.Timestamp));
                            }                            
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Fail");
                        Console.WriteLine(e.ToString());
                    }

                    if (!automatedBuild)
                    {
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey(false);
                    }
                }
            }
        }
    }
}
