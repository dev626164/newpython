﻿//-----------------------------------------------------------------------
// <copyright file="LanguagesTests.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/07/09</date>
// <summary>Languages Tests</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Languages Tests
    /// </summary>
    [TestClass]
    public class LanguagesTests
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguagesTests"/> class.
        /// </summary>
        public LanguagesTests()
        {
        }

        /// <summary>
        /// Tests the language en GB.
        /// </summary>
        [TestMethod]
        public void TestLanguageEnglish()
        {
            using (LanguageServiceClient client = new LanguageServiceClient())
            {
                var stringDict = client.GetLanguage("en-GB");
                Assert.IsNotNull(stringDict);
                client.Close();
            }
        }
    }
}
