﻿//-----------------------------------------------------------------------
// <copyright file="ApplicationServicesTests.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>13/08/09</date>
// <summary>Application Services Tests</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Application Services Tests
    /// </summary>
    [TestClass]
    public class ApplicationServicesTests
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationServicesTests"/> class.
        /// </summary>
        public ApplicationServicesTests()
        {
        }

        /// <summary>
        /// Gets the rating test.
        /// </summary>
        [TestMethod]
        public void GetRatingTest()
        {
            Microsoft.AirWatch.Core.Data.AverageRating avgRating = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.RatingService.GetRating(0, 0, "Air", 0.03);
            Assert.IsNotNull(avgRating);
        }
    }
}
