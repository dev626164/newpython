﻿//-----------------------------------------------------------------------
// <copyright file="MapDataEntryServiceTest.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>21/07/09</date>
// <summary>MapDataEntryService Test</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.ServiceModel;
    using System.Threading;
    using System.Xml;
    using System.Xml.Linq;
    using Microsoft.AirWatch.Common;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// MapDataEntryService Tests
    /// </summary>
    [TestClass]
    public class MapDataEntryServiceTest
    {
        /// <summary>
        /// The Queue account
        /// </summary>
        private CloudStorageAccount queueAccount;

        /// <summary>
        /// The queue storage
        /// </summary>
        private CloudQueueClient queueStorage;

        /// <summary>
        /// The test queue
        /// </summary>
        private CloudQueue testQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapDataEntryServiceTest"/> class.
        /// </summary>
        public MapDataEntryServiceTest()
        {
            //this.queueAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            //this.queueStorage = this.queueAccount.CreateCloudQueueClient();
            //this.testQueue = this.queueStorage.GetQueueReference("testqueue");
            //this.testQueue.CreateIfNotExist();
        }

        /// <summary>
        /// Cleans up the test
        /// </summary>
        [TestCleanup]
        public void TestCleanup()
        {
        }

        /// <summary>
        /// Maps the data entry service.
        /// </summary>
        [TestMethod]
        public void MapDataEntryServiceCloud()
        {
            string data = string.Empty;

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.iaqp_caqi2.bin"))
            {
                data = StreamToBase64String(stream);
            }

            IDataEntryService service = ChannelFactory<IDataEntryService>.CreateChannel(new BasicHttpBinding(BasicHttpSecurityMode.None), new EndpointAddress("http://airwatchtest.cloudapp.net/Services/DataEntryService.svc"));
            service.EnterMapModelData(data);
        }

        /// <summary>
        /// Maps the data entry service.
        /// </summary>
        [TestMethod]
        public void MapDataEntryServiceLocal()
        {
            new Microsoft.AirWatch.CloudService.WorkerRole.AirWatchWorkerRole().OnStart();

            string data = string.Empty;

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.iaqp_caqi2.bin"))
            {
                data = StreamToBase64String(stream);
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                client.EnterMapModelData(data);
            }

            string result = this.LoopUntilReceived();
            Assert.IsTrue(bool.Parse(result));
        }

        /// <summary>
        /// Maps the data entry service local bin.
        /// </summary>
        [TestMethod]
        public void MapDataEntryServiceLocalBin()
        {
            // new Microsoft.AirWatch.CloudService.WorkerRole.AirWatchWorkerRole().OnStart();

            string data = string.Empty;

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.iaqp_caqi_6_5_2010.bin"))
            {
                data = StreamToBase64String(stream);
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                client.EnterMapModelData(data);   
            }

            // string result = this.LoopUntilReceived();
        }

        /// <summary>
        /// Maps the data entry service local cloud queue.
        /// </summary>
        [TestMethod]
        public void MapDataEntryServiceLocalCloudQueue()
        {
            string data = string.Empty;

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.iaqp_caqi2.bin"))
            {
                data = StreamToBase64String(stream);
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                client.EnterMapModelData(data);
            }
        }

        /// <summary>
        /// Reads the bin.
        /// </summary>
        [TestMethod]
        public void ReadBin()
        {
            using (BinaryReader file = new BinaryReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.iaqp_caqi2.bin")))
            {
                List<MapPoint> mapPoints = new List<MapPoint>();

                int lines;
                int id_size = file.ReadInt32();
                Debug.WriteLine(id_size);

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(file.ReadChars(id_size));

                Debug.WriteLine(sb.ToString());
                Debug.WriteLine(file.ReadInt32());

                lines = file.ReadInt32();
                Debug.WriteLine(lines);
                Debug.WriteLine(file.ReadInt32());

                for (int i = 0; i < lines; i++)
                {
                    var mapPoint = new MapPoint()
                                  {
                                      Lattitude = (double)file.ReadInt16() / 100,
                                      Longitude = (double)file.ReadInt16() / 100,
                                      Caqi = (double)file.ReadInt16() / 10,
                                      Ozone = (double)file.ReadInt16() / 10,
                                      NO2 = (double)file.ReadInt16() / 10,
                                      PM10 = (double)file.ReadInt16() / 10
                                  };

                    mapPoints.Add(mapPoint);

                    Console.WriteLine(string.Format(System.Globalization.CultureInfo.InvariantCulture, "Latitude {0}, Longitude {1}, Caqi {2}, Ozone {3}, NO2 {4}, PM10 {5}", mapPoint.Lattitude, mapPoint.Longitude, mapPoint.Caqi, mapPoint.Ozone, mapPoint.NO2, mapPoint.PM10));
                }
            }
        }

        /// <summary>
        /// Tests entering air stations.
        /// </summary>
        [TestMethod]
        public void EnterAirStations()
        {
            new Microsoft.AirWatch.CloudService.WorkerRole.AirWatchWorkerRole().OnStart();

            XmlDocument xmlDoc = new XmlDocument();
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.AirStation22-09-09.xml")))
            {
                xmlDoc.LoadXml(sr.ReadToEnd());
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                var root = xmlDoc.GetElementsByTagName("ns1:Root");

                if (root.Count != 0)
                {
                    client.EnterAirStationData((XmlElement)root[0]);
                }
                else
                {
                    Assert.Fail("Could not find root element in data");
                }
            }

            this.LoopUntilReceived();
        }

        /// <summary>
        /// Tests entering water stations.
        /// </summary>
        [TestMethod]
        public void EnterWaterStations()
        {
            new Microsoft.AirWatch.CloudService.WorkerRole.AirWatchWorkerRole().OnStart();

            XmlDocument xmlDoc = new XmlDocument();
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.WaterStationsInput.xml")))
            {
                xmlDoc.LoadXml(sr.ReadToEnd());
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                var root = xmlDoc.GetElementsByTagName("ns0:Root");

                if (root.Count != 0)
                {
                    var s = Stopwatch.StartNew();
                    client.EnterWaterStationData((XmlElement)root[0]);
                    s.Stop();
                    Console.WriteLine(s.Elapsed);
                }
                else
                {
                    Assert.Fail("Could not find root element in data");
                }
            }

            this.LoopUntilReceived();
        }

        /// <summary>
        /// Enters all data to the cloud storage.
        /// </summary>
        [TestMethod]
        public void EnterAllDataCloud()
        {
            XmlDocument xmlDoc = new XmlDocument();
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.AirStationsInput.xml")))
            {
                xmlDoc.LoadXml(sr.ReadToEnd());
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                var root = xmlDoc.GetElementsByTagName("ns0:Root");

                if (root.Count != 0)
                {
                    client.EnterAirStationData((XmlElement)root[0]);
                }
                else
                {
                    Assert.Fail("Could not find root element in data");
                }
            }

            xmlDoc = new XmlDocument();
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.WaterStationsInput.xml")))
            {
                xmlDoc.LoadXml(sr.ReadToEnd());
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                var root = xmlDoc.GetElementsByTagName("Root");

                if (root.Count != 0)
                {
                    var s = Stopwatch.StartNew();
                    client.EnterWaterStationData((XmlElement)root[0]);
                    s.Stop();
                    Console.WriteLine(s.Elapsed);
                }
                else
                {
                    Assert.Fail("Could not find root element in data");
                }
            }

            string data = string.Empty;

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.Tests.Data.iaqp_caqi.lres.bin"))
            {
                data = StreamToBase64String(stream);
            }

            using (DataEntryServiceClient client = new DataEntryServiceClient())
            {
                client.EnterMapModelData(data);
            }
        }

        /// <summary>
        /// Streams to base64 string.
        /// </summary>
        /// <param name="mystream">The mystream.</param>
        /// <returns>Base64 string</returns>
        private static string StreamToBase64String(Stream mystream)
        {
            byte[] byteArr = new byte[mystream.Length];
            mystream.Read(byteArr, 0, (int)mystream.Length);
            return Convert.ToBase64String(byteArr, 0, (int)mystream.Length, Base64FormattingOptions.None);
        }

        /// <summary>
        /// Loops the until received.
        /// </summary>
        /// <returns>Result string</returns>
        private string LoopUntilReceived()
        {
            CloudQueueMessage msg;
            DateTime start = DateTime.UtcNow;

            while (true)
            {
                msg = this.testQueue.GetMessage();

                if (msg != null)
                {
                    this.testQueue.DeleteMessage(msg);

                    return msg.AsString;
                }
                else
                {
                    //// if looping for more than a minute
                    if (start - DateTime.UtcNow > new TimeSpan(0, 1, 0))
                    {
                        return "Loop timeout";
                    }
                    else
                    {
                        Thread.Sleep(2000);
                    }
                }
            }
        }

        /// <summary>
        /// Map point class
        /// </summary>
        private class MapPoint
        {
            /// <summary>
            /// Gets or sets the lattitude.
            /// </summary>
            /// <value>The lattitude.</value>
            public double Lattitude { get; set; }

            /// <summary>
            /// Gets or sets the longitude.
            /// </summary>
            /// <value>The longitude.</value>
            public double Longitude { get; set; }

            /// <summary>
            /// Gets or sets the caqi.
            /// </summary>
            /// <value>The air quality index.</value>
            public double Caqi { get; set; }

            /// <summary>
            /// Gets or sets the ozone.
            /// </summary>
            /// <value>The ozone.</value>
            public double Ozone { get; set; }

            /// <summary>
            /// Gets or sets the P M10.
            /// </summary>
            /// <value>The P M10.</value>
            public double PM10 { get; set; }

            /// <summary>
            /// Gets or sets the N o2.
            /// </summary>
            /// <value>The N o2 val.</value>
            public double NO2 { get; set; }
        }
    }
}
