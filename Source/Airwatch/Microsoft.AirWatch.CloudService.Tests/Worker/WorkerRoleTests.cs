﻿// <copyright file="WorkerRoleTests.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>30-04-2009</date>
// <summary>WorkerRoleTests</summary>
namespace Microsoft.AirWatch.CloudService.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using Microsoft.AirWatch.CloudService.WorkerRole;
    using Microsoft.AirWatch.Common;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// WorkerRole Tests
    /// </summary>
    [TestClass]
    public class WorkerRoleTests
    {
        /// <summary>
        /// The Queue account
        /// </summary>
        private CloudStorageAccount queueAccount;

        /// <summary>
        /// The queue storage
        /// </summary>
        private CloudQueueClient queueStorage;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private CloudQueue heatMapTileServerQueue;

        /// <summary>
        /// The test queue
        /// </summary>
        private CloudQueue testQueue;

        /// <summary>
        /// test context instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkerRoleTests"/> class.
        /// </summary>
        public WorkerRoleTests()
        {
            this.queueAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueStorage = this.queueAccount.CreateCloudQueueClient();
            this.testQueue = this.queueStorage.GetQueueReference("testqueue");
            this.testQueue.CreateIfNotExist();

            this.heatMapTileServerQueue = this.queueStorage.GetQueueReference("heatmaptileserverqueue");
            this.heatMapTileServerQueue.CreateIfNotExist();
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        #endregion

        /// <summary>
        /// Cleans up the test
        /// </summary>
        [TestCleanup]
        public void TestCleanup()
        {
            List<string> deleteMessagesFromQueuesList = new List<string>();
            deleteMessagesFromQueuesList.Add("shortmessagequeue");
            deleteMessagesFromQueuesList.Add("testqueue");
            deleteMessagesFromQueuesList.Add("heatmaptileserverqueue");

            using (DevelopmentStorageDbEntities ent = new DevelopmentStorageDbEntities())
            {
                foreach (string queueName in deleteMessagesFromQueuesList)
                {
                    var query = from m in ent.Message
                                from mapping in ent.AcctQueueContainerMap
                                where mapping.QueueName == queueName
                                select m;

                    foreach (Message result in query)
                    {
                        ent.DeleteObject(result);
                    }

                    try
                    {
                        ent.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ApplicationEnvironment.LogError(ex.Message);
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Texts the from49 valid.
        /// </summary>
        [TestMethod]
        public void TextFrom49Valid()
        {
            this.PutOnMessageQueue(new CloudQueueMessage("497510708710%8888%luft Warszawa"));
            string messageToBeSent = this.LoopUntilReceived();
            Assert.IsTrue(messageToBeSent.StartsWith("Der Luftqualitatsindex", StringComparison.OrdinalIgnoreCase), "Message to be sent: " + messageToBeSent);
        }

        /// <summary>
        /// Texts from 999 which is not valid.
        /// </summary>
        [TestMethod]
        public void TextFrom999NotValid()
        {
            this.PutOnMessageQueue(new CloudQueueMessage("9997510708710%8888%luft Warszawa"));
            string messageToBeSent = this.LoopUntilReceived();
            Assert.IsTrue(messageToBeSent.StartsWith("Der Luftqualitatsindex", StringComparison.OrdinalIgnoreCase), "Message to be sent: " + messageToBeSent);
        }

        /// <summary>
        /// Texts the from49 luft gibberish.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Luft", Justification = "Luft is spelled correctly, it's just not english"), TestMethod]
        public void TextFrom44LuftGibberish()
        {
            this.PutOnMessageQueue(new CloudQueueMessage("447510708710%8888%luft dfgsdfgsdfgsdfgsdfg"));
            string messageToBeSent = this.LoopUntilReceived();
            Assert.AreEqual("STÖRUNG - die Position wurde nicht erkannt. Verwenden Sie bitte dieses Format: LUFT Wiesbaden, Hessen", messageToBeSent, true, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Texts the from1 air.
        /// </summary>
        [TestMethod]
        public void TextAtlantaFrom1Air()
        {
            this.PutOnMessageQueue(new CloudQueueMessage("+11234567890%8888%air atlanta"));
            string messageToBeSent = this.LoopUntilReceived();
            Assert.AreEqual(messageToBeSent, "ERROR - There is no data for this location. Please note that Eye on Earth currently only provides data for the 32 EEA member countries", true, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Texts with brackets
        /// </summary>
        [TestMethod]
        public void TestFromBracketsSpaces()
        {
            this.PutOnMessageQueue(new CloudQueueMessage("+4 (979) 2708710%8888%luft warszawa"));
            string messageToBeSent = this.LoopUntilReceived();
            Assert.IsTrue(messageToBeSent.StartsWith("Der Luftqualitatsindex", StringComparison.OrdinalIgnoreCase), "Message to be sent: " + messageToBeSent);
        }

        /// <summary>
        /// Tests the heat map tile server.
        /// </summary>
        [TestMethod]
        public void TestHeatMapTileServer()
        {
            this.PutOnHeatMapQueue(new CloudQueueMessage("soisoisoi"));
            string response = this.LoopUntilReceived();
            Assert.AreEqual("success", response);
        }

        /// <summary>
        /// Loops the until message on the test queue is received.
        /// </summary>
        /// <returns>The result of the test - the message that would be sent</returns>
        private string LoopUntilReceived()
        {
            CloudQueueMessage msg;
            DateTime start = DateTime.UtcNow;

            while (true)
            {
                msg = this.testQueue.GetMessage();

                if (msg != null)
                {
                    this.testQueue.DeleteMessage(msg);
                    
                    return msg.AsString;
                }
                else
                {
                    // if looping for more than a minute
                    if (start - DateTime.UtcNow > new TimeSpan(0, 1, 0))
                    {
                        return "Loop timeout";
                    }
                    else
                    {
                        Thread.Sleep(2000);
                    }
                }
            }
        }

        /// <summary>
        /// Puts the on message queue.
        /// </summary>
        /// <param name="message">The message.</param>
        private void PutOnMessageQueue(CloudQueueMessage message)
        {
            new AirWatchWorkerRole().OnStart();

            CloudQueue shortMessageQueue = this.queueStorage.GetQueueReference("shortmessagequeue");
            shortMessageQueue.CreateIfNotExist();

            shortMessageQueue.AddMessage(message);
        }

        /// <summary>
        /// Puts the on heat map queue.
        /// </summary>
        /// <param name="message">The message.</param>
        private void PutOnHeatMapQueue(CloudQueueMessage message)
        {
            new AirWatchWorkerRole().OnStart();

            CloudQueue heatTileQueue = this.queueStorage.GetQueueReference("heatmaptileserverqueue");
            heatTileQueue.CreateIfNotExist();

            heatTileQueue.AddMessage(message);
        }
    }
}
