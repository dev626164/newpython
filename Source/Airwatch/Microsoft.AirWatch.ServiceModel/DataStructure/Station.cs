﻿//-----------------------------------------------------------------------
// <copyright file="Station.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>14 October 2009</date>
// <summary>Extension properties for station</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    /// <summary>
    /// Extension properties for station
    /// </summary>
    public partial class Station
    {
        /// <summary>
        /// backing field for CanRate
        /// </summary>
        private bool canRate;

        /// <summary>
        /// Gets or sets a value indicating whether the user can rate.
        /// </summary>
        /// <value><c>true</c> if this instance can rate; otherwise, <c>false</c>.</value>
        public bool CanRate 
        {
            get
            {
                return this.canRate;
            }
            
            set
            {
                this.canRate = value;
                this.RaisePropertyChanged("CanRate");
            }
        }
    }
}
