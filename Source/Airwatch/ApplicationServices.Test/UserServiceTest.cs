﻿//-----------------------------------------------------------------------
// <copyright file="UserServiceTest.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Test for the UserService.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices.Test
{
    #region Using

    using System;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    #endregion

    /// <summary>
    /// Test for the UserService.
    /// </summary>
    [TestClass]
    public class UserServiceTest
    {
        /// <summary>
        /// Context of the Tests
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        /// <summary>
        /// Tests the creation of a user
        /// </summary>
        [TestMethod]
        public void TestCreationOfUser()
        {
            Guid guid = Guid.NewGuid();
            User user = ServiceProvider.UserService.CreateUser(guid);

            Assert.IsNotNull(user);
            Assert.AreEqual(guid, user.UserId);
        }

        /// <summary>
        /// Tests the creation and deletion of a pushpin.
        /// </summary>
        [TestMethod]
        public void TestCreationAndDeletionOfPushpin()
        {
            Pushpin p = ServiceProvider.PushpinService.GetPushpin(1, 30);

            ServiceProvider.PushpinService.DeletePushpin(p.PushpinId);
        }
    }
}
