﻿//-----------------------------------------------------------------------
// <copyright file="TileLayer.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>24-04-2009</date>
// <summary>Representation of a data overlay.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.UICommon
{
    #region Using

    using System;
    using System.ComponentModel;

    #endregion

    /// <summary>
    /// Representation of a data overlay.
    /// </summary>
    public class TileLayer : INotifyPropertyChanged
    {      
        /// <summary>
        /// Backing field for Visible Property.
        /// </summary>
        private bool visible;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        /// <value>The URI to the tile layer.</value>
        public Uri Uri { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TileLayer"/> is visible.
        /// </summary>
        /// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
        public bool Visible 
        {
            get
            {
                return this.visible;
            }
            
            set
            {
                this.visible = value;

                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("Visible"));
                }
            }
        }
    }
}
