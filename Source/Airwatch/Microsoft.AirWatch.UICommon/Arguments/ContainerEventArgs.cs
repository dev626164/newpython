﻿//-----------------------------------------------------------------------
// <copyright file="ContainerEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Stuart McCarthy</author>
// <email>smccar@microsoft.com</email>
// <date>07-07-2009</date>
// <summary>To pass objects.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.UICommon
{
    #region Using
    
    using System;

    #endregion

    /// <summary>
    /// Event arguments containing user object as returned from User web service.
    /// </summary>
    public class ContainerEventArgs : EventArgs
    {
        /// <summary>
        /// Backing field for Container property.
        /// </summary>
        private object container;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerEventArgs"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public ContainerEventArgs(object container)
        {
            this.container = container;
        }

        /// <summary>
        /// Gets the container.
        /// </summary>
        /// <value>The container.</value>
        public object Container
        {
            get
            {
                return this.container;
            }
        }
    }
}
