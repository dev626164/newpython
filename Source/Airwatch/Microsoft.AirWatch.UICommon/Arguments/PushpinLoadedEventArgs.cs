﻿//-----------------------------------------------------------------------
// <copyright file="PushpinLoadedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Stuart McCarthy</author>
// <email>smccar@microsoft.com</email>
// <date>07-07-2009</date>
// <summary>Event arguments containing pushpin object as returned from Pushpin web service.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.UICommon
{
    #region Using
    
    using System;
    using Microsoft.AirWatch.Core.Data;

    #endregion

    /// <summary>
    /// Event arguments containing user object as returned from User web service.
    /// </summary>
    public class PushpinLoadedEventArgs : EventArgs
    {
        /// <summary>
        /// Backing field for Pushpin property.
        /// </summary>
        private Pushpin pushpin;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushpinLoadedEventArgs"/> class.
        /// </summary>
        /// <param name="pushpin">The pushpin.</param>
        public PushpinLoadedEventArgs(Pushpin pushpin)
        {
            this.pushpin = pushpin;
        }

        /// <summary>
        /// Gets the pushpin.
        /// </summary>
        /// <value>The pushpin that has been loaded.</value>
        public Pushpin Pushpin
        {
            get
            {
                return this.pushpin;
            }
        }
    }
}
