﻿//-----------------------------------------------------------------------
// <copyright file="StationLoadedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>17-09-2009</date>
// <summary>Event arguments containing station object as returned from Station web service.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.UICommon
{
    #region Using
    
    using System;
    using Microsoft.AirWatch.Core.Data;

    #endregion

    /// <summary>
    /// Event arguments containing station object as returned from Station web service.
    /// </summary>
    public class StationLoadedEventArgs : EventArgs
    {
        /// <summary>
        /// Backing field for Station property.
        /// </summary>
        private Station station;

        /// <summary>
        /// Initializes a new instance of the <see cref="StationLoadedEventArgs"/> class.
        /// </summary>
        /// <param name="station">The station.</param>
        public StationLoadedEventArgs(Station station)
        {
            this.station = station;
        }

        /// <summary>
        /// Gets the station.
        /// </summary>
        /// <value>The station that has been loaded.</value>
        public Station Station
        {
            get
            {
                return this.station;
            }
        }
    }
}
