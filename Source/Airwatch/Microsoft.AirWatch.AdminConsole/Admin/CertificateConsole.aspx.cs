﻿//-----------------------------------------------------------------------
// <copyright file="CertificateConsole.aspx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>24/08/09</date>
// <summary>Certificate admin console</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AdminConsole.Admin
{
    using System;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Permissions;
    using Microsoft.AirWatch.Common;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;


    /// <summary>
    /// Certificate Admin Console
    /// </summary>
    public partial class CertificateConsole : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Files the upload button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        [EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        private void FileUploadButtonClick(object sender, EventArgs e)
        {
            this.errorBox.Text = string.Empty;

            if (this.fileUpload.HasFile)
            {
                byte[] entryContent = new byte[this.fileUpload.PostedFile.InputStream.Length];
                this.fileUpload.PostedFile.InputStream.Read(entryContent, 0, (int)this.fileUpload.PostedFile.InputStream.Length);

                X509Certificate2 cert = new X509Certificate2(entryContent);

                if (!cert.Verify())
                {
                    System.Diagnostics.Trace.TraceInformation("Tried uploading an invalid certificate: could not be verified");

                    this.errorBox.Text = "certificate invalid: could not be verified";
                }
                else
                {
                    X509Chain chain = new X509Chain();
                    chain.Build(cert);
                    if (chain.ChainStatus.Length > 0)
                    {
                        System.Diagnostics.Trace.TraceInformation("Tried uploading an invalid certificate: certificate chain has a length > 0");

                        this.errorBox.Text = "certificate invalid: certificate chain has a length > 0";
                    }
                    else
                    {
                        // need to ensure that this is created as private
                        var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
                        var blobClient = account.CreateCloudBlobClient();
                        var blobContainer = blobClient.GetContainerReference("dataentrycheck");
                        blobContainer.CreateIfNotExist();

                        var blob = blobContainer.GetBlobReference("entry");
                        blob.UploadByteArray(entryContent);
                        
                        //StorageAccountInfo info = StorageAccountInfo.GetDefaultBlobStorageAccountFromConfiguration();
                        //BlobStorage storage = BlobStorage.Create(info);
                        //BlobContainer container = storage.GetBlobContainer("dataentrycheck");

                        //if (container.DoesContainerExist() || container.CreateContainer(null, ContainerAccessControl.Private))
                        //{
                        //    BlobContents contents = new BlobContents(entryContent);
                        //    BlobProperties props = new BlobProperties("entry");

                        //    container.CreateBlob(props, contents, true);
                        //}
                    }
                }
            }
        }
    }
}
