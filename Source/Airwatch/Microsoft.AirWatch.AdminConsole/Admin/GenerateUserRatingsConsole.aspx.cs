﻿//-----------------------------------------------------------------------
// <copyright file="GenerateUserRatingsConsole.aspx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>04/11/09</date>
// <summary>Generate User Ratings - Console</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AdminConsole.Admin
{
    using System;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// Languages for User Interface console
    /// </summary>
    public partial class GenerateUserRatingsConsole : System.Web.UI.Page
    {
        /// <summary>
        /// Azure account from config
        /// </summary>
        private CloudStorageAccount queueAccount;

        /// <summary>
        /// Azure queue storage
        /// </summary>
        private CloudQueueClient queueClient;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private CloudQueue lightMapQueue;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.queueAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.queueAccount.CreateCloudQueueClient();

            this.lightMapQueue = queueClient.GetQueueReference("batchgenerate");
            this.lightMapQueue.CreateIfNotExist();
        }

        /// <summary>
        /// Handles the Click event of the GenerateAggregationButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void GenerateAggregationButton_Click(object sender, EventArgs e)
        {
            bool success = ServiceProvider.StationService.AttachUserRatingAggregateForWaterStations(5);
            if (success)
            {
                this.Status.Text = "Success";
            }
            else
            {
                this.Status.Text = "Failed";
            }
        }

        /// <summary>
        /// Handles the Click event of the stationAirButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void StationAirButton_Click(object sender, EventArgs e)
        {
            this.lightMapQueue.AddMessage(new CloudQueueMessage("AirStation"));
        }

        /// <summary>
        /// Handles the Click event of the stationWaterButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void StationWaterButton_Click(object sender, EventArgs e)
        {
            this.lightMapQueue.AddMessage(new CloudQueueMessage("WaterStation"));
        }

        /// <summary>
        /// Handles the Click event of the userAirButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void UserAirButton_Click(object sender, EventArgs e)
        {
            this.lightMapQueue.AddMessage(new CloudQueueMessage("UserFeedbackAir"));
        }

        /// <summary>
        /// Handles the Click event of the userWaterButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void UserWaterButton_Click(object sender, EventArgs e)
        {
            this.lightMapQueue.AddMessage(new CloudQueueMessage("UserFeedbackWater"));
        }
    }
}
