﻿//-----------------------------------------------------------------------
// <copyright file="TableStorageConsole.aspx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>19/06/09</date>
// <summary>Table storage admin console</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AdminConsole.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.Services.Client;
    using System.Globalization;
    using System.Linq;
    using System.Web.UI.WebControls;
    using System.Xml;
    using System.Xml.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.WindowsAzure;

    /// <summary>
    /// Admin table storage console
    /// </summary>
    public partial class TableStorageConsole : System.Web.UI.Page
    {

        private LocalisationEntityTable localisationEntityTable;

        public TableStorageConsole()
        {
            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.localisationEntityTable = new LocalisationEntityTable(account.TableEndpoint.ToString(), account.Credentials);
        }

        /// <summary>
        /// Builds the key.
        /// </summary>
        /// <param name="dataItem">The data item.</param>
        /// <returns>The localisation key</returns>
        public static string BuildKey(object dataItem)
        {
            LocalisationEntity localisation = dataItem as LocalisationEntity;
            return string.Format(CultureInfo.InvariantCulture, "{0}|{1}", localisation.PartitionKey, localisation.RowKey);
        }

        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.localizationsGrid.DataSource = this.localisationEntityTable.Localisations;
            this.localizationsGrid.DataBind();
        }

        /// <summary>
        /// Handles the Click event of the addButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AddButton_Click(object sender, EventArgs e)
        {
            var svc = this.localisationEntityTable;

            svc.AddObject("Localisations", new LocalisationEntity(this.CultureTextBox.Text, this.countryCodeTextBox.Text, this.airReplyStringTextBox.Text, this.waterReplyStringTextBox.Text, this.airNameTextBox.Text, this.waterNameTextBox.Text, this.incorrectRequestStringTextBox.Text, this.AirUnknownLocationErrorStringTextBox.Text, this.WaterUnknownLocationErrorStringTextBox.Text, this.NoMeasurementsForLocationStringTextBox.Text));

            svc.SaveChanges();
        }

        /// <summary>
        /// Handles the Click event of the populateTestDataButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PopulateTestDataButton_Click(object sender, EventArgs e)
        {
            var svc = this.localisationEntityTable;

            List<LocalisationEntity> testLocalisationList = new List<LocalisationEntity>();
            testLocalisationList.Add(new LocalisationEntity(
                "en-GB",
                "44",
                "The air quality index for {0} is {1}",
                "The water quality index for {0} is {1}",
                "air",
                "water",
                "ERROR – please use the following format: AIR/WATER Reading, UK etc...",
                "ERROR – The location received has not been recognised. Please use the following format: AIR Reading, UK etc...",
                "ERROR – The location received has not been recognised. Please use the following format: WATER Reading, UK etc...",
                "ERROR - There is no data for this location. Please note that Eye on Earth currently only provides data for the 32 EEA member countries"));

            testLocalisationList.Add(new LocalisationEntity(
                "de-DE",
                "49",
                "Der Luftqualitatsindex fur {0} ist {1}",
                "Der Wasserqualitatsindex fur {0} ist {1}",
                "luft",
                "wasser",
                "STÖRUNG - verwenden Sie bitte dieses Format: LUFT/WASSER Wiesbaden, Hessen",
                "STÖRUNG - Die Position wurde nicht erkannt. Verwenden Sie bitte dieses Format: LUFT Wiesbaden, Hessen",
                "STÖRUNG - Die Position wurde nicht erkannt. Verwenden Sie bitte dieses Format: WASSER Wiesbaden, Hessen",
                "STÖRUNG - Es gibt keine Daten für diesen Standort. Bitte beachten Sie, dass Eye on Earth derzeit nur Daten für den 32 EEA Länder"));

            testLocalisationList.Add(new LocalisationEntity(
                "pl-PL",
                "48",
                "Jakosc powietrza dla {0} wynosi {1}",
                "Jakosc wody dla {0} wynosi {1}",
                "powietrze",
                "woda",
                "BLAD - prosze uzyc nastepujacego formatu: POWIETRZE/WODA Wroclaw, Dolnoslaskie",
                "BLAD - Lokalizacja nie zostala rozpoznana. prosze uzyc nastepujacego formatu: POWIETRZE Wroclaw, Dolnoslaskie",
                "BLAD - Lokalizacja nie zostala rozpoznana. prosze uzyc nastepujacego formatu: WODA Wroclaw, Dolnoslaskie",
                "BLAD - Brak danych dla tej lokalizacji. Prosze pamietac, ze obecnie Eye on Earth dostarcza danych jedynie dla 32 panstw czlonkowskich EEA"));

            foreach (LocalisationEntity local in testLocalisationList)
            {
                svc.AddObject("Localisations", local);
            }

            svc.SaveChanges();
        }

        /// <summary>
        /// Handles the Click event of the deleteButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;
            string[] p = button.CommandArgument.Split(new char[] { '|' });
            string partitionKey = p[0];
            string rowKey = p[1];

            LocalisationEntity localisationToDelete = new LocalisationEntity()
            {
                PartitionKey = partitionKey,
                RowKey = rowKey
            };

            LocalisationEntityTable localisationTable = this.localisationEntityTable;

            localisationTable.AttachTo("Localisations", localisationToDelete, "*");
            localisationTable.DeleteObject(localisationToDelete);

            try
            {
                localisationTable.SaveChanges();
            }
            catch (DataServiceRequestException)
            {
                //// handle missing localisation
            }
        }

        /// <summary>
        /// Files the upload button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void FileUploadButtonClick(object sender, EventArgs e)
        {
            if (this.fileUpload.HasFile)
            {
                string fileExtension = System.IO.Path.GetExtension(this.fileUpload.FileName).ToUpperInvariant();

                if (fileExtension.Equals(".XML"))
                {
                    using (XmlReader reader = XmlReader.Create(this.fileUpload.PostedFile.InputStream))
                    {
                        XElement element = XElement.Load(reader);

                        var allContents = from el in element.Elements()
                                          select new
                                          {
                                              PartitionKey = el.Element("PartitionKey").Value,
                                              Culture = el.Element("RowKey").Value,
                                              CountryCode = el.Element("CountryCode").Value,
                                              AirReplyString = el.Element("AirReplyString").Value,
                                              WaterReplyString = el.Element("WaterReplyString").Value,
                                              AirName = el.Element("AirName").Value,
                                              WaterName = el.Element("WaterName").Value,
                                              IncorrectRequestString = el.Element("IncorrectRequestString").Value,
                                              AirUnknownLocationErrorString = el.Element("AirUnknownLocationErrorString").Value,
                                              WaterUnknownLocationErrorString = el.Element("WaterUnknownLocationErrorString").Value,
                                              NoMeasurementsForLocationString = el.Element("NoMeasurementsForLocationString").Value
                                          };
                        var svc = this.localisationEntityTable;

                        foreach (LocalisationEntity existingLoc in svc.Localisations)
                        {
                            // svc.AttachTo("Localisations", existingLoc, "*");
                            svc.DeleteObject(existingLoc);
                        }

                        try
                        {
                            svc.SaveChanges();
                        }
                        catch (Exception)
                        {
                            throw;
                        }

                        foreach (var str in allContents)
                        {
                            svc.AddObject("Localisations", new LocalisationEntity(str.Culture, str.CountryCode, str.AirReplyString, str.WaterReplyString, str.AirName, str.WaterName, str.IncorrectRequestString, str.AirUnknownLocationErrorString, str.WaterUnknownLocationErrorString, str.NoMeasurementsForLocationString));
                        }

                        try
                        {
                            svc.SaveChanges();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    // file not uploaded
                }
            }
        }
    }
}