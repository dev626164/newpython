﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LanguagesUserInterfaceConsole.aspx.cs" MasterPageFile="~/admin/Site.master" Inherits="Microsoft.AirWatch.AdminConsole.Admin.LanguagesUserInterfaceConsole" %>

<asp:Content ID="Content2" ContentPlaceHolderID="titleContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <asp:Panel ID="Panel1" CssClass="bluePanel" runat="server" Width="100%">
        <h1>
            Eye on Earth Azure Languages Table</h1>
        <table>
            <tr>
                <td align="right">
                    Culture:
                </td>
                <td align="left">
                    <asp:TextBox ID="CultureTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    StringID:
                </td>
                <td align="left">
                    <asp:TextBox ID="StringIdTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Value:
                </td>
                <td align="left">
                    <asp:TextBox ID="ValueTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <asp:Button ID="submitButton" Text="Add a Language" OnClick="AddButton_Click"
                        runat="server" />
                </td>
            </tr>
        </table>
        <p />
        <asp:TextBox ID="SearchBox" runat="server" />
        &nbsp;
        <asp:Button ID="filterButton" runat="server" OnClick="FilterButton_Click" Text="Filter by culture" />
        <asp:Button ID="stopFilterButton" runat="server" OnClick="StopFilterButton_Click" Text="Remove filter" />
        <p />
        <asp:DataGrid ID="LanguagesGrid" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateColumn>
                    <HeaderStyle BackColor="Azure" Font-Bold="true" Width="200" />
                    <HeaderTemplate>
                        <asp:Label ID="localisationsGridCultureHeader" runat="server" Text="Culture" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "PartitionKey") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <HeaderStyle BackColor="Azure" Font-Bold="true" Width="160" />
                    <HeaderTemplate>
                        <asp:Label ID="localisationsGridStringIdHeader" runat="server" Text="StringID" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "RowKey") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                    <HeaderTemplate>
                        <asp:Label ID="localisationsGridValueHeader" runat="server" Text="Value" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "Value"))%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <HeaderStyle BackColor="Azure" Font-Bold="true" Width="40" />
                    <HeaderTemplate>
                        <asp:Label ID="GridDeleteHeader" runat="server" Text="Delete" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="deleteLanguageButton" runat="server" CommandArgument='<%# BuildKey(Container.DataItem) %>'
                            OnClick="DeleteButton_Click">
                            <img alt="delete" style="border-width:0;" src="images/close.png" />
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <asp:HyperLink ID="previousLink" runat="server" Text="&laquo; Previous Page" NavigateUrl="javascript:history.go(-1)" />
        <asp:HyperLink ID="nextLink" runat="server" Text="Next Page &raquo;" />
    </asp:Panel>
    <asp:Button ID="fileUploadButton" runat="server" OnClick="FileUploadButtonClick" Text="DeployLanguages" />
    <asp:FileUpload ID="fileUpload" runat="server" />
</asp:Content>