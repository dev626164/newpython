﻿// <copyright file="Site.Master.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>16-07-2009</date>
// <summary>Site.Master code behind</summary>
namespace Microsoft.AirWatch.AdminConsole.Admin
{
    using System;

    /// <summary>
    /// Site.Master code behind
    /// </summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
