﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableStorageConsole.aspx.cs" MasterPageFile="~/admin/Site.master" Inherits="Microsoft.AirWatch.AdminConsole.Admin.TableStorageConsole" %>

<asp:Content ID="Content2" ContentPlaceHolderID="titleContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div>
        <h1>AirWatch Azure tables</h1>
        <table>
            <tr>
                <td align="right">
                    Culture:
                </td>
                <td align="left">
                    <asp:TextBox ID="CultureTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Country code:
                </td>
                <td align="left">
                    <asp:TextBox ID="countryCodeTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    AirName:
                </td>
                <td align="left">
                    <asp:TextBox ID="airNameTextBox" runat="server" />
                </td>
            </tr>
                        <tr>
                <td align="right">
                    WaterName:
                </td>
                <td align="left">
                    <asp:TextBox ID="waterNameTextBox" runat="server" />
                </td>
            </tr>
                        <tr>
                <td align="right">
                    AirReplyString:
                </td>
                <td align="left">
                    <asp:TextBox ID="airReplyStringTextBox" runat="server" />
                </td>
            </tr>
                        <tr>
                <td align="right">
                    WaterReplyString:
                </td>
                <td align="left">
                    <asp:TextBox ID="waterReplyStringTextBox" runat="server" />
                </td>
            </tr>
                        <tr>
                <td align="right">
                    IncorrectRequestString:
                </td>
                <td align="left">
                    <asp:TextBox ID="incorrectRequestStringTextBox" runat="server" />
                </td>
            </tr><tr>
                <td align="right">
                    AirErrorString:
                </td>
                <td align="left">
                    <asp:TextBox ID="AirUnknownLocationErrorStringTextBox" runat="server" />
                </td>
            </tr><tr>
                <td align="right">
                    WaterErrorString:
                </td>
                <td align="left">
                    <asp:TextBox ID="WaterUnknownLocationErrorStringTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    NoMeasurementsForLocationString:
                </td>
                <td align="left">
                    <asp:TextBox ID="NoMeasurementsForLocationStringTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <asp:Button ID="submitButton" Text="Add a localisation" OnClick="AddButton_Click" runat="server" />
                </td>
            </tr>
        </table>
        <p />
        <asp:DataGrid ID="localizationsGrid" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="200" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridCultureHeader" runat="server" Text="loc" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "PartitionKey") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="160" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridCountryCodeHeader" runat="server" Text="Culture" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "RowKey") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridAirNameHeader" runat="server" Text="AirName" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "AirName")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridWaterNameHeader" runat="server" Text="WaterName" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "WaterName")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridAirReplyStringHeader" runat="server" Text="AirReplyString" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "AirReplyString")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridWaterReplyStringHeader" runat="server" Text="WaterReplyString" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "WaterReplyString")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridIncorrectRequestStringHeader" runat="server" Text="IncorrectRequestString" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "IncorrectRequestString")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridIncorrectRequestStringHeader" runat="server" Text="AirUnknownLocationErrorString" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "AirUnknownLocationErrorString"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridIncorrectRequestStringHeader" runat="server" Text="WaterUnknownLocationErrorString" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "WaterUnknownLocationErrorString"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn>
                            <HeaderStyle BackColor="Azure" Font-Bold="true" Width="120" />
                            <HeaderTemplate>
                                <asp:Label ID="localisationsGridIncorrectRequestStringHeader" runat="server" Text="NoMeasurementsForLocationString" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "NoMeasurementsForLocationString"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <HeaderStyle BackColor="Azure" Font-Bold="true"  Width="40"/>
                    <HeaderTemplate>
                        <asp:Label ID="booksGridDeleteHeader" runat="server" Text="" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="deleteLocalisationButton" runat="server" CommandArgument='<%# BuildKey(Container.DataItem) %>'
                             OnClick="DeleteButton_Click">
                            <img alt="delete" style="border-width:0;" src="images/close.png" />
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <asp:Button ID="fileUploadButton" runat="server" OnClick="FileUploadButtonClick" Text="DeployLanguages" />
    <asp:FileUpload ID="fileUpload" runat="server" />
    </asp:Content>
