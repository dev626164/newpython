﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateUserRatingsConsole.aspx.cs" MasterPageFile="~/admin/Site.master" Inherits="Microsoft.AirWatch.AdminConsole.Admin.GenerateUserRatingsConsole" %>

<asp:Content ID="Content2" ContentPlaceHolderID="titleContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <asp:Panel ID="Panel1" CssClass="bluePanel" runat="server" Width="100%">
        <h1>
            Eye on Earth Historic User Rating Aggregations</h1>
        <p />
        &nbsp;
        <asp:Button ID="GenerateAggregationButton" runat="server" OnClick="GenerateAggregationButton_Click" Text="Generate Historic Aggregations" />
        <asp:Label ID="Status" runat="server" />
        
        <h1>Batch Generate Tiles</h1>
        <asp:Button ID="stationAirButton" runat="server" Text="Batch Air Station" OnClick="StationAirButton_Click"/>
        <asp:Button ID="stationWaterButton" runat="server" Text="Batch Water Station" OnClick="StationWaterButton_Click" />
        <asp:Button ID="userAirButton" runat="server" Text="Batch Air User Feedback" OnClick="UserAirButton_Click" />
        <asp:Button ID="userWaterButton" runat="server" Text="Batch Air User Feedback" OnClick="UserWaterButton_Click" />        
        <p />
        <asp:HyperLink ID="previousLink" runat="server" Text="&laquo; Previous Page" NavigateUrl="javascript:history.go(-1)" />
        <asp:HyperLink ID="nextLink" runat="server" Text="Next Page &raquo;" />
    </asp:Panel>
    </asp:Content>