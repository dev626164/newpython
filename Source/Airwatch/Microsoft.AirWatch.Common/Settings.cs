﻿// <copyright file="Settings.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>Settings 
// </summary>
namespace Microsoft.AirWatch.Common
{
    using System.Configuration;
    using System.Web.Configuration;
    using Microsoft.WindowsAzure.ServiceRuntime;
    
    /// <summary>
    /// Application settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        internal Settings()
        {
        }

        /// <summary>
        /// Gets the <see cref="System.String"/> with the specified name.
        /// </summary>
        /// <value></value>
        /// <param name="name">The name of the setting</param>
        public string this[string name]
        {
            get
            {
                switch (ApplicationEnvironment.ApplicationModel)
                {
                    case ApplicationModel.Cloud:
                        return RoleEnvironment.GetConfigurationSettingValue(name);
                    case ApplicationModel.Desktop:
                        return ConfigurationManager.AppSettings[name];
                    case ApplicationModel.Web:
                        return WebConfigurationManager.AppSettings[name];
                    default:
                        return null;
                }
            }
        }
    }
}
