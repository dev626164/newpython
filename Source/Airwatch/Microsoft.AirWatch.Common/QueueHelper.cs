﻿//-----------------------------------------------------------------------
// <copyright file="QueueHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>12 August 2009</date>
// <summary>Helper Class and methods for working with Queues.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Common
{
    using System;
    using System.IO;
    using System.Xml.Serialization;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// Helper Class and methods for working with Queues.
    /// </summary>
    public static class QueueHelper
    {
        /// <summary>
        /// Serializes a given object into a message for transfer on the queue.
        /// </summary>
        /// <param name="originalMessage">Original class to be transported.</param>
        /// <returns>Message to add to queue.</returns>
        public static CloudQueueMessage SerializeMessage(object originalMessage)
        {
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xmlSerializer = new XmlSerializer(originalMessage.GetType());
            xmlSerializer.Serialize(memoryStream, originalMessage);

            return new CloudQueueMessage(memoryStream.ToArray());
        }

        /// <summary>
        /// Deserializes a given byte array from a message.
        /// </summary>
        /// <param name="serializedMessage">Message contents that we want to deserialize.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <returns>The deserialized data from the message.</returns>
        public static object DeserializeMessage(CloudQueueMessage serializedMessage, Type contentType)
        {
            MemoryStream memoryStream = new MemoryStream(serializedMessage.AsBytes);
            XmlSerializer xmlSerializer = new XmlSerializer(contentType);
            object deserializedMessage = xmlSerializer.Deserialize(memoryStream);

            return deserializedMessage;
        }
    }
}
