﻿// <copyright file="RoleManagerLogger.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>RoleManagerLogger 
// </summary>
namespace Microsoft.AirWatch.Common
{
    using System.Diagnostics;
    using System;
    using System.Globalization;

    /// <summary>
    /// Role manager logger
    /// </summary>
    public class RoleManagerLogger : LogPrimitives
    {
        /// <summary>
        /// Logs the alert.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogAlert(string msg)
        {
            WriteMessage("Alert", msg);
            System.Diagnostics.Debugger.Break();
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogError(string msg)
        {
            WriteMessage("Error", msg);
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogWarning(string msg)
        {
            WriteMessage("Warning", msg);
        }

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogInformation(string msg)
        {
            WriteMessage("Information", msg);
        }

        /// <summary>
        /// Initializes static members of the {Type name} class.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogVerbose(string msg)
        {
            WriteMessage("Verbose", msg);
        }

        /// <summary>
        /// Writes the message.
        /// </summary>
        /// <param name="tag">The message tag.</param>
        /// <param name="message">The message.</param>
        private static void WriteMessage(string tag, string message)
        {
            System.Diagnostics.Trace.WriteLine(String.Format(CultureInfo.InvariantCulture, "{0,02} {1,12}: {2}", System.Threading.Thread.CurrentThread.ManagedThreadId, tag, message));
        }
    }
}
