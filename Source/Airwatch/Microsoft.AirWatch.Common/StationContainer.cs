﻿//-----------------------------------------------------------------------
// <copyright file="StationContainer.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Stuart McCarthy</author>
// <email>smccar@microsoft.com</email>
// <date>06/08/09</date>
// <summary>Class for returning station requests to the client</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Common
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class for returning station requests to the client
    /// </summary>
    [DataContract]
    public class StationContainer
    {
        /// <summary>
        /// Backing field for StationId
        /// </summary>
        private int stationId;

        /// <summary>
        /// Backing field for EuropeanCode
        /// </summary>
        private string europeanCode;

        /// <summary>
        /// Backing field for Quality Index
        /// </summary>
        private int? qualityIndex;

        /// <summary>
        /// Backing field for Latitude
        /// </summary>
        private decimal latitude;

        /// <summary>
        /// Backing field for Longitude
        /// </summary>
        private decimal longitude;

        /// <summary>
        /// Backing field for StationPurpose
        /// </summary>
        private string stationPurpose;

        /// <summary>
        /// Gets or sets the station id.
        /// </summary>
        /// <value>The station id.</value>
        [DataMember]
        public int StationId
        {
            get { return this.stationId; }
            set { this.stationId = value; } 
        }

        /// <summary>
        /// Gets or sets the station id.
        /// </summary>
        /// <value>The station id.</value>
        [DataMember]
        public string EuropeanCode
        {
            get { return this.europeanCode; }
            set { this.europeanCode = value; }
        }

        /// <summary>
        /// Gets or sets the index of the quality.
        /// </summary>
        /// <value>The index of the quality.</value>
        [DataMember]
        public int? QualityIndex
        {
            get { return this.qualityIndex; }
            set { this.qualityIndex = value; }
        }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        [DataMember]
        public decimal Latitude
        {
            get { return this.latitude; }
            set { this.latitude = value; }
        }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        [DataMember]
        public decimal Longitude
        {
            get { return this.longitude; }
            set { this.longitude = value; }
        }

        /// <summary>
        /// Gets or sets the type of the station.
        /// </summary>
        /// <value>The type of the station.</value>
        [DataMember]
        public string StationPurpose
        {
            get { return this.stationPurpose; }
            set { this.stationPurpose = value; }
        }
    }
}
