﻿// <copyright file="ConsoleLogger.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>ConsoleLogger 
// </summary>
namespace Microsoft.AirWatch.Common
{
    using System;
    using System.Diagnostics;
    using System.Globalization;

    /// <summary>
    /// Logs to the console / output
    /// </summary>
    public class ConsoleLogger : LogPrimitives
    {
        /// <summary>
        /// Default console colour
        /// </summary>
        private static ConsoleColor defaultColor = System.Console.ForegroundColor;

        /// <summary>
        /// Logs the alert.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogAlert(string msg)
        {
            WriteMessage(ConsoleColor.Magenta, "Alert", msg);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogError(string msg)
        {
            WriteMessage(ConsoleColor.Red, "Error", msg);
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogWarning(string msg)
        {
            WriteMessage(ConsoleColor.Yellow, "Warning", msg);
        }

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogInformation(string msg)
        {
            WriteMessage(ConsoleColor.White, "Information", msg);
        }

        /// <summary>
        /// Logs the verbose.
        /// </summary>
        /// <param name="msg">The message.</param>
        public override void LogVerbose(string msg)
        {
            WriteMessage(defaultColor, "Verbose", msg);
        }

        /// <summary>
        /// Writes the message.
        /// </summary>
        /// <param name="color">The color.</param>
        /// <param name="tag">The message tag.</param>
        /// <param name="message">The message.</param>
        private static void WriteMessage(ConsoleColor color, string tag, string message)
        {
            System.Console.ForegroundColor = color;
            Debug.WriteLine(String.Format(CultureInfo.InvariantCulture, "{0} {1}: {2}", System.DateTime.Now, tag, message));
        }
    }
}
