﻿//-----------------------------------------------------------------------
// <copyright file="MapImageHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>24/08/2009</date>
// <summary>Helper class for the image generation, will need to be refactored.</summary>
//-----------------------------------------------------------------------
namespace Microsoft.AirWatch.Common
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Helper Class to Generate Tile Images.
    /// </summary>
    public sealed class MapImageHelper
    {
        /// <summary>
        /// Minimum Latitude.
        /// </summary>
        private static double minLatitude;

        /// <summary>
        /// Maximum Latitude.
        /// </summary>
        private static double maxLatitude;

        /// <summary>
        /// Minimum Longitude.
        /// </summary>
        private static double minLongitude;

        /// <summary>
        /// Maximum Longitude.
        /// </summary>
        private static double maxLongitude;

        /// <summary>
        /// Pixel X Minimum.
        /// </summary>
        private static double pixelXMin;

        /// <summary>
        /// Pixel Y Minimum.
        /// </summary>
        private static double pixelYMin;

        /// <summary>
        /// Store for the air location data.
        /// </summary>
        private static List<StationMinimal> airLocations;

        /// <summary>
        /// Store for the water location data.
        /// </summary>
        private static List<StationMinimal> waterLocations;

        /// <summary>
        /// Flag to identify if its the first load of the data.
        /// </summary>
        private static bool firstLoad = true;

        /// <summary>
        /// Prevents a default instance of the <see cref="MapImageHelper"/> class from being created.
        /// </summary>
        private MapImageHelper()
        {
        }

        /// <summary>
        /// Lats the long to pixel XY.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="levelOfDetail">The level of detail.</param>
        /// <param name="offsetX">The offset X.</param>
        /// <param name="offsetY">The offset Y.</param>
        /// <param name="pixelX">The pixel X.</param>
        /// <param name="pixelY">The pixel Y.</param>
        public static void LatLongToPixelXY(double latitude, double longitude, int levelOfDetail, double offsetX, double offsetY, out int pixelX, out int pixelY)
        {
            latitude = Clip(latitude, minLatitude, maxLatitude);
            longitude = Clip(longitude, minLongitude, maxLongitude);

            double x = (longitude + 180) / 360;
            double sinLatitude = Math.Sin(latitude * Math.PI / 180);
            double y = 0.5 - (Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI));

            int mapSize = MapSize(levelOfDetail);

            pixelX = (int)(Clip((x * mapSize) + 0.5, 0, mapSize - 1) - offsetX);
            pixelY = (int)(Clip((y * mapSize) + 0.5, 0, mapSize - 1) - offsetY);
        }

        /// <summary>
        /// Determines the map width and height (in pixels) at a specified level
        /// of detail.
        /// </summary>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail).</param>
        /// <returns>The map width and height in pixels.</returns>
        public static int MapSize(int levelOfDetail)
        {
            return (int)256 << levelOfDetail;
        }

        /// <summary>
        /// Generates from quad key.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="type">The type of the layer.</param>
        /// <returns>
        /// Stream representing the image for the tile.
        /// </returns>
        public static Stream GenerateFromQuadKey(string quadKey, LightMapType type)
        {
            // Determine Zoom-Level
            int zoomLevel = quadKey.Length;

            // create binary quadkey
            string binaryQuadKey = CreateBinaryQuadKey(quadKey);

            // Break binary Quadtree in binary tileX and tileY
            string binaryTileY;
            string binaryTileX;
            GetBinaryTileCoOrdinates(binaryQuadKey, out binaryTileX, out binaryTileY);

            // Convert binary tileX and tileY to decimal tileX and tileY
            int decimalTileX = ConvertBinaryTileToDecimal(binaryTileX, zoomLevel);
            int decimalTileY = ConvertBinaryTileToDecimal(binaryTileY, zoomLevel);

            // Determine pixel positions of top right and bottom left.
            pixelXMin = decimalTileX * 256;
            double pixelXMax = ((decimalTileX + 1) * 256) - 1;
            pixelYMin = decimalTileY * 256;
            double pixelYMax = ((decimalTileY + 1) * 256) - 1;

            // Calculate upper left and lower right long / lat for the tile
            minLongitude = ConvertPixelPositionToLong(pixelXMin, zoomLevel);
            maxLongitude = ConvertPixelPositionToLong(pixelXMax, zoomLevel);
            maxLatitude = ConvertPixelPositionToLat(pixelYMin, zoomLevel);
            minLatitude = ConvertPixelPositionToLat(pixelYMax, zoomLevel);

            // Get the stations for the given tile.
            if (type != LightMapType.UserFeedback)
            {
                List<StationMinimal> stations = GetStationsInTile(minLongitude, maxLongitude, minLatitude, maxLatitude, type);
                return GenerateTileImage(stations, quadKey);
            }
            else
            {
                List<UserRating> stations = GetStationsInTile(minLongitude, maxLongitude, minLatitude, maxLatitude);
                return GenerateTileImage(stations, quadKey);
            }
        }

        /// <summary>
        /// Gets the stations in tile.
        /// </summary>
        /// <param name="longMin">The long min.</param>
        /// <param name="longMax">The long max.</param>
        /// <param name="latMin">The lat min.</param>
        /// <param name="latMax">The lat max.</param>
        /// <param name="type">The type of the layer.</param>
        /// <returns>A List of locations</returns>
        private static List<StationMinimal> GetStationsInTile(double longMin, double longMax, double latMin, double latMax, LightMapType type)
        {
            if (firstLoad)
            {
                airLocations = (from stations in ServiceProvider.StationService.GetStationsOfGivenType("Air")
                                select stations).ToList();

                waterLocations = (from stations in ServiceProvider.StationService.GetStationsOfGivenType("Water")
                                  select stations).ToList();

                firstLoad = false;
            }

            List<StationMinimal> locations = new List<StationMinimal>();

            switch (type)
            {
                case LightMapType.AirStation:
                    locations = (from stations in airLocations
                                 where stations.Latitude >= (decimal)latMin
                                            && stations.Latitude <= (decimal)latMax
                                            && stations.Longitude <= (decimal)longMax
                                            && stations.Longitude >= (decimal)longMin
                                 select stations).ToList();
                    break;

                case LightMapType.WaterStation:
                    locations = (from stations in waterLocations
                                 where stations.Latitude >= (decimal)latMin
                                            && stations.Latitude <= (decimal)latMax
                                            && stations.Longitude <= (decimal)longMax
                                            && stations.Longitude >= (decimal)longMin
                                 select stations).ToList();
                    break;
            }

            return locations;
        }

        /// <summary>
        /// Gets the stations in tile.
        /// </summary>
        /// <param name="longMin">The long min.</param>
        /// <param name="longMax">The long max.</param>
        /// <param name="latMin">The lat min.</param>
        /// <param name="latMax">The lat max.</param>
        /// <returns>Stations in the given tile.</returns>
        private static List<UserRating> GetStationsInTile(double longMin, double longMax, double latMin, double latMax)
        {
            List<UserRating> userRatings = (from userRating in ServiceProvider.RatingService.GetUserRatingsForBoundary(latMax, longMax, latMin, longMin)
                                            select userRating).ToList();

            return userRatings;
        }

        /// <summary>
        /// Creates the binary quad key.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>Binary version of the inputted quad key.</returns>
        private static string CreateBinaryQuadKey(string quadKey)
        {
            // Convert Quadtree in binary Quadtree
            string binaryQuadKey = string.Empty;
            string tempString = string.Empty;

            for (int i = 0; i <= quadKey.Length - 1; i++)
            {
                tempString = quadKey.Substring(quadKey.Length - i - 1, 1);
                switch (tempString)
                {
                    case "3":
                        binaryQuadKey = "11" + binaryQuadKey;
                        break;
                    case "2":
                        binaryQuadKey = "10" + binaryQuadKey;
                        break;
                    case "1":
                        binaryQuadKey = "01" + binaryQuadKey;
                        break;
                    case "0":
                        binaryQuadKey = "00" + binaryQuadKey;
                        break;
                }
            }

            return binaryQuadKey;
        }

        /// <summary>
        /// Gets the binary tile co ordinates.
        /// </summary>
        /// <param name="binaryQuadKey">The binary quad key.</param>
        /// <param name="binaryTileX">The binary tile X.</param>
        /// <param name="binaryTileY">The binary tile Y.</param>
        private static void GetBinaryTileCoOrdinates(string binaryQuadKey, out string binaryTileX, out string binaryTileY)
        {
            string tempString = string.Empty;
            binaryTileX = string.Empty;
            binaryTileY = string.Empty;

            for (int i = 0; i <= binaryQuadKey.Length - 1; i++)
            {
                tempString = binaryQuadKey.Substring(binaryQuadKey.Length - i - 1, 1);
                if (i % 2 == 0)
                {
                    binaryTileX = tempString + binaryTileX;
                }
                else
                {
                    binaryTileY = tempString + binaryTileY;
                }
            }
        }

        /// <summary>
        /// Converts the binary tile co-ordinate to decimal values.
        /// </summary>
        /// <param name="binaryTile">The binary tile co-ordinate.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Decimal value for the tile co-ordinate</returns>
        private static int ConvertBinaryTileToDecimal(string binaryTile, int zoomLevel)
        {
            int decimalTileValue = 0;
            string tempString = string.Empty;

            for (int i = 0; i <= zoomLevel - 1; i++)
            {
                tempString = binaryTile.Substring(binaryTile.Length - i - 1, 1);
                decimalTileValue = (int)(decimalTileValue + (int.Parse(tempString, System.Globalization.CultureInfo.InvariantCulture) * Math.Pow(2, i)));
            }

            return decimalTileValue;
        }

        /// <summary>
        /// Converts the pixel position to long.
        /// </summary>
        /// <param name="deciamlValue">The deciaml value.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Longitude Value for point.</returns>
        private static double ConvertPixelPositionToLong(double deciamlValue, int zoomLevel)
        {
            return ((deciamlValue * 360) / (256 * Math.Pow(2, zoomLevel))) - 180;
        }

        /// <summary>
        /// Converts the pixel position to lat.
        /// </summary>
        /// <param name="decimalValue">The decimal value.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Latitude value for given pixel.</returns>
        private static double ConvertPixelPositionToLat(double decimalValue, int zoomLevel)
        {
            return Math.Asin((Math.Exp((0.5 - (decimalValue / (256 * Math.Pow(2, zoomLevel)))) * 4 * Math.PI) - 1) / (Math.Exp((0.5 - (decimalValue / (256 * Math.Pow(2, zoomLevel)))) * 4 * Math.PI) + 1)) * (180 / Math.PI);
        }

        /// <summary>
        /// Generates the tile image.
        /// </summary>
        /// <param name="stations">The stations.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>Stream representing the image.</returns>
        private static Stream GenerateTileImage(List<StationMinimal> stations, string quadKey)
        {
            Bitmap mybitmap = new Bitmap(256, 256);

            Graphics graphics = Graphics.FromImage(mybitmap);

            int pixelx;
            int pixely;
            foreach (StationMinimal loc in stations)
            {
                LatLongToPixelXY((double)loc.Latitude, (double)loc.Longitude, quadKey.Length, pixelXMin, pixelYMin, out pixelx, out pixely);
                float pixelFloatX = (float)(pixelx - (quadKey.Length / 2.0));
                float pixelFloatY = (float)(pixely - (quadKey.Length / 2.0));

                Color color;

                if (loc.StationPurpose == "Air")
                {
                    color = Color.FromArgb(247, 255, 0);
                }
                else
                {
                    color = Color.FromArgb(0, 251, 255);
                }

                if (quadKey.Length <= 3)
                {
                    graphics.FillEllipse(new SolidBrush(color), pixelFloatX, pixelFloatY, 1, 1);
                }
                else
                {
                    graphics.FillEllipse(new SolidBrush(color), pixelFloatX, pixelFloatY, quadKey.Length / 2, quadKey.Length / 2);
                }
            }

            MemoryStream ms = new MemoryStream();
            mybitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;
            return ms;
        }

        /// <summary>
        /// Generates the tile image.
        /// </summary>
        /// <param name="stations">The stations.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>Stream representing the image.</returns>
        private static Stream GenerateTileImage(List<UserRating> stations, string quadKey)
        {
            Bitmap mybitmap = new Bitmap(256, 256);

            Graphics graphics = Graphics.FromImage(mybitmap);

            int pixelx;
            int pixely;
            foreach (UserRating loc in stations)
            {
                LatLongToPixelXY((double)loc.Latitude, (double)loc.Longitude, quadKey.Length, pixelXMin, pixelYMin, out pixelx, out pixely);
                float pixelFloatX = (float)(pixelx - (quadKey.Length / 2.0));
                float pixelFloatY = (float)(pixely - (quadKey.Length / 2.0));

                Color color;

                switch (loc.Rating)
                {
                    case 1:
                        color = Color.FromArgb(27, 144, 0);
                        break;
                    case 2:
                        color = Color.FromArgb(116, 255, 0);
                        break;
                    case 3:
                        color = Color.FromArgb(255, 255, 0);
                        break;
                    case 4:
                        color = Color.FromArgb(252, 135, 16);
                        break;
                    case 5:
                        color = Color.FromArgb(150, 0, 0);
                        break;
                    default:
                        color = Color.Transparent;
                        break;
                }

                if (quadKey.Length <= 3)
                {
                    graphics.FillEllipse(new SolidBrush(color), pixelFloatX, pixelFloatY, 1, 1);
                }
                else
                {
                    graphics.FillEllipse(new SolidBrush(color), pixelFloatX, pixelFloatY, quadKey.Length / 2, quadKey.Length / 2);
                }
            }

            MemoryStream ms = new MemoryStream();
            mybitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;
            return ms;
        }

        /// <summary>
        /// Clips a number to the specified minimum and maximum values.
        /// </summary>
        /// <param name="n">The number to clip.</param>
        /// <param name="minValue">Minimum allowable value.</param>
        /// <param name="maxValue">Maximum allowable value.</param>
        /// <returns>The clipped value.</returns>
        private static double Clip(double n, double minValue, double maxValue)
        {
            return Math.Min(Math.Max(n, minValue), maxValue);
        }
    }
}
