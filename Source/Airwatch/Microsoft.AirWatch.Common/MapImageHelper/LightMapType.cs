﻿//-----------------------------------------------------------------------
// <copyright file="LightMapType.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>03/09/2009</date>
// <summary>Enumeration for the types of light maps.</summary>
//-----------------------------------------------------------------------
namespace Microsoft.AirWatch.Common
{
    /// <summary>
    /// Enumeration for the types of light maps.
    /// </summary>
    public enum LightMapType
    {
        /// <summary>
        /// Air Station light map.
        /// </summary>
        AirStation,

        /// <summary>
        /// Water Staton light map.
        /// </summary>
        WaterStation,
        
        /// <summary>
        /// User Feedback light map.
        /// </summary>
        UserFeedback
    }
}
