﻿// <copyright file="AccountSettings.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>AccountSettings 
// </summary>
namespace Microsoft.AirWatch.Common
{
    using System;
    using System.Configuration;
    using System.Web.Configuration;
    using Microsoft.WindowsAzure;
    
    /// <summary>
    /// Settings of the storage account
    /// </summary>
    public class AccountSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountSettings"/> class.
        /// </summary>
        internal AccountSettings()
        {
        }

        /// <summary>
        /// Gets the <see cref="Microsoft.Samples.ServiceHosting.StorageClient.StorageAccountInfo"/> with the specified name.
        /// </summary>
        /// <param name="name">The name of the setting</param>
        /// <value></value>
        public CloudStorageAccount this[string name]
        {
            get
            {
                switch (ApplicationEnvironment.ApplicationModel)
                {
                    // will need to double check what is happening here.
                    case ApplicationModel.Cloud:
                        return CloudStorageAccount.FromConfigurationSetting(name);
                    case ApplicationModel.Desktop:
                        return new CloudStorageAccount(new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings["AccountName"], ConfigurationManager.AppSettings["AccountSharedKey"]), false);
                    case ApplicationModel.Web:
                        return new CloudStorageAccount(new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings["AccountName"], ConfigurationManager.AppSettings["AccountSharedKey"]), false);
                    default:
                        return null;
                }
            }
        }
    }
}
