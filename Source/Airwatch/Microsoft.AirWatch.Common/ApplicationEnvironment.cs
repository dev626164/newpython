﻿// <copyright file="ApplicationEnvironment.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>ApplicationEnvironment 
// </summary>
namespace Microsoft.AirWatch.Common
{
    using System;
    using System.Globalization;
    using Microsoft.WindowsAzure.ServiceRuntime;

    /// <summary>
    /// Mode of application
    /// </summary>
    public enum ApplicationModel
    {
        /// <summary>
        /// In the web environment
        /// </summary>
        Web,

        /// <summary>
        /// Test environment
        /// </summary>
        Desktop,

        /// <summary>
        /// Cloud / Development fabric environment
        /// </summary>
        Cloud
    }

    /// <summary>
    /// Environment of the application
    /// </summary>
    public static class ApplicationEnvironment
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static LogPrimitives logger;

        /// <summary>
        /// Initializes static members of the <see cref="ApplicationEnvironment"/> class.
        /// </summary>
        static ApplicationEnvironment()
        {
            if (RoleEnvironment.IsAvailable)
            {
                ApplicationEnvironment.ApplicationModel = ApplicationModel.Cloud;
                ApplicationEnvironment.logger = new RoleManagerLogger();
            }
            else if (System.Web.HttpContext.Current != null)
            {
                ApplicationEnvironment.ApplicationModel = ApplicationModel.Web;
                ApplicationEnvironment.logger = new TraceLogger();
                System.Diagnostics.Trace.Listeners.Add(new System.Web.WebPageTraceListener());
            }
            else
            {
                ApplicationEnvironment.ApplicationModel = ApplicationModel.Desktop;
                ApplicationEnvironment.logger = new ConsoleLogger();
            }

            ApplicationEnvironment.Settings = new Settings();
            ApplicationEnvironment.LocalStores = new LocalStores();
            ApplicationEnvironment.AccountSettings = new AccountSettings();
        }

        /// <summary>
        /// Gets the application model.
        /// </summary>
        /// <value>The application model.</value>
        public static ApplicationModel ApplicationModel
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the local stores.
        /// </summary>
        /// <value>The local stores.</value>
        public static LocalStores LocalStores
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <value>The settings.</value>
        public static Settings Settings
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the account settings.
        /// </summary>
        /// <value>The account settings.</value>
        public static AccountSettings AccountSettings
        {
            get;
            private set;
        }

        /// <summary>
        /// Logs the alert.
        /// </summary>
        /// <param name="msg">The message.</param>
        public static void LogAlert(string msg)
        {
            logger.LogAlert(msg);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="msg">The message.</param>
        public static void LogError(string msg)
        {
            logger.LogError(msg);
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="msg">The message.</param>
        public static void LogWarning(string msg)
        {
            logger.LogWarning(msg);
        }

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="msg">The message.</param>
        public static void LogInformation(string msg)
        {
            logger.LogInformation(msg);
        }

        /// <summary>
        /// Logs the verbose.
        /// </summary>
        /// <param name="msg">The message.</param>
        public static void LogVerbose(string msg)
        {
            logger.LogVerbose(msg);
        }

        #region Useful Overloads

        /// <summary>
        /// Logs the alert.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void LogAlert(string format, params object[] args)
        {
            LogAlert(FormatMessage(format, args));
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void LogError(string format, params object[] args)
        {
            LogError(FormatMessage(format, args));
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void LogWarning(string format, params object[] args)
        {
            LogWarning(FormatMessage(format, args));
        }

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void LogInformation(string format, params object[] args)
        {
            LogInformation(FormatMessage(format, args));
        }

        /// <summary>
        /// Logs the verbose.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void LogVerbose(string format, params object[] args)
        {
            LogVerbose(FormatMessage(format, args));
        }

        /// <summary>
        /// Formats the message.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The message</returns>
        private static string FormatMessage(string format, object[] args)
        {
            return String.Format(CultureInfo.InvariantCulture, format, args);
        }

        #endregion
    } 
}
