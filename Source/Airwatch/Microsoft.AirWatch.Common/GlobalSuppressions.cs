// <copyright file="GlobalSuppressions.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>Global 
// </summary>

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes", Scope = "member", Target = "Microsoft.AirWatch.Common.AccountSettings.#Item[System.String]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Scope = "member", Target = "Microsoft.AirWatch.Common.ApplicationEnvironment.#.cctor()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "tag", Scope = "member", Target = "Microsoft.AirWatch.Common.ConsoleLogger.#WriteMessage(System.ConsoleColor,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "message", Scope = "member", Target = "Microsoft.AirWatch.Common.ConsoleLogger.#WriteMessage(System.ConsoleColor,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Common.Settings.#Item[System.String]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Common.AccountSettings.#Item[System.String]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "5#", Scope = "member", Target = "Microsoft.AirWatch.Common.MapImageHelper.#LatLongToPixelXY(System.Double,System.Double,System.Int32,System.Double,System.Double,System.Int32&,System.Int32&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "6#", Scope = "member", Target = "Microsoft.AirWatch.Common.MapImageHelper.#LatLongToPixelXY(System.Double,System.Double,System.Int32,System.Double,System.Double,System.Int32&,System.Int32&)")]
