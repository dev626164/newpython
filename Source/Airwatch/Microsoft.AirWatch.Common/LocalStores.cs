﻿// <copyright file="LocalStores.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>LocalStores 
// </summary>
namespace Microsoft.AirWatch.Common
{
    using System.IO;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.ServiceRuntime;

    /// <summary>
    /// Local stores
    /// </summary>
    public class LocalStores
    {
        /// <summary>
        /// Stores directory
        /// </summary>
        private string storesDir;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalStores"/> class.
        /// </summary>
        internal LocalStores()
        {
            if (ApplicationEnvironment.ApplicationModel != ApplicationModel.Cloud)
            {
                this.storesDir = Path.Combine(Path.Combine(Path.GetTempPath(), "localstores"), Path.GetRandomFileName());
            }
        }

        /// <summary>
        /// Gets the <see cref="System.String"/> with the specified name.
        /// </summary>
        /// <param name="name">The name of the setting</param>
        /// <value></value>
        public string this[string name]
        {
            get
            {
                switch (ApplicationEnvironment.ApplicationModel)
                {
                    case ApplicationModel.Cloud:
                        return RoleEnvironment.GetConfigurationSettingValue(name);
                    case ApplicationModel.Desktop:
                    case ApplicationModel.Web:
                        string dirPath = Path.Combine(this.storesDir, name);
                        Directory.CreateDirectory(dirPath);
                        return dirPath;
                    default:
                        return null;
                }
            }
        }
    }
}
