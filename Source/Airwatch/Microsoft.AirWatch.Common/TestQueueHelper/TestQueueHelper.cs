﻿//-----------------------------------------------------------------------
// <copyright file="TestQueueHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/07/09</date>
// <summary>Test queue helper</summary>
//----------------------------------------------------------------------- 
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;

namespace Microsoft.AirWatch.Common
{
    /// <summary>
    /// Helper for tests - the test queue
    /// </summary>
    public static class TestQueueHelper
    {
        /// <summary>
        /// Puts the message.
        /// </summary>
        /// <param name="messageContent">Content of the message.</param>
        public static void PutMessage(string messageContent)
        {
            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            var client = account.CreateCloudQueueClient();
            var queue = client.GetQueueReference("testqueue");
            queue.CreateIfNotExist();

            queue.AddMessage(new CloudQueueMessage(messageContent));
        }
    }
}
