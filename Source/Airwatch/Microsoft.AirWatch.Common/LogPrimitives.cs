﻿// <copyright file="LogPrimitives.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>LogPrimitives 
// </summary>
namespace Microsoft.AirWatch.Common
{
    /// <summary>
    /// Log the primitives
    /// </summary>
    public abstract class LogPrimitives
    {
        /// <summary>
        /// Logs the alert.
        /// </summary>
        /// <param name="msg">The Message.</param>
        public abstract void LogAlert(string msg);

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="msg">The Message.</param>
        public abstract void LogError(string msg);

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="msg">The Message.</param>
        public abstract void LogWarning(string msg);

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="msg">The Message.</param>
        public abstract void LogInformation(string msg);

        /// <summary>
        /// Logs the verbose.
        /// </summary>
        /// <param name="msg">The Message.</param>
        public abstract void LogVerbose(string msg);
    }
}
