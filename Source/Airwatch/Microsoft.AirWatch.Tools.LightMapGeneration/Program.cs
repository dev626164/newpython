﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>08/09/2009</date>
// <summary>Program to start the batch generation of quad keys.</summary>
//-----------------------------------------------------------------------
namespace Microsoft.AirWatch.Tools.LightMapGeneration
{
    using Microsoft.AirWatch.Core.ApplicationServices;

    /// <summary>
    /// Program for the batch generation of quad keys.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Program"/> class.
        /// </summary>
        internal Program()
        {
        }

        /// <summary>
        /// Mains the specified args.
        /// </summary>
        public static void Main()
        
        {
            new BatchGenerateQuadKey().SingleQuad("01", LightMapType.UserFeedbackAir);

            //new BatchGenerateQuadKey().SingleQuad("01", LightMapType.AirStation);
            //new BatchGenerateQuadKey().SingleQuad("02", LightMapType.AirStation);
            //new BatchGenerateQuadKey().SingleQuad("03", LightMapType.AirStation);
            //new BatchGenerateQuadKey().SingleQuad("00", LightMapType.AirStation);
            // new BatchGenerateQuadKey().StartGenerate(LightMapType.AirStation);
            // new BatchGenerateQuadKey().StartGenerate(LightMapType.WaterStation);
            // new BatchGenerateQuadKey().StartGenerate(LightMapType.UserFeedbackAir);
            // new BatchGenerateQuadKey().StartGenerate(LightMapType.UserFeedbackWater);
        }
    }
}
