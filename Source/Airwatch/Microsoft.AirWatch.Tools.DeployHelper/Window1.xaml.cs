﻿//-----------------------------------------------------------------------
// <copyright file="Window1.xaml.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>30/07/09</date>
// <summary>Deployment helper Window</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Tools.DeployHelper
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        /// <summary>
        /// Open folder dialog result
        /// </summary>
        private System.Windows.Forms.DialogResult folderBrowserResult;

        /// <summary>
        /// Full xml element
        /// </summary>
        private XElement fullXml;

        /// <summary>
        /// Open file dialog
        /// </summary>
        private System.Windows.Forms.FolderBrowserDialog openDirectoryDialog;

        /// <summary>
        /// Initializes a new instance of the <see cref="Window1"/> class.
        /// </summary>
        public Window1()
        {
            InitializeComponent();

            this.openDirectoryDialog = new System.Windows.Forms.FolderBrowserDialog();

            this.openDirectoryDialog.SelectedPath = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) + "\\Microsoft.AirWatch.CloudService\\AirWatch.CloudService\\bin";

            // The whole configuration
            this.fullXml = XElement.Load("..\\..\\Properties.xml");

            this.OutText.Foreground = Brushes.Green;
        }

        /// <summary>
        /// Fires on Open csx
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">Routed arguments</param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.folderBrowserResult = this.openDirectoryDialog.ShowDialog();
        }

        /// <summary>
        /// Handles the Click event of the Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.folderBrowserResult == System.Windows.Forms.DialogResult.OK)
            {              
                ////if (true)//(System.IO.Path.GetExtension(csxPath).Equals(".csx", StringComparison.OrdinalIgnoreCase))
                ////{

                //// For each config file to be changed (specified in the Properties.xml)
                foreach (XElement configListItem in this.fullXml.Element("configs").Elements())
                {
                    string configFileLocation = configListItem.Value;
                    string csxPath = this.openDirectoryDialog.SelectedPath;

                    if (configFileLocation.Contains("^"))
                    {
                        foreach (char c in configFileLocation.ToCharArray())
                        {
                            if (c.Equals('^'))
                            {
                                csxPath = System.IO.Path.GetDirectoryName(csxPath);
                            }
                        }

                        configFileLocation = configFileLocation.Replace("^", string.Empty);
                    }

                    configFileLocation = csxPath + "\\" + configFileLocation;

                    try
                    {
                        ////XDocument config = null;

                        ////List<ZipValueEntry> zipEntryList;

                        ////if (System.IO.Path.GetExtension(configFileLocation).Equals(".xap", StringComparison.OrdinalIgnoreCase))
                        ////{
                        ////    zipEntryList = new List<ZipValueEntry>();

                        ////    using (Stream stream = File.Open(configFileLocation, FileMode.Open, FileAccess.Read))
                        ////    {
                        ////        using (ZipInputStream zipInStream = new ZipInputStream(stream))
                        ////        {
                        ////            ZipValueEntry entry;
                        ////            while ((entry = new ZipValueEntry(zipInStream.GetNextEntry())).ZipEntry != null)
                        ////            {
                        ////                StreamReader sr = new StreamReader(zipInStream);

                        ////                char[] configBuffer = new char[entry.ZipEntry.Size];
                        ////                sr.Read(configBuffer, 0, Convert.ToInt32(entry.ZipEntry.Size));
                        ////                StringBuilder sb = new StringBuilder();
                        ////                sb.Append(configBuffer);

                        ////                if (entry.ZipEntry.Name.Equals("ServiceReferences.ClientConfig", StringComparison.OrdinalIgnoreCase))
                        ////                {
                        ////                    config = XDocument.Parse(sb.ToString());
                        ////                    ConfigWork(config, ref configFileLocation);
                        ////                    //entry.Comment = "last";
                        ////                    entry.Value = config.ToString().ToCharArray();
                        ////                }
                        ////                else
                        ////                {
                        ////                    entry.Value = configBuffer;
                        ////                }

                        ////                zipEntryList.Add(entry);
                        ////            }
                        ////        }
                        ////    }

                        ////    using (Stream streamOut = File.Open(configFileLocation, FileMode.Truncate, FileAccess.Write))
                        ////    {
                        ////        ZipOutputStream zipOutStream = new ZipOutputStream(streamOut);

                        ////        foreach (ZipValueEntry entry in zipEntryList)
                        ////        {

                        ////                //if (entry.Comment.Equals("last", StringComparison.Ordinal))
                        ////                //{
                        ////                ZipEntry zEnt = new ZipEntry(entry.ZipEntry.Name)
                        ////                {
                        ////                    Size = entry.Value.Length,
                        ////                    DateTime = DateTime.UtcNow
                        ////                };

                        ////                zipOutStream.PutNextEntry(zEnt);
                        ////                zipOutStream.Write(Encoding.ASCII.GetBytes(entry.Value), 0, Convert.ToInt32(zEnt.Size));

                        ////                //}
                        ////                //else
                        ////                //{
                        ////                //    zipOutStream.PutNextEntry(entry);
                        ////                //    zipOutStream.Write(Encoding.ASCII.GetBytes(xapCharArray), Convert.ToInt32(entry.Offset), Convert.ToInt32(entry.Size));
                        ////                //}

                        ////        }

                        ////        zipOutStream.Close();

                        ////    }
                        ////}
                        ////else
                        ////{

                        XDocument config = XDocument.Load(configFileLocation);
                        this.ConfigWork(config);
                        try
                        {
                            config.Save(configFileLocation);
                            this.OutText.Text += "Succesfully written " + configFileLocation + Environment.NewLine;
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            this.OutText.Foreground = Brushes.Red;
                            this.OutText.Text += "Error in " + configFileLocation + ex.Message + Environment.NewLine;
                        }
                        //// }
                    }
                    catch (System.IO.IOException exx)
                    {
                        this.OutText.Foreground = Brushes.Salmon;
                        this.OutText.Text += "Error in " + configFileLocation + exx.Message + Environment.NewLine;
                    }
                }

                ////TaskItem[] packRoles = new TaskItem[1];
                ////TaskItem packRole = new TaskItem()
                ////{

                ////}; //@"obj\Debug\Microsoft.AirWatch.CloudService.WebRole\;C:\Projects\Microsoft.AirWatch\Main\Source\Airwatch\Microsoft.AirWatch.CloudService\Microsoft.AirWatch.CloudService.WorkerRole\bin\Debug\");
                ////packRoles[0] = packRole;

                ////// TODO: get from registry
                //////string serviceHostingSDKBinDir = @"C:\Program Files (x86)\MSBuild\Microsoft\Cloud Service\v1.0";
                ////string projectName = this.fullXml.Element("serviceDefinition").Attribute("name").Value;

                ////IsolatedCSPack isoPack = new IsolatedCSPack();

                ////isoPack.ServiceDefinitionFile = @"C:\Program Files\Windows Azure SDK\v1.0\bin" + "\\Microsoft.ServiceHosting.Tools.MSBuildTasks.dll";
                ////isoPack.ServiceDefinitionFile = this.openDirectoryDialog.SelectedPath + "\\" + this.fullXml.Element("serviceDefinition").Value;
                ////isoPack.Output = csx + "\\DeploymentHelper\\" + projectName + ".cspkg";
                ////isoPack.PackRoles = packRoles;
                ////isoPack.PublishSSLCertificateThumbprint = String.Empty;
                ////isoPack.CopyOnly = false;

                ////isoPack.Execute();

                ////RunCloudServicePack(csxPath,  , , );
                ////}
                ////else
                ////{
                ////    MessageBox.Show("The selected folder is not a valid csx");
                ////}
            }
            else
            {
                MessageBox.Show("Select a csx first");
            }
        }

        /// <summary>
        /// Does work on the config
        /// </summary>
        /// <param name="config">The config.</param>
        private void ConfigWork(XDocument config)
        {
            XElement sqlServersCombo = XElement.Parse((this.SqlServersComboBox.SelectedItem as XmlLinkedNode).OuterXml);
            XElement webServersCombo = XElement.Parse((this.WebServersComboBox.SelectedItem as XmlLinkedNode).OuterXml);

            if (config != null)
            {
                foreach (XElement confSection in sqlServersCombo.Elements().Elements())
                {
                    try
                    {
                        IEnumerable<XElement> sections = config.Descendants();

                        foreach (XElement property in sections.ToList())
                        {
                            if (property.Name.LocalName.Equals(confSection.Name.LocalName))
                            {
                                foreach (XAttribute configPropertyAttribute in property.Attributes())
                                {
                                    if (confSection.Attribute(confSection.Parent.Attribute("identifier").Value).Name.LocalName.Equals(configPropertyAttribute.Name.LocalName, StringComparison.OrdinalIgnoreCase))
                                    {
                                        if (confSection.Attribute(confSection.Parent.Attribute("identifier").Value).Value.Equals(configPropertyAttribute.Value, StringComparison.OrdinalIgnoreCase))
                                        {
                                            property.ReplaceWith(confSection);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (NullReferenceException)
                    {
                    }
                }

                foreach (XElement confSection in webServersCombo.Elements().Elements())
                {
                    try
                    {
                        IEnumerable<XElement> sections = config.Descendants();

                        foreach (XElement property in sections.ToList())
                        {
                            if (property.Name.LocalName.Equals(confSection.Name.LocalName))
                            {
                                foreach (XAttribute configPropertyAttribute in property.Attributes())
                                {
                                    if (confSection.Attribute(confSection.Parent.Attribute("identifier").Value).Name.LocalName.Equals(configPropertyAttribute.Name.LocalName, StringComparison.OrdinalIgnoreCase))
                                    {
                                        if (confSection.Attribute(confSection.Parent.Attribute("identifier").Value).Value.Equals(configPropertyAttribute.Value, StringComparison.OrdinalIgnoreCase))
                                        {
                                            property.ReplaceWith(confSection);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (NullReferenceException)
                    {
                    }
                }
            }
        }
    }
}
