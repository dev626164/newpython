﻿//-----------------------------------------------------------------------
// <copyright file="LightMapWorkerRole.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>07/09/2009</date>
// <summary>Light Map Worker Role.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.Samples.ServiceHosting.StorageClient;
    using System;

    /// <summary>
    /// Light Map Worker Role.
    /// </summary>
    public class LightMapWorkerRole
    {
        /// <summary>
        /// Azure account from config
        /// </summary>
        private StorageAccountInfo queueAccount;

        /// <summary>
        /// Azure queue storage
        /// </summary>
        private QueueStorage queueStorage;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private MessageQueue lightMapQueue;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.queueAccount = ApplicationEnvironment.AccountSettings["QueueStorageEndpoint"];
            this.queueStorage = QueueStorage.Create(this.queueAccount);
            this.lightMapQueue = this.queueStorage.GetQueue("lightmapqueue");

            if (!this.lightMapQueue.DoesQueueExist())
            {
                this.lightMapQueue.CreateQueue();
                ApplicationEnvironment.LogInformation("Light map tile server queue created");
            }
            else
            {
                ApplicationEnvironment.LogInformation("Light map tile server queue already exists");
            }

            this.lightMapQueue.MessageReceived += new MessageReceivedEventHandler(this.MessageRecived);
            this.lightMapQueue.StartReceiving();
        }

        /// <summary>
        /// Messages the recived.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.Samples.ServiceHosting.StorageClient.MessageReceivedEventArgs"/> instance containing the event data.</param>
        private void MessageRecived(object sender, MessageReceivedEventArgs e)
        {
            try
            {
                LightMapMessage recivedMessage = (LightMapMessage)QueueHelper.DeserializeMessage(e.Message, typeof(LightMapMessage));
                ServiceProvider.TileImageService.GenerateFromQuadKey(recivedMessage.QuadKey, recivedMessage.LightMapType, recivedMessage.IsOverlap);
                this.lightMapQueue.DeleteMessage(e.Message);
            }
            catch (Exception)
            {
                ApplicationEnvironment.LogWarning("Problem creating tile image.");
            }
        }
    }
}
