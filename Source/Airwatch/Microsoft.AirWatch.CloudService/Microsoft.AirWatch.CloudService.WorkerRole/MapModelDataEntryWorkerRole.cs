﻿//-----------------------------------------------------------------------
// <copyright file="MapModelDataEntryWorkerRole.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>13 July 2009</date>
// <summary>Class to be used by the worker role for Map Model Data Entry</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    #region Using

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Core.ApplicationServices.DataStructures;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    #endregion

    /// <summary>
    /// Class to be used by the worker role for Map Model Data Entry
    /// </summary>
    public class MapModelDataEntryWorkerRole : IDisposable
    {
        /// <summary>
        /// Azure account from config
        /// </summary>
        private StorageAccountInfo queueAccount;

        /// <summary>
        /// Azure queue storage
        /// </summary>
        private QueueStorage queueStorage;

        /// <summary>
        /// Azure message queue
        /// </summary>
        private MessageQueue dataEntryMessageQueue;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private MessageQueue heatMapTileServerQueue;

        /// <summary>
        /// Azure account from config
        /// </summary>
        private StorageAccountInfo blobAccount;

        /// <summary>
        /// Azure blob storage
        /// </summary>
        private BlobStorage blobStorage;

        /// <summary>
        /// Azure blob container
        /// </summary>
        private BlobContainer blobContainer;

        /// <summary>
        /// Blob container for testing the amount of images in the tiles blob store.
        /// </summary>
        private BlobContainer blobTileContainer;

        /// <summary>
        /// Last level to generate.
        /// </summary>
        private int floorLevel;

        /// <summary>
        /// Character array for the quad keys.
        /// </summary>
        private string[] characterArray = new string[4] { "0", "1", "2", "3" };

        /// <summary>
        /// Palette to do colorisation.
        /// </summary>
        private Bitmap palette;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapModelDataEntryWorkerRole"/> class.
        /// </summary>
        public void Initialize()
        {
            this.queueAccount = ApplicationEnvironment.AccountSettings["QueueStorageEndpoint"];
            this.queueStorage = QueueStorage.Create(this.queueAccount);
            this.dataEntryMessageQueue = this.queueStorage.GetQueue("mapmodeldataqueue");
            this.heatMapTileServerQueue = this.queueStorage.GetQueue("heatmaptileserverqueue");

            if (!this.dataEntryMessageQueue.DoesQueueExist())
            {
                if (this.dataEntryMessageQueue.CreateQueue())
                {
                    ApplicationEnvironment.LogInformation("Map Model queue created");
                }
                else
                {
                    ApplicationEnvironment.LogError("Can not create Map Model queue");
                }
            }
            else
            {
                ApplicationEnvironment.LogInformation("Map Model queue already exists");
            }

            // Poll every 6 minutes so that it does not try entering more than one at a time
            this.dataEntryMessageQueue.PollInterval = 6000 * 60;

            if (!this.heatMapTileServerQueue.DoesQueueExist())
            {
                if (this.heatMapTileServerQueue.CreateQueue())
                {
                    ApplicationEnvironment.LogInformation("Heat map tile server queue created");
                }
                else
                {
                    ApplicationEnvironment.LogError("Can not create Heat map tile server queue");
                }
            }
            else
            {
                ApplicationEnvironment.LogInformation("Heat map tile server queue already exists");
            }

            this.blobAccount = StorageAccountInfo.GetDefaultBlobStorageAccountFromConfiguration();
            this.blobStorage = BlobStorage.Create(this.blobAccount);
            this.blobContainer = this.blobStorage.GetBlobContainer("mapmodeldatablob");

            if (!this.blobContainer.DoesContainerExist())
            {
                if (!this.blobContainer.CreateContainer())
                {
                    ApplicationEnvironment.LogError("Can not create blob container");
                }
            }

            // This section is only used for testing that the crucncher has put the desired number of images into the container.
            this.blobTileContainer = this.blobStorage.GetBlobContainer("heatmaptileserver");

            if (!this.blobTileContainer.DoesContainerExist())
            {
                if (!this.blobTileContainer.CreateContainer(null, ContainerAccessControl.Public))
                {
                    ApplicationEnvironment.LogError("Can not create tile blob container");
                }
            }

            this.palette = new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.WorkerRole.TileHeatMap.palette.png"));
            this.dataEntryMessageQueue.MessageReceived += new MessageReceivedEventHandler(this.DataEntryMessageQueue_MessageReceived);
            this.dataEntryMessageQueue.StartReceiving();
            ApplicationEnvironment.LogInformation("dataEntryMessageQueue started receiving");
        }

        /// <summary>
        /// Generates the quad keys wrapper.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="level">The level.</param>
        /// <returns>List of all keys.</returns>
        public List<string> GenerateQuadKeysWrapper(string quadKey, int level)
        {
            this.floorLevel = level;
            return this.GenerateQuadKeys(quadKey);
        }

        /// <summary>
        /// Generates the quad keys.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>List of quad keys.</returns>
        public List<string> GenerateQuadKeys(string quadKey)
        {
            List<string> tempList = new List<string>();

            if (quadKey.Length != this.floorLevel)
            {
                for (int i = 0; i <= 3; i++)
                {
                    tempList.AddRange(this.GenerateQuadKeys(quadKey + this.characterArray[i]));
                }
            }

            tempList.Add(quadKey);
            return tempList;
        }

        #region IDisposable Members
        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
            this.palette.Dispose();
        }

        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        /// <param name="onlyNative">if true will only dispose of the native resources</param>
        protected virtual void Dispose(bool onlyNative)
        {
            if (!onlyNative)
            {
                this.palette.Dispose();
            }
        }

        #endregion

        /// <summary>
        /// Gets the points from data.
        /// </summary>
        /// <param name="data">The base64 encoded string to get the data from.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="resolution">The resolution of the map.</param>
        /// <returns>Collection of map model points</returns>
        private static Collection<MapModelPoint> GetPointsFromData(string data, out long timestamp, out float resolution)
        {
            byte[] binaryData = null;

            try
            {
                binaryData = Convert.FromBase64String(data);
            }
            catch (FormatException)
            {
                ApplicationEnvironment.LogWarning("Data not in base 64 format");
            }

            Collection<MapModelPoint> mapPoints = null;
            timestamp = 0;
            resolution = 0.5f;

            if (binaryData != null)
            {
                mapPoints = new Collection<MapModelPoint>();

                using (BinaryReader binaryReader = new BinaryReader(new MemoryStream(binaryData)))
                {
                    int id_size;
                    int lines;
                    int columns;
                    int intResolution;

                    ApplicationEnvironment.LogVerbose((id_size = binaryReader.ReadInt32()).ToString(System.Globalization.CultureInfo.InvariantCulture));

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(binaryReader.ReadChars(id_size));

                    ApplicationEnvironment.LogVerbose(sb.ToString());
                    ApplicationEnvironment.LogVerbose((timestamp = binaryReader.ReadInt64()).ToString(System.Globalization.CultureInfo.InvariantCulture));
                    ApplicationEnvironment.LogVerbose((lines = binaryReader.ReadInt32()).ToString(System.Globalization.CultureInfo.InvariantCulture));
                    columns = binaryReader.ReadInt32();
                    ApplicationEnvironment.LogVerbose(columns.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    intResolution = binaryReader.ReadInt32();

                    resolution = ((float)intResolution) / 10;
                    ApplicationEnvironment.LogVerbose(resolution.ToString(System.Globalization.CultureInfo.InvariantCulture));

                    // check to see if lines has some valid amount of data in
                    if (lines > 1000)
                    {
                        for (int i = 0; i < lines; i++)
                        {
                            var mapPoint = new MapModelPoint()
                            {
                                Latitude = (double)binaryReader.ReadInt16() / 10,
                                Longitude = (double)binaryReader.ReadInt16() / 10,
                                QualityValue = (double)binaryReader.ReadInt16() / 10,
                                Ozone = (double)binaryReader.ReadInt16() / 10,
                                NO2 = (double)binaryReader.ReadInt16() / 10,
                                PM10 = (double)binaryReader.ReadInt16() / 10
                            };

                            mapPoint.Caqi = GetQualityRange(mapPoint.QualityValue);

                            if (mapPoint.Caqi > 0)
                            {
                                mapPoints.Add(mapPoint);
                            }
                        }
                    }
                    else
                    {
                        mapPoints = null;
                    }
                }
            }

            return mapPoints;
        }

        /// <summary>
        /// Gets the quality range.
        /// </summary>
        /// <param name="inputDobule">The input dobule.</param>
        /// <returns>The quality range</returns>
        private static double GetQualityRange(double inputDobule)
        {
            if (inputDobule > 0)
            {
                string airQualityRanges = "25;50;75;100;999";
                string[] rangesArray = airQualityRanges.Split(';');

                int i = 0;
                foreach (string rangeTopBound in rangesArray)
                {
                    i++;
                    if (inputDobule < int.Parse(rangeTopBound, System.Globalization.CultureInfo.InvariantCulture))
                    {
                        return (double)i;
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Creates the array.
        /// </summary>
        /// <param name="mapPoints">The map points.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="maxLatitude">The max latitude.</param>
        /// <param name="minLongitude">The min longitude.</param>
        /// <param name="resolution">The resolution.</param>
        /// <returns>
        /// 2-dimensional array where 0th dimension is the longitude, 1st dimension is the lattitude and the value at each array point is the CAQI value (not index).
        /// </returns>
        private static byte[,] CreateArray(Collection<MapModelPoint> mapPoints, double width, double height, double maxLatitude, double minLongitude, double resolution)
        {
            byte[,] byteArray = new byte[(int)width + 1, (int)height + 1];

            foreach (MapModelPoint mapPoint in mapPoints)
            {
                int x = Math.Abs((int)((mapPoint.Longitude - minLongitude) / resolution));
                int y = Math.Abs((int)((MapModelBoundsHelper.GetMercatorProjection(maxLatitude) - MapModelBoundsHelper.GetMercatorProjection(mapPoint.Latitude)) / resolution));

                byteArray[x, y] = (byte)(mapPoint.QualityValue * (255 / 120));
            }

            return byteArray;
        }

        /// <summary>
        /// Sets the resolution.
        /// </summary>
        /// <param name="resolution">The new resolution.</param>
        private static void SetResolution(float resolution)
        {
            ServiceProvider.CenterLocationService.SetResolution(resolution);
        }

        /// <summary>
        /// Handles the MessageReceived event of the dataEntryMessageQueue control.
        /// be aware that there is a 5 minute poll interval on this so messages won't be seen immediately
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Samples.ServiceHosting.StorageClient.MessageReceivedEventArgs"/> instance containing the event data.</param>
        private void DataEntryMessageQueue_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            string blobName = e.Message.ContentAsString();

            bool dataEntered = false;

            // get the blob specified in the message and enter it to the database
            if (this.blobContainer.DoesBlobExist(blobName))
            {
                ApplicationEnvironment.LogInformation("Map Model data entry: found blob, decoding and entering data");
                BlobContents contents = new BlobContents(new MemoryStream());
                BlobProperties properties = this.blobContainer.GetBlob(blobName, contents, false);

                if (properties.ContentType == null)
                {
                    ApplicationEnvironment.LogWarning("No content type set for blob.");
                }

                string data = Encoding.Unicode.GetString(contents.AsBytes());

                long timestamp;
                float resolution;
                Collection<MapModelPoint> points = GetPointsFromData(data, out timestamp, out resolution);

                // if the data is incorrect, delete the message
                if (points != null && points.Count > 1000)
                {
                    ApplicationEnvironment.LogInformation("Map Model data entry: entering data");

                    // now we have the points, update the database and update the tile server
                    dataEntered = ServiceProvider.CenterLocationService.InputData(points, timestamp, 5);

                    if (dataEntered)
                    {
                        // set the resolution where needed.
                        SetResolution(resolution);
                    }

                    // Place in table storage
                    MapModelBoundsEntity mapModelBoundsEntity = new MapModelBoundsEntity()
                    {
                        MinimumLongitude = points.Min(p => p.Longitude),
                        MaximumLongitude = points.Max(p => p.Longitude),
                        MinimumLatitude = points.Min(p => p.Latitude),
                        MaximumLatitude = points.Max(p => p.Latitude)
                    };

                    MapModelBoundsHelper.MapModelBounds = mapModelBoundsEntity;

                    // Generate Heatmap source image
                    int width = Math.Abs((int)((mapModelBoundsEntity.MaximumLongitude - mapModelBoundsEntity.MinimumLongitude) / 0.5));
                    int height = Math.Abs((int)((MapModelBoundsHelper.GetMercatorProjection(mapModelBoundsEntity.MaximumLatitude) - MapModelBoundsHelper.GetMercatorProjection(mapModelBoundsEntity.MinimumLatitude)) / 0.5));

                    byte[,] byteArray = CreateArray(points, width, height, mapModelBoundsEntity.MaximumLatitude, mapModelBoundsEntity.MinimumLongitude, 0.5);

                    Bitmap bitmap = new Bitmap(width, height);

                    Stream stream = Interpolator.InterpolateIncrease(bitmap, byteArray, 1, this.palette);

                    BlobContainer heatMapImageBlobContainer = this.blobStorage.GetBlobContainer("heatmapsourceimageblob");

                    if (!heatMapImageBlobContainer.DoesContainerExist())
                    {
                        heatMapImageBlobContainer.CreateContainer(null, ContainerAccessControl.Private);
                    }

                    BlobProperties blobProperties = new BlobProperties("heatMapSourceImage.png")
                    {
                        ContentType = "image/png"
                    };

                    stream.Position = 0;

                    BlobContents blobContents = new BlobContents(stream);

                    byte[] bytes = blobContents.AsBytes();
                    blobContents = new BlobContents(bytes);

                    heatMapImageBlobContainer.CreateBlob(blobProperties, blobContents, true);

                    // XXX: Test deploy when a tile source gets generated, save another with the date appended to it
                    blobProperties = new BlobProperties("heatMapSourceImage" + DateTime.Now.ToString().Replace(' ', '_') + ".png")
                    {
                        ContentType = "image/png"
                    };

                    blobContents = new BlobContents(bytes);

                    heatMapImageBlobContainer.CreateBlob(blobProperties, blobContents, true);
                    /// XXX: END

                    int zoomLevel = 6;
                    List<string> quadKeys = this.GenerateQuadKeysWrapper("013", zoomLevel);
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("031", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("102", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("103", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("033", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("122", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("120", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("121", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("123", zoomLevel));
                    quadKeys.Add("0");
                    quadKeys.Add("1");
                    quadKeys.Add("03");
                    quadKeys.Add("12");
                    quadKeys.Add("01");
                    quadKeys.Add("10");

                    CrunchMessage crunchMessage = new CrunchMessage();
                    foreach (string quadKey in quadKeys)
                    {
                        crunchMessage.QuadKey = quadKey;
                        crunchMessage.ImageOverlayType = ImageType.HeatMap;
                        this.heatMapTileServerQueue.PutMessage(QueueHelper.SerializeMessage(crunchMessage));
                    }
                }
                else
                {
                    // if the data is corrupt (i.e. not enough lines, or cannot read), delete the message and the blob
                    ApplicationEnvironment.LogError("MapModelDataEntry: Problem reading the data from the file given, disposing");

                    try
                    {
                        this.dataEntryMessageQueue.DeleteMessage(e.Message);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an air station data entry message from the queue");
                    }

                    try
                    {
                        this.blobContainer.DeleteBlob(blobName);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a map model data entry blob");
                    }
                }
            }
            else
            {
                // if the blob doesn't exist, get rid of the message
                try
                {
                    this.dataEntryMessageQueue.DeleteMessage(e.Message);
                }
                catch (StorageClientException)
                {
                    ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an air station data entry message from the queue");
                }
            }

            if (dataEntered)
            {
                try
                {
                    this.blobContainer.DeleteBlob(blobName);
                }
                catch (StorageClientException)
                {
                    ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a map model data entry blob");
                }

                try
                {
                    this.dataEntryMessageQueue.DeleteMessage(e.Message);
                }
                catch (StorageClientException)
                {
                    ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a map model data entry message from the queue");
                }
            }
        }
    }
}
