﻿// <copyright file="AirWatchWorkerRole.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>AirWatch Azure worker role</summary>
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Microsoft.AirWatch.Common;
    using Neudesic.Azure;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.ServiceRuntime;
    using Microsoft.WindowsAzure.Diagnostics;
    using System.Net;
    using System.Linq;
    using System.Diagnostics;
    using Microsoft.AirWatch.CloudService.WorkerRole.StationDataEntryTypes;

    /// <summary>
    /// Short message environmental category request
    /// </summary>
    public enum EnvironmentalCategoryRequest
    {
        /// <summary>
        /// Air quality request
        /// </summary>
        Air,

        /// <summary>
        /// Water quality request
        /// </summary>
        Water
    }

    /// <summary>
    /// AirWatch worker role
    /// </summary>
    public class AirWatchWorkerRole : RoleEntryPoint
    {
        List<IWorkerTask> workerTasks = new List<IWorkerTask>();

        public override void Run()
        {
            // need to sleep for ever
            Thread.Sleep(Timeout.Infinite);
        }

        /// <summary>
        /// Worker role entry point
        /// </summary>
        public override bool  OnStart()
        {
            try
            {
                // Set the maximum number of concurrent connections 
                ServicePointManager.DefaultConnectionLimit = 12;

                DiagnosticMonitorConfiguration dmc = DiagnosticMonitor.GetDefaultInitialConfiguration();
                dmc.Logs.ScheduledTransferPeriod = TimeSpan.FromSeconds(30);
                dmc.Logs.ScheduledTransferLogLevelFilter = LogLevel.Verbose;
                DiagnosticMonitor.Start("DiagnosticsConnectionString", dmc);

                //read storage account configuration settings
                CloudStorageAccount.SetConfigurationSettingPublisher((configName, configSetter) =>
                {
                    configSetter(RoleEnvironment.GetConfigurationSettingValue(configName));
                });

                // For information on handling configuration changes
                // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
                RoleEnvironment.Changing += RoleEnvironmentChanging;

                Trace.TraceInformation("Worker Process entry point called");

                this.workerTasks.Add(new ShortMessageWorkerTask());
                this.workerTasks.Add(new MapModelDataEntryWorkerTask());
                this.workerTasks.Add(new StationDataEntryWorkerTask(new AirStationData()));
                this.workerTasks.Add(new StationDataEntryWorkerTask(new WaterStationData()));
                this.workerTasks.Add(new TileWorkerTask());
                this.workerTasks.Add(new LightMapWorkerTask());
                this.workerTasks.Add(new BatchGenerateWorkerTask());
                this.workerTasks.Add(new UserLightMapWorkerTask());

                this.workerTasks.ForEach(x => x.Start());

                return base.OnStart();
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogError("Exception thrown while starting a worker role" + ex.Message);
                throw;
            }
        }

        public override void OnStop()
        {
            this.workerTasks.ForEach(x => x.Stop());

            base.OnStop();
        }

        private void RoleEnvironmentChanging(object sender, RoleEnvironmentChangingEventArgs e)
        {
            // If a configuration setting is changing
            if (e.Changes.Any(change => change is RoleEnvironmentConfigurationSettingChange))
            {
                // Set e.Cancel to true to restart this role instance
                e.Cancel = true;
            }
        }
    }
}
