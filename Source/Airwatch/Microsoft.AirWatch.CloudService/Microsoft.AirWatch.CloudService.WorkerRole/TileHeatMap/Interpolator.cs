﻿//-----------------------------------------------------------------------
// <copyright file="Interpolator.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>2009-09-04</date>
// <summary>Creates and scales a bitmap from a 2 dimensional array if bytes.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    #region Using

    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;

    #endregion

    /// <summary>
    /// Creates and scales a bitmap from a 2 dimensional array if bytes.
    /// </summary>
    public static class Interpolator
    {
        /// <summary>
        /// Interpolates the increase.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <param name="heatPoints">The heat points.</param>
        /// <param name="increase">The increase.</param>
        /// <param name="palette">The palette.</param>
        /// <returns>Stream of the needed image.</returns>
        public static Stream InterpolateIncrease(Bitmap bitmap, byte[,] heatPoints, int increase, Bitmap palette)
        {
            for (int y = 0; y < heatPoints.GetUpperBound(1) - 1; y++)
            {
                for (int x = 0; x < heatPoints.GetUpperBound(0) - 1; x++)
                {
                    if (heatPoints[x, y] != 0)
                    {
                        // copy down to fill the space between points (mecator distortion)
                        // add 3 as the range being used, there is no more than 2 pixel gap within the distortion
                        for (int copyDown = y; copyDown < y + 3 && copyDown < bitmap.Height; copyDown++)
                        {
                            bitmap.SetPixel(x, copyDown, Color.FromArgb(heatPoints[x, y], heatPoints[x, y], heatPoints[x, y]));
                        }
                    }
                }
            }

            Stream s = new MemoryStream();

            bitmap.Save(s, ImageFormat.Png);
            s.Position = 0;

            bitmap = Colorize(bitmap, palette);
            bitmap.Save(s, ImageFormat.Png);
            s.Position = 0;

            Bitmap scaledImage = new Bitmap(heatPoints.GetUpperBound(0) * increase, heatPoints.GetUpperBound(1) * increase);
            Graphics graphics = Graphics.FromImage(scaledImage);

            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            Rectangle rect = new Rectangle(0, 0, heatPoints.GetUpperBound(0) * increase, heatPoints.GetUpperBound(1) * increase);
            graphics.DrawImage(bitmap, rect);
            scaledImage.Save(s, ImageFormat.Png);
            s.Position = 0;

            return s;
        }

        /// <summary>
        /// Colorizes the specified mask.
        /// </summary>
        /// <param name="mask">The intensity mask.</param>
        /// <param name="palette">The palette.</param>
        /// <returns>
        /// A colorised version of the grayscale bitmap
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Passing through to a method that requires 'GetPixel' method on bitmap")]
        public static Bitmap Colorize(Bitmap mask, Bitmap palette)
        {
            // Create new bitmap to act as a work surface for the colorization process
            Bitmap output = new Bitmap(mask.Width, mask.Height, PixelFormat.Format32bppArgb);

            // Create a graphics object from our memory bitmap so we can draw on it and clear it's drawing surface
            Graphics surface = Graphics.FromImage(output);
            surface.Clear(Color.Transparent);

            // Build an array of color mappings to remap our greyscale mask to full color
            // Accept an alpha byte to specify the transparancy of the output image
            ColorMap[] colors = CreatePaletteIndex(palette);

            // Create new image attributes class to handle the color remappings
            // Inject our color map array to instruct the image attributes class how to do the colorization
            ImageAttributes remapper = new ImageAttributes();
            remapper.SetRemapTable(colors);

            // Draw our mask onto our memory bitmap work surface using the new color mapping scheme
            surface.DrawImage(mask, new Rectangle(0, 0, mask.Width, mask.Height), 0, 0, mask.Width, mask.Height, GraphicsUnit.Pixel, remapper);

            // Send back newly colorized memory bitmap
            return output;
        }

        /// <summary>
        /// Creates the index of the palette.
        /// </summary>
        /// <param name="palette">The palette.</param>
        /// <returns>Color Map palette</returns>
        private static ColorMap[] CreatePaletteIndex(Bitmap palette)
        {
            ColorMap[] outputMap = new ColorMap[256];

            // Loop through each pixel and create a new color mapping
            for (int x = 0; x <= 255; x++)
            {
                outputMap[x] = new ColorMap();
                outputMap[x].OldColor = Color.FromArgb(x, x, x);
                outputMap[x].NewColor = palette.GetPixel(x, 0);
            }

            return outputMap;
        }
    }
}