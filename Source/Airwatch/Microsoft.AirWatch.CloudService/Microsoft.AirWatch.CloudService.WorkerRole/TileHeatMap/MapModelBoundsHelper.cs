﻿//-----------------------------------------------------------------------
// <copyright file="MapModelBoundsHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>2009-09-02</date>
// <summary>Map Model Bounds Helper class.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    #region Using

    using System;
    using System.Data.Services.Client;
    using System.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Common;
    using Microsoft.Samples.ServiceHosting.StorageClient;
    using Microsoft.WindowsAzure;

    #endregion

    /// <summary>
    /// Map Model Bounds Helper class.
    /// </summary>
    public static class MapModelBoundsHelper
    {
        /// <summary>
        /// Gets or sets the map model bounds.
        /// </summary>
        /// <value>The map model bounds.</value>
        [CLSCompliant(false)]
        public static MapModelBoundsEntity MapModelBounds
        {
            get
            {
                CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
                MapModelBoundsEntityTable mapModelBoundsEntityTable = new MapModelBoundsEntityTable(account.TableEndpoint.ToString(), account.Credentials);

                var query = from conf in mapModelBoundsEntityTable.MapModelBounds
                            where conf.RowKey == "AirMapModelBounds" && conf.PartitionKey == "MapModelBounds"
                            select conf;

                return query.FirstOrDefault();
            }

            set
            {
                CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
                MapModelBoundsEntityTable table = new MapModelBoundsEntityTable(account.TableEndpoint.ToString(), account.Credentials);

                MapModelBoundsEntity mapModelBounds = (from conf in table.MapModelBounds
                                                       where conf.RowKey == "AirMapModelBounds" && conf.PartitionKey == "MapModelBounds"
                                                       select conf).FirstOrDefault();

                // MapModelBoundsEntity mapModelBounds = query.FirstOrDefault();
                MapModelBoundsEntityTable mapModelBoundsTable = new MapModelBoundsEntityTable(account.TableEndpoint.ToString(), account.Credentials);

                if (mapModelBounds != null)
                {
                    mapModelBounds.MaximumLatitude = value.MaximumLatitude;
                    mapModelBounds.MaximumLongitude = value.MaximumLongitude;
                    mapModelBounds.MinimumLatitude = value.MinimumLatitude;
                    mapModelBounds.MinimumLongitude = value.MinimumLongitude;

                    mapModelBoundsTable.AttachTo("MapModelBounds", mapModelBounds, "*");
                    mapModelBoundsTable.UpdateObject(mapModelBounds);
                }
                else
                {
                    mapModelBounds = new MapModelBoundsEntity("MapModelBounds", "AirMapModelBounds", value.MinimumLatitude, value.MinimumLongitude, value.MaximumLatitude, value.MaximumLongitude);

                    // mapModelBoundsTable.AttachTo("MapModelBounds", mapModelBounds, "*");
                    mapModelBoundsTable.AddObject("MapModelBounds", mapModelBounds);
                }

                try
                {
                    mapModelBoundsTable.SaveChanges();
                }
                catch (DataServiceRequestException dataEx)
                {
                    ApplicationEnvironment.LogError("Could not update the map model bounds table: " + dataEx.Message);
                }
            }
        }

        /// <summary>
        /// Gets the mercator projection from a latitude.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <returns>The mercator projection distorted latitude</returns>
        public static double GetMercatorProjection(double latitude)
        {
            // latitude * Math.PI/180 - .net uses radions not degrees, then *180/Math.Pi to get back to degrees
            double y = Math.Log(Math.Tan(0.25 * Math.PI + 0.5 * (latitude * Math.PI/180)))*180/Math.PI;

            return y;
        }
    }
}
