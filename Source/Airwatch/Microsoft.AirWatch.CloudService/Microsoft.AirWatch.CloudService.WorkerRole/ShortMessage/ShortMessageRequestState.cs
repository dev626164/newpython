﻿//-----------------------------------------------------------------------
// <copyright file="ShortMessageRequestState.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki </author>
// <email>t-fikarn@microsoft.com</email>
// <date>15/06/09</date>
// <summary>ShortMessage RequestState</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    /// <summary>
    /// Short message request state
    /// </summary>
    public class ShortMessageRequestState
    {
        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>The category.</value>
        public EnvironmentalCategoryRequest Category { get; set; }

        /// <summary>
        /// Gets or sets the short message reply string.
        /// </summary>
        /// <value>The short message reply string.</value>
        public string ShortMessageReplyString { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the components.
        /// </summary>
        /// <value>The components.</value>
        public string Components { get; set; }

        /// <summary>
        /// Gets or sets the string which will be sent if the request is invalid or returned no results
        /// </summary>
        /// <value>The invalid text request string.</value>
        public string InvalidTextRequestString { get; set; }

        /// <summary>
        /// Gets or sets the air error string.
        /// </summary>
        /// <value>The air error string.</value>
        public string AirUnknownLocationErrorString { get; set; }

        /// <summary>
        /// Gets or sets the water error string.
        /// </summary>
        /// <value>The water error string.</value>
        public string WaterUnknownLocationErrorString { get; set; }

        /// <summary>
        /// Gets or sets the no measurements for location string.
        /// </summary>
        /// <value>The no measurements for location string.</value>
        public string NoMeasurementsForLocationString { get; set; }
    }
}
