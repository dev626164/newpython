﻿//-----------------------------------------------------------------------
// <copyright file="LocalisationHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/06/09</date>
// <summary>Short message localisation helper class</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.WindowsAzure;

    /// <summary>
    /// Localisation helper class for the Short Message service (actually the worker role)
    /// </summary>
    public static class LocalisationHelper
    {
        /// <summary>
        /// Gets the emergency state of the request.
        /// </summary>
        /// <value>The state of the emergency request.</value>
        public static ShortMessageRequestState EmergencyRequestState
        {
            get
            {
                return new ShortMessageRequestState()
                {
                    Culture = "en-GB"
                };
            }
        }

        /// <summary>
        /// Gets the emergency localisation.
        /// </summary>
        /// <value>The emergency localisation.</value>
        public static LocalisationEntity EmergencyLocalisation
        {
            get
            {
                return new LocalisationEntity()
                {
                    WaterUnknownLocationErrorString = "ERROR – The location received has not been recognised. Please use the following format: WATER Reading, UK etc...",
                    AirUnknownLocationErrorString = "ERROR – The location received has not been recognised. Please use the following format: AIR Reading, UK etc...",
                    NoMeasurementsForLocationString = "ERROR - There is no data for this location. Please note that Eye on Earth currently only provides data for the 32 EEA member countries",
                    IncorrectRequestString = "Your request is not valid. Example: air reading, uk",
                    AirName = "air",
                    WaterName = "water",
                    AirReplyString = "{0}{1}",
                    WaterReplyString = "{0}{1}",
                    PartitionKey = "loc",
                    RowKey = "en-GB",
                    CountryCode = "44"
                };
            }
        }

        /// <summary>
        /// Gets all localisations.
        /// </summary>
        /// <value>All localisations.</value>
        public static Dictionary<string, LocalisationEntity> AllLocalisations
        {
            get
            {
                CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
                return (from LocalisationEntity le in new LocalisationEntityTable(account.TableEndpoint.ToString(), account.Credentials).Localisations
                        select le).ToDictionary(
                        keySelector => 
                            keySelector.CountryCode);
            }
        }

        /// <summary>
        /// Gets the localisations.
        /// </summary>
        /// <param name="keyword">The keyword.</param>
        /// <returns>List of localisations</returns>
        public static List<LocalisationEntity> GetLocalisations(string keyword)
        {
            CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            return (from LocalisationEntity le in new LocalisationEntityTable(account.TableEndpoint.ToString(), account.Credentials).Localisations
                    where le.AirName.Equals(keyword, StringComparison.OrdinalIgnoreCase)
                    || le.WaterName.Equals(keyword, StringComparison.OrdinalIgnoreCase)
                    select le).ToList(); 
        }

        /// <summary>
        /// Resolves the many localisations.
        /// </summary>
        /// <param name="localList">The local list.</param>
        /// <param name="countryCode">The country code.</param>
        /// <returns>Localisation .</returns>
        public static LocalisationEntity ResolveManyLocalisations(List<LocalisationEntity> localList, string countryCode)
        {
            var query = from l in localList
                        where l.CountryCode.Equals(countryCode, StringComparison.OrdinalIgnoreCase)
                        select l;

            if (query.Count() == 0)
            {
                return localList.First();
            }
            else
            {
                return query.FirstOrDefault();
            }
        }
    }
}
