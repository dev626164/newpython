﻿//-----------------------------------------------------------------------
// <copyright file="InternalConfigurationHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/06/09</date>
// <summary>Internal Configuration Helper</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Data.Services.Client;
    using System.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.WindowsAzure;

    /// <summary>
    /// Internal configuration helper class
    /// </summary>
    public static class InternalConfigurationHelper
    {
        /// <summary>
        /// Gets the internal config.
        /// </summary>
        /// <value>The internal config.</value>
        [CLSCompliant(false)]
        public static InternalConfigEntity InternalConfig
        {
            get
            {
                CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
                InternalConfigEntityTable internalConfigTable = new InternalConfigEntityTable(account.TableEndpoint.ToString(), account.Credentials);

                var query = from conf in internalConfigTable.InternalConfiguration
                             where conf.RowKey == "configuration1" && conf.PartitionKey == "InternalConfig"
                             select conf;

                InternalConfigEntity internalConfig = query.FirstOrDefault();

                if (internalConfig != null)
                {
                    if (String.IsNullOrEmpty(internalConfig.BingToken))
                    {
                        ApplicationEnvironment.LogInformation("Token null or empty. Getting the VEWebServices token");
                        internalConfig.BingToken = BingMapServicesHelper.GetBingToken("127.0.0.1");

                        internalConfigTable.AttachTo("InternalConfiguration", internalConfig, "*");
                        internalConfigTable.UpdateObject(internalConfig);

                        try
                        {
                            internalConfigTable.SaveChanges();
                        }
                        catch (DataServiceRequestException dataEx)
                        {
                            ApplicationEnvironment.LogError("Could not update the internal config table: " + dataEx.Message);
                        }
                    }
                }
                else
                {
                    internalConfig = new InternalConfigEntity()
                    {
                        BingToken = BingMapServicesHelper.GetBingToken("127.0.0.1")
                    };

                    ApplicationEnvironment.LogError("Could not retrieve the internal config from ProcessMessageRquest method");
                }

                return internalConfig;
            }
        }
    }
}
