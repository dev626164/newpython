﻿//-----------------------------------------------------------------------
// <copyright file="TileMessage.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>12 August 2009</date>
// <summary>Class to hold data about Tile image</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Class will hold data about the tile image.
    /// </summary>
    [Serializable]
    public class TileMessage
    {
        /// <summary>
        /// Gets or sets the pointer to image BLOB.
        /// </summary>
        /// <value>The pointer to image BLOB.</value>
        public string PointerToImageBlob { get; set; }
        
        /// <summary>
        /// Gets or sets the quad keys.
        /// </summary>
        /// <value>The quad keys.</value>
        public Collection<string> QuadKeys { get; set; }
        
        /// <summary>
        /// Gets or sets the max longitude.
        /// </summary>
        /// <value>The max longitude.</value>
        public double MaxLongitude { get; set; }

        /// <summary>
        /// Gets or sets the min longitude.
        /// </summary>
        /// <value>The min longitude.</value>
        public double MinLongitude { get; set; }
        
        /// <summary>
        /// Gets or sets the max lattitude.
        /// </summary>
        /// <value>The max lattitude.</value>
        public double MaxLatitude { get; set; }

        /// <summary>
        /// Gets or sets the min lattitude.
        /// </summary>
        /// <value>The min lattitude.</value>
        public double MinLatitude { get; set; }

        /// <summary>
        /// Gets or sets the height of the image.
        /// </summary>
        /// <value>The height.</value>
        public double Height { get; set; }

        /// <summary>
        /// Gets or sets the width of the image.
        /// </summary>
        /// <value>The width.</value>
        public double Width { get; set; }
    }
}
