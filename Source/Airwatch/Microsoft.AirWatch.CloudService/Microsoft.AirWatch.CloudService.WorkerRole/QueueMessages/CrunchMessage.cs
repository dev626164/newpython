﻿//-----------------------------------------------------------------------
// <copyright file="CrunchMessage.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>24/08/2009</date>
// <summary>Wrapper class to pass over teh wire.</summary>
//-----------------------------------------------------------------------
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    
    /// <summary>
    /// Enumeration for the different layer types.
    /// </summary>
    public enum ImageType
    {
        /// <summary>
        /// Heat map enumeration.
        /// </summary>
        HeatMap,

        /// <summary>
        /// Light map enumeration. 
        /// </summary>
        LightMap,

        /// <summary>
        /// User map enumeration.
        /// </summary>
        UserMap
    }

    /// <summary>
    /// Wrapper Class for the crunch service.
    /// </summary>
    [Serializable]
    public class CrunchMessage
    {
        /// <summary>
        /// Gets or sets the quad key.
        /// </summary>
        /// <value>The quad key.</value>
        public string QuadKey { get; set; }

        /// <summary>
        /// Gets or sets the type of the image overlay.
        /// </summary>
        /// <value>The type of the image overlay.</value>
        public ImageType ImageOverlayType { get; set; }
    }
}
