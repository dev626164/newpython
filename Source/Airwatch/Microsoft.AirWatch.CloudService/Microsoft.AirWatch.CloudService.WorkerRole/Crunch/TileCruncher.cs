﻿namespace Microsoft.AirWatch.CloudService.WorkerRole.Crunch
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using Microsoft.AirWatch.Common;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;

    public class TileCruncher
    {
        /// <summary>
        /// Original Image to be crunched.
        /// </summary>
        private Bitmap originalImage;

        private bool CalculateImageDataSuccess;

        private CloudBlobContainer sourceBlobContainer;

        private CloudBlobContainer blobContainer;

        /// <summary>
        /// Minimum Longitude for given image.
        /// </summary>
        private double minLongitude;

        /// <summary>
        /// Maximum Longitude for given image. 
        /// </summary>
        private double maxLongitude;

        /// <summary>
        /// Minimum Latitude for given image.
        /// </summary>
        private double minLatitude;

        /// <summary>
        /// Maximum Lotitude for the given image.
        /// </summary>
        private double maxLatitude;

        /// <summary>
        /// Radar zoom of the original image compared to the map.
        /// </summary>
        private double radarZoomX;

        /// <summary>
        /// Radar zoom of the original image compared to the map.
        /// </summary>
        private double radarZoomY;

        /// <summary>
        /// Contains the current quadKey being processed.
        /// </summary>
        private string quadKey;

        public TileCruncher()
        {
            CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            var client = account.CreateCloudBlobClient();
            this.sourceBlobContainer = client.GetContainerReference("heatmapsourceimageblob");

            this.blobContainer = client.GetContainerReference("heatmaptileserver");
            this.blobContainer.CreateIfNotExist();
        }

        /// <summary>
        /// Calculates the radar zoom.
        /// </summary>
        /// <param name="valueA">First input value will either be longitude or lattitude.</param>
        /// <param name="valueB">Second input value to match with the first.</param>
        /// <param name="imageDimension">The image dimension either height for lattiude or width for longitude.</param>
        /// <param name="isLattitude">If set to true [is lattitude].</param>
        /// <returns>Radar Zoom for given values.</returns>
        private static double CalculateRadarZoom(double valueA, double valueB, int imageDimension, bool isLattitude)
        {
            double bigValue;
            double smallValue;
            double differenceInPositions;

            // Calculate which value is bigger.
            if (valueB <= valueA)
            {
                bigValue = valueA;
                smallValue = valueB;
            }
            else
            {
                bigValue = valueB;
                smallValue = valueA;
            }

            // Call the correct methods to get the difference in positions depending if its latt or long.
            if (isLattitude)
            {
                differenceInPositions = RadarY(smallValue) - RadarY(bigValue);
            }
            else
            {
                differenceInPositions = RadarX(bigValue) - RadarX(smallValue);
            }

            // Return the value of radar zoom.
            return Math.Log(imageDimension / Math.Abs(differenceInPositions)) / Math.Log(2);
        }

        /// <summary>
        /// Calculate the individual positioning for a given longitude.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <returns>Positioning relative to the map and image.</returns>
        private static double RadarX(double longitude)
        {
            return ((longitude - 180) / 360) * 256;
        }

        /// <summary>
        /// Radars the Y.
        /// </summary>
        /// <param name="lattitude">The lattitude.</param>
        /// <returns>Positioning relative to the map and image.</returns>
        private static double RadarY(double lattitude)
        {
            double neutralSin = Math.Sin((lattitude * Math.PI) / 180);
            double middleStep = (1 + neutralSin) / (1 - neutralSin);

            // Seems odd that this was calculated using log 10 when everything else used the natural log (ln).
            double logOfBothSin = Math.Log10(middleStep);
            double logOverFourPi = logOfBothSin / (4 * Math.PI);
            return (0.5 - logOverFourPi) * 256;
        }

        /// <summary>
        /// Creates the binary quad key.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>Binary version of the inputted quad key.</returns>
        private static string CreateBinaryQuadKey(string quadKey)
        {
            // Convert Quadtree in binary Quadtree
            string binaryQuadKey = string.Empty;
            string tempString = string.Empty;

            for (int i = 0; i <= quadKey.Length - 1; i++)
            {
                tempString = quadKey.Substring(quadKey.Length - i - 1, 1);
                switch (tempString)
                {
                    case "3":
                        binaryQuadKey = "11" + binaryQuadKey;
                        break;
                    case "2":
                        binaryQuadKey = "10" + binaryQuadKey;
                        break;
                    case "1":
                        binaryQuadKey = "01" + binaryQuadKey;
                        break;
                    case "0":
                        binaryQuadKey = "00" + binaryQuadKey;
                        break;
                }
            }

            return binaryQuadKey;
        }

        /// <summary>
        /// Gets the binary tile co ordinates.
        /// </summary>
        /// <param name="binaryQuadKey">The binary quad key.</param>
        /// <param name="binaryTileX">The binary tile X.</param>
        /// <param name="binaryTileY">The binary tile Y.</param>
        private static void GetBinaryTileCoOrdinates(string binaryQuadKey, out string binaryTileX, out string binaryTileY)
        {
            string tempString = string.Empty;
            binaryTileX = string.Empty;
            binaryTileY = string.Empty;

            for (int i = 0; i <= binaryQuadKey.Length - 1; i++)
            {
                tempString = binaryQuadKey.Substring(binaryQuadKey.Length - i - 1, 1);
                if (i % 2 == 0)
                {
                    binaryTileX = tempString + binaryTileX;
                }
                else
                {
                    binaryTileY = tempString + binaryTileY;
                }
            }
        }

        /// <summary>
        /// Converts the binary tile co-ordinate to decimal values.
        /// </summary>
        /// <param name="binaryTile">The binary tile co-ordinate.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Decimal value for the tile co-ordinate</returns>
        private static int ConvertBinaryTileToDecimal(string binaryTile, int zoomLevel)
        {
            int decimalTileValue = 0;
            string tempString = string.Empty;

            for (int i = 0; i <= zoomLevel - 1; i++)
            {
                tempString = binaryTile.Substring(binaryTile.Length - i - 1, 1);
                decimalTileValue = (int)(decimalTileValue + (int.Parse(tempString, System.Globalization.CultureInfo.InvariantCulture) * Math.Pow(2, i)));
            }

            return decimalTileValue;
        }

        /// <summary>
        /// Converts the pixel position to long.
        /// </summary>
        /// <param name="deciamlValue">The deciaml value.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Longitude Value for point.</returns>
        private static double ConvertPixelPositionToLong(double deciamlValue, int zoomLevel)
        {
            return ((deciamlValue * 360) / (256 * Math.Pow(2, zoomLevel))) - 180;
        }

        /// <summary>
        /// Converts the pixel position to lat.
        /// </summary>
        /// <param name="decimalValue">The decimal value.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Latitude value for given pixel.</returns>
        private static double ConvertPixelPositionToLat(double decimalValue, int zoomLevel)
        {
            return Math.Asin((Math.Exp((0.5 - (decimalValue / (256 * Math.Pow(2, zoomLevel)))) * 4 * Math.PI) - 1) / (Math.Exp((0.5 - (decimalValue / (256 * Math.Pow(2, zoomLevel)))) * 4 * Math.PI) + 1)) * (180 / Math.PI);
        }

        /// <summary>
        /// Calculates the image X offset.
        /// </summary>
        /// <param name="imageLongitude">The image longitude (upper left).</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Image offset X.</returns>
        private static double CalculateImageXOffset(double imageLongitude, int zoomLevel)
        {
            return (((imageLongitude + 180) / 360) * 256) * Math.Pow(2, zoomLevel);
        }

        /// <summary>
        /// Calculates the image Y offset.
        /// </summary>
        /// <param name="imageLatitude">The image latitude.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Image offset in Y.</returns>
        private static double CalculateImageYOffset(double imageLatitude, int zoomLevel)
        {
            return RadarY(imageLatitude) * Math.Pow(2, zoomLevel);
        }

        /// <summary>
        /// Calculates the image overlay position longitude.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="offsetX">The x offset.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Position of the image in relation to Bing Maps.</returns>
        private int CalculateImageOverlayPositionLongitude(double longitude, double offsetX, int zoomLevel)
        {
            return Convert.ToInt32(((((longitude + 180) / 360) * 256 * Math.Pow(2, zoomLevel)) - offsetX) / Math.Pow(2, zoomLevel) * Math.Pow(2, this.radarZoomX));
        }

        /// <summary>
        /// Calculates the image overlay position latitude.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="offsetY">The y offset.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Position of the image in relation to Bing Maps.</returns>
        private int CalculateImageOverlayPositionLatitude(double latitude, double offsetY, int zoomLevel)
        {
            return Convert.ToInt32((CalculateImageYOffset(latitude, zoomLevel) - offsetY) * Math.Pow(2, this.radarZoomY) / Math.Pow(2, zoomLevel));
        }

        /// <summary>
        /// Crops the image given the correct coordinates to place in the tile.
        /// </summary>
        /// <param name="imageXMin">The image X min.</param>
        /// <param name="imageYMin">The image Y min.</param>
        /// <param name="imageSizeX">The image size X.</param>
        /// <param name="imageSizeY">The image size Y.</param>
        /// <returns>Cropped area of the original bitmap image.</returns>
        private Bitmap CropImage(int imageXMin, int imageYMin, int imageSizeX, int imageSizeY)
        {
            Bitmap croppedImage = new Bitmap(256, 256);
            Graphics myGraphics = Graphics.FromImage(croppedImage);
            Rectangle sourceRectangle = new Rectangle(imageXMin, imageYMin, imageSizeX, imageSizeY);
            Rectangle destRectangle1 = new Rectangle(0, 0, 256, 256);
            myGraphics.DrawImage(this.originalImage, destRectangle1, sourceRectangle, GraphicsUnit.Pixel);
            myGraphics.Dispose();

            return croppedImage;
        }

        /// <summary>
        /// Saves the image to storage.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="image">The image.</param>
        private void SaveImageToStorage(string quadKey, Bitmap image)
        {
            CloudBlob blob = this.blobContainer.GetBlobReference(quadKey + ".png");
            blob.Attributes.Properties.ContentType = "image/png";

            using (MemoryStream memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                memStream.Position = 0;

                blob.UploadFromStream(memStream);
            }
        }

        /// <summary>
        /// Calculates the image data.
        /// </summary>
        /// <returns>Boolean value indicating if succesful.</returns>
        public TileCruncher CalculateImageData()
        {
            MemoryStream memStream = new MemoryStream();
            CloudBlob sourceImage = this.sourceBlobContainer.GetBlobReference("heatMapSourceImage.png");

            if (sourceImage.Exists())
            {
                sourceImage.DownloadToStream(memStream);

                if (memStream.Length > 0)
                {
                    memStream.Position = 0;
                    this.originalImage = new Bitmap(memStream);

                    // this data needs to be uploaded to a store when the data is processed.
                    // need not to be hard coded! 75; 30; -40.0;65;
                    this.maxLatitude = MapModelBoundsHelper.MapModelBounds.MaximumLatitude;
                    this.minLatitude = MapModelBoundsHelper.MapModelBounds.MinimumLatitude;
                    this.minLongitude = MapModelBoundsHelper.MapModelBounds.MinimumLongitude;
                    this.maxLongitude = MapModelBoundsHelper.MapModelBounds.MaximumLongitude;

                    this.radarZoomX = CalculateRadarZoom(this.minLongitude, this.maxLongitude, this.originalImage.Width, false);
                    this.radarZoomY = CalculateRadarZoom(this.minLatitude, this.maxLatitude, this.originalImage.Height, true);

                    this.CalculateImageDataSuccess = true;
                }
                else
                {
                    this.CalculateImageDataSuccess = false;
                }
            }
            else
            {
                Trace.TraceWarning("Tile Cruncher: No Source Image found for heatMapSourceImage.png");
            }

            return this;
        }

        /// <summary>
        /// Crunches the specified quad key.
        /// </summary>
        public void Crunch(string quadKey)
        {
            if (CalculateImageDataSuccess)
            {
                this.quadKey = quadKey;

                // Determine Zoom-Level
                int zoomLevel = this.quadKey.Length;

                // create binary quadkey
                string binaryQuadKey = CreateBinaryQuadKey(this.quadKey);

                // Break binary Quadtree in binary tileX and tileY
                string binaryTileY;
                string binaryTileX;
                GetBinaryTileCoOrdinates(binaryQuadKey, out binaryTileX, out binaryTileY);

                // Convert binary tileX and tileY to decimal tileX and tileY
                int decimalTileX = ConvertBinaryTileToDecimal(binaryTileX, zoomLevel);
                int decimalTileY = ConvertBinaryTileToDecimal(binaryTileY, zoomLevel);

                // Determine pixel positions of top right and bottom left.
                double pixelXMin = decimalTileX * 256;
                double pixelXMax = ((decimalTileX + 1) * 256) - 1;
                double pixelYMin = decimalTileY * 256;
                double pixelYMax = ((decimalTileY + 1) * 256) - 1;

                // Calculate upper left and lower right long / lat for the tile
                double longMin = ConvertPixelPositionToLong(pixelXMin, zoomLevel);
                double longMax = ConvertPixelPositionToLong(pixelXMax, zoomLevel);
                double latMin = ConvertPixelPositionToLat(pixelYMin, zoomLevel);
                double latMax = ConvertPixelPositionToLat(pixelYMax, zoomLevel);

                // Calculate Pixel-Coordinates in radar-image
                double radarXOffset = CalculateImageXOffset(this.minLongitude, zoomLevel);
                double radarYOffset = CalculateImageYOffset(this.maxLatitude, zoomLevel);

                // Calculate X & Y coordinates of the image corresponding to the Bing Map.
                int radarXMin = this.CalculateImageOverlayPositionLongitude(longMin, radarXOffset, zoomLevel);
                int radarXMax = this.CalculateImageOverlayPositionLongitude(longMax, radarXOffset, zoomLevel);
                int radarYMin = this.CalculateImageOverlayPositionLatitude(latMin, radarYOffset, zoomLevel);
                int radarYMax = this.CalculateImageOverlayPositionLatitude(latMax, radarYOffset, zoomLevel);

                int radarSizeX = radarXMax - radarXMin;
                int radarSizeY = radarYMax - radarYMin;

                Bitmap imageToOverlay = this.CropImage(radarXMin, radarYMin, radarSizeX, radarSizeY);

                this.SaveImageToStorage(this.quadKey, imageToOverlay);
            }
        }
    }
}
