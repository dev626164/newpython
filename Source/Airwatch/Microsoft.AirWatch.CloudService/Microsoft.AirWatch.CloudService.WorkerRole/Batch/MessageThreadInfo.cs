﻿//-----------------------------------------------------------------------
// <copyright file="MessageThreadInfo.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>08/09/2009</date>
// <summary>Message Thread Info class to help with threading.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System.Threading;
    using Microsoft.AirWatch.Core.ApplicationServices;

    /// <summary>
    /// Message Thread Info class to help with threading.
    /// </summary>
    public class MessageThreadInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageThreadInfo"/> class.
        /// </summary>
        /// <param name="resetEvent">The reset event.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="lightMapType">Type of the light map.</param>
        public MessageThreadInfo(AutoResetEvent resetEvent, string quadKey, LightMapType lightMapType)
        {
            this.ResetEvent = resetEvent;
            this.QuadKey = quadKey;
            this.LightMapType = lightMapType;
        }

        /// <summary>
        /// Gets or sets the reset event.
        /// </summary>
        /// <value>The reset event.</value>
        public AutoResetEvent ResetEvent { get; set; }

        /// <summary>
        /// Gets or sets the quad key.
        /// </summary>
        /// <value>The quad key.</value>
        public string QuadKey { get; set; }

        /// <summary>
        /// Gets or sets the type of the light map.
        /// </summary>
        /// <value>The type of the light map.</value>
        public LightMapType LightMapType { get; set; }
    }
}
