﻿//-----------------------------------------------------------------------
// <copyright file="BatchGenerateQuadKey.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>08/09/2009</date>
// <summary>Batch Generate Quad Key class.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// Batch Generate Quad Key class.
    /// </summary>
    public class BatchGenerateQuadKey
    {
        /// <summary>
        /// Azure account from config
        /// </summary>
        private CloudStorageAccount queueAccount;

        /// <summary>
        /// Azure queue storage
        /// </summary>
        private CloudQueueClient queueStorage;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private CloudQueue lightMapQueue;

        /// <summary>
        /// List of stations for image generation.
        /// </summary>
        private List<StationMinimal> stations;

        /// <summary>
        /// List of user stations for image generation.
        /// </summary>
        private List<UserRating> userStations;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchGenerateQuadKey"/> class.
        /// </summary>
        public BatchGenerateQuadKey()
        {
            this.queueAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueStorage = this.queueAccount.CreateCloudQueueClient();
            this.lightMapQueue = this.queueStorage.GetQueueReference("lightmapqueue");
            this.lightMapQueue.CreateIfNotExist();
        }


        /// <summary>
        /// Singles the quad.
        /// </summary>
        /// <param name="quad">The quad.</param>
        /// <param name="type">The type.</param>
        public void SingleQuad(string quad, LightMapType type)
        {
            LightMapMessage message = new LightMapMessage();
            message.QuadKey = quad;
            message.LightMapType = type;
            message.IsOverlap = false;

            this.lightMapQueue.AddMessage(QueueHelper.SerializeMessage(message));
        }


        /// <summary>
        /// Starts the generate.
        /// </summary>
        /// <param name="type">The type of images to generate.</param>
        public void StartGenerate(LightMapType type)
        {
            string keyword = "Water";
            if (type == LightMapType.AirStation || type == LightMapType.UserFeedbackAir)
            {
                keyword = "Air";
            }

            HashSet<string> quadKeys = new HashSet<string>();
            if (type == LightMapType.AirStation || type == LightMapType.WaterStation)
            {
                this.stations = (from stations in ServiceProvider.StationService.GetStationsOfGivenType(keyword)
                                 select stations).ToList();

                int countx = 0;
                foreach (StationMinimal station in this.stations)
                {
                    string quad = ServiceProvider.TileImageService.GetQuadKeyForGivenLocation(station.Longitude, station.Latitude, 11);
                    quadKeys.Add(quad);
                    countx++;
                    Console.WriteLine("QuadKey: {0} Count:{1} Total:{2} Unique: {3}", quad, countx, this.stations.Count, quadKeys.Count);
                }
            }
            else
            {
                this.userStations = ServiceProvider.StationService.GetUserStationsOfGivenType(type).ToList();

                int countx = 0;
                foreach (UserRating rating in this.userStations)
                {
                    string quad = ServiceProvider.TileImageService.GetQuadKeyForGivenLocation(rating.Longitude, rating.Latitude, 11);
                    quadKeys.Add(quad);
                    countx++;
                    Console.WriteLine("QuadKey: {0} Count:{1} Total:{2} Unique: {3}", quad, countx, this.userStations.Count, quadKeys.Count);
                }
            }

            quadKeys = SplitQuadKeysIntoLevels(quadKeys);

            int min = 0;
            int max = 60;
            int count = quadKeys.Count();
            if (count > 0)
            {
                while (true)
                {
                    List<AutoResetEvent> autoResetEvents = new List<AutoResetEvent>();
                    for (int i = min; i < max; i++)
                    {
                        autoResetEvents.Add(new AutoResetEvent(false));
                        ThreadPool.QueueUserWorkItem(new WaitCallback(this.PutMessageOnQueue), new MessageThreadInfo(autoResetEvents[autoResetEvents.Count - 1], quadKeys.ElementAt(i), type));
                    }

                    WaitHandle.WaitAll(autoResetEvents.ToArray());
                    min = max;
                    max += 60;
                    if (max > count)
                    {
                        max = count;
                    }

                    if (min == count)
                    {
                        break;
                    }

                    Console.WriteLine("{0} of {1}", max, count);
                }
            }
        }

        /// <summary>
        /// Splits the quad keys into levels.
        /// </summary>
        /// <param name="baseKeys">The base keys.</param>
        /// <returns>Hash Set of unique quad keys.</returns>
        private static HashSet<string> SplitQuadKeysIntoLevels(HashSet<string> baseKeys)
        {
            StringBuilder quadKeyBuilder = new StringBuilder();
            HashSet<string> unique = new HashSet<string>();

            foreach (string quadKey in baseKeys)
            {
                quadKeyBuilder.Remove(0, quadKeyBuilder.Length);
                quadKeyBuilder.Append(quadKey);
                for (int i = 0; i < quadKey.Length; i++)
                {
                    unique.Add(quadKeyBuilder.ToString());
                    quadKeyBuilder.Remove(quadKeyBuilder.Length - 1, 1);
                }
            }

            return unique;
        }

        /// <summary>
        /// Puts the message on queue.
        /// </summary>
        /// <param name="state">The state.</param>
        private void PutMessageOnQueue(object state)
        {
            MessageThreadInfo mti = (MessageThreadInfo)state;

            LightMapMessage message = new LightMapMessage();
            message.QuadKey = mti.QuadKey;
            message.LightMapType = mti.LightMapType;
            message.IsOverlap = false;

            this.lightMapQueue.AddMessage(QueueHelper.SerializeMessage(message));

            mti.ResetEvent.Set();
        }
    }
}
