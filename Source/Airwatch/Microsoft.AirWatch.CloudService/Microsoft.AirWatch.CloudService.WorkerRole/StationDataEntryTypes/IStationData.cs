﻿namespace Microsoft.AirWatch.CloudService.WorkerRole.StationDataEntryTypes
{
    using System.Xml.Linq;

    public interface IStationData
    {
        string GetBlobContainerName();

        string GetQueueName();

        bool InputData(XElement data, int retrys);
    }
}