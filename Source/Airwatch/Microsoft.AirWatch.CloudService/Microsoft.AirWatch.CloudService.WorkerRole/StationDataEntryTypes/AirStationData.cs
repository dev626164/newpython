﻿namespace Microsoft.AirWatch.CloudService.WorkerRole.StationDataEntryTypes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.AirWatch.Core.ApplicationServices;

    public class AirStationData : IStationData
    {
        private string blobContainerName;
        private string QueueName;

        public AirStationData()
        {
            this.blobContainerName = "airstationdatablob";
            this.QueueName = "airstationdataqueue";
        }

        #region IStationData Members

        public string GetBlobContainerName()
        {
            return this.blobContainerName;
        }

        public string GetQueueName()
        {
            return this.QueueName;
        }

        public bool InputData(System.Xml.Linq.XElement data, int retrys)
        {
            return ServiceProvider.StationService.InputAirStationData(data, retrys);
        }

        #endregion
    }
}
