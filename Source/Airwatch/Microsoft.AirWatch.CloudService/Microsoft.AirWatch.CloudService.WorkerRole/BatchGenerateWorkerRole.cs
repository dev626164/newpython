﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.AirWatch.Common;
using Microsoft.AirWatch.Core.ApplicationServices;

namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    public class BatchGenerateWorkerRole : StationDataEntryWorkerRole
    {
        // <summary>
        /// Azure account from config
        /// </summary>
        private StorageAccountInfo queueAccount;

        /// <summary>
        /// Azure queue storage
        /// </summary>
        private QueueStorage queueStorage;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private MessageQueue lightMapQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="StationDataEntryWorkerRole"/> class.
        /// </summary>
        public override void Initialize()
        {
            this.queueAccount = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();
            this.queueStorage = QueueStorage.Create(this.queueAccount);
            this.lightMapQueue = this.queueStorage.GetQueue("batchgenerate");

            if (!this.lightMapQueue.DoesQueueExist())
            {
                this.lightMapQueue.CreateQueue();
            }

            this.lightMapQueue.MessageReceived += new MessageReceivedEventHandler(lightMapQueue_MessageReceived);
            this.lightMapQueue.StartReceiving();
        }

        void lightMapQueue_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            try
            {
                string type = e.Message.ContentAsString();
                this.lightMapQueue.DeleteMessage(e.Message);
                switch(type)
                {
                    case "AirStation":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.AirStation);
                        break;
                    case "WaterStation":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.WaterStation);
                        break;
                    case "UserFeedbackAir":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.UserFeedbackAir);
                        break;
                    case "UserFeedbackWater":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.UserFeedbackWater);
                        break;
                }

               
            }
            catch (Exception)
            {
                ApplicationEnvironment.LogWarning("Problem Batch Generating.");
            }
        }
    }
}
