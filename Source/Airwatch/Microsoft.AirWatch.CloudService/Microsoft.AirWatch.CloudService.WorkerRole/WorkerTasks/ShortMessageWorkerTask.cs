﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Diagnostics;
    using System.Mobile.Messaging;
    using Microsoft.AirWatch.AzureTables;
    using System.Mobile.Messaging.Web;
    using System.Net;
    using System.Mobile.Messaging.Protocols;
    using Microsoft.AirWatch.Common;
    using dev.virtualearth.net.webservices.v1.geocode;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using System.Globalization;
    using System.Data.Services.Client;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class ShortMessageWorkerTask : IWorkerTask
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudQueue shortMessageQueue;
        private Regex firstThreeRegex;
        private bool IsRunning;

        private void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            this.shortMessageQueue = this.queueClient.GetQueueReference("shortmessagequeue");
            this.shortMessageQueue.CreateIfNotExist();

            this.firstThreeRegex = new Regex("[0-9]{3}", RegexOptions.Compiled);
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                try
                {
                    while (IsRunning)
                    {
                        Thread.Sleep(100);

                        // need to find the max invisability timeout.
                        CloudQueueMessage message = this.shortMessageQueue.GetMessage();

                        if (message != null)
                        {
                            this.MessageReceived(message);
                        }
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void MessageReceived(CloudQueueMessage message)
        {
            try
            {
                ShortMessage shortMessage = new ShortMessage();

                string[] messageStringArray = message.AsString.Split('%');

                string fromDigitsOnly = Regex.Replace(messageStringArray[0], "[^0-9]", string.Empty);
                shortMessage.From = fromDigitsOnly;

                shortMessage.To = messageStringArray[1];
                shortMessage.Message = messageStringArray[2];

                Trace.WriteLine("Message parsed. From: " + shortMessage.From + " to: " + shortMessage.To + " msg: " + shortMessage.Message);

                ShortMessageRequestState requestState = new ShortMessageRequestState();
                LocalisationEntity localisation = new LocalisationEntity();

                string regexFirstWordString = @"^([\w]+\s+){1}";
                string firstWord = Regex.Match(shortMessage.Message, regexFirstWordString).Value;

                try
                {
                    List<LocalisationEntity> localisationList = LocalisationHelper.GetLocalisations(firstWord);

                    if (localisationList.Count == 1)
                    {
                        localisation = localisationList.First();
                        requestState.Culture = localisation.RowKey;
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(this.firstThreeRegex.Match(fromDigitsOnly).Value);
                        Dictionary<string, LocalisationEntity> allLocDict = LocalisationHelper.AllLocalisations;

                        while (sb.Length > 0)
                        {
                            if (allLocDict.ContainsKey(sb.ToString()))
                            {
                                localisation = allLocDict[sb.ToString()];
                                requestState.Culture = localisation.RowKey;
                                break;
                            }
                            else
                            {
                                sb.Remove(sb.Length - 1, 1);
                            }
                        }

                        if (localisationList.Count > 1)
                        {
                            localisation = LocalisationHelper.ResolveManyLocalisations(localisationList, localisation.CountryCode);
                        }
                        else
                        {
                            // You're doing it wrong
                            SendMessageToMGW(new ShortMessage(shortMessage.From, requestState.InvalidTextRequestString));
                        }
                    }

                    if (String.IsNullOrEmpty(requestState.Culture))
                    {
                        throw new InvalidOperationException();
                    }
                }
                catch (InvalidOperationException invExe)
                {
                    localisation = LocalisationHelper.EmergencyLocalisation;
                    Trace.TraceWarning("Could not find the localisation settings for the request: " + invExe.Message);
                }

                requestState.InvalidTextRequestString = localisation.IncorrectRequestString;
                requestState.NoMeasurementsForLocationString = localisation.NoMeasurementsForLocationString;
                requestState.WaterUnknownLocationErrorString = localisation.WaterUnknownLocationErrorString;
                requestState.AirUnknownLocationErrorString = localisation.AirUnknownLocationErrorString;

                if (firstWord.Trim().Equals(localisation.AirName, StringComparison.OrdinalIgnoreCase))
                {
                    requestState.ShortMessageReplyString = localisation.AirReplyString;
                    requestState.Category = EnvironmentalCategoryRequest.Air;
                    requestState.Components = "CAQI";
                    shortMessage.Message = Regex.Replace(shortMessage.Message, regexFirstWordString, string.Empty);

                    this.ProcessMessageRquest(shortMessage, requestState, null);
                }
                else if (firstWord.Trim().Equals(localisation.WaterName, StringComparison.OrdinalIgnoreCase))
                {
                    requestState.ShortMessageReplyString = localisation.WaterReplyString;
                    requestState.Category = EnvironmentalCategoryRequest.Water;
                    requestState.Components = "CWQI";
                    shortMessage.Message = Regex.Replace(shortMessage.Message, regexFirstWordString, string.Empty);

                    // TODO: Not quite
                    this.ProcessMessageRquest(shortMessage, requestState, null);
                }
                else
                {
                    //// Send "You're doing it wrong"
                }
            }
            catch (Exception ex)
            {
                if (ApplicationEnvironment.ApplicationModel == ApplicationModel.Desktop)
                {
                    CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
                    CloudQueueClient client = account.CreateCloudQueueClient();
                    CloudQueue queue = client.GetQueueReference("testqueue");
                    queue.CreateIfNotExist();

                    queue.AddMessage(new CloudQueueMessage("Processing short message: " + ex.Message));
                }

                ApplicationEnvironment.LogError("An exception has been thrown while trying to process the message off the queue" + ex.Message);
            }
            finally
            {
                try
                {
                    this.shortMessageQueue.DeleteMessage(message);
                }
                catch (StorageClientException)
                {
                    // swallows exception, not good!
                }
            }
        }

        /// <summary>
        /// Sends a short message to the user
        /// </summary>
        /// <param name="msg">Short message to be sent</param>
        private void SendMessageToMGW(ShortMessage msg)
        {
            try
            {
                Trace.WriteLine("Sending message to MGW");

                // Create Web Service Proxy
                using (TextMessageService smsGateway = new TextMessageService())
                {
                    smsGateway.Url = "https://www.theredfort.net/MwsTextMessageService/TextMessageService.asmx";
                    smsGateway.Credentials = new NetworkCredential("airwatch", "::bio9sphere::", "redfort");

                    // Create a Short Message Submission Request
                    ShortMessageSubmitRequest submitRequest = new ShortMessageSubmitRequest();
                    submitRequest.Message = ShortMessage.CreateShortMessage(msg.From, msg.To, msg.Message);

                    if (Boolean.Parse(ApplicationEnvironment.Settings["production"]))
                    {
                        // Send the message to the Mobile Web Gateway server which will handle delivery of the message.
                        // ShortMessageSubmitResponse submitResponse = smsGateway.SubmitRequest(submitRequest);
                        smsGateway.SubmitRequest(submitRequest);
                        Trace.WriteLine("Sms request: From: " + submitRequest.Message.From + " To: " + submitRequest.Message.To + " Message: " + submitRequest.Message.Message);
                    }
                    else
                    {
                        Trace.WriteLine("In prod would submit a request to the MGW: From:" + submitRequest.Message.From + " To: " + submitRequest.Message.To + " Message: " + submitRequest.Message.Message);
                    }

                    if (ApplicationEnvironment.ApplicationModel == ApplicationModel.Desktop)
                    {
                        var testQueue = this.queueClient.GetQueueReference("testqueue");
                        testQueue.CreateIfNotExist();

                        testQueue.AddMessage(new CloudQueueMessage(msg.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogInformation("Exception thrown while trying to send a send text requet to the gateway" + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Processes the message rquest.
        /// </summary>
        /// <param name="shortMessage">The short message.</param>
        /// <param name="requestState">State of the request.</param>
        /// <param name="retryToken">If it's the first request, specify as null. Otherwise, specify the token not to retrieve it again</param>
        private void ProcessMessageRquest(ShortMessage shortMessage, ShortMessageRequestState requestState, string retryToken)
        {
            InternalConfigEntity internalConfig;

            if (retryToken == null)
            {
                internalConfig = InternalConfigurationHelper.InternalConfig;
            }
            else
            {
                internalConfig = new InternalConfigEntity()
                {
                    BingToken = retryToken
                };
            }

            try
            {
                GeocodeResponse geocodeResponse = BingMapServicesHelper.CallBingMapServices(
                    internalConfig.BingToken,
                    requestState.Culture,
                    shortMessage.Message);

                if (geocodeResponse.Results.Count() <= 0)
                {
                    switch (requestState.Category)
                    {
                        case EnvironmentalCategoryRequest.Air:
                            SendMessageToMGW(new ShortMessage(shortMessage.From, requestState.AirUnknownLocationErrorString));
                            break;

                        case EnvironmentalCategoryRequest.Water:
                            SendMessageToMGW(new ShortMessage(shortMessage.From, requestState.WaterUnknownLocationErrorString));
                            break;
                    }
                }
                else
                {
                    string result = String.Format(CultureInfo.InvariantCulture, "Lat: {0} Long: {1}", geocodeResponse.Results[0].Locations[0].Latitude, geocodeResponse.Results[0].Locations[0].Longitude);
                    ApplicationEnvironment.LogInformation("Result from geocode service:" + result);

                    decimal? measurement = GetMeasurement((decimal)geocodeResponse.Results[0].Locations[0].Longitude, (decimal)geocodeResponse.Results[0].Locations[0].Latitude, requestState.Components);

                    if (measurement != null)
                    {
                        // Swap from and to (reply)
                        SendMessageToMGW(new ShortMessage()
                        {
                            From = shortMessage.To,
                            To = shortMessage.From,
                            Message = String.Format(
                            CultureInfo.InvariantCulture,
                            requestState.ShortMessageReplyString, // eg. The air quality for 
                            geocodeResponse.Results[0].DisplayName, // {0} is 

                            // {1}
                            measurement)
                        });
                    }
                    else
                    {
                        SendMessageToMGW(new ShortMessage()
                        {
                            From = shortMessage.To,
                            To = shortMessage.From,
                            Message = requestState.NoMeasurementsForLocationString
                        });
                    }
                }
            }
            catch (System.ServiceModel.FaultException<dev.virtualearth.net.webservices.v1.common.ResponseSummary>)
            {
                string newToken = BingMapServicesHelper.GetBingToken("127.0.0.1");

                if (internalConfig != null)
                {
                    // Save the new token to configuration
                    internalConfig.BingToken = newToken;

                    InternalConfigEntityTable configTable = new InternalConfigEntityTable(account.TableEndpoint.ToString(), account.Credentials);
                    configTable.AttachTo("InternalConfiguration", internalConfig, "*");
                    configTable.UpdateObject(internalConfig);

                    try
                    {
                        configTable.SaveChanges();
                    }
                    catch (DataServiceRequestException dataEx)
                    {
                        ApplicationEnvironment.LogError("Could not update the internal config table: " + dataEx.Message);
                    }

                    this.ProcessMessageRquest(shortMessage, requestState, newToken);
                }
                else
                {
                    ApplicationEnvironment.LogError("Could not retrieve the internal config from ProcessMessageRquest method");
                }
            }
        }

        /// <summary>
        /// Gets the measurement.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="component">The component.</param>
        /// <returns>The measurement value for a component</returns>
        private decimal? GetMeasurement(decimal longitude, decimal latitude, string component)
        {
            decimal? measureToReturn = null;

            if (component == "CAQI")
            {
                // If the data requested is air, then access the closest Air Index
                CentreLocation centreLocation = ServiceProvider.CenterLocationService.GetCentreLocationForPoint(longitude, latitude);

                measureToReturn = centreLocation == null ? null : centreLocation.QualityIndex;
            }
            else
            {
                // otherwise access the component requested
                var query = from Measurement measurement in ServiceProvider.CenterLocationService.GetIndexForPoint(longitude, latitude, component)
                            select measurement;

                Measurement measure = query.FirstOrDefault();

                measureToReturn = measure == null ? null : measure.Value;
            }

            return measureToReturn;
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting the TaskThread");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the ThreadTask");
        }

        #endregion

    }
}
