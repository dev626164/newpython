﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;
    using System.Threading;
    using System.Xml.Linq;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.CloudService.WorkerRole.StationDataEntryTypes;
    using System.Xml;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class StationDataEntryWorkerTask : IWorkerTask
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudQueue dataEntryMessageQueue;
        private CloudBlobClient blobClient;
        private CloudBlobContainer stationDataContainer;
        private bool IsRunning;
        private IStationData stationDataEntry;

        public StationDataEntryWorkerTask(IStationData stationDataInput)
        {
            this.stationDataEntry = stationDataInput;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AirStationDataEntryWorkerRole"/> class.
        /// </summary>
        private void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            this.blobClient = this.account.CreateCloudBlobClient();
                        
            this.dataEntryMessageQueue = this.queueClient.GetQueueReference(this.stationDataEntry.GetQueueName());
            this.dataEntryMessageQueue.CreateIfNotExist();
                        
            this.stationDataContainer = this.blobClient.GetContainerReference(this.stationDataEntry.GetBlobContainerName());
            this.stationDataContainer.CreateIfNotExist();
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                try
                {
                    while (IsRunning)
                    {
                        Thread.Sleep(100);

                        // need to find the max invisability timeout.
                        CloudQueueMessage message = this.dataEntryMessageQueue.GetMessage();

                        if (message != null)
                        {
                            this.MessageReceived(message);
                        }
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void MessageReceived(CloudQueueMessage message)
        {
            string blobName = message.AsString;

            XElement rootElement = this.GetRootElement(blobName, message);

            if (rootElement != null)
            {
                bool dataEntered = this.stationDataEntry.InputData(rootElement, 5);

                // if the data is entered, delete the blob and message
                if (dataEntered)
                {
                    try
                    {
                        this.stationDataContainer.GetBlobReference(blobName).DeleteIfExists();
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an air station data entry blob");
                    }

                    try
                    {
                        this.dataEntryMessageQueue.DeleteMessage(message);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an air station data entry message from the queue");
                    }
                }
            }
        }

        private XElement GetRootElement(string blobName, CloudQueueMessage message)
        {
            // get the blob specified in the message and enter it to the database
            XElement rootElement = null;

            if (this.stationDataContainer != null && this.dataEntryMessageQueue != null)
            {
                var dataBlob = this.stationDataContainer.GetBlobReference(blobName);
                if (dataBlob.Exists())
                {
                    string contentString = System.Text.Encoding.Unicode.GetString(dataBlob.DownloadByteArray());

                    if (contentString != null)
                    {
                        try
                        {
                            rootElement = XElement.Parse(contentString);
                        }
                        catch (XmlException ex)
                        {
                            ApplicationEnvironment.LogWarning("Station data entry: Data not in XML Format details:" + ex.Message);
                        }

                        // if it could not be parsed, then throw the blob and message away as it is invalid
                        if (rootElement == null)
                        {
                            try
                            {
                                dataBlob.DeleteIfExists();
                            }
                            catch (StorageClientException)
                            {
                                ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a station data entry blob");
                            }

                            try
                            {
                                this.dataEntryMessageQueue.DeleteMessage(message);
                            }
                            catch (StorageClientException)
                            {
                                ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a station data entry message from the queue");
                            }
                        }
                    }
                }
                else
                {
                    // if the blob doesn't exist, get rid of the message
                    try
                    {
                        this.dataEntryMessageQueue.DeleteMessage(message);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a station data entry message from the queue");
                    }
                }
            }

            return rootElement;
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting StationDataEntryWorkerTask");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the StationDataEntryWorkerTask");
        }


        #endregion
    }
}
