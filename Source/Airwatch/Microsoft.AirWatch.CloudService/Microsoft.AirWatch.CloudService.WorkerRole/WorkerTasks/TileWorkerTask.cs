﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;
    using Microsoft.WindowsAzure;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.CloudService.WorkerRole.Crunch;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class TileWorkerTask : IWorkerTask
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudQueue heatMapTileServerQueue;
        private CloudBlobClient blobClient;
        private CloudBlobContainer blobContainer;
        private bool IsRunning;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            this.heatMapTileServerQueue = this.queueClient.GetQueueReference("heatmaptileserverqueue");
            this.heatMapTileServerQueue.CreateIfNotExist();
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                Thread.CurrentThread.Name = "TileWorkerTask";

                try
                {
                    while (IsRunning)
                    {
                        Thread.Sleep(100);

                        // need to find the max invisability timeout.
                        CloudQueueMessage message = this.heatMapTileServerQueue.GetMessage();

                        if (message != null)
                        {
                            this.MessageReceived(message);
                        }
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void MessageReceived(CloudQueueMessage message)
        {
            CrunchMessage recivedMessage = (CrunchMessage)QueueHelper.DeserializeMessage(message, typeof(CrunchMessage));
            var quadKey = recivedMessage.QuadKey;

            ApplicationEnvironment.LogInformation("tile: " + quadKey);

            TileCruncher cruncher = new TileCruncher();
                
            cruncher.CalculateImageData().Crunch(quadKey);

            try
            {
                this.heatMapTileServerQueue.DeleteMessage(message);
            }
            catch (StorageClientException)
            {
                ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an tile message from the queue");
            }
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting TileWorkerTask");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the TileWorkerTask");
        }

        #endregion
    }
}
