﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Drawing;
    using System.Reflection;
    using System.Diagnostics;
    using System.Threading;
    using Microsoft.AirWatch.Common;
    using System.Collections.ObjectModel;
    using Microsoft.AirWatch.Core.ApplicationServices.DataStructures;
    using System.IO;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class MapModelDataEntryWorkerTask : IWorkerTask
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudQueue dataEntryMessageQueue;
        private CloudQueue heatMapTileMessageQueue;
        private CloudBlobClient blobClient;
        private CloudBlobContainer mapModelDataContainer;
        private CloudBlobContainer heatMapTileContainer;
        private Bitmap palette;
        
        /// <summary>
        /// Last level to generate.
        /// </summary>
        private int floorLevel;

        /// <summary>
        /// Character array for the quad keys.
        /// </summary>
        private string[] characterArray = new string[4] { "0", "1", "2", "3" };
        
        private bool IsRunning;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapModelDataEntryWorkerRole"/> class.
        /// </summary>
        private void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            this.dataEntryMessageQueue = this.queueClient.GetQueueReference("mapmodeldataqueue");
            this.dataEntryMessageQueue.CreateIfNotExist();

            this.heatMapTileMessageQueue = this.queueClient.GetQueueReference("heatmaptileserverqueue");
            this.heatMapTileMessageQueue.CreateIfNotExist();

            this.blobClient = this.account.CreateCloudBlobClient();
            this.mapModelDataContainer = this.blobClient.GetContainerReference("mapmodeldatablob");
            this.mapModelDataContainer.CreateIfNotExist();

            this.heatMapTileContainer = this.blobClient.GetContainerReference("heatmaptileserver");
            this.heatMapTileContainer.CreateIfNotExist();

            this.palette = new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.CloudService.WorkerRole.TileHeatMap.palette.png"));
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                try
                {
                    while (IsRunning)
                    {
                        // Poll every 6 minutes so that it does not try entering more than one at a time
                        Thread.Sleep(6000 * 60);

                        // need to find the max invisability timeout.
                        CloudQueueMessage message = this.dataEntryMessageQueue.GetMessage(new TimeSpan(6000 * 60));

                        if (message != null)
                        {
                            this.MessageReceived(message);
                        }
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    if (ex.InnerException != null)
                    {
                        Trace.TraceError("  inner exception: " + ex.InnerException.Message);
                    }

                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void MessageReceived(CloudQueueMessage message)
        {
            string blobName = message.AsString;
            bool dataEntered = false;

            // get the blob specified in the message and enter it to the database
            CloudBlob dataBlob = this.mapModelDataContainer.GetBlobReference(blobName);

            if (dataBlob.Exists())
            {
                Trace.TraceInformation("Map Model data entry: found blob, decoding and entering data");
                string data = Encoding.Unicode.GetString(dataBlob.DownloadByteArray());
                
                long timestamp;
                float resolution;
                Collection<MapModelPoint> points = GetPointsFromData(data, out timestamp, out resolution);

                // if the data is incorrect, delete the message
                if (points != null && points.Count > 1000)
                {
                    ApplicationEnvironment.LogInformation("Map Model data entry: entering data");

                    // now we have the points, update the database and update the tile server
                    dataEntered = ServiceProvider.CenterLocationService.InputData(points, timestamp, 5);

                    if (dataEntered)
                    {
                        // set the resolution where needed.
                        SetResolution(resolution);
                    }

                    // Place in table storage
                    MapModelBoundsEntity mapModelBoundsEntity = new MapModelBoundsEntity()
                    {
                        MinimumLongitude = points.Min(p => p.Longitude),
                        MaximumLongitude = points.Max(p => p.Longitude),
                        MinimumLatitude = points.Min(p => p.Latitude),
                        MaximumLatitude = points.Max(p => p.Latitude)
                    };

                    MapModelBoundsHelper.MapModelBounds = mapModelBoundsEntity;

                    // Generate Heatmap source image
                    int width = Math.Abs((int)((mapModelBoundsEntity.MaximumLongitude - mapModelBoundsEntity.MinimumLongitude) / 0.5));
                    int height = Math.Abs((int)((MapModelBoundsHelper.GetMercatorProjection(mapModelBoundsEntity.MaximumLatitude) - MapModelBoundsHelper.GetMercatorProjection(mapModelBoundsEntity.MinimumLatitude)) / 0.5));
                    byte[,] byteArray = CreateArray(points, width, height, mapModelBoundsEntity.MaximumLatitude, mapModelBoundsEntity.MinimumLongitude, 0.5);
                    Bitmap bitmap = new Bitmap(width, height);
                    Stream stream = Interpolator.InterpolateIncrease(bitmap, byteArray, 1, this.palette);
                    stream.Position = 0;

                    CloudBlobContainer imageBlobContainer = this.blobClient.GetContainerReference("heatmapsourceimageblob");
                    imageBlobContainer.CreateIfNotExist();

                    CloudBlob imageBlob = imageBlobContainer.GetBlobReference("heatMapSourceImage.png");
                    imageBlob.Attributes.Properties.ContentType = "image/png";
                    imageBlob.UploadFromStream(stream);

                    int zoomLevel = 6;
                    List<string> quadKeys = this.GenerateQuadKeysWrapper("013", zoomLevel);
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("031", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("102", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("103", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("033", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("122", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("120", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("121", zoomLevel));
                    quadKeys.AddRange(this.GenerateQuadKeysWrapper("123", zoomLevel));
                    quadKeys.Add("0");
                    quadKeys.Add("1");
                    quadKeys.Add("03");
                    quadKeys.Add("12");
                    quadKeys.Add("01");
                    quadKeys.Add("10");

                    CrunchMessage crunchMessage = new CrunchMessage();
                    foreach (string quadKey in quadKeys)
                    {
                        crunchMessage.QuadKey = quadKey;
                        crunchMessage.ImageOverlayType = ImageType.HeatMap;
                        this.heatMapTileMessageQueue.AddMessage(QueueHelper.SerializeMessage(crunchMessage));
                    }
                }
                else
                {
                    // if the data is corrupt (i.e. not enough lines, or cannot read), delete the message and the blob
                    ApplicationEnvironment.LogError("MapModelDataEntry: Problem reading the data from the file given, disposing");

                    try
                    {
                        this.dataEntryMessageQueue.DeleteMessage(message);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an air station data entry message from the queue");
                    }

                    try
                    {
                        dataBlob.DeleteIfExists();
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a map model data entry blob");
                    }
                }
            }
            else
            {
                // if the blob doesn't exist, get rid of the message
                try
                {
                    this.dataEntryMessageQueue.DeleteMessage(message);
                }
                catch (StorageClientException)
                {
                    ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an air station data entry message from the queue");
                }
            }

            if (dataEntered)
            {
                try
                {
                    dataBlob.DeleteIfExists();
                }
                catch (StorageClientException)
                {
                    ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a map model data entry blob");
                }

                try
                {
                    this.dataEntryMessageQueue.DeleteMessage(message);
                }
                catch (StorageClientException)
                {
                    ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a map model data entry message from the queue");
                }
            }
        }

        /// <summary>
        /// Gets the points from data.
        /// </summary>
        /// <param name="data">The base64 encoded string to get the data from.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="resolution">The resolution of the map.</param>
        /// <returns>Collection of map model points</returns>
        private static Collection<MapModelPoint> GetPointsFromData(string data, out long timestamp, out float resolution)
        {
            byte[] binaryData = null;

            try
            {
                binaryData = Convert.FromBase64String(data);
            }
            catch (FormatException)
            {
                ApplicationEnvironment.LogWarning("Data not in base 64 format");
            }

            Collection<MapModelPoint> mapPoints = null;
            timestamp = 0;
            resolution = 0.5f;

            if (binaryData != null)
            {
                mapPoints = new Collection<MapModelPoint>();

                using (BinaryReader binaryReader = new BinaryReader(new MemoryStream(binaryData)))
                {
                    int id_size;
                    int lines;
                    int columns;
                    int intResolution;

                    ApplicationEnvironment.LogVerbose((id_size = binaryReader.ReadInt32()).ToString(System.Globalization.CultureInfo.InvariantCulture));

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(binaryReader.ReadChars(id_size));

                    ApplicationEnvironment.LogVerbose(sb.ToString());
                    ApplicationEnvironment.LogVerbose((timestamp = binaryReader.ReadInt64()).ToString(System.Globalization.CultureInfo.InvariantCulture));
                    ApplicationEnvironment.LogVerbose((lines = binaryReader.ReadInt32()).ToString(System.Globalization.CultureInfo.InvariantCulture));
                    columns = binaryReader.ReadInt32();
                    ApplicationEnvironment.LogVerbose(columns.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    intResolution = binaryReader.ReadInt32();

                    resolution = ((float)intResolution) / 10;
                    ApplicationEnvironment.LogVerbose(resolution.ToString(System.Globalization.CultureInfo.InvariantCulture));

                    // check to see if lines has some valid amount of data in
                    if (lines > 1000)
                    {
                        for (int i = 0; i < lines; i++)
                        {
                            var mapPoint = new MapModelPoint()
                            {
                                Latitude = (double)binaryReader.ReadInt16() / 10,
                                Longitude = (double)binaryReader.ReadInt16() / 10,
                                QualityValue = (double)binaryReader.ReadInt16() / 10,
                                Ozone = (double)binaryReader.ReadInt16() / 10,
                                NO2 = (double)binaryReader.ReadInt16() / 10,
                                PM10 = (double)binaryReader.ReadInt16() / 10
                            };

                            mapPoint.Caqi = GetQualityRange(mapPoint.QualityValue);

                            if (mapPoint.Caqi > 0)
                            {
                                mapPoints.Add(mapPoint);
                            }
                        }
                    }
                    else
                    {
                        mapPoints = null;
                    }
                }
            }

            return mapPoints;
        }

        /// <summary>
        /// Gets the quality range.
        /// </summary>
        /// <param name="inputDobule">The input dobule.</param>
        /// <returns>The quality range</returns>
        private static double GetQualityRange(double inputDobule)
        {
            if (inputDobule > 0)
            {
                string airQualityRanges = "25;50;75;100;999";
                string[] rangesArray = airQualityRanges.Split(';');

                int i = 0;
                foreach (string rangeTopBound in rangesArray)
                {
                    i++;
                    if (inputDobule < int.Parse(rangeTopBound, System.Globalization.CultureInfo.InvariantCulture))
                    {
                        return (double)i;
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Sets the resolution.
        /// </summary>
        /// <param name="resolution">The new resolution.</param>
        private static void SetResolution(float resolution)
        {
            ServiceProvider.CenterLocationService.SetResolution(resolution);
        }

        /// <summary>
        /// Creates the array.
        /// </summary>
        /// <param name="mapPoints">The map points.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="maxLatitude">The max latitude.</param>
        /// <param name="minLongitude">The min longitude.</param>
        /// <param name="resolution">The resolution.</param>
        /// <returns>
        /// 2-dimensional array where 0th dimension is the longitude, 1st dimension is the lattitude and the value at each array point is the CAQI value (not index).
        /// </returns>
        private static byte[,] CreateArray(Collection<MapModelPoint> mapPoints, double width, double height, double maxLatitude, double minLongitude, double resolution)
        {
            byte[,] byteArray = new byte[(int)width + 1, (int)height + 1];

            foreach (MapModelPoint mapPoint in mapPoints)
            {
                int x = Math.Abs((int)((mapPoint.Longitude - minLongitude) / resolution));
                int y = Math.Abs((int)((MapModelBoundsHelper.GetMercatorProjection(maxLatitude) - MapModelBoundsHelper.GetMercatorProjection(mapPoint.Latitude)) / resolution));

                byteArray[x, y] = (byte)(mapPoint.QualityValue * (255 / 120));
            }

            return byteArray;
        }

        /// <summary>
        /// Generates the quad keys wrapper.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="level">The level.</param>
        /// <returns>List of all keys.</returns>
        public List<string> GenerateQuadKeysWrapper(string quadKey, int level)
        {
            this.floorLevel = level;
            return this.GenerateQuadKeys(quadKey);
        }

        /// <summary>
        /// Generates the quad keys.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>List of quad keys.</returns>
        public List<string> GenerateQuadKeys(string quadKey)
        {
            List<string> tempList = new List<string>();

            if (quadKey.Length != this.floorLevel)
            {
                for (int i = 0; i <= 3; i++)
                {
                    tempList.AddRange(this.GenerateQuadKeys(quadKey + this.characterArray[i]));
                }
            }

            tempList.Add(quadKey);
            return tempList;
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting MapModelDataEntryWorkerTask");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the ThreadTask");
        }

        #endregion
    }
}
