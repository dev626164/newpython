﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using Microsoft.AirWatch.Common;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Threading;
    using Microsoft.WindowsAzure;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class BatchGenerateWorkerTask : IWorkerTask
    {

        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudQueue batchQueue;
        private bool IsRunning;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            this.batchQueue = this.queueClient.GetQueueReference("batchgenerate");
            this.batchQueue.CreateIfNotExist();
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                try
                {
                    while (IsRunning)
                    {
                        Thread.Sleep(100);

                        // need to find the max invisability timeout.
                        CloudQueueMessage message = this.batchQueue.GetMessage();

                        if (message != null)
                        {
                            this.MessageReceived(message);
                        }
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void MessageReceived(CloudQueueMessage message)
        {
            try
            {
                string type = message.AsString;
                switch (type)
                {
                    case "AirStation":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.AirStation);
                        break;
                    case "WaterStation":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.WaterStation);
                        break;
                    case "UserFeedbackAir":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.UserFeedbackAir);
                        break;
                    case "UserFeedbackWater":
                        new BatchGenerateQuadKey().StartGenerate(LightMapType.UserFeedbackWater);
                        break;
                }

                this.batchQueue.DeleteMessage(message);
            }
            catch (Exception)
            {
                ApplicationEnvironment.LogWarning("Problem Batch Generating.");
            }
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting BatchGenerateWorkerTask");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the BatchGenerateWorkerTask");
        }

        #endregion
    }
}
