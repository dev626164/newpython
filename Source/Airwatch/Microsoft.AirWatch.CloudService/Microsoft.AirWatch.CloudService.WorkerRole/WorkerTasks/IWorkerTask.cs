﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    public interface IWorkerTask
    {
        void Start();

        void Stop();
    }
}
