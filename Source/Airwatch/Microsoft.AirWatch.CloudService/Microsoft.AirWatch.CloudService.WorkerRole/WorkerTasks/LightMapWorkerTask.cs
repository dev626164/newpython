﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Common;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class LightMapWorkerTask : IWorkerTask
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudQueue lightMapQueue;
        private bool IsRunning;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            this.lightMapQueue = this.queueClient.GetQueueReference("lightmapqueue");
            this.lightMapQueue.CreateIfNotExist();
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                Thread.CurrentThread.Name = "LightMapWorkerTask";
                try
                {
                    while (IsRunning)
                    {
                        Thread.Sleep(100);

                        // need to find the max invisability timeout.
                        CloudQueueMessage message = this.lightMapQueue.GetMessage();

                        if (message != null)
                        {
                            this.MessageReceived(message);
                        }
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void MessageReceived(CloudQueueMessage message)
        {
            try
            {
                LightMapMessage recivedMessage = (LightMapMessage)QueueHelper.DeserializeMessage(message, typeof(LightMapMessage));
                ServiceProvider.TileImageService.GenerateFromQuadKey(recivedMessage.QuadKey, recivedMessage.LightMapType, recivedMessage.IsOverlap);
                this.lightMapQueue.DeleteMessage(message);
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogWarning("Problem creating tile image.");
            }
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting LightMapWorkerTask");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the LightMapWorkerTask");
        }

        #endregion
    }
}
