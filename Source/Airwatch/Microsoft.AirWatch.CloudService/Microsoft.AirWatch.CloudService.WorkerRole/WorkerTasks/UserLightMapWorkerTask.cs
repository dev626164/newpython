﻿namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using Microsoft.AirWatch.Common;
    using System.Threading;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.WindowsAzure;
    using System.Collections.ObjectModel;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class UserLightMapWorkerTask : IWorkerTask
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudBlobClient blobClient;
        private CloudBlobContainer lockContainer;

        private CloudQueue lightMapQueue;

        private bool IsRunning;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void Initialize()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.queueClient = this.account.CreateCloudQueueClient();
            

            this.lightMapQueue = this.queueClient.GetQueueReference("lightmapqueue");
            this.lightMapQueue.CreateIfNotExist();

            this.blobClient = this.account.CreateCloudBlobClient();
            this.lockContainer = this.blobClient.GetContainerReference("userlighttimerblob");
            this.lockContainer.CreateIfNotExist();
            this.UpdateTimeBlob();
        }

        /// <summary>
        /// Updates the time BLOB.
        /// </summary>
        /// <returns>DateTime of the update</returns>
        private void UpdateTimeBlob()
        {
            try
            {
                DateTime now = DateTime.Now.ToUniversalTime();
                DateTime dateNextDue = new DateTime(now.Year, now.Month, now.Day + 1);

                var lockBlob = this.lockContainer.GetBlobReference("timeblob");
                lockBlob.Attributes.Properties.ContentType = "text/xml";
                lockBlob.UploadByteArray(BitConverter.GetBytes(dateNextDue.ToBinary()));
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogError(ex.ToString());
            }
        }

        private void TaskThread()
        {
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                try
                {
                    while (IsRunning)
                    {
                        Thread.Sleep(3600000);

                        this.TryAndGetLock();
                    }
                }
                catch (ThreadAbortException tae)
                {
                    // if error occours need to recycle
                    Trace.TraceError(tae.Message);
                    RoleEnvironment.RequestRecycle();
                }
                catch (Exception ex)
                {
                    // if error occours need to recycle
                    Trace.TraceError(ex.Message);
                    RoleEnvironment.RequestRecycle();
                }
            }
            );
        }

        private void TryAndGetLock()
        {
            DateTime blobTime = new DateTime();
            bool gotTime = false;

            if (!gotTime)
            {
                var timeBlob = this.lockContainer.GetBlobReference("timeblob");
                if (timeBlob.Exists())
                {
                    blobTime = DateTime.FromBinary(BitConverter.ToInt64(timeBlob.DownloadByteArray(), 0));
                }
                else
                {
                    this.UpdateTimeBlob();
                    blobTime = DateTime.Now.ToUniversalTime().AddSeconds(-1);
                }

                gotTime = true;
            }

            if (DateTime.Now.ToUniversalTime() > blobTime && gotTime)
            {
                Collection<string> quadKeys = new Collection<string>();
                quadKeys = GetUserFeedbackWithinGivenTime(blobTime, LightMapType.UserFeedbackAir);
                quadKeys = SplitQuadKeysIntoLevels(quadKeys);
                this.GenerateImagesForUnique(quadKeys, LightMapType.UserFeedbackAir);

                quadKeys = GetUserFeedbackWithinGivenTime(blobTime, LightMapType.UserFeedbackWater);
                quadKeys = SplitQuadKeysIntoLevels(quadKeys);
                this.GenerateImagesForUnique(quadKeys, LightMapType.UserFeedbackWater);

                this.UpdateTimeBlob();
                gotTime = false;
            }
        }

        /// <summary>
        /// Gets the user feedback in last24 hours.
        /// </summary>
        /// <param name="time">The time that the images are needed for.</param>
        /// <param name="type">The type of images to generate.</param>
        /// <returns>List of unique quad keys.</returns>
        public static Collection<string> GetUserFeedbackWithinGivenTime(DateTime time, LightMapType type)
        {
            UserRating[] userRatings = ServiceProvider.RatingService.GetUserRatingWithinGivenTime(time, type);
            UserRating[] deleteRatings = ServiceProvider.RatingService.GetUserRatingOutsideGivenTime(time.AddYears(-1), type);

            Collection<string> baseKeys = new Collection<string>();
            foreach (UserRating userRating in userRatings)
            {
                baseKeys.Add(ServiceProvider.TileImageService.GetQuadKeyForGivenLocation(userRating.Longitude, userRating.Latitude, 10));
            }

            foreach (UserRating userRating in deleteRatings)
            {
                baseKeys.Add(ServiceProvider.TileImageService.GetQuadKeyForGivenLocation(userRating.Longitude, userRating.Latitude, 10));
            }

            return baseKeys;
        }

        /// <summary>
        /// Splits the quad keys into levels.
        /// </summary>
        /// <param name="baseKeys">The base keys.</param>
        /// <returns>List of quad Keys.</returns>
        private static Collection<string> SplitQuadKeysIntoLevels(Collection<string> baseKeys)
        {
            string quadKeyBuilder = string.Empty;
            Collection<string> allQuadKeys = new Collection<string>();

            foreach (string quadKey in baseKeys)
            {
                for (int i = 0; i < quadKey.Length; i++)
                {
                    allQuadKeys.Add(quadKeyBuilder += quadKey[i]);
                }

                quadKeyBuilder = string.Empty;
            }

            HashSet<string> unique = new HashSet<string>(allQuadKeys);

            Collection<string> uniqueQuads = new Collection<string>();
            foreach (string s in unique)
            {
                uniqueQuads.Add(s);
            }

            return uniqueQuads;
        }

        /// <summary>
        /// Generates the images for unique.
        /// </summary>
        /// <param name="quadKeys">The quad keys.</param>
        /// <param name="type">The type of light map.</param>
        private void GenerateImagesForUnique(Collection<string> quadKeys, LightMapType type)
        {
            LightMapMessage message = new LightMapMessage();

            foreach (string quadKey in quadKeys)
            {
                message.QuadKey = quadKey;
                message.LightMapType = type;
                message.IsOverlap = false;
                this.lightMapQueue.AddMessage(QueueHelper.SerializeMessage(message));
            }
        }

        #region IWorkerTask Members

        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Initialize();
                this.IsRunning = true;

                Trace.TraceInformation("Starting UserLightMapWorkerTask");

                this.TaskThread();
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            Trace.TraceInformation("Stopping the UserLightMapWorkerTask");
        }

        #endregion
    }
}
