﻿//-----------------------------------------------------------------------
// <copyright file="UserLightMapWorkerRole.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>07/09/2009</date>
// <summary>Worker Role for processing user light map images.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Threading;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// Worker Role to poll the creation of new user feedback.
    /// </summary>
    public class UserLightMapWorkerRole
    {
        /// <summary>
        /// Azure account from config
        /// </summary>
        private StorageAccountInfo blobAccount;

        /// <summary>
        /// Azure blob storage
        /// </summary>
        private BlobStorage blobStorage;

        /// <summary>
        /// Azure blob container
        /// </summary>
        private BlobContainer blobContainer;

        /// <summary>
        /// Azure account from config
        /// </summary>
        private StorageAccountInfo queueAccount;

        /// <summary>
        /// Azure queue storage
        /// </summary>
        private QueueStorage queueStorage;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private MessageQueue lightMapQueue;

        /// <summary>
        /// Gets the user feedback in last24 hours.
        /// </summary>
        /// <param name="time">The time that the images are needed for.</param>
        /// <param name="type">The type of images to generate.</param>
        /// <returns>List of unique quad keys.</returns>
        public static Collection<string> GetUserFeedbackWithinGivenTime(DateTime time, LightMapType type)
        {           
            UserRating[] userRatings = ServiceProvider.RatingService.GetUserRatingWithinGivenTime(time, type);
            UserRating[] deleteRatings = ServiceProvider.RatingService.GetUserRatingOutsideGivenTime(time.AddYears(-1), type);
            
            Collection<string> baseKeys = new Collection<string>();
            foreach (UserRating userRating in userRatings)
            {
                baseKeys.Add(ServiceProvider.TileImageService.GetQuadKeyForGivenLocation(userRating.Longitude, userRating.Latitude, 10));
            }

            foreach (UserRating userRating in deleteRatings)
            {
                baseKeys.Add(ServiceProvider.TileImageService.GetQuadKeyForGivenLocation(userRating.Longitude, userRating.Latitude, 10));                
            }

            return baseKeys;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.blobAccount = StorageAccountInfo.GetDefaultBlobStorageAccountFromConfiguration();
            this.blobStorage = BlobStorage.Create(this.blobAccount);
            this.blobContainer = this.blobStorage.GetBlobContainer("userlighttimerblob");

            // need to ensure that there is a blob container and time blob for use.
            if (!this.blobContainer.DoesContainerExist())
            {
                if (!this.blobContainer.CreateContainer(null, ContainerAccessControl.Private))
                {
                    ApplicationEnvironment.LogError("Can not create blob container");
                }
                else
                {
                    this.UpdateTimeBlob();
                }
            }
            else
            {
                if (!this.blobContainer.DoesBlobExist("timeblob"))
                {
                    this.UpdateTimeBlob();
                }
            }

            this.queueAccount = ApplicationEnvironment.AccountSettings["QueueStorageEndpoint"];
            this.queueStorage = QueueStorage.Create(this.queueAccount);
            this.lightMapQueue = this.queueStorage.GetQueue("lightmapqueue");

            if (!this.lightMapQueue.DoesQueueExist())
            {
                this.lightMapQueue.CreateQueue();
                ApplicationEnvironment.LogInformation("Light map tile server queue created");
            }
            else
            {
                ApplicationEnvironment.LogInformation("Light map tile server queue already exists");
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            System.Timers.Timer myTimer = new System.Timers.Timer();
            myTimer.Elapsed += new System.Timers.ElapsedEventHandler(myTimer_Elapsed);
            myTimer.Interval = 3600000;
            myTimer.Start();
        }

        /// <summary>
        /// Handles the Elapsed event of the myTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void myTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            DateTime blobTime = new DateTime();
            bool gotTime = false;

            if (!gotTime)
            {
                BlobContents blobContents = new BlobContents(new MemoryStream());
                if (this.blobContainer.DoesBlobExist("timeblob"))
                {
                    this.blobContainer.GetBlob("timeblob", blobContents, false);
                    blobTime = DateTime.FromBinary(BitConverter.ToInt64(blobContents.AsBytes(), 0));
                }
                else
                {
                    this.UpdateTimeBlob();
                    blobTime = DateTime.Now.ToUniversalTime().AddSeconds(-1);
                }

                gotTime = true;
            }

            if (DateTime.Now.ToUniversalTime() > blobTime && gotTime)
            {
                Collection<string> quadKeys = new Collection<string>();
                quadKeys = GetUserFeedbackWithinGivenTime(blobTime, LightMapType.UserFeedbackAir);
                quadKeys = SplitQuadKeysIntoLevels(quadKeys);
                this.GenerateImagesForUnique(quadKeys, LightMapType.UserFeedbackAir);

                quadKeys = GetUserFeedbackWithinGivenTime(blobTime, LightMapType.UserFeedbackWater);
                quadKeys = SplitQuadKeysIntoLevels(quadKeys);
                this.GenerateImagesForUnique(quadKeys, LightMapType.UserFeedbackWater);
                
                this.UpdateTimeBlob();
                gotTime = false;
            }
        }

        /// <summary>
        /// Splits the quad keys into levels.
        /// </summary>
        /// <param name="baseKeys">The base keys.</param>
        /// <returns>List of quad Keys.</returns>
        private static Collection<string> SplitQuadKeysIntoLevels(Collection<string> baseKeys)
        {
            string quadKeyBuilder = string.Empty;
            Collection<string> allQuadKeys = new Collection<string>();

            foreach (string quadKey in baseKeys)
            {
                for (int i = 0; i < quadKey.Length; i++)
                {
                    allQuadKeys.Add(quadKeyBuilder += quadKey[i]);
                }

                quadKeyBuilder = string.Empty;
            }

            HashSet<string> unique = new HashSet<string>(allQuadKeys);

            Collection<string> uniqueQuads = new Collection<string>();
            foreach (string s in unique)
            {
                uniqueQuads.Add(s);
            }

            return uniqueQuads;
        }

        /// <summary>
        /// Generates the images for unique.
        /// </summary>
        /// <param name="quadKeys">The quad keys.</param>
        /// <param name="type">The type of light map.</param>
        private void GenerateImagesForUnique(Collection<string> quadKeys, LightMapType type)
        {
            LightMapMessage message = new LightMapMessage();

            foreach (string quadKey in quadKeys)
            {
                message.QuadKey = quadKey;
                message.LightMapType = type;
                message.IsOverlap = false;
                this.lightMapQueue.PutMessage(QueueHelper.SerializeMessage(message));
            }
        }

        /// <summary>
        /// Updates the time BLOB.
        /// </summary>
        /// <returns>DateTime of the update</returns>
        private void UpdateTimeBlob()
        {
            try
            {
                DateTime now = DateTime.Now.ToUniversalTime();
                DateTime dateNextDue = new DateTime(now.Year, now.Month, now.Day + 1);

                this.blobContainer.CreateBlob(
                   new BlobProperties("timeblob") { ContentType = "text/xml" },
                   new BlobContents(BitConverter.GetBytes(dateNextDue.ToBinary())),
                   true);
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogError(ex.ToString());
            }
        }
    }
}
