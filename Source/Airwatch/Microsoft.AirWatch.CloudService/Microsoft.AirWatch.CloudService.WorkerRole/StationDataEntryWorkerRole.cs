﻿//-----------------------------------------------------------------------
// <copyright file="StationDataEntryWorkerRole.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>4 September 2009</date>
// <summary>Abstract class for processing station data entry</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using System.Diagnostics;
using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using Microsoft.AirWatch.Common;


    /// <summary>
    /// Abstract class for processing station data entry
    /// </summary>
    public abstract class StationDataEntryWorkerRole
    {
        protected CloudStorageAccount account { get; set; }

        /// <summary>
        /// Gets or sets the Azure queue storage
        /// </summary>
        protected CloudQueueClient QueueClient { get; set; }

        /// <summary>
        /// Gets or sets the Azure message queue
        /// </summary>
        protected CloudQueue DataEntryMessageQueue { get; set; }

        /// <summary>
        /// Gets or sets the Azure blob storage
        /// </summary>
        protected CloudBlobClient BlobClient { get; set; }


        /// <summary>
        /// Gets or sets the Azure blob container
        /// </summary>
        protected CloudBlobContainer BlobContainer { get; set; }

        /// <summary>
        /// Gets the root element.
        /// </summary>
        /// <param name="blobName">Name of the BLOB.</param>
        /// <param name="e">The <see cref="Microsoft.Samples.ServiceHosting.StorageClient.MessageReceivedEventArgs"/> instance containing the event data.</param>
        /// <returns>The root element of the XML, null if not parsed</returns>
        protected XElement GetRootElement(string blobName, CloudQueueMessage e)
        {
            // get the blob specified in the message and enter it to the database
            XElement rootElement = null;

            if (this.BlobContainer != null && this.DataEntryMessageQueue != null)
            {
                var dataBlob = this.BlobContainer.GetBlobReference(blobName);
                if (dataBlob.Attributes.Properties.Length > 0)
                {
                    string contentString = System.Text.Encoding.Unicode.GetString(dataBlob.DownloadByteArray());
                    
                    if (contentString != null)
                    {
                        try
                        {
                            rootElement = XElement.Parse(contentString);
                        }
                        catch (XmlException ex)
                        {
                            ApplicationEnvironment.LogWarning("Station data entry: Data not in XML Format details:" + ex.Message);
                        }

                        // if it could not be parsed, then throw the blob and message away as it is invalid
                        if (rootElement == null)
                        {
                            try
                            {
                                dataBlob.DeleteIfExists();
                            }
                            catch (StorageClientException)
                            {
                                ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a station data entry blob");
                            }

                            try
                            {
                                this.DataEntryMessageQueue.DeleteMessage(e);
                            }
                            catch (StorageClientException)
                            {
                                ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a station data entry message from the queue");
                            }
                        }
                    }
                }
                else
                {
                    // if the blob doesn't exist, get rid of the message
                    try
                    {
                        this.DataEntryMessageQueue.DeleteMessage(e);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete a station data entry message from the queue");
                    }
                }
            }

            return rootElement;
        }
    }
}
