﻿//-----------------------------------------------------------------------
// <copyright file="WaterStationDataEntryWorkerRole.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>04 September 2009</date>
// <summary>Class to be used by the worker role for Water Station Data Entry</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.CloudService.WorkerRole
{
    #region Using

    using System.Xml.Linq;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    
    #endregion

    /// <summary>
    /// Class to be used by the worker role for Water Station Data Entry
    /// </summary>
    public class WaterStationDataEntryWorkerRole : StationDataEntryWorkerRole
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StationDataEntryWorkerRole"/> class.
        /// </summary>
        public override void Initialize()
        {
            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            var queueClient = account.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference("waterstationdataqueue");

            //this.QueueAccount = ApplicationEnvironment.AccountSettings["QueueStorageEndpoint"];
            //this.QueueStorage = QueueStorage.Create(this.QueueAccount);
            //this.DataEntryMessageQueue = this.QueueStorage.GetQueue("waterstationdataqueue");

            if (!this.DataEntryMessageQueue.DoesQueueExist())
            {
                if (this.DataEntryMessageQueue.CreateQueue())
                {
                    ApplicationEnvironment.LogInformation("Bathing Water Station queue created");
                }
                else
                {
                    ApplicationEnvironment.LogError("Can not create Bathing Water Station queue");
                }
            }
            else
            {
                ApplicationEnvironment.LogInformation("Bathing Water Station queue already exists");
            }

            this.BlobAccount = StorageAccountInfo.GetDefaultBlobStorageAccountFromConfiguration();
            this.BlobStorage = BlobStorage.Create(this.BlobAccount);
            this.BlobContainer = this.BlobStorage.GetBlobContainer("waterstationdatablob");

            if (!this.BlobContainer.DoesContainerExist())
            {
                if (!this.BlobContainer.CreateContainer())
                {
                    ApplicationEnvironment.LogError("Can not create blob container");
                }
            }

            this.DataEntryMessageQueue.MessageReceived += new MessageReceivedEventHandler(this.DataEntryMessageQueue_MessageReceived);
            this.DataEntryMessageQueue.StartReceiving();
            ApplicationEnvironment.LogInformation("Bathing Water Station Data Queue started receiving");
        }

        /// <summary>
        /// Handles the MessageReceived event of the dataEntryMessageQueue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Samples.ServiceHosting.StorageClient.MessageReceivedEventArgs"/> instance containing the event data.</param>
        private void DataEntryMessageQueue_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            string blobName = e.Message.ContentAsString();

            XElement rootElement = this.GetRootElement(blobName, e);

            if (rootElement != null)
            {
                bool dataEntered = ServiceProvider.StationService.InputWaterStationData(rootElement, 5);

                // if the data is entered, delete the blob and message
                if (dataEntered)
                {
                    try
                    {
                        this.BlobContainer.DeleteBlob(blobName);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an water station data entry blob");
                    }

                    try
                    {
                        this.DataEntryMessageQueue.DeleteMessage(e.Message);
                    }
                    catch (StorageClientException)
                    {
                        ApplicationEnvironment.LogWarning("Storage Client exception when trying to delete an water station data entry message from the queue");
                    }
                }

                ////if (ApplicationEnvironment.ApplicationModel == ApplicationModel.Desktop)
                ////{
                ////    MessageQueue testQueue = this.QueueStorage.GetQueue("testqueue");
                ////    if (!testQueue.DoesQueueExist())
                ////    {
                ////        testQueue.CreateQueue();
                ////    }

                ////    testQueue.PutMessage(new Message(dataEntered.ToString()));
                ////}
            }
        }
    }
}
