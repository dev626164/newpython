﻿// <copyright file="Help.aspx.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>06-07-2009</date>
// <summary>Site help page</summary>

namespace AirWatch.CloudService.WebRole
{
    using System;
    using Microsoft.AirWatch.AzureTables;

    /// <summary>
    /// Class containing help page functions
    /// </summary>
    public partial class Help : System.Web.UI.Page
    {
        /// <summary>
        /// Event handling page load event
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
