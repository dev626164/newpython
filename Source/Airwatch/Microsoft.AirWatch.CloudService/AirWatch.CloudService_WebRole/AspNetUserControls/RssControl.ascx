﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RssControl.ascx.cs" Inherits="AirWatch.CloudService.WebRole.AspNetUserControls.RSSControl" %>
<asp:Repeater ID="RSSRepeater" runat="server">  
    <ItemTemplate> 
    <div>
        <span><a href="<%# Eval("Url") %>">
        <%# Eval("Title")%>
        </a>
        </span>
    </div>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>