﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BarsDisplayControl.ascx.cs"
    Inherits="AirWatch.CloudService.WebRole.AspNetUserControls.BarsDisplayControl" %>
<table>
    <tr id="ReadingBarList">
        <td width="22px;" style="color: #CCCCCC; position: absolute; left: 6px; padding-right: 2px;
            vertical-align: bottom;">
            <img style="width: 20px;" src='../AspNetVisualAssets/MeasurementIcons/Scale.png' alt='Scale (0 to 400)'/>
        </td>
        <td style="width: 22px; position: relative; vertical-align: bottom;">
        </td>
        <asp:ListView ID="barsControl" runat="server" ItemPlaceholderID="barPlaceHolder">
            <LayoutTemplate>
                <asp:PlaceHolder runat="server" ID="barPlaceholder" />
            </LayoutTemplate>
            <ItemTemplate>
                <td style="vertical-align: top; height: 80px; width: 45px; position: relative;">
                    <div id="SurroundingBar" style="vertical-align: top; height: 81px; width: 25px; border: solid 1px white;
                        position: absolute;">
                        <%# GetHtmlForThresholds(Eval("Measurement"))%>
                        <img src="../Images/blankbar.png"  title="<%# GetTooltipText(Eval("Measurement")) %>" alt="<%# GetTooltipText(Eval("Measurement")) %>" style="z-index:9999; position: absolute;"/>
                        <div style="position: relative; height: <%# GetBarSize(Convert.ToDouble(Eval("Measurement.Value"))) %>px;
                            top: <%# 81 - GetBarSize(Convert.ToDouble(Eval("Measurement.Value"))) %>px;
                            background-color: <%# GetBarColor(Eval("Measurement")) %>">                      
                    </div>
                    <div style="color: White; position: absolute; vertical-align: bottom; bottom: 0px; right: -12px;">
                        <img style="width: 8px;" src='../AspNetVisualAssets/MeasurementIcons/<%#Eval("Measurement.Component.Caption")%>.png' alt='<%#Eval("Measurement.Component.Caption")%>'/>
                    </div>
                </td>
            </ItemTemplate>
            <EmptyDataTemplate>
                Data not available
            </EmptyDataTemplate>
        </asp:ListView>
    </tr>
</table>
