﻿//-----------------------------------------------------------------------
// <copyright file="RssControl.ascx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>27-10-2209</date>
// <summary>Downloads and RSS feed and displays it.</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.AspNetUserControls
{
    #region Using

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    #endregion

    /// <summary>
    /// Downloads and RSS feed and displays it.
    /// </summary>
    public partial class RSSControl : System.Web.UI.UserControl
    {     
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the URL to the RSS Feed.
        /// </summary>
        /// <value>The URL to the RSS Feed</value>
        public Uri Uri { get; set; }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RSSRepeater.DataSource = this.RefreshFeed();
            this.RSSRepeater.DataBind();
        }

        /// <summary>
        /// Refreshes the feed.
        /// </summary>
        /// <returns>A DataSet with the RSS Feed XML in it.</returns>
        private List<RSSEntry> RefreshFeed()
        {
            XDocument rssFeed = XDocument.Load(this.Uri.ToString());

            var posts = from item in rssFeed.Descendants("item")
                        select new RSSEntry
                        {
                            Title = item.Element("title").Value,                         
                            Url = new Uri(item.Element("link").Value)
                        };

            return posts.ToList();
        }

        /// <summary>
        /// RssEntry class for RSSEntries.
        /// </summary>
        protected class RSSEntry
        {
            /// <summary>
            /// Gets or sets the title.
            /// </summary>
            /// <value>The title.</value>
            public string Title { get; set; }

            /// <summary>
            /// Gets or sets the URL.
            /// </summary>
            /// <value>The URL of the RSS Feed.</value>
            public Uri Url { get; set; }
        }
    }
}