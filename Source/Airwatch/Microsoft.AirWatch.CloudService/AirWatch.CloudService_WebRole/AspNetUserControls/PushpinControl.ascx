﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PushpinControl.ascx.cs"
    Inherits="AirWatch.CloudService.WebRole.AspNetUserControls.PushpinControl" %>
<%@ Register TagPrefix="AirWatch" TagName="TagCloud" Src="TagCloudControl.ascx" %>
<%@ Register TagPrefix="AirWatch" TagName="BarsDisplayControl" Src="BarsDisplayControl.ascx" %>
<asp:Panel ID="HasDataPanel" runat="server">
    <div class="PushpinToolbar">
    <div id="Pushpin" class="PushpinContainer">
        <div class="PushpinStationInfo">
            <asp:Label ID="StationNameLabel" runat="server"/>
        </div>
        <span style="float: right">
            <img src="../AspNetVisualAssets/CloseIcon.png" class="PushpinContainerLink" alt='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_CLOSE", this.Culture) %>' title='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_CLOSE", this.Culture) %>' onclick="javascript:ClosePushpinControl()"/>
        </span>
        <img src="../AspNetVisualAssets/RateIcon.png" class="PushpinContainerLink" alt='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_RATE_TOOLTIP", this.Culture) %>' title='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_RATE_TOOLTIP", this.Culture) %>' onclick="javascript:SetUpRating(<%= this.PushpinId %>, '<%= this.StationTypeString %>', <%= this.StationId %>);" />
        <span runat="server" id="removeButton" class="PushpinContainerLink">  
            <img src="../AspNetVisualAssets/DeleteIcon.png" alt='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_DELETE_TOOLTIP", this.Culture) %>' title='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_DELETE_TOOLTIP", this.Culture) %>' onclick="javascript:RemovePushpin(<%= this.PushpinId %>);" />
        </span>
        <span runat="server" id="historyButton" class="PushpinContainerLink" visible="false">
            <img src="../AspNetVisualAssets/HistoricalIcon.png" alt='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_HISTORY_TOOLTIP", this.Culture) %>' title='<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_HISTORY_TOOLTIP", this.Culture) %>' onclick="javascript:SetUpHistory();" />
        </span>
    </div>
    <div class="PushpinContainer" style="position: absolute; top: 32px;">
        <div id="PushpinHomeView" class="PushPinMainBody" style='display: block;'>
             <div id="leftNoData" class="PushpinLeft" runat="server" visible="false">
                <center style="vertical-align:middle">
                        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture)%>                    
                </center>
            </div>
            <div id="PushpinLeft" class="PushpinLeft" runat="server">
                <div style="float: left; width: 30%; padding-right: 2px;">
                    <asp:Image ID="EEAImage" runat="server" />
                </div>
                <div style="overflow:hidden">
                    <span class="PushPinUpperCase">
                        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_OURRATING", this.Culture)%>
                    </span>
                    <br />
                    <span>
                        <asp:Label ID="EEARating" runat="server" CssClass="FlyOutRatingText" />
                    </span>
                </div>
                <div>
                    <div id="StationAddress" class="StationAddressLabel" style="float: left; left: 2px;">
                        <br />
                        <asp:Label ID="NearestStationLabel" runat="server" Visible="false"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_STATIONADDRESS", this.Culture)%></asp:Label>
                        <br />
                        <span style="color: White"><asp:Label ID="StationAddressLabel" runat="server" Visible="false" CssClass="NearestStationText"><%= this.Address %></asp:Label></span>
                    </div>
                </div>
                <br />
                <div>
                    <AirWatch:BarsDisplayControl runat="server" ID="BarsDisplayControl" MaxBarHeight="80"
                            MaxBarValue="400" />
                </div>
                <div id="PushpinIcons" class="PushpinBottomIcon" style="vertical-align: bottom; bottom: -25px;">
                    <asp:Image ID="UpdatedDate" runat="server" ToolTip="" ImageUrl="~/AspNetVisualAssets/DataInfoIcons/Calendar19x22.png"/>                        
                    <asp:Image ID="ImageType" runat="server" ToolTip="" ImageUrl="" Visible="false"/>
                </div>
            </div>
            <div id="rightNoData" class="PushpinRight" runat="server" visible="false">
                <center style="vertical-align:middle">
                        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture)%>                    
                </center>
            </div>
            <div id="yourRatingArea" class="PushpinRight" runat="server">
                <div style="float: left; width: 25%;">
                    <asp:Image ID="currentRating" runat="server" ImageUrl="~/AspNetVisualAssets/UserIcons/UserIconNoData.png"/>                        
                </div>
                <div>
                    <span >
                        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_YOURRATING", this.Culture) %>
                    </span>
                    <br />
                    <span class="FlyOutRatingText">
                         <asp:Label ID="currentRatingWord" runat="server"/>
                    </span>
                </div>
                <br />
                <div style="font-size:small; height: 94px; overflow-x:hidden;">
                    <AirWatch:TagCloud runat="server" ID="TagCloud" MaxTextSize="30" MinTextSize="6" />
                    <span ID="NoRatingCloud" runat="server" visible="false" />
                </div>
                <div id="RatingCount" align="center" style="position: relative; font-size: 8pt; top: 10px;">
                     <asp:Label ID="currentRatingCount" runat="server" /> ratings
                </div>
            </div>
        </div>
        <div id="PushpinRateView" style='display: none;'>
            <div style="color:White">
                <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_YOURRATING", this.Culture)%>
                <br />
                <span style="font-size: 8pt; color: gray">1: 
                <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_SELECTYOURRATING", this.Culture)%>
                </span>                
            </div>
            <div>
                <table id="ratingIcons" cellpadding="0" cellspacing="0" border="0" style="color: #C3C3C3;" width="100%">
                    <tr>
                        <td width="20%" align="center"><img id="userRating1" alt="User Rating 5" src="../AspNetVisualAssets/UserIcons/UserIcon5.png"/><br/><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", this.Culture) %></td>
                        <td width="20%" align="center"><img id="userRating2" alt="User Rating 4" src="../AspNetVisualAssets/UserIcons/UserIcon4.png"/><br/><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_BAD", this.Culture)%></td>
                        <td width="20%" align="center"><img id="userRating3" alt="User Rating 3" src="../AspNetVisualAssets/UserIcons/UserIcon3.png"/><br/><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", this.Culture)%></td>
                        <td width="20%" align="center"><img id="userRating4" alt="User Rating 2" src="../AspNetVisualAssets/UserIcons/UserIcon2.png"/><br/><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", this.Culture)%></td>
                        <td width="20%" align="center"><img id="userRating5" alt="User Rating 1" src="../AspNetVisualAssets/UserIcons/UserIcon1.png"/><br/><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_VERYGOOD", this.Culture)%></td>
                    </tr>
                </table>
            </div>
            <div style="font-size: 8pt; height: 16px; vertical-align: bottom; bottom: -4px; position: relative; color: gray">2: 
            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_SELECTTHECONDITION", this.Culture)%>
            </div>
            <br />
            <div id="wordList"  style="position: absolute; vertical-align: bottom; bottom: 34px;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; color: rgb(195, 195, 195);">
                <tbody><tr>
                <td width="20px"/>
                <td valign="top" width="240px">
                <asp:CheckBoxList ID="wordCheckBoxList" AutoPostBack="False" CellPadding="1" CellSpacing="0"
                    RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Right" 
                    runat="server">
                </asp:CheckBoxList>
                </td></tr></tbody>
                </table>
            </div>
            <div style="float: right; position: absolute; vertical-align: bottom; right: 8px; bottom: 40px; height: 20px;">
                <span id="FakeRateButton" class="PushpinContainerLink" style="color:Gray !important;">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_OK", this.Culture)%>
                </span>
                <span id="RateButton" class="PushpinContainerLink" onclick="javascript:RatePushpin(<%= this.PushpinId %>, document.getElementById('wordList'), '<%= this.StationTypeString %>', <%= this.StationId %>, '<%= this.GetUserIPAddress() %>');" style="display:none">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_OK", this.Culture)%>
                </span>
                <span class="PushpinContainerLink" onclick="javascript:CancelRate();">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_CANCEL", this.Culture)%>
                </span>
            </div>
        </div>
        <div id="PushpinRatedView" style='display: none;'>
            <div style="color:White">
                <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("RATELOCATION_ALREADYRATED", this.Culture)%>                                               
            </div>   
            <div style="float: right; position: absolute; vertical-align: bottom; right: 8px; bottom: 28px; height: 20px;">
               <span class="PushpinContainerLink" onclick="javascript:CancelRate();">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_CANCEL", this.Culture)%>
                </span>
            </div>         
        </div>
        <div id="PushpinHistoryView" style='display: none;'>
            <div style="width: 50%; float: left; color: white; font-size:7pt;">
                <span>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_ANNUALDATA", this.Culture)%>
                </span>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <th width="58px" align="left">
                            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_YEAR", this.Culture)%>
                        </th>
                        <th width="38px" align="left">
                            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_OURS", this.Culture)%>
                        </th>
                        <th align="left">
                            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_YOURS", this.Culture) %>
                        </th>
                    </tr>
                </table>
                <div id="ScrollListYearly" style="height: 135px; overflow-y: scroll; overflow-x:hidden; text-align: left;">
                    <asp:Repeater ID="YearlyData" runat="server">
                        <HeaderTemplate>
                            <table width="100%" cellspacing="2" cellpadding="0" border="0" style="font-size: 6pt;">
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="38%" valign="middle">
                                    <%# Eval("Measurement.Timestamp.Year")%>
                                </td>
                                <td width="20%" valign="middle">
                                    <img src='<%# GetImage(Eval("Measurement.Value").ToString()) %>' title='<%# this.GetRatingToolTip(Eval("Measurement.Value").ToString()) %>' alt='<%# this.GetRatingToolTip(Eval("Measurement.Value").ToString()) %>'/>  
                                </td>
                                <td width="20%" valign="middle">
                                    <img src='<%# GetUserImage(Eval("AggregatedUserRating").ToString())  %>' title='<%# this.GetUserToolTip(Eval("AggregatedUserRating").ToString()) %>' alt='<%# this.GetUserToolTip(Eval("AggregatedUserRating").ToString()) %>'/>
                                </td>
                                <td width="22%" valign="middle">
                                    (<%# Eval("AggregatedUserRatingCount")%>)
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div style="width: 50%; float: left; color: white; font-size:7pt;">
                <span>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_SEASONALDATA", this.Culture)%>
                </span>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <th width="58px" align="left">
                            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_DATE", this.Culture)%>
                        </th>
                        <th width="38px" align="left">
                            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_OURS", this.Culture)%>
                        </th>
                        <th align="left">
                            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HISTORICALDATA_YOURS", this.Culture)%>
                        </th>
                    </tr>
                </table>
                <div id="ScrollListSeasonal" style="height: 135px; overflow-y: scroll; overflow-x:hidden; text-align: left;">
                    <asp:Repeater ID="SeasonalData" runat="server">
                        <HeaderTemplate>
                            <table width="100%" cellspacing="2" cellpadding="0" border="0" style="font-size: 6pt;">
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="38%" valign="middle">
                                    <%# ((DateTime)Eval("Measurement.Timestamp.Date")).ToString("dd/MM/yyyy") %>
                                </td>
                                <td width="20%" valign="middle">
                                    <img src='<%# GetImage(Eval("Measurement.Value").ToString()) %>' title='<%# this.GetRatingToolTip(Eval("Measurement.Value").ToString()) %>' alt='<%# this.GetRatingToolTip(Eval("Measurement.Value").ToString()) %>'/>
                                </td>
                                <td width="20%" valign="middle">
                                    <img src='<%# GetUserImage(Eval("AggregatedUserRating").ToString()) %>' title='<%# this.GetUserToolTip(Eval("AggregatedUserRating").ToString()) %>' alt='<%# this.GetUserToolTip(Eval("AggregatedUserRating").ToString()) %>'/>
                                </td>
                                <td width="22%" valign="middle">
                                    (<%# Eval("AggregatedUserRatingCount")%>)
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div style="float: right; position: relative; bottom: -4px; height: 20px;">
                </span><span class="PushpinContainerLink" onclick="javascript:document.getElementById('PushpinHomeView').style.display='block'; javascript:document.getElementById('PushpinRateView').style.display='none';javascript:document.getElementById('PushpinHistoryView').style.display='none';">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_CANCEL", this.Culture) %>
                </span>
            </div>
        </div>
    </div>
</asp:Panel>
