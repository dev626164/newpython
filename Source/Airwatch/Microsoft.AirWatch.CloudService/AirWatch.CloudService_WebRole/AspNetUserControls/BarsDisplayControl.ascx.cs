﻿// <copyright file="BarsDisplayControl.ascx.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>11-08-2009</date>
// <summary>Partial class code-behind for BarsDisplayControl.aspx</summary>
namespace AirWatch.CloudService.WebRole.AspNetUserControls
{
    using System;
    using System.Data.Objects.DataClasses;
    using System.Globalization;
    using System.Text;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Control for displaying bars
    /// </summary>
    public partial class BarsDisplayControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// Collection of readings
        /// </summary>
        private EntityCollection<CentreLocationMeasurement> readings;

        /// <summary>
        /// Collection of station measurements.
        /// </summary>
        private EntityCollection<StationMeasurement> stationMeasurements;

        /// <summary>
        /// Gets or sets the maximum bar reading value
        /// </summary>
        public double MaxBarValue { get; set; }

        /// <summary>
        /// Gets or sets the maximum bar height
        /// </summary>
        public double MaxBarHeight { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the collection of readings
        /// </summary>
        public EntityCollection<CentreLocationMeasurement> Readings
        {
            get
            {
                return this.readings;
            }

            set
            {
                this.readings = value;
                this.UpdateReadings(false);
            }
        }

        /// <summary>
        /// Gets or sets the station measurements.
        /// </summary>
        public EntityCollection<StationMeasurement> StationMeasurements
        {
            get
            {
                return this.stationMeasurements;
            }

            set
            {
                this.stationMeasurements = value;
                this.UpdateReadings(true);
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>String representation of the value.</returns>
        protected static string GetBarValue(object input)
        {
            Measurement measurement = (Measurement)input;

            if (measurement.Value.HasValue)
            {
                return Math.Round(measurement.Value.GetValueOrDefault()).ToString(CultureInfo.InvariantCulture);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the color of the bar.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>color of the bar</returns>
        protected static string GetBarColor(object input)
        {
            Measurement measurement = (Measurement)input;

            if (measurement.Value.HasValue)
            {
                decimal value = measurement.Value.GetValueOrDefault();
                Threshold[] threshold = new Threshold[4];
                measurement.Component.Threshold.CopyTo(threshold, 0);

                if (value < threshold[0].Value)
                {
                    return "#1F5F00";
                }
                else if (value < threshold[1].Value)
                {
                    return "#71BF04";
                }
                else if (value < threshold[2].Value)
                {
                    return "#CECF04";
                }
                else if (value < threshold[3].Value)
                {
                    return "#FF8000";
                }
            }

            return "#930000";
        }

        /// <summary>
        /// Gets the name of the friendly.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Friendly name of the component.</returns>
        protected string GetFriendlyName(object input)
        {
            return LocalizationHelper.Instance.LocalizeString("COMPONENT_" + ((Measurement)input).Component.Caption.ToUpper(CultureInfo.InvariantCulture), this.Culture);
        }

        /// <summary>
        /// Returns the size of the bar
        /// </summary>
        /// <param name="measurement">double measurement</param>
        /// <returns>size of the bar</returns>
        protected double GetBarSize(double measurement)
        {
            double heightRatio = this.MaxBarHeight / Math.Log(this.MaxBarValue);
            double height = Math.Log(measurement + 1) * heightRatio;

            if (height > this.MaxBarHeight)
            {
                height = this.MaxBarHeight;
            }

            return height;
        }

        /// <summary>
        /// Gets the tooltip text.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Tooltip string for bars.</returns>
        protected string GetTooltipText(object input)
        {
            Measurement measurement = (Measurement)input;
            
            return this.GetFriendlyName(input) + " : " + GetBarValue(input) + measurement.Component.Unit;
        }

        /// <summary>
        /// Gets the code for thresholds.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>HTML to position the thresholds</returns>
        protected string GetHtmlForThresholds(object input)
        {
            Measurement measurement = (Measurement)input;

            StringBuilder stringBuilder = new StringBuilder();

            Threshold[] threshold = new Threshold[4];
            measurement.Component.Threshold.CopyTo(threshold, 0);

            for (int i = 3; i >= 0; i--)
            {
                stringBuilder.Append("<div style='z-index:9998; position: absolute; top: ");
                stringBuilder.Append(this.MaxBarHeight - this.GetBarSize(decimal.ToDouble(threshold[i].Value)));
                stringBuilder.Append("px'>");
                stringBuilder.Append("<img src='AspNetVisualAssets/threshold" + i + ".png' />");
                stringBuilder.Append("</div>");
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Method to update the reading values
        /// </summary>
        /// <param name="station">if set to <c>true</c> [station].</param>
        private void UpdateReadings(bool station)
        {
            if (station)
            {
                this.barsControl.DataSource = this.StationMeasurements;
            }
            else
            {
                this.barsControl.DataSource = this.Readings;
            }

            this.barsControl.DataBind();
        }
    }
}