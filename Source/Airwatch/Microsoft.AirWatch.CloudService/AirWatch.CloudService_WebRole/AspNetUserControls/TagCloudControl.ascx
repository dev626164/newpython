﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TagCloudControl.ascx.cs"
    Inherits="AirWatch.CloudService.WebRole.AspNetUserControls.TagCloudControl" %>
<asp:ListView ID="tagCloud" runat="server" ItemPlaceholderID="itemPlaceHolder">
    <LayoutTemplate>
        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
    </LayoutTemplate>
    <ItemTemplate>
        <span style="font-size:<%# GetTagClass(Eval("WordIdentifier").ToString()) %>em;">
            <%# GetLocalizedString(Eval("WordIdentifier").ToString()) %>
        </span>
    </ItemTemplate>
</asp:ListView>
 