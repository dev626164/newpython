﻿// <copyright file="StarControl.ascx.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>07-08-2009</date>
// <summary>Partial class code-behind for StarControl.aspx</summary>
namespace AirWatch.CloudService.WebRole.AspNetUserControls
{
    using System;
    using System.Text;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Stars that represent user rating
    /// </summary>
    public partial class StarControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// Number star rating is out of
        /// </summary>
        private int maxNumberOfStars = 5;

        /// <summary>
        /// Backing field for rating value passed to control
        /// </summary>
        private int compareValue;

        /// <summary>
        /// Backing field for IsReadonly property
        /// </summary>
        private bool isReadOnly = true;

        /// <summary>
        /// Backing field for number of user ratings made
        /// </summary>
        private int numberOfRatings;

        /// <summary>
        /// Gets or sets the rating value the control will
        /// </summary>
        public int CompareValue
        {
            get
            {
                return this.compareValue;
            }

            set
            {
                this.compareValue = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is read only.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly
        {
            get
            {
                return this.isReadOnly;
            }

            set
            {
                this.isReadOnly = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of ratings made
        /// </summary>
        public int NumberOfRatings
        {
            get
            {
                return this.numberOfRatings;
            }

            set
            {
                this.numberOfRatings = value;
            }
        }

        /// <summary>
        /// Gets or sets the max number of stars.
        /// </summary>
        /// <value>The max number of stars.</value>
        public int MaxNumberOfStars
        {
            get 
            { 
                return this.maxNumberOfStars; 
            }

            set 
            { 
                this.maxNumberOfStars = value; 
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<div>");

            for (int i = 1; i <= this.maxNumberOfStars; i++)
            {
                stringBuilder.Append("<img height='15' width='15' ");
                
                if (this.isReadOnly == false)
                {
                    stringBuilder.Append("onclick='javascript:RateClickStar(" + i + ", this.parentNode)' ");
                }

                if (i <= this.CompareValue)
                {
                    stringBuilder.Append("src='/AspNetVisualAssets/FullStar.jpg' ");
                }
                else
                {
                    stringBuilder.Append("src='/AspNetVisualAssets/EmptyStar.jpg' ");
                }

                stringBuilder.Append("/>");
            }

            if (this.isReadOnly == true)
            {
                stringBuilder.Append("(" + this.numberOfRatings + ")");  
            }

            stringBuilder.Append("</div>");

            Literal starLiteral = new Literal();
            starLiteral.Text = stringBuilder.ToString();
            this.StarPlaceholder.Controls.Add(starLiteral);
        }
    }
}