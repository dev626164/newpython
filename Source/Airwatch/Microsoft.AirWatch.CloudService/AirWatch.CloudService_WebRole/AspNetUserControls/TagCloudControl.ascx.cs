﻿// <copyright file="TagCloudControl.ascx.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>06-08-2009</date>
// <summary>Partial class code-behind for TagCloudControl.aspx</summary>
namespace AirWatch.CloudService.WebRole.AspNetUserControls
{
    using System;
    using System.Globalization;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Class for tag could control
    /// </summary>
    public partial class TagCloudControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// Average rating for tag cloud
        /// </summary>
        private AverageRating averageRating;

        /// <summary>
        /// Max text size for tags in cloud
        /// </summary>
        private int maxTextSize;

        /// <summary>
        /// Min text size for tags in cloud
        /// </summary>
        private int minTextSize;

        /// <summary>
        /// User culture
        /// </summary>
        private string culture;

        /// <summary>
        /// Tag Style for size of text.
        /// </summary>
        private double[] tagStyle = new double[8];

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public string Culture
        {
            get
            {
                return this.culture;
            }

            set
            {
                this.culture = value;
            }
        }

        /// <summary>
        /// Gets or sets the average rating
        /// </summary>
        public AverageRating AverageRating
        {
            get
            {
                return this.averageRating;
            }

            set
            {
                this.averageRating = value;
                this.UpdateRating();
            }
        }

        /// <summary>
        /// Gets or sets the max text size for tags
        /// </summary>
        public int MaxTextSize
        {
            get
            {
                return this.maxTextSize;
            }

            set
            {
                this.maxTextSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the min text.
        /// </summary>
        /// <value>The size of the min text.</value>
        public int MinTextSize
        {
            get
            {
                return this.minTextSize;
            }

            set
            {
                this.minTextSize = value;
            }
        }

        /// <summary>
        /// Gets the text size for a tag within the cloud
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>string fontSize</returns>
        public string GetTagClass(string input)
        {
            return this.tagStyle[int.Parse(input[input.Length - 1].ToString(CultureInfo.InvariantCulture), CultureInfo.CurrentCulture)].ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the localized string.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Localized version of the string.</returns>
        public string GetLocalizedString(string input)
        {
            return LocalizationHelper.Instance.LocalizeString(input, this.Culture);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Gets the em value.
        /// </summary>
        /// <param name="percentage">The percentage.</param>
        /// <returns>Size of the text.</returns>
        private static double GetEmValue(double percentage)
        {
            return ((2 - 0.5) * percentage) + 0.5;
        }

        /// <summary>
        /// Updates the average rating
        /// </summary>
        private void UpdateRating()
        {
            if (this.AverageRating != null && this.AverageRating.RatingQualifierIdentifiers != null)
            {
                var ratings = this.AverageRating.RatingQualifierIdentifiers;
                for (var i = 0; i < ratings.Count; i++)
                {
                    this.tagStyle[i] = GetEmValue(ratings[i].Percentage.GetValueOrDefault());
                }

                this.tagCloud.DataSource = this.AverageRating.RatingQualifierIdentifiers;
                this.tagCloud.DataBind();
            }
        }
    }
}