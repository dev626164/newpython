﻿//-----------------------------------------------------------------------
// <copyright file="PushpinControl.ascx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki, Mark Newman</author>
// <email>t-fikarn@microsoft.com, t-mnewma@microsoft.com</email>
// <date>05/08/09</date>
// <summary>Pushpin user control</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.AspNetUserControls
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Web.UI.WebControls;
    using AirWatch.CloudService.WebRole.Services;
    using dev.virtualearth.net.webservices.v1.common;
    using dev.virtualearth.net.webservices.v1.geocode;
    using Microsoft.AirWatch.Core.Data;
    
    /// <summary>
    /// Pushpin user control for the aspx page
    /// </summary>
    public partial class PushpinControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// type of station ie air or water
        /// </summary>
        private StationType stationType;

        /// <summary>
        /// type of location ie pushpin or station
        /// </summary>
        private LocationType locationType;

        /// <summary>
        /// Enum for location type
        /// </summary>
        private enum LocationType
        {
            /// <summary>
            /// Pushpin type
            /// </summary>
            Pushpin,

            /// <summary>
            /// Station type
            /// </summary>
            Station
        }

        /// <summary>
        /// Enum for station type
        /// </summary>
        private enum StationType
        {
            /// <summary>
            /// Type Air data
            /// </summary>
            Air,

            /// <summary>
            /// Type Water Data
            /// </summary>
            Water
        }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has data.
        /// </summary>
        /// <value><c>true</c> if this instance has data; otherwise, <c>false</c>.</value>
        public bool HasData { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        /// <value>The rating.</value>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets the pushpin id.
        /// </summary>
        /// <value>The pushpin id.</value>
        public int PushpinId { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the station type string.
        /// </summary>
        /// <value>The station type string.</value>
        public string StationTypeString { get; set; }

        /// <summary>
        /// Gets or sets the station id.
        /// </summary>
        /// <value>The station id.</value>
        public int StationId { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the average rating.
        /// </summary>
        /// <value>The average rating.</value>
        public int AverageRating { get; set; }

        /// <summary>
        /// Gets the image SRC.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>String value representing the image to display.</returns>
        public static string GetImage(string value)
        {
           return "/AspNetVisualAssets/HistoricalWaterIcons/WaterIcon" + (int)double.Parse(value, CultureInfo.InvariantCulture) + ".png";
        }

        /// <summary>
        /// Gets the user image.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>String vlaue representing the image.</returns>
        public static string GetUserImage(string value)
        {
            return "/AspNetVisualAssets/HistoricalUserIcons/UserIcon" + (int)double.Parse(value, CultureInfo.InvariantCulture) + ".png";
        }
      
        /// <summary>
        /// Gets date without time portion.
        /// </summary>
        /// <param name="value">The date and time value.</param>
        /// <returns>String vlaue representing the date part of the string.</returns>
        public static string GetDateOnly(string value)
        {
            return value.Split(' ')[0];
        }

        /// <summary>
        /// Inits the control.
        /// </summary>
        /// <param name="id">The id for sync to client.</param>
        /// <param name="latitudeParameter">The latitude param.</param>
        /// <param name="longitudeParameter">The longitude param.</param>
        /// <param name="cultureParameter">The culture param.</param>
        public void InitControl(int id, double latitudeParameter, double longitudeParameter, string cultureParameter)
        {
            this.PushpinId = id;
            this.locationType = LocationType.Pushpin;
            this.Latitude = latitudeParameter;
            this.Longitude = longitudeParameter;
            this.Culture = cultureParameter;
        }

        /// <summary>
        /// Gets the user tool tip.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Tool tip for the user Rating.</returns>
        public string GetUserToolTip(string value)
        {
            int rating = (int)double.Parse(value, CultureInfo.InvariantCulture);
            string word = string.Empty;
            switch (rating)
            {
                case 1:
                    word = "COMMON_VERYBAD";
                    break;
                case 2:
                    word = "COMMON_BAD";
                    break;
                case 3:
                    word = "COMMON_MODERATE";
                    break;
                case 4:
                    word = "COMMON_GOOD";
                    break;
                case 5:
                    word = "COMMON_VERYGOOD";
                    break;
                default:
                    word = "COMMON_NODATA";
                    break;
            }

            return LocalizationHelper.Instance.LocalizeString(word, this.Culture);
        }

        /// <summary>
        /// Gets the rating tool tip.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Tool tip for the official rating.</returns>
        public string GetRatingToolTip(string value)
        {
            int qualityIndex = (int)double.Parse(value, CultureInfo.InvariantCulture);
            string bindingTerm = string.Empty;

            switch (qualityIndex)
            {
                case 1:
                    bindingTerm = "COMMON_GOOD";
                    break;
                case 2:
                    bindingTerm = "COMMON_MODERATE";
                    break;
                case 3:
                    bindingTerm = "COMMON_BAD";
                    break;
                default:
                    bindingTerm = "COMMON_NODATA";
                    break;
            }

            return LocalizationHelper.Instance.LocalizeString(bindingTerm, this.Culture);
        }

        /// <summary>
        /// Inits the control.
        /// </summary>
        /// <param name="id">The id of the call.</param>
        /// <param name="stationId">The station id.</param>
        /// <param name="cultureParameter">The culture parameter.</param>
        public void InitControl(int id, int stationId, string cultureParameter)
        {
            this.PushpinId = id;
            this.locationType = LocationType.Station;
            this.StationId = stationId;
            this.Culture = cultureParameter;

            this.removeButton.Visible = false;
        }

        /// <summary>
        /// Gets the user ip address.
        /// </summary>
        /// <returns>Current User Ip Address.</returns>
        public string GetUserIPAddress()
        {
            string ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ip))
            {
                string[] range = ip.Split(',');
                string trueIP = range[0];
            }
            else
            {
                ip = Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BarsDisplayControl.Culture = this.Culture;
        }

        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // WHEN WE HAVE A GET STATION SERVICE THE IF STATEMENT WILL JUST BE TO DECIDE WHICH SERVICE WE CALL
            if (this.locationType == LocationType.Station)
            {
                this.HandleStation();
            }

            if (this.locationType == LocationType.Pushpin)
            {
                this.HandlePushPin();
            }

            string pre = string.Empty;
            if (this.stationType == StationType.Water)
            {
                pre = "RATELOCATION_WATER_";
            }
            else
            {
                pre = "RATELOCATION_AIR_";
            }

            for (int i = 0; i < 8; i++)
            {
                string tempWord = pre + i;
                ListItem listItem = new ListItem();
                listItem.Text = string.Format(CultureInfo.InvariantCulture, " {0} ", AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString(tempWord, this.Culture));
                listItem.Value = tempWord;
                this.wordCheckBoxList.Items.Add(listItem);
            }
        }

        /// <summary>
        /// Sets the type image.
        /// </summary>
        /// <param name="typeInput">The type input.</param>
        private void SetTypeImage(string typeInput)
        {
            this.ImageType.Visible = true;

            if (this.locationType == LocationType.Station)
            {
                if (typeInput == "Background")
                {
                    this.ImageType.ImageUrl = "~/AspNetVisualAssets/DataInfoIcons/Background21x21.png";
                    this.ImageType.ToolTip = LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_DATATYPE_BACKGROUND", this.Culture);
                }
                else if (typeInput == "Roadside" || typeInput == "Traffic")
                {
                    this.ImageType.ImageUrl = "~/AspNetVisualAssets/DataInfoIcons/RoadSide20x18.png";
                    this.ImageType.ToolTip = LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_DATATYPE_ROADSIDE", this.Culture);
                }
                else if (typeInput == "Industrial")
                {
                    this.ImageType.ImageUrl = "~/AspNetVisualAssets/DataInfoIcons/FactoryIcon.png";
                    this.ImageType.ToolTip = LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_DATATYPE_INDUSTRIAL", this.Culture);
                }
                else
                {
                    this.ImageType.Visible = false;
                }
            }
            else
            {
                this.ImageType.ImageUrl = "~/AspNetVisualAssets/DataInfoIcons/AirModel23x22.png";
                this.ImageType.ToolTip = LocalizationHelper.Instance.LocalizeString("PUSHPINFLYOUT_DATATYPE_AIRMODEL", this.Culture);
            }
        }

        /// <summary>
        /// Handles the station.
        /// </summary>
        private void HandleStation()
        {
            StationService stationServiceClient = new StationService();
            Station station = stationServiceClient.GetStationByCode(this.StationId);

            // Station station = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.StationService.GetStationForStationCode(this.stationId);
            this.Latitude = (double)station.Location.Latitude;
            this.Longitude = (double)station.Location.Longitude;
            
            this.SetTypeImage(station.TypeOfStation);

            switch (station.StationPurpose)
            {
                case "Water":
                    this.stationType = StationType.Water;
                    this.historyButton.Visible = true;

                    SearchService searchServiceClient = new SearchService();
                    string clientIPAddress = searchServiceClient.GetToken(this.GetUserIPAddress());

                    GeocodeResponse response = searchServiceClient.GetReverseGeocode(clientIPAddress, this.Culture.ToUpper(CultureInfo.InvariantCulture), this.Longitude, this.Latitude);

                    if (response != null && response.Results != null)
                    {
                        GeocodeResult[] responses = response.Results;

                        if (responses.Length > 0)
                        {
                            this.Address = responses[0].Address.FormattedAddress;
                        }
                        else
                        {
                            this.Address = string.Empty;
                        }

                        station.FriendlyAddress = this.Address;
                        
                        this.NearestStationLabel.Visible = true;
                        this.StationAddressLabel.Text = station.FriendlyAddress;
                        this.StationAddressLabel.Visible = true;
                    }

                    break;

                case "Air":
                default:
                    this.stationType = StationType.Air;
                    break;
            }

            if (station.Name != null &&
                station.Name.Length > 24)
            {
                this.StationNameLabel.Text = string.Concat(station.Name.Substring(0, 24), "...");
            }
            else
            {
                this.StationNameLabel.Text = station.Name;
            }

            this.StationNameLabel.ToolTip = station.Name;
            this.StationNameLabel.Visible = true;
            this.StationTypeString = station.StationPurpose;            
            this.Rating = station.QualityIndex.GetValueOrDefault();

            this.StationSelectImage();

            if (station.Timestamp.HasValue)
            {
                this.UpdatedDate.ToolTip = this.GetLastUpdatedString(station.Timestamp.GetValueOrDefault());
            }            

            if (station.AverageRating != null)
            {
                this.AverageRating = station.AverageRating.Rating.GetValueOrDefault();

                this.TagCloud.Culture = this.Culture;
                this.TagCloud.AverageRating = station.AverageRating;

                var image = string.Empty;
                var word = string.Empty;
                switch (station.AverageRating.Rating)
                {
                    case 1:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon5.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", this.Culture);
                        break;
                    case 2:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon4.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_BAD", this.Culture);
                        break;
                    case 3:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon3.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", this.Culture);
                        break;
                    case 4:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon2.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", this.Culture);
                        break;
                    case 5:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon1.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_VERYGOOD", this.Culture);
                        break;
                    default:
                        image = "~/AspNetVisualAssets/UserIcons/UserIconNoData.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);
                        break;
                }

                this.currentRating.ImageUrl = image;
                this.currentRatingCount.Text = "(" + station.AverageRating.Count.ToString() + ")";
                this.currentRatingWord.Text = word;
            }
            else
            {
                this.yourRatingArea.Visible = false;
                this.rightNoData.Visible = true;

                this.TagCloud.Visible = false;
                this.NoRatingCloud.Visible = true;
                this.NoRatingCloud.InnerText = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);

                this.currentRating.ImageUrl = "~/AspNetVisualAssets/UserIcons/UserIconNoData.png";
                this.currentRatingCount.Text = "(0)";
                this.currentRatingWord.Text = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);
            }

            if (this.stationType == StationType.Air)
            {
                if (station.StationMeasurement.Count > 0)
                {
                    this.BarsDisplayControl.Visible = true;
                    this.BarsDisplayControl.StationMeasurements = station.StationMeasurement;
                }
            }
            else
            {
                this.SeasonalData.DataSource = HistoricalSeasonConverter.Convert(station.StationMeasurement).OrderByDescending((o) => o.Measurement.Timestamp);
                this.SeasonalData.DataBind();

                this.YearlyData.DataSource = HistoricalYearlyConverter.Convert(station.StationMeasurement).OrderByDescending((o) => o.Measurement.Timestamp);
                this.YearlyData.DataBind();

                this.BarsDisplayControl.Visible = false;
            }

            if (this.Rating != 0)
            {
                if (this.stationType == StationType.Air)
                {
                    this.EEAImage.ImageUrl = "/AspNetVisualAssets/RatingIcons/EEAAir" + this.Rating + ".png";
                }
                else if (this.stationType == StationType.Water)
                {
                    this.EEAImage.ImageUrl = "/AspNetVisualAssets/RatingIcons/EEAWater" + this.Rating + ".png";
                }
            }
            else
            {
                this.EEAImage.ImageUrl = "/AspNetVisualAssets/RatingIcons/EEAAirNoData.png";
            }

            this.HasData = true;
        }

        /// <summary>
        /// Stations the select image.
        /// </summary>
        private void StationSelectImage()
        {
            switch (this.Rating)
            {
                case 1:
                    this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_VERYGOOD", this.Culture);
                    break;
                case 2:
                    if (this.stationType == StationType.Air)
                    {
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", this.Culture);
                    }
                    else
                    {
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", this.Culture);
                    }

                    break;
                case 3:
                    if (this.stationType == StationType.Air)
                    {
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", this.Culture);
                    }
                    else
                    {
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", this.Culture);
                    }

                    break;
                case 4:
                    this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_BAD", this.Culture);
                    break;
                case 5:
                    this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", this.Culture);
                    break;
                default:
                    this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);
                    break;
            }
        }

        /// <summary>
        /// Handles the push pin.
        /// </summary>
        private void HandlePushPin()
        {
            Pushpin pushpin = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.PushpinService.GetPushpin((decimal)this.Longitude, (decimal)this.Latitude);

            pushpin.AverageRating = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.RatingService.GetRating(this.Latitude, this.Longitude, System.Enum.GetName(typeof(StationType), this.stationType), 0.03);

            // organise the user rating word cloud within the pushpin
            PushpinRatingHelper.SetPushpinRatingsWordCloud(pushpin);

            this.StationTypeString = "Air";
            this.SetTypeImage(string.Empty);

            SearchService searchServiceClient = new SearchService();
            string clientIPAddress  = searchServiceClient.GetToken(this.GetUserIPAddress());
            GeocodeResponse response = searchServiceClient.GetReverseGeocode(clientIPAddress, this.Culture.ToUpper(CultureInfo.InvariantCulture), this.Longitude, this.Latitude);

            if (response != null && response.Results != null)
            {
                GeocodeResult[] responses = response.Results;

                if (responses.Length > 0)
                {
                    this.Address = responses[0].Address.FormattedAddress;
                }
                else
                {
                    this.Address = string.Empty;
                }

                if (this.Address.Length > 24)
                {
                    this.StationNameLabel.Text = string.Concat(this.Address.Substring(0, 24), "...");
                }
                else
                {
                    this.StationNameLabel.Text = this.Address;
                }

                this.StationNameLabel.Visible = true;
                this.StationNameLabel.ToolTip = this.Address;
            }

            if (pushpin.AverageRating != null)
            {
                this.TagCloud.Culture = this.Culture;
                this.TagCloud.AverageRating = pushpin.AverageRating;

                var image = string.Empty;
                var word = string.Empty;
                switch (pushpin.AverageRating.Rating)
                {
                    case 1:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon5.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", this.Culture);
                        break;
                    case 2:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon4.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_BAD", this.Culture);
                        break;
                    case 3:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon3.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", this.Culture);
                        break;
                    case 4:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon2.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", this.Culture);
                        break;
                    case 5:
                        image = "~/AspNetVisualAssets/UserIcons/UserIcon1.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_VERYGOOD", this.Culture);
                        break;
                    default:
                        image = "~/AspNetVisualAssets/UserIcons/UserIconNoData.png";
                        word = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);
                        break;
                }

                this.currentRating.ImageUrl = image;
                this.currentRatingCount.Text = "(" + pushpin.AverageRating.Count.ToString() + ")";
                this.currentRatingWord.Text = word;

                this.HasData = true;
            }
            else
            {
                this.yourRatingArea.Visible = false;
                this.rightNoData.Visible = true;

                this.TagCloud.Visible = false;
                this.NoRatingCloud.Visible = true;
                this.NoRatingCloud.InnerText = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);

                this.currentRating.ImageUrl = "~/AspNetVisualAssets/UserIcons/UserIconNoData.png";
                this.currentRatingCount.Text = string.Empty;
                this.currentRatingWord.Text = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);
            }

            if (pushpin.CentreLocation != null)
            {
                switch (pushpin.CentreLocation.QualityIndex.GetValueOrDefault())
                {
                    case 1:
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_VERYGOOD", this.Culture);
                        break;
                    case 2:
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", this.Culture);
                        break;
                    case 3:
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", this.Culture);
                        break;
                    case 4:
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_BAD", this.Culture);
                        break;
                    case 5:
                        this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", this.Culture);
                        break;
                }

                if (pushpin.CentreLocation.Timestamp.HasValue)
                {
                    this.UpdatedDate.ToolTip = this.GetLastUpdatedString(pushpin.CentreLocation.Timestamp.GetValueOrDefault());
                }                

                this.BarsDisplayControl.Readings = pushpin.CentreLocation.CentreLocationMeasurement;
                if (this.stationType == StationType.Air)
                {
                    this.EEAImage.ImageUrl = "/AspNetVisualAssets/RatingIcons/EEAAir" + pushpin.CentreLocation.QualityIndex.Value + ".png";
                }
                else if (this.stationType == StationType.Water)
                {
                    this.EEAImage.ImageUrl = "/AspNetVisualAssets/RatingIcons/EEAWater" + pushpin.CentreLocation.QualityIndex.Value + ".png";
                }

                this.HasData = true;
            }
            else
            {
                this.PushpinLeft.Visible = false;
                this.leftNoData.Visible = true;

                this.EEARating.Text = LocalizationHelper.Instance.LocalizeString("COMMON_NODATA", this.Culture);

                // Temporary solution: ideally we need to hide the divs, as per silverlight
                this.EEAImage.ImageUrl = "/AspNetVisualAssets/RatingIcons/EEAAirNoData.png";
                this.UpdatedDate.ToolTip = LocalizationHelper.Instance.LocalizeString("COMMON_NODATE", this.Culture);
            }
        }

        /// <summary>
        /// Gets the last updated string.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>The localised formated date string</returns>
        private string GetLastUpdatedString(DateTime dateTime)
        {
            string localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_NODATE", this.Culture);

            TimeSpan timeSpan = DateTime.Now.Subtract((DateTime)dateTime);

            if (timeSpan.Days > 365)
            {
                int years = timeSpan.Days / 365;

                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_YEARS", this.Culture);
                localizedString = string.Format(CultureInfo.InvariantCulture, localizedString, years + 1);
            }
            else if (timeSpan.Days > 273)
            {
                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_YEAR", this.Culture);
            }
            else if (timeSpan.Days > 30)
            {
                int months = timeSpan.Days / 30;

                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_MONTHS", this.Culture);
                localizedString = string.Format(CultureInfo.InvariantCulture, localizedString, months + 1);
            }
            else if (timeSpan.Days > 15)
            {
                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_MONTH", this.Culture);
            }
            else if (timeSpan.Days >= 1)
            {
                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_DAYS", this.Culture);
                localizedString = string.Format(CultureInfo.InvariantCulture, localizedString, timeSpan.Days + 1);
            }
            else if (timeSpan.Hours > 12)
            {                
                    localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_DAY", this.Culture);                
            }
            else if (timeSpan.Hours >= 1)
            {
                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_HOURS", this.Culture);
                localizedString = string.Format(CultureInfo.InvariantCulture, localizedString, timeSpan.Hours + 1);
            }
            else
            {
                localizedString = LocalizationHelper.Instance.LocalizeString("COMMON_HOUR", this.Culture);
            }

            return localizedString;
        }
    }
}
