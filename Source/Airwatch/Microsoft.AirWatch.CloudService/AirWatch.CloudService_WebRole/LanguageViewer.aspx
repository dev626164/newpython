﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LanguageViewer.aspx.cs" Inherits="AirWatch.CloudService.WebRole.LanguageViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Localisation load status for : <% Response.Write(Environment.MachineName); %> </h1>  
        <h3>Total languages to load : <% Response.Write(AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LanguageDescriptions.Length); %></h3>       
        <asp:Repeater runat="server" ID="LanguageRepeater">
            <HeaderTemplate>
                <h3>Languages loaded : <% Response.Write(AirWatch.CloudService.WebRole.LocalizationHelper.Instance.AllLanguagesDictionary.Count); %></h3>                
            </HeaderTemplate>
            <ItemTemplate> 
                <div>
                    <span style="width:30;border-color:black;border-width:thin">      
                        <%# (Eval("Value") as System.Collections.Generic.Dictionary<string,string>)["CULTURE_DISPLAYNAME"] %>
                    </span>
                    <span style="width:30;border-color:black;border-width:thin">
                        (<%# Eval("Key") %>)
                    </span>
                </div>                
            </ItemTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
