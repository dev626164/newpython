﻿//-----------------------------------------------------------------------
// <copyright file="UserFeedbackWaterLightMapOverlayHandler.ashx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>13 October 2009</date>
// <summary>HTTP handler for the UserFeedbackWaterLightMap overlay getting from blob storage</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net;
    using System.Web;
    using Microsoft.WindowsAzure;

    /// <summary>
    /// HTTP handler for the UserFeedbackWaterLightMap overlay getting from blob storage
    /// </summary>
    public class UserFeedbackWaterLightMapOverlayHandler : IHttpHandler
    {
        /// <summary>
        /// Collection of quadkeys which are blank
        /// </summary>
        private static Collection<string> blankQuadkeys;

        /// <summary>
        /// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler"/> instance.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Web.IHttpHandler"/> instance is reusable; otherwise, false.
        /// </returns>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {
            // set the cache expiry for midnight the next day (regenerated once)
            DateTime now = DateTime.Now;
            DateTime expiryTime = new DateTime(now.Year, now.Month, now.Day).AddDays(1);

            // if it has passed its expiry reset
            if (expiryTime < DateTime.Now && blankQuadkeys != null)
            {
                blankQuadkeys.Clear();
            }

            context.Response.ContentType = "application/octet-stream";
            context.Response.Cache.SetExpires(expiryTime);

            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");

            string blobEndpoint = account.BlobEndpoint.ToString();
            string quadkey = context.Request.Url.LocalPath.Replace(".userfeedbackwaterlightmap", string.Empty).Replace("/", string.Empty);

            string file;

            // when running on dev storage, add the account name into the url
            if (blobEndpoint.Contains("127.0.0.1"))
            {
                file = blobEndpoint + account.Credentials.AccountName + "/userfeedbackwaterlightmap/" + quadkey + ".png";
            }
            else
            {
                file = blobEndpoint + "/userfeedbackwaterlightmap/" + quadkey + ".png";
            }

            bool imageFound = false;
            byte[] imageBytes = null;

            if (quadkey != null)
            {
                if (!QuadKeyHelper.ContainsKey(quadkey, blankQuadkeys))
                {
                    try
                    {
                        Uri imageUri = new Uri(file);
                        WebRequest imageRequest = WebRequest.Create(imageUri);
                        WebResponse imageResponse = imageRequest.GetResponse();

                        Image image = Image.FromStream(imageResponse.GetResponseStream());

                        using (MemoryStream ms = new MemoryStream())
                        {
                            image.Save(ms, ImageFormat.Png);

                            imageBytes = ms.ToArray();
                        }

                        imageFound = true;
                    }
                    catch (WebException ex)
                    {
                        if (blankQuadkeys == null)
                        {
                            blankQuadkeys = new Collection<string>();
                        }

                        if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                        {
                            blankQuadkeys.Add(quadkey);
                        }

                        imageFound = false;
                    }
                    catch (ArgumentException)
                    {
                        if (blankQuadkeys == null)
                        {
                            blankQuadkeys = new Collection<string>();
                        }

                        blankQuadkeys.Add(quadkey);

                        imageFound = false;
                    }
                }
                else
                {
                    imageFound = false;
                }
            }

            if (imageFound && imageBytes != null)
            {
                context.Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
            }
            else
            {
                context.Response.WriteFile("~/Images/blank.png");
            }
        }
    }
}
