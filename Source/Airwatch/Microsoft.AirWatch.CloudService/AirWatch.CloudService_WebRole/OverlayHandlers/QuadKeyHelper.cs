﻿//-----------------------------------------------------------------------
// <copyright file="QuadKeyHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>13 October 2009</date>
// <summary>Helper for http handlers relating to quadkeys</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Helper for http handlers relating to quadkeys
    /// </summary>
    public static class QuadKeyHelper
    {
        /// <summary>
        /// Determines whether the blankQuadkeys collection contains the quadkey specified or a parent quadkey.
        /// </summary>
        /// <param name="quadKey">The quadkey.</param>
        /// <param name="blankQuadKeys">The collection of existing blank quad keys.</param>
        /// <returns>
        /// <c>true</c> if the blankQuadkeys collection contains the quadkey specified or a parent quadkey, otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsKeyOrParentKey(string quadKey, Collection<string> blankQuadKeys)
        {
            bool quadkeyFound = false;

            string currentKeyChild = quadKey;

            if (blankQuadKeys == null)
            {
                blankQuadKeys = new Collection<string>();
            }

            while (currentKeyChild.Length > 0 && !quadkeyFound)
            {
                if (blankQuadKeys.Contains(currentKeyChild))
                {
                    quadkeyFound = true;
                }
                else
                {
                    currentKeyChild = currentKeyChild.Substring(0, currentKeyChild.Length - 1);
                }
            }

            return quadkeyFound;
        }

        /// <summary>
        /// Determines whether the blankQuadkeys collection contains the quadkey specified.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="blankQuadKeys">The blank quad keys.</param>
        /// <returns>
        /// <c>true</c> if the specified quad key contains key; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsKey(string quadKey, Collection<string> blankQuadKeys)
        {
            bool quadkeyFound = false;

            if (blankQuadKeys == null)
            {
                blankQuadKeys = new Collection<string>();
            }

            if (blankQuadKeys.Contains(quadKey))
            {
                quadkeyFound = true;
            }

            return quadkeyFound;
        }
    }
}
