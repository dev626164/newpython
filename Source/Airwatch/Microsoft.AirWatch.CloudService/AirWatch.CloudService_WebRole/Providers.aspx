﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Providers.aspx.cs" Inherits="AirWatch.CloudService.WebRole.Providers" %>

<%@ Register src="AspNetUserControls/RSSControl.ascx" tagname="RSSControl" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_PROVIDERS_HEADER", Request.QueryString.Get("culture"))%></h1>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_PROVIDERS_TEXT", Request.QueryString.Get("culture"))%>
        <br />
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_PROVIDERS_BECOMEPROVIDER", Request.QueryString.Get("culture"))%>&nbsp;<a href="mailto:eoe.data@eea.europa.eu">eoe.data@eea.europa.eu</a>
        <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_PROVIDERS_AIR", Request.QueryString.Get("culture"))%></h2>
        <div>            
            <uc1:RSSControl ID="RSSControl1" runat="server" Uri="http://dataconnector.eea.europa.eu/eoe_airquality/xml/airquality_providers.xml" />            
        </div>
        <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_PROVIDERS_WATER", Request.QueryString.Get("culture"))%></h2>
        <div>                   
            <uc1:RSSControl ID="RSSControl2" runat="server" Uri="http://dataconnector.eea.europa.eu/eoe_bathingwater/xml/bathingwater_providers.xml" />                    
        </div>
    </div>
    </form>
</body>
</html>
