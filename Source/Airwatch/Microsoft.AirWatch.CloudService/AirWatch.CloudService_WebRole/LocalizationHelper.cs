﻿//-----------------------------------------------------------------------
// <copyright file="LocalizationHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Mark Newman</author>
// <email>t-mnewma@microsoft.com</email>
// <date>07-08-2009</date>
// <summary>Localization Helper singleton object container.</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Core.ApplicationServices;

    /// <summary>
    /// Localisation Helper singleton object container.
    /// </summary>
    public class LocalizationHelper
    {
        /// <summary>
        /// Backing field for <see cref="Instance" /> property.
        /// </summary>
        private static LocalizationHelper instance;

        /// <summary>
        /// All languages that have been used since last language update
        /// </summary>
        private Dictionary<string, Dictionary<string, string>> allLanguagesDictionary = new Dictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// dateTimeDictionary so we known when to refresh the language dictionary collection
        /// </summary>
        private Dictionary<string, DateTime> dateTimeDictionary = new Dictionary<string, DateTime>();

        /// <summary>
        /// Backing property for LanguageDescriptions
        /// </summary>
        private LanguageDescription[] languageDescriptions = null;

        /// <summary>
        /// Prevents a default instance of the <see cref="LocalizationHelper"/> class from being created.
        /// </summary>
        private LocalizationHelper()
        {
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static LocalizationHelper Instance
        {
            get
            {
                if (LocalizationHelper.instance == null)
                {
                    LocalizationHelper.instance = new LocalizationHelper();
                }

                return LocalizationHelper.instance;
            }
        }      

        /// <summary>
        /// Gets the language descriptions.
        /// </summary>
        /// <value>The language descriptions.</value>
        public LanguageDescription[] LanguageDescriptions
        {
            get
            {
                if (this.languageDescriptions == null)
                {
                    this.languageDescriptions = ServiceProvider.LanguageService.GetLanguages();
                }

                return this.languageDescriptions;
            }
        }

        /// <summary>
        /// Gets the AllLanguages Dictionary.
        /// </summary>
        /// <value>The AllLanguages Dictionary.</value>
        public Dictionary<string, Dictionary<string, string>> AllLanguagesDictionary
        {
            get
            {
                return this.allLanguagesDictionary;
            }
        }

        /// <summary>
        /// Localises the string.
        /// </summary>
        /// <param name="dictionaryKey">The dictionary key.</param>
        /// <param name="culture">User culture</param>
        /// <returns>Localised string</returns>
        public string LocalizeString(string dictionaryKey, string culture)
        {
            string stringToReturn = String.Empty;

            try
            {
                if (!string.IsNullOrEmpty(culture))
                {
                    if (this.allLanguagesDictionary.ContainsKey(culture))
                    {
                        stringToReturn = this.allLanguagesDictionary[culture][dictionaryKey];
                    }
                    else
                    {
                        this.allLanguagesDictionary[culture] = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.LanguageService.GetLanguage(culture);
                        this.dateTimeDictionary[culture] = ServiceProvider.LanguageService.GetTimestamp(culture);

                        try
                        {
                            stringToReturn = this.allLanguagesDictionary[culture][dictionaryKey];
                        }
                        catch (ArgumentNullException)
                        {
                            stringToReturn = dictionaryKey;
                        }
                        catch (KeyNotFoundException)
                        {
                            stringToReturn = dictionaryKey;
                        }
                    }
                }
                else
                {
                    if (this.allLanguagesDictionary.ContainsKey("en-UK"))
                    {
                        stringToReturn = this.LocalizeString(dictionaryKey, "en-UK");
                    }
                    else
                    {
                        stringToReturn = dictionaryKey;
                    }
                }
            }
            catch (ArgumentNullException)
            {
                if (this.allLanguagesDictionary.ContainsKey("en-UK"))
                {
                    stringToReturn = this.LocalizeString(dictionaryKey, "en-UK");
                }
                else
                {
                    stringToReturn = dictionaryKey;
                }
            }
            catch (KeyNotFoundException)
            {
                if (this.allLanguagesDictionary.ContainsKey("en-UK"))
                {
                    stringToReturn = this.LocalizeString(dictionaryKey, "en-UK");
                }
                else
                {
                    stringToReturn = dictionaryKey;
                }
            }

            return stringToReturn;
        }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>The localised dictionary.</returns>
        public Dictionary<string, string> GetLanguage(string culture)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (this.allLanguagesDictionary.ContainsKey(culture) && ServiceProvider.LanguageService.GetTimestamp(culture) == this.dateTimeDictionary[culture])
            {
                dictionary = this.allLanguagesDictionary[culture];
            }
            else
            {
                dictionary = this.allLanguagesDictionary[culture] = ServiceProvider.LanguageService.GetLanguage(culture);
                this.dateTimeDictionary[culture] = ServiceProvider.LanguageService.GetTimestamp(culture);
            }

            return dictionary; 
        }

        /// <summary>
        /// Preloads the languages.
        /// </summary>        
        public void PreloadLanguages()
        {
            foreach (LanguageDescription culture in ServiceProvider.LanguageService.GetLanguages())
            {   
                this.PreloadLanguage(culture.LanguageCode);
            }
        }

        /// <summary>
        /// Preloads the language.
        /// </summary>
        /// <param name="culture">The culture.</param>
        private void PreloadLanguage(string culture)
        {      
            if (!string.IsNullOrEmpty(culture))
            {
                this.GetLanguage(culture);
            }
        }
    }
}