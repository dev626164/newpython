﻿// <copyright file="Default.aspx.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-04-2009</date>
// <summary>Default partial class for the ASP.NET hosting for the web interface</summary>
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.AirWatch.AzureTables;

    /// <summary>
    /// Default partial class for the ASP.NET hosting for the web interface
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// Gets or sets the user culture.
        /// </summary>
        /// <value>The user culture.</value>
        public string UserCulture { get; set; }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UserCulture = this.LangCulture.Value = "en-GB";

            StringBuilder stringBuilder = new StringBuilder();

            if (Request.UserLanguages != null)
            {
                foreach (string s in Request.UserLanguages)
                {
                    stringBuilder.Append(s + ",");
                }

                this.LangCulture.Value = stringBuilder.ToString();
            }

            LanguageDescription[] languageDescriptionArray = LocalizationHelper.Instance.LanguageDescriptions;

            if (Request.UserLanguages != null)
            {
                foreach (string s in Request.UserLanguages)
                {
                    string[] nonPercentageLanguages = s.Split(new char[] { ';' });

                    if (nonPercentageLanguages[0] != null)
                    {
                        LanguageDescription languageDescription = languageDescriptionArray.SingleOrDefault(p => p.LanguageCode.ToUpperInvariant().Substring(0, 2) == nonPercentageLanguages[0].ToUpperInvariant().Substring(0, 2));

                        if (languageDescription != null)
                        {
                            this.UserCulture = languageDescription.LanguageCode;
                            break;
                        }
                    }
                }
            }
        }
    }
}
