﻿//-----------------------------------------------------------------------
// <copyright file="LanguagesUserInterfaceConsole.aspx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>18/07/09</date>
// <summary>Languages for User Interface - Console</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Data.Services.Client;
    using System.Globalization;
    using System.Linq;
    using System.Web.UI.WebControls;
    using System.Xml;
    using System.Xml.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.WindowsAzure;

    /// <summary>
    /// Languages for User Interface console
    /// </summary>
    public partial class LanguagesUserInterfaceConsole : System.Web.UI.Page
    {
        private LanguageEntityTable languageEntityTable;
        
        public LanguagesUserInterfaceConsole()
        {
            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.languageEntityTable = new LanguageEntityTable(account.TableEndpoint.ToString(), account.Credentials);
        }

        /// <summary>
        /// Builds the key.
        /// </summary>
        /// <param name="dataItem">The data item.</param>
        /// <returns>The built key</returns>
        public static string BuildKey(object dataItem)
        {
            LanguageEntity language = dataItem as LanguageEntity;
            return string.Format(CultureInfo.InvariantCulture, "{0}|{1}", language.PartitionKey, language.RowKey);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Files the upload button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void FileUploadButtonClick(object sender, EventArgs e)
        {
            if (this.fileUpload.HasFile)
            {
                string fileExtension = System.IO.Path.GetExtension(this.fileUpload.FileName).ToUpperInvariant();

                if (fileExtension.Equals(".XML"))
                {
                    using (XmlReader reader = XmlReader.Create(this.fileUpload.PostedFile.InputStream))
                    {
                        XElement element = XElement.Load(reader);

                        var allContents = from el in element.Elements().Elements()
                                          select new
                                          {
                                              Culture = el.Name.LocalName,
                                              StringId = el.Parent.Attribute("id").Value,
                                              Value = el.Value
                                          };

                        var svc = this.languageEntityTable;

                        foreach (LanguageEntity existingLang in svc.Languages)
                        {
                            // svc.AttachTo("Languages", existingLang, "*");
                            svc.DeleteObject(existingLang);
                        }

                        try
                        {
                            svc.SaveChanges();
                        }
                        catch (Exception)
                        {
                            throw;
                        }

                        foreach (var languageString in allContents)
                        {
                            svc.AddObject("Languages", new LanguageEntity(languageString.Culture, languageString.StringId, languageString.Value));
                        }

                        try
                        {
                            svc.SaveChanges();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    // file not uploaded
                }
            }
        }

        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            DataServiceQuery<LanguageEntity> query = null;

            string filt = string.Empty;

            if (Session["filter"] != null)
            {
                filt = Session["filter"].ToString();
            }

            if (!String.IsNullOrEmpty(filt))
            {
                query = (DataServiceQuery<LanguageEntity>)
                this.languageEntityTable.Languages.Where(predicate => predicate.PartitionKey.Equals(filt, StringComparison.OrdinalIgnoreCase)).Take(20);
            }
            else
            {
                query = (DataServiceQuery<LanguageEntity>)
                this.languageEntityTable.Languages.Take(20);
            }

            var continuation = Request["ct"];
            if (continuation != null)
            {
                string[] tokens = continuation.Split('/');
                var partitionToken = tokens[0];
                var rowToken = tokens[1];

                query = query.AddQueryOption(
                    "NextPartitionKey",
                    partitionToken).AddQueryOption(
                    "NextRowKey",
                    rowToken);
                this.nextLink.Visible = true;
            }
            else
            {
                this.previousLink.Visible = false;
            }

            var res = query.Execute();
            var qor = (QueryOperationResponse)res;
            string nextPartition = null;
            string nextRow = null;
            qor.Headers.TryGetValue(
                "x-ms-continuation-NextPartitionKey",
                out nextPartition);
            qor.Headers.TryGetValue(
                "x-ms-continuation-NextRowKey",
                out nextRow);

            if (nextPartition != null && nextRow != null)
            {
                this.nextLink.NavigateUrl = string.Format(
                    CultureInfo.InvariantCulture,
                    "?ct={0}/{1}",
                    nextPartition,
                    nextRow);
            }
            else
            {
                this.nextLink.Visible = false;
            }

            this.LanguagesGrid.DataSource = res;
            this.LanguagesGrid.DataBind();
        }

        /// <summary>
        /// Handles the Click event of the FilterButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void FilterButton_Click(object sender, EventArgs e)
        {
            Session.Add("filter", this.SearchBox.Text);
            Response.Redirect(this.Request.Url.ToString());
        }

        /// <summary>
        /// Handles the Click event of the StopFilterButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void StopFilterButton_Click(object sender, EventArgs e)
        {
            Session.Remove("filter");
            Response.Redirect(this.Request.Url.ToString());
        }

        /// <summary>
        /// Handles the Click event of the addButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AddButton_Click(object sender, EventArgs e)
        {
            var svc = this.languageEntityTable;

            svc.AddObject("Languages", new LanguageEntity(this.CultureTextBox.Text, this.StringIdTextBox.Text, this.ValueTextBox.Text));

            svc.SaveChanges();
        }

        /// <summary>
        /// Handles the Click event of the DeleteButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;
            string[] p = button.CommandArgument.Split(new char[] { '|' });
            string partitionKey = p[0];
            string rowKey = p[1];

            LanguageEntity languageToDelete = new LanguageEntity()
            {
                PartitionKey = partitionKey,
                RowKey = rowKey
            };

            LanguageEntityTable languageTable = this.languageEntityTable;

            languageTable.AttachTo("Languages", languageToDelete, "*");
            languageTable.DeleteObject(languageToDelete);

            try
            {
                languageTable.SaveChanges();
            }
            catch (DataServiceRequestException)
            {
                //// handle missing language
            }
        }
    }
}
