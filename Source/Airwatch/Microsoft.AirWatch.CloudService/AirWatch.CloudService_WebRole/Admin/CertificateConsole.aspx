﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin/Site.master"
    CodeBehind="CertificateConsole.aspx.cs" Inherits="AirWatch.CloudService.WebRole.CertificateConsole" %>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

    <asp:FileUpload ID="fileUpload" runat="server" />
        <asp:Button ID="fileUploadButton" runat="server" OnClick="FileUploadButtonClick"
        Text="Add certificate" />
    <asp:Label ID="errorBox" runat="server" />
</asp:Content>
