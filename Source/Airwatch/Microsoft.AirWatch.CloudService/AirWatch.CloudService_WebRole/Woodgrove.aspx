﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Woodgrove.aspx.cs" Inherits="AirWatch.CloudService.WebRole.Woodgrove" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <img src="AspNetVisualAssets/Woodgrove.png" />                  
            <div class="hslice" id="SwineFlu" style="position:absolute;top:179px;left:480px;width:320px;height:240px">                
               <div style="display: none" class="entry-title">Eye On Earth</div>
               <a style="display: none" href="http://eyeonearthtest.cloudapp.net/default.aspx?gadget=true&location=43.5684,6.1811&header=Air%20Quality" rel="entry-content"></a>
               <iframe frameborder="0" id="Iframe1" width="320" height="240" src="http://eyeonearthtest.cloudapp.net/default.aspx?gadget=true&location=43.5684,6.1811&header=Air%20Quality"></iframe>                                    
               <span class="ttl" style="display: none">15</span>
               <div class="entry-content" runat="server" id="updateTrigger"></div>
            </div>
        </div>       
    </form>
</body>
</html>
