﻿//-----------------------------------------------------------------------
// <copyright file="DataEntryService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>13 July 2009</date>
// <summary>Web service for data entry of Map Model Data</summary>
//
// ADD TO svc to use factory
// Factory="AirWatch.CloudService.WebRole.Services.DataEntryHostFactory"
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel.Activation;
    using System.Threading;
    using System.Xml.Linq;
    using Microsoft.AirWatch.Common;
    using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// Web service for data entry of Map Model Data
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DataEntryService : IDataEntryService
    {
        private static CloudStorageAccount account;
        private static CloudBlobClient blobClient;
        private static CloudQueueClient queueClient;

        private static void SetUpStorage()
        {
            DataEntryService.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            DataEntryService.blobClient = DataEntryService.account.CreateCloudBlobClient();
            DataEntryService.queueClient = DataEntryService.account.CreateCloudQueueClient();
        }

        /// <summary>
        /// Enter Data will take the data write it to a blob in mapmodeldatablob
        /// then put a message on the mapmodeldataqueue with the name of the blob to get
        /// </summary>
        /// <param name="data">The Data to enter</param>
        public void EnterMapModelData(string data)
        {
            try
            {
                ApplicationEnvironment.LogInformation("EnterMapModelData Called");

                DataEntryService.SetUpStorage();
                var dataBlobContainer = DataEntryService.blobClient.GetContainerReference("mapmodeldatablob");
                dataBlobContainer.CreateIfNotExist();

                string blobName = Guid.NewGuid().ToString();
                var dataBlob = dataBlobContainer.GetBlobReference(blobName);

                byte[] databytes = System.Text.Encoding.Unicode.GetBytes(data);
                dataBlob.UploadByteArray(databytes);

                ApplicationEnvironment.LogInformation("Map Model Data entered into Blob container: " + blobName + ", data written");

                // Put a message onto the queue to let the worker role know that there is data to be processed
                var messageQueue = DataEntryService.queueClient.GetQueueReference("mapmodeldataqueue");
                messageQueue.AddMessage(new CloudQueueMessage(blobName));
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogError("Exception Thrown on EnterMapModelData");
                ApplicationEnvironment.LogError("Message: " + ex.Message);
                ApplicationEnvironment.LogError("Source: " + ex.Source);
                ApplicationEnvironment.LogError("StackTrace: " + ex.StackTrace);

                if (data == null)
                {
                    throw new InvalidOperationException("The data entered was null", ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Enter Data will take the data write it to blobs in airstationdatablob
        /// then put a message on the airstationdataqueue with the name of the blob to get
        /// </summary>
        /// <param name="data">The Data to enter</param>
        public void EnterAirStationData(XElement data)
        {
            try
            {
                ApplicationEnvironment.LogInformation("EnterAirStationData Called");

                if (data != null)
                {
                    Thread processStationData = new Thread(this.EnterStationDataAirThreadEntry);
                    processStationData.Start((object)data);
                }
                else
                {
                    throw new InvalidOperationException("The data entered was null");
                }
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogError("Exception Thrown on EnterAirStationData");
                ApplicationEnvironment.LogError("Message: " + ex.Message);
                ApplicationEnvironment.LogError("Source: " + ex.Source);
                ApplicationEnvironment.LogError("StackTrace: " + ex.StackTrace);

                if (data == null)
                {
                    throw new InvalidOperationException("The data entered was null", ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Enter Data will take the data write it to blobs in waterstationdatablob
        /// then put a message on the waterstationdataqueue with the name of the blob to get
        /// </summary>
        /// <param name="data">The Data to enter</param>
        public void EnterWaterStationData(XElement data)
        {
            try
            {
                ApplicationEnvironment.LogInformation("EnterWaterStationData Called");

                if (data != null)
                {
                    Thread processStationData = new Thread(this.EnterStationDataWaterThreadEntry);
                    processStationData.Start((object)data);
                }
                else
                {
                    throw new InvalidOperationException("The data entered was null");
                }
            }
            catch (Exception ex)
            {
                ApplicationEnvironment.LogError("Exception Thrown on EnterWaterStationData");
                ApplicationEnvironment.LogError("Message: " + ex.Message);
                ApplicationEnvironment.LogError("Source: " + ex.Source);
                ApplicationEnvironment.LogError("StackTrace: " + ex.StackTrace);

                if (data == null)
                {
                    throw new InvalidOperationException("The data entered was null", ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Splits the stations.
        /// </summary>
        /// <param name="xmlDocument">The root XML document.</param>
        /// <returns>A list of XmlElements representing a split of stations</returns>
        private static List<XElement> SplitStations(XElement xmlDocument)
        {
            List<XElement> splitList = new List<XElement>();

            if (xmlDocument != null)
            {
                var stations = xmlDocument.Descendants("station");

                int i = 0;
                XElement split = null;

                // go through each station for the list and add it to the split items list
                foreach (var station in stations)
                {
                    split = new XElement(station);
                    splitList.Add(split);
                }

                // if there were any stations left and not put into the list, put them into the list now
                if (i > 0 && split != null)
                {
                    splitList.Add(split);
                }
            }

            return splitList;
        }

        /// <summary>
        /// Combines the stations with the same code.
        /// </summary>
        /// <param name="list">The list of stations.</param>
        /// <returns>The combined list of stations</returns>
        private static List<XElement> CombineSameStations(List<XElement> list)
        {
            List<XElement> combinedList = new List<XElement>();

            var sortedStations = (from station in list
                                  orderby station.Element("code").Value
                                  select station).ToList();

            string currentCode = null;
            XElement stations = new XElement("stations");

            foreach (var station in sortedStations)
            {
                string code = station.Element("code") != null ? station.Element("code").Value : null;

                // if it's a new code, add the previous one to the list, and move onto the new code
                if (code != currentCode)
                {
                    if (currentCode != null)
                    {
                        combinedList.Add(stations);
                    }

                    stations = new XElement("stations");

                    currentCode = code;
                }

                // add the new station to the stations
                stations.Add(station);
            }

            // add the last one to the end
            if (currentCode != null)
            {
                combinedList.Add(stations);
            }

            return combinedList;
        }

        /// <summary>
        /// Private generic method to allow the easy entry of data of any type of station.
        /// </summary>
        /// <param name="data">The data to enter.</param>
        /// <param name="type">The type of stations.</param>
        private static void EnterStationData(XElement data, string type)
        {
            // Split the data up so that it can be processed alter split size for performance optimization
            List<XElement> splitDataList = SplitStations(data);

            // combine same stations so they only get input once per station (no race condition)
            splitDataList = CombineSameStations(splitDataList);

            // get the blob settings
            DataEntryService.SetUpStorage();
            var dataBlobContainer = DataEntryService.blobClient.GetContainerReference(type.ToLowerInvariant() + "stationdatablob");
            dataBlobContainer.CreateIfNotExist();

            // get the queue settings
            var queue = DataEntryService.queueClient.GetQueueReference(type.ToLowerInvariant() + "stationdataqueue");
            queue.CreateIfNotExist();

            //// XXX: Debugging, store historically
            DateTime now = DateTime.Now;
            string storeBlobName = now.Year + "_" + now.Month + "_" + now.Day + "_" + now.Hour + "_" + now.Minute + "_" + now.Second;
            var blob = dataBlobContainer.GetBlobReference(storeBlobName);
            blob.UploadByteArray(System.Text.Encoding.Unicode.GetBytes(data.ToString()));

            ApplicationEnvironment.LogInformation(type + " Station Data entered for debugging, data written");
            //// XXX: Debugging end

 
            foreach (XElement splitData in splitDataList)
            {
                // Add the data to the blob
                string blobName = Guid.NewGuid().ToString();

                var splitBlob = dataBlobContainer.GetBlobReference(blobName);
                splitBlob.UploadByteArray(System.Text.Encoding.Unicode.GetBytes(splitData.ToString()));

                ApplicationEnvironment.LogInformation(type + " Station Data entered into Blob container: " + blobName + ", data written");

                // Put a message onto the queue to let the worker role know that there is data to be processed
                queue.AddMessage(new CloudQueueMessage(blobName));
            }
        }

        /// <summary>
        /// Method used by the thread, calling the original EnterStationData
        /// </summary>
        /// <param name="data">Passed on as a object, but casts as XElement for the threading</param>
        private void EnterStationDataAirThreadEntry(object data)
        {
            EnterStationData((XElement)data, "air");
        }

        /// <summary>
        /// Method used by the thread, calling the original EnterStationData
        /// </summary>
        /// <param name="data">Passed on as a object, but casts as XElement for the threading</param>
        private void EnterStationDataWaterThreadEntry(object data)
        {
            EnterStationData((XElement)data, "water");
        }
    }
}
