﻿//-----------------------------------------------------------------------
// <copyright file="RatingService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>19/08/09</date>
// <summary>User rating service</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Security.Cryptography;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Channels;
    using System.Text;
    using Microsoft.AirWatch.Core.Data;
    using System.Diagnostics;

    /// <summary>
    /// User rating service
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RatingService : IRatingService
    {
        /// <summary>
        /// Radius the user is restricted from rating between
        /// </summary>
        private const double RatingRadius = 1;

        /// <summary>
        /// Number of ratings a user can do within the thresholds
        /// </summary>
        private const int NumberOfRatingsInThresholds = 1;

        /// <summary>
        /// Threshold of the date in which the user is restricted from rating between
        /// </summary>
        private static TimeSpan dateThreshold = new TimeSpan(0, 0, 0, 1);

        #region IRatingService Members

        /// <summary>
        /// Sets the rating.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="rating">The rating.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userAddress">The user address.</param>
        /// <param name="wordQualifiers">The word qualifiers.</param>
        public void SetRating(double latitude, double longitude, int rating, string ratingTarget, string userAddress, bool[] wordQualifiers)
        {
            try
            {
                // get the IP address from client and take a hash of it
                RemoteEndpointMessageProperty clientEndpoint = OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                string identifierHash = SHA256Hash(clientEndpoint.Address);

                // check it hasn't been rated within dateThreshold and RatingRadius degrees
                bool canRate = ValidateUser(latitude, longitude, ratingTarget, identifierHash);

                if (canRate)
                {
                    System.Data.Objects.DataClasses.EntityCollection<Microsoft.AirWatch.Core.Data.WordQualifier> wordQualifierCollection = new System.Data.Objects.DataClasses.EntityCollection<Microsoft.AirWatch.Core.Data.WordQualifier>();

                    for (int i = 0; i < wordQualifiers.Length; i++)
                    {
                        if (wordQualifiers[i])
                        {
                            wordQualifierCollection.Add(new Microsoft.AirWatch.Core.Data.WordQualifier() { WordId = i });
                        }
                    }

                    Microsoft.AirWatch.Core.Data.UserRating userRating = new Microsoft.AirWatch.Core.Data.UserRating()
                    {
                        Latitude = (decimal)latitude,
                        Longitude = (decimal)longitude,
                        Rating = rating,
                        RatingTarget = ratingTarget,
                        IPAddress = identifierHash,
                        WordQualifier = wordQualifierCollection,
                        RatingTime = DateTime.UtcNow
                    };

                    Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.RatingService.RateLocation(userRating);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling SetRating in RatingService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
            }
        }

        #endregion

        /// <summary>
        /// Get back an SHA512 hash of the string value given
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Base64 SHA512 hash of the value</returns>
        private static string SHA256Hash(string value)
        {
            SHA256 hashProvider = new SHA256CryptoServiceProvider();
            byte[] hashVal = hashProvider.ComputeHash(Encoding.Unicode.GetBytes(value));

            return Convert.ToBase64String(hashVal);
        }

        /// <summary>
        /// Validates the user with the thresholds.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>true if they are allowed to rate, false if they are not</returns>
        private static bool ValidateUser(double latitude, double longitude, string ratingTarget, string userIdentifier)
        {
            DateTime now = DateTime.UtcNow;
            DateTime thresholdAgo = now.Subtract(dateThreshold);

            Collection<UserRating> ratings = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.RatingService.GetAllRatings(latitude, longitude, ratingTarget, RatingRadius, thresholdAgo, now);

            int ratingsByUserInArea = (from r in ratings
                                       where r.IPAddress == userIdentifier
                                       select r).Count();

            return ratingsByUserInArea < NumberOfRatingsInThresholds;
        }
    }
}
