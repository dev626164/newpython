﻿//-----------------------------------------------------------------------
// <copyright file="ILanguageService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/07/09</date>
// <summary>ILanguageService interface</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using Microsoft.AirWatch.AzureTables;

    /// <summary>
    /// Lnaguage service interface
    /// </summary>
    [ServiceContract]
    public interface ILanguageService
    {
        /// <summary>
        /// Get all languages from the database
        /// </summary>
        /// <returns>Array of Languages</returns>
        [OperationContract]
        LanguageDescription[] GetLanguages();

        /// <summary>
        /// Gets the specified language.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>Language dictionary</returns>
        [OperationContract]
        Dictionary<string, string> GetLanguage(string culture);

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>The time stamp of the last update for this culture.</returns>
        [OperationContract]
        DateTime GetTimestamp(string culture);
    }
}
