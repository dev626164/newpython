﻿//-----------------------------------------------------------------------
// <copyright file="ISearchService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>22/06/09</date>
// <summary>Search service interface</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System.ServiceModel;

    /// <summary>
    /// The interface for the bing map services search
    /// </summary>
    [ServiceContract]
    public interface ISearchService
    {
        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <param name="clientIPAddress">The client IP address.</param>
        /// <returns>The Bing Map Services token</returns>
        [OperationContract]
        string GetToken(string clientIPAddress);

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="bingToken">The bing token.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>Geocode result</returns>
        [OperationContract]
        dev.virtualearth.net.webservices.v1.common.GeocodeResult[] GetSearchResults(string query, string bingToken, string culture);

        /// <summary>
        /// Calls the reverse geocode to get an address from a location.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>The Geocode Response</returns>
        [OperationContract]
        dev.virtualearth.net.webservices.v1.geocode.GeocodeResponse GetReverseGeocode(string token, string culture, double longitude, double latitude); 
    }
}
