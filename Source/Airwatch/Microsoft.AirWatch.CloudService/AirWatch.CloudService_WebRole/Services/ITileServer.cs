﻿//-----------------------------------------------------------------------
// <copyright file="ITileServer.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>24/08/2009</date>
// <summary>Interface for the Image Service, will retrive images a layer.</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System.IO;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    /// <summary>
    /// Interface for the Image Service, will retrive images a layer.
    /// </summary>
    [ServiceContract]
    public interface ITileServer
    {
        /// <summary>
        /// Gets the tile image.
        /// </summary>
        /// <param name="layer">The layer.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>Stream representing the needed image.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2243:AttributeStringLiteralsShouldParseCorrectly", Justification = "URL currently works, will change when stable")]
        [WebGet(UriTemplate = "{layer}/{quadKey}")]
        [OperationContract]
        Stream GetTileImage(string layer, string quadKey);
    }
}
