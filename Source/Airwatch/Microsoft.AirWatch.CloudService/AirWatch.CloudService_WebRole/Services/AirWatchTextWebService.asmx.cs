﻿// <copyright file="AirWatchTextWebService.asmx.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>Short message web service</summary>
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Mobile.Messaging;
    using System.Text.RegularExpressions;
    using System.Web.Services;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;

    /// <summary>
    /// AirWatch Short Message WebService
    /// </summary>
    [WebService(Namespace = "http://schemas.microsoft.com/ws/2004/05/mws/sms")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AirWatchTextWebService : System.Web.Services.WebService
    {
        /// <summary>
        /// Azure account from config
        /// </summary>
        // private StorageAccountInfo account = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();
        private CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");

        /// <summary>
        /// Azure queue storage
        /// </summary>
        // private QueueStorage queueStorage;
        private CloudQueueClient queueStorage;

        /// <summary>
        /// Azure message queue
        /// </summary>
        // private MessageQueue shortMessageQueue;
        private CloudQueue shortMessageQueue;
        /// <summary>
        /// Phone number regex
        /// </summary>
        private Regex phoneNumberRegex = new Regex(@"[0-9+,.\(\)\- ]+", RegexOptions.Compiled);

        /// <summary>
        /// Message body regex
        /// </summary>
        private Regex messageBodyRegex = new Regex(@"[A-Za-z0-9+;,.\(\)\- ]+", RegexOptions.Compiled);

        /// <summary>
        /// Processes incoming messages
        /// </summary>
        /// <param name="msg">Short message with request</param>
        /// <returns>Short message with reply</returns>
        [WebMethod]
        public ShortMessage ProcessMessageWithResponse(ShortMessage msg)
        {
            Trace.TraceInformation("New Message: Origination: " + msg.From + " Destination: " + msg.To + "Body: " + msg.Message);

            this.queueStorage = this.account.CreateCloudQueueClient();
            this.shortMessageQueue = queueStorage.GetQueueReference("shortmessagequeue");
            shortMessageQueue.CreateIfNotExist();

            ShortMessage regexedMessage = new ShortMessage();

            try
            {
                // If the regexed message does NOT contain an empty or null property
                if (!String.IsNullOrEmpty(this.phoneNumberRegex.Match(msg.From).Value.Trim())
                    && !String.IsNullOrEmpty(this.phoneNumberRegex.Match(msg.To).Value.Trim())
                    && !String.IsNullOrEmpty(this.messageBodyRegex.Match(msg.Message).Value.Trim()))
                {
                    regexedMessage.From = this.phoneNumberRegex.Match(msg.From).Value;
                    regexedMessage.To = this.phoneNumberRegex.Match(msg.To).Value;
                    regexedMessage.Message = this.messageBodyRegex.Match(msg.Message).Value;
                }
                else
                {
                    Trace.TraceWarning("The message contains a parameter with no content");
                    return new ShortMessage("200", "The message contains a parameter with no content");
                }
            }
            catch (ArgumentNullException)
            {
                Trace.TraceWarning("The message contains a null parameter");
                return new ShortMessage("100", "The message contains a null parameter");
            }

            CloudQueueMessage queueMsg = new CloudQueueMessage(regexedMessage.From + "%" + regexedMessage.To + "%" + regexedMessage.Message);
            this.shortMessageQueue.AddMessage(queueMsg);

            Trace.TraceInformation("Message put on the queue: " + queueMsg.AsString);

            return new ShortMessage("000", "ok");
        }

        /// <summary>
        /// Processes incoming messages
        /// </summary>
        /// <param name="msg">Short message with request</param>
        [WebMethod]
        public void ProcessMessage(ShortMessage msg)
        {
            Trace.TraceInformation("New Message: Origination: " + msg.From + " Destination: " + msg.To + "Body: " + msg.Message);
            this.queueStorage = this.account.CreateCloudQueueClient();
            this.shortMessageQueue = queueStorage.GetQueueReference("shortmessagequeue");
            shortMessageQueue.CreateIfNotExist();

            ShortMessage regexedMessage = new ShortMessage();

            try
            {
                // If the regexed message does NOT contain an empty or null property
                if (!String.IsNullOrEmpty(this.phoneNumberRegex.Match(msg.From).Value.Trim())
                    && !String.IsNullOrEmpty(this.phoneNumberRegex.Match(msg.To).Value.Trim())
                    && !String.IsNullOrEmpty(this.messageBodyRegex.Match(msg.Message).Value.Trim()))
                {
                    regexedMessage.From = this.phoneNumberRegex.Match(msg.From).Value;
                    regexedMessage.To = this.phoneNumberRegex.Match(msg.To).Value;
                    regexedMessage.Message = this.messageBodyRegex.Match(msg.Message).Value;
                }
                else
                {
                    Trace.TraceWarning("The message contains a parameter with no content");
                    return;
                }
            }
            catch (ArgumentNullException)
            {
                Trace.TraceWarning("The message contains a null parameter");
                return;
            }

            CloudQueueMessage queueMsg = new CloudQueueMessage(regexedMessage.From + "%" + regexedMessage.To + "%" + regexedMessage.Message);

            this.shortMessageQueue.AddMessage(queueMsg);
        }
    }
}
