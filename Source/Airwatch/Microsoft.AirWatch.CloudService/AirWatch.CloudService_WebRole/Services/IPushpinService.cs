﻿//-----------------------------------------------------------------------
// <copyright file="IPushpinService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2008</date>
// <summary>Service Contract Interface for Pushpin Service</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System.ServiceModel;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Service Contract Interface for Pushpin Service
    /// </summary>
    [ServiceContract]
    public interface IPushpinService
    {
        /// <summary>
        /// Allows the user to create a pushpin
        /// </summary>
        /// <param name="longitude">The longitude of the pushpin</param>
        /// <param name="latitude">The latitude of the pushpin</param>
        /// <returns>A pushpin object at the location specified</returns>
        [OperationContract]
        Pushpin GetPushpin(decimal longitude, decimal latitude);
    }
}
