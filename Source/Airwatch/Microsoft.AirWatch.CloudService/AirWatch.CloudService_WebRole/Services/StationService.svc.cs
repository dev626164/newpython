﻿//-----------------------------------------------------------------------
// <copyright file="StationService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>17 August 2009</date>
// <summary>Web Service for getting station data</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel.Activation;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Core.Data;
    using System.Diagnostics;
    using Microsoft.AirWatch.Common;

    /// <summary>
    /// Web Service for getting station data
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class StationService : IStationService
    {
        /// <summary>
        /// Gets the station descriptions for map boundaries.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <param name="threshold">The threshold, returns an empty collection if the number is more than this, enter 0 to return all (override).</param>
        /// <returns>Array of min info about the station</returns>
        public Collection<StationContainer> GetStationDescriptionsForMapBoundaries(double north, double east, double south, double west, int threshold)
        {
            try
            {
                Microsoft.AirWatch.Core.Data.StationMinimal[] stationArray = ServiceProvider.StationService.GetStationsForBoundary(north, east, south, west);
                Collection<StationContainer> stationCollection = new Collection<StationContainer>();

                if (stationArray.Count() > 0 && (stationArray.Count() <= threshold || threshold == 0))
                {
                    foreach (Microsoft.AirWatch.Core.Data.StationMinimal station in stationArray)
                    {
                        stationCollection.Add(new StationContainer() { StationId = station.StationId, Latitude = station.Latitude, Longitude = station.Longitude, EuropeanCode = station.EuropeanCode, QualityIndex = station.QualityIndex, StationPurpose = station.StationPurpose.ToString() });
                    }
                }

                return stationCollection;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetStationDescriptionsForMapBoundaries in StationService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Gets the station by code.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        /// <returns>Station for stationId</returns>
        public Station GetStationByCode(int stationId)
        {
            try
            {
                Station stationToReturn = ServiceProvider.StationService.GetStationForStationCode(stationId);
                stationToReturn = AttachAggregatedUserRatingToStation(stationToReturn);

                return stationToReturn;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetStationByCode in StationService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Gets the closest station.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="stationType">Type of the station.</param>
        /// <returns>The closest station of the type specified to the long/lat given or null if none found</returns>
        public Station GetClosestStation(double longitude, double latitude, string stationType)
        {
            try
            {
                Station station = ServiceProvider.StationService.GetClosestStation(longitude, latitude, stationType);
                station = AttachAggregatedUserRatingToStation(station);

                return station;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetClosestStation in StationService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Attaches the aggregated user rating to station.
        /// </summary>
        /// <param name="station">The station.</param>
        /// <returns>The Station with the attached ratings</returns>
        private static Station AttachAggregatedUserRatingToStation(Station station)
        {
            station.AverageRating = ServiceProvider.RatingService.GetRating(
                        (double)station.Location.Latitude, (double)station.Location.Longitude, station.StationPurpose, Microsoft.AirWatch.Core.ApplicationServices.StationService.Offset);

            Collection<RatingQualifierIdentifiers> rqi = new Collection<RatingQualifierIdentifiers>();

            if (station.AverageRating != null)
            {
                var ratingQualifiers = station.AverageRating.RatingQualifierIdentifiers.ToDictionary(ratingQualifier => ratingQualifier.WordIdentifier);

                for (int i = 0; i < 8; i++)
                {
                    if (station.AverageRating.RatingQualifierIdentifiers != null)
                    {
                        if (ratingQualifiers.ContainsKey(i.ToString(CultureInfo.CurrentCulture)))
                        {
                            rqi.Add(new RatingQualifierIdentifiers()
                            {
                                WordIdentifier = string.Format(CultureInfo.InvariantCulture, "RATELOCATION_{0}_{1}", station.StationPurpose.ToUpper(CultureInfo.InvariantCulture), i),
                                Percentage = ratingQualifiers[i.ToString(CultureInfo.CurrentCulture)].Percentage
                            });
                        }
                        else
                        {
                            rqi.Add(new RatingQualifierIdentifiers()
                            {
                                WordIdentifier = string.Format(CultureInfo.InvariantCulture, "RATELOCATION_{0}_{1}", station.StationPurpose.ToUpper(CultureInfo.InvariantCulture), i),
                                Percentage = 0
                            });
                        }
                    }
                    else
                    {
                        rqi.Add(new RatingQualifierIdentifiers()
                        {
                            WordIdentifier = string.Format(CultureInfo.InvariantCulture, "RATELOCATION_{0}_{1}", station.StationPurpose.ToUpper(CultureInfo.InvariantCulture), i),
                            Percentage = 0
                        });
                    }
                }

                station.AverageRating.RatingQualifierIdentifiers.Clear();
                station.AverageRating.RatingQualifierIdentifiers = rqi;
            }

            return station;
        }
    }
}
