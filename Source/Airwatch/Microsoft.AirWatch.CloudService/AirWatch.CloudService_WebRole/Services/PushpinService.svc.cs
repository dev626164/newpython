﻿//-----------------------------------------------------------------------
// <copyright file="PushpinService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2009</date>
// <summary>Concrete implementation of IPushpinService Service Contract Implementation</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel.Activation;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.AirWatch.Core.Data;
    using System.Diagnostics;

    /// <summary>
    /// Concrete implementation of IPushpinService Service Contract Implementation
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PushpinService : IPushpinService
    {
        /// <summary>
        /// Temporary constant for offset when getting ratings
        /// </summary>
        private const double Offset = 0.03;

        /// <summary>
        /// Allows the user to create a pushpin
        /// </summary>
        /// <param name="longitude">The longitude of the pushpin</param>
        /// <param name="latitude">The latitude of the pushpin</param>
        /// <returns>A pushpin object at the location specified</returns>
        public Pushpin GetPushpin(decimal longitude, decimal latitude)
        {
            try
            {
                Pushpin pushpinToReturn = ServiceProvider.PushpinService.GetPushpin(longitude, latitude);

                // NB: air value hard-coded as a pushpin will only ever show air data.
                pushpinToReturn.AverageRating = ServiceProvider.RatingService.GetRating(
                        (double)latitude, (double)longitude, "Air", Offset);

                // organise the user rating word cloud within the pushpin
                PushpinRatingHelper.SetPushpinRatingsWordCloud(pushpinToReturn);
                
                return pushpinToReturn;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetPushpin in PushpinService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }
    }
}
