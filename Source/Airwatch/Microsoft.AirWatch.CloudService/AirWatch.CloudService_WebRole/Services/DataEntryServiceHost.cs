﻿//-----------------------------------------------------------------------
// <copyright file="DataEntryServiceHost.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki, Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>24/08/09</date>
// <summary>Data Entry Service Host</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System;
    using System.IO;
    using System.Security.Cryptography.X509Certificates;
    using System.ServiceModel;
    using Microsoft.AirWatch.Common;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;

    /// <summary>
    /// Data Entry Service Host
    /// </summary>
    public class DataEntryServiceHost : ServiceHost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataEntryServiceHost"/> class.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="baseAddresses">The base addresses.</param>
        public DataEntryServiceHost(Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
        }

        /// <summary>
        /// Loads the service description information from the configuration file and applies it to the runtime being constructed.
        /// </summary>
        /// <exception cref="T:System.InvalidOperationException">The description of the service hosted is null.</exception>
        protected override void ApplyConfiguration()
        {
            base.ApplyConfiguration();

            CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            CloudBlobClient blobClient = account.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("dataentrycheck");
            blobContainer.CreateIfNotExist();

            CloudBlob blob = blobContainer.GetBlobReference("entry");
            if (blob.Exists())
            {
                var blobContents = blob.DownloadByteArray();

                if (blobContents.Length > 0)
                {
                    X509Certificate2 cert = new X509Certificate2(blobContents);
                    this.Credentials.ClientCertificate.Certificate = cert;
                }
                else
                {
                    this.Credentials.ServiceCertificate.Certificate = null;
                    Trace.TraceWarning("Could not retrieve the data entry certificate, no data may be entered");
                }
            }
            else
            {
                this.Credentials.ServiceCertificate.Certificate = null;
                Trace.TraceWarning("There is no data entry certificate, no data may be entered");
            }
        }
    }
}
