﻿//-----------------------------------------------------------------------
// <copyright file="IMappingService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>15-06-2009</date>
// <summary>Service Contract Interface for User Service.</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    #region Using

    using System.ServiceModel;

    #endregion

    /// <summary>
    /// Service Contract Interface for User Service.
    /// </summary>
    [ServiceContract]
    public interface IMappingService
    {
        /// <summary>
        /// Does the work.
        /// </summary>
        [OperationContract]
        void DoWork();
    }
}
