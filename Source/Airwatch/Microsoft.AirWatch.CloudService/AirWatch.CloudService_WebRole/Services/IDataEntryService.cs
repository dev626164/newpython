﻿//-----------------------------------------------------------------------
// <copyright file="IDataEntryService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>13 July 2009</date>
// <summary>Interface for the data entry webservice for Map Model Data</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System.ServiceModel;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Interface for the data entry webservice for Map Model Data
    /// </summary>
    [ServiceContract]
    public interface IDataEntryService
    {
        /// <summary>
        /// Enter Map Model Data will take the data write it to a blob in mapmodeldatablob
        /// then put a message on the mapmodeldataqueue with the name of the blob to get
        /// </summary>
        /// <param name="data">The Data to enter</param>
        [OperationContract]
        void EnterMapModelData(string data);

        /// <summary>
        /// Enter Data will take the data write it to blobs in airstationdatablob
        /// then put a message on the airstationdataqueue with the name of the blob to get
        /// </summary>
        /// <param name="data">The Data to enter</param>
        [OperationContract]
        void EnterAirStationData(XElement data);

        /// <summary>
        /// Enter Data will take the data write it to blobs in waterstationdatablob
        /// then put a message on the waterstationdataqueue with the name of the blob to get
        /// </summary>
        /// <param name="data">The Data to enter</param>
        [OperationContract]
        void EnterWaterStationData(XElement data);
    }
}
