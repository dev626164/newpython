﻿//-----------------------------------------------------------------------
// <copyright file="LanguageService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>12/07/09</date>
// <summary>Language Service</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel.Activation;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using System.Diagnostics;

    /// <summary>
    /// Language service
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LanguageService : ILanguageService
    {
        #region ILanguageService Members

        /// <summary>
        /// Gets the specified language.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>Language dictionary</returns>
        public Dictionary<string, string> GetLanguage(string culture)
        {
            try
            {                
                return LocalizationHelper.Instance.GetLanguage(culture);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetLanguage in LanguageService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Gets the time stamp.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>The timestamp of the last update to this Culture.</returns>
        public DateTime GetTimestamp(string culture)
        {
            try
            {
                return ServiceProvider.LanguageService.GetTimestamp(culture);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetLanguage in LanguageService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Get all languages from the database
        /// </summary>
        /// <returns>Array of Languages</returns>
        public LanguageDescription[] GetLanguages()
        {
            try
            {
                return LocalizationHelper.Instance.LanguageDescriptions;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetLanguages in LanguageService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        #endregion
    }
}
