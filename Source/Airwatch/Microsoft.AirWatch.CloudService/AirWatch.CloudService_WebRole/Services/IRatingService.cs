﻿//-----------------------------------------------------------------------
// <copyright file="IRatingService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>19/08/09</date>
// <summary>Rating service contract</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System.ServiceModel;

    /// <summary>
    /// User rating service interface
    /// </summary>
    [ServiceContract]
    public interface IRatingService
    {
        /// <summary>
        /// Sets the rating.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="rating">The rating.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userAddress">The user address.</param>
        /// <param name="wordQualifiers">The word qualifiers.</param>
        [OperationContract]
        void SetRating(double latitude, double longitude, int rating, string ratingTarget, string userAddress, bool[] wordQualifiers);
    }
}
