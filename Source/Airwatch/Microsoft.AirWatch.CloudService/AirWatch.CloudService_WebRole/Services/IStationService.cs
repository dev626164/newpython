﻿//-----------------------------------------------------------------------
// <copyright file="IStationService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>17 August 2009</date>
// <summary>Web Service for getting station data</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.Services
{
    using System.Collections.ObjectModel;
    using System.ServiceModel;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.AirWatch.Common;

    /// <summary>
    /// Web Service for getting station data
    /// </summary>
    [ServiceContract]
    public interface IStationService
    {
        /// <summary>
        /// Gets the station descriptions for map boundaries.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <param name="threshold">The threshold, returns an empty collection if the number is more than this, enter 0 to return all (override).</param>
        /// <returns>Array of min info about the station</returns>
        [OperationContract]
        Collection<StationContainer> GetStationDescriptionsForMapBoundaries(double north, double east, double south, double west, int threshold);

        /// <summary>
        /// Gets the station by code.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        /// <returns>Station for stationId</returns>
        [OperationContract]
        Station GetStationByCode(int stationId);

        /// <summary>
        /// Gets the closest station.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="stationType">Type of the station.</param>
        /// <returns>The closest station of the type specified to the long/lat given or null if none found</returns>
        [OperationContract]
        Station GetClosestStation(double longitude, double latitude, string stationType);
    }
}
