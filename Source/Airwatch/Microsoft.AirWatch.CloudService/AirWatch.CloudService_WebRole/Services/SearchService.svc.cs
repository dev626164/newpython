﻿//-----------------------------------------------------------------------
// <copyright file="SearchService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>22/06/09</date>
// <summary>Bing Maps Search service</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.ServiceModel.Activation;
    using dev.virtualearth.net.webservices.v1.common;
    using dev.virtualearth.net.webservices.v1.geocode;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using System.Diagnostics;

    /// <summary>
    /// The bing maps AirWatch search service
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SearchService : ISearchService
    {
        #region ISearchService Members

        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <param name="clientIPAddress">The client IP address.</param>
        /// <returns>Bing token</returns>
        public string GetToken(string clientIPAddress)
        {
            try
            {
                return BingMapServicesHelper.GetBingToken(clientIPAddress);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetToken in SearchService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="bingToken">The bing token.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>Geocode results</returns>
        public GeocodeResult[] GetSearchResults(string query, string bingToken, string culture)
        {
            try
            {
                return BingMapServicesHelper.CallBingMapServices(bingToken, culture, query).Results;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetSearchResults in SearchService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Calls the reverse geocode to get an address from a location.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>The Geocode Response</returns>
        public GeocodeResponse GetReverseGeocode(string token, string culture, double longitude, double latitude)
        {
            try
            {
                return BingMapServicesHelper.CallReverseGeocodeServices(token, culture, longitude, latitude);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Exception thrown when calling GetReverseGeocode in SearchService");
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return null;
            }
        }

        #endregion
    }
}
