﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Developers.aspx.cs" Inherits="AirWatch.CloudService.WebRole.Developers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link href="style/Developers.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="banner">
	        <div id="eoeLogo">
		        <img  alt="Eye on Earth Logo" src="AspNetVisualAssets/BuiltOnWindowsAzure274x60.png"/>
            </div>
            <div id="contentRightPlaceholder">                
                <div class="eeaLogo">
                     <a href="http://www.eea.europa.eu"><img alt="EEA Logo" src="AspNetVisualAssets/EEALogo232x35.png" border="0" /></a>
                </div>
            </div>    
        </div>
        <div class="content">
            <div>
                <div class="intro">
                    <h1>Eye On Earth Developer Platform</h1>
                    <p>Eye On Earth is a platform as well as a portal.  
                    The intention was to allow 3rd parties to be able to consume aspects of Eye On Earth in a number of ways.</p>
                    <p>You can embed Eye On Earth in your application very easily by using the following simple features</p>
                    <ul>
                        <li>Hosting the full Eye On Earth application as an IFrame</li> 
                        <li>Hosting Eye On Earth on your site using it's 'gadget mode'</li>                       
                        <li>Or making use of the IE 8 Webslice features.</li>
                    </ul>
                    <p>You can also extend the Eye On Earth platform by consuming the webservices that are available to you.
                    These webservices include:</p>
                    <ul>
                        <li>Station data</li>                
                        <li>Pushpin data</li>
                        <li>User feedback and rating data</li>
                    </ul>
                </div>
                <div class="platformImage">
                    <img src="AspNetVisualAssets/Developers/platform.png" />
                </div>
            </div>
            <div class="embed">
                <h2>Embedding Eye On Earth in an IFrame</h2> 
                
                When embedding the Eye On Earth application in your webpage you can pass the following parameters into the query string:
                <ul>
                    <li>gadget - Determines whether to show the Eye On Earth application in full or 'gadget mode'</li>
                    <li>header - The header text to use when in 'gadget mode'</li>
                    <li>longitude - Initial longitude of the map center in decimal degrees <i>(for example 50.825)</i></li>
                    <li>latitude - Initial latitude of the map center in decimal degrees <i>(for example 1.221)</i></li>
                    <li>zoom - Initial zoom level of the map (between 3 and 24)</li>
                    <li>showpushpin - If the flag is set to true then a pushpin will be added to the location.</li>
                    <li>width / height - will be determined by the width and height of the iframe. Please note the optimal dimensions for the gadget are at least 1024*768, with minimum dimensions of 875 * 400.</li>                    
                </ul> 
                <h3>Example 1 - Embedding the full application</h3>
                <p>In this example we embed the entire application in to a site by using the <code>&lt;iframe&gt;</code> tag. We also set the properties for <i>latitude</i>, <i>longitude</i>, <i>zoom</i> and also indicate that we want to see a pushpin by using the <i>showpushpin</i> parameter.</p>
                <code class="indent1">
                    &lt;div&gt;
                </code>
                <br />
                <code class="indent2">
                    &lt;iframe name=&quot;eoe_frame&quot; src=&quot;http://eyeonearthtest.cloudapp.net.eu/Default.aspx?latitude=<i>55.676&amp;</i>longitude=<i>12.569&amp;</i>zoom=<i>10&amp;</i>showpushpin=<i>true</i>&quot;                   
                </code>
                <br />
                <code class="indent4">
                    style=&quot;<b>height:<i>500px</i>; width: <i>875px</i></b>;<b>&quot;</b> /&gt;<br />
                </code>
                <code class="indent1">
                    &lt;/div&gt;
                </code>
                <br />
                <h3>Example 2 - Embedding the application in gadget mode</h3>
                <p>In this example we embed the application in gadget mode by adding the <i>gadgetmode</i> and <i>header</i> parameters.</p>
                <code class="indent2">
                   &lt;iframe name=&quot;eoe_frame&quot; 
                </code>
                <br />
                <code class="indent4">
                   href="http://eyeonearthtest.cloudapp.net/default.aspx?gadget=true&amp;location=43.5,6.1&amp;header=Air%20Quality                   
                </code>
                <br />
                <code class="indent4">
                    width="320"
                </code>
                <br />
                <code class="indent4">
                    height="240"&gt;
                </code>
                <br />
                <code class="indent2">
                    &lt;/iframe&gt;
                </code>                 
                <h3>Example 3 - Turning the Eye On Earth appplication into an IE8 Web Slice</h3>
                <p>In this example we wrap 'Example 2' up inside an Internet Explorer 8 Web Slice.</p>
                <code class="indent1">
                    &lt;div class="hslice" id="MyWebSlice"&gt;
                </code>
                <br />
                <code class="indent2">
                    &lt;div style="display:none" class="entry-title"&gt;Eye On Earth&lt;div&gt;
                </code>
                <br />
                <code class="indent2">
                    &lt;a style="display:none"
                </code>
                <br />
                <code class="indent3">
                    rel="entry-content"
                </code>
                <br />
                <code class="indent3">
                    href="http://eyeonearthtest.cloudapp.net/default.aspx?gadget=true&amp;location=43.5,6.1&amp;header=Air%20Quality"&gt;
                </code>
                <br />
                <code class="indent2">
                    &lt;/a&gt;
                </code>
                <br />
                <code class="indent2">
                   &lt;iframe name=&quot;eoe_frame&quot; 
                </code>
                <br />
                <code class="indent4">
                   href="http://eyeonearthtest.cloudapp.net/default.aspx?gadget=true&amp;location=43.5,6.1&amp;header=Air%20Quality"                   
                </code>
                <br />
                <code class="indent4">
                    width="320"
                </code>
                <br />
                <code class="indent4">
                    height="240"&gt;
                </code>
                <br />
                <code class="indent2">
                    &lt;/iframe&gt;
                </code> 
                <br /> 
                <code class="indent2">
                    &lt;span class="ttl" style="display: none"&gt;15&lt;/span&gt;
                </code> 
                <br /> 
                <code class="indent1">
                    &lt;/div&gt;
                </code> 
             </div>    
            <div class="webServices">
                <h2>Using the Eye On Earth Web Services</h2>
                <p>The Eye On Earth WCF Web Services platform gives you access to the underlying API that EoE consumes. This API is available to everyone under the <i>http://eyeonearthtest.cloudapp.net url</i>.  For production use please contact the <a href="mailto:eeaeoe@hotmail.com" >Eye On Earth team</a>.</p>
                <div id="StationService">
                    <h3>Station Service</h3>
                    <p><a href="http://eyeonearthtest.cloudapp.net/Services/StationService.svc">http://eyeonearthtest.cloudapp.net/Services/StationService.svc</a></p>
                    Web Service for getting station data.
                    <div>
                        <h4>Methods</h4>
                        <div id="GetStationDescriptionsForMapBoundaries">
                            <code class="indent1">
                                <i>GetStationDescriptionsForMapBoundaries(System.Double,System.Double,System.Double,System.Double,System.Int32)</i>
                            </code>
                            <br />
                            <span class="indent2">
                                Gets the station descriptions for map boundaries. 
                            </span>
                            <h5 class="indent3">Parameters</h5>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">north</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The northern boundary of the area to search for stations</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">east</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The eastern boundary of the area to search for stations</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">south</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The southern boundary of the area to search for stations</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">west</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The western boundary of the area to search for stations</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">threshold</span>
                                <span class="paramCol2"><code>System.In32</code></span>                   
                                <span class="paramCol3">The threshold, returns an empty collection if the number is more than this, enter 0 to return all (override).</span>
                            </div>
                            <div class="indent3">
                                <h5>Returns</h5>
                                <p>Array of min info about the station</p>
                            </div>
                        </div>
                        <div id="GetStationByCode">
                            <code class="indent1">
                                <i>GetStationByCode(System.Int32)</i>
                            </code>
                            <br />
                            <span class="indent2">
                                Gets a Station object given the standard EEA European code for a monitoring station. 
                            </span>
                            <h5 class="indent3">Parameters</h5>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">stationId</span>
                                <span class="paramCol2"><code>System.String</code></span>
                                <span class="paramCol3">The station id</span>
                            </div>
                            <div class="indent3">
                                <h5>Returns</h5>
                                <p>A Station object for the given EEA European Code of a station</p>
                            </div>
                        </div>
                        <div id="GetClosestStation">
                            <code class="indent1">
                                <i>GetClosestStation(System.Double,System.Double,System.String)</i>
                            </code>
                            <br />
                            <span class="indent2">
                               Gets the closest station to a particular latitude and longitude of the requested type. 
                            </span>
                            <h5 class="indent3">Parameters</h5>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">longitude</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The longitude of the station being requested</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">latitude</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The latitude of the station being requested</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">stationType</span>
                                <span class="paramCol2"><code>System.String</code></span>
                                <span class="paramCol3">The longitude of the station being requested</span>
                            </div>
                            <div class="indent3">
                                <h5>Returns</h5>
                                <p>The closest station of the type specified to the long/lat given or null if none found</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="PushpinService">
                    <h3>Pushpin Service</h3>
                    <p><a href="http://eyeonearthtest.cloudapp.net/Services/PushpinService.svc">http://eyeonearthtest.cloudapp.net/Services/PushpinService.svc</a></p>
                    Web Service for getting pushpin data.
                    <div>
                        <h4>Methods</h4>
                        <div id="GetPushpin">
                            <code class="indent1">
                                <i>GetPushpin(System.Decimal,System.Decimal)</i>
                            </code>
                            <br />
                            <span class="indent2">
                                Allows the user to create a pushpin . 
                            </span>
                            <h5 class="indent3">Parameters</h5>                            
                            <div class="paramRow">
                                <span class="indent4 paramCol1">longitude</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The longitude of the pushpin</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">latitude</span>
                                <span class="paramCol2"><code>System.Double</code></span>                   
                                <span class="paramCol3">The latitude of the pushpin</span>
                            </div>
                            <div class="indent3">
                                <h5>Returns</h5>
                                <p>A pushpin object at the location specified</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="RatingService">
                    <h3>Rating Service</h3>
                    <p><a href="http://eyeonearthtest.cloudapp.net/Services/RatingService.svc">http://eyeonearthtest.cloudapp.net/Services/RatingService.svc</a></p>
                    Web Service for getting pushpin data.
                    <div>
                        <h4>Methods</h4>
                        <div id="SetRating">
                            <code class="indent1">
                                <i>SetRating(System.Double,System.Double,System.Int32,System.String,System.String,System.Boolean[])</i>
                            </code>
                            <br />
                            <span class="indent2">
                                Sets the user rating of an air station, water station or pushpin
                            </span>
                            <h5 class="indent3">Parameters</h5>                            
                            <div class="paramRow">
                                <span class="indent4 paramCol1">latitude</span>
                                <span class="paramCol2"><code>System.Double</code></span>                   
                                <span class="paramCol3">The latitude of the pushpin</span>
                            </div>                            
                            <div class="paramRow">
                                <span class="indent4 paramCol1">longitude</span>
                                <span class="paramCol2"><code>System.Double</code></span>
                                <span class="paramCol3">The longitude of the pushpin</span>
                            </div>
                            <div class="paramRow">
                                <span class="indent4 paramCol1">rating</span>
                                <span class="paramCol2"><code>System.Int32</code></span>                   
                                <span class="paramCol3">The rating of the pushpin between 1 and 5</span>
                            </div>  
                            <div class="paramRow">
                                <span class="indent4 paramCol1">ratingTarget</span>
                                <span class="paramCol2"><code>System.String</code></span>                   
                                <span class="paramCol3">A string containing either 'water' or 'air' indicating what is being rated.</span>
                            </div> 
                            <div class="paramRow">
                                <span class="indent4 paramCol1">userAddress</span>
                                <span class="paramCol2"><code>System.String</code></span>                   
                                <span class="paramCol3">The public ipAddress of the client that is calling the web service.</span>
                            </div>               
                            <div class="paramRow">
                                <span class="indent4 paramCol1">wordQualifiers</span>
                                <span class="paramCol2"><code>System.Boolean[]</code></span>                   
                                <span class="paramCol3">A boolean array indicating which word qualifiers have been chosen</span>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
    </form>
</body>
</html>
