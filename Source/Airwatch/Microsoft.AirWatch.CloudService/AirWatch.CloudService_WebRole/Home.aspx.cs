﻿// <copyright file="Home.aspx.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>16-07-2009</date>
// <summary>Partial class code-behind for home.aspx</summary>
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Linq;   
    using System.Web;    
    using Microsoft.AirWatch.AzureTables;

    /// <summary>
    /// Partial class code-behind for home.aspx
    /// </summary>
    public partial class Home : System.Web.UI.Page
    {
        /// <summary>
        /// User culture
        /// </summary>
        private string userCulture;

        /// <summary>
        /// Gets or sets the user culture.
        /// </summary>
        /// <value>The user culture.</value>
        public string UserCulture
        {
            get
            {
                return this.userCulture;
            }

            set
            {
                this.userCulture = value;
            }
        }

        /// <summary>
        /// Gets the supported VE culture.
        /// </summary>
        /// <value>The supported user culture.</value>
        /// <returns>A supported Virtual Earth Culture</returns>
        public string SupportedUserCulture
        {
            get
            {
                string culture = string.Empty;

                switch (this.UserCulture)
                {
                    case "en-GB":
                    case "en-CA":
                    case "en-US":
                    case "ja-JP":
                    case "cs-CZ":
                    case "da-DK":
                    case "nl-NL":
                    case "fi-FI":
                    case "fr-FR":
                    case "de-DE":
                    case "it-IT":
                    case "nb-NO":
                    case "pt-PT":
                    case "es-ES":
                    case "sv-SE":
                    case "zh-CN":
                        culture = this.UserCulture;
                        break;
                    default:
                        culture = "en-GB";
                        break;
                }

                return culture;
            }
        }

        /// <summary>
        /// Gets or sets the language description array.
        /// </summary>
        /// <value>The language description array.</value>
        public LanguageDescription[] LanguageDescriptionArray { get; set; }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                this.LanguageDescriptionArray = LocalizationHelper.Instance.LanguageDescriptions;
                this.LanguageSelect.DataTextField = "LanguageName";
                this.LanguageSelect.DataValueField = "LanguageCode";
                this.LanguageSelect.DataSource = this.LanguageDescriptionArray;
                this.LanguageSelect.DataBind();   

                ClientScript.RegisterClientScriptBlock(this.GetType(), "script", "var blobStorageUrl = '" + this.Request.Url.Host + "';", true);                
            }
        }        

        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                this.UserCulture = this.LanguageSelect.SelectedValue = this.LanguageSelect.SelectedItem.Value;

                Response.Cookies["CurrentCulture"].Value = this.UserCulture;
                Response.Cookies["CurrentCulture"].Expires = DateTime.MaxValue;
            }
            else
            {
                this.UserCulture = this.LanguageSelect.SelectedValue = "en-GB";

                if (Request.Cookies["CurrentCulture"] != null)
                {
                    this.UserCulture = this.LanguageSelect.SelectedValue = Request.Cookies["CurrentCulture"].Value;
                }
                else
                {
                    this.LanguageDescriptionArray = LocalizationHelper.Instance.LanguageDescriptions;

                    if (Request.UserLanguages != null)
                    {
                        foreach (string s in Request.UserLanguages)
                        {
                            string[] nonPercentageLanguages = s.Split(new char[] { ';' });

                            if (nonPercentageLanguages[0] != null)
                            {
                                LanguageDescription languageDescription = this.LanguageDescriptionArray.SingleOrDefault(p => p.LanguageCode.ToUpperInvariant().Substring(0, 2) == nonPercentageLanguages[0].ToUpperInvariant().Substring(0, 2));

                                if (languageDescription != null)
                                {
                                    this.UserCulture = this.LanguageSelect.SelectedValue = languageDescription.LanguageCode;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        this.UserCulture = this.LanguageSelect.SelectedValue = "en-GB";
                    }
                }
            }
        }        
    }
}
