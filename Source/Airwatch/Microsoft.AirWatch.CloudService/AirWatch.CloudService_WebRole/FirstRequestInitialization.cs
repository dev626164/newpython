﻿// <copyright file="FirstRequestInitialization.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>Global 
// </summary>
namespace AirWatch.CloudService.WebRole
{
    using System.Collections.Generic;
    using System.Threading;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Core.ApplicationServices;
    using Microsoft.Samples.ServiceHosting.StorageClient;
    using Neudesic.Azure;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// First request init
    /// </summary>
    internal static class FirstRequestInitialization
    {
        /// <summary>
        /// Initialised already
        /// </summary>
        private static bool initializedAlready;

        /// <summary>
        /// Object lock
        /// </summary>
        private static object lockObject = new object();

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public static void Initialize()
        {
            if (initializedAlready)
            {
                return;
            }

            lock (lockObject)
            {
                if (initializedAlready)
                {
                    return;
                }

                initializedAlready = true;
                ApplicationStartUponFirstRequest();                
            }
        }

        /// <summary>
        /// Application starts upon first request.
        /// </summary>
        private static void ApplicationStartUponFirstRequest()
        {
            ////// This is where you put initialization logic for the site.
            ////// RoleManager is properly initialized at this point.

            ////// Create the tables on first request initialization as there is a performance impact
            ////// if you call CreateTablesFromModel() when the tables already exist. This limits the exposure of
            ////// creating tables multiple times.

            ////// Get the settings from the Service Configuration file
            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            var tableClient = account.CreateCloudTableClient();

            ////// Create the tables
            ////// This will create tables for all public properties that are IQueryable (collections)
            tableClient.CreateTableIfNotExist("Localisations");
            tableClient.CreateTableIfNotExist("InternalConfiguration");
            tableClient.CreateTableIfNotExist("Languages");
            tableClient.CreateTableIfNotExist("MapModelBounds");
            
            Thread thread = new Thread(LocalizationHelper.Instance.PreloadLanguages);
            thread.Start();
        }
    }
}