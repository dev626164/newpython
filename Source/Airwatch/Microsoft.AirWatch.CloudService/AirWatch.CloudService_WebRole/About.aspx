﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="AirWatch.CloudService.WebRole.About" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href="SubPage.css" rel="Stylesheet" type="text/css" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div>
    <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT_HEADER", Request.QueryString.Get("culture"))%></h2>
    <p>
    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT_TEXT1", Request.QueryString.Get("culture"))%>
    </p>
    <p>
    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT_TEXT2", Request.QueryString.Get("culture"))%>
    </p>
    <p>
    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT_TEXT3", Request.QueryString.Get("culture"))%>
    </p>
    </div>
    </div>
    </form>
</body>
</html>
