﻿//-----------------------------------------------------------------------
// <copyright file="SampleDataEntryWebService.asmx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The Fifth Medium.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>8 June 2009</date>
// <summary>A Sample web service to allow entry of data and prints to log</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System.Web.Services;
    using System.Xml;
    using Microsoft.Samples.ServiceHosting.StorageClient;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;

    /// <summary>
    /// A Sample web service to allow entry of data and prints to log
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    //// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //// [System.Web.Script.Services.ScriptService]
    public class SampleDataEntryWebService : System.Web.Services.WebService
    {
        /// <summary>
        /// Enter Data will take the data and write it to the Role Manager's Log
        /// </summary>
        /// <param name="data">The Data to log</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId = "System.Xml.XmlNode", Justification = "IXPathNavigaable is not Serializable therefore can not be parameterized in a web method"), WebMethod]
        public void EnterData(XmlDocument data)
        {
            CloudStorageAccount account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            CloudBlobClient blobClient = account.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("datablob");
            blobContainer.CreateIfNotExist();

            string blobName = System.DateTime.UtcNow.ToString();
            CloudBlob blob = blobClient.GetBlobReference(blobName);
            blob.UploadByteArray(System.Text.Encoding.Unicode.GetBytes(data.InnerText));
        }
    }
}
