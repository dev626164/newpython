﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="AirWatch.CloudService.WebRole.Help" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <link href="SubPage.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_1",Request.QueryString.Get("culture"))%></h2>
        <ul>
        <li><asp:HyperLink runat="server" NavigateUrl="#hyperCurrentBathingSeason"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_1",Request.QueryString.Get("culture"))%>
        </asp:HyperLink></li>
        <li><asp:HyperLink runat="server" NavigateUrl="#hyperBathingSeasons"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_2",Request.QueryString.Get("culture"))%> 
        </asp:HyperLink></li>
        <li><asp:HyperLink runat="server" NavigateUrl="#hyperMoreInformation"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_3",Request.QueryString.Get("culture"))%>
        </asp:HyperLink></li>
        </ul>
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperCurrentBathingSeason"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_1",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_1",Request.QueryString.Get("culture"))%>
        </p>
        <asp:Table ID="tableCurrentBathingSeason" runat="server" CssClass="tabelWaterIcons">
        <asp:TableHeaderRow><asp:TableHeaderCell ColumnSpan="3" HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_HEADER",Request.QueryString.Get("culture"))%></asp:TableHeaderCell></asp:TableHeaderRow>
        <asp:TableRow><asp:TableCell><img alt="" src="AspNetVisualAssets/RatingIcons/EEAWater1.png" /></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_IMAGETITLE_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_TEXT_1",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><img alt="" src="AspNetVisualAssets/RatingIcons/EEAWater2.png" /></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_IMAGETITLE_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_TEXT_2",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><img alt="" src="AspNetVisualAssets/RatingIcons/EEAWater3.png" /></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_IMAGETITLE_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_1_TEXT_3",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        </asp:Table>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_2",Request.QueryString.Get("culture"))%>
        </p>
        </div>   
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperBathingSeasons"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_2",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_3",Request.QueryString.Get("culture"))%>
        </p><asp:Table ID="tableBathingSeasons" runat="server" CssClass="tabelWaterIcons">
        <asp:TableHeaderRow><asp:TableHeaderCell ColumnSpan="3" HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_HEADER",Request.QueryString.Get("culture"))%></asp:TableHeaderCell></asp:TableHeaderRow>
        <asp:TableRow><asp:TableCell><img alt="" src="AspNetVisualAssets/RatingIcons/EEAWater1.png" /></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_IMAGETITLE_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_TEXT_1",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><img alt="" src="AspNetVisualAssets/RatingIcons/EEAWater2.png" /></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_IMAGETITLE_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_TEXT_2",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><img alt="" src="AspNetVisualAssets/RatingIcons/EEAWater3.png" /></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_IMAGETITLE_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_2_TEXT_3",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        </asp:Table>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_4",Request.QueryString.Get("culture"))%>
        </p>
        </div>             
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperMoreInformation"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_3",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_5",Request.QueryString.Get("culture"))%>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_6",Request.QueryString.Get("culture"))%>
        </p>
        </div>
        
        <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_2", Request.QueryString.Get("culture"))%></h2>
        <ul>
        <li>
        <asp:HyperLink runat="server" NavigateUrl="#HyperQualityIndex"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_4",Request.QueryString.Get("culture"))%></asp:HyperLink>
        </li>
        <li>
        <asp:HyperLink runat="server" NavigateUrl="#hyperPushpins"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_5",Request.QueryString.Get("culture"))%></asp:HyperLink>
        </li>
        <li>
        <asp:HyperLink runat="server" NavigateUrl="#hyperAirModel"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_6",Request.QueryString.Get("culture"))%></asp:HyperLink>
        </li>
        </ul>
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperQualityIndex"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_4",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_7",Request.QueryString.Get("culture"))%>
        </p>
        <asp:Table ID="tableAir" runat="server" CssClass="tabelWaterIcons">
        <asp:TableHeaderRow><asp:TableHeaderCell ColumnSpan="6" HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_HEADER",Request.QueryString.Get("culture"))%></asp:TableHeaderCell></asp:TableHeaderRow>
        <asp:TableRow><asp:TableCell HorizontalAlign="Center"><img alt="" src="AspNetVisualAssets/RatingIcons/EEAAir1.png" /></asp:TableCell><asp:TableCell HorizontalAlign="Center"><img alt="" src="AspNetVisualAssets/RatingIcons/EEAAir2.png" /></asp:TableCell><asp:TableCell HorizontalAlign="Center"><img alt="" src="AspNetVisualAssets/RatingIcons/EEAAir3.png" /></asp:TableCell><asp:TableCell HorizontalAlign="Center"><img alt="" src="AspNetVisualAssets/RatingIcons/EEAAir4.png" /></asp:TableCell><asp:TableCell HorizontalAlign="Center"><img alt="" src="AspNetVisualAssets/RatingIcons/EEAAir5.png" /></asp:TableCell><asp:TableCell HorizontalAlign="Center"><img alt="" src="AspNetVisualAssets/RatingIcons/EEAAirNOAQI.png" /></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_4",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_5",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell HorizontalAlign="Left"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_6",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        </asp:Table> 
        <h4><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_4_1",Request.QueryString.Get("culture"))%></h4>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_8",Request.QueryString.Get("culture"))%>
        </p>
        <asp:Table ID="concentrationAir"  runat="server" CssClass="tabelWaterIcons">
        <asp:TableHeaderRow><asp:TableHeaderCell HorizontalAlign="Left" ColumnSpan="6"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_HEADER", Request.QueryString.Get("culture"))%></asp:TableHeaderCell></asp:TableHeaderRow>
        <asp:TableRow><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_1_CELL_0",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_1_CELL_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_1_CELL_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_1_CELL_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_1_CELL_4",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_1_CELL_5",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_2_CELL_0",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_2_CELL_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_2_CELL_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_2_CELL_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_2_CELL_4",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_2_CELL_5",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_3_CELL_0",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_3_CELL_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_3_CELL_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_3_CELL_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_3_CELL_4",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_3_CELL_5",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        <asp:TableRow><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_4_CELL_0",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_4_CELL_1",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_4_CELL_2",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_4_CELL_3",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_4_CELL_4",Request.QueryString.Get("culture"))%></asp:TableCell><asp:TableCell><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_4_ROW_4_CELL_5",Request.QueryString.Get("culture"))%></asp:TableCell></asp:TableRow>
        </asp:Table>    
        </div>
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperPushpins"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_5",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_STATIONDATA", Request.QueryString.Get("culture"))%>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_STATIONDATA2", Request.QueryString.Get("culture"))%>
        </div>
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperAirModel"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_6",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>       
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_9",Request.QueryString.Get("culture"))%>
        </p>
        <p>
        <a href="http://wdc.dlr.de/data_products/projects/promote/IAQ/index.html">http://wdc.dlr.de/data_products/projects/promote/IAQ/index.html</a>
        </p>
        <h3><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_3", Request.QueryString.Get("culture"))%></h3>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_10", Request.QueryString.Get("culture"))%></p>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_11", Request.QueryString.Get("culture"))%></p>
        </div>
        <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_3", Request.QueryString.Get("culture"))%></h2>
        <ul>
        <li style="text-transform:uppercase">
            <asp:HyperLink runat="server" NavigateUrl="#hyperFeedbackIcons"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_7",Request.QueryString.Get("culture"))%></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" NavigateUrl="#hyperFeedbackLightMap"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_8",Request.QueryString.Get("culture"))%></asp:HyperLink>
        </li>
        </ul>
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperFeedbackIcons"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_7",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_12",Request.QueryString.Get("culture"))%>
        </p>
        </div>
        <asp:Table ID="table1" runat="server" CssClass="tabelWaterIcons">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell ColumnSpan="6" HorizontalAlign="Left">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_7", Request.QueryString.Get("culture"))%>
                </asp:TableHeaderCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <img alt="" src="AspNetVisualAssets/UserIcons/UserIcon1.png" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                   <img alt="" src="AspNetVisualAssets/UserIcons/UserIcon2.png" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                   <img alt="" src="AspNetVisualAssets/UserIcons/UserIcon3.png" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                   <img alt="" src="AspNetVisualAssets/UserIcons/UserIcon4.png" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                   <img alt="" src="AspNetVisualAssets/UserIcons/UserIcon5.png" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
               <asp:TableCell HorizontalAlign="Center">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_1",Request.QueryString.Get("culture"))%> 
               </asp:TableCell>
               <asp:TableCell HorizontalAlign="Center">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_2",Request.QueryString.Get("culture"))%>
               </asp:TableCell>
               <asp:TableCell HorizontalAlign="Center">
                   <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_3",Request.QueryString.Get("culture"))%>
               </asp:TableCell>
               <asp:TableCell HorizontalAlign="Center">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_4",Request.QueryString.Get("culture"))%>
               </asp:TableCell>
               <asp:TableCell HorizontalAlign="Center">
                   <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TABLE_3_IMAGETITLE_5",Request.QueryString.Get("culture"))%>
               </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
        <div>
        <h3><asp:HyperLink runat="server" ID="hyperFeedbackLightMap"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_HEADER_SUB_8",Request.QueryString.Get("culture"))%></asp:HyperLink></h3>
        <p>
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("HELP_TEXT_13",Request.QueryString.Get("culture"))%>
        </p>
        </div>
    </div>
    </form>
</body>
</html>
