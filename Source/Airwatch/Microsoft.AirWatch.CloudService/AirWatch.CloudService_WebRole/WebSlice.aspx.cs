﻿// <copyright file="WebSlice.aspx.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>06-07-2009</date>
// <summary>Site about page</summary>
namespace AirWatch.CloudService.WebRole
{
    using System;

    /// <summary>
    /// Web Slice host
    /// </summary>
    public partial class WebSlice : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
