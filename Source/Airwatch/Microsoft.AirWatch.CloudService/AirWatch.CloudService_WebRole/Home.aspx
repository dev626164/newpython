﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="AirWatch.CloudService.WebRole.Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%=AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_TITLE", this.UserCulture)%></title>
      <link href="style/Home.css" rel="stylesheet" type="text/css" />
    <link href="style/Pushpin.css" rel="stylesheet" type="text/css" />
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=<%= this.SupportedUserCulture %>"></script>
    <script type="text/javascript" src="scripts/default.js"></script>
    <script type="text/javascript" src="scripts/locations.js"></script>
    
  
</head>
<body onload="GetMap();" onresize="MapResize(false);">
<div id="body">
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
        <Services>
			 <asp:ServiceReference Path="~/ScriptableServices/ScriptablePushpinService.svc" />
        </Services>
    </asp:ScriptManager>
    <div>	
	<div id="banner">
	    <div id="eoeLogo">
		    <img  alt="Eye on Earth Logo" src="AspNetVisualAssets/BuiltOnWindowsAzure274x60.png"/>
        </div>
        <div id="contentRightPlaceholder">
            <div class="languageLinks">
                  <asp:DropDownList ID="LanguageSelect" runat="server" AutoPostBack="true" tabindex="4" />
                  <a href="ShortMessageService.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="5"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_SMS", UserCulture)%></a> |
                  <a href="About.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="6"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT", UserCulture)%></a> |
                  <a href="Help.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="7" ><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_DATA", UserCulture)%></a> |
                  <a href="Providers.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="8"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_PROVIDERS", UserCulture)%></a> |
                  <a href="Disclaimer.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="9" ><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_DISCLAIMER", UserCulture)%></a> 
            </div>
            <div class="eeaLogo">
                 <a href="http://www.eea.europa.eu"><img alt="EEA Logo" src="AspNetVisualAssets/EEALogo232x35.png" border="0" /></a>
            </div>
        </div>    
    </div>
   
    <div id="eoeControls">
        <div id="searchControl">
            <div style="float:left; padding-top:5px" >
                <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_SEARCH", UserCulture)%>
			</div>
			<div style="background-color: White; float:left; margin-left:10px;">
			    <div style="float:left; padding-left:2px;">
		            <dl class="dropdown">
                        <dt><input id="SearchLocationTextBox" type="text" style="width:135px; border-width:0px; padding-top:1px;" onkeypress="searchEnter(event)" tabindex="1" /></dt>
                        <dd>
                            <ul>
                            </ul>
                        </dd>
                    </dl>
		        </div>
    			
		        <div style="float:left; padding:1px;">
		            <img alt="Search" class="searchControlButton" src="AspNetVisualAssets/SearchButton.png" onkeypress="searchEnter(event)" onclick="javascript:FindLoc($('#SearchLocationTextBox').val());" tabindex="2" />
                </div>
            </div>
        </div>
        <div id="stationViews">
             <select id="changeStationTileLayerSelect" onchange="ChangeTileLayer('Station');" tabindex="3">
                <option value="None">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_STATIONDATA_NONE", UserCulture)%>
                </option>
                <option value="All">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_STATIONDATA_ALL_SIMPLE", UserCulture)%>                    
                </option>
                <option value="Air">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_STATIONDATA_AIR_SIMPLE", UserCulture)%></option>
                <option value="Water">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_STATIONDATA_WATER_SIMPLE", UserCulture)%></option>
            </select>
        </div> 
        <div id="userViews">
            <select id="changeUserFeedbackTileLayerSelect" onchange="ChangeTileLayer('UserFeedback');">
                <option value="None">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_USERFEEDBACK_NONE", UserCulture)%>
                </option>
                <option value="All">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_USERFEEDBACK_ALL_SIMPLE", UserCulture)%>                   
                </option>
                 <option value="Air">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_USERFEEDBACK_AIR_SIMPLE", UserCulture)%>
                    </option>
                <option value="Water">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_USERFEEDBACK_WATER_SIMPLE", UserCulture)%></option>
            </select>
        </div>
        <div id="heatMap"> 
            <input id="changeHeatMap" type="checkbox" onclick="javascript:ChangeHeatMapTileLayer();"/>
            <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPVIEWS_AIRHEATMAP", UserCulture) %>
        </div>   
        <div class="pushPin" >
            <img src="AspNetVisualAssets/PushPin1.png" title="<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPNAVIGATIONBAR_PUSHPIN_SIMPLE", UserCulture)%>" alt="<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPNAVIGATIONBAR_PUSHPIN_SIMPLE", UserCulture)%>" onmousedown="javascript:DragNewPushPin()"/>
        </div>
        <div class="deleteAll">
            <img src="AspNetVisualAssets/RemovePushPin3.png" title="<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPNAVIGATIONBAR_DELETEALL", UserCulture)%>" alt="<%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAPNAVIGATIONBAR_DELETEALL", UserCulture)%>" onclick="javascript:RemovePushPins()"/>
        </div>
    </div>
    </div>
    <div id="mapKeyDiv" class="mapKeyContainer">
            <div id="mapKeyBackground">
                <span id="AirModelKey" class="mapKeySpan" style="display:none">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_AIRMODEL", UserCulture)%>
                    <br />
                    <span class="mapKeyText"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", UserCulture)%></span>
                    <img src="AspNetVisualAssets/KeyIcons/DLRHeatMapGradientAsset.png" alt="Air model heat map" style="vertical-align: middle;"/>
                    <span class="mapKeyText"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_BAD", UserCulture)%></span>
                </span>
                <span id="UserFeedbackKey" class="mapKeySpan" style="display:none">
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_USERFEEDBACK", UserCulture)%>
                    <img src="AspNetVisualAssets/KeyIcons/Square.png" />
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_AIRSTATIONS", UserCulture)%>
                    <img src="AspNetVisualAssets/KeyIcons/Circle.png" />
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_WATERSTATIONS", UserCulture)%>
                     <a href="Help.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="5"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_MOREINFO", UserCulture)%></a>
                    <br />
                    <span class="mapKeyText">
                        <img src="AspNetVisualAssets/KeyIcons/1_Green.png" alt="Very Good"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_VERYGOOD", UserCulture)%>
                        <img src="AspNetVisualAssets/KeyIcons/2_LightGreen.png" alt="Good"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_GOOD", UserCulture)%>
                        <img src="AspNetVisualAssets/KeyIcons/3_Yellow.png" alt="Moderate"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_MODERATE", UserCulture)%>
                        <img src="AspNetVisualAssets/KeyIcons/4_Orange.png" alt="Bad"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_BAD", UserCulture)%>
                        <img src="AspNetVisualAssets/KeyIcons/5_Red.png" alt="Very Bad"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_VERYBAD", UserCulture)%>
                    </span>
                </span>
                <span id="StationsKey" class="mapKeySpan">
                   <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_STATIONS", UserCulture)%>
                   <a href="Help.aspx?culture=<%= UserCulture%>" target="_blank" tabindex="5"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_MOREINFO", UserCulture)%></a>
                   <br />
                    <span class="mapKeyText">
                        <img src="AspNetVisualAssets/KeyIcons/AirStation.png" alt="Air Stations"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_AIRSTATIONS", UserCulture)%>
                        <img src="AspNetVisualAssets/KeyIcons/WaterStation.png" alt="Water Stations"/>
                    <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("VIRTUALEARTHMAP_KEY_WATERSTATIONS", UserCulture)%>
                    </span>
                </span>
            </div>
        </div>
    <div id="mapContainer">
        <div id='myMap' class="map"></div>
    </div>
    <div id="LocationSelectDiv" class="LocationSelect" style="position:absolute; width: 240px; z-index: 1; background-color:White;" >
    <div id="errorMessage" style="display:none">
        <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("RESULTSFLYOUT_ERROR", UserCulture)%>
    </div>
    <script type="text/javascript">
           var userCulture = "<%= this.UserCulture %>";
    </script> 
</form>
</div>    
<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_project=5312027; 
var sc_invisible=1; 
var sc_partition=59; 
var sc_click_stat=1; 
var sc_security="c32919b5"; 
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
<noscript>
   <div class="statcounter">
      <a title="counter customisable" href="http://www.statcounter.com/free_hit_counter.html" target="_blank">
         <img class="statcounter" src="http://c.statcounter.com/5312027/0/c32919b5/1/" alt="counter customisable" />
      </a>
   </div>
</noscript>
<!-- End of StatCounter Code -->
</body>
</html>
