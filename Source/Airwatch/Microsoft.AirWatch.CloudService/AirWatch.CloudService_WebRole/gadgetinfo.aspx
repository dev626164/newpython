﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gadgetinfo.aspx.cs" Inherits="AirWatch.CloudService.WebRole.GadgetInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT_HEADER", Request.QueryString.Get("culture"))%></title>
    <style type="text/css">
        code
        {
            font-family: Monaco, "Courier New" , Courier, monospace;
            font-size: 120%;
            color: Black;
            background-color: #e9efcd;
            padding: 0 0.1em;
        }
        body
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            margin: 0 0 0.75em 0;
            line-height: 1.5em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2>
             <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_ABOUT_HEADER", Request.QueryString.Get("culture"))%>
           </h2>
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_DESCRIPTION", Request.QueryString.Get("culture"))%>
        <br />
        <br />
        <code>&lt;iframe name=&quot;eoe_frame&quot; src=&quot;http://eyeonearth.eu/Default.aspx?<b>latitude=<i>55.676&amp;</i>longitude=<i>12.569&amp;</i>zoom=<i>10&amp;</i>showpushpin=<i>true</i></b>&quot;
            style=&quot;<b>height:<i>500px</i>; width: <i>875px</i></b>;<b>&quot;</b> /&gt;<br />
        </code>
        <br />
        <br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_PARAMS", Request.QueryString.Get("culture"))%><br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_LONGITUDE", Request.QueryString.Get("culture"))%><br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_LATITUDE", Request.QueryString.Get("culture"))%><br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_ZOOM", Request.QueryString.Get("culture"))%><br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_SHOWPUSHPIN", Request.QueryString.Get("culture"))%><br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_WIDTHHEIGHT", Request.QueryString.Get("culture"))%>
        <br />
        <br />
         <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("MAINPAGE_WEBMASTER_EXAMPLE", Request.QueryString.Get("culture"))%>
         <br />
         <br />
        <iframe name="eoe_frame" src="http://eyeonearth.eu/Default.aspx?latitude=55.676&longitude=12.569&zoom=15&showpushpin=true"
            style="height: 500px; width: 900px;" />
    </div>
    </form>
</body>
</html>
