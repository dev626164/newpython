﻿// <copyright file="gadgetinfo.aspx.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>11-11-2009</date>
// <summary>Partial class code-behind for gadgetinfo.aspx</summary>
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Gadget info page
    /// </summary>
    public partial class GadgetInfo : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
