﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AirWatch.CloudService.WebRole.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv=”X-UA-Compatible” content=”IE=8″ />
    <meta name="title" content="Eye on Earth" />
    <meta name="description" content="Eye On Earth is a global observatory for environmental factors.  It monitors Air and Water quality." />
    <link rel="image_src" href="~/AspNetVisualAssets/EyeOnEarthLogoBW.png" />
    <title><%=AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("COMMON_TITLE", this.UserCulture)%></title>
    
    <script type="text/javascript" src="Silverlight.js"></script>
    <script type="text/javascript" src="scripts/Silverlight.logger.js"></script>
    <script type="text/javascript">

        // Configure logging parameters
        SLS.appName = "EyeOnEarth";
        SLS.appVersion = "1.0";
        SLS.hqPlayerSlVersion = "3.0.40818.0";

        var PromptRestart = "<div><p>Please restart your browser.</p></div>";
                
        function checkOS() {
            if (navigator.appVersion.indexOf("X11") != -1 || navigator.appVersion.indexOf("Linux") != -1) {
                javascript: SLS.logInstallFlow(5);                
                window.location = "home.aspx";
            }
        }
        
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

////            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

////            errMsg += "Code: " + iErrorCode + "    \n";
////            errMsg += "Category: " + errorType + "       \n";
////            errMsg += "Message: " + args.ErrorMessage + "     \n";

////            if (errorType == "ParserError") {
////                errMsg += "File: " + args.xamlFile + "     \n";
////                errMsg += "Line: " + args.lineNumber + "     \n";
////                errMsg += "Position: " + args.charPosition + "     \n";
////            }
////            else if (errorType == "RuntimeError") {
////                if (args.lineNumber != 0) {
////                    errMsg += "Line: " + args.lineNumber + "     \n";
////                    errMsg += "Position: " + args.charPosition + "     \n";
////                }
////                errMsg += "MethodName: " + args.methodName + "     \n";
////            }

            var errMsg = "Sorry, Eye On Earth is currently experiencing problems. Please try again later."

            throw new Error(errMsg);
        }
    </script>
    
    <style type="text/css">
    html, body {
	    height: 100%;
	    overflow:hidden;
    }
    body {
	    padding: 0;
	    margin: 0;
    }
    #silverlightControlHost {
	    height: 100%;
	    text-align:center;
    }
    </style>
</head>
<!-- <body onload="checkOS();"> !-->
<body onload="checkOS();" >
    <a id="externalAnchor" style="display:none;"></a>
    <input id="externalButton" type="button" onclick="window.open(document.getElementById('externalAnchor').href)" style="display:none;" />

    <form id="form1" runat="server" style="height:100%;">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <object data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
		      <param name="source" value="ClientBin/AirWatch.UI.xap"/>
		      <param name="onError" value="onSilverlightError" />
		      <param name="background" value="white" />
		      <param name="minRuntimeVersion" value="3.0.40624.0" />
		      <param name="autoUpgrade" value="true" />
		      <param name="initparams" value="ipAddress=127.0.0.1" />
		      <param name="onload" value="onSilverlightLoad" />
		      <param name="splashScreenSource" value="SplashScreen.xaml" />	 
    		
                <div style="background-position: center center; background: black; height:100%; vertical-align: middle; display: block; background-image: url('AspNetVisualAssets/eyeonearthnoslbackground.png'); background-repeat: no-repeat; background-position:center center">
		           <div style="height:15%"></div>
		           <span style="color: #FFFFFF; font-family: Verdana; text-align: center; margin-right: auto; margin-left: auto; display: block;">
		             <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SPLASHSCREEN_WELCOMETO", this.UserCulture)%>
		           </span>		     
		           <img alt="" src="AspNetVisualAssets/EyeOnEarthLogoV6370x71.png" style="margin-left: auto; margin-right: auto; display: block;" />
		           <br />        		     
		           <span style="color: #FFFFFF; font-family: Verdana; text-align: center; margin-right: auto; margin-left: auto; display: block; font-size:12px; padding-left:20px; padding-right:20px">
		             <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SPLASHSCREEN_SILVERLIGHTREQUIRED", this.UserCulture)%>
		           </span>
		           <br />
		           <a onclick='InstallClicked()' href="http://go.microsoft.com/fwlink/?LinkID=149156&v=3.0.40624.0" style="text-decoration:none; vertical-align:middle;"><img src="http://go.microsoft.com/fwlink/?LinkId=108181" 
                      alt="Get Microsoft Silverlight" 
                      style="margin-right: auto; margin-left: auto; border-width:0; display: block;"/></a>
                   <br />
                   <span style="color: #FFFFFF; font-family: Verdana; font-size:9pt; text-align: center; margin-right: auto; margin-left: auto; display: block;">
		             <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SPLASHSCREEN_VIEWHTMLSITE", this.UserCulture)%>&nbsp;<a href="Home.aspx"><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SPLASHSCREEN_CLICKHERE", this.UserCulture)%></a>.
		           </span>
                </div>
	        </object>	   
	    <iframe id="Iframe1" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe>
        <asp:HiddenField ID="LangCulture" runat="server" />
        <asp:HiddenField ID="SessionUser" runat="server" />
        <asp:HiddenField ID="UserAddress" runat="server" />
    </form>
<!-- Start of StatCounter Code -->
<script type="text/javascript">
    var sc_project = 5312027;
    var sc_invisible = 1;
    var sc_partition = 59;
    var sc_click_stat = 1;
    var sc_security = "c32919b5"; 
</script>

<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
<noscript>
   <div class="statcounter">
      <a title="counter customisable" href="http://www.statcounter.com/free_hit_counter.html" target="_blank">
         <img class="statcounter" src="http://c.statcounter.com/5312027/0/c32919b5/1/" alt="counter customisable" />
      </a>
   </div>
</noscript>
<!-- End of StatCounter Code -->
</body>
</html>
