﻿//-----------------------------------------------------------------------
// <copyright file="Developers.aspx.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>17/01/2010</date>
// <summary>Developers Page</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    /// <summary>
    /// Developers Page
    /// </summary>
    public partial class Developers : System.Web.UI.Page
    {        
    }
}
