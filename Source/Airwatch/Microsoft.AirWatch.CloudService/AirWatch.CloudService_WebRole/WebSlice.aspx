﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebSlice.aspx.cs" Inherits="AirWatch.CloudService.WebRole.WebSlice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title></title>    
</head>
<body>   
    <div id="spotlight" class="hslice" width="320" height="240">
        <div class="entry-title">
            <h1>Eye On Earth</h1>
            <iframe id="eoeIframe" width="320" height="240" src="http://localhost:81/default.aspx?gadget=true&location=43.5684,6.1811&header=Air%20Quality" />
        </div>
        <div class="entry-content">
            <asp:Label runat="server" ID="placeholder" /></div>
        <a rel="entry-content" href="http://localhost:81/default.aspx?gadget=true&location=43.5684,6.1811&header=Air%20Quality" style="display: none;" />
        <a rel="bookmark" target="_blank" href="http://www.eyeonearth.eu" style="display: none;" /><span class="ttl"
                style="display: none;">15</span>
    </div>
</body>
</html>
