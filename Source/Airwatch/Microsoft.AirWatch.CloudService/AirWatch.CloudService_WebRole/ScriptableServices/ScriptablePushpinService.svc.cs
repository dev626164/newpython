﻿//-----------------------------------------------------------------------
// <copyright file="ScriptablePushpinService.svc.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>14 July 2009</date>
// <summary>Web Service to provide a scriptable method to access pushpin data</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.ScriptableServices
{
    #region Using
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Channels;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using AirWatch.CloudService.WebRole.AspNetUserControls;
    using Microsoft.AirWatch.Common;
    using Microsoft.AirWatch.Core.Data;
    #endregion

    /// <summary>
    /// Web Service to provide a scriptable method to access pushpin data
    /// </summary>
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ScriptablePushpinService
    {
        /// <summary>
        /// Radius the user is restricted from rating between
        /// </summary>
        private const double RatingRadius = 1;

        /// <summary>
        /// Number of ratings a user can do within the thresholds
        /// </summary>
        private const int NumberOfRatingsInThresholds = 1;

        /// <summary>
        /// Threshold of the date in which the user is restricted from rating between
        /// </summary>
        private static TimeSpan dateThreshold = new TimeSpan(0, 0, 0, 1);

        /// <summary>
        /// Prevents a default instance of the <see cref="ScriptablePushpinService"/> class from being created.
        /// </summary>
        private ScriptablePushpinService()
        {
        }

        /// <summary>
        /// Gets the station descriptions for map boundaries.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <param name="stationPurpose">The station purpose.</param>
        /// <param name="stationAmount">The station amount.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Array of min info about the station</returns>
        [OperationContract]
        public Collection<StationContainer> GetStationDescriptionsForMapBoundaries(double north, double east, double south, double west, string stationPurpose, int stationAmount, int zoomLevel)
        {
            Microsoft.AirWatch.Core.Data.StationMinimal[] stationArray;

            if (string.IsNullOrEmpty(stationPurpose))
            {
                stationArray = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.StationService.GetStationsForBoundary(north, east, south, west);
            }
            else
            {
                stationArray = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.StationService.GetStationsForBoundary(north, east, south, west, stationPurpose);
            }

            Collection<StationContainer> stationCollection = new Collection<StationContainer>();

            foreach (Microsoft.AirWatch.Core.Data.StationMinimal station in stationArray)
            {
                stationCollection.Add(new StationContainer() { StationId = station.StationId, Latitude = station.Latitude, Longitude = station.Longitude, EuropeanCode = station.EuropeanCode, StationPurpose = station.StationPurpose.ToString(), QualityIndex = station.QualityIndex });
            }

            if (stationCollection.Count > stationAmount && zoomLevel < 12)
            {
                stationCollection = null;
            }

            return stationCollection;
        }

        /// <summary>
        /// Gets the pushpin HTML.
        /// </summary>
        /// <param name="id">The pushpin id.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>Pushpin flyout html</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "We need to not have this static in order to call from JS"), OperationContract]
        public HtmlContainer GetPushpinHtml(int id, double latitude, double longitude, string culture)
        {
            Page page = new Page();
            PushpinControl pushpinControl = (PushpinControl)page.LoadControl("~/AspNetUserControls/PushpinControl.ascx");
            pushpinControl.InitControl(id, latitude, longitude, culture);
            string html = LoadControl(pushpinControl);
            return new HtmlContainer()
            {
                Id = id,
                Html = html,
                HasData = pushpinControl.HasData,
            };
        }

        /// <summary>
        /// Gets the station HTML.
        /// </summary>
        /// <param name="id">The id of the call.</param>
        /// <param name="stationId">The station id.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>Html station container</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "We need to not have this static in order to call from JS"), OperationContract]
        public HtmlContainer GetStationHtml(int id, int stationId, string culture)
        {
            Page page = new Page();
            PushpinControl pushpinControl = (PushpinControl)page.LoadControl("~/AspNetUserControls/PushpinControl.ascx");
            pushpinControl.InitControl(id, stationId, culture);
            string html = LoadControl(pushpinControl);
            return new HtmlContainer()
            {
                Id = id,
                Html = html,
                Rating = pushpinControl.Rating,
                StationType = pushpinControl.StationTypeString,
                HasData = pushpinControl.HasData
            };
        }

        /// <summary>
        /// Sets the rating.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="rating">The rating.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userAddress">The ip address of the user.</param>
        /// <param name="wordQualifiers">The word qualifiers.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "We need to not have this static in order to call from JS"), OperationContract]
        public void SetRating(double latitude, double longitude, int rating, string ratingTarget, string userAddress, int[] wordQualifiers)
        {
            // get the IP address from client and take a hash of it
            RemoteEndpointMessageProperty clientEndpoint = OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            string identifierHash = SHA256Hash(clientEndpoint.Address);

            // check it hasn't been rated within dateThreshold and RatingRadius degrees
            bool canRate = ValidateUser(latitude, longitude, ratingTarget, identifierHash);

            if (canRate)
            {
                System.Data.Objects.DataClasses.EntityCollection<Microsoft.AirWatch.Core.Data.WordQualifier> wordQualifierColelction = new System.Data.Objects.DataClasses.EntityCollection<Microsoft.AirWatch.Core.Data.WordQualifier>();

                foreach (int i in wordQualifiers)
                {
                    wordQualifierColelction.Add(new Microsoft.AirWatch.Core.Data.WordQualifier() { WordId = i });
                }

                Microsoft.AirWatch.Core.Data.UserRating userRating = new Microsoft.AirWatch.Core.Data.UserRating()
                {
                    Latitude = (decimal)latitude,
                    Longitude = (decimal)longitude,
                    Rating = rating,
                    RatingTarget = ratingTarget,
                    IPAddress = identifierHash,
                    WordQualifier = wordQualifierColelction,
                    RatingTime = DateTime.UtcNow
                };

                Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.RatingService.RateLocation(userRating);
            }
        }

        /// <summary>
        /// Get back an SHA512 hash of the string value given
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Base64 SHA512 hash of the value</returns>
        private static string SHA256Hash(string value)
        {
            SHA256 hashProvider = new SHA256CryptoServiceProvider();
            byte[] hashVal = hashProvider.ComputeHash(Encoding.Unicode.GetBytes(value));

            return Convert.ToBase64String(hashVal);
        }

        /// <summary>
        /// Validates the user with the thresholds.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>true if they are allowed to rate, false if they are not</returns>
        private static bool ValidateUser(double latitude, double longitude, string ratingTarget, string userIdentifier)
        {
            DateTime now = DateTime.UtcNow;
            DateTime thresholdAgo = now.Subtract(dateThreshold);

            Collection<UserRating> ratings = Microsoft.AirWatch.Core.ApplicationServices.ServiceProvider.RatingService.GetAllRatings(latitude, longitude, ratingTarget, RatingRadius, thresholdAgo, now);

            int ratingsByUserInArea = (from r in ratings
                                       where r.IPAddress == userIdentifier
                                       select r).Count();

            return ratingsByUserInArea < NumberOfRatingsInThresholds;
        }

        /// <summary>
        /// Cleans the HTML.
        /// </summary>
        /// <param name="html">The HTML requested</param>
        /// <returns>"form"-less html</returns>
        private static string CleanHtml(string html)
        {
            return Regex.Replace(html, @"<[/]?(form)[^>]*?>", String.Empty, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Loads the specified control.
        /// </summary>
        /// <param name="userControl">The user control.</param>
        /// <returns>The HTML as string</returns>
        private static string LoadControl(UserControl userControl)
        {
            // Create instance of the page control
            Page page = new Page();

            // Disabled ViewState- If required
            userControl.EnableViewState = false;

            // Form control is mandatory on page control to process User Controls
            HtmlForm form = new HtmlForm();

            // Add user control to the form
            form.Controls.Add(userControl);

            // Add form to the page 
            page.Controls.Add(form);

            // Write the control Html to text writer
            StringWriter textWriter = new StringWriter(System.Globalization.CultureInfo.InvariantCulture);

            // Execute page on server 
            HttpContext.Current.Server.Execute(page, textWriter, false);

            // Clean up code and return html
            return CleanHtml(textWriter.ToString());
        }
    }
}
