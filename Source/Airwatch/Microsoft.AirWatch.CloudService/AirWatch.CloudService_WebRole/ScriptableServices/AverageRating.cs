﻿//-----------------------------------------------------------------------
// <copyright file="AverageRating.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>14/08/09</date>
// <summary>Average rating temporary class instead of POCO</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.ScriptableServices
{
    /// <summary>
    /// Temporary POCO average rating class
    /// </summary>
    public class AverageRating
    {
        /// <summary>
        /// Gets or sets the rating qualifiers.
        /// </summary>
        /// <value>The rating qualifiers.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Justification = "temporary poco")]
        public RatingQualifierIdentifiers[] RatingQualifiers { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        /// <value>The rating.</value>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get; set; }
    }
}
