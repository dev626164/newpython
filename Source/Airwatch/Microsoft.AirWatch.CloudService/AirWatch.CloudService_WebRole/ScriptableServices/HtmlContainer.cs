﻿//-----------------------------------------------------------------------
// <copyright file="HtmlContainer.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Stuart McCarthy</author>
// <email>smccar@microsoft.com</email>
// <date>06/08/09</date>
// <summary>Class for returning HTML requests to JS client</summary>
//----------------------------------------------------------------------- 

namespace AirWatch.CloudService.WebRole
{
    /// <summary>
    /// Class for returning HTML requests to JS client
    /// </summary>
    public class HtmlContainer
    {
        /// <summary>
        /// Backing field for Id
        /// </summary>
        private int id;

        /// <summary>
        /// Backing field for Html
        /// </summary>
        private string html;

        /// <summary>
        /// Backing field for HasData
        /// </summary>
        private bool hasData;

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id of the pushpin.</value>
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the HTML.
        /// </summary>
        /// <value>The HTML for the pushpin.</value>
        public string Html
        {
            get { return this.html; }
            set { this.html = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has data.
        /// </summary>
        /// <value><c>true</c> if this instance has data; otherwise, <c>false</c>.</value>
        public bool HasData
        {
            get { return this.hasData; }
            set { this.hasData = value; }
        }

        /// <summary>
        /// Gets or sets the type of the station.
        /// </summary>
        /// <value>The type of the station.</value>
        public string StationType { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        /// <value>The rating.</value>
        public int Rating { get; set; }
    }
}
