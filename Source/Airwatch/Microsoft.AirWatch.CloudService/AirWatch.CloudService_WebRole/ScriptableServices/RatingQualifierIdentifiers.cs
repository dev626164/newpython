﻿//-----------------------------------------------------------------------
// <copyright file="RatingQualifierIdentifiers.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>14/08/09</date>
// <summary>RatingQualifierIdentifiers temporary class instead of POCO</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole.ScriptableServices
{
    /// <summary>
    /// RatingQualifierIdentifiers temporary class instead of POCO
    /// </summary>
    public class RatingQualifierIdentifiers
    {
        /// <summary>
        /// Gets or sets the word identifier.
        /// </summary>
        /// <value>The word identifier.</value>
        public string WordIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the percentage.
        /// </summary>
        /// <value>The percentage.</value>
        public double Percentage { get; set; }
    }
}
