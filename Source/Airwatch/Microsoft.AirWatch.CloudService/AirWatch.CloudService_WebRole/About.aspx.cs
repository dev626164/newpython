﻿// <copyright file="About.aspx.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>06-07-2009</date>
// <summary>Site about page</summary>

namespace AirWatch.CloudService.WebRole
{
    using System;

    /// <summary>
    /// Class containing about page functions
    /// </summary>
    public partial class About : System.Web.UI.Page
    {
        /// <summary>
        /// Event handling page load event
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
