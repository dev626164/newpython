﻿/// <reference path="jquery-1.3.2-vsdoc2.js"/>
/// <reference path="default.js"/>

var userRatings = new Array(false, false, false, false, false);
var InfoBoxOriginalHide = null;
var ratedPushpin = null;
var ratedStation = null;

function SetUpRating(pushpinId, stationType, stationId) {
    var rated = false;
    
    if (stationId != 0) {
        var stationCookie = getCookie("rated" + stationType + stationId);

        if (stationCookie != "") {
            rated = true;
        }
    }
    else {

        var currentPushpin = pushpins[pushpinId];
        var latitude = currentPushpin.Latitude;
        var longitude = currentPushpin.Longitude;

        var pushpinCookie = getCookie("rated" + latitude + ":" + longitude);
        
        if (pushpinCookie != "") {
            rated = true;
        }
         else {
             if (document.cookie.length > 0) {
                 var pattern = /(Air|pushpin)\d+=([-]*\d+[.]*\d*%3A[-]*\d+[.]*\d*)/g
                var results = document.cookie.match(pattern);
               
                for (var longlatjoin in results) {
                    if(longlatjoin.search(/^\d+$/) != -1){
                        var x = results[parseInt(longlatjoin)].match(/[-]*\d+[.]*\d*%3A[-]*\d+[.]*\d*/);
                        var longlat = x[0].split("%3A");
                        var ratingArea = 0.03;
                        if ((longlat[1] >= (longitude - ratingArea)) && (longlat[1] <= (longitude + ratingArea)) && (longlat[0] >= (latitude - ratingArea)) && (longlat[0] <= (latitude + ratingArea))) {
                            rated = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    $("#PushpinHomeView").css("display", "none");
    if (!rated) {
        $("#PushpinRateView").css("display", "block");
    }
    else {
        $("#PushpinRatedView").css("display", "block");
    }
    $("#PushpinHistoryView").css("display", "none");
    $("#wordList table:eq(1)").attr("width", "100%");

    $("#wordList input:checkbox").attr("checked", false);
    $("#RateButton").css("display", "none");
    $("#FakeRateButton").css("display", "block");
    userRatings = new Array(false, false, false, false, false);
    UserRatingSetOpacity();

    $("#ratingIcons img").click(
            function() {
                var value = parseInt($(this).attr("id").charAt(10));

                UserRatingSetOpacity();
                $(this).css("opacity", 1.0);

                userRatings = new Array(false, false, false, false, false);
                userRatings[value - 1] = true;
                CheckAbleToRate();
            }
       );

        $("#ratingIcons img").hover(function() {
            var value = parseInt($(this).attr("id").charAt(10));
            if (!userRatings[value - 1]) {
                $(this).css("opacity", 1.0);
            }
        }, function() {
            var value = parseInt($(this).attr("id").charAt(10));
            if (!userRatings[value - 1]) {
                $(this).css("opacity", 0.5);
            }
        });

        $("#wordList input:checkbox").click(function() {
            CheckAbleToRate();
        });
}

function CheckAbleToRate() {
    if($("#wordList input:checked").length > 0)
    {
        for (icon in userRatings) {
            if (userRatings[icon] == true) {
                $("#FakeRateButton").css("display", "none"); 
                $("#RateButton").css("display", "block");  
                return;
            }
        }
    }

    $("#RateButton").css("display", "none");
    $("#FakeRateButton").css("display", "block");       
}

function UserRatingSetOpacity() {
    $("#ratingIcons img").each(function() {
        $(this).css("opacity", 0.5);
    });
}

function SetUpHistory() {
    $("#PushpinHomeView").css("display", "none");
    $("#PushpinRateView").css("display", "none");
    $("#PushpinRatedView").css("display", "none");
    $("#PushpinHistoryView").css("display", "block");
}

function CancelRate() {
    $("#PushpinHomeView").css("display", "block");
    $("#PushpinRatedView").css("display", "none");
    $("#PushpinRateView").css("display", "none");
    $("#PushpinHistoryView").css("display", "none");
}

function RatePushpin(pushpinId, ratingWordsDiv, stationType, stationId, userIp) {
    var rating = -1;
    for (var i = 0; i < userRatings.length; i++) {
        if (userRatings[i]) {
            rating = i + 1;
            break;
        }
    }

    if (rating != -1) {
        if ($("#wordList input:checked").length > 0) {
            var words = checkBoxes();
            ratePushPinId = pushpinId;
            rateStationId = stationId;

            if (rateStationId != 0) {
                ratedStation = stationId;
                setCookie("rated" + stationType + stationId, stationHash[pushpinId].Latitude + ":" + stationHash[pushpinId].Longitude, 7);
                ScriptablePushpinService.SetRating(stationHash[pushpinId].Latitude, stationHash[pushpinId].Longitude, rating, stationType, userIp, words, RatePushpin_OnSuccess, RatePushpin_OnFailed);
                CancelRate();
            }
            else {
                ratedPushpin = pushpinId;
                setCookie("rated" + pushpins[pushpinId].Latitude + ":" + pushpins[pushpinId].Longitude, pushpins[pushpinId].Latitude + ":" + pushpins[pushpinId].Longitude, 7);
                ScriptablePushpinService.SetRating(pushpins[pushpinId].Latitude, pushpins[pushpinId].Longitude, rating, stationType, userIp, words, RatePushpin_OnSuccess, RatePushpin_OnFailed);
                CancelRate();
            }
        }
        else {
            // need to display an error message.
        }
    }
}
var tempStation;

function RatePushpin_OnSuccess() {
    if (ratedPushpin != null) {
        GetCurrentPushpin(ratedPushpin, pushpins[ratedPushpin].Latitude, pushpins[ratedPushpin].Longitude);
    }

    if (ratedStation != null) {
        tempStation = stationHash[ratedStation];
        GetStation(ratedStation);
        ratedStation == null;
    }
    
    stationHash = new Object();

    for (var i = 0; i < pushpins.length; i++) {
        GetPushpin(i, pushpins[i].Latitude, pushpins[i].Longitude)
    }
    
    ratedPushpin = null;
}

function RatePushpin_OnFailed() {
    ClosePushpinControl();
    // need to display message
}

function ClosePushpinControl() {
    enableInfoBoxHide();
    map.HideInfoBox();
}

function RemovePushpin(pushpinId) {
    ClosePushpinControl();
    setCookie("pushpin" + pushpinId, "deleted", 365);
    map.DeleteShape(pushpins[pushpinId]);
}

function checkBoxes() {
    var words = new Array();
    $("#wordList input:checked").each(function(i) {
        words[i] = $(this).attr("id").match(/\d$/)[0];
    });
    return words;
}
var hideDisabled = false;
function disableInfoBoxHide() {
    if (!hideDisabled) {
        InfoBoxOriginalHide = window.ero.hide;
        window.ero.hide = function(a) { return; }
        hideDisabled = true;
    }
}

function enableInfoBoxHide() {
    if (hideDisabled) {
        window.ero.hide = InfoBoxOriginalHide;
        InfoBoxOriginalHide = null;
        hideDisabled = false;
    }
}