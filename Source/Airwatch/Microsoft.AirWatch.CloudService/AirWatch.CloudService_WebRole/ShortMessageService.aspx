﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShortMessageService.aspx.cs" Inherits="AirWatch.CloudService.WebRole.ShortMessageService" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h1><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("FLYOUTPANEL_SMSSERVICE_HEADER", Request.QueryString.Get("culture"))%></h1>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_TEXT2", Request.QueryString.Get("culture"))%></p>
        <h1><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_AIRNOTIFICATIONS_STEPS_TEXT4", Request.QueryString.Get("culture"))%></h1>
        <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES", Request.QueryString.Get("culture"))%></h2>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_TEXT1", Request.QueryString.Get("culture"))%></p>
        <h3><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS", Request.QueryString.Get("culture"))%></h3>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT1", Request.QueryString.Get("culture"))%></p>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT2", Request.QueryString.Get("culture"))%></p>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT3", Request.QueryString.Get("culture"))%>&nbsp;
           <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT4", Request.QueryString.Get("culture"))%>&nbsp;
           <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT5", Request.QueryString.Get("culture"))%>&nbsp;
           <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT6", Request.QueryString.Get("culture"))%>&nbsp;
           <%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT7", Request.QueryString.Get("culture"))%>&nbsp;
        </p>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT8", Request.QueryString.Get("culture"))%>&nbsp;</p>
        <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT9", Request.QueryString.Get("culture"))%>&nbsp;</p>           
         <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_INSTANTUPDATES_STEPS_TEXT10", Request.QueryString.Get("culture"))%>&nbsp;</p>
         <h2><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_TERMSOFSERVICE", Request.QueryString.Get("culture"))%></h2>
         <p><%= AirWatch.CloudService.WebRole.LocalizationHelper.Instance.LocalizeString("SMS_HELP_TERMSOFSERVICE_TEXT1", Request.QueryString.Get("culture"))%></p>
         <a href="mailto:dataprotectionofficer@eea.europa.eu">dataprotectionofficer@eea.europa.eu</a>
        
    </form>
</body>
</html>
