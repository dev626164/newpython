﻿//-----------------------------------------------------------------------
// <copyright file="PushpinRatingHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>26 October 2009</date>
// <summary>Helper class for pushpins</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Helper class for pushpins
    /// </summary>
    public static class PushpinRatingHelper
    {
        /// <summary>
        /// Sets the pushpin ratings word cloud.
        /// </summary>
        /// <param name="pushpin">The pushpin.</param>
        public static void SetPushpinRatingsWordCloud(Pushpin pushpin)
        {
            Collection<RatingQualifierIdentifiers> rqi = new Collection<RatingQualifierIdentifiers>();

            if (pushpin.AverageRating != null)
            {
                var ratingQualifiers = pushpin.AverageRating.RatingQualifierIdentifiers.ToDictionary(ratingQualifier => ratingQualifier.WordIdentifier);

                for (int i = 0; i < 8; i++)
                {
                    if (pushpin.AverageRating.RatingQualifierIdentifiers != null)
                    {
                        if (ratingQualifiers.ContainsKey(i.ToString(CultureInfo.CurrentCulture)))
                        {
                            rqi.Add(new RatingQualifierIdentifiers()
                            {
                                WordIdentifier = "RATELOCATION_AIR_" + ratingQualifiers[i.ToString(CultureInfo.CurrentCulture)].WordIdentifier,
                                Percentage = ratingQualifiers[i.ToString(CultureInfo.CurrentCulture)].Percentage
                            });
                        }
                        else
                        {
                            rqi.Add(new RatingQualifierIdentifiers()
                            {
                                WordIdentifier = "RATELOCATION_AIR_" + i.ToString(CultureInfo.CurrentCulture),
                                Percentage = 0
                            });
                        }
                    }
                    else
                    {
                        rqi.Add(new RatingQualifierIdentifiers()
                        {
                            WordIdentifier = "RATELOCATION_AIR_" + i.ToString(CultureInfo.CurrentCulture),
                            Percentage = 0
                        });
                    }
                }

                pushpin.AverageRating.RatingQualifierIdentifiers.Clear();
                pushpin.AverageRating.RatingQualifierIdentifiers = rqi;
            }
        }
    }
}
