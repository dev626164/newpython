﻿//-----------------------------------------------------------------------
// <copyright file="HistoricalSeasonConverter.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <summary>Historical Season Converter</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.ObjectModel;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Historical Season Converter.
    /// </summary>
    public static class HistoricalSeasonConverter
    {
        /// <summary>
        /// Converts the specified source collection.
        /// </summary>
        /// <param name="sourceCollection">The source collection.</param>
        /// <returns>Seasonal Data Only.</returns>
        public static Collection<StationMeasurement> Convert(EntityCollection<StationMeasurement> sourceCollection)
        {
            Collection<StationMeasurement> targetCollection = new Collection<StationMeasurement>();

            int sourceCount = 0;

            if (sourceCollection != null)
            {
                sourceCount = sourceCollection.Count;
            }

            if (sourceCount == 0 || sourceCollection.First().Measurement.Timestamp.HasValue == false)
            {
                return targetCollection;
            }

            int year = sourceCollection.Last().Measurement.Timestamp.Value.Year;

            foreach (StationMeasurement s in sourceCollection)
            {
                DateTime? sourceTimestamp = s.Measurement.Timestamp;

                if (sourceTimestamp.HasValue && sourceTimestamp.Value.Year == year
                    && !(sourceTimestamp.Value.Month == 12 && sourceTimestamp.Value.Day == 31))
                {
                    s.AggregatedUserRating = s.AggregatedUserRating.GetValueOrDefault();
                    s.AggregatedUserRatingCount = s.AggregatedUserRatingCount.GetValueOrDefault();
                    s.Measurement.Value = s.Measurement.Value.GetValueOrDefault();
                    targetCollection.Add(s);
                }
            }

            return targetCollection;
        }
    }
}