﻿//-----------------------------------------------------------------------
// <copyright file="HistoricalYearlyConverter.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <summary>Historical Yearly Converter.</summary>
//----------------------------------------------------------------------- 
namespace AirWatch.CloudService.WebRole
{
    using System;
    using System.Collections.ObjectModel;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Historical Yearly Converter.
    /// </summary>
    public static class HistoricalYearlyConverter
    {
        /// <summary>
        /// Converts the specified source collection.
        /// </summary>
        /// <param name="sourceCollection">The source collection.</param>
        /// <returns>Collection of Station Measurements for the yearly data.</returns>
        public static Collection<StationMeasurement> Convert(EntityCollection<StationMeasurement> sourceCollection)
        {
            Collection<StationMeasurement> targetCollection = new Collection<StationMeasurement>();

            int sourceCount = 0;

            if (sourceCollection != null)
            {
                sourceCount = sourceCollection.Count;
            }

            int startYear = DateTime.Now.Year - 1;

            if (sourceCount > 0 && sourceCollection.Last().Measurement.Timestamp.Value.Year == startYear)
            {
                // if the latest reading is other than 31/12 or if the latest is 31/12 but there is another from same year
                if (sourceCollection.Last().Measurement.Timestamp.Value.Day != 31
                && sourceCollection.Last().Measurement.Timestamp.Value.Month != 12)
                {
                    startYear--;
                }
                else if (sourceCount > 1 && sourceCollection.ElementAt(sourceCount - 2).Measurement.Timestamp.Value.Year == startYear)
                {
                    startYear--;
                }
            }

            for (int currentYear = startYear; currentYear >= 1990; currentYear--)
            {
                bool found = false;

                if (sourceCollection != null)
                {
                    for (int i = 0; i < sourceCount; i++)
                    {
                        if (sourceCollection.ElementAt(i).Measurement.Timestamp.HasValue && sourceCollection.ElementAt(i).Measurement.Timestamp.Value.Year == currentYear)
                        {
                            targetCollection.Add(sourceCollection.ElementAt(i));
                            found = true;
                            break;
                        }
                    }
                }

                if (found == false)
                {
                    targetCollection.Add(new StationMeasurement
                    {
                        Measurement = new Measurement
                        {
                            Timestamp = new DateTime?(new DateTime(currentYear, 12, 31))
                        },
                        AggregatedUserRatingCount = 0
                    });
                }
            }

            foreach (var station in targetCollection)
            {
                station.AggregatedUserRating = station.AggregatedUserRating.GetValueOrDefault();
                station.AggregatedUserRatingCount = station.AggregatedUserRatingCount.GetValueOrDefault();
                station.Measurement.Value = station.Measurement.Value.GetValueOrDefault();
            }

            if (targetCollection.Count > 0)
            {
                targetCollection.Reverse();
            }

            return targetCollection;
        }
    }
}
