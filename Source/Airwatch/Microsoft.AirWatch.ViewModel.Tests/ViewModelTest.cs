﻿// <copyright file="ViewModelTest.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>30-04-2009</date>
// <summary>Unit tests for the AirWatch ViewModel</summary>
namespace Microsoft.AirWatch.ViewModel.Tests
{
    using Microsoft.AirWatch.ViewModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the AirWatch ViewModel 
    /// </summary>
    [TestClass]
    public class ViewModelTests
    {
        /// <summary>
        /// The context the test is run in
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the ViewModelTests class
        /// </summary>
        public ViewModelTests()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// You can use the following additional attributes as you write your tests:
        ////
        //// Use ClassInitialize to run code before running the first test in the class
        //// [ClassInitialize()]
        //// public static void MyClassInitialize(TestContext testContext) { }
        ////
        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }
        ////
        #endregion

        /// <summary>
        /// Sample test method
        /// </summary>
        [TestMethod]
        public void TestMethod()
        {
        }
    }
}
