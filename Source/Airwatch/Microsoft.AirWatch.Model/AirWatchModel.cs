﻿//-----------------------------------------------------------------------
// <copyright file="AirWatchModel.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>17-06-2009</date>
// <summary>Actual concrete implementation AirWatchModel.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.IO;
    using System.IO.IsolatedStorage;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Windows.Browser;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.AirWatch.Model.Data;
    using Microsoft.AirWatch.UICommon;

    /// <summary>
    /// Actual concrete implementation AirWatchModel.
    /// </summary>
    public class AirWatchModel : IAirWatchModel
    {
        /// <summary>
        /// Backing storage constant for looking to see if we are running out of storage on the client
        /// </summary>
        public const long MinStorageSize = 1024;

        /// <summary>
        /// Backing storage constant for the isolated storage increase amount.
        /// </summary>
        private const long QuotaIncreaseSize = 1024 * 1024 * 10;

        /// <summary>
        /// The maximum number of web services failures before we go to the error screen.
        /// </summary>
        private const short MaxErrorCount = 3;

        /// <summary>
        /// Backing static field for Instance static property.
        /// </summary>
        private static AirWatchModel instance;

        /// <summary>
        /// A dictionary of webservice methods and the number of times they've failed
        /// </summary>
        private Dictionary<string, int> webServiceErrorCount = new Dictionary<string, int>();

        /// <summary>
        /// Language Service Client
        /// </summary>
        private LanguageServiceClient languageServiceClient = new LanguageServiceClient();        

        /// <summary>
        /// Prevents a default instance of the <see cref="AirWatchModel"/> class from being created.
        /// </summary>
        private AirWatchModel()
        {
            this.CurrentLanguageDictionary = this.GetIsolatedStorageLanguage();
            this.languageServiceClient.GetLanguageCompleted += new EventHandler<GetLanguageCompletedEventArgs>(this.GetLanguageCompleted);
            this.languageServiceClient.GetLanguagesCompleted += new EventHandler<GetLanguagesCompletedEventArgs>(this.GetLanguagesCompleted);
            this.languageServiceClient.GetTimestampCompleted += new EventHandler<GetTimestampCompletedEventArgs>(this.GetTimeStampCompleted);
        }        

        /// <summary>
        /// Occurs when [languages loaded].
        /// </summary>
        public event EventHandler ChangeCultureCompleted;

        /// <summary>
        /// Occurs when [languages loaded].
        /// </summary>
        public event EventHandler<LanguagesReceivedEventArgs> GetCulturesCompleted;

        /// <summary>
        /// Occurs when [isolated storage user retrieved].
        /// </summary>
        public event EventHandler<UserLoadedEventArgs> IsolatedStorageUserLoaded;

        /// <summary>
        /// Occurs when [isolated storage pushpins retrieved].
        /// </summary>
        public event EventHandler<IsolatedStoragePushpinEventArgs> IsolatedStoragePushpinsRetrieved;

        /// <summary>
        /// Occurs when [isolated storage ratings retrieved].
        /// </summary>
        public event EventHandler<IsolatedStorageRatingEventArgs> IsolatedStorageRatingsRetrieved;

        /// <summary>
        /// Occurs when [token retrieved].
        /// </summary>
        public event EventHandler<TokenRetrievedEventArgs> MapTokenRetrieved;

        /// <summary>
        /// Occurs when [search results retrieved].
        /// </summary>
        public event EventHandler<SearchResultsEventArgs> SearchResultsRetrieved;

        /// <summary>
        /// Occurs when [address results retrieved].
        /// </summary>
        public event EventHandler<AddressResultsEventArgs> AddressResultsRetrieved;

        /// <summary>
        /// Occurs when [pushpin loaded].
        /// </summary>
        public event EventHandler<PushpinLoadedEventArgs> PushpinLoaded;

        /// <summary>
        /// Occurs when [station loaded].
        /// </summary>
        public event EventHandler<StationLoadedEventArgs> StationLoaded;

        /// <summary>
        /// Occurs when [station area loaded].
        /// </summary>
        public event EventHandler<StationAreaLoadedEventArgs> StationAreaLoaded;

        /// <summary>
        /// Occurs when [isolated storage probed].
        /// </summary>
        public event EventHandler<ProbeIsolatedStorageEventArgs> IsolatedStorageProbed;

        /// <summary>
        /// Occurs when [home pushpin loaded].
        /// </summary>
        public event EventHandler<PushpinLoadedEventArgs> HomePushpinLoaded;

        /// <summary>
        /// Occurs when [home water station loaded].
        /// </summary>
        public event EventHandler<StationLoadedEventArgs> HomeWaterStationLoaded;

        /// <summary>
        /// Occurs when [get tile layers completed].
        /// </summary>
        public event EventHandler<GetTileLayersCompletedEventArgs> GetTileLayersCompleted;

        /// <summary>
        /// Occurs when [station rated].
        /// </summary>
        public event EventHandler<StationLoadedEventArgs> StationRated;

        /// <summary>
        /// Occurs when [pushpin rated].
        /// </summary>
        public event EventHandler<PushpinLoadedEventArgs> PushpinRated;

        /// <summary>
        /// Occurs when an [error occured] on a web service call.
        /// </summary>
        public event EventHandler ErrorOccurred;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static AirWatchModel Instance
        {
            get
            {
                if (AirWatchModel.instance == null)
                {
                    AirWatchModel.instance = new AirWatchModel();
                }

                return AirWatchModel.instance;
            }
        }        

        #region Languages
        
        /// <summary>
        /// Gets or sets the current culture.
        /// </summary>
        /// <value>The current culture.</value>
        public string CurrentCulture { get; set; }

        /// <summary>
        /// Gets or sets the current language dictionary.
        /// </summary>
        /// <value>The current language dictionary.</value>
        public Dictionary<string, string> CurrentLanguageDictionary { get; set; }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <param name="culture">The culture (eg. en-GB).</param>
        public void GetLanguage(string culture)
        {
            this.CurrentCulture = culture;
            this.languageServiceClient.GetLanguageAsync(culture);
        }

        #endregion

        #region IsolatedStorage

        /// <summary>
        /// Gets the isolated storage entry.
        /// </summary>
        public void GetIsolatedStorageEntry()
        {
            IsolatedStorageEntry storageEntry = new IsolatedStorageEntry();            

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("AirWatchStorage.txt", FileMode.OpenOrCreate, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(typeof(IsolatedStorageEntry));
                        try
                        {
                            storageEntry = serialiser.ReadObject(isfs) as IsolatedStorageEntry;
                        }
                        catch (SerializationException)
                        {
                        }
                    }
                }
            }
            catch (IsolatedStorageException)
            {
                if (this.IsolatedStorageProbed != null)
                {
                    this.IsolatedStorageProbed(this, new ProbeIsolatedStorageEventArgs(false));
                }
            }
            catch (IOException)
            {
            }

            if (!string.IsNullOrEmpty(storageEntry.Culture))
            {
                this.CurrentCulture = storageEntry.Culture;
            }
              
            this.languageServiceClient.GetLanguagesAsync();                      
                
            User user = new User();  

            Location loc;

            if (storageEntry.Longitude == null || storageEntry.Latitude == null)
            {
                loc = new Location();
            }
            else
            {
                loc = new Location()
                {
                    Longitude = (decimal)storageEntry.Longitude,
                    Latitude = (decimal)storageEntry.Latitude
                };
            }

            user.UserHomePushpin = new UserFavoritePushpin()
            {
                Pushpin = new Pushpin()
                {
                    Location = loc,
                    FriendlyName = storageEntry.FriendlyName
                }
            };

            if (this.IsolatedStorageUserLoaded != null)
            {
                this.IsolatedStorageUserLoaded(this, new UserLoadedEventArgs(user, user != null));
            }
        }        

        /// <summary>
        /// Saves the isolated storage entry.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="friendlyName">Name of the friendly.</param>
        public void SaveIsolatedStorageEntry(decimal? longitude, decimal? latitude, string friendlyName)
        {
            if (!string.IsNullOrEmpty(this.CurrentCulture))
            {
                this.SaveIsolatedStorageEntry(this.CurrentCulture, longitude, latitude, friendlyName);
            }
            else
            {
                this.SaveIsolatedStorageEntry("en-GB", longitude, latitude, friendlyName);
            }
        }

        /// <summary>
        /// Saves the isolated storage entry.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="friendlyName">Name of the friendly.</param>
        public void SaveIsolatedStorageEntry(string culture, decimal? longitude, decimal? latitude, string friendlyName)
        {
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("AirWatchStorage.txt", FileMode.Create, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(
                            typeof(
                            IsolatedStorageEntry));

                        serialiser.WriteObject(isfs, new IsolatedStorageEntry() { Culture = culture, FriendlyName = friendlyName, Longitude = longitude, Latitude = latitude });
                    }
                }
            }
            catch (IsolatedStorageException)
            {
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Saves the isolated storage entry.
        /// </summary>
        /// <param name="languageDictionary">The language dictionary.</param>
        public void SaveLanguageDictionary(Dictionary<string, string> languageDictionary)
        {
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("LanguageDictionary.txt", FileMode.Create, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(
                            typeof(
                            Dictionary<string, string>));

                        serialiser.WriteObject(isfs, languageDictionary);
                    }                    
                }  
                
                this.languageServiceClient.GetTimestampAsync(languageDictionary["CULTURE_ID"]);
            }
            catch (IsolatedStorageException)
            {
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Saves the isolated storage pushpins.
        /// </summary>
        /// <param name="pushpinList">The pushpin list.</param>
        public void SaveIsolatedStoragePushpins(ObservableCollection<Pushpin> pushpinList)
        {
            ObservableCollection<Pushpin> newPushpinList = new ObservableCollection<Pushpin>();

            foreach (Pushpin p in pushpinList)
            {
                if (p != null)
                {
                    newPushpinList.Add(p);
                }
            }

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (MemoryStream memoryStream = new MemoryStream(0))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(
                                typeof(
                                IsolatedStoragePushpins));

                        serialiser.WriteObject(memoryStream, new IsolatedStoragePushpins() { PushpinCollection = pushpinList });

                        if (this.ProbeIsolatedStorage(memoryStream.ToArray().Length, true))
                        {
                            using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("AirWatchPushpinStorage.txt", FileMode.Create, isf))
                            {
                                serialiser.WriteObject(isfs, new IsolatedStoragePushpins() { PushpinCollection = pushpinList });
                            }
                        }
                    }                
                }
            }
            catch (IsolatedStorageException)
            {                
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Gets the isolated storage pushpins.
        /// </summary>
        public void GetIsolatedStoragePushpins()
        {
            IsolatedStoragePushpins storagePushpinEntry = new IsolatedStoragePushpins();

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("AirWatchPushpinStorage.txt", FileMode.OpenOrCreate, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(typeof(IsolatedStoragePushpins));
                        try
                        {
                            storagePushpinEntry = serialiser.ReadObject(isfs) as IsolatedStoragePushpins;
                        }
                        catch (SerializationException)
                        {
                        }
                    }
                }

                if (this.IsolatedStoragePushpinsRetrieved != null)
                {
                    this.IsolatedStoragePushpinsRetrieved(this, new IsolatedStoragePushpinEventArgs(storagePushpinEntry));
                }
            }
            catch (IsolatedStorageException)
            {
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Saves the isolated storage ratings.
        /// </summary>
        /// <param name="ratingList">The rating list.</param>
        public void SaveIsolatedStorageRatings(ObservableCollection<Rating> ratingList)
        {
            ObservableCollection<Rating> newRatingList = new ObservableCollection<Rating>();

            foreach (Rating r in ratingList)
            {
                if (r != null && r.Date.AddDays(6).CompareTo(DateTime.Today) > 0)
                {
                    newRatingList.Add(r);
                }
            }

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("AirWatchRatingStorage.txt", FileMode.Create, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(
                            typeof(
                            IsolatedStorageRatings));

                        serialiser.WriteObject(isfs, new IsolatedStorageRatings() { RatingCollection = newRatingList });
                    }
                }
            }
            catch (IsolatedStorageException)
            {
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Gets the isolated storage ratings.
        /// </summary>
        public void GetIsolatedStorageRatings()
        {
            IsolatedStorageRatings storageRatingsEntry = new IsolatedStorageRatings();

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("AirWatchRatingStorage.txt", FileMode.OpenOrCreate, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(typeof(IsolatedStorageRatings));
                        try
                        {
                            storageRatingsEntry = serialiser.ReadObject(isfs) as IsolatedStorageRatings;
                        }
                        catch (SerializationException)
                        {
                        }
                    }
                }
                
                // only keeps entries made today
                ObservableCollection<Rating> newRatingCollection = new ObservableCollection<Rating>();

                if (storageRatingsEntry != null && storageRatingsEntry.RatingCollection != null)
                {
                    foreach (Rating r in storageRatingsEntry.RatingCollection)
                    {
                        if (r != null && r.Date.AddDays(6).CompareTo(DateTime.Today) > 0)
                        {
                            newRatingCollection.Add(r);
                        }
                    }

                    storageRatingsEntry.RatingCollection = newRatingCollection;

                    if (this.IsolatedStorageRatingsRetrieved != null)
                    {
                        this.IsolatedStorageRatingsRetrieved(this, new IsolatedStorageRatingEventArgs(storageRatingsEntry));
                    }
                }
            }
            catch (IsolatedStorageException)
            {
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Gets the isolated storage language.
        /// </summary>
        /// <returns>The locally stored dictionary</returns>
        public Dictionary<string, string> GetIsolatedStorageLanguage()
        {
            Dictionary<string, string> languageDictionary = new Dictionary<string, string>();

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("LanguageDictionary.txt", FileMode.OpenOrCreate, isf))
                    {
                        DataContractSerializer serialiser = new DataContractSerializer(typeof(Dictionary<string, string>));
                        try
                        {
                            languageDictionary = serialiser.ReadObject(isfs) as Dictionary<string, string>;

                            if (languageDictionary != null && languageDictionary.ContainsKey("CULTURE_ID"))
                            {
                                this.CurrentCulture = languageDictionary["CULTURE_ID"];                                
                            }  

                            this.languageServiceClient.GetTimestampAsync(this.CurrentCulture);
                        }
                        catch (SerializationException)
                        {
                        }
                    }
                }
            }
            catch (IsolatedStorageException)
            {
            }
            catch (IOException)
            {
            }

            return languageDictionary;
        }

        /// <summary>
        /// Probes the isolated storage.
        /// </summary>
        /// <param name="bytesRequired">The number of bytes required.</param>
        /// <param name="increaseQuota">if set to <c>true</c> [increase quota].</param>
        /// <returns>True if there is enough space left, false otherwise.</returns>
        public bool ProbeIsolatedStorage(long bytesRequired, bool increaseQuota)
        {
            bool success = false;

            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // check to see if there is space left in the isolated storage
                    if (isf.AvailableFreeSpace > bytesRequired)
                    {
                        success = true;                        
                    }
                    else
                    {
                        if (increaseQuota)
                        {
                            success = isf.IncreaseQuotaTo(isf.Quota + QuotaIncreaseSize);
                        }
                    }
                }
            }
            catch (IsolatedStorageException)
            {
                success = false;
            }
            finally
            {
                if (increaseQuota)
                {
                    if (this.IsolatedStorageProbed != null)
                    {
                        this.IsolatedStorageProbed(this, new ProbeIsolatedStorageEventArgs(success));
                    }
                }
            }

            return success;
        }

        #endregion

        #region Map

        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <param name="clientIPAddress">client ip address</param>
        public void GetMapToken(string clientIPAddress)
        {
            SearchServiceClient client = new SearchServiceClient();
            client.GetTokenCompleted += new EventHandler<GetTokenCompletedEventArgs>(this.GetMapTokenCompleted);
            client.GetTokenAsync(clientIPAddress);
            client.CloseAsync();
        }

        /// <summary>
        /// Gets the searchresults.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture</param>
        public void GetSearchResults(string query, string token, string culture)
        {
            SearchServiceClient client = new SearchServiceClient();
            client.GetSearchResultsCompleted += new EventHandler<GetSearchResultsCompletedEventArgs>(this.GetSearchResultsCompleted);
            client.GetSearchResultsAsync(query, token, culture);
            client.CloseAsync();
        }

        /// <summary>
        /// Gets the address.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="id">The id if the pushpin or station.</param>
        /// <param name="pinType">Type of the pin.</param>
        public void GetAddress(double longitude, double latitude, string token, string culture, int id, string pinType)
        {
            SearchServiceClient client = new SearchServiceClient();
            client.GetReverseGeocodeCompleted += new EventHandler<GetReverseGeocodeCompletedEventArgs>(this.GetReverseGeocodeCompleted);

            object[] userState = new object[] { id, pinType };

            client.GetReverseGeocodeAsync(token, culture, longitude, latitude, userState);
            client.CloseAsync();
        }

        /// <summary>
        /// Gets the tile layers.
        /// </summary>
        public void GetTileLayers()
        {
            string endpoint = string.Format(CultureInfo.InvariantCulture, "http://{0}:{1}/", HtmlPage.Document.DocumentUri.Host, HtmlPage.Document.DocumentUri.Port); 

            ObservableCollection<TileLayer> tileLayers = new ObservableCollection<TileLayer>();

            TileLayer airModel = new TileLayer();
            airModel.Identifier = "Air Heatmap";
            airModel.Uri = new Uri(endpoint + "{quadkey}.heatmap");

            tileLayers.Add(airModel);

            TileLayer airStations = new TileLayer();
            airStations.Identifier = "Air Stations";
            airStations.Uri = new Uri(endpoint + "{quadkey}.airlightmap");

            tileLayers.Add(airStations);

            TileLayer waterStations = new TileLayer();
            waterStations.Identifier = "Water Stations";
            waterStations.Uri = new Uri(endpoint + "{quadkey}.waterlightmap");

            tileLayers.Add(waterStations);

            TileLayer airUserFeedback = new TileLayer();
            airUserFeedback.Identifier = "Air User Feedback";
            airUserFeedback.Uri = new Uri(endpoint + "{quadkey}.userfeedbackairlightmap");

            tileLayers.Add(airUserFeedback);

            TileLayer waterUserFeedback = new TileLayer();
            waterUserFeedback.Identifier = "Water User Feedback";
            waterUserFeedback.Uri = new Uri(endpoint + "{quadkey}.userfeedbackwaterlightmap");

            tileLayers.Add(waterUserFeedback);

            GetTileLayersCompletedEventArgs getTileLayersCompletedEventArgs = new GetTileLayersCompletedEventArgs(tileLayers, true);

            this.GetTileLayersCompleted(this, getTileLayersCompletedEventArgs);
        }

        #endregion

        #region Pushpins

        /// <summary>
        /// Gets the pushpin.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="pushpinId">The pushpin id.</param>
        public void GetPushpin(decimal latitude, decimal longitude, int pushpinId)
        {
            PushpinServiceClient pushpinServiceClient = new PushpinServiceClient();
            pushpinServiceClient.GetPushpinCompleted += new EventHandler<GetPushpinCompletedEventArgs>(this.GetPushpinCompleted);
            pushpinServiceClient.GetPushpinAsync(longitude, latitude, pushpinId);
        }

        /// <summary>
        /// Gets the home pushpin.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        public void GetHomePushpin(decimal latitude, decimal longitude)
        {
            PushpinServiceClient pushpinServiceClient = new PushpinServiceClient();
            pushpinServiceClient.GetPushpinCompleted += new EventHandler<GetPushpinCompletedEventArgs>(this.GetHomePushpinCompleted);
            pushpinServiceClient.GetPushpinAsync(longitude, latitude);
        }

        #endregion

        #region Stations

        /// <summary>
        /// Gets the station descriptions for map boundaries.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <param name="numberToReturn">The number to return.</param>
        public void GetStationDescriptionsForMapBoundaries(double north, double east, double south, double west, int numberToReturn)
        {
            StationServiceClient stationServiceClient = new StationServiceClient();
            stationServiceClient.GetStationDescriptionsForMapBoundariesCompleted += new EventHandler<GetStationDescriptionsForMapBoundariesCompletedEventArgs>(this.GetStationDescriptionsForMapBoundariesCompleted);
            stationServiceClient.GetStationDescriptionsForMapBoundariesAsync(north, east, south, west, numberToReturn);
        }

        /// <summary>
        /// Gets the station by code.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        public void GetStationById(int stationId)
        {
            StationServiceClient stationServiceClient = new StationServiceClient();
            stationServiceClient.GetStationByCodeCompleted += new EventHandler<GetStationByCodeCompletedEventArgs>(this.GetStationByCodeCompleted);
            stationServiceClient.GetStationByCodeAsync(stationId);
        }

        /// <summary>
        /// Gets the home water station.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        public void GetHomeWaterStation(decimal latitude, decimal longitude)
        {
            StationServiceClient stationServiceClient = new StationServiceClient();
            stationServiceClient.GetClosestStationCompleted += new EventHandler<GetClosestStationCompletedEventArgs>(this.GetClosestStationCompleted);
            stationServiceClient.GetClosestStationAsync((double)longitude, (double)latitude, "Water");
        }

        #endregion

        #region Rating

        /// <summary>
        /// Sets the rating.
        /// </summary>
        /// <param name="pushpinId">The pushpin id.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="rating">The rating.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userAddress">The user address.</param>
        /// <param name="wordQualifiers">The word qualifiers.</param>
        public void SetPushpinRating(int pushpinId, double latitude, double longitude, int rating, string ratingTarget, string userAddress, bool[] wordQualifiers)
        {
            RatingServiceClient client = new RatingServiceClient();
            client.SetRatingCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(this.SetPushpinRatingCompleted);
            client.SetRatingAsync(
                latitude, 
                longitude, 
                rating, 
                ratingTarget, 
                userAddress, 
                wordQualifiers,
                new Pushpin { PushpinId = pushpinId, Location = new Location { Latitude = (decimal)latitude, Longitude = (decimal)longitude } });
            client.CloseAsync();
        }

        /// <summary>
        /// Sets the station rating.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="rating">The rating.</param>
        /// <param name="ratingTarget">The rating target.</param>
        /// <param name="userAddress">The user address.</param>
        /// <param name="wordQualifiers">The word qualifiers.</param>
        public void SetStationRating(int stationId, double latitude, double longitude, int rating, string ratingTarget, string userAddress, bool[] wordQualifiers)
        {
            RatingServiceClient client = new RatingServiceClient();
            client.SetRatingCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(this.SetStationRatingCompleted);
            client.SetRatingAsync(
                latitude,
                longitude,
                rating,
                ratingTarget,
                userAddress,
                wordQualifiers,
                new Station { StationId = stationId, Location = new Location { Latitude = (decimal)latitude, Longitude = (decimal)longitude }, StationPurpose = ratingTarget });
            client.CloseAsync();
        }

        #endregion

        #region Languages callbacks

        /// <summary>
        /// Gets the languages completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="GetLanguagesCompletedEventArgs"/> instance containing the event data.</param>
        private void GetLanguagesCompleted(object sender, GetLanguagesCompletedEventArgs eventArgs)
        {
            try
            {
                if (this.GetCulturesCompleted != null && eventArgs != null)
                {
                    if (this.CurrentLanguageDictionary == null || this.CurrentLanguageDictionary.Count == 0)
                    {
                        string browserCulturesString = HtmlPage.Document.GetElementById("LangCulture").GetAttribute("value").ToLower(CultureInfo.InvariantCulture);

                        string[] browserCultures = browserCulturesString.Split(new char[] { ',' });

                        string browserCulture = string.Empty;

                        foreach (string s in browserCultures)
                        {
                            string[] nonPercentageLanguages = s.Split(new char[] { ';' });

                            if (!string.IsNullOrEmpty(nonPercentageLanguages[0]))
                            {
                                LanguageDescription languageDescription = eventArgs.Result.SingleOrDefault(p => p.LanguageCode.ToLower(CultureInfo.InvariantCulture).Substring(0, 2) == nonPercentageLanguages[0].Substring(0, 2));

                                if (languageDescription != null)
                                {
                                    browserCulture = languageDescription.LanguageCode;
                                    break;
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(this.CurrentCulture) && !string.IsNullOrEmpty(browserCulture))
                        {
                            this.CurrentCulture = browserCulture;
                        }
                        else
                        {
                            this.CurrentCulture = "en-GB";
                        }

                        this.ResetRetry("GetLanguages");

                        this.languageServiceClient.GetLanguageAsync(this.CurrentCulture);
                    }

                    this.GetCulturesCompleted(sender, new LanguagesReceivedEventArgs(new Collection<LanguageDescription>(eventArgs.Result.ToList())));
                }
                else
                {
                    if (this.HandleRetry("GetLanguages"))
                    {
                        this.languageServiceClient.GetLanguagesAsync();
                    }
                    else
                    {
                        if (this.ErrorOccurred != null)
                        {
                            this.ErrorOccurred(this, new EventArgs());
                        }
                    }
                }
            }
            catch
            {
                if (this.HandleRetry("GetLanguages"))
                {
                    this.languageServiceClient.GetLanguagesAsync();
                }
                else
                {
                    if (this.ErrorOccurred != null)
                    {
                        this.ErrorOccurred(this, new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Handles the GetLanguageCompleted event of the client control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GetLanguageCompletedEventArgs"/> instance containing the event data.</param>
        private void GetLanguageCompleted(object sender, GetLanguageCompletedEventArgs e)
        {
            if (this.ChangeCultureCompleted != null)
            {
                if (e != null && e.Error == null && e.Result != null)
                {
                    this.CurrentLanguageDictionary = e.Result;
                    this.SaveLanguageDictionary(e.Result);
                    this.ResetRetry("GetLanguage");
                    this.ChangeCultureCompleted(sender, new EventArgs());
                }
                else
                {
                    if (this.HandleRetry("GetLanguage"))
                    {
                        if (!string.IsNullOrEmpty(this.CurrentCulture))
                        {
                            this.languageServiceClient.GetLanguageAsync(this.CurrentCulture);
                        }
                    }
                    else
                    {
                        this.ErrorOccurred(this, new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Gets the time stamp completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GetTimestampCompletedEventArgs"/> instance containing the event data.</param>
        private void GetTimeStampCompleted(object sender, GetTimestampCompletedEventArgs e)
        {
            DateTime lastUpdatedLanguageDate;

            if (e != null && e.Error == null && e.Result != null)
            {
                // Open or create the LastUpdatedLanguageDate.txt file and deserialize it.
                using (IsolatedStorageFile dateIsf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream dateIsfs = new IsolatedStorageFileStream("LastUpdatedLanguageDate.txt", FileMode.OpenOrCreate, dateIsf))
                    {
                        DataContractSerializer dateSerialiser = new DataContractSerializer(typeof(DateTime));

                        try
                        {
                            lastUpdatedLanguageDate = (DateTime)dateSerialiser.ReadObject(dateIsfs);
                        }
                        catch (SerializationException)
                        {
                            // If there is a deserialization exception ensure that when we check our existing date
                            // against the webservice result.
                            lastUpdatedLanguageDate = DateTime.MinValue;
                        }
                    }
                }

                // If we have an hold language get the dictionary for the current culture and save the timestamp.
                if (e.Result != lastUpdatedLanguageDate)
                {
                    this.GetLanguage(this.CurrentCulture);

                    using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("LastUpdatedLanguageDate.txt", FileMode.Create, isf))
                        {
                            DataContractSerializer serialiser = new DataContractSerializer(typeof(DateTime));

                            serialiser.WriteObject(isfs, e.Result);
                        }
                    }
                }

                this.ResetRetry("GetTimeStamp");
            }
            else
            {
                // If there is an error in the service result check if we should try again.
                if (this.HandleRetry("GetTimeStamp"))
                {
                    this.languageServiceClient.GetTimestampAsync(this.CurrentCulture);
                }
                else
                {
                    // If we have reached the retry limit on 'GetTimeStamp' try lets try just getting the language.
                    if (this.HandleRetry("GetLanguage"))
                    {
                        this.languageServiceClient.GetLanguageAsync(this.CurrentCulture);
                    }
                    else
                    {
                        if (this.ErrorOccurred != null)
                        {
                            this.ErrorOccurred(this, new EventArgs());
                        }
                    }
                }
            }
        }

        #endregion Languages callbacks

        #region Map callbacks

        /// <summary>
        /// Handles the GetTokenCompleted event of the client control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GetTokenCompletedEventArgs"/> instance containing the event data.</param>
        private void GetMapTokenCompleted(object sender, GetTokenCompletedEventArgs e)
        {
            if (this.MapTokenRetrieved != null && e != null && e.Error == null && e.Result != null)
            {
                this.MapTokenRetrieved(this, new TokenRetrievedEventArgs(e.Result, e.Error != null));
            }
        }

        /// <summary>
        /// Handles the GetSearchResultsCompleted event of the client control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GetSearchResultsCompletedEventArgs"/> instance containing the event data.</param>
        private void GetSearchResultsCompleted(object sender, GetSearchResultsCompletedEventArgs e)
        {
            if (this.SearchResultsRetrieved != null && e != null && e.Error == null && e.Result != null)
            {
                this.SearchResultsRetrieved(this, new SearchResultsEventArgs(e.Result));
            }
        }

        /// <summary>
        /// Handles the GetReverseGeocodeCompleted event of the client control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GetReverseGeocodeCompletedEventArgs"/> instance containing the event data.</param>
        private void GetReverseGeocodeCompleted(object sender, GetReverseGeocodeCompletedEventArgs e)
        {
            if (this.AddressResultsRetrieved != null)
            {
                if (e != null && e.Error == null && e.Result != null && e.UserState != null)
                {
                    object[] userState = e.UserState as object[];

                    if (userState != null && (int?)userState[0] != null)
                    {
                        int? id = userState[0] as int?;
                        string pinType = userState[1].ToString();

                        if (id.HasValue && string.IsNullOrEmpty(pinType) == false)
                        {
                            if (e != null && e.Error == null && e.Result != null)
                            {
                                this.AddressResultsRetrieved(this, new AddressResultsEventArgs(e.Result, id.Value, pinType, true));
                            }
                            else
                            {
                                this.AddressResultsRetrieved(this, new AddressResultsEventArgs(e.Result, id.Value, pinType, false));
                            }
                        }
                    }
                    else
                    {
                        this.AddressResultsRetrieved(this, new AddressResultsEventArgs(e.Result, -1, string.Empty, false));
                    }
                }
            }
        }

        #endregion

        #region Pushpins callbacks

        /// <summary>
        /// Handles the GetPushpinCompleted event of the userServiceClient control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GetPushpinCompletedEventArgs"/> instance containing the event data.</param>
        private void GetPushpinCompleted(object sender, GetPushpinCompletedEventArgs e)
        {
            if (this.PushpinLoaded != null)
            {
                if (e != null && e.Error == null && e.Result != null)
                {
                    Pushpin pushpin = e.Result;
                    pushpin.PushpinId = (int)e.UserState;
                    this.PushpinLoaded(this, new PushpinLoadedEventArgs(pushpin));
                }                
            }
        }

        /// <summary>
        /// Handles the GetHomePushpinCompleted event of the UserServiceClient control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GetPushpinCompletedEventArgs"/> instance containing the event data.</param>
        private void GetHomePushpinCompleted(object sender, GetPushpinCompletedEventArgs e)
        {
            if (this.HomePushpinLoaded != null && e != null && e.Error == null)
            {
                this.HomePushpinLoaded(this, new PushpinLoadedEventArgs(e.Result));
            }
        }

        /// <summary>
        /// Sets the pushpin rating completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.AsyncCompletedEventArgs"/> instance containing the event data.</param>
        private void SetPushpinRatingCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (this.PushpinRated != null && e != null && e.Error == null && e.UserState != null)
            {
                this.PushpinRated.Invoke(this, new PushpinLoadedEventArgs(e.UserState as Pushpin));
            }
        }

        #endregion Pushpins callbacks

        #region Stations callbacks

        /// <summary>
        /// Gets the station descriptions for map boundaries completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GetStationDescriptionsForMapBoundariesCompletedEventArgs"/> instance containing the event data.</param>
        private void GetStationDescriptionsForMapBoundariesCompleted(object sender, GetStationDescriptionsForMapBoundariesCompletedEventArgs e)
        {
            if (this.StationAreaLoaded != null && e != null && e.Error == null && e.Result != null)
            {
                this.StationAreaLoaded(this, new StationAreaLoadedEventArgs(e.Result));
            }
        }

        /// <summary>
        /// Gets the station by code completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GetStationByCodeCompletedEventArgs"/> instance containing the event data.</param>
        private void GetStationByCodeCompleted(object sender, GetStationByCodeCompletedEventArgs e)
        {
            if (this.StationLoaded != null && e != null && e.Error == null && e.Result != null)
            {
                this.StationLoaded(this, new StationLoadedEventArgs(e.Result));
            }
        }

        /// <summary>
        /// Gets the closest station completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GetClosestStationCompletedEventArgs"/> instance containing the event data.</param>
        private void GetClosestStationCompleted(object sender, GetClosestStationCompletedEventArgs e)
        {
            if (this.HomeWaterStationLoaded != null && e != null && e.Error == null)
            {
                this.HomeWaterStationLoaded(this, new StationLoadedEventArgs(e.Result));
            }
        }

        /// <summary>
        /// Sets the station rating completed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.AsyncCompletedEventArgs"/> instance containing the event data.</param>
        private void SetStationRatingCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (this.StationRated != null && e != null && e.Error == null && e.UserState != null)
            {
                this.StationRated.Invoke(this, new StationLoadedEventArgs(e.UserState as Station));
            }
        }

        #endregion

        /// <summary>
        /// Resets the retry count for a particular method.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        private void ResetRetry(string methodName)
        {
            if (this.webServiceErrorCount.ContainsKey(methodName))
            {
                this.webServiceErrorCount.Remove(methodName);
            }
        }

        /// <summary>
        /// Handles the retry.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>Whether the web service call should be retried.</returns>
        private bool HandleRetry(string methodName)
        {
            bool retry = false;

            if (this.webServiceErrorCount.ContainsKey(methodName))
            {
                if (this.webServiceErrorCount[methodName] < AirWatchModel.MaxErrorCount)
                {
                    this.webServiceErrorCount[methodName]++;
                    retry = true;
                }
            }
            else
            {
                this.webServiceErrorCount.Add(methodName, 1);
                retry = true;
            }

            return retry;
        }
    }
}
