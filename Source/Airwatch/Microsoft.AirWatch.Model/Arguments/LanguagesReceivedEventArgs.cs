﻿//-----------------------------------------------------------------------
// <copyright file="LanguagesReceivedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>17/07/09</date>
// <summary>LanguagesReceived Event Args</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.ObjectModel;
    using Microsoft.AirWatch.AzureTables;

    /// <summary>
    /// Language received event args
    /// </summary>
    public class LanguagesReceivedEventArgs : EventArgs
    {
        /// <summary>
        /// Backing field for LanguageDescriptions property.
        /// </summary>
        private Collection<LanguageDescription> languageDescriptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguagesReceivedEventArgs"/> class.
        /// </summary>
        /// <param name="languageDescriptions">The language descriptions.</param>
        public LanguagesReceivedEventArgs(Collection<LanguageDescription> languageDescriptions)
        {
            this.languageDescriptions = languageDescriptions;
        }

        /// <summary>
        /// Gets the language descriptions.
        /// </summary>
        /// <value>The language descriptions.</value>
        public Collection<LanguageDescription> LanguageDescriptions
        {
            get
            {
                return this.languageDescriptions;
            }
        }
    }
}
