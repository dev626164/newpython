﻿//-----------------------------------------------------------------------
// <copyright file="ProbeIsolatedStorageEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>11/08/09</date>
// <summary>Probe IsolatedStorage EventArgs</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;

    /// <summary>
    /// Probe IsolatedStorage EventArgs
    /// </summary>
    public class ProbeIsolatedStorageEventArgs : EventArgs
    {
        /// <summary>
        /// probe success
        /// </summary>
        private bool success;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeIsolatedStorageEventArgs"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public ProbeIsolatedStorageEventArgs(bool success)
        {
            this.success = success;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ProbeIsolatedStorageEventArgs"/> is success.
        /// </summary>
        /// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
        public bool Success
        {
            get
            {
                return this.success;
            }

            set
            {
                this.success = value;
            }
        }
    }
}
