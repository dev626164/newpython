﻿//-----------------------------------------------------------------------
// <copyright file="StationAreaLoadedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>17-09-2009</date>
// <summary>Event arguments containing station objects as returned from Station web service.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    #region Using

    using System;
    using System.Collections.ObjectModel;
    using Microsoft.AirWatch.Common;

    #endregion

    /// <summary>
    /// Event arguments containing station object as returned from Station web service.
    /// </summary>
    public class StationAreaLoadedEventArgs : EventArgs
    {
        /// <summary>
        /// Backing field for Station property.
        /// </summary>
        private ObservableCollection<StationContainer> stations;

        /// <summary>
        /// Initializes a new instance of the <see cref="StationAreaLoadedEventArgs"/> class.
        /// </summary>
        /// <param name="stations">The stations.</param>
        public StationAreaLoadedEventArgs(ObservableCollection<StationContainer> stations)
        {
            this.stations = stations;
        }

        /// <summary>
        /// Gets the stations.
        /// </summary>
        /// <returns>The stations that has been loaded.</returns>
        public ObservableCollection<StationContainer> GetStations()
        {
            return this.stations;
        }
    }
}
