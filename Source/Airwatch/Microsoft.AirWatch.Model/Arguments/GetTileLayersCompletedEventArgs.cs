﻿//-----------------------------------------------------------------------
// <copyright file="GetTileLayersCompletedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/07/09</date>
// <summary>Event Args for when the TileLayers Collection is returned.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.ObjectModel;
    using Microsoft.AirWatch.UICommon;

    /// <summary>
    /// Language received event args
    /// </summary>
    public class GetTileLayersCompletedEventArgs : BaseEventArgs
    {
        /// <summary>
        /// Backing field for TileLayer property.
        /// </summary>
        private ObservableCollection<TileLayer> tileLayers;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetTileLayersCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="tileLayers">The tile layers.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public GetTileLayersCompletedEventArgs(ObservableCollection<TileLayer> tileLayers, bool success) : base(success)
        {
            this.tileLayers = tileLayers;
        }

        /// <summary>
        /// Gets the tile layers.
        /// </summary>
        /// <value>The tile layers.</value>
        public ObservableCollection<TileLayer> TileLayers
        {
            get
            {
                return this.tileLayers;
            }
        }
    }
}
