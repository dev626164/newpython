﻿//-----------------------------------------------------------------------
// <copyright file="SearchResultsEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author> </author>
// <email> @microsoft.com</email>
// <date>date</date>
// <summary>Search Results EventArgs</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.ObjectModel;
    using dev.virtualearth.net.webservices.v1.common;

    /// <summary>
    /// Search results event args
    /// </summary>
    public class SearchResultsEventArgs : EventArgs
    {
        /// <summary>
        /// Restults from bing services
        /// </summary>
        private ObservableCollection<GeocodeResult> results;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchResultsEventArgs"/> class.
        /// </summary>
        /// <param name="results">The results.</param>
        public SearchResultsEventArgs(ObservableCollection<GeocodeResult> results)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        /// <value>The results.</value>
        public ObservableCollection<GeocodeResult> Results
        {
            get
            {
                return this.results;
            }

            set
            {
                this.results = value;
            }
        }
    }
}
