﻿//-----------------------------------------------------------------------
// <copyright file="AddressResultsEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author> </author>
// <email> @microsoft.com</email>
// <date>date</date>
// <summary>Search Results EventArgs</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.ObjectModel;
    using dev.virtualearth.net.webservices.v1.geocode;

    /// <summary>
    /// Search results event args
    /// </summary>
    public class AddressResultsEventArgs : BaseEventArgs
    {
        /// <summary>
        /// Backnig field for PinType property.
        /// </summary>
        private string pinType;

        /// <summary>
        /// Restults from bing services
        /// </summary>
        private GeocodeResponse result;

        /// <summary>
        /// the pin id
        /// </summary>
        private int pinId;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressResultsEventArgs"/> class.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="pinId">The pin id.</param>
        /// <param name="pinType">Type of the pin.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public AddressResultsEventArgs(GeocodeResponse result, int pinId, string pinType, bool success) : base(success)
        {
            this.pinType = pinType;
            this.result = result;
            this.pinId = pinId;
        }

        /// <summary>
        /// Gets the type - pushpin, station, home.
        /// </summary>
        /// <value>The pin type.</value>
        public string PinType 
        {
            get
            {
                return this.pinType;
            }
        }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The result.</value>
        public GeocodeResponse Result
        {
            get
            {
                return this.result;
            }

            set
            {
                this.result = value;
            }
        }

        /// <summary>
        /// Gets or sets the pin id.
        /// </summary>
        /// <value>The pin id.</value>
        public int PinId
        {
            get
            {
                return this.pinId;
            }

            set
            {
                this.pinId = value;
            }
        }
    }
}
