﻿//-----------------------------------------------------------------------
// <copyright file="IsolatedStorageEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>23/06/09</date>
// <summary>Isolated Storage Event Args</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;

    /// <summary>
    /// Isolated storage event args
    /// </summary>
    public class IsolatedStorageEventArgs : EventArgs
    {
        /// <summary>
        /// Isolated storage entry
        /// </summary>
        private IsolatedStorageEntry isolatedStorageEntry;

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageEventArgs"/> class.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public IsolatedStorageEventArgs(IsolatedStorageEntry entry)
        {
            this.isolatedStorageEntry = entry;
        }

        /// <summary>
        /// Gets or sets the isolated storage entry.
        /// </summary>
        /// <value>The isolated storage entry.</value>
        public IsolatedStorageEntry IsolatedStorageEntry
        {
            get
            {
                return this.isolatedStorageEntry;
            }

            set
            {
                this.isolatedStorageEntry = value;
            }
        }
    }
}
