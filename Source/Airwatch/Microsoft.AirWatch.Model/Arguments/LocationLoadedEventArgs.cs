﻿// <copyright file="LocationLoadedArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>30-04-2009</date>
// <summary>Event Arguments to pass back when the Locations have been loaded</summary>
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Event Arguments to pass back when the Locations have been loaded 
    /// </summary>
    public class LocationLoadedEventArgs : EventArgs
    {
        /// <summary>
        /// Property for the collection of locations
        /// TODO: When we get a database running change the type to the data type in that
        /// </summary>
        private ObservableCollection<Microsoft.AirWatch.Model.Data.Location> locations;

        /// <summary>
        /// Initializes a new instance of the LocationLoadedEventArgs class
        /// with values within Locations
        /// </summary>
        /// <param name="locations">The collection of Locations which were loaded</param>
        public LocationLoadedEventArgs(ObservableCollection<Microsoft.AirWatch.Model.Data.Location> locations)
        {
            this.locations = locations;
        }

        /// <summary>
        /// Gets the Observable Collection of Locations which were loaded
        /// TODO: When we get a database running change the type to the data type in that
        /// </summary>
        public ObservableCollection<Microsoft.AirWatch.Model.Data.Location> Locations
        {
            get
            {
                return this.locations;
            }
        }
    }
}
