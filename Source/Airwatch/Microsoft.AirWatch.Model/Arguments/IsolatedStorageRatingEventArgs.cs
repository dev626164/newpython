﻿//-----------------------------------------------------------------------
// <copyright file="IsolatedStorageRatingEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <date>15/10/09</date>
// <summary>Isolated Storage Rating EventArgs</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;

    /// <summary>
    /// Isolated Storage Rating EventArgs
    /// </summary>
    public class IsolatedStorageRatingEventArgs : EventArgs
    {
        /// <summary>
        /// Isolated storage pushpins
        /// </summary>
        private IsolatedStorageRatings isolatedStorageRatings;

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageRatingEventArgs"/> class.
        /// </summary>
        /// <param name="ratings">The ratings.</param>
        public IsolatedStorageRatingEventArgs(IsolatedStorageRatings ratings)
        {
            this.isolatedStorageRatings = ratings;
        }

        /// <summary>
        /// Gets or sets the isolated storage pushpins.
        /// </summary>
        /// <value>The isolated storage pushpins.</value>
        public IsolatedStorageRatings IsolatedStorageRatings
        {
            get
            {
                return this.isolatedStorageRatings;
            }

            set
            {
                this.isolatedStorageRatings = value;
            }
        }
    }
}
