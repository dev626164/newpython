﻿//-----------------------------------------------------------------------
// <copyright file="UserLoadedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>16-06-2009</date>
// <summary>Event arguments containing user object as returned from User web service.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    #region Using
    using System;
    #endregion

    /// <summary>
    /// Event arguments containing user object as returned from User web service.
    /// </summary>
    public class UserLoadedEventArgs : BaseEventArgs
    {
        /// <summary>
        /// Backing field for User property.
        /// </summary>
        private User user;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLoadedEventArgs"/> class.
        /// </summary>
        /// <param name="user">The user that has been loaded.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public UserLoadedEventArgs(User user, bool success) : base(success)
        {
            this.user = user;
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <value>The user that has been loaded.</value>
        public User User
        {
            get
            {
                return this.user;
            }
        }
    }
}
