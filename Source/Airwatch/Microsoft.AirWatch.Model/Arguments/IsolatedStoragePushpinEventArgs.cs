﻿//-----------------------------------------------------------------------
// <copyright file="IsolatedStoragePushpinEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>01/08/09</date>
// <summary>Isolated Storage Pushpin EventArgs</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;

    /// <summary>
    /// Isolated Storage Pushpin EventArgs
    /// </summary>
    public class IsolatedStoragePushpinEventArgs : EventArgs
    {
        /// <summary>
        /// Isolated storage pushpins
        /// </summary>
        private IsolatedStoragePushpins isolatedStoragePushpins;

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStoragePushpinEventArgs"/> class.
        /// </summary>
        /// <param name="pushpins">The pushpins.</param>
        public IsolatedStoragePushpinEventArgs(IsolatedStoragePushpins pushpins)
        {
            this.isolatedStoragePushpins = pushpins;
        }

        /// <summary>
        /// Gets or sets the isolated storage pushpins.
        /// </summary>
        /// <value>The isolated storage pushpins.</value>
        public IsolatedStoragePushpins IsolatedStoragePushpins
        {
            get
            {
                return this.isolatedStoragePushpins;
            }

            set
            {
                this.isolatedStoragePushpins = value;
            }
        }
    }
}
