﻿// <copyright file="TokenRetrievedEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>17-06-2009</date>
// <summary>Custom EventArgs for token retrieval</summary>
namespace Microsoft.AirWatch.Model
{
    using System;

    /// <summary>
    /// Class for custom event args for token retrieval
    /// </summary>
    public class TokenRetrievedEventArgs : BaseEventArgs
    {
        /// <summary>
        /// String name for map token
        /// </summary>
        private string bingToken;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenRetrievedEventArgs"/> class.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public TokenRetrievedEventArgs(string token, bool success) : base(success)
        {
            this.bingToken = token;
        }

        /// <summary>
        /// Gets or sets the string name for map token
        /// </summary>
        public string BingToken
        {
            get
            {
                return this.bingToken;
            }

            set
            {
                this.bingToken = value;
            }
        }
    }
}
