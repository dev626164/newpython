﻿//-----------------------------------------------------------------------
// <copyright file="BaseEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author> </author>
// <email> @microsoft.com</email>
// <date>date</date>
// <summary>Search Results EventArgs</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System;
    using System.Collections.ObjectModel;
    using dev.virtualearth.net.webservices.v1.geocode;

    /// <summary>
    /// Search results event args
    /// </summary>
    public class BaseEventArgs : EventArgs
    {    
        /// <summary>
        /// Was the webservice call successful.
        /// </summary>
        private bool success;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEventArgs"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public BaseEventArgs(bool success)
        {
            this.Success = success;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BaseEventArgs"/> is success.
        /// </summary>
        /// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
        public bool Success
        {
            get
            {
                return this.success;
            }

            set
            {
                this.success = value;
            }
        }        
    }
}
