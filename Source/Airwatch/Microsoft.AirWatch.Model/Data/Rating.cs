﻿// <copyright file="Rating.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>15-10-2009</date>
// <summary>Used to store rating location and times
// </summary>
namespace Microsoft.AirWatch.Model.Data
{
    using System;

    /// <summary>
    /// Used to store rating location and times
    /// </summary>
    public class Rating
    {
        /// <summary>
        /// Gets or sets the longitude of the location
        /// </summary>
        public decimal Longitude { get; set; }

        /// <summary>
        /// Gets or sets the latitude of the location
        /// </summary>
        public decimal Latitude { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>The date of the rating.</value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The pin type.</value>
        public string PinType { get; set; }
    }
}
