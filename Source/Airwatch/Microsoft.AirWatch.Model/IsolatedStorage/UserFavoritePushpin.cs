﻿//-----------------------------------------------------------------------
// <copyright file="UserFavoritePushpin.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2009</date>
// <summary>Construct for UserFavoritePushpin for Isolated Storage</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Construct for UserFavoritePushpin for Isolated Storage
    /// </summary>
    public class UserFavoritePushpin
    {
        /// <summary>
        /// Gets or sets the pushpin.
        /// </summary>
        /// <value>The pushpin.</value>
        public Pushpin Pushpin { get; set; }
    }
}
