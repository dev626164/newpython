﻿//-----------------------------------------------------------------------
// <copyright file="IsolatedStorageRatings.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <date>15/10/09</date>
// <summary>Isolated Storage Ratings</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.AirWatch.Model.Data;

    /// <summary>
    /// Isolated storage ratings
    /// </summary>
    [DataContract]
    public class IsolatedStorageRatings
    {
        /// <summary>
        /// Gets or sets the pushpin collection.
        /// </summary>
        /// <value>The pushpin collection.</value>
        [DataMember]
        public ObservableCollection<Rating> RatingCollection { get; set; } 
    }
}
