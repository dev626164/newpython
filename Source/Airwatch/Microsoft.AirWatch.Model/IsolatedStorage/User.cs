﻿//-----------------------------------------------------------------------
// <copyright file="User.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2009</date>
// <summary>Construct for User for Isolated Storage</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    /// <summary>
    /// Construct for User for Isolated Storage
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets the user home pushpin.
        /// </summary>
        /// <value>The user home pushpin.</value>
        public UserFavoritePushpin UserHomePushpin { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public string Culture { get; set; }
    }
}
