﻿//-----------------------------------------------------------------------
// <copyright file="IsolatedStoragePushpins.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>01/08/09</date>
// <summary>Isolated Storage Pushpins</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Isolated storage pushpins
    /// </summary>
    [DataContract]
    public class IsolatedStoragePushpins
    {
        /// <summary>
        /// Gets or sets the pushpin collection.
        /// </summary>
        /// <value>The pushpin collection.</value>
        [DataMember]
        public ObservableCollection<Pushpin> PushpinCollection { get; set; } 
    }
}
