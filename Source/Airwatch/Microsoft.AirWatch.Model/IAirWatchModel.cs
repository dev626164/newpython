﻿// <copyright file="IAirWatchModel.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>30-04-2009</date>
// <summary>Interface to model the data access for AirWatch data</summary>
namespace Microsoft.AirWatch.Model
{
    using System;
    using Microsoft.AirWatch.UICommon;

    /// <summary>
    /// Interface to model the data access for AirWatch data
    /// </summary>
    public interface IAirWatchModel
    {
        /// <summary>
        /// Occurs when [get language completed].
        /// </summary>
        event EventHandler ChangeCultureCompleted;

        /// <summary>
        /// Occurs when [get languages completed].
        /// </summary>
        event EventHandler<LanguagesReceivedEventArgs> GetCulturesCompleted;

        /// <summary>
        /// Occurs when [token retrieved].
        /// </summary>
        event EventHandler<TokenRetrievedEventArgs> MapTokenRetrieved;

        /// <summary>
        /// Occurs when [search results retrieved].
        /// </summary>
        event EventHandler<SearchResultsEventArgs> SearchResultsRetrieved;

        /// <summary>
        /// Occurs when [isolated storage user retrieved].
        /// </summary>
        event EventHandler<UserLoadedEventArgs> IsolatedStorageUserLoaded;

        /// <summary>
        /// Occurs when [pushpin loaded].
        /// </summary>
        event EventHandler<PushpinLoadedEventArgs> PushpinLoaded;

        /// <summary>
        /// Occurs when [home pushpin loaded].
        /// </summary>
        event EventHandler<PushpinLoadedEventArgs> HomePushpinLoaded;

        /// <summary>
        /// Occurs when [station loaded].
        /// </summary>
        event EventHandler<StationLoadedEventArgs> StationLoaded;

        /// <summary>
        /// Occurs when [station area loaded].
        /// </summary>
        event EventHandler<StationAreaLoadedEventArgs> StationAreaLoaded;

        /// <summary>
        /// Occurs when [get tile layers completed].
        /// </summary>
        event EventHandler<GetTileLayersCompletedEventArgs> GetTileLayersCompleted;

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <param name="culture">The culture (eg. en-GB).</param>
        void GetLanguage(string culture);

        /// <summary>
        /// Gets the pushpin.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="pushpinId">The pushpin id.</param>
        void GetPushpin(decimal latitude, decimal longitude, int pushpinId);

        /// <summary>
        /// Gets the home pushpin.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        void GetHomePushpin(decimal latitude, decimal longitude);

        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <param name="clientIPAddress">The client IP address.</param>
        void GetMapToken(string clientIPAddress);

        /// <summary>
        /// Gets the searchresults.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture</param>
        void GetSearchResults(string query, string token, string culture);

        /// <summary>
        /// Gets the isolated storage entry.
        /// </summary>
        void GetIsolatedStorageEntry();

        /// <summary>
        /// Probes the isolated storage.
        /// </summary>
        /// <param name="bytesRequired">The bytes required.</param>
        /// <param name="increaseQuota">if set to <c>true</c> [increase quota].</param>
        /// <returns>True if there is enough space left, false otherwise.</returns>
        bool ProbeIsolatedStorage(long bytesRequired, bool increaseQuota);

        /// <summary>
        /// Saves the isolated storage entry.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="friendlyName">Name of the friendly.</param>
        void SaveIsolatedStorageEntry(string culture, decimal? longitude, decimal? latitude, string friendlyName);

        /// <summary>
        /// Gets the station descriptions for map boundaries.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <param name="numberToReturn">The number to return.</param>
        void GetStationDescriptionsForMapBoundaries(double north, double east, double south, double west, int numberToReturn);

        /// <summary>
        /// Gets the station by code.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        void GetStationById(int stationId);

        /// <summary>
        /// Gets the tile layers.
        /// </summary>
        void GetTileLayers();
    }
}
