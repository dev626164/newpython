﻿//-----------------------------------------------------------------------
// <copyright file="UserUnitTests.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The Fifth Medium.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>8 June 2009</date>
// <summary>Unit Tests for the Users Business and Data layers</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AdoDataModel.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit Tests for the Users Business and Data layers
    /// </summary>
    [TestClass]
    public class UserUnitTests
    {
        /// <summary>
        /// The context for the tests
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the UserUnitTests class.
        /// </summary>
        public UserUnitTests()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        ////
        //// You can use the following additional attributes as you write your tests:
        ////
        //// Use ClassInitialize to run code before running the first test in the class
        //// [ClassInitialize()]
        //// public static void MyClassInitialize(TestContext testContext) { }
        ////
        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }
        ////
        #endregion

        /// <summary>
        /// Test the login of a test user
        /// </summary>
        [TestMethod]
        public void TestLoadUser()
        {
            AirWatchUsersBusinessLayer businessLayer = AirWatchUsersBusinessLayer.Instance;
            Guid identifier = new Guid("00000000-0000-0000-0000-000000000000");

            User user = businessLayer.LoadUser(identifier);

            Assert.IsNotNull(user);
            Assert.AreEqual(identifier, user.UserId);
        }

        /// <summary>
        /// Test the creation and deletion of a pushpin
        /// </summary>
        [TestMethod]
        public void TestCreationAndDeletionOfPushpin()
        {
            AirWatchUsersBusinessLayer businessLayer = AirWatchUsersBusinessLayer.Instance;

            decimal longitude = 0, latitude = 50;

            Pushpin p = businessLayer.CreatePushpin(longitude, latitude);

            Assert.AreEqual(longitude, p.Location.Longitude);
            Assert.AreEqual(latitude, p.Location.Latitude);

            long pushpinId = p.PushpinId, locationId = p.Location.LocationId;

            businessLayer.DeletePushpin(p.PushpinId);

            using (AirWatchEntities userProxy = new AirWatchEntities())
            {
                int count = (from pin in userProxy.Pushpin
                             where pin.PushpinId == pushpinId
                             select pin).Count();

                Assert.AreEqual(0, count, "The pushpin was found and so not deleted correctly");

                count = (from location in userProxy.Location
                         where location.LocationId == locationId
                         select location).Count();

                Assert.AreEqual(0, count, "The location was found and so not deleted correctly with the pushpin");

                userProxy.Dispose();
            }
        }

        /// <summary>
        /// Test the creation and deletion of a pushpin when it is rated
        /// </summary>
        [TestMethod]
        public void TestCreationAndDeletionOfPushpinWithARating()
        {
            AirWatchUsersBusinessLayer businessLayer = AirWatchUsersBusinessLayer.Instance;

            decimal longitude = 0, latitude = 50;

            Pushpin p = businessLayer.CreatePushpin(longitude, latitude);

            Assert.AreEqual(longitude, p.Location.Longitude);
            Assert.AreEqual(latitude, p.Location.Latitude);

            int ratingIndex = 1;

            UserRating rating = businessLayer.CreateUserRatingForLocation(p.Location.LocationId, ratingIndex);

            long pushpinId = p.PushpinId, locationId = p.Location.LocationId;

            businessLayer.DeletePushpin(p.PushpinId);

            using (AirWatchEntities userProxy = new AirWatchEntities())
            {
                int count = (from pin in userProxy.Pushpin
                             where pin.PushpinId == pushpinId
                             select pin).Count();

                Assert.AreEqual(0, count, "The pushpin was found and so not deleted correctly");

                var locations = from location in userProxy.Location
                                where location.LocationId == locationId
                                select location;

                Assert.AreEqual(1, locations.Count(), "The location was not found and so deleted with the pushpin incorrectly");

                // CLEANUP ON THE USER RATING (NORMALLY NOT DONE)
                userProxy.DeleteObject(locations.First());
                userProxy.DeleteObject((from uRating in userProxy.UserRating
                                        where uRating.UserRatingId == rating.UserRatingId
                                        select uRating).First());

                userProxy.SaveChanges();

                userProxy.Dispose();
            }
        }

        /// <summary>
        /// Tests a user's mobile gets updated
        /// </summary>
        [TestMethod]
        public void TestUpdateOfUserMobile()
        {
            AirWatchUsersBusinessLayer businessLayer = AirWatchUsersBusinessLayer.Instance;
            Guid identifier = new Guid("00000000-0000-0000-0000-000000000000");

            // ensure user exists
            User user = businessLayer.CreateUser(identifier);

            // ensure that the user's mobile is null before starting the test
            if (user.MobileNumber != null)
            {
                businessLayer.SetMobile(identifier, null, "en-gb");

                Assert.IsNull(user.MobileNumber, "Unable to set the user's mobile number to null before the test starts");
            }

            string mobile = "07654321234";
            businessLayer.SetMobile(identifier, mobile, "en-gb");

            using (AirWatchEntities userProxy = new AirWatchEntities())
            {
                user = (from u in userProxy.User
                        where u.UserId == identifier
                        select u).First();

                Assert.AreEqual(mobile, user.MobileNumber);

                // reset back to null
                businessLayer.SetMobile(identifier, null, "en-gb");
            }
        }

        /// <summary>
        /// Test the creation and deletion of a new user (attaching a user favorite)
        /// </summary>
        [TestMethod]
        public void TestCreationAndDeletionOfUser()
        {
            AirWatchUsersBusinessLayer businessLayer = AirWatchUsersBusinessLayer.Instance;
            Guid id = Guid.NewGuid();
            User user = businessLayer.CreateUser(id);

            businessLayer.DeleteUserAndAssociatedData(user.UserId);

            using (AirWatchEntities userProxy = new AirWatchEntities())
            {
                var numberOfUsers = (from u in userProxy.User
                                     where u.UserId == id
                                     select u).Count();

                Assert.AreEqual(0, numberOfUsers);
            }
        }

        /// <summary>
        /// Test the creation and deletion of a new user (attaching a user favorite)
        /// </summary>
        [TestMethod]
        public void TestCreationAndDeletionOfUserWithSettingsAndAlerts()
        {
            AirWatchUsersBusinessLayer businessLayer = AirWatchUsersBusinessLayer.Instance;
            Guid id = Guid.NewGuid();
            User user = businessLayer.CreateUser(id);

            for (int i = 0; i < 2; i++)
            {
                Station station = AirWatchServerBusinessLayer.Instance.GetStationForStationCode("AD0944A");
                //// UserFavoriteStation fav = new UserFavoriteStation()
                //// {
                ////     Station = station,
                //// };

                //// UserFavoriteStation faveStation = businessLayer.AddUserFavoriteStation(user.UserId, fav.Station.EuropeanCode);
                //// businessLayer.CreateAlertForStation(1, faveStation.UserFavoriteStationId);

                Pushpin p = businessLayer.CreatePushpin(50, 0);
                UserFavoritePushpin favePushpin = businessLayer.AddUserFavoritePushpin(user.UserId, p.PushpinId);
                businessLayer.CreateAlertForPushpin(1, favePushpin.UserFavoritePushpinId);

                businessLayer.SetHomeLocation(user.UserId, favePushpin);
            }

            businessLayer.DeleteUserAndAssociatedData(user.UserId);

            using (AirWatchEntities userProxy = new AirWatchEntities())
            {
                var numberOfUsers = (from u in userProxy.User
                                     where u.UserId == id
                                     select u).Count();

                Assert.AreEqual(0, numberOfUsers);
            }
        }

        /// <summary>
        /// Tests the updating of a user from a detached user object
        /// </summary>
        [TestMethod]
        public void TestUpdateUser()
        {
            User attachedUser = AirWatchUsersBusinessLayer.Instance.CreateUser(new Guid("00000000-0000-0000-0000-000000000000"));

            User detachedUser = new User()
            {
                UserId = new Guid("00000000-0000-0000-0000-000000000000"),
                MobileNumber = "UPDATED!!!",
            };

            AirWatchUsersBusinessLayer.Instance.UpdateUser(detachedUser);

            Assert.AreEqual("UPDATED!!!", attachedUser.MobileNumber);

            // Reset back to original
            detachedUser = new User()
            {
                UserId = new Guid("00000000-0000-0000-0000-000000000000"),
                MobileNumber = null,
            };
            AirWatchUsersBusinessLayer.Instance.UpdateUser(detachedUser);
        }
    }
}
