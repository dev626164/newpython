﻿// <copyright file="ServerUnitTests.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>ServerUnitTest 
// </summary>
namespace Microsoft.AirWatch.AdoDataModel.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// ServerBusinessLayer Unit Tests 
    /// </summary>
    [TestClass]
    public class ServerUnitTests
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServerUnitTests"/> class.
        /// </summary>
        public ServerUnitTests()
        {
            // TODO: Add constructor logic here
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion

        /// <summary>
        /// Gets the index for point test.
        /// </summary>
        [TestMethod]
        public void GetIndexForPointTest()
        {
            Assert.IsNotNull(AirWatchServerBusinessLayer.Instance.GetIndexForPoint(0.322m, 50.328m, "CAQI"), "The query returned no results");
        }

        /// <summary>
        /// Gets the centre location for point test.
        /// </summary>
        [TestMethod]
        public void GetCentreLocationForPointTest()
        {
            Assert.IsNotNull(AirWatchServerBusinessLayer.Instance.GetCentreLocationForPoint(0.322m, 50.328m),  "The query returned no results");
        }

        /// <summary>
        /// Gets the station for station code test.
        /// </summary>
        [TestMethod]
        public void GetStationForStationCodeTest()
        {
            Assert.IsNotNull(AirWatchServerBusinessLayer.Instance.GetStationForStationCode("0"), "The query returned no results");
        }

        /// <summary>
        /// Gets the language for culture test.
        /// </summary>
        [TestMethod]
        public void GetLanguageForCultureTest()
        {
            Assert.IsNotNull(AirWatchServerBusinessLayer.Instance.GetLanguageForCulture("en-GB"), "The query returned no results");
        }

        /// <summary>
        /// Gets all languages from the database
        /// </summary>
        [TestMethod]
        public void GetLanguagesTest()
        {
            Language[] langs = AirWatchServerBusinessLayer.Instance.GetLanguages();
            Assert.IsNotNull(langs, "The query returned no languages");
            Assert.IsTrue(langs.Count() > 0, "No Languages were returned");
        }
    }
}
