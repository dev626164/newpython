﻿//-----------------------------------------------------------------------
// <copyright file="RatingQualifierIdentifiers.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2009</date>
// <summary>Construct representing the Rating Qualifier Identifiers for Average Ratings</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Construct representing the Rating Qualifier Identifiers for Average Ratings
    /// </summary>
    [DataContractAttribute]
    [Serializable]
    public partial class RatingQualifierIdentifiers
    {
        /// <summary>
        /// Backing data storage for Word Identifier
        /// </summary>
        private string wordIdentifier;

        /// <summary>
        /// Backing data storage for Percentage
        /// </summary>
        private double? percentage;

        /// <summary>
        /// Backing data storage for AverageRating
        /// </summary>
        private AverageRating averageRating;

        /// <summary>
        /// Gets or sets the word identifier.
        /// </summary>
        /// <value>The word identifier.</value>
        [DataMemberAttribute]
        public string WordIdentifier
        {
            get
            {
                return this.wordIdentifier;
            }

            set
            {
                this.wordIdentifier = value;
            }
        }

        /// <summary>
        /// Gets or sets the percentage.
        /// </summary>
        /// <value>The percentage.</value>
        [DataMemberAttribute]
        public double? Percentage
        {
            get
            {
                return this.percentage;
            }

            set
            {
                this.percentage = value;
            }
        }

        /// <summary>
        /// Gets or sets the average rating.
        /// </summary>
        /// <value>The average rating.</value>
        [DataMemberAttribute]
        public AverageRating AverageRating
        {
            get
            {
                return this.averageRating;
            }

            set
            {
                this.averageRating = value;
            }
        }

        /// <summary>
        /// Create a new RatingQualifierIdentifiers object.
        /// </summary>
        /// <returns>The RatingQualifierIdentifier created</returns>
        public static RatingQualifierIdentifiers CreateRatingQualifierIdentifiers()
        {
            RatingQualifierIdentifiers ratingQualifierIdentifiers = new RatingQualifierIdentifiers();
            return ratingQualifierIdentifiers;
        }
    }
}
