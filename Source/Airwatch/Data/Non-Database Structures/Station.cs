﻿//-----------------------------------------------------------------------
// <copyright file="Station.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>27 August 2009</date>
// <summary>Construct representing Stations adding Average Rating to the partial class</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Partial Construct representing Stations
    /// </summary>
    public partial class Station
    {
        /// <summary>
        /// Backing data storage for AverageRating
        /// </summary>
        private AverageRating averageRating;

        /// <summary>
        /// Gets or sets the average rating.
        /// </summary>
        /// <value>The average rating.</value>
        [DataMemberAttribute]
        public AverageRating AverageRating
        {
            get
            {
                return this.averageRating;
            }

            set
            {
                this.averageRating = value;
            }
        }
    }
}
