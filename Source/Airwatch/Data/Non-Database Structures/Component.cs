﻿//-----------------------------------------------------------------------
// <copyright file="Component.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>27 August 2009</date>
// <summary>Construct representing Component adding ComponentLocalizationKey to the partial class.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Construct representing Component adding ComponentLocalizationKey to the partial class.
    /// </summary>
    public partial class Component
    {
        /// <summary>
        /// Backing field for ComponentLocalizationKey property.
        /// </summary>
        private string componentLocalizationKey;

        /// <summary>
        /// Gets or sets the component localization key.
        /// </summary>
        /// <value>The component localization key.</value>
        [DataMemberAttribute]
        public string ComponentLocalizationKey
        {
            get
            {
                switch (this.Caption)
                {
                    case "NO2":
                        this.componentLocalizationKey = "COMPONENT_NO2";
                        break;
                    case "O3":
                        this.componentLocalizationKey = "COMPONENT_O3";
                        break;
                    case "PM10":
                        this.componentLocalizationKey = "COMPONENT_PM10";
                        break;
                    default:
                        this.componentLocalizationKey = this.Caption;
                        break;
                }

                return this.componentLocalizationKey;
            }

            set
            {
                this.componentLocalizationKey = value;
            }
        }
    }
}
