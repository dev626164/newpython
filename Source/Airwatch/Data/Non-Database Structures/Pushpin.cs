﻿//-----------------------------------------------------------------------
// <copyright file="Pushpin.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2009</date>
// <summary>Construct representing pushpins</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Construct representing pushpins
    /// </summary>
    [DataContractAttribute]
    [Serializable]
    public partial class Pushpin
    {
        /// <summary>
        /// Backing data storage for PushpinId
        /// </summary>
        private int pushpinId;

        /// <summary>
        /// Backing data storage for Friendly Name
        /// </summary>
        private string friendlyName;

        /// <summary>
        /// Backing data storage for CentreLocation
        /// </summary>
        private CentreLocation centreLocation;

        /// <summary>
        /// Backing data storage for Location
        /// </summary>
        private Location location;

        /// <summary>
        /// Backing data storage for AverageRating
        /// </summary>
        private AverageRating averageRating;

        /// <summary>
        /// Gets or sets the pushpin id.
        /// </summary>
        /// <value>The pushpin id.</value>
        [DataMemberAttribute]
        public int PushpinId
        {
            get
            {
                return this.pushpinId;
            }

            set
            {
                this.pushpinId = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the friendly.
        /// </summary>
        /// <value>The name of the friendly.</value>
        [DataMemberAttribute]
        public string FriendlyName
        {
            get
            {
                return this.friendlyName;
            }

            set
            {
                this.friendlyName = value;
            }
        }

        /// <summary>
        /// Gets or sets the centre location.
        /// </summary>
        /// <value>The centre location.</value>
        [DataMemberAttribute]
        public CentreLocation CentreLocation
        {
            get
            {
                return this.centreLocation;
            }

            set
            {
                this.centreLocation = value;
            }
        }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        [DataMemberAttribute]
        public Location Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.location = value;
            }
        }

        /// <summary>
        /// Gets or sets the average rating.
        /// </summary>
        /// <value>The average rating.</value>
        [DataMemberAttribute]
        public AverageRating AverageRating
        {
            get
            {
                return this.averageRating;
            }

            set
            {
                this.averageRating = value;
            }
        }

        /// <summary>
        /// Create a new Pushpin object.
        /// </summary>
        /// <returns>The Created Pushpin</returns>
        public static Pushpin CreatePushpin()
        {
            Pushpin pushpin = new Pushpin();
            return pushpin;
        }
    }
}
