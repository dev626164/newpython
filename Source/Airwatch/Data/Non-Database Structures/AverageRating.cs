﻿//-----------------------------------------------------------------------
// <copyright file="AverageRating.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>18 August 2009</date>
// <summary>Construct representing the average rating for a pushpin</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Construct representing the average rating for a pushpin
    /// </summary>
    [DataContractAttribute]
    [Serializable]
    public partial class AverageRating
    {
        /// <summary>
        /// Backing data storage for Rating
        /// </summary>
        private int? rating;

        /// <summary>
        /// Backing data storage for Count
        /// </summary>
        private int? count;

        /// <summary>
        /// Backing data storage for RatingQualifierIdentifiers
        /// </summary>
        private Collection<RatingQualifierIdentifiers> ratingQualifierIdentifiers;

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        /// <value>The rating.</value>
        [DataMemberAttribute]
        public int? Rating
        {
            get
            {
                return this.rating;
            }

            set
            {
                this.rating = value;
            }
        }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>The count.</value>
        [DataMemberAttribute]
        public int? Count
        {
            get
            {
                return this.count;
            }

            set
            {
                this.count = value;
            }
        }

        /// <summary>
        /// Gets or sets the rating qualifier identifiers.
        /// </summary>
        /// <value>The rating qualifier identifiers.</value>
        [DataMemberAttribute]
        public Collection<RatingQualifierIdentifiers> RatingQualifierIdentifiers
        {
            get
            {
                return this.ratingQualifierIdentifiers;
            }

            set
            {
                if (value != null)
                {
                    this.ratingQualifierIdentifiers = value;
                }
            }
        }

        /// <summary>
        /// Create a new AverageRating object.
        /// </summary>
        /// <returns>The AverageRating Created</returns>
        public static AverageRating CreateAverageRating()
        {
            AverageRating averageRating = new AverageRating();
            return averageRating;
        }
    }
}
