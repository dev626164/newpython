//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3074
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace Microsoft.AirWatch.Core.Data.DTO
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.CodeDom.Compiler;

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class Alert
    {
        [DataMember]
        public Int64 AlertId { get; set; }
        [DataMember]
        public Int32? Threshold { get; set; }
        [DataMember]
        public Boolean NotifiedBad { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class CentreLocation
    {
        [DataMember]
        public Int64 CentreLocationId { get; set; }
        [DataMember]
        public Int32? QualityIndex { get; set; }
        [DataMember]
        public DateTime? Timestamp { get; set; }
        [DataMember]
        public Double? QualityValue { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Location Location { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.CentreLocationMeasurement> CentreLocationMeasurement { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class CentreLocationMeasurement
    {
        [DataMember]
        public Int64 CentreLocationMeasurementId { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.CentreLocation CentreLocation { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Measurement Measurement { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class Component
    {
        [DataMember]
        public Int64 ComponentId { get; set; }
        [DataMember]
        public String Caption { get; set; }
        [DataMember]
        public String Unit { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.Measurement> Measurement { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.Threshold> Threshold { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class Location
    {
        [DataMember]
        public Int64 LocationId { get; set; }
        [DataMember]
        public Decimal Longitude { get; set; }
        [DataMember]
        public Decimal Latitude { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.CentreLocation> CentreLocation { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.Station> Station { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class MapModelDataEntry
    {
        [DataMember]
        public Int64 DataEntryKey { get; set; }
        [DataMember]
        public Double Longitude { get; set; }
        [DataMember]
        public Double Latitude { get; set; }
        [DataMember]
        public Double? CAQI { get; set; }
        [DataMember]
        public Double? Ozone { get; set; }
        [DataMember]
        public Double? NitrogenDioxide { get; set; }
        [DataMember]
        public Double? ParticulateMatter10 { get; set; }
        [DataMember]
        public DateTime Timestamp { get; set; }
        [DataMember]
        public Double? QualityValue { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class Measurement
    {
        [DataMember]
        public Int64 MeasurementId { get; set; }
        [DataMember]
        public Decimal? Value { get; set; }
        [DataMember]
        public DateTime? Timestamp { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.CentreLocationMeasurement> CentreLocationMeasurement { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Component Component { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.StationMeasurement> StationMeasurement { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class Station
    {
        [DataMember]
        public Int32 StationId { get; set; }
        [DataMember]
        public String EuropeanCode { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public String CountryName { get; set; }
        [DataMember]
        public String FriendlyAddress { get; set; }
        [DataMember]
        public String TypeOfStation { get; set; }
        [DataMember]
        public String TypeOfArea { get; set; }
        [DataMember]
        public String TypeOfAreaSubcat { get; set; }
        [DataMember]
        public String StreetType { get; set; }
        [DataMember]
        public Int32? QualityIndex { get; set; }
        [DataMember]
        public DateTime? Timestamp { get; set; }
        [DataMember]
        public String StationPurpose { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Location Location { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.StationMeasurement> StationMeasurement { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class StationDataEntry
    {
        [DataMember]
        public Int64 DataEntryKey { get; set; }
        [DataMember]
        public Decimal Longitude { get; set; }
        [DataMember]
        public Decimal Latitude { get; set; }
        [DataMember]
        public Decimal? CAQI { get; set; }
        [DataMember]
        public Decimal? Ozone { get; set; }
        [DataMember]
        public Decimal? NitrogenDioxide { get; set; }
        [DataMember]
        public Decimal? ParticulateMatter10 { get; set; }
        [DataMember]
        public DateTime Timestamp { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class StationMeasurement
    {
        [DataMember]
        public Int32 StationMeasurementId { get; set; }
        [DataMember]
        public Int32? AggregatedUserRating { get; set; }
        [DataMember]
        public Int32? AggregatedUserRatingCount { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Measurement Measurement { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Station Station { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class UserRating
    {
        [DataMember]
        public Int64 UserRatingId { get; set; }
        [DataMember]
        public Int32 Rating { get; set; }
        [DataMember]
        public DateTime RatingTime { get; set; }
        [DataMember]
        public String RatingTarget { get; set; }
        [DataMember]
        public String IPAddress { get; set; }
        [DataMember]
        public Decimal Longitude { get; set; }
        [DataMember]
        public Decimal Latitude { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.RatingQualifier> RatingQualifier { get; set; }
        [DataMember]
        public IList<Microsoft.AirWatch.Core.Data.DTO.WordQualifier> WordQualifier { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class RatingQualifier
    {
        [DataMember]
        public Int64 RatingQualifierId { get; set; }
        [DataMember]
        public String Identifier { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.UserRating UserRating { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class StationMinimal
    {
        [DataMember]
        public String EuropeanCode { get; set; }
        [DataMember]
        public Decimal Latitude { get; set; }
        [DataMember]
        public Decimal Longitude { get; set; }
        [DataMember]
        public String StationPurpose { get; set; }
        [DataMember]
        public Int32? QualityIndex { get; set; }
        [DataMember]
        public Int32 StationId { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class WordQualifier
    {
        [DataMember]
        public Int64 QualifiersId { get; set; }
        [DataMember]
        public Int32 WordId { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.UserRating UserRating { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class Threshold
    {
        [DataMember]
        public Int64 ThresholdId { get; set; }
        [DataMember]
        public Decimal Value { get; set; }
        [DataMember]
        public Microsoft.AirWatch.Core.Data.DTO.Component Component { get; set; }
    }

    [GeneratedCodeAttribute("EF POCO ClassGenerator", "1.3")]
    [DataContract]
    public class MapModelData
    {
        [DataMember]
        public Int32 MapModelDataId { get; set; }
        [DataMember]
        public Double? Resolution { get; set; }
        [DataMember]
        public String Type { get; set; }
    }
}
