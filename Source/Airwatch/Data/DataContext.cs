﻿//-----------------------------------------------------------------------
// <copyright file="DataContext.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Provides a thin abstraction layer between the Entity Framework and the Services Layer.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.Data
{
    using System;
    using Microsoft.WindowsAzure.ServiceRuntime;

    /// <summary>
    /// Provides a thin abstraction layer between the Entity Framework and the Services Layer.
    /// </summary>
    public class DataContext : IDisposable
    {
        /// <summary>
        /// Backing field for AirWatchEntities property.
        /// </summary>
        private AirWatchEntities airWatchEntities;

        /// <summary>
        /// Prevents a default instance of the <see cref="DataContext"/> class from being created.
        /// </summary>
        private DataContext()
        {
            string entityFrameworkConnection = string.Empty;

            if (RoleEnvironment.IsAvailable)
            {
                string connectionString = RoleEnvironment.GetConfigurationSettingValue("ConnectionString");
                entityFrameworkConnection = String.Format(System.Globalization.CultureInfo.InvariantCulture, RoleEnvironment.GetConfigurationSettingValue("EFConnection"), connectionString);
            }
            else
            {
                entityFrameworkConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            }

            this.airWatchEntities = new AirWatchEntities(entityFrameworkConnection);
        }

        /// <summary>
        /// Gets the AirWatchEntities object.
        /// </summary>
        /// <value>The air watch entites.</value>
        public AirWatchEntities AirWatchEntities
        {
            get
            {
                return this.airWatchEntities;
            }
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>A new instance of the DataContext.</returns>
        public static DataContext Create()
        {
            return new DataContext();
        }

        #region IDisposable Members
        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
            this.airWatchEntities.Dispose();
        }

        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        /// <param name="onlyNative">if true will only dispose of the native resources</param>
        protected virtual void Dispose(bool onlyNative)
        {
            if (!onlyNative)
            {
                this.airWatchEntities.Dispose();
            }
        }

        #endregion
    }
}
