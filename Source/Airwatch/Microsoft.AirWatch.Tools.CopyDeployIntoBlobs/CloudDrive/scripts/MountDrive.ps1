function MountDrive {
Param (
	$Account = "airwatchuatstaging",
	$Key = "TodB0C3QwqLR+fd/Zni5AKkmjfxFYSiG9669kO1P2tLrtzod8Gglrk4D/y2l7Aq0OoquL583qP25Qjgj3yeBuQ==",
	$ServiceUrl="http://blob.core.windows.net",
	$DriveName="Blob",
	$ProviderName="BlobDrive")

 # Power Shell Snapin setup
 add-pssnapin CloudDriveSnapin -ErrorAction SilentlyContinue

 # Create the credentials
 $password = ConvertTo-SecureString -AsPlainText -Force $Key
 $cred = New-Object -TypeName Management.Automation.PSCredential -ArgumentList $Account, $password

 # Mount storage service as a drive
 new-psdrive -psprovider $ProviderName -root $ServiceUrl -name $DriveName -cred $cred -scope global
}

MountDrive -ServiceUrl "http://blob.core.windows.net" -DriveName "Blob" -ProviderName "BlobDrive"
MountDrive -ServiceUrl "http://queue.core.windows.net" -DriveName "Queue" -ProviderName "QueueDrive"