﻿SET-PSDEBUG T

#Example of using a filter 
function ListFiltered([string]$path, [string]$filter )
{
    Write-Host "The following will return all items under the specific path:"
    Write-Host "    dir -Path $path \n\n"
    dir -Path $path
    
    Write-Host "The following will return all items beginning with filter, but filtering is done client-side"
    Write-Host "That means for LARGE number of items in `"$path`" it will have to download them all\n\n"
    Write-Host "   dir -Path $path -Include $filter"
    dir -Path $path $filter
    
    Write-Host "The following will return all items beginning with filter, but filtering is done server-side"
    Write-Host "That means for LARGE number of items in `"$path`" it will only download matching items"
    Write-Host "    dir -Path $path -Filter $filter \n\n"
    dir -Path $path -Filter $filter
}

#Helper function to compute relative paths
function RelativePath([string]$path, [string]$base)
{  
    $fullPath = resolve-path $path
    $fullBase = resolve-path $base
    return $fullPath.Path.SubString($fullBase.Path.Length + 1)
#    return [system.io.path]::GetFullPath($path).SubString([system.io.path]::GetFullPath($base).Length + 1)
} 

#This function performs something similar to xcopy /d - copy files that are newer on source than destination
# Note: The script does not try to ensure that the data is not change during the execution of the script.
#       This means that if the file gets updated between the check in the script and the upload time, 
#       the upload will still happen.
function BackupData([string]$src, [string]$dst)
{
    # This version is written in classical iteration style, which requires all of the source files to be
    #  listed before it will proceed. This may pose a problem if there are too many files to fit into memory
    # Pipelined version could work around this limitation but is harder to read
    $files = get-childitem $src -recurse 
    
    foreach ($file in $files) 
    {
        $fileRelativePath = RelativePath $file.FullName $src
        #Write-Host $fileRelativePath
        $destFullPath = join-path $dst $fileRelativePath 
        #Write-Host $destFullPath
        
        # If destination doesn't exist, copy the file there
        if (!(Test-Path $destFullPath)) {
            Write-Host "Copying $file to $destFullPath"
            copy-cd $file.FullName $destFullPath
            continue
        }
        
        $destObject = get-item $destFullPath
        if ($file.LastWriteTimeUtc.CompareTo($destObject.LastWriteTimeUtc) -gt 0) {
            write-host "Updating because source file $fileRelativePath is newer than the destination $destFullPath"
            copy-cd $file.FullName $destFullPath
        }
    }
}


# Execute the backup script for current scripts
$scriptFolder = (split-path $MyInvocation.MyCommand.Definition)

if (!(get-psdrive -Name Blob -EA SilentlyContinue) ) {
  Invoke-expression (join-path $scriptFolder MountDrive.ps1)
}

# make sure the container exists
new-item blob:\backup-scripts | out-null

BackupData $scriptFolder blob:\backup-scripts
ListFiltered -Path blob:\backup-scripts -filter Sample*