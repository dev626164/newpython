﻿# A small sample on using CloudDrive to drive a worker role
# This script depends on Thumbnails sample and inserts elements into the work queue
SET-PSDEBUG -STRICT


function CreateThumbnail([string]$PictureSource, 
    [string]$BlobPath = "Blob:\photogallery\",
    [string]$QueuePath = "Queue:\thumbnailmaker") {
    
  Write-Host Copying images from $PictureSource
  
  $queue = get-item $QueuePath -EA SilentlyContinue 
  
  if (!$queue) {
    Write-Host Creating the work queue
    $queue = new-item $QueuePath
   }
   
   if (!$queue) {
     Write-Host Unable to access the queue. Please make sure `"$QueuePath`" is valid.
     return
   }
  
  # Make sure container exist
  
  if (! (test-path -Path $BlobPath)) {
    Write-Host Creating the destination location
    new-item $BlobPath -EA SilentlyContinue 
  }
  
  if (! (test-path -Path $BlobPath)) {
    Write-Host Unable to access the blob service. Please make sure `"$BlobPath`" is valid.
    return
  }
  
  #Copy the file 
  
  foreach ($file in (get-childitem -Recurse $PictureSource)) {
    Write-Host Uploading $file
    $dstFile = $BlobPath + "\\" + $file.Name

    copy-cd $file.FullName $dstFile 
    Write-Host "Inserting" $file.Name "into the queue"
    $queue.PutMessage($file.Name) |out-null
  }
}


Write-Host This Script uses Thumbnail sample to generate thumbnails using the Worker role
Write-Host For best effect, please launch the Thumbnail sample

if (!(get-psdrive -Name queue -EA SilentlyContinue) -or !(get-psdrive -Name Blob -EA SilentlyContinue) ) {
  Invoke-expression (join-path (split-path $MyInvocation.MyCommand.Definition) MountDrive.ps1)
}


# Look for an image. Windows Wallpaper ship with every OS
$wallpapers = resolve-path (join-path $env:SystemRoot "web\wallpaper\")

CreateThumbnail -PictureSource $wallpapers