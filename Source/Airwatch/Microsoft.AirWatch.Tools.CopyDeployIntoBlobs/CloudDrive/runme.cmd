@echo off
setlocal

:: Check to see if PowerShell is already installed by running it.
%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell -NoProfile /? >NUL
IF %ERRORLEVEL%==0 GOTO Run

:Usage
echo Please make sure that PowerShell is installed and activated
GOTO :EOF

:Run

if EXIST "..\rundevstore.cmd" (
  echo Starting DevStore
  start ..\rundevstore.cmd
) else (
  echo *******
  echo Devstore is not started. Make sure you run devstore before accessing the service. 
  echo If you are using non-local storage, make sure to edit scripts/mountdrive.ps1
  echo *******
)

rem Checking for correct execution policy
%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe  -NoProfile -Command ".\scripts\InstallDrive" >NUL

rem Try to set the policy if it's not valid
if %ERRORLEVEL%==1 (
  %SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe  -NoProfile Set-ExecutionPolicy RemoteSigned >NUL
)

rem Still not allowed to execute, Notify the user
if %ERRORLEVEL%==1 (
  echo The current execution policy is preventing execution of the scripts. Please run the following command in an elevated prompt:
  echo %SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe  -NoProfile Set-ExecutionPolicy RemoteSigned
  echo See http://msdn.microsoft.com/en-us/library/bb648601.aspx for details on PowerShell Execution Policy
  goto :EOF
)

rem Install the drive
%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe  -NoProfile -Command ".\scripts\InstallDrive ..\..\..\..\ReferencedAssemblies\StorageClient\clouddrive.dll"

if %ERRORLEVEL%==1 (
  goto :EOF
)
endlocal

echo ****Starting PowerShell prompt ****
echo To switch to Blob drive: cd Blob:
echo To display the contents: dir 
echo ...
%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -NoExit -Command ".\Scripts\MountDrive.ps1" 
