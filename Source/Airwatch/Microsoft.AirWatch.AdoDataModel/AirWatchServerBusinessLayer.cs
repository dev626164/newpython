﻿// <copyright file="AirWatchServerBusinessLayer.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-05-2009</date>
// <summary>AirWatchServerBusinessLayer</summary>
namespace Microsoft.AirWatch.AdoDataModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Server business layer
    /// </summary>
    public class AirWatchServerBusinessLayer : IDisposable
    {
        /// <summary>
        /// Degrees offset
        /// </summary>
        private const decimal DegreesOffset = 0.5m;

        /// <summary>
        /// The instance
        /// </summary>
        private static AirWatchServerBusinessLayer instance;

        /// <summary>
        /// Data proxy
        /// </summary>
        private AirWatchEntities serverDataProxy;

        #region Constructor + Instance
        /// <summary>
        /// Prevents a default instance of the AirWatchServerBusinessLayer class from being created.
        /// </summary>
        private AirWatchServerBusinessLayer()
        {
            this.serverDataProxy = new AirWatchEntities();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static AirWatchServerBusinessLayer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AirWatchServerBusinessLayer();
                }

                return instance;
            }
        }
        #endregion

        /// <summary>
        /// Gets the Degrees offset
        /// </summary>
        public static decimal DegreesOffsetProp
        {
            get
            {
                return DegreesOffset;
            }
        }

        /// <summary>
        /// Gets the centre location for point. 
        /// Returns null if not found
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>Centre location</returns>
        public CentreLocation GetCentreLocationForPoint(decimal longitude, decimal latitude)
        {
            var query = from CentreLocation centreLoc in this.serverDataProxy.GetCentreLocationForPoint(longitude, latitude, DegreesOffset)
                        select centreLoc;

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Gets the station for station code.
        /// Returns null if not found
        /// </summary>
        /// <param name="stationEuropeanCode">The station european code.</param>
        /// <returns>Station entity</returns>
        public Station GetStationForStationCode(string stationEuropeanCode)
        {
            var query = from Station station in this.serverDataProxy.GetStationForStationEuropeanCode(stationEuropeanCode)
                        select station;

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Gets measurements for point.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>List of Measurement entity</returns>
        public IEnumerable<Measurement> GetMeasurementsForPoint(decimal longitude, decimal latitude)
        {
            var query = from Measurement measurement in this.serverDataProxy.GetIndexForPoint(longitude, latitude, DegreesOffset, "CAQI")
                        select measurement;

            return query;
        }

        /// <summary>
        /// Gets the index for point.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="caption">The caption.</param>
        /// <returns>A quality index value</returns>
        public decimal? GetIndexForPoint(decimal longitude, decimal latitude, string caption)
        {
            var query = from Measurement measurement in this.serverDataProxy.GetIndexForPoint(longitude, latitude, DegreesOffset, caption)
                        select measurement;

            Measurement measure = query.FirstOrDefault();

            return measure == null ? null : measure.Value;
        }

        /// <summary>
        /// Gets the language for culture.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>The language for a culture</returns>
        public Language GetLanguageForCulture(string culture)
        {
            var query = from Language language in this.serverDataProxy.GetLanguageForCulture(culture)
                        select language;

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Get all languages from the database
        /// </summary>
        /// <returns>Array of Languages</returns>
        public Language[] GetLanguages()
        {
            try
            {
                return (from Language language in this.serverDataProxy.Language
                        select language).ToArray();
            }
            catch (ArgumentException)
            {
                return new Language[0];
            }
        }

        #region IDisposable Members

        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
            this.serverDataProxy.Dispose();
        }

        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        /// <param name="onlyNative">if true will only dispose of the native resources</param>
        protected virtual void Dispose(bool onlyNative)
        {
            if (!onlyNative)
            {
                this.serverDataProxy.Dispose();
            }
        }

        #endregion
    }
}
