﻿//-----------------------------------------------------------------------
// <copyright file="AirWatchUsersBusinessLayer.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The Fifth Medium.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>9 June 2009</date>
// <summary>Class to expose functionality to the database
//  Concurrency from http://msdn.microsoft.com/en-us/library/bb399228.aspx
// </summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.AdoDataModel
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Microsoft.AirWatch.AdoDataModel;

    /// <summary>
    /// Class to expose functionality to the database
    /// </summary>
    public class AirWatchUsersBusinessLayer : IDisposable
    {
        /// <summary>
        /// The instance
        /// </summary>
        private static AirWatchUsersBusinessLayer instance;

        /// <summary>
        /// The context for the user entities
        /// </summary>
        private AirWatchEntities userDataProxy;

        #region Constructor + Instance
        /// <summary>
        /// Prevents a default instance of the AirWatchUsersBusinessLayer class from being created
        /// </summary>
        private AirWatchUsersBusinessLayer()
        {
            this.userDataProxy = new AirWatchEntities();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static AirWatchUsersBusinessLayer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AirWatchUsersBusinessLayer();
                }

                return instance;
            }
        }
        #endregion

        #region Creation and Public Loading
        /// <summary>
        /// Loads a user, if the user is not found returns null
        /// if more than one user with the userId is found throws an InvalidOperationException
        /// This will also leave the context open for later use so the User returned isn't cached
        /// </summary>
        /// <param name="userId">The unique identifier of a user from .net authentication</param>
        /// <returns>The user if found, null if it is not found or throws InvalidOperationException if there are more than one found</returns>
        public User LoadUser(Guid userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId", "The userId argument when Finding a user was null");
            }

            User userToReturn = null;

            var users = from user in this.userDataProxy.User.Include("Language").Include("UserHomePushpin.Pushpin.Location")
                        where user.UserId == userId
                        select user;

            // There is a user so return it
            if (users.Count() == 1)
            {
                userToReturn = users.First();
            }
            else if (users.Count() > 1)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The Database has {0} entries relating to id {1}, this is an error as it should be unique", users.Count(), userId));
            }

            return userToReturn;
        }

        /// <summary>
        /// Creates or Loads a user depending whether the user guid is found
        /// </summary>
        /// <param name="userId">The unique identifier of a user from .net authentication</param>
        /// <returns>If the user with the guid does not exist, creates a new user and returns that object
        /// otherwise returns that user object with the guid 
        /// </returns>
        public User CreateUser(Guid userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId", "The userId argument when Finding a user was null");
            }

            User userToReturn = this.LoadUser(userId);

            if (userToReturn == null)
            {
                // There are no users with that ID so create one
                User newUser = new User()
                {
                    UserId = userId,
                };

                this.userDataProxy.AddToUser(newUser);
                this.userDataProxy.SaveChanges();

                userToReturn = newUser;
            }

            return userToReturn;
        }

        /// <summary>
        /// Allows the user to create a pushpin
        /// </summary>
        /// <param name="longitude">The longitude of the pushpin</param>
        /// <param name="latitude">The latitude of the pushpin</param>
        /// <returns>A pushpin object at the location specified</returns>
        public Pushpin CreatePushpin(decimal longitude, decimal latitude)
        {
            Location pushpinLocation = new Location()
            {
                Longitude = longitude,
                Latitude = latitude,
            };
            this.userDataProxy.AddToLocation(pushpinLocation);

            // Get the Center Location from the Server Business Layer
            CentreLocation closestCentreLocation = (from CentreLocation centreLoc in this.userDataProxy.GetCentreLocationForPoint(longitude, latitude, AirWatchServerBusinessLayer.DegreesOffsetProp)
                                                    select centreLoc).FirstOrDefault();

            Pushpin pushpinToReturn = new Pushpin()
            {
                Location = pushpinLocation,
                CentreLocation = closestCentreLocation,
            };
            this.userDataProxy.AddToPushpin(pushpinToReturn);

            try
            {
                this.userDataProxy.SaveChanges();
            }
            catch (OptimisticConcurrencyException)
            {
                this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, closestCentreLocation);

                this.userDataProxy.SaveChanges();
            }
            catch (EntityException)
            {
                this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, closestCentreLocation);

                this.userDataProxy.SaveChanges();
            }

            return pushpinToReturn;
        }

        /// <summary>
        /// Create an alert for a station
        /// </summary>
        /// <param name="threshold">The threshold for the alert</param>
        /// <param name="userFavoriteStationId">The favorite station ID to add the alert to</param>
        /// <returns>The created alert, or null if the alert couldn't be created</returns>
        public Alert CreateAlertForStation(int threshold, long userFavoriteStationId)
        {
            UserFavoriteStation favoriteStation = this.LoadUserFavoriteStation(userFavoriteStationId);

            Alert alertToReturn = null;

            if (favoriteStation != null)
            {
                alertToReturn = new Alert()
                {
                    Threshold = threshold,
                    NotifiedBad = false,
                };
                favoriteStation.Alert = alertToReturn;
                this.userDataProxy.AddToAlert(alertToReturn);

                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, favoriteStation);
                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, favoriteStation);
                    this.userDataProxy.SaveChanges();
                }
            }

            return alertToReturn;
        }

        /// <summary>
        /// Create an alert for a pushpin
        /// </summary>
        /// <param name="threshold">The threshold for the alert</param>
        /// <param name="userFavoritePushpinId">The favorite pushpin ID to add the alert to</param>
        /// <returns>The created alert, or null if the alert couldn't be created</returns>
        public Alert CreateAlertForPushpin(int threshold, long userFavoritePushpinId)
        {
            UserFavoritePushpin favoritePushpin = this.LoadUserFavoritePushpin(userFavoritePushpinId);

            Alert alertToReturn = null;

            if (favoritePushpin != null)
            {
                alertToReturn = new Alert()
                {
                    Threshold = threshold,
                    NotifiedBad = false,
                };
                favoritePushpin.Alert = alertToReturn;
                this.userDataProxy.AddToAlert(alertToReturn);

                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, favoritePushpin);
                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, favoritePushpin);
                    this.userDataProxy.SaveChanges();
                }
            }

            return alertToReturn;
        }

        /// <summary>
        /// Create a User Rating from a location
        /// </summary>
        /// <param name="locationId">The Id of the location to rate</param>
        /// <param name="rating">The rating value the user gave</param>
        /// <returns>The user rating object created</returns>
        public UserRating CreateUserRatingForLocation(long locationId, int rating)
        {
            Location location = this.LoadLocation(locationId);

            UserRating userRatingToReturn = null;

            if (location != null)
            {
                userRatingToReturn = new UserRating()
                {
                    Rating = rating,
                    Location = location,
                    RatingTime = DateTime.Now,
                };

                this.userDataProxy.AddToUserRating(userRatingToReturn);

                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, location);
                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, location);
                    this.userDataProxy.SaveChanges();
                }
            }

            return userRatingToReturn;
        }

        #region Favorites
        /// <summary>
        /// Add a station to the user's favorites
        /// </summary>
        /// <param name="userId">Guid of the user to add the favorite to</param>
        /// <param name="stationId">The unique id of the station</param>
        /// <returns>The UserFavoriteStation created</returns>
        public UserFavoriteStation AddUserFavoriteStation(Guid userId, string stationId)
        {
            // Use the database version of the user rather than the cached version, otherwise it will not add to db.
            User user = this.LoadUser(userId);

            UserFavoriteStation userFavoriteStation = null;

            if (user != null)
            {
                Station station = AirWatchServerBusinessLayer.Instance.GetStationForStationCode(stationId);

                userFavoriteStation = new UserFavoriteStation()
                {
                    Station = station,
                    User = user,
                };

                user.UserFavoriteStation.Add(userFavoriteStation);
                
                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, user);
                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, user);
                    this.userDataProxy.SaveChanges();
                }
            }

            return userFavoriteStation;
        }

        /// <summary>
        /// Add a Pushpin to the user's favorites
        /// </summary>
        /// <param name="userId">Guid of the user to add the favorite to</param>
        /// <param name="pushpinId">The unique id of the Pushpin</param>
        /// <returns>The UserFavoritePushpin created</returns>
        public UserFavoritePushpin AddUserFavoritePushpin(Guid userId, long pushpinId)
        {
            User user = this.LoadUser(userId);
            Pushpin pushpin = this.LoadPushpin(pushpinId);

            UserFavoritePushpin userFavoritePushpin = null;

            if (user != null && pushpin != null)
            {
                userFavoritePushpin = new UserFavoritePushpin()
                {
                    Pushpin = pushpin,
                    User = user,
                };

                this.userDataProxy.AddToUserFavoritePushpin(userFavoritePushpin);

                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, user);
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, pushpin);
                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, user);
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, pushpin);
                    this.userDataProxy.SaveChanges();
                }
            }

            return userFavoritePushpin;
        }

        #endregion

        #endregion

        #region Preference Setting
        /// <summary>
        /// Update a user from a user object
        /// </summary>
        /// <param name="user">The user to update</param>
        /// <returns>The updated user</returns>
        public User UpdateUser(User user)
        {
            if (user != null)
            {
                try
                {
                    this.userDataProxy.ApplyPropertyChanges("User", user);
                    this.userDataProxy.SaveChanges();

                    this.SetLanguage(user.UserId, user.Language.LanguageCode);
                }
                catch (InvalidOperationException ex)
                {
                    throw new InvalidOperationException("Error when trying to update the user", ex);
                }
            }

            return user;
        }

        /// <summary>
        /// Sets the user's mobile number and language (for alerting)
        /// </summary>
        /// <param name="userId">User Identifying GUID</param>
        /// <param name="mobile">Mobile Number</param>
        /// <param name="language">The Languages for the alerts</param>
        /// <returns>The updated user object</returns>
        public User SetMobile(Guid userId, string mobile, string language)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("user", "The user argument when setting the mobile number for a user was null");
            }

            User user = this.SetLanguage(userId, language);

            if (user != null)
            {
                user.MobileNumber = mobile;

                this.userDataProxy.SaveChanges();
            }

            return user;
        }

        /// <summary>
        /// Sets the user's language
        /// </summary>
        /// <param name="userId">User Identifying GUID</param>
        /// <param name="culture">The users culture</param>
        /// <returns>The updated user object</returns>
        public User SetLanguage(Guid userId, string culture)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("user", "The user argument when setting the language for a user was null");
            }

            var language = (from lang in this.userDataProxy.Language
                            where lang.LanguageCode == culture
                            select lang).First();

            // try to find the language in the Language table
            if (language == null)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Unable to find language: {0}", culture));
            }

            User user = this.LoadUser(userId);

            if (user != null)
            {
                user.LanguageReference.EntityKey = language.EntityKey;

                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, language);
                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, language);
                    this.userDataProxy.SaveChanges();
                }
            }

            return user;
        }

        /// <summary>
        /// Sets the user's home location pushpin
        /// </summary>
        /// <param name="userId">User Identifying GUID</param>
        /// <param name="favoritePushpin">userFavoritePushpin to set as the home location</param>
        /// <returns>The updated user object</returns>
        public User SetHomeLocation(Guid userId, UserFavoritePushpin favoritePushpin)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("user", "The user argument when setting the home location for a user was null");
            }

            if (favoritePushpin == null)
            {
                throw new ArgumentNullException("favoritePushpin", "The favoritePushpin argument when setting the home location for a user was null");
            }

            User user = this.LoadUser(userId);
            UserFavoritePushpin favePushpin = this.LoadUserFavoritePushpin(favoritePushpin.UserFavoritePushpinId);

            if (user != null && favePushpin != null)
            {
                user.UserHomePushpinReference.EntityKey = favePushpin.EntityKey;

                try
                {
                    this.userDataProxy.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, favePushpin);
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, user);

                    this.userDataProxy.SaveChanges();
                }
                catch (EntityException)
                {
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, favePushpin);
                    this.userDataProxy.Refresh(System.Data.Objects.RefreshMode.ClientWins, user);

                    this.userDataProxy.SaveChanges();
                }
            }

            return user;
        }

        #endregion

        #region Deletion
        /// <summary>
        /// Delete a user and all associated data on the system
        /// </summary>
        /// <param name="userId">The user to delete</param>
        public void DeleteUserAndAssociatedData(Guid userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId", "The userId Guid argument when Deleting a user was null");
            }

            User user = this.LoadUser(userId);

            if (user != null)
            {
                user.UserFavoriteStation.Load();

                //// remove all associated Favorite Stations
                //// Cannot use foreach as the remove statement would break the enumeration
                while (user.UserFavoriteStation.Count > 0)
                {
                    UserFavoriteStation favoriteStation = user.UserFavoriteStation.First();

                    // delete the UserFavoriteStation in the database
                    this.DeleteFavoriteStationAndAssociatedData(favoriteStation.UserFavoriteStationId);

                    // and remove the link
                    user.UserFavoriteStation.Remove(favoriteStation);
                }

                user.UserFavoritePushpin.Load();

                // remove all associated Favorite Pushpins
                while (user.UserFavoritePushpin.Count > 0)
                {
                    UserFavoritePushpin favoritePushpin = user.UserFavoritePushpin.First();

                    // delete the UserFavoritePushpin in the database 
                    this.DeleteFavoritePushpinAndAssociatedData(favoritePushpin.UserFavoritePushpinId);
                }

                // finally delete the user object
                this.userDataProxy.DeleteObject(user);
                this.userDataProxy.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("Tried to delete a user which was not found in the database");
            }
        }

        /// <summary>
        /// Deletes a user's home location (just removes the link, not the actual UserFavoritePushpin)
        /// </summary>
        /// <param name="userId">The id of the user</param>
        public void DeleteHomeLocation(Guid userId)
        {
            User user = this.LoadUser(userId);

            if (user != null)
            {
                user.UserHomePushpin = null;
            }

            this.userDataProxy.SaveChanges();
        }

        /// <summary>
        /// Delete a user's Favorite Station and all associated Data (alerts)
        /// </summary>
        /// <param name="userFavoriteStationId">The UserFavoriteStation's Id to delete</param>
        public void DeleteFavoriteStationAndAssociatedData(long userFavoriteStationId)
        {
            UserFavoriteStation favoriteStation = this.LoadUserFavoriteStation(userFavoriteStationId);

            if (favoriteStation.Alert != null)
            {
                this.userDataProxy.DeleteObject(favoriteStation.Alert);
            }

            // delete the favorite station
            this.userDataProxy.DeleteObject(favoriteStation);
            this.userDataProxy.SaveChanges();
        }

        /// <summary>
        /// Delete a user's Favorite Pushpin and all associated Data (alerts and Pushpin)
        /// </summary>
        /// <param name="userFavoritePushpinId">The UserFavoritePushpin's Id to delete</param>
        public void DeleteFavoritePushpinAndAssociatedData(long userFavoritePushpinId)
        {
            UserFavoritePushpin favoritePushpin = this.LoadUserFavoritePushpin(userFavoritePushpinId);

            // delete any associated alerts
            if (favoritePushpin.Alert != null)
            {
                this.userDataProxy.DeleteObject(favoritePushpin.Alert);
            }

            // Check to see if the user has a home pushpin first, if it's the user's home location, set their home location to be null
            if (favoritePushpin.User.UserHomePushpin != null && favoritePushpin.User.UserHomePushpin.UserFavoritePushpinId == favoritePushpin.UserFavoritePushpinId)
            {
                this.DeleteHomeLocation(favoritePushpin.User.UserId);
            }

            // delete the associated pushpin
            if (favoritePushpin.Pushpin != null)
            {
                this.userDataProxy.DeleteObject(favoritePushpin.Pushpin);
            }

            // finally delete the favoritePushpin
            this.userDataProxy.DeleteObject(favoritePushpin);

            this.userDataProxy.SaveChanges();
        }

        /// <summary>
        /// Deletes a Pushpin
        /// </summary>
        /// <param name="pushpinId">The Id of the Pushpin to delete</param>
        public void DeletePushpin(long pushpinId)
        {
            Pushpin pushpin = this.LoadPushpin(pushpinId);

            if (pushpin != null)
            {
                // if there are no ratings then delete the location
                if (pushpin.Location.UserRating.Count == 0)
                {
                    this.userDataProxy.DeleteObject(pushpin.Location);
                }

                this.userDataProxy.DeleteObject(pushpin);
                this.userDataProxy.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes an Alert
        /// </summary>
        /// <param name="alertId">The Alert to delete</param>
        public void DeleteAlert(long alertId)
        {
            Alert alert = this.LoadAlert(alertId);

            if (alert != null)
            {
                this.userDataProxy.DeleteObject(alert);
                this.userDataProxy.SaveChanges();
            }
        }

        #endregion

        #region IDisposable Members
        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
            this.userDataProxy.Dispose();
        }

        /// <summary>
        /// When disposing of the class, also dispose of the AirWatchUsersEntities
        /// </summary>
        /// <param name="onlyNative">if true will only dispose of the native resources</param>
        protected virtual void Dispose(bool onlyNative)
        {
            if (!onlyNative)
            {
                this.userDataProxy.Dispose();
            }
        }

        #endregion

        #region Private Loaders
        /// <summary>
        /// Loads a UserFavoriteStation, if it is not found returns null
        /// if more than one is found throws an InvalidOperationException
        /// This will also leave the context open for later use so the UserFavoriteStation returned isn't cached
        /// </summary>
        /// <param name="userFavoriteStationId">The unique identifier of a favorite station</param>
        /// <returns>The favorite station if found, null if it is not found or throws InvalidOperationException if there are more than one found</returns>
        private UserFavoriteStation LoadUserFavoriteStation(long userFavoriteStationId)
        {
            UserFavoriteStation userFavoriteStationToReturn = null;

            var userFavoriteStations = from userFavStat in this.userDataProxy.UserFavoriteStation
                                       where userFavStat.UserFavoriteStationId == userFavoriteStationId
                                       select userFavStat;

            // There is a user favorite station so return it
            if (userFavoriteStations.Count() == 1)
            {
                userFavoriteStationToReturn = userFavoriteStations.First();
            }
            else if (userFavoriteStations.Count() > 1)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The Database has {0} entries relating to id {1}, this is an error as it should be unique", userFavoriteStations.Count(), userFavoriteStationId));
            }

            return userFavoriteStationToReturn;
        }

        /// <summary>
        /// Loads a UserFavoritePushpin, if it is not found returns null
        /// if more than one is found throws an InvalidOperationException
        /// This will also leave the context open for later use so the UserFavoritePushpin returned isn't cached
        /// </summary>
        /// <param name="userFavoritePushpinId">The unique identifier of a favorite pushpin</param>
        /// <returns>The favorite pushpin if found, null if it is not found or throws InvalidOperationException if there are more than one found</returns>
        private UserFavoritePushpin LoadUserFavoritePushpin(long userFavoritePushpinId)
        {
            UserFavoritePushpin userFavoritePushpinToReturn = null;

            var userFavoritePushpins = from userFavPush in this.userDataProxy.UserFavoritePushpin
                                       where userFavPush.UserFavoritePushpinId == userFavoritePushpinId
                                       select userFavPush;

            // There is a user favorite fushpin so return it
            if (userFavoritePushpins.Count() == 1)
            {
                userFavoritePushpinToReturn = userFavoritePushpins.First();
            }
            else if (userFavoritePushpins.Count() > 1)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The Database has {0} entries relating to id {1}, this is an error as it should be unique", userFavoritePushpins.Count(), userFavoritePushpinId));
            }

            return userFavoritePushpinToReturn;
        }

        /// <summary>
        /// Loads a Pushpin, if it is not found returns null
        /// if more than one is found throws an InvalidOperationException
        /// This will also leave the context open for later use so the Pushpin returned isn't cached
        /// </summary>
        /// <param name="pushpinId">The unique identifier of a pushpin</param>
        /// <returns>The pushpin if found, null if it is not found or throws InvalidOperationException if there are more than one found</returns>
        private Pushpin LoadPushpin(long pushpinId)
        {
            Pushpin pushpinToReturn = null;

            var pushpins = from pin in this.userDataProxy.Pushpin
                           where pin.PushpinId == pushpinId
                           select pin;

            // There is a pushpin so return it
            if (pushpins.Count() == 1)
            {
                pushpinToReturn = pushpins.First();
            }
            else if (pushpins.Count() > 1)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The Database has {0} entries relating to id {1}, this is an error as it should be unique", pushpins.Count(), pushpinId));
            }

            return pushpinToReturn;
        }

        /// <summary>
        /// Loads an Alert, if it is not found returns null
        /// if more than one is found throws an InvalidOperationException
        /// This will also leave the context open for later use so the Alert returned isn't cached
        /// </summary>
        /// <param name="alertId">The unique identifier of an alert</param>
        /// <returns>The alert if found, null if it is not found or throws InvalidOperationException if there are more than one found</returns>
        private Alert LoadAlert(long alertId)
        {
            Alert alertToReturn = null;

            var alerts = from alert in this.userDataProxy.Alert
                         where alert.AlertId == alertId
                         select alert;

            // There is an alert so return it
            if (alerts.Count() == 1)
            {
                alertToReturn = alerts.First();
            }
            else if (alerts.Count() > 1)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The Database has {0} entries relating to id {1}, this is an error as it should be unique", alerts.Count(), alertId));
            }

            return alertToReturn;
        }

        /// <summary>
        /// Loads a Location, if it is not found returns null
        /// if more than one is found throws an InvalidOperationException
        /// This will also leave the context open for later use so the Location returned isn't cached
        /// </summary>
        /// <param name="locationId">The unique identifier of a location</param>
        /// <returns>The location if found, null if it is not found or throws InvalidOperationException if there are more than one found</returns>
        private Location LoadLocation(long locationId)
        {
            Location locationToReturn = null;

            var locations = from location in this.userDataProxy.Location
                            where location.LocationId == locationId
                            select location;

            // There is a location so return it
            if (locations.Count() == 1)
            {
                locationToReturn = locations.First();
            }
            else if (locations.Count() > 1)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The Database has {0} entries relating to id {1}, this is an error as it should be unique", locations.Count(), locationId));
            }

            return locationToReturn;
        }

        #endregion
    }
}
