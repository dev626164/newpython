//-----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>17/07/09</date>
// <summary>Global Suppressions file</summary>
//----------------------------------------------------------------------- 

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.LanguageService.#GetLanguage(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Microsoft.AirWatch.Core.ApplicationServices.DataStructures")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Caqi", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.DataStructures.MapModelPoint.#Caqi")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.LanguageService.#GetLanguages()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.PushpinService.#GetPushpin(System.Decimal,System.Decimal)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.StationService.#GetMeasurementOfType(System.Xml.Linq.XElement,System.Xml.Linq.XName)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Caqi", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.DataStructures.MapModelPoint.#CaqiValue")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#GenerateTileImage(System.Collections.Generic.List`1<Microsoft.AirWatch.Core.Data.UserRating>,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#Clip(System.Double,System.Double,System.Double)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#ConvertBinaryTileToDecimal(System.String,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#ConvertPixelPositionToLat(System.Double,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#ConvertPixelPositionToLong(System.Double,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#CreateBinaryQuadKey(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#GetBinaryTileCoOrdinates(System.String,System.String&,System.String&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ForGiven", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#GetQuadKeyForGivenLocation(System.Decimal,System.Decimal,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#GetStationsInTile(System.Double,System.Double,System.Double,System.Double,Microsoft.AirWatch.Core.ApplicationServices.LightMapType)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#GetUserStationsInTile(System.Double,System.Double,System.Double,System.Double,Microsoft.AirWatch.Core.ApplicationServices.LightMapType)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#MapSize(System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#PixelXYToTileXY(System.Int32,System.Int32,System.Int32&,System.Int32&)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#TileXYToQuadKey(System.Int32,System.Int32,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.StationService.#GetAddressFromLocation(System.Decimal,System.Decimal)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Geocode", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.BingMapServicesHelper.#CallReverseGeocodeServices(System.String,System.String,System.Double,System.Double)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.LanguageService.#GetTimeStamp(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.LanguageService.#GetTimestamp(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Microsoft.AirWatch.Core.ApplicationServices.TileImageService.#GenerateTileImage(System.Collections.Generic.List`1<Microsoft.AirWatch.Core.Data.UserRating>,System.String,System.Boolean)")]
