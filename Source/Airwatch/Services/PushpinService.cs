﻿//-----------------------------------------------------------------------
// <copyright file="PushpinService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Service for pushpin related transactions</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    #region Using

    using Microsoft.AirWatch.Core.Data;

    #endregion

    /// <summary>
    /// Service for pushpin related transactions
    /// </summary>
    public class PushpinService
    {
        /// <summary>
        /// Internal representation of Entity Framework data context.
        /// </summary>
        private DataContext dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushpinService"/> class.
        /// </summary>
        internal PushpinService()
        {
            this.dataContext = DataContext.Create();
        }

        #region Loading And Creation
        /// <summary>
        /// Allows the user to create a pushpin
        /// </summary>
        /// <param name="longitude">The longitude of the pushpin</param>
        /// <param name="latitude">The latitude of the pushpin</param>
        /// <returns>
        /// A pushpin object at the location specified
        /// </returns>
        public Pushpin GetPushpin(decimal longitude, decimal latitude)
        {
            return CreatePushpin(longitude, latitude);
        }        

        /// <summary>
        /// Allows the user to create a pushpin
        /// </summary>
        /// <param name="longitude">The longitude of the pushpin</param>
        /// <param name="latitude">The latitude of the pushpin</param>
        /// <param name="name">The name of the pushpin.</param>
        /// <returns>
        /// A pushpin object at the location specified.
        /// </returns>
        public Pushpin CreatePushpin(decimal longitude, decimal latitude, string name)
        {
            Location pushpinLocation = new Location()
            {
                Longitude = longitude,
                Latitude = latitude,
            };
            this.dataContext.AirWatchEntities.AddToLocation(pushpinLocation);

            // Get the Center Location from the Server Business Layer
            CentreLocation closestCentreLocation = ServiceProvider.CenterLocationService.GetCentreLocationForPoint(longitude, latitude);

            Pushpin pushpinToReturn = new Pushpin()
            {
                Location = pushpinLocation,
                FriendlyName = name,
            };

            pushpinToReturn.CentreLocation = closestCentreLocation;

            return pushpinToReturn;
        }

        #endregion

        /// <summary>
        /// Gets the pushpin.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>A pushpin object at the location specified</returns>
        private static Pushpin CreatePushpin(decimal longitude, decimal latitude)
        {
            Location pushpinLocation = new Location()
            {
                Longitude = longitude,
                Latitude = latitude,
            };

            // Get the Center Location from the Server Business Layer
            CentreLocation closestCentreLocation = ServiceProvider.CenterLocationService.GetCentreLocationForPoint(longitude, latitude);

            if (closestCentreLocation != null)
            {
                if (closestCentreLocation.LocationReference != null)
                {
                    closestCentreLocation.LocationReference.Load();
                }

                if (closestCentreLocation.CentreLocationMeasurement != null)
                {
                    closestCentreLocation.CentreLocationMeasurement.Load();

                    foreach (CentreLocationMeasurement centreLocationMeasurement in closestCentreLocation.CentreLocationMeasurement)
                    {
                        if (centreLocationMeasurement.MeasurementReference != null)
                        {
                            centreLocationMeasurement.MeasurementReference.Load();

                            if (centreLocationMeasurement.Measurement != null)
                            {
                                if (centreLocationMeasurement.Measurement.ComponentReference != null)
                                {
                                    centreLocationMeasurement.Measurement.ComponentReference.Load();

                                    if (centreLocationMeasurement.Measurement.Component != null)
                                    {
                                        centreLocationMeasurement.Measurement.Component.Threshold.Load();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Pushpin pushpinToReturn = new Pushpin()
            {
                Location = pushpinLocation,
                CentreLocation = closestCentreLocation
            };

            return pushpinToReturn;
        }
    }
}
