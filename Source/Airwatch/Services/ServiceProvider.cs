﻿//-----------------------------------------------------------------------
// <copyright file="ServiceProvider.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Provides access to Application Services.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    /// <summary>
    /// Provides access to Application Services.
    /// </summary>
    public sealed class ServiceProvider
    {
        /// <summary>
        /// Tile Image Service.
        /// </summary>
        private static TileImageService tileImageService = new TileImageService();

        /// <summary>
        /// Prevents a default instance of the ServiceProvider class from being created.
        /// </summary>
        private ServiceProvider()
        {
        }

        /// <summary>
        /// Gets the pushpin service.
        /// </summary>
        /// <value>The pushpin service.</value>
        public static PushpinService PushpinService
        {
            get
            {
                return new PushpinService();
            }
        }

        /// <summary>
        /// Gets the station service.
        /// </summary>
        /// <value>The station service.</value>
        public static StationService StationService
        {
            get
            {
                return new StationService();
            }
        }

        /// <summary>
        /// Gets the rating service.
        /// </summary>
        /// <value>The rating service.</value>
        public static RatingService RatingService
        {
            get
            {
                return new RatingService();
            }
        }

        /// <summary>
        /// Gets the center location service.
        /// </summary>
        /// <value>The center location service.</value>
        public static CenterLocationService CenterLocationService
        {
            get
            {
                return new CenterLocationService();
            }
        }

        /// <summary>
        /// Gets the language service.
        /// </summary>
        /// <value>The language service.</value>
        public static LanguageService LanguageService
        {
            get
            {
                return new LanguageService();
            }
        }

        /// <summary>
        /// Gets the tile image service.
        /// </summary>
        /// <value>The tile image service.</value>
        public static TileImageService TileImageService
        {
            get
            {
                return tileImageService;
            }
        }
    }
}
