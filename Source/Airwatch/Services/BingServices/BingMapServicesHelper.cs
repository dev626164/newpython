﻿//-----------------------------------------------------------------------
// <copyright file="BingMapServicesHelper.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The Fifth Medium.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>9 June 2009</date>
// <summary>Bing Maps Helper</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    using System;
    using System.ServiceModel;
    using dev.virtualearth.net.webservices.v1.common;
    using dev.virtualearth.net.webservices.v1.geocode;
    using VECommonService;

    /// <summary>
    /// Bing Maps services helper
    /// </summary>
    public static class BingMapServicesHelper
    {
        /// <summary>
        /// News the token.
        /// </summary>
        /// <param name="clientIPAddress">The client IP address.</param>
        /// <returns>The bing token</returns>
        public static string GetBingToken(string clientIPAddress)
        {
                // Set Virtual Earth Platform Developer Account credentials to access the Token Service
                CommonService commonService = new CommonService();
                commonService.Credentials = new System.Net.NetworkCredential("142179", "airDevWatch11.");
                
                // Set the token specification properties
                TokenSpecification tokenSpec = new TokenSpecification()
                {
                    ClientIPAddress = clientIPAddress,
                    TokenValidityDurationMinutes = 480
                };

                string token = string.Empty;

                // Get a token
                try
                {
                    token = commonService.GetClientToken(tokenSpec);
                }
                catch (InvalidOperationException)
                {
                    // ApplicationEnvironment.LogError("Exception occured while retrieving a virtual earth services token: " + ex.Message);
                }

                return token;
        }

        /// <summary>
        /// Calls the bing map services.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="query">The query.</param>
        /// <returns>The response from the geocode service</returns>
        public static GeocodeResponse CallBingMapServices(string token, string culture, string query)
        {
            // ApplicationEnvironment.LogInformation("Calling the geocode service");
            GeocodeRequest geocodeRequest = new GeocodeRequest();

            // Set the credentials using a valid Virtual Earth token
            geocodeRequest.Credentials = new Credentials();
            geocodeRequest.Credentials.Token = token;
            geocodeRequest.Culture = culture;

            // Set the full address query
            geocodeRequest.Query = query;

            // Set the options to only return high confidence results 
            ConfidenceFilter[] filters = new ConfidenceFilter[1];
            filters[0] = new ConfidenceFilter();
            filters[0].MinimumConfidence = Confidence.Medium;

            GeocodeOptions geocodeOptions = new GeocodeOptions();
            geocodeOptions.Filters = filters;
            geocodeRequest.Options = geocodeOptions;

            GeocodeResponse geocodeResponse = new GeocodeResponse();

            try
            {
                using (GeocodeServiceClient client = new GeocodeServiceClient())
                {
                    // Make the geocode request
                    geocodeResponse = client.Geocode(geocodeRequest);
                }
            }
            catch (FaultException<dev.virtualearth.net.webservices.v1.common.ResponseSummary>)
            {
                throw;
            }
            catch (FaultException)
            {
                // ApplicationEnvironment.LogWarning("A fault exception happened when calling Map Services with query {0} and culture {1}", query, culture);
            }
            
            return geocodeResponse;
        }

        /// <summary>
        /// Calls the bing map services.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>The response from the geocode service</returns>
        public static GeocodeResponse CallReverseGeocodeServices(string token, string culture, double longitude, double latitude)
        {
            // ApplicationEnvironment.LogInformation("Calling the geocode service");
            ReverseGeocodeRequest geocodeRequest = new ReverseGeocodeRequest();

            // Set the credentials using a valid Virtual Earth token
            geocodeRequest.Credentials = new Credentials();
            geocodeRequest.Credentials.Token = token;
            geocodeRequest.Culture = culture;

            // Set the full address query
            geocodeRequest.Location = new Location()
            {
                Longitude = longitude,
                Latitude = latitude,
            };

            // Set the options to only return high confidence results 
            ConfidenceFilter[] filters = new ConfidenceFilter[1];
            filters[0] = new ConfidenceFilter();
            filters[0].MinimumConfidence = Confidence.Low;

            GeocodeOptions geocodeOptions = new GeocodeOptions();
            geocodeOptions.Filters = filters;

            GeocodeResponse geocodeResponse = new GeocodeResponse();

            try
            {
                using (GeocodeServiceClient client = new GeocodeServiceClient())
                {
                    // Make the geocode request
                    geocodeResponse = client.ReverseGeocode(geocodeRequest);
                }
            }
            catch (FaultException<dev.virtualearth.net.webservices.v1.common.ResponseSummary>)
            {
                throw;
            }
            catch (FaultException)
            {
                // ApplicationEnvironment.LogWarning("A fault exception happened when calling Map Services with query {0} and culture {1}", query, culture);
            }

            return geocodeResponse;
        }
    }
}
