﻿//-----------------------------------------------------------------------
// <copyright file="LightMapMessage.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>07/09/2009</date>
// <summary>Light Map Message.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    /// <summary>
    /// Light Map Message.
    /// </summary>
    public class LightMapMessage
    {
        /// <summary>
        /// Gets or sets the quad key.
        /// </summary>
        /// <value>The quad key.</value>
        public string QuadKey { get; set; }

        /// <summary>
        /// Gets or sets the type of the light map.
        /// </summary>
        /// <value>The type of the light map.</value>
        public LightMapType LightMapType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is overlap.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is overlap; otherwise, <c>false</c>.
        /// </value>
        public bool IsOverlap { get; set; }
    }
}
