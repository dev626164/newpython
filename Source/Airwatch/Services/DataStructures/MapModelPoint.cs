﻿//-----------------------------------------------------------------------
// <copyright file="MapModelPoint.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dave Thompson</author>
// <email>davet@microsoft.com</email>
// <date>14 July 2009</date>
// <summary>Class for modelling data of a map model point</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices.DataStructures
{
    /// <summary>
    /// Class for modelling data of a map model point
    /// </summary>
    public class MapModelPoint
    {
        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets the caqi.
        /// </summary>
        /// <value>The common air quality index.</value>
        public double Caqi { get; set; }

        /// <summary>
        /// Gets or sets the caqi value.
        /// </summary>
        /// <value>The common air quality value.</value>
        public double QualityValue { get; set; }

        /// <summary>
        /// Gets or sets the ozone.
        /// </summary>
        /// <value>The ozone.</value>
        public double Ozone { get; set; }

        /// <summary>
        /// Gets or sets the NO2.
        /// </summary>
        /// <value>The Nitrigen Dioxide.</value>
        public double NO2 { get; set; }

        /// <summary>
        /// Gets or sets the PM10.
        /// </summary>
        /// <value>The Particulate Matter.</value>
        public double PM10 { get; set; }
    }
}
