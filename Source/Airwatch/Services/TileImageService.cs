﻿//-----------------------------------------------------------------------
// <copyright file="TileImageService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Dominic Green</author>
// <email>dogreen@microsoft.com</email>
// <date>03/09/2009</date>
// <summary>Tile Image Service to deal with all tile generation / deletion.</summary>
//-----------------------------------------------------------------------
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml.Serialization;
    using Microsoft.AirWatch.Core.Data;
    using Microsoft.WindowsAzure;
    using Microsoft.WindowsAzure.StorageClient;
    using System.Diagnostics;

    /// <summary>
    /// Tile Image Service to deal with all tile generation / deletion.
    /// </summary>
    public class TileImageService
    {
        /// <summary>
        /// Offeset for Radius.
        /// </summary>
        private const double OFFSET = 0.00001;
        
        /// <summary>
        /// Store for the air location data.
        /// </summary>
        private static List<StationMinimal> airLocations;

        /// <summary>
        /// Store for the water location data.
        /// </summary>
        private static List<StationMinimal> waterLocations;

        /// <summary>
        /// Flag to identify if its the first user station load.
        /// </summary>
        private static bool firstUser = true;

        /// <summary>
        /// Air Station Image.
        /// </summary>
        private static Image airStationImage;

        /// <summary>
        /// Water Station Image.
        /// </summary>
        private static Image waterStationImage;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userWaterRatingImage1;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userWaterRatingImage2;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userWaterRatingImage3;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userWaterRatingImage4;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userWaterRatingImage5;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userAirRatingImage1;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userAirRatingImage2;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userAirRatingImage3;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userAirRatingImage4;

        /// <summary>
        /// User Feedback Station Image.
        /// </summary>
        private static Image userAirRatingImage5;

        /// <summary>
        /// Minimum Latitude.
        /// </summary>
        private double minLatitude = -85.05112878;

        /// <summary>
        /// Maximum Latitude.
        /// </summary>
        private double maxLatitude = 85.05112878;

        /// <summary>
        /// Minimum Longitude.
        /// </summary>
        private double minLongitude = -180;

        /// <summary>
        /// Maximum Longitude.
        /// </summary>
        private double maxLongitude = 180;

        /// <summary>
        /// Pixel X Minimum.
        /// </summary>
        private double pixelXMin;

        /// <summary>
        /// Pixel Y Minimum.
        /// </summary>
        private double pixelYMin;

        /// <summary>
        /// Azure blob container for air station images.
        /// </summary>
        private CloudBlobContainer airStationBlobContainer;

        /// <summary>
        /// Azure blob container for water station images.
        /// </summary>
        private CloudBlobContainer waterStationBlobContainer;

        /// <summary>
        /// Azure blob container for user feedback air images.
        /// </summary>
        private CloudBlobContainer userFeedbackAirBlobContainer;

        /// <summary>
        /// Azure blob container for user feedback water images.
        /// </summary>
        private CloudBlobContainer userFeedbackWaterBlobContainer;

        /// <summary>
        /// Heat map tile server queue
        /// </summary>
        private CloudQueue  lightMapQueue;

        /// <summary>
        /// Number of retrys to save in blob storage when generated a tile
        /// </summary>
        private int retry = 5;

        /// <summary>
        /// The light map type.
        /// </summary>
        private LightMapType lightMapType;

        private CloudStorageAccount account;
        private CloudBlobClient blobClient;
        private CloudQueueClient queueClient;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="TileImageService"/> class.
        /// </summary>
        internal TileImageService()
        {
            this.account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.blobClient = this.account.CreateCloudBlobClient();
            this.airStationBlobContainer = this.blobClient.GetContainerReference("airstationlightmap");
            this.airStationBlobContainer.CreateIfNotExist();
            this.airStationBlobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });

            this.waterStationBlobContainer = this.blobClient.GetContainerReference("waterstationlightmap");
            this.waterStationBlobContainer.CreateIfNotExist();
            this.waterStationBlobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });

            this.userFeedbackAirBlobContainer = this.blobClient.GetContainerReference("userfeedbackairlightmap");
            this.userFeedbackAirBlobContainer.CreateIfNotExist();
            this.userFeedbackAirBlobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });

            this.userFeedbackWaterBlobContainer = this.blobClient.GetContainerReference("userfeedbackwaterlightmap");
            this.userFeedbackWaterBlobContainer.CreateIfNotExist();
            this.userFeedbackWaterBlobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });
            
            this.queueClient = this.account.CreateCloudQueueClient();
            this.lightMapQueue = this.queueClient.GetQueueReference("lightmapqueue");
            this.lightMapQueue.CreateIfNotExist();
        }

        /// <summary>
        /// Res the generate tiles for new station.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="type">The type of light map.</param>
        public void GenerateTilesForNewStation(decimal longitude, decimal latitude, LightMapType type)
        {
            string quadKey = this.GetQuadKeyForGivenLocation(longitude, latitude, 11);
            string quadKeyBuilder = string.Empty;

            for (int i = 0; i < quadKey.Length; i++)
            {
                quadKeyBuilder += quadKey[i];

                LightMapMessage message = new LightMapMessage();
                message.QuadKey = quadKeyBuilder;
                message.LightMapType = type;
                message.IsOverlap = false;

                this.lightMapQueue.AddMessage(SerializeMessage(message));
            }
        }

        /// <summary>
        /// Gets the quad key for given long lat.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Level 10 Quad Key for given location.</returns>
        public string GetQuadKeyForGivenLocation(decimal longitude, decimal latitude, int zoomLevel)
        {
            int pixelX, pixelY, tileX, tileY;
            this.LatLongToPixelXY((double)latitude, (double)longitude, zoomLevel, 0, 0, out pixelX, out pixelY);
            this.PixelXYToTileXY(pixelX, pixelY, out tileX, out tileY);
            return this.TileXYToQuadKey(tileX, tileY, zoomLevel);
        }

        /// <summary>
        /// Generates from quad key.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="type">The type of the layer.</param>
        /// <param name="isOverlap">if set to <c>true</c> [is overlap].</param>
        /// <returns>
        /// Stream representing the image for the tile.
        /// </returns>
        public Stream GenerateFromQuadKey(string quadKey, LightMapType type, bool isOverlap)
        {
            this.lightMapType = type;

            // Determine Zoom-Level
            int zoomLevel = quadKey.Length;

            // create binary quadkey
            string binaryQuadKey = this.CreateBinaryQuadKey(quadKey);

            // Break binary Quadtree in binary tileX and tileY
            string binaryTileY;
            string binaryTileX;
            this.GetBinaryTileCoOrdinates(binaryQuadKey, out binaryTileX, out binaryTileY);

            // Convert binary tileX and tileY to decimal tileX and tileY
            int decimalTileX = this.ConvertBinaryTileToDecimal(binaryTileX, zoomLevel);
            int decimalTileY = this.ConvertBinaryTileToDecimal(binaryTileY, zoomLevel);

            // Determine pixel positions of top right and bottom left.
            this.pixelXMin = decimalTileX * 256;
            double pixelXMax = ((decimalTileX + 1) * 256) - 1;
            this.pixelYMin = decimalTileY * 256;
            double pixelYMax = ((decimalTileY + 1) * 256) - 1;

            double pixelOffset = 0;

            if (type == LightMapType.AirStation || type == LightMapType.WaterStation)
            {
                // calculate the offset size for whether the circles overflow into another tile
                if (zoomLevel < 6)
                {
                    // if the zoom level is between 0 and 5, it is a single pixel, so no offset needed
                    pixelOffset = 0;
                }
                else if (zoomLevel < 10)
                {
                    // between 5 and 9 it is the zoom level size, as we are interested in the radius, we halve this
                    pixelOffset = zoomLevel / 2;
                }
                else
                {
                    pixelOffset = 5;
                }
            }
            else
            {
                // calculate the offset size for whether the circles overflow into another tile
                if (zoomLevel < 6)
                {
                    // if the zoom level is between 0 and 7, it is a single pixel, so no offset needed
                    pixelOffset = 0;
                }
                else
                {
                    // otherwise it is a pixel width of 7
                    pixelOffset = 4;
                }
            }

            // Calculate upper left and lower right long / lat for the tile
            this.minLongitude = this.ConvertPixelPositionToLong(this.pixelXMin - pixelOffset, zoomLevel);
            this.maxLongitude = this.ConvertPixelPositionToLong(pixelXMax + pixelOffset, zoomLevel);
            this.maxLatitude = this.ConvertPixelPositionToLat(this.pixelYMin - pixelOffset, zoomLevel);
            this.minLatitude = this.ConvertPixelPositionToLat(pixelYMax + pixelOffset, zoomLevel);

            Stream generatedImage = null;

            // Get the stations for the given tile.
            if (type == LightMapType.AirStation || type == LightMapType.WaterStation)
            {
                List<StationMinimal> stations = null;

                try
                {
                    stations = this.GetStationsInTile(this.minLongitude, this.maxLongitude, this.minLatitude, this.maxLatitude, type);
                }
                catch (EntityException ex)
                {
                    Trace.TraceError("Exception when generating station LightMap: " + ex.Message + " " + ex.InnerException);
                    throw new EntityException("exception with getting tiles from DB", ex);
                }

                if (stations != null && stations.Count > 0)
                {
                    generatedImage = this.GenerateTileImage(stations, quadKey, isOverlap);
                    this.SaveTileToBlob(quadKey, type, generatedImage);
                }
            }
            else
            {
                List<UserRating> stations = null;

                try
                {
                    stations = this.GetUserStationsInTile(this.minLongitude, this.maxLongitude, this.minLatitude, this.maxLatitude, type);
                }
                catch (EntityException ex)
                {
                    Trace.TraceError("Exception when generating feedback LightMap: " + ex.Message + " " + ex.InnerException);
                    throw new EntityException("exception with getting tiles from DB", ex);
                }

                if (stations != null && stations.Count > 0)
                {
                    generatedImage = this.GenerateTileImage(stations, quadKey, isOverlap);
                    this.SaveTileToBlob(quadKey, type, generatedImage);
                }
            }

            return generatedImage;
        }

        /// <summary>
        /// Serializes a given object into a message for transfer on the queue.
        /// </summary>
        /// <param name="originalMessage">Original class to be transported.</param>
        /// <returns>Message to add to queue.</returns>
        private static CloudQueueMessage SerializeMessage(object originalMessage)
        {
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xmlSerializer = new XmlSerializer(originalMessage.GetType());
            xmlSerializer.Serialize(memoryStream, originalMessage);

            return new CloudQueueMessage(memoryStream.ToArray());
        }

        /// <summary>
        /// Gets the average rating for past year.
        /// </summary>
        /// <param name="userRatings">The user ratings.</param>
        /// <returns>list of average ratings for specific locations</returns>
        private static Collection<LocationUserRating> GetAverageRatingForPastYear(List<UserRating> userRatings)
        {
            List<UserRating> ratingsTemp = (from r in userRatings
                                           where r.RatingTime > DateTime.UtcNow.AddYears(-1)
                                           select r).ToList();

            DateTime endingDate = DateTime.Now;
            DateTime startingDate = endingDate.AddYears(-1);

            TimeSpan timeBoundary = endingDate.Subtract(startingDate);

            Collection<LocationUserRating> averageLocationUserRatings = new Collection<LocationUserRating>();

            // go through each user rating
            while (ratingsTemp.Count > 0)
            {
                UserRating currentUserRating = ratingsTemp[0];

                var ratingsAtLocation = (from r in ratingsTemp
                                         where r.Longitude == currentUserRating.Longitude &&
                                               r.Latitude == currentUserRating.Latitude
                                         orderby r.RatingTime descending
                                         select r).ToList();

                if (ratingsAtLocation.Count == 1)
                {
                    averageLocationUserRatings.Add(new LocationUserRating()
                    {
                        Latitude = (float)currentUserRating.Latitude,
                        Longitude = (float)currentUserRating.Longitude,
                        Count = 1,
                        Rating = ratingsAtLocation[0].Rating
                    });

                    ratingsTemp.Remove(ratingsAtLocation[0]);
                    continue;
                }

                endingDate = ratingsAtLocation[0].RatingTime;
                timeBoundary = endingDate.Subtract(startingDate);

                double ratingAccumulator = 0;
                double ratingDivisor = 0;

                foreach (UserRating userRatingLocation in ratingsAtLocation)
                {
                    // work out the amount valid by the distance and time
                    TimeSpan timeDistance = endingDate.Subtract(userRatingLocation.RatingTime);

                    // calculate the linear dropoff for the times
                    double timeValid = 1 - (((double)timeDistance.Ticks) / ((double)timeBoundary.Ticks));
                    ratingAccumulator += userRatingLocation.Rating * timeValid;
                    ratingDivisor += timeValid;

                    ratingsTemp.Remove(userRatingLocation);
                }

                int currentRating = 0;

                if (ratingDivisor != 0)
                {
                    currentRating = (int)Math.Round(ratingAccumulator / ratingDivisor, 0);
                }

                averageLocationUserRatings.Add(new LocationUserRating()
                {
                    Latitude = (float)currentUserRating.Latitude,
                    Longitude = (float)currentUserRating.Longitude,
                    Count = ratingsAtLocation.Count(),
                    Rating = currentRating
                });
            }

            return averageLocationUserRatings;
        }

        /// <summary>
        /// Checks the left bound.
        /// </summary>
        /// <param name="pixelx">The pixel value.</param>
        /// <param name="length">The length.</param>
        /// <returns>Boolean value to say if that side needs regeneration.</returns>
        private static bool CheckLeft(int pixelx, int length)
        {
            return pixelx >= 0 && pixelx < (length / 2);
        }

        /// <summary>
        /// Checks the top bound.
        /// </summary>
        /// <param name="pixely">The pixel value.</param>
        /// <param name="length">The length.</param>
        /// <returns>Boolean value to say if that side needs regeneration.</returns>
        private static bool CheckBottom(int pixely, int length)
        {
            return pixely <= 256 && pixely > 256 - (length / 2);
        }

        /// <summary>
        /// Checks the bottom bound.
        /// </summary>
        /// <param name="pixely">The pixel value.</param>
        /// <param name="length">The length.</param>
        /// <returns>Boolean value to say if that side needs regeneration.</returns>
        private static bool CheckTop(int pixely, int length)
        {
            return pixely >= 0 && pixely < (length / 2);
        }

        /// <summary>
        /// Checks the right bound.
        /// </summary>
        /// <param name="pixelx">The pixel value.</param>
        /// <param name="length">The length.</param>
        /// <returns>Boolean value to say if that side needs regeneration.</returns>
        private static bool CheckRight(int pixelx, int length)
        {
            return pixelx <= 256 && pixelx > 256 - (length / 2);
        }

        /// <summary>
        /// Converts pixel XY coordinates into tile XY coordinates of the tile containing
        /// the specified pixel.
        /// </summary>
        /// <param name="pixelX">Pixel X coordinate.</param>
        /// <param name="pixelY">Pixel Y coordinate.</param>
        /// <param name="tileX">Output parameter receiving the tile X coordinate.</param>
        /// <param name="tileY">Output parameter receiving the tile Y coordinate.</param>
        private void PixelXYToTileXY(int pixelX, int pixelY, out int tileX, out int tileY)
        {
            tileX = pixelX / 256;
            tileY = pixelY / 256;
        }

        /// <summary>
        /// Converts tile XY coordinates into a QuadKey at a specified level of detail.
        /// </summary>
        /// <param name="tileX">Tile X coordinate.</param>
        /// <param name="tileY">Tile Y coordinate.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail).</param>
        /// <returns>A string containing the QuadKey.</returns>
        private string TileXYToQuadKey(int tileX, int tileY, int levelOfDetail)
        {
            StringBuilder quadKey = new StringBuilder();
            for (int i = levelOfDetail; i > 0; i--)
            {
                char digit = '0';
                int mask = 1 << (i - 1);
                if ((tileX & mask) != 0)
                {
                    digit++;
                }

                if ((tileY & mask) != 0)
                {
                    digit++;
                    digit++;
                }

                quadKey.Append(digit);
            }

            return quadKey.ToString();
        }

        /// <summary>
        /// Lats the long to pixel XY.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="levelOfDetail">The level of detail.</param>
        /// <param name="offsetX">The offset X.</param>
        /// <param name="offsetY">The offset Y.</param>
        /// <param name="pixelX">The pixel X.</param>
        /// <param name="pixelY">The pixel Y.</param>
        private void LatLongToPixelXY(double latitude, double longitude, int levelOfDetail, double offsetX, double offsetY, out int pixelX, out int pixelY)
        {
            latitude = this.Clip(latitude, this.minLatitude, this.maxLatitude);
            longitude = this.Clip(longitude, this.minLongitude, this.maxLongitude);

            double x = (longitude + 180) / 360;
            double sinLatitude = Math.Sin(latitude * Math.PI / 180);
            double y = 0.5 - (Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI));

            uint mapSize = this.MapSize(levelOfDetail);

            pixelX = (int)(this.Clip((x * mapSize) + 0.5, 0, mapSize - 1) - offsetX);
            pixelY = (int)(this.Clip((y * mapSize) + 0.5, 0, mapSize - 1) - offsetY);
        }

        /// <summary>
        /// Determines the map width and height (in pixels) at a specified level
        /// of detail.
        /// </summary>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail).</param>
        /// <returns>The map width and height in pixels.</returns>
        private uint MapSize(int levelOfDetail)
        {
            return (uint)256 << levelOfDetail;
        }

        /// <summary>
        /// Gets the stations in tile.
        /// </summary>
        /// <param name="longMin">The long min.</param>
        /// <param name="longMax">The long max.</param>
        /// <param name="latMin">The lat min.</param>
        /// <param name="latMax">The lat max.</param>
        /// <param name="type">The type of the layer.</param>
        /// <returns>A List of locations</returns>
        private List<StationMinimal> GetStationsInTile(double longMin, double longMax, double latMin, double latMax, LightMapType type)
        {
            airLocations = (from stations in ServiceProvider.StationService.GetStationsOfGivenType("Air")
                            select stations).ToList();

            waterLocations = (from stations in ServiceProvider.StationService.GetStationsOfGivenType("Water")
                              select stations).ToList();

            waterStationImage = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.StationLightMapIconWater.png"));
            airStationImage = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.StationLightMapIconAir.png"));

            List<StationMinimal> locations = new List<StationMinimal>();

            switch (type)
            {
                case LightMapType.AirStation:
                    locations = (from stations in airLocations
                                 where stations.Latitude >= (decimal)latMin
                                            && stations.Latitude <= (decimal)latMax
                                            && stations.Longitude <= (decimal)longMax
                                            && stations.Longitude >= (decimal)longMin
                                 select stations).ToList();
                    break;

                case LightMapType.WaterStation:
                    locations = (from stations in waterLocations
                                 where stations.Latitude >= (decimal)latMin
                                            && stations.Latitude <= (decimal)latMax
                                            && stations.Longitude <= (decimal)longMax
                                            && stations.Longitude >= (decimal)longMin
                                 select stations).ToList();
                    break;
            }

            return locations;
        }

        /// <summary>
        /// Gets the stations in tile.
        /// </summary>
        /// <param name="longMin">The long min.</param>
        /// <param name="longMax">The long max.</param>
        /// <param name="latMin">The lat min.</param>
        /// <param name="latMax">The lat max.</param>
        /// <param name="mapType">Type of the map.</param>
        /// <returns>Stations in the given tile.</returns>
        private List<UserRating> GetUserStationsInTile(double longMin, double longMax, double latMin, double latMax, LightMapType mapType)
        {
            string target = string.Empty;
            if (mapType == LightMapType.UserFeedbackAir)
            {
                target = "Air";
            }
            else if (mapType == LightMapType.UserFeedbackWater)
            {
                target = "Water";
            }

            List<UserRating> userRatings = new List<UserRating>();
            if (!string.IsNullOrEmpty(target))
            {
                userRatings = (from userRating in ServiceProvider.RatingService.GetUserRatingsForBoundary(latMax, longMax, latMin, longMin)
                               where userRating.RatingTarget == target
                               select userRating).ToList();
            }

            return userRatings;
        }

        /// <summary>
        /// Creates the binary quad key.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <returns>Binary version of the inputted quad key.</returns>
        private string CreateBinaryQuadKey(string quadKey)
        {
            // Convert Quadtree in binary Quadtree
            string binaryQuadKey = string.Empty;
            string tempString = string.Empty;

            for (int i = 0; i <= quadKey.Length - 1; i++)
            {
                tempString = quadKey.Substring(quadKey.Length - i - 1, 1);
                switch (tempString)
                {
                    case "3":
                        binaryQuadKey = "11" + binaryQuadKey;
                        break;
                    case "2":
                        binaryQuadKey = "10" + binaryQuadKey;
                        break;
                    case "1":
                        binaryQuadKey = "01" + binaryQuadKey;
                        break;
                    case "0":
                        binaryQuadKey = "00" + binaryQuadKey;
                        break;
                }
            }

            return binaryQuadKey;
        }

        /// <summary>
        /// Gets the binary tile co ordinates.
        /// </summary>
        /// <param name="binaryQuadKey">The binary quad key.</param>
        /// <param name="binaryTileX">The binary tile X.</param>
        /// <param name="binaryTileY">The binary tile Y.</param>
        private void GetBinaryTileCoOrdinates(string binaryQuadKey, out string binaryTileX, out string binaryTileY)
        {
            string tempString = string.Empty;
            binaryTileX = string.Empty;
            binaryTileY = string.Empty;

            for (int i = 0; i <= binaryQuadKey.Length - 1; i++)
            {
                tempString = binaryQuadKey.Substring(binaryQuadKey.Length - i - 1, 1);
                if (i % 2 == 0)
                {
                    binaryTileX = tempString + binaryTileX;
                }
                else
                {
                    binaryTileY = tempString + binaryTileY;
                }
            }
        }

        /// <summary>
        /// Converts the binary tile co-ordinate to decimal values.
        /// </summary>
        /// <param name="binaryTile">The binary tile co-ordinate.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Decimal value for the tile co-ordinate</returns>
        private int ConvertBinaryTileToDecimal(string binaryTile, int zoomLevel)
        {
            int decimalTileValue = 0;
            string tempString = string.Empty;

            for (int i = 0; i <= zoomLevel - 1; i++)
            {
                tempString = binaryTile.Substring(binaryTile.Length - i - 1, 1);
                decimalTileValue = (int)(decimalTileValue + (int.Parse(tempString, System.Globalization.CultureInfo.InvariantCulture) * Math.Pow(2, i)));
            }

            return decimalTileValue;
        }

        /// <summary>
        /// Converts the pixel position to long.
        /// </summary>
        /// <param name="deciamlValue">The deciaml value.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Longitude Value for point.</returns>
        private double ConvertPixelPositionToLong(double deciamlValue, int zoomLevel)
        {
            return ((deciamlValue * 360) / (256 * Math.Pow(2, zoomLevel))) - 180;
        }

        /// <summary>
        /// Converts the pixel position to lat.
        /// </summary>
        /// <param name="decimalValue">The decimal value.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <returns>Latitude value for given pixel.</returns>
        private double ConvertPixelPositionToLat(double decimalValue, int zoomLevel)
        {
            return Math.Asin((Math.Exp((0.5 - (decimalValue / (256 * Math.Pow(2, zoomLevel)))) * 4 * Math.PI) - 1) / (Math.Exp((0.5 - (decimalValue / (256 * Math.Pow(2, zoomLevel)))) * 4 * Math.PI) + 1)) * (180 / Math.PI);
        }

        /// <summary>
        /// Generates the tile image.
        /// </summary>
        /// <param name="stations">The stations.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="isOverlap">if set to <c>true</c> [is overlap].</param>
        /// <returns>Stream representing the image.</returns>
        private Stream GenerateTileImage(List<StationMinimal> stations, string quadKey, bool isOverlap)
        {
            Bitmap transparentBitmap = new Bitmap(256, 256);

            Bitmap mybitmap = transparentBitmap;
            Graphics graphics = Graphics.FromImage(mybitmap);

            int pixelx;
            int pixely;
            HashSet<string> unique = new HashSet<string>();

            foreach (StationMinimal loc in stations)
            {
                this.LatLongToPixelXY((double)loc.Latitude, (double)loc.Longitude, quadKey.Length, this.pixelXMin, this.pixelYMin, out pixelx, out pixely);

                Color color;
                Image stationImage = null;
                if (loc.StationPurpose == "Air")
                {
                    stationImage = airStationImage;
                    color = Color.FromArgb(230, 230, 0);
                }
                else
                {
                    stationImage = waterStationImage;
                    color = Color.FromArgb(0, 251, 255);
                }

                int length = quadKey.Length;

                if (length <= 5)
                {
                    if (pixelx > 0 && pixelx < 256 && pixely > 0 && pixely < 256)
                    {
                        mybitmap.SetPixel(pixelx, pixely, color);
                    }
                }
                else
                {
                    if (length > 9)
                    {
                        length = 9;
                    }

                    if (!isOverlap)
                    {
                        foreach (string quad in this.CheckBounds(pixelx, pixely, quadKey, length))
                        {
                            unique.Add(quad);
                        }
                    }

                    float pixelFloatX = (float)(pixelx - (quadKey.Length / 2.0));
                    float pixelFloatY = (float)(pixely - (quadKey.Length / 2.0));
                    graphics.DrawImage(stationImage, pixelFloatX, pixelFloatY, length, length);
                }
            }

            MemoryStream ms = new MemoryStream();
            mybitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

            this.AddAdjacentTilesToQueue(unique);
            ms.Position = 0;
            return ms;
        }

        /// <summary>
        /// Generates the tile image.
        /// </summary>
        /// <param name="stations">The stations.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="isOverlap">if set to <c>true</c> [is overlap].</param>
        /// <returns>Stream representing the image.</returns>
        private Stream GenerateTileImage(List<UserRating> stations, string quadKey, bool isOverlap)
        {
            if (firstUser)
            {
                userWaterRatingImage1 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserLightMapIcon1.png"));
                userWaterRatingImage2 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserLightMapIcon2.png"));
                userWaterRatingImage3 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserLightMapIcon3.png"));
                userWaterRatingImage4 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserLightMapIcon4.png"));
                userWaterRatingImage5 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserLightMapIcon5.png"));

                userAirRatingImage1 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserAirLightMapIcon1.png"));
                userAirRatingImage2 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserAirLightMapIcon2.png"));
                userAirRatingImage3 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserAirLightMapIcon3.png"));
                userAirRatingImage4 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserAirLightMapIcon4.png"));
                userAirRatingImage5 = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("Microsoft.AirWatch.Core.ApplicationServices.Images.UserAirLightMapIcon5.png"));
            }

            string target = string.Empty;
            if (this.lightMapType == LightMapType.UserFeedbackAir)
            {
                target = "Air";
            }
            else if (this.lightMapType == LightMapType.UserFeedbackWater)
            {
                target = "Water";
            }

            if (!string.IsNullOrEmpty(target))
            {
                Bitmap transparentBitmap = new Bitmap(256, 256);
                MemoryStream tms = new MemoryStream();
                transparentBitmap.Save(tms, System.Drawing.Imaging.ImageFormat.Png);

                Bitmap mybitmap = transparentBitmap;
                Graphics graphics = Graphics.FromImage(mybitmap);

                int pixelx;
                int pixely;

                Collection<LocationUserRating> ratings = GetAverageRatingForPastYear(stations);
                
                Image stationImage;
                HashSet<string> unique = new HashSet<string>();
                
                foreach (LocationUserRating rating in ratings)
                {
                    Color color = new Color();

                    switch ((int)Math.Ceiling(rating.Rating))
                    {
                        case 1:
                            color = Color.FromArgb(150, 0, 0);
                            if (target != "Air")
                            {
                                stationImage = userWaterRatingImage5;
                            }
                            else
                            {
                                stationImage = userAirRatingImage5;
                            }

                            break;
                        case 2:
                            color = Color.FromArgb(255, 128, 0);
                            if (target != "Air")
                            {
                                stationImage = userWaterRatingImage4;
                            }
                            else
                            {
                                stationImage = userAirRatingImage4;
                            }

                            break;
                        case 3:
                            color = Color.FromArgb(232, 233, 26);
                            if (target != "Air")
                            {
                                stationImage = userWaterRatingImage3;
                            }
                            else
                            {
                                stationImage = userAirRatingImage3;
                            }

                            break;
                        case 4:
                            color = Color.FromArgb(112, 255, 0);
                            if (target != "Air")
                            {
                                stationImage = userWaterRatingImage2;
                            }
                            else
                            {
                                stationImage = userAirRatingImage2;
                            }

                            break;
                        case 5:
                            color = Color.FromArgb(27, 142, 0);
                            if (target != "Air")
                            {
                                stationImage = userWaterRatingImage1;
                            }
                            else
                            {
                                stationImage = userAirRatingImage1;
                            }

                            break;
                        default:
                            continue;
                    }

                    this.LatLongToPixelXY((double)rating.Latitude, (double)rating.Longitude, quadKey.Length, this.pixelXMin, this.pixelYMin, out pixelx, out pixely);
                    int length = quadKey.Length;

                    if (length <= 5)
                    {
                        if (pixelx > 0 && pixelx < 256 && pixely > 0 && pixely < 256)
                        {
                            mybitmap.SetPixel(pixelx, pixely, color);
                        }
                    }
                    else
                    {
                        if (length > 9)
                        {
                            length = 9;
                        }

                        if (!isOverlap)
                        {
                            foreach (string quad in this.CheckBounds(pixelx, pixely, quadKey, length))
                            {
                                unique.Add(quad);
                            }
                        }

                        float pixelFloatX = (float)(pixelx - (quadKey.Length / 2.0));
                        float pixelFloatY = (float)(pixely - (quadKey.Length / 2.0));

                        graphics.DrawImage(stationImage, pixelFloatX, pixelFloatY, length, length);
                    }
                }

                MemoryStream ms = new MemoryStream();
                mybitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                mybitmap.Dispose();

                this.AddAdjacentTilesToQueue(unique);
                ms.Position = 0;
                return ms;
            }

            return null;
        }

        /// <summary>
        /// Checks the bounds for each edge of the current quad key area.
        /// </summary>
        /// <param name="pixelx">The pixel value x.</param>
        /// <param name="pixely">The pixel value y.</param>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="length">The length.</param>
        /// <returns>Collection of quad keys needed to generate.</returns>
        private Collection<string> CheckBounds(int pixelx, int pixely, string quadKey, int length)
        {
            Collection<string> quadKeys = new Collection<string>();

            if (CheckLeft(pixelx, length) && CheckTop(pixely, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 0));
            }

            if (CheckLeft(pixelx, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 1));
            }

            if (CheckLeft(pixelx, length) && CheckBottom(pixely, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 2));
            }

            if (CheckTop(pixely, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 3));
            }

            if (CheckBottom(pixely, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 5));
            }

            if (CheckRight(pixelx, length) && CheckTop(pixely, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 6));
            }

            if (CheckRight(pixelx, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 7));
            }

            if (CheckRight(pixelx, length) && CheckBottom(pixely, length))
            {
                quadKeys.Add(this.GetAdjacentTile(quadKey, 8));
            }

            return quadKeys;
        }

        /// <summary>
        /// Gets the adjacent tile.
        /// </summary>
        /// <param name="currentQuadTile">The current quad tile.</param>
        /// <param name="relativePos">The relative pos.</param>
        /// <returns>String Value of the Tile Needed.</returns>
        private string GetAdjacentTile(string currentQuadTile, int relativePos)
        {
            var levelOfDetail = currentQuadTile.Length;

            // create binary quadkey
            string binaryQuadKey = this.CreateBinaryQuadKey(currentQuadTile);

            // Break binary Quadtree in binary tileX and tileY
            string binaryTileY;
            string binaryTileX;
            this.GetBinaryTileCoOrdinates(binaryQuadKey, out binaryTileX, out binaryTileY);

            // Convert binary tileX and tileY to decimal tileX and tileY
            int decimalTileX = this.ConvertBinaryTileToDecimal(binaryTileX, levelOfDetail);
            int decimalTileY = this.ConvertBinaryTileToDecimal(binaryTileY, levelOfDetail);
            
            var maxValue = System.Convert.ToInt32(Math.Pow(2, levelOfDetail)) - 1;
            var currentPosition = 0;
            string[] generatedQuads = new string[9];

            int tempx;
            int tempy;

            for (int i = -1; i < 2; i++)
            {
                tempx = decimalTileX + i;

                for (int j = -1; j < 2; j++)
                {
                    tempy = decimalTileY + j;

                    if (tempx == -1)
                    {
                        tempx = maxValue;
                    }

                    if (tempy == -1)
                    {
                        tempy = maxValue;
                    }

                    generatedQuads[currentPosition] = this.TileXYToQuadKey(tempx, tempy, levelOfDetail);
                    currentPosition++;
                }
            }

            //// layout positions of relative quads.
            //// 0 | 3 | 6
            //// 1 | x | 7
            //// 2 | 5 | 8

            // add to the queue.
            return generatedQuads[relativePos];
        }

        /// <summary>
        /// Adds the adjacent tiles to queue.
        /// </summary>
        /// <param name="uniqueQuads">The unique quads.</param>
        private void AddAdjacentTilesToQueue(HashSet<string> uniqueQuads)
        {
            foreach (string s in uniqueQuads)
            {
                // add to the queue.
                LightMapMessage message = new LightMapMessage();
                message.QuadKey = s;
                message.LightMapType = this.lightMapType;
                message.IsOverlap = true;

                this.lightMapQueue.AddMessage(SerializeMessage(message));
            }
        }

        /// <summary>
        /// Clips a number to the specified minimum and maximum values.
        /// </summary>
        /// <param name="n">The number to clip.</param>
        /// <param name="minValue">Minimum allowable value.</param>
        /// <param name="maxValue">Maximum allowable value.</param>
        /// <returns>The clipped value.</returns>
        private double Clip(double n, double minValue, double maxValue)
        {
            return Math.Min(Math.Max(n, minValue), maxValue);
        }

        /// <summary>
        /// Saves the tile to BLOB.
        /// </summary>
        /// <param name="quadKey">The quad key.</param>
        /// <param name="mapType">Type of the map.</param>
        /// <param name="generatedTileImage">The generated tile image.</param>
        private void SaveTileToBlob(string quadKey, LightMapType mapType, Stream generatedTileImage)
        {
            int attempt = 0;
            bool saved = false;
            
            CloudBlob blob = null;

            do
            {
                switch (mapType)
                {
                    case LightMapType.AirStation:
                        blob = this.airStationBlobContainer.GetBlobReference(quadKey);
                        break;

                    case LightMapType.WaterStation:
                        blob = this.waterStationBlobContainer.GetBlobReference(quadKey);
                        break;

                    case LightMapType.UserFeedbackWater:
                        blob = this.userFeedbackWaterBlobContainer.GetBlobReference(quadKey);
                        break;

                    default:
                    case LightMapType.UserFeedbackAir:
                        blob = this.userFeedbackAirBlobContainer.GetBlobReference(quadKey);
                        break;

                }

                try
                {
                    attempt++;
             
                    blob.Properties.ContentType = "image/png";
                    blob.UploadFromStream(generatedTileImage);

                    saved = true;
                }
                catch (Exception ex)
                {
                    // error uploading
                    Trace.TraceError("Error Uploading tile image: " + ex.Message.ToString());
                }
            }
            while (!saved && attempt < this.retry);
        }
    }
}
