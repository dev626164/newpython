﻿//-----------------------------------------------------------------------
// <copyright file="LanguageService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Service for language related transactions</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    #region Using
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AirWatch.AzureTables;
    using Microsoft.WindowsAzure;

    #endregion

    /// <summary>
    /// Service for language related transactions
    /// </summary>
    public class LanguageService
    {
        /// <summary>
        /// language Entity Table.
        /// </summary>
        private LanguageEntityTable languageEntityTable;

        /// <summary>
        /// Initializes a new instance of the LanguageService class.
        /// </summary>
        internal LanguageService()
        {
            var storageAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            this.languageEntityTable = new LanguageEntityTable(storageAccount.TableEndpoint.ToString(), storageAccount.Credentials);
        }

        #region Loading

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>The last updated time stamp for the current culture from Azure Storage.</returns>
        public DateTime GetTimestamp(string culture)
        {
            var time = from l in this.languageEntityTable.Languages
                       where l.PartitionKey == culture && l.RowKey == "CULTURE_ID"
                       select l;

            LanguageEntity[] languageEntityArray = time.ToArray();

            if (languageEntityArray.Length > 0)
            {
                return languageEntityArray[0].Timestamp;
            }
            else
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Get all languages from the database
        /// </summary>
        /// <returns>Array of Languages</returns>
        public LanguageDescription[] GetLanguages()
        {
            int i = 0;

            var languages = this.languageEntityTable.Languages.Where(predicate => predicate.RowKey.Equals("CULTURE_DISPLAYNAME"));
            List<LanguageEntity> languageList = languages.ToList();
            LanguageDescription[] languageArray = new LanguageDescription[languageList.Count];                        

            while (i < languageList.Count)
            {
                try
                {
                    languageArray[i] = new LanguageDescription
                        {
                            LanguageCode = languageList.ElementAt(i).PartitionKey,
                            LanguageName = languageList.ElementAt(i).Value
                        };
                }
                catch (ArgumentException)
                {
                    languageArray[i] = new LanguageDescription
                    {
                        LanguageCode = languageList.ElementAt(i).PartitionKey,
                        LanguageName = languageList.ElementAt(i).Value
                    };
                }
                finally
                {
                    i++;
                }
            }

            return languageArray;
        }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>Dictionary of string values for the user interface</returns>
        public Dictionary<string, string> GetLanguage(string culture)
        {
            string cultureCode = culture.Substring(0, 3) + culture.Substring(3, 2).ToUpperInvariant();
                        
            var languages = this.languageEntityTable.Languages;

            var selectedCulture = from l in languages
                                  where l.PartitionKey.Equals(cultureCode, StringComparison.OrdinalIgnoreCase)
                                  select l;

            Dictionary<string, string> languageDictionary = new Dictionary<string, string>();

            foreach (LanguageEntity languageEntity in selectedCulture)
            {
                languageDictionary.Add(languageEntity.RowKey, languageEntity.Value);
            }            

            return languageDictionary;
        }

        #endregion
    }
}
