﻿//-----------------------------------------------------------------------
// <copyright file="RatingService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-fikarn@microsoft.com</email>
// <date>13/08/09</date>
// <summary>Rating Service</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.AirWatch.Core.Data;

    /// <summary>
    /// Location rating service
    /// </summary>
    public class RatingService
    {
        /// <summary>
        /// Internal representation of Entity Framework data context.
        /// </summary>
        private DataContext dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="RatingService"/> class.
        /// </summary>
        internal RatingService()
        {
            this.dataContext = DataContext.Create();
        }

        /// <summary>
        /// Rates the location.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Success bool</returns>
        public bool RateLocation(Microsoft.AirWatch.Core.Data.UserRating rating)
        {
            try
            {
                if (rating != null)
                {
                    this.dataContext.AirWatchEntities.AddToUserRating(rating);
                    this.dataContext.AirWatchEntities.SaveChanges();
                }
            }
            catch (System.Data.SqlClient.SqlException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets all ratings.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="radius">The radius.</param>
        /// <param name="startingDate">The starting date.</param>
        /// <param name="endingDate">The ending date.</param>
        /// <returns>List of ratings within the parameters specified</returns>
        public Collection<UserRating> GetAllRatings(double latitude, double longitude, string targetType, double radius, DateTime startingDate, DateTime endingDate)
        {
            return new Collection<UserRating>(this.dataContext.AirWatchEntities.GetUserRatingsInRadiusForTime((decimal)longitude, (decimal)latitude, targetType, (decimal)radius, startingDate, endingDate).ToList());
        }

        /// <summary>
        /// Gets the rating.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="targetType">Type of the target eg "air" or "water".</param>
        /// <param name="radius">The radius from the long/lat for it to be valid must be > 0.</param>
        /// <returns>User rating for that location, null if radius is less than or equal to 0</returns>
        public AverageRating GetRating(double latitude, double longitude, string targetType, double radius)
        {
            DateTime oneYearAgo = DateTime.Now.AddYears(-1);

            return this.GetRating(latitude, longitude, targetType, radius, oneYearAgo, DateTime.Now);
        }

        /// <summary>
        /// Gets the rating.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="targetType">Type of the target eg "air" or "water".</param>
        /// <param name="radius">The radius from the long/lat for it to be valid must be > 0.</param>
        /// <param name="startingDate">The starting date.</param>
        /// <param name="endingDate">The ending date.</param>
        /// <returns>User rating for that location, null if radius is less than or equal to 0</returns>
        public AverageRating GetRating(double latitude, double longitude, string targetType, double radius, DateTime startingDate, DateTime endingDate)
        {
            TimeSpan timeBoundary = endingDate.Subtract(startingDate);

            if (radius <= 0)
            {
                return null;
            }

            var userRatings = this.dataContext.AirWatchEntities.GetUserRatingsInRadiusForTime((decimal)longitude, (decimal)latitude, targetType, (decimal)radius, startingDate, endingDate).ToList();
            Collection<RatingQualifierIdentifiers> ratingQualifierIdentifiers = new Collection<RatingQualifierIdentifiers>();

            // if there are no ratings return null
            if (userRatings.Count == 0)
            {
                return null;
            }

            double ratingAccumulator = 0;
            double ratingDivisor = 0;

            int qualifyingWordsCount = 0;

            AverageRating averageRating = new AverageRating();
            averageRating.Count = userRatings.Count;

            // go through each user rating
            foreach (UserRating userRating in userRatings)
            {
                // work out the amount valid by the distance and time
                var distance = Math.Sqrt(Math.Pow((double)userRating.Longitude - longitude, 2) + Math.Pow((double)userRating.Latitude - latitude, 2));
                TimeSpan timeDistance = endingDate.Subtract(userRating.RatingTime);

                double distanceValid = GetDistanceAmountValid(distance, radius);
                double timeValid = GetTimeAmountValid(timeDistance, timeBoundary);

                ratingAccumulator += userRating.Rating * (distanceValid * timeValid);
                ratingDivisor += distanceValid * timeValid;

                // load the qualifying words
                userRating.WordQualifier.Load();
                qualifyingWordsCount += userRating.WordQualifier.Count;
            }

            if (ratingDivisor != 0)
            {
                averageRating.Rating = (int)Math.Round(ratingAccumulator / ratingDivisor, 0);
            }

            var qualifierIdentifiersList =
              (from wq in userRatings.SelectMany<UserRating, WordQualifier>(selector => selector.WordQualifier)
               group wq by wq.WordId into g
               select new
               {
                   WordIdentifier = g.Key,
                   Percentage = (double)g.Count() / qualifyingWordsCount,
               }).ToList();

            foreach (var ident in qualifierIdentifiersList)
            {
                ratingQualifierIdentifiers.Add(new RatingQualifierIdentifiers() { Percentage = ident.Percentage, WordIdentifier = ident.WordIdentifier.ToString(System.Globalization.CultureInfo.InvariantCulture) });
            }

            averageRating.RatingQualifierIdentifiers = ratingQualifierIdentifiers;

            return averageRating;
        }

        /// <summary>
        /// Gets the user ratings for boundary.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <returns>UserRatings in the given map boundary</returns>
        public UserRating[] GetUserRatingsForBoundary(double north, double east, double south, double west)
        {
            var query = from UserRating userRating in this.dataContext.AirWatchEntities.GetUserRatingsForMapBoundary((decimal)north, (decimal)east, (decimal)south, (decimal)west)
                        select userRating;

            return query.ToArray();
        }

        /// <summary>
        /// Gets the user rating with in given time.
        /// </summary>
        /// <param name="time">The time period in which we need images.</param>
        /// <param name="type">The type of stations to return.</param>
        /// <returns>
        /// Collection of user ratings in given time period.
        /// </returns>
        public UserRating[] GetUserRatingWithinGivenTime(DateTime time, LightMapType type)
        {
            string target = string.Empty;
            if (type == LightMapType.UserFeedbackAir)
            {
                target = "Air";
            }
            else if (type == LightMapType.UserFeedbackWater)
            {
                target = "Water";
            }
            
            if (!string.IsNullOrEmpty(target))
            {
                DateTime tempTime = time.AddDays(-1);
                var query = (from UserRating userRating in this.dataContext.AirWatchEntities.UserRating
                             where (userRating.RatingTime > tempTime) && (userRating.RatingTarget == target)
                                select userRating).ToArray();
                return query;
            }

            return null;
        }

        /// <summary>
        /// Gets the user rating outside of given time.
        /// </summary>
        /// <param name="time">The time period in which we need images.</param>
        /// <param name="type">The type of stations to return.</param>
        /// <returns>
        /// Collection of user ratings in given time period.
        /// </returns>
        public UserRating[] GetUserRatingOutsideGivenTime(DateTime time, LightMapType type)
        {
            string target = string.Empty;
            if (type == LightMapType.UserFeedbackAir)
            {
                target = "Air";
            }
            else if (type == LightMapType.UserFeedbackWater)
            {
                target = "Water";
            }

            UserRating[] query = null;
            if (!string.IsNullOrEmpty(target))
            {
                DateTime tempTime = time.AddYears(-1);
                query = (from UserRating userRating in this.dataContext.AirWatchEntities.UserRating
                         where userRating.RatingTime < tempTime && userRating.RatingTarget == target
                         select userRating).ToArray();
            }

            return query;
        }

        /// <summary>
        /// Gets a linear drop off between a distance and a max distance
        /// </summary>
        /// <param name="distance">The distance.</param>
        /// <param name="radius">The radius.</param>
        /// <returns>A value between 0 and 1 representing the amount valid of the distance assuming distance less than or equal to radius</returns>
        private static double GetDistanceAmountValid(double distance, double radius)
        {
            return 1 - (distance / radius);
        }

        /// <summary>
        /// Gets a linear drop off between a time and a max time
        /// </summary>
        /// <param name="time">The now minus the time of the rating.</param>
        /// <param name="timeBoundary">The time boundary (now minus one year ago).</param>
        /// <returns>A value between 0 and 1 representing the amount valid of the time assuming time less than or equal to timeBoundary</returns>
        private static double GetTimeAmountValid(TimeSpan time, TimeSpan timeBoundary)
        {
            return 1 - (((double)time.Ticks) / ((double)timeBoundary.Ticks));
        }
    }
}
