﻿//-----------------------------------------------------------------------
// <copyright file="StationService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Service for station related transactions</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Core.ApplicationServices
{
    #region Using

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Xml.Linq;
    using Microsoft.AirWatch.Core.Data;
    using Map = dev.virtualearth.net.webservices.v1.common;

    #endregion

    /// <summary>
    /// Service for station related transactions
    /// </summary>
    public class StationService
    {
        /// <summary>
        /// The Minimum air quality index 
        /// </summary>
        private const int MINWATERINDEX = 1;

        /// <summary>
        /// The Maximum water quality index
        /// </summary>
        private const int MAXWATERINDEX = 3;

        /// <summary>
        /// The Minimum air quality index 
        /// </summary>
        private const int MINAIRINDEX = 1;

        /// <summary>
        /// The Maximum air quality index
        /// </summary>
        private const int MAXAIRINDEX = 5;

        /// <summary>
        /// The radius in degrees for the closest station radius
        /// </summary>
        private const double CLOSESTSTATIONRADIUS = 0.167;

        /// <summary>
        /// Temporary constant for offset when getting ratings
        /// </summary>
        private const double OFFSET = 0.00001;

        /// <summary>
        /// Internal representation of Entity Framework data context.
        /// </summary>
        private DataContext dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="StationService"/> class.
        /// </summary>
        internal StationService()
        {
            this.dataContext = DataContext.Create();
        }

        /// <summary>
        /// Gets the offset.
        /// </summary>
        /// <value>The offset.</value>
        public static double Offset
        {
            get
            {
                return OFFSET;
            }
        }

        #region Data Input (WorkerRoleOnly)

        /// <summary>
        /// Inputs the air station data from the Xml Root Element parameter.
        /// </summary>
        /// <param name="rootElement">The root element.</param>
        /// <param name="retry">The number of times to retry a save.</param>
        /// <returns>True if data is entered, false if it failed</returns>
        public bool InputAirStationData(XContainer rootElement, int retry)
        {
            IEnumerable<XElement> stations = rootElement.Elements("station");

            bool saved = true;

            // load the components to be used in measurements
            Component ozoneComponent = null, nitrogenDioxideComponent = null, particulateComponent = null;

            var ozoneQuery = from component in this.dataContext.AirWatchEntities.Component
                             where component.Caption == "O3"
                             select component;
            if (ozoneQuery.Count() > 0)
            {
                ozoneComponent = ozoneQuery.First();
            }

            var nitrogenDioxideQuery = from component in this.dataContext.AirWatchEntities.Component
                                       where component.Caption == "NO2"
                                       select component;
            if (nitrogenDioxideQuery.Count() > 0)
            {
                nitrogenDioxideComponent = nitrogenDioxideQuery.First();
            }

            var particulateQuery = from component in this.dataContext.AirWatchEntities.Component
                                   where component.Caption == "PM10"
                                   select component;
            if (particulateQuery.Count() > 0)
            {
                particulateComponent = particulateQuery.First();
            }

            foreach (XElement xmlStation in stations)
            {
                string stationCode = xmlStation.Element("code") != null ? xmlStation.Element("code").Value : null;

                if (stationCode == null)
                {
                    continue;
                }

                // see if the station already exists
                var currentStations = from station in this.dataContext.AirWatchEntities.Station.Include("StationMeasurement")
                                      where station.EuropeanCode == stationCode
                                      select station;

                // get the timestamp of the station
                DateTime? stationTimestamp = null;

                if (xmlStation.Element("timestamp") != null)
                {
                    DateTime stationTimestampOut;

                    if (DateTime.TryParse(xmlStation.Element("timestamp").Value, out stationTimestampOut))
                    {
                        stationTimestamp = stationTimestampOut;
                    }
                }

                if (currentStations.Count() > 0)
                {
                    // Pre Existing Station - if the station exists, then delete all previous measurements and then add the new ones
                    Station databaseStation = currentStations.First();

                    // check to see if the timestamp is later than the timestamp on the station
                    if (databaseStation.Timestamp == null || (stationTimestamp != null && stationTimestamp.Value > databaseStation.Timestamp))
                    {
                        // check to see if the new CAQI value is valid
                        int? qualityIndex = GetQualityMeasurement(xmlStation.Element("measurements"), "average", "CAQI");

                        // if the quality index is null, or within range, then input the new measurement, otherwise do nothing
                        if (qualityIndex == null || (qualityIndex >= MINAIRINDEX && qualityIndex <= MAXAIRINDEX))
                        {
                            // delete previous measurements
                            IEnumerable<StationMeasurement> databaseStationMeasurements = databaseStation.StationMeasurement.AsEnumerable();

                            while (databaseStationMeasurements.Count() > 0)
                            {
                                // delete the station measurement (will delete measurement with trigger)
                                this.dataContext.AirWatchEntities.DeleteObject(databaseStationMeasurements.First());
                            }

                            // try to delete the measures from the db (using retry)
                            bool error = !this.RetrySave(retry);
                            if (error)
                            {
                                return false;
                            }

                            // go through the xml measurements for this station and load values
                            this.CreateAndAttachAirMeasurement(xmlStation.Element("measurements"), databaseStation, stationTimestamp, ozoneComponent, nitrogenDioxideComponent, particulateComponent);

                            // update the timestamp on the station
                            databaseStation.Timestamp = stationTimestamp;
                        }
                    }
                }
                else
                {
                    // New Station - if the station doesn't exist create it
                    Station station = this.CreateAndAttachNewStation(xmlStation, stationCode, stationTimestamp, "Air", "CAQI");

                    if (station != null)
                    {
                        // go through the xml measurements for this station and load values
                        this.CreateAndAttachAirMeasurement(xmlStation.Element("measurements"), station, stationTimestamp, ozoneComponent, nitrogenDioxideComponent, particulateComponent);
                    }
                }

                saved &= this.RetrySave(retry);
            }

            return saved;
        }

        /// <summary>
        /// Inputs the water station data from the Xml Root Element parameter.
        /// </summary>
        /// <param name="rootElement">The root element.</param>
        /// <param name="retry">The number of times to retry a save.</param>
        /// <returns>True if data is entered, false if it failed</returns>
        public bool InputWaterStationData(XContainer rootElement, int retry)
        {
            IEnumerable<XElement> stations = rootElement.Elements("station");

            bool saved = true;

            Collection<string> stationCodes = new Collection<string>();

            foreach (XElement xmlStation in stations)
            {
                string stationCode = xmlStation.Element("code") != null ? xmlStation.Element("code").Value : null;

                if (stationCode == null)
                {
                    continue;
                }

                if (!stationCodes.Contains(stationCode))
                {
                    stationCodes.Add(stationCode);
                }

                // see if the station already exists
                var currentStations = from station in this.dataContext.AirWatchEntities.Station.Include("StationMeasurement")
                                      where station.EuropeanCode == stationCode
                                      select station;

                // get the timestamp of the station
                DateTime? stationTimestamp = null;

                if (xmlStation.Element("timestamp") != null)
                {
                    DateTime stationTimestampOut;

                    if (DateTime.TryParse(xmlStation.Element("timestamp").Value, out stationTimestampOut))
                    {
                        stationTimestamp = stationTimestampOut;
                    }
                }

                if (currentStations.Count() > 0)
                {
                    Station databaseStation = currentStations.First();
                    this.InputPreExistingWaterStation(xmlStation, stationTimestamp, databaseStation, retry);
                }
                else
                {
                    // New Station - if the station doesn't exist create it
                    this.CreateAndAttachNewStation(xmlStation, stationCode, stationTimestamp, "Water", "Water");
                }

                saved &= this.RetrySave(retry);
            }

            // input aggregates for user ratings on missed years
            this.AttachUserRatingsForWaterStationsMissedYears(stationCodes);

            return saved;
        }

        /// <summary>
        /// Attaches the user rating aggregate for all water stations without it in the measurements.
        /// </summary>
        /// <param name="retrySaveTimes">The number of times to try to save.</param>
        /// <returns>whether the save succeded</returns>
        public bool AttachUserRatingAggregateForWaterStations(int retrySaveTimes)
        {
            var waterStationMeasurements = this.dataContext.AirWatchEntities.GetAllStationMeasurementsWithoutUserAggregationOfType("water");

            foreach (var waterStationMeasurement in waterStationMeasurements)
            {
                AverageRating averageRating = this.GetAverageRatingForStationMeasurement(waterStationMeasurement, true);
                waterStationMeasurement.AggregatedUserRating = averageRating.Rating;
                waterStationMeasurement.AggregatedUserRatingCount = averageRating.Count;
            }

            return this.RetrySave(retrySaveTimes);
        }

        #endregion

        #region Loading

        /// <summary>
        /// Gets the station for station code.
        /// </summary>
        /// <param name="stationId">The station id.</param>
        /// <returns>Station entity</returns>
        public Station GetStationForStationCode(int stationId)
        {
            var query = from Station station in this.dataContext.AirWatchEntities.GetStationForStationId(stationId)
                        select station;

            Station stationToReturn = query.FirstOrDefault();

            if (stationToReturn != null)
            {
                if (stationToReturn.LocationReference != null)
                {
                    stationToReturn.LocationReference.Load();
                }

                if (stationToReturn.StationMeasurement != null)
                {
                    stationToReturn.StationMeasurement.Load();

                    foreach (StationMeasurement stationMeasurement in stationToReturn.StationMeasurement)
                    {
                        if (stationMeasurement.MeasurementReference != null)
                        {
                            stationMeasurement.MeasurementReference.Load();

                            if (stationMeasurement.Measurement != null)
                            {
                                if (stationMeasurement.Measurement.ComponentReference != null)
                                {
                                    stationMeasurement.Measurement.ComponentReference.Load();

                                    if (stationMeasurement.Measurement.Component != null)
                                    {
                                        stationMeasurement.Measurement.Component.Threshold.Load();
                                    }
                                }
                            }
                        }
                    }

                    AttachMissingYearsToFeed(stationToReturn);

                    // order the measurements in time order if water stations
                    if (stationToReturn.StationPurpose == "Water")
                    {
                        var orderedStationMeasurements = (from m in stationToReturn.StationMeasurement
                                                          orderby m.Measurement.Timestamp descending
                                                          select m).ToList();

                        stationToReturn.StationMeasurement.Clear();

                        foreach (StationMeasurement m in orderedStationMeasurements)
                        {
                            stationToReturn.StationMeasurement.Add(m);
                        }
                    }
                }
            }

            return stationToReturn;
        }

        /// <summary>
        /// Gets the stations for boundary.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <returns>Stations in the given map boundary</returns>
        public StationMinimal[] GetStationsForBoundary(double north, double east, double south, double west)
        {
            var query = from StationMinimal stationMinInfo in this.dataContext.AirWatchEntities.GetStationsForMapBoundary((decimal)north, (decimal)east, (decimal)south, (decimal)west)
                        select stationMinInfo;

            return query.ToArray();
        }

        /// <summary>
        /// Gets the stations for boundary and purpose.
        /// </summary>
        /// <param name="north">The north boundary.</param>
        /// <param name="east">The east boundary.</param>
        /// <param name="south">The south boundary.</param>
        /// <param name="west">The west boundary.</param>
        /// <param name="stationPurpose">The station purpose.</param>
        /// <returns>Stations in the given map boundary</returns>
        public StationMinimal[] GetStationsForBoundary(double north, double east, double south, double west, string stationPurpose)
        {
            var query = from StationMinimal stationMinInfo in this.dataContext.AirWatchEntities.GetStationsForMapBoundaryAndType((decimal)north, (decimal)east, (decimal)south, (decimal)west, stationPurpose)
                        select stationMinInfo;

            return query.ToArray();
        }

        /// <summary>
        /// Gets all stations.
        /// </summary>
        /// <param name="type">The type of Stations we wish to recive.</param>
        /// <returns>All the stations for a given type.</returns>
        public StationMinimal[] GetStationsOfGivenType(string type)
        {
            var stationOfType = from StationMinimal stations in this.dataContext.AirWatchEntities.StationMinimal
                                where stations.StationPurpose == type
                                select stations;

            return stationOfType.ToArray();
        }

        /// <summary>
        /// Gets the type of the user stations of given.
        /// </summary>
        /// <param name="type">The type of light map to generate.</param>
        /// <returns>Array of user ratings.</returns>
        public UserRating[] GetUserStationsOfGivenType(LightMapType type)
        {
            string target = string.Empty;
            if (type == LightMapType.UserFeedbackAir)
            {
                target = "Air";
            }
            else if (type == LightMapType.UserFeedbackWater)
            {
                target = "Water";
            }

            UserRating[] userStationsOfType = null;
            if (!string.IsNullOrEmpty(target))
            {
                userStationsOfType = (from UserRating stations in this.dataContext.AirWatchEntities.UserRating
                                      where stations.RatingTarget == target
                                      select stations).ToArray();
            }

            return userStationsOfType;
        }

        /// <summary>
        /// Gets the closest station.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="stationType">Type of the station.</param>
        /// <returns>The closest station of the type specified to the long/lat given or null if none found</returns>
        public Station GetClosestStation(double longitude, double latitude, string stationType)
        {
            Station closestStation = this.dataContext.AirWatchEntities.GetClosestStation((decimal)longitude, (decimal)latitude, stationType).FirstOrDefault();

            if (closestStation != null)
            {
                if (closestStation.LocationReference != null)
                {
                    closestStation.LocationReference.Load();
                }

                if (closestStation.Location != null)
                {
                    // check to see if the station is within the radius
                    double closestLat = (double)closestStation.Location.Latitude;
                    double closestLong = (double)closestStation.Location.Longitude;

                    double distance = Math.Sqrt(Math.Pow(longitude - closestLong, 2) + Math.Pow(latitude - closestLat, 2));

                    // return null if it is outside the radius
                    if (distance > CLOSESTSTATIONRADIUS)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

                if (closestStation.StationMeasurement != null)
                {
                    closestStation.StationMeasurement.Load();

                    foreach (StationMeasurement stationMeasurement in closestStation.StationMeasurement)
                    {
                        if (stationMeasurement.MeasurementReference != null)
                        {
                            stationMeasurement.MeasurementReference.Load();

                            if (stationMeasurement.Measurement != null)
                            {
                                if (stationMeasurement.Measurement.ComponentReference != null)
                                {
                                    stationMeasurement.Measurement.ComponentReference.Load();
                                }
                            }
                        }
                    }
                }
            }

            return closestStation;
        }

        #endregion

        /// <summary>
        /// Gets the measurement of the type specified.
        /// </summary>
        /// <param name="measurement">The measurement element.</param>
        /// <param name="type">The type attribute to find in values.</param>
        /// <returns>The value of the given type, null if none exist</returns>
        private static decimal? GetMeasurementOfType(XElement measurement, XName type)
        {
            if (measurement != null)
            {
                foreach (XElement xmlValue in measurement.Elements("value"))
                {
                    // if the type is null, then get the first value, otherwise get the specific type
                    if (xmlValue.Attribute("type") == null || xmlValue.Attribute("type").Value.ToLowerInvariant() == type)
                    {
                        decimal value;
                        if (decimal.TryParse(xmlValue.Value, out value))
                        {
                            return value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the quality measurement.
        /// </summary>
        /// <param name="xmlMeasurements">The XML measurements.</param>
        /// <param name="type">The type to look for in value, null for the first value.</param>
        /// <param name="qualityCaption">The caption of the quality rating (e.g. CAQI for air).</param>
        /// <returns>The value of the quality found</returns>
        private static int? GetQualityMeasurement(XElement xmlMeasurements, XName type, string qualityCaption)
        {
            int? qualityIndex = null;

            if (xmlMeasurements != null)
            {
                foreach (XElement xmlMeasurement in xmlMeasurements.Elements("measurement"))
                {
                    string componentCaption = xmlMeasurement.Attribute("component") != null ? xmlMeasurement.Attribute("component").Value : null;

                    if (componentCaption != null && componentCaption.ToUpperInvariant() == qualityCaption.ToUpperInvariant())
                    {
                        qualityIndex = (int?)GetMeasurementOfType(xmlMeasurement, type);
                    }
                }
            }

            return qualityIndex;
        }

        /// <summary>
        /// Gets the quality measurement.
        /// </summary>
        /// <param name="xmlMeasurements">The XML measurements.</param>
        /// <returns>The value of the quality found</returns>
        private static int? GetWaterQualityMeasurement(XElement xmlMeasurements)
        {
            int? qualityIndex = null;

            if (xmlMeasurements != null)
            {
                foreach (XElement xmlMeasurement in xmlMeasurements.Elements("measurement"))
                {
                    qualityIndex = (int?)GetMeasurementOfType(xmlMeasurement, "eoestatus");
                }
            }

            return qualityIndex;
        }

        /// <summary>
        /// Creates a measurement based on the XElement measure.
        /// </summary>
        /// <param name="xmlMeasure">The measure XElement.</param>
        /// <param name="type">The type to look for in value, null for first value.</param>
        /// <param name="stationTimestamp">The station timestamp to be used if there is no station measurement.</param>
        /// <returns>
        /// The created Entity Framework Measurement, not attached to the DB
        /// </returns>
        private static Measurement CreateMeasurement(XElement xmlMeasure, XName type, DateTime? stationTimestamp)
        {
            DateTime? measurementTimestamp = null;
            if (xmlMeasure.Attribute("stop") != null)
            {
                DateTime measurementTimestampOut;
                if (DateTime.TryParse(xmlMeasure.Attribute("stop").Value, out measurementTimestampOut))
                {
                    measurementTimestamp = measurementTimestampOut;
                }
            }
            else
            {
                // no measurement timestamp, so use the stations timestamp
                measurementTimestamp = stationTimestamp;
            }

            decimal? measurementValue = GetMeasurementOfType(xmlMeasure, type);

            return new Measurement()
            {
                Timestamp = measurementTimestamp,
                Value = measurementValue,
            };
        }

        /// <summary>
        /// Checks to see if the timestamp is in the previous station measurement
        /// </summary>
        /// <param name="value">The timestamp to look in</param>
        /// <param name="station">The station to look at previous timestamps</param>
        /// <returns>true if found, false otherwise</returns>
        private static bool ContainsPreviousTimestamp(DateTime value, Station station)
        {
            station.StationMeasurement.Load();

            foreach (StationMeasurement sm in station.StationMeasurement)
            {
                sm.MeasurementReference.Load();

                if (sm.Measurement.Timestamp.Equals(value))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks the quality and inserts if valid.
        /// </summary>
        /// <param name="qualityComponent">The quality component.</param>
        /// <param name="xmlMeasurements">The XML measurements.</param>
        /// <param name="station">The station.</param>
        private static void CheckQualityAndInsert(string qualityComponent, XElement xmlMeasurements, Station station)
        {
            switch (qualityComponent)
            {
                case "Water":
                    int? waterQualityIndex = GetWaterQualityMeasurement(xmlMeasurements);

                    // water quality must be within range
                    if (waterQualityIndex == null || (waterQualityIndex >= MINWATERINDEX && waterQualityIndex <= MAXWATERINDEX))
                    {
                        station.QualityIndex = waterQualityIndex;
                    }

                    break;

                case "Air":
                    int? airQualityIndex = GetQualityMeasurement(xmlMeasurements, "average", qualityComponent);

                    // air quality must be within range
                    if (airQualityIndex != null || (airQualityIndex >= MINAIRINDEX && airQualityIndex <= MAXAIRINDEX))
                    {
                        station.QualityIndex = airQualityIndex;
                    }

                    break;
            }
        }

        /// <summary>
        /// Attaches the missing years to feed if the current year should be in the historic list
        /// </summary>
        /// <param name="station">The station.</param>
        private static void AttachMissingYearsToFeed(Station station)
        {
            // if it is a water station, and there is a missing historic rating which appears in the current
            // status, add that to the previous measurements
            if (station.StationPurpose == "Water" && station.Timestamp.HasValue
                && station.Timestamp.Value.Year != DateTime.Now.Year
                && station.Timestamp.Value.Month == 12 && station.Timestamp.Value.Day == 31)
            {
                // if there are null measurements (i.e. attached user ratings)
                var attachedMeasurements = (from StationMeasurement sm in station.StationMeasurement
                                            where sm.Measurement.Value == null
                                            select sm.Measurement).ToList();

                if (attachedMeasurements.Count > 0)
                {
                    // if there is an already attached measurement (i.e. there is an aggregated user rating, but no official measurement)
                    foreach (Measurement m in attachedMeasurements)
                    {
                        if (m.Timestamp.HasValue && station.Timestamp.HasValue
                            && m.Timestamp.Value.Day == station.Timestamp.Value.Day
                            && m.Timestamp.Value.Month == station.Timestamp.Value.Month
                            && m.Timestamp.Value.Year == station.Timestamp.Value.Year)
                        {
                            m.Value = station.QualityIndex;
                        }
                    }
                }
                else
                {
                    // there is no aggregate, so create one
                    Measurement m = new Measurement()
                    {
                        Timestamp = station.Timestamp,
                        Value = station.QualityIndex
                    };

                    StationMeasurement sm = new StationMeasurement()
                    {
                        Measurement = m,
                        Station = station,
                        AggregatedUserRating = null,
                        AggregatedUserRatingCount = 0
                    };

                    station.StationMeasurement.Add(sm);
                }
            }
        }

        #region Bing Map Searcher
        /// <summary>
        /// Gets the address from longitude and latitude calling the bing map helper.
        /// </summary>
        /// <param name="longitude">The longitude of the station.</param>
        /// <param name="latitude">The latitude of the station.</param>
        /// <returns>The formatted address back from the reverse geocode lookup</returns>
        private static string GetAddressFromLocation(decimal longitude, decimal latitude)
        {
            string bingToken = null;
            string address = null;

            try
            {
                // TODO: same as client currently
                bingToken = BingMapServicesHelper.GetBingToken("127.0.0.1");
            }
            catch (Exception)
            {
                return null;
            }

            if (bingToken != null)
            {
                try
                {
                    Map.GeocodeResult[] responses = BingMapServicesHelper.CallReverseGeocodeServices(bingToken, "EN-GB", (double)longitude, (double)latitude).Results;

                    if (responses.Length > 0)
                    {
                        address = responses.First().Address.FormattedAddress;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return address;
        }

        #endregion

        /// <summary>
        /// Inputs the pre existing water station.
        /// </summary>
        /// <param name="xmlStation">The XML station.</param>
        /// <param name="stationTimestamp">The station timestamp.</param>
        /// <param name="databaseStation">The database station.</param>
        /// <param name="retry">The retry count.</param>
        private void InputPreExistingWaterStation(XElement xmlStation, DateTime? stationTimestamp, Station databaseStation, int retry)
        {
            // Pre Existing Station - if the station exists, then move the previous Quality Index into measurements and then add the new one
            Component waterQualityComponent = null;

            var waterQualityQuery = from component in this.dataContext.AirWatchEntities.Component
                                    where component.Caption == "CWQI"
                                    select component;
            if (waterQualityQuery.Count() > 0)
            {
                waterQualityComponent = waterQualityQuery.First();
            }

            // if the timestamp is more recent than the one in the database, archive the one in the database and add new value
            if (stationTimestamp != null && (databaseStation.Timestamp == null || stationTimestamp.Value > databaseStation.Timestamp.Value))
            {
                XElement xmlMeasurements = xmlStation.Element("measurements");
                int? waterQualityIndex = GetWaterQualityMeasurement(xmlMeasurements);

                // water quality must be within range
                if (waterQualityIndex == null || (waterQualityIndex >= MINWATERINDEX && waterQualityIndex <= MAXWATERINDEX))
                {
                    Measurement historicMeasure = new Measurement()
                    {
                        Component = waterQualityComponent,
                        Value = databaseStation.QualityIndex,
                        Timestamp = databaseStation.Timestamp,
                    };

                    // add previous user ratings to station measurement
                    AverageRating averageRating = this.GetAverageRatingForStation(databaseStation);

                    StationMeasurement historicStationMeasure = new StationMeasurement()
                    {
                        Measurement = historicMeasure,
                        Station = databaseStation,
                        AggregatedUserRating = averageRating.Rating,
                        AggregatedUserRatingCount = averageRating.Count,
                    };

                    this.dataContext.AirWatchEntities.AddToMeasurement(historicMeasure);
                    this.dataContext.AirWatchEntities.AddToStationMeasurement(historicStationMeasure);

                    // update the station with new data
                    databaseStation.Timestamp = stationTimestamp;

                    databaseStation.QualityIndex = waterQualityIndex;
                }
            }
            else
            {
                // if the timestamp already exists, ignore it
                if (stationTimestamp != null && databaseStation.Timestamp != null &&
                    databaseStation.Timestamp.Value != stationTimestamp.Value &&
                    !ContainsPreviousTimestamp(stationTimestamp.Value, databaseStation))
                {
                    // if the new station has an older time, archive this one instead
                    XElement xmlMeasurements = xmlStation.Element("measurements");
                    int? waterQualityIndex = GetWaterQualityMeasurement(xmlMeasurements);

                    // water quality must be within range
                    if (waterQualityIndex == null || (waterQualityIndex >= MINWATERINDEX && waterQualityIndex <= MAXWATERINDEX))
                    {
                        Measurement historicMeasure = new Measurement()
                        {
                            Component = waterQualityComponent,
                            Value = waterQualityIndex,
                            Timestamp = stationTimestamp,
                        };

                        StationMeasurement historicStationMeasure = new StationMeasurement()
                        {
                            Measurement = historicMeasure,
                            Station = databaseStation,
                        };

                        // call it with false attachment, as it has not yet been saved to the database
                        AverageRating averageRating = this.GetAverageRatingForStationMeasurement(historicStationMeasure, false);

                        historicStationMeasure.AggregatedUserRating = averageRating.Rating;
                        historicStationMeasure.AggregatedUserRatingCount = averageRating.Count;

                        this.dataContext.AirWatchEntities.AddToMeasurement(historicMeasure);
                        this.dataContext.AirWatchEntities.AddToStationMeasurement(historicStationMeasure);

                        this.RetrySave(retry);

                        // find the next historic measure and recalculate the user rating
                        StationMeasurement nextStationMeasurement = this.GetNextStationMeasurement(stationTimestamp.Value, databaseStation.StationId);

                        if (nextStationMeasurement != null)
                        {
                            AverageRating nextAverageRating = this.GetAverageRatingForStationMeasurement(nextStationMeasurement, true);

                            nextStationMeasurement.AggregatedUserRating = nextAverageRating.Rating;
                            nextStationMeasurement.AggregatedUserRatingCount = nextAverageRating.Count;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Attaches the user ratings for water stations missed years.
        /// </summary>
        /// <param name="stationCodes">The station codes.</param>
        private void AttachUserRatingsForWaterStationsMissedYears(Collection<string> stationCodes)
        {
            int currentYear = DateTime.Now.Year;
            foreach (string stationCode in stationCodes)
            {
                Station databaseStation = (from station in this.dataContext.AirWatchEntities.Station.Include("StationMeasurement.Measurement")
                                           where station.EuropeanCode == stationCode
                                           select station).FirstOrDefault();

                if (databaseStation != null)
                {
                    for (int year = 2008; year < currentYear; year++)
                    {
                        Component waterQualityComponent = null;

                        var waterQualityQuery = from component in this.dataContext.AirWatchEntities.Component
                                                where component.Caption == "CWQI"
                                                select component;
                        if (waterQualityQuery.Count() > 0)
                        {
                            waterQualityComponent = waterQualityQuery.First();
                        }

                        int measurementsInYear = (from stationMeasurement in databaseStation.StationMeasurement
                                                  where stationMeasurement.Measurement != null &&
                                                        stationMeasurement.Measurement.Timestamp.HasValue &&
                                                        stationMeasurement.Measurement.Timestamp.Value.Year == year
                                                  select stationMeasurement).Count();

                        if (measurementsInYear == 0)
                        {
                            Measurement m = new Measurement()
                            {
                                Component = waterQualityComponent,
                                Value = null,
                                Timestamp = new DateTime(year, 12, 31, 23, 59, 59)
                            };

                            StationMeasurement sm = new StationMeasurement()
                            {
                                Measurement = m,
                                Station = databaseStation
                            };

                            AverageRating averageRating = this.GetAverageRatingForStationMeasurement(sm, false);

                            if (averageRating.Count > 0)
                            {
                                sm.AggregatedUserRating = averageRating.Rating;
                                sm.AggregatedUserRatingCount = averageRating.Count;
                            }
                            else
                            {
                                sm.Station = null;
                                this.dataContext.AirWatchEntities.DeleteObject(m);
                                this.dataContext.AirWatchEntities.DeleteObject(sm);
                            }
                        }
                    }

                    this.RetrySave(5);
                }
            }
        }

        /// <summary>
        /// tries to save on datacontext the given retry number of times
        /// </summary>
        /// <param name="retry">The times to retry.</param>
        /// <returns>true if success, false if failed</returns>
        private bool RetrySave(int retry)
        {
            bool thrown = false;
            int attempt = 0;

            do
            {
                thrown = false;
                try
                {
                    this.dataContext.AirWatchEntities.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    thrown = true;
                    attempt++;
                }
                catch (UpdateException)
                {
                    thrown = true;
                    attempt++;
                }
            }
            while (thrown && attempt < retry);

            return !thrown;
        }

        /// <summary>
        /// Gets the next measurement from a time
        /// </summary>
        /// <param name="value">The timestamp to look in</param>
        /// <param name="stationId">The station's id to look at previous timestamps</param>
        /// <returns>null if none found, Station</returns>
        private StationMeasurement GetNextStationMeasurement(DateTime value, long stationId)
        {
            var stationMeasurements = this.dataContext.AirWatchEntities.GetStationMeasurementOfStationInTimestampOrder(stationId, value).ToList();

            StationMeasurement nextMeasure = null;

            if (stationMeasurements.Count() > 0)
            {
                nextMeasure = stationMeasurements.First();
            }

            return nextMeasure;
        }

        /// <summary>
        /// Creates and attaches a new station.
        /// </summary>
        /// <param name="xmlStation">The XML station.</param>
        /// <param name="stationCode">The station code.</param>
        /// <param name="stationTimestamp">The station timestamp.</param>
        /// <param name="stationPurpose">The station purpose (e.g. "Air", "Water" etc).</param>
        /// <param name="qualityComponent">The quality component (e.g. CAQI for air).</param>
        /// <returns>The new station to be attached if Measurements need to be attached to it also</returns>
        private Station CreateAndAttachNewStation(XElement xmlStation, string stationCode, DateTime? stationTimestamp, string stationPurpose, string qualityComponent)
        {
            Station station = null;

            decimal longitudeOut, latitudeOut;
            decimal? longitude = null, latitude = null;

            if (xmlStation.Element("longitude") != null && decimal.TryParse(xmlStation.Element("longitude").Value, out longitudeOut))
            {
                longitude = longitudeOut;
            }

            if (xmlStation.Element("latitude") != null && decimal.TryParse(xmlStation.Element("latitude").Value, out latitudeOut))
            {
                latitude = latitudeOut;
            }

            // only enter the data if there is a longitude and latitude, otherwise it would be useless
            // also ignore 
            if (longitude != null && latitude != null && !(longitude == 0 && latitude == 0))
            {
                string stationType = xmlStation.Element("type") != null ? xmlStation.Element("type").Value : string.Empty;
                string friendlyName = xmlStation.Element("name") != null ? xmlStation.Element("name").Value : string.Empty;

                Data.Location location = new Data.Location()
                {
                    Longitude = (decimal)longitude,
                    Latitude = (decimal)latitude,
                };

                // get the address of the station from the longitude/latitude values from bing
                string address = GetAddressFromLocation(location.Longitude, location.Latitude);

                XElement xmlMeasurements = xmlStation.Element("measurements");

                station = new Data.Station()
                {
                    EuropeanCode = stationCode,
                    Name = friendlyName,
                    FriendlyAddress = address,
                    TypeOfStation = stationType,
                    StationPurpose = stationPurpose,
                    Timestamp = stationTimestamp,
                    Location = location,
                };

                CheckQualityAndInsert(qualityComponent, xmlMeasurements, station);

                station.LocationReference.EntityKey = location.EntityKey;

                this.dataContext.AirWatchEntities.AddToLocation(location);
                this.dataContext.AirWatchEntities.AddToStation(station);

                LightMapType type = new LightMapType();
                switch (station.StationPurpose)
                {
                    case "Air":
                        type = LightMapType.AirStation;
                        break;

                    case "Water":
                        type = LightMapType.WaterStation;
                        break;
                }

                ServiceProvider.TileImageService.GenerateTilesForNewStation(station.Location.Longitude, station.Location.Latitude, type);
            }

            return station;
        }

        /// <summary>
        /// Creates the and attaches air measurement to the Entity Framework (does not save).
        /// also creates the StationMeasurement link between this and the station given
        /// </summary>
        /// <param name="xmlMeasurements">The measurements element.</param>
        /// <param name="station">The station to attach to.</param>
        /// <param name="stationTimestamp">The station timestamp.</param>
        /// <param name="ozoneComponent">The ozone component.</param>
        /// <param name="nitrogenDioxideComponent">The nitrogen dioxide component.</param>
        /// <param name="particulateComponent">The particulate component.</param>
        private void CreateAndAttachAirMeasurement(XElement xmlMeasurements, Station station, DateTime? stationTimestamp, Component ozoneComponent, Component nitrogenDioxideComponent, Component particulateComponent)
        {
            if (xmlMeasurements != null)
            {
                bool caqiFound = false;

                foreach (XElement xmlMeasure in xmlMeasurements.Elements("measurement"))
                {
                    string componentCaption = xmlMeasure.Attribute("component") != null ? xmlMeasure.Attribute("component").Value : null;

                    // if it's the quality index, only add it to the quality index of the station
                    if (componentCaption.ToUpperInvariant() == "CAQI")
                    {
                        int? quality = (int?)GetMeasurementOfType(xmlMeasure, "average");

                        if (quality != null && quality >= MINAIRINDEX && quality <= MAXAIRINDEX)
                        {
                            station.QualityIndex = quality;
                        }
                        else
                        {
                            station.QualityIndex = null;
                        }

                        caqiFound = true;
                    }
                    else
                    {
                        // if it's a normal component
                        Data.Measurement measurement = CreateMeasurement(xmlMeasure, "average", stationTimestamp);

                        bool componentFound = true;

                        switch (componentCaption.ToUpperInvariant())
                        {
                            case "O3":
                                if (ozoneComponent != null)
                                {
                                    measurement.ComponentReference.EntityKey = ozoneComponent.EntityKey;
                                }

                                break;

                            case "NO2":
                                if (nitrogenDioxideComponent != null)
                                {
                                    measurement.ComponentReference.EntityKey = nitrogenDioxideComponent.EntityKey;
                                }

                                break;

                            case "PM10":
                                if (particulateComponent != null)
                                {
                                    measurement.ComponentReference.EntityKey = particulateComponent.EntityKey;
                                }

                                break;

                            default:
                                componentFound = false;
                                break;
                        }

                        if (componentFound)
                        {
                            StationMeasurement stationMeasurement = new StationMeasurement()
                            {
                                Measurement = measurement,
                                Station = station,
                            };

                            this.dataContext.AirWatchEntities.AddToMeasurement(measurement);
                            this.dataContext.AirWatchEntities.AddToStationMeasurement(stationMeasurement);
                        }
                    }
                }

                if (!caqiFound)
                {
                    station.QualityIndex = null;
                }
            }
        }

        /// <summary>
        /// Gets the average rating for station.
        /// </summary>
        /// <param name="currentStation">The station to find the rating from.</param>
        /// <returns>The Average Rating for the station</returns>
        private AverageRating GetAverageRatingForStation(Station currentStation)
        {
            AverageRating averageRating;

            currentStation.LocationReference.Load();
            if (currentStation.Location == null || currentStation.Timestamp == null)
            {
                return new AverageRating()
                {
                    Count = 0,
                    Rating = 0,
                };
            }

            decimal longitude = currentStation.Location.Longitude, latitude = currentStation.Location.Latitude;

            averageRating = this.GetAverageRating(currentStation.StationId, currentStation.Timestamp.Value, longitude, latitude);

            return averageRating;
        }

        /// <summary>
        /// Gets the average rating for a specific station measurement.
        /// Used for when historic user rating data is being batch processed
        /// </summary>
        /// <param name="stationMeasurement">The station measurement.</param>
        /// <param name="attached">true if the station measurement is part of the database object graph
        /// (will avoid loading on stationMeasurement if it is not loaded)</param>
        /// <returns>
        /// The average rating for the station measurement and the previous timestamp
        /// </returns>
        private AverageRating GetAverageRatingForStationMeasurement(StationMeasurement stationMeasurement, bool attached)
        {
            AverageRating averageRating;

            if (attached)
            {
                stationMeasurement.StationReference.Load();
            }

            Station currentStation = stationMeasurement.Station;

            Location currentLocation = null;
            if (currentStation != null)
            {
                currentStation.LocationReference.Load();
                currentLocation = currentStation.Location;
            }

            if (attached)
            {
                stationMeasurement.MeasurementReference.Load();
            }

            Measurement currentMeasurement = stationMeasurement.Measurement;

            if (currentMeasurement == null || currentMeasurement.Timestamp == null ||
                currentStation == null || currentLocation == null)
            {
                averageRating = new AverageRating()
                {
                    Count = 0,
                    Rating = 0,
                };
            }
            else
            {
                averageRating = this.GetAverageRating(currentStation.StationId, currentMeasurement.Timestamp.Value, currentLocation.Longitude, currentLocation.Latitude);
            }

            return averageRating;
        }

        /// <summary>
        /// Gets the average rating given a stationId, timestamp of the rating, longitude and latitude.
        /// </summary>
        /// <param name="stationId">The id of the station to find the previous measurement.</param>
        /// <param name="timestamp">The timestamp to go up to (i.e. the current date).</param>
        /// <param name="longitude">The longitude of the station.</param>
        /// <param name="latitude">The latitude of the station.</param>
        /// <returns>The average rating at the longitude and latitude provided between given timestamp and the previous measurement's time of the station given</returns>
        private AverageRating GetAverageRating(long stationId, DateTime timestamp, decimal longitude, decimal latitude)
        {
            AverageRating averageRating;

            // find the previous timestamp
            DateTime? previousTimestamp;

            StationMeasurement previousStationMeasurement;

            var stationMeasurements = this.dataContext.AirWatchEntities.GetPreviousStationMeasurement(stationId, timestamp);

            List<StationMeasurement> stationMeasurementList = stationMeasurements.ToList();

            if (stationMeasurementList.Count() > 0)
            {
                previousStationMeasurement = stationMeasurementList[0];
            }
            else
            {
                previousStationMeasurement = null;
            }

            if (previousStationMeasurement == null)
            {
                previousTimestamp = null;
            }
            else
            {
                previousStationMeasurement.MeasurementReference.Load();
                if (previousStationMeasurement.Measurement != null)
                {
                    previousTimestamp = previousStationMeasurement.Measurement.Timestamp;
                }
                else
                {
                    previousTimestamp = null;
                }
            }

            // find the rating for this historic measure
            // if there is a previous timestamp, use that, otherwise go to the first of the year specified
            if (previousTimestamp != null && previousTimestamp.Value.Year == timestamp.Year)
            {
                // if the previous is in a different year, use the first of the year
                averageRating = ServiceProvider.RatingService.GetRating((double)latitude, (double)longitude, "Water", Offset, previousTimestamp.Value, timestamp);
            }
            else
            {
                DateTime firstOfTheYear = new DateTime(timestamp.Year, 1, 1);

                averageRating = ServiceProvider.RatingService.GetRating((double)latitude, (double)longitude, "Water", Offset, firstOfTheYear, timestamp);
            }

            if (averageRating == null)
            {
                averageRating = new AverageRating()
                {
                    Rating = 0,
                    Count = 0,
                };
            }

            averageRating.Rating = averageRating.Rating == null ? 0 : averageRating.Rating;
            averageRating.Count = averageRating.Count == null ? 0 : averageRating.Count;

            return averageRating;
        }
    }
}
