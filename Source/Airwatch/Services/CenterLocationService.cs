﻿//-----------------------------------------------------------------------
// <copyright file="CenterLocationService.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01/07/2009</date>
// <summary>Service for center location related transactions</summary>
//----------------------------------------------------------------------- 

namespace Microsoft.AirWatch.Core.ApplicationServices
{
    #region Using

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.EntityClient;
    using System.Data.SqlClient;
    using System.Linq;
    using Microsoft.AirWatch.Core.ApplicationServices.DataStructures;
    using Microsoft.AirWatch.Core.Data;
    using core = Microsoft.AirWatch.Core.Data;
    using Microsoft.WindowsAzure.ServiceRuntime;
    using System.Diagnostics;

    #endregion

    /// <summary>
    /// Service for center location related transactions
    /// </summary>
    public class CenterLocationService
    {
        /// <summary>
        /// identifier of the Air Metadata within the database
        /// </summary>
        private const string AirMetadata = "airModel";

        /// <summary>
        /// Degrees offset
        /// </summary>
        private static decimal degreesOffset;

        /// <summary>
        /// Internal representation of Entity Framework data context.
        /// </summary>
        private core.DataContext dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="CenterLocationService"/> class.
        /// </summary>
        internal CenterLocationService()
        {
            this.dataContext = core.DataContext.Create();

            var query = this.dataContext.AirWatchEntities.MapModelData.Where(m => m.Type == AirMetadata);
            List<MapModelData> mapModelMetadata = query.ToList();

            if (mapModelMetadata.Count == 0)
            {
                degreesOffset = 0.5M;
            }
            else
            {
                degreesOffset = mapModelMetadata.First().Resolution != null ? (decimal)mapModelMetadata.First().Resolution.Value : 0.5M;
            }
        }

        /// <summary>
        /// Gets the Degrees offset
        /// </summary>
        public static decimal DegreesOffsetProp
        {
            get
            {
                return degreesOffset;
            }
        }

        #region Data Input (WorkerRoleOnly)

        /// <summary>
        /// Sets the resolution.
        /// </summary>
        /// <param name="resolution">The resolution.</param>
        public void SetResolution(float resolution)
        {
            var query = this.dataContext.AirWatchEntities.MapModelData.Where(m => m.Type == AirMetadata);
            List<MapModelData> mapModelMetadata = query.ToList();

            if (mapModelMetadata.Count == 0)
            {
                MapModelData newMetaData = new MapModelData()
                {
                    Type = AirMetadata,
                    Resolution = resolution
                };

                this.dataContext.AirWatchEntities.AddToMapModelData(newMetaData);

                this.RetrySave(5);
            }
            else
            {
                mapModelMetadata.First().Resolution = resolution;

                this.RetrySave(5);
            }
        }

        /// <summary>
        /// Peter's developement / Inputs the data from a list of points.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="retry">The times that sql will retry before it fails.</param>
        /// <returns>If the data was entered</returns>
        public bool InputData(Collection<MapModelPoint> points, long timestamp, int retry)
        {
            //// check that dataContext is not null as we reference properties in that in Name = AirWatchEntites
            if (points != null && this.dataContext != null)
            {
                bool thrown = false;
                int attempt = 0;

                string connectionString = RoleEnvironment.GetConfigurationSettingValue("ConnectionString");
                string entityFrameworkConnection = String.Format(System.Globalization.CultureInfo.InvariantCulture, RoleEnvironment.GetConfigurationSettingValue("EFConnection"), connectionString);

                do
                {
                    thrown = false;
                    Trace.TraceInformation("Map Model data entry: Starting attempt " + attempt);

                    Trace.TraceInformation("Map Model data entry: Removing previous entries in MapModelDataEntry Table");

                    // Firstly Delete anything which could be in the staging table already
                    try
                    {
                        using (EntityConnection conn = new EntityConnection(entityFrameworkConnection))
                        {
                            // call the transfer procedure to put the data into the production environment in batches
                            var deleteDataCommand = conn.CreateCommand();

                            deleteDataCommand.CommandType = CommandType.StoredProcedure;
                            deleteDataCommand.CommandText = "AirWatchEntities.DeleteMapModelDataEntry";
                            conn.Open();
                            deleteDataCommand.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    catch (SqlException ex)
                    {
                        Trace.TraceInformation("Map Model data entry: Removing previous entries in MapModelDataEntry Table failed with SQL Exception. message " + ex.Message + " Inner Exception: " + ex.InnerException.Message);
                        thrown = true;
                        attempt++;
                    }

                    DateTime time = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    time = time.ToLocalTime().AddSeconds(timestamp);
                    long ts = timestamp;
                    ts = ts + 1;

                    // Populate the datatable with the data from the point object
                    DataTable dt = new DataTable();
                    dt.Locale = System.Globalization.CultureInfo.InvariantCulture;

                    // Setup the columns in the datatable
                    dt.Columns.Add("Longitude");
                    dt.Columns.Add("Latitude");
                    dt.Columns.Add("QualityValue");
                    dt.Columns.Add("CAQI");
                    dt.Columns.Add("Ozone");
                    dt.Columns.Add("NitrogenDioxide");
                    dt.Columns.Add("ParticulateMatter10");
                    dt.Columns.Add("Timestamp");

                    foreach (MapModelPoint point in points)
                    {
                        // Create new row, populte it with the point data and add it to the datatable
                        DataRow row = dt.NewRow();
                        row.SetField("Longitude", point.Longitude);
                        row.SetField("Latitude", point.Latitude);
                        row.SetField("QualityValue", point.QualityValue);
                        row.SetField("CAQI", point.Caqi);
                        row.SetField("Ozone", point.Ozone);
                        row.SetField("NitrogenDioxide", point.NO2);
                        row.SetField("ParticulateMatter10", point.PM10);
                        row.SetField("Timestamp", time);
                        dt.Rows.Add(row);
                    }

                    Trace.TraceInformation("Map Model data entry: adding new entries in MapModelDataEntry Table");

                    // Do the SqlBulkLoad and push all the data from the datatable into the database
                    using (SqlBulkCopy copy = new SqlBulkCopy(connectionString))
                    {
                        copy.BulkCopyTimeout = 180; // wait 3 minutes for the copy
                        copy.ColumnMappings.Add("Longitude", "Longitude");
                        copy.ColumnMappings.Add("Latitude", "Latitude");
                        copy.ColumnMappings.Add("QualityValue", "QualityValue");
                        copy.ColumnMappings.Add("CAQI", "CAQI");
                        copy.ColumnMappings.Add("Ozone", "Ozone");
                        copy.ColumnMappings.Add("NitrogenDioxide", "NitrogenDioxide");
                        copy.ColumnMappings.Add("ParticulateMatter10", "ParticulateMatter10");
                        copy.ColumnMappings.Add("Timestamp", "Timestamp");
                        copy.DestinationTableName = "MapModelDataEntry";
                        copy.WriteToServer(dt);
                    }

                    if (!thrown)
                    {
                        Trace.TraceInformation("Map Model data entry: Transfering from MapModelDataEntry Table to live tables attempt " + attempt);

                        try
                        {
                            using (EntityConnection conn = new EntityConnection(entityFrameworkConnection))
                            {
                                // call the transfer procedure to put the data into the production environment in batches
                                var transferDataCommand = conn.CreateCommand();
                                transferDataCommand.CommandTimeout = 900; // wait up to 15 mins for the transfer

                                transferDataCommand.CommandType = CommandType.StoredProcedure;
                                transferDataCommand.CommandText = "AirWatchEntities.TransferMapModelData";

                                conn.Open();
                                transferDataCommand.ExecuteNonQuery();
                                conn.Close();
                            }
                        }
                        catch (SqlException ex)
                        {
                            Trace.TraceInformation("Map Model data entry: Transfering from MapModelDataEntry Table failed with SQL Exception. message " + ex.Message + " Inner Exception: " + ex.InnerException.Message);
                            thrown = true;
                            attempt++;
                        }
                        catch (EntityCommandExecutionException ex)
                        {
                            Trace.TraceInformation("Map Model data entry: Transfering from MapModelDataEntry Table failed with EntityCommandExecutionException. message " + ex.Message + " Inner Exception: " + ex.InnerException.Message);
                            thrown = true;
                            attempt++;
                        }
                    }
                }
                while (thrown && attempt < retry);

                Trace.TraceInformation("Map Model data entry: Transfering from MapModelDataEntry Table completed on attempt " + attempt);

                if (thrown)
                {
                    Trace.TraceInformation("Map Model data entry: Transfering from MapModelDataEntry Table failed");

                    // if it fails, remove all data from the staging table
                    try
                    {
                        using (EntityConnection conn = new EntityConnection(entityFrameworkConnection))
                        {
                            // call the transfer procedure to put the data into the production environment in batches
                            var deleteDataCommand = conn.CreateCommand();

                            deleteDataCommand.CommandType = CommandType.StoredProcedure;
                            deleteDataCommand.CommandText = "AirWatchEntities.DeleteMapModelDataEntry";
                            conn.Open();
                            deleteDataCommand.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    catch (SqlException)
                    {
                        thrown = true;
                        attempt++;
                    }

                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        #endregion

        #region Loading
        /// <summary>
        /// Gets the centre location for point. 
        /// Returns null if not found
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>Centre location</returns>
        public core.CentreLocation GetCentreLocationForPoint(decimal longitude, decimal latitude)
        {
            var query = from core.CentreLocation centreLoc in this.dataContext.AirWatchEntities.GetCentreLocationForPoint(longitude, latitude, degreesOffset)
                        select centreLoc;

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Gets measurements for point.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <returns>List of Measurement entity</returns>
        public IEnumerable<core.Measurement> GetMeasurementsForPoint(decimal longitude, decimal latitude)
        {
            var query = from core.Measurement measurement in this.dataContext.AirWatchEntities.GetIndexForPoint(longitude, latitude, degreesOffset, "CAQI")
                        select measurement;

            return query;
        }

        /// <summary>
        /// Gets measurements for point.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="component">The component.</param>
        /// <returns>List of Measurement entity</returns>
        public IEnumerable<core.Measurement> GetIndexForPoint(decimal longitude, decimal latitude, string component)
        {
            var query = from core.Measurement measurement in this.dataContext.AirWatchEntities.GetIndexForPoint(longitude, latitude, degreesOffset, component)
                        select measurement;

            return query;
        }
        #endregion

        /// <summary>
        /// tries to save on datacontext the given retry number of times
        /// </summary>
        /// <param name="retry">The times to retry.</param>
        /// <returns>true if success, false if failed</returns>
        private bool RetrySave(int retry)
        {
            bool thrown = false;
            int attempt = 0;

            do
            {
                thrown = false;
                try
                {
                    this.dataContext.AirWatchEntities.SaveChanges();
                }
                catch (OptimisticConcurrencyException)
                {
                    thrown = true;
                    attempt++;
                }
                catch (UpdateException)
                {
                    thrown = true;
                    attempt++;
                }
            }
            while (thrown && attempt < retry);

            return !thrown;
        }
    }
}
