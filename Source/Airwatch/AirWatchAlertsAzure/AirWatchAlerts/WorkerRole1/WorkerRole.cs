﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using System.Net;
using System.Text;
using System.Data.Services.Client;
using Microsoft.ServiceHosting.ServiceRuntime;

using dev.virtualearth.net.webservices.v1.geocode;

using Microsoft.AirWatch.Common;
using Microsoft.AirWatch.Core.ApplicationServices;
using Microsoft.AirWatch.Core.Data;

using System.Mobile.Messaging;
using Microsoft.Mobile.Messaging.Utils;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.Azure.WorkerRole.Utilities;
using Microsoft.AirWatch.Notifications;
using Microsoft.AirWatch.Notifications.Storage;




namespace WorkerRole1
{
    public class WorkerRole : RoleEntryPoint
    {
        private WorkerRoleState m_State = WorkerRoleState.Uninitialized;
        private int m_MessagesRecv = 0;
        private int m_MessagesSent = 0;
        private int m_NestedOperations = 0;
        private const int TTL_ELECTION_REQUEST = 5*60;
        private const int TTL_NOTIFICATION_COUNTRY_CODE = 30 * 60;
        private const int TTL_OUTBOUND_MESSAGES = 60 * 60;
        private const int WORKER_ROLE_WAIT_INTERVAL = -1;
        private const int OPERATIONS_MASTER_TIMER_DELAYED_START = 10 * 1000;
        private const int OPERATIONS_MASTER_TIMER_INTERVAL = 1 * 1000;
        private const int ELECTION_MASTER_TIMER_DELAYED_START = 30 * 1000;
        private const int ELECTION_MASTER_TIMER_INTERVAL = 2 * 1000;
        private const string INBOUND_MESSAGE_QUEUE_NAME = "queue-inbound-messages";
        private const string OUTBOUND_MESSAGE_QUEUE_NAME = "queue-outbound-messages";
        private const string INBOUND_CONTROL_QUEUE_NAME = "queue-inbound-control";
        private const string ELECTION_MASTER_QUEUE_NAME = "queue-election-master";
        private const string NOTIFICATION_COUNTRY_QUEUE_NAME = "queue-notify-country";

        private const string MEASUREMENT_AIR_QUALITY = "CAQI";
        private const string MEASUREMENT_WATER_QUALITY = "CWQI";

        private Guid m_RoleId;
        private StorageManager m_StorageManager;
        private ShortMessageSerializer m_smsSerializer;
        private MessageQueue m_mqElectionMaster;
        private MessageQueue m_mqInboundMessages;
        private MessageQueue m_mqOutboundMessages;
        private MessageQueue m_mqNotificationCountry;
        private ManualResetEvent m_WorkerRoleStarting;
        private ManualResetEvent m_WorkerRoleExit;
        private WaitHandle[] m_WaitHandleArray;
        private Mutex m_OperationsMutex;
        private Timer m_OperationsTimer;
        private Timer m_ElectionTimer;
        private string m_szBingToken;
        private string m_szLastElectionRequest;


        public override void Start()
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole.Start()");

            try
            {
                //
                // Initialize this worker role configuration
                //
                InitializeConfiguration();


                //
                // Run this worker role and wait for exit signal
                //
                Run();


                //
                // Normal exit path
                //
                AzureLogging.WriteToLog("Information", "AirWatch Notification Worker Role exiting normally.");
            }
            catch (System.Threading.ThreadAbortException taex)
            {
                Trace.WriteLine(String.Format("THREAD ABORT: {0}", taex.Message));
                Trace.WriteLine(String.Format("THREAD ABORT DUMP:\n{0}", taex));
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole.Start()");
        }



        public override void Stop()
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole.Stop()");

            try
            {
                //
                // Log stop request
                //
                AzureLogging.WriteToLog("Information", "AirWatch Notification Worker Process Stop() method called.");

                //
                // Initialize to stopping state;
                //
                m_State = WorkerRoleState.Stopping;

                //
                // Indicate shutdown request
                //
                m_WorkerRoleExit.Set();

                //
                // Disable operations timer
                //
                m_OperationsTimer.Change(-1, -1);

                //
                // Disable election timer
                //
                m_ElectionTimer.Change(-1, -1);

                //
                // Stop inbound queue from polling
                //
                m_mqInboundMessages.StopReceiving();

                //
                // Stop outbound queue from polling
                //
                m_mqOutboundMessages.StopReceiving();

                //
                // Stopped state;
                //
                m_State = WorkerRoleState.Stopped;

            }
            catch (System.Threading.ThreadAbortException taex)
            {
                Trace.WriteLine(String.Format("THREAD ABORT: {0}", taex.Message));
                Trace.WriteLine(String.Format("THREAD ABORT DUMP:\n{0}", taex));
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            //
            // Base shutdown code
            //
            base.Stop();

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole.Stop()");
        }

        public override RoleStatus GetHealthStatus()
        {
            RoleStatus rs = RoleStatus.NonExistent;

            switch (m_State)
            {
                case WorkerRoleState.Uninitialized:
                    rs = RoleStatus.Starting;
                    break;
                case WorkerRoleState.Initialized:
                    rs = RoleStatus.Starting;
                    break;
                case WorkerRoleState.Starting:
                    rs = RoleStatus.Starting;
                    break;
                case WorkerRoleState.Running:
                    rs = RoleStatus.Healthy;
                    break;
                case WorkerRoleState.Stopping:
                    rs = RoleStatus.Stopping;
                    break;
                case WorkerRoleState.Stopped:
                    rs = RoleStatus.Stopped;
                    break;
                default:
                    rs = RoleStatus.Unhealthy;
                    break;
            }
            return rs;
        }

        private void InitializeConfiguration()
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole.InitializeConfiguration()");

            try
            {
                //
                // Log start
                //
                AzureLogging.WriteToLog("Information", "AirWatch Notification Worker Role InitializeConfiguration() method");

                //
                // Initialize to starting state;
                //
                m_State = WorkerRoleState.Starting;

                //
                // Create RoleId for election master process
                //
                m_RoleId = Guid.NewGuid();

                //
                // Persist logging id
                //
                AzureLogging.LoggingId = m_RoleId.ToString();

                //
                // Log start
                //
                AzureLogging.WriteToLog("Information", "AirWatch Notification Worker Role entry point called");

                //
                // Catch unhandled exceptions
                //
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);


                //
                // Log Init
                //

                //
                // Create a serializer
                //
                m_smsSerializer = new ShortMessageSerializer();

                //
                // Set when worker role operations timer is activated 
                //
                m_WorkerRoleStarting = new ManualResetEvent(false);

                //
                // Set when worker role should stop and exit
                //
                m_WorkerRoleExit = new ManualResetEvent(false);

                //
                // Create wait handle array
                //
                m_WaitHandleArray = new WaitHandle[1];
                m_WaitHandleArray[0] = m_WorkerRoleExit;


                //
                // Create our location resolver & add it to the notification engine
                //
                NotificationEngine.GetCurrentEngine().LocationResolverHandler += new LocationResolver(WorkerRole_LocationResolverHandler);

                //
                // Create our measurement handler & add it to the notification engine
                //
                NotificationEngine.GetCurrentEngine().RetrieveMeasurementDataHandler += new RetrieveMeasurementData(WorkerRole_RetrieveMeasurementDataHandler);

                //
                // Create our air quality index handler & add it to the notification engine
                //
                NotificationEngine.GetCurrentEngine().RetrieveAirQualityIndexHandler += new RetrieveAirQualityIndexDelegate(WorkerRole_RetrieveAirQualityIndexHandler);

                //
                // Create our water quality index handler & add it to the notification engine
                //
                NotificationEngine.GetCurrentEngine().RetrieveWaterQualityIndexHandler += new RetrieveWaterQualityIndexDelegate(WorkerRole_RetrieveWaterQualityIndexHandler);


                //
                // Create our send message handler & add it to the notification engine
                //
                NotificationEngine.GetCurrentEngine().SendMessageHandler += new SendMessageDelegate(WorkerRole_SendMessageHandler);

                //
                // Operations Mutex
                //
                m_OperationsMutex = new Mutex();

                //
                // Configure Operations Master timer
                //
                m_OperationsTimer = new Timer(new TimerCallback(OnOperationsTimer));
                m_OperationsTimer.Change(OPERATIONS_MASTER_TIMER_DELAYED_START, OPERATIONS_MASTER_TIMER_INTERVAL);

                //
                // Wait until operations master timer is running before completing initialization phase
                //
                if (m_WorkerRoleStarting.WaitOne(-1))
                {
                    AzureLogging.WriteToLog("Verbose", ">> m_WorkerRoleStarting.WaitOne(-1)");

                    //
                    // Create & initialize the azure storage manager
                    //
                    m_StorageManager = new StorageManager();

                    m_StorageManager.Initialize();

                    //
                    // Configure Election Master timer
                    //
                    m_ElectionTimer = new Timer(new TimerCallback(OnElectionMasterTimer));


#if false || DELETE_QUEUES
                    //
                    // Delete Election Master Queue
                    //
                    m_StorageManager.DeleteMessageQueues(ELECTION_MASTER_QUEUE_NAME, false);
#endif

                    //
                    // Open queue for handling election master requests
                    //
                    m_mqElectionMaster = m_StorageManager.OpenMessageQueue(ELECTION_MASTER_QUEUE_NAME, true);



#if false || DELETE_QUEUES
                    //
                    // Delete Inbound Message Queue
                    //
                    m_StorageManager.DeleteMessageQueues(INBOUND_MESSAGE_QUEUE_NAME, false);
#endif

                    //
                    // Open queue for handling inbound messages
                    //
                    m_mqInboundMessages = m_StorageManager.OpenMessageQueue(INBOUND_MESSAGE_QUEUE_NAME, true);


                    //
                    // Configure Message Queue Event Handlers for new messages
                    //
                    m_mqInboundMessages.MessageReceived += new MessageReceivedEventHandler(OnMessageReceived);


#if false || DELETE_QUEUES
                    //
                    // Delete Outbound Message Queue
                    //
                    m_StorageManager.DeleteMessageQueues(OUTBOUND_MESSAGE_QUEUE_NAME, false);
#endif

                    //
                    // Open queue for handling outbound messages
                    //
                    m_mqOutboundMessages = m_StorageManager.OpenMessageQueue(OUTBOUND_MESSAGE_QUEUE_NAME, true);


                    //
                    // Configure Message Queue Event Handlers for sent messages
                    //
                    m_mqOutboundMessages.MessageReceived += new MessageReceivedEventHandler(OnMessageSent);


#if false || DELETE_QUEUES
                    //
                    // Delete Notification Country Code Queue
                    //
                    m_StorageManager.DeleteMessageQueues(NOTIFICAION_COUNTRY_QUEUE_NAME, false);
#endif

                    //
                    // Open queue for handling notification requests for a given country code
                    //
                    m_mqNotificationCountry = m_StorageManager.OpenMessageQueue(NOTIFICATION_COUNTRY_QUEUE_NAME, true);


                    //
                    // Configure Message Queue Event Handlers for notification processing for a given country code
                    //
                    m_mqNotificationCountry.MessageReceived += new MessageReceivedEventHandler(m_mqNotificationCountry_MessageReceived);

                    AzureLogging.WriteToLog("Verbose", "<< m_WorkerRoleStarting.WaitOne(-1)");
                }
            }
            catch (System.Threading.ThreadAbortException taex)
            {
                Trace.WriteLine(String.Format("THREAD ABORT: {0}", taex.Message));
                Trace.WriteLine(String.Format("THREAD ABORT DUMP:\n{0}", taex));
                throw taex;
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole.InitializeConfiguration()");
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole.CurrentDomain_UnhandledException()");

            AzureLogging.WriteToLog("Critical", "CurrentDomain_UnhandledException()");

            AzureLogging.WriteToLog("Verbose", String.Format("Totals: Recv [{0}], Sent [{1}]", m_MessagesRecv, m_MessagesSent));

            System.Exception ex = e.ExceptionObject as System.Exception;

            if (ex != null)
            {
                AzureLogging.WriteToLog("Critical", String.Format("UnhandledException: {0}", ex.Source.ToString()));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole.CurrentDomain_UnhandledException()");
        }


        private void Run()
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole.Run()");

            try
            {
                //
                // Log start
                //
                AzureLogging.WriteToLog("Information", "AirWatch Notification Worker Role Run() method");

                if (m_State == WorkerRoleState.Starting)
                {
                    if (m_StorageManager != null)
                    {
                        if ((m_StorageManager != null) && (m_mqElectionMaster != null) && (m_mqNotificationCountry != null))
                        {
                            if ((m_mqInboundMessages != null) && (m_mqInboundMessages != null))
                            {
                                AzureLogging.WriteToLog("Information", "Loading Additional Localization Resources...");

                                //
                                // Load additional language resources
                                //
                                int iResourcesLoaded = NotificationEngine.GetCurrentEngine().LoadLocalizationData();

                                AzureLogging.WriteToLog("Information", "Testing Bing Map Services...");

                                //
                                // Resolve a location to kick start request of Bing token
                                //
                                MessageNotificationLocation location = ResolveLocation("london,uk", "en-GB");

                                AzureLogging.WriteToLog("Information", "Testing AirWatch Measurement Services...");

                                //
                                // Validate DB connection
                                //
                                if (location != null)
                                {
                                    decimal? dMeasurement = GetMeasurement((decimal)location.Longitude, (decimal)location.Latitude, "CAQI");

                                    if (dMeasurement != null)
                                    {
                                        AzureLogging.WriteToLog("Information", "All connection verified, entering running state...");

                                        //
                                        // Update to running state
                                        //
                                        m_State = WorkerRoleState.Running;

                                        AzureLogging.WriteToLog("Information", "Enabling azure queue message handlers...");
                                        //
                                        // Enable timers for all message queues
                                        //
                                        m_ElectionTimer.Change(ELECTION_MASTER_TIMER_DELAYED_START, ELECTION_MASTER_TIMER_INTERVAL);
                                        m_mqInboundMessages.StartReceiving();
                                        m_mqOutboundMessages.StartReceiving();
                                        m_mqNotificationCountry.StartReceiving();

                                    }
                                    else
                                    {
                                        AzureLogging.WriteToLog("Information", "Starting - db failed");
                                    }
                                }
                            }
                        }
                    }
                }

                if (m_State == WorkerRoleState.Running)
                {
                    AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.MainLoop()");
                    do
                    {
                        AzureLogging.WriteToLog("Information", "AirWatch.Notifications.MainLoop() - waiting for signal for role to exit");
                    }
                    while (WaitHandle.WaitAny(m_WaitHandleArray, WORKER_ROLE_WAIT_INTERVAL, false) != 0);
                    AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.MainLoop()");
                }
                else
                {
                    this.Stop();
                }
            }
            catch (System.Threading.ThreadAbortException taex)
            {
                Trace.WriteLine(String.Format("THREAD ABORT: {0}", taex.Message));
                Trace.WriteLine(String.Format("THREAD ABORT DUMP:\n{0}", taex));
                throw taex;
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole.Run()");
        }


        private void OnOperationsTimer(object state)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.OnOperationsTimer()");

            try
            {
                if (m_OperationsMutex.WaitOne(0))
                {
                    AzureLogging.WriteToLog("Verbose", ">> OnOperations()");

                    AzureLogging.WriteToLog("Verbose", String.Format("OnOperations() - State [{0}]", m_State));

                    switch (m_State)
                    {
                        case WorkerRoleState.Starting:
                            AzureLogging.WriteToLog("Information", ">> OnOperations() - setting worker role starting event");
                            m_WorkerRoleStarting.Set();
                            AzureLogging.WriteToLog("Information", "<< OnOperations() - setting worker role starting event");
                            break;

                        case WorkerRoleState.Running:
                            {
                                //
                                // Check if any other worker role has called an election
                                //
                                if (CheckElectionRequests())
                                {
                                    //
                                    // Submit this worker role into the election master to drive the notification process
                                    //
                                    SubmitElectionRequest(false);
                                }
                                else
                                {
                                    //
                                    // Check if we need to send notifications (should this be on it's own thread?)
                                    //
                                    bool fCriteraMet = NotificationEngine.GetCurrentEngine().EvaluateNotificationCriteria();

                                    if (fCriteraMet)
                                    {
                                        AzureLogging.WriteToLog("Information", "OnOperations() - PROCESSING NOTIFICATIONS");

                                        //
                                        // Submit this worker role into the election master to drive the notification process
                                        //
                                        SubmitElectionRequest(true);
                                    }
                                    else
                                    {
                                        AzureLogging.WriteToLog("Verbose", "OnOperations() - SKIPPING NOTIFICATIONS, CRITERIA NOT MET");
                                    }
                                }
                            }
                            break;
                        
                        default:
                            AzureLogging.WriteToLog("Warning", String.Format(">> OnOperations() - No Valid Operation Timer Action for State [{0}]", m_State));
                            break;

                    }

                    AzureLogging.WriteToLog("Verbose", "<< OnOperations()");

                    //
                    // Release mutex
                    //
                    m_OperationsMutex.ReleaseMutex();
                }
                else
                {
                    AzureLogging.WriteToLog("Warning", "OnOperationsTimer() - task already active, skipping...");
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OnOperationsTimer()");
        }



        private bool SubmitElectionRequest(bool fElectionMaster)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.SubmitElectionRequest()");

            bool fSuccess = false;

            try
            {
                //
                // Submit this worker roles' ID for the election process
                //
                string szElectionMessage = String.Format("{0} - [{1}] - [{2}]", m_RoleId.ToString(), DateTime.UtcNow.Ticks, fElectionMaster.ToString());

                if (fSuccess = m_mqElectionMaster.PutMessage(new Message(szElectionMessage), TTL_ELECTION_REQUEST))
                {
                    AzureLogging.WriteToLog("Information", String.Format("Queued Election [{2}]: RoleID [{0}], Time [{1}]", m_RoleId, DateTime.UtcNow.ToString(), fElectionMaster? "Request": "Response"));

                    m_szLastElectionRequest = null;
                }

                if (!fSuccess)
                {
                    AzureLogging.WriteToLog("Error", String.Format("Failed to Queue Election Message: RoleID [{0}], Time [{1}]", m_RoleId, DateTime.UtcNow.ToString()));
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.SubmitElectionRequest()");

            return fSuccess;
        }


        private void OnElectionMasterTimer(object state)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.OnElectionMasterTimer()");

            try
            {
                //
                // Check for any available election requests in the queue
                //
                Message queueMessage = this.m_mqElectionMaster.PeekMessage();

                //
                // If messages available, Check if this worker role is the election winner
                //
                if (queueMessage != null)
                {
                    if (queueMessage.ContentAsString() != m_szLastElectionRequest)
                    {
                        //
                        // If the top message in the queue is this worker role's Id, then it is the election winner.
                        //
                        if ((queueMessage.ContentAsString().StartsWith(m_RoleId.ToString(), StringComparison.OrdinalIgnoreCase)) && (queueMessage.ContentAsString().EndsWith(String.Format("[{0}]", bool.TrueString), StringComparison.OrdinalIgnoreCase)))
                        {
                            Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}]", m_RoleId));

                            //
                            // Do the work of the election master first, then clear queue
                            //
                            int iNestLevel = Interlocked.Add(ref m_NestedOperations, 1);

                            Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] >> NESTED OPERATIONS ({1})", m_RoleId, iNestLevel));

                            if (iNestLevel == 1)
                            {
                                Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] >> PROCESS ELECTION MASTER TASK ({1})", m_RoleId, iNestLevel));

                                //
                                // Check if we need to send notifications (should this be on it's own thread?)
                                //
#if true || DISABLE_NOTIFICATIONS
#else
                                NotificationEngine.GetCurrentEngine().ProcessNotifications(new ProcessNotificationsDelegate(QueueNotificationsForCountryCode));
#endif

#if ARTIFICIAL_DELAYS_FOR_TESTING
                                Thread.Sleep(2 * ELECTION_MASTER_TIMER_INTERVAL);
#endif
                                Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] << PROCESS ELECTION MASTER TASK ({1})", m_RoleId, iNestLevel));

                                Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] >> WAITING FOR OTHER ROLES TO RESPOND ({1})", m_RoleId, iNestLevel));

                                //
                                // Ensure all other roles have responded before we begin
                                //
                                Thread.Sleep(2 * ELECTION_MASTER_TIMER_INTERVAL);

                                Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] << WAITING FOR OTHER ROLES TO RESPOND ({1})", m_RoleId, iNestLevel));

                                //
                                // Purge the queue of all other roles pending election messages for this round
                                // 
                                m_mqElectionMaster.Clear();

                            }
                            else
                            {
                                Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] IGNORING TIMER DURING NESTED OPERATIONS ({1})", m_RoleId, iNestLevel));
                            }

                            Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] << NESTED OPERATIONS ({1})", m_RoleId, iNestLevel));

                            iNestLevel = Interlocked.Add(ref m_NestedOperations, -1);

                            Trace.WriteLine(String.Format("New Election Winner: RoleId [{0}] Final Nest Level ({1})", m_RoleId, iNestLevel));


                        }
                        else if ((queueMessage.ContentAsString().StartsWith(m_RoleId.ToString(), StringComparison.OrdinalIgnoreCase)) && (queueMessage.ContentAsString().EndsWith(String.Format("[{0}]", bool.FalseString), StringComparison.OrdinalIgnoreCase)))
                        {
                            //
                            // 
                            //
                            Trace.WriteLine(String.Format("Ignore Election Response From Self: RoleId [{0}]", m_RoleId));
                        }
                        else
                        {
                            //
                            // Timing Alignment Error between roles; update our clock to winners
                            //
                            Trace.WriteLine(String.Format("Aligning Notification Time Stamps to Election Master: RoleId [{0}]", queueMessage.ContentAsString()));

                            NotificationEngine.GetCurrentEngine().UpdateNotificationTimeStamps(true);

                            m_szLastElectionRequest = queueMessage.ContentAsString();

                        }
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OnElectionMasterTimer()");
        }



        private bool CheckElectionRequests()
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.CheckElectionRequests()");

            bool fElectionRequested = false;

            try
            {
                //
                // Check for any available election requests in the queue
                //
                Message queueMessage = this.m_mqElectionMaster.PeekMessage();

                //
                // If messages available, Check if this worker role is the election requestor.
                //
                if (queueMessage != null)
                {
                    //
                    // If the top message in the queue is this not this worker role's Id, then respond to election request.
                    //
                    if (!queueMessage.ContentAsString().StartsWith(m_RoleId.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        if (!queueMessage.ContentAsString().EndsWith(String.Format("[{0}]", bool.FalseString), StringComparison.OrdinalIgnoreCase))
                        {
                            if (queueMessage.ContentAsString() != m_szLastElectionRequest)
                            {
                                AzureLogging.WriteToLog("Verbose", String.Format("Found Election Request [{0}]", queueMessage.ContentAsString()));

                                fElectionRequested = true;

                                m_szLastElectionRequest = queueMessage.ContentAsString();
                            }
                        }
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.CheckElectionRequests()");

            return fElectionRequested;
        }

        private void OnMessageReceived(object sender, MessageReceivedEventArgs e)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.OnMessageReceived()");

            ShortMessage msg = null;

            try
            {
                Trace.WriteLine("Checking for available messages from queue.");

                //
                // Get new message
                //
                Message queueMessage = e.Message;

                //
                // If messages available, deserialze and return them
                //
                if (queueMessage != null)
                {
                    //
                    // Delete the message
                    //
                    m_mqInboundMessages.DeleteMessage(queueMessage);

                    //
                    // Deserialize back to short message
                    //
                    msg = m_smsSerializer.DeserializeFromString(queueMessage.ContentAsString());

                    if (msg != null)
                    {
                        int iMesssges = Interlocked.Add(ref m_MessagesRecv, 1);

                        Trace.WriteLine(String.Format("New Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));

                        //
                        // Process the message
                        //
                        ShortMessage msgResponse = ProcessMessage(msg);

                        if (msgResponse != null)
                        {
                            QueueMessage(m_mqOutboundMessages, msgResponse); 
                        }

                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OnMessageReceived()");
        }


        private void OnMessageSent(object sender, MessageReceivedEventArgs e)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.OnMessageSent()");

            ShortMessage msg = null;

            try
            {
                //
                // Get new message to send
                //
                Message queueMessage = e.Message;

                //
                // If messages available, deserialze and return them
                //
                if (queueMessage != null)
                {
                    bool fSent = false;

                    //
                    // Deserialize back to short message
                    //
                    msg = m_smsSerializer.DeserializeFromString(queueMessage.ContentAsString());

                    if (msg != null)
                    {
                        int iMesssges = Interlocked.Add(ref m_MessagesSent, 1);

                        //
                        // Check if the destination is on the block list
                        //
                        if (NotificationEngine.GetCurrentEngine().IsMobileNumberBlocked(msg.GetRawDestinationAddress()))
                        {
                            Trace.WriteLine(String.Format("WARNING: Dropping outgoing messaged for blocked mobile subscriber [{0}]", msg.GetRawDestinationAddress()));
                        }
                        else
                        {
                            //
                            // Send the message
                            //
                            Trace.WriteLine(String.Format("Sending Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
                            fSent = MobileWebGatewayClient.SendMessage(msg);
                        }
                    }

                    //
                    // Delete the message on success
                    //
                    if (fSent)
                    {
                        m_mqOutboundMessages.DeleteMessage(queueMessage);
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OnMessageSent()");
        }


        private ShortMessage GetNextAvailableMessage(MessageQueue azMessageQueue)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.GetNextAvailableMessage()");

            ShortMessage msg = null;

            try
            {
                Trace.WriteLine("Checking for available messages from queue.");

                //
                // Check for any available messages in the queue
                //
                Message queueMessage = azMessageQueue.GetMessage();

                //
                // If messages available, deserialze and return them
                //
                if (queueMessage != null)
                {
                    //
                    // Delete the message
                    //
                    azMessageQueue.DeleteMessage(queueMessage);

                    //
                    // Deserialize back to short message
                    //
                    msg = m_smsSerializer.DeserializeFromString(queueMessage.ContentAsString());

                    if (msg != null)
                    {
                        Trace.WriteLine(String.Format("New Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.GetNextAvailableMessage()");

            return msg;
        }


        private void m_mqNotificationCountry_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.m_mqNotificationCountry_MessageReceived()");

            try
            {
                //
                // Get new notification request
                //
                Message queueMessage = e.Message;

                //
                // If notification requests pending, retrieve and dispatch them.
                //
                if (queueMessage != null)
                {
                    //
                    // Delete the message from the queue 
                    //
                    m_mqNotificationCountry.DeleteMessage(queueMessage);

                    //
                    // Convert the notification request to a country code
                    //
                    string szCountryCode = queueMessage.ContentAsString();

                    if (!String.IsNullOrEmpty(szCountryCode))
                    {
                        //
                        // Process this country code's notifications
                        //
                        NotificationEngine.GetCurrentEngine().ProcessNotificationsForCountryCode(szCountryCode);
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OnMessageReceived()");
        }


        private bool QueueMessage(MessageQueue azMessageQueue, ShortMessage msg)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.QueueMessage()");

            bool fSuccess = false;

            try
            {
                //
                // Serialize text messages
                //
                string szMessageXml = m_smsSerializer.SerializeAsString(msg);

                if (!String.IsNullOrEmpty(szMessageXml))
                {
                    if (fSuccess = azMessageQueue.PutMessage(new Message(szMessageXml), TTL_OUTBOUND_MESSAGES))
                    {
                        AzureLogging.WriteToLog("Information", String.Format("Queued Sent Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
                    }
                }

                if (!fSuccess)
                {
                    AzureLogging.WriteToLog("Error", String.Format("Failed to Queue Sent Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
                throw we;
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
                throw se;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.QueueMessage()");

            return fSuccess;
        }


        public ShortMessage ProcessMessage(ShortMessage msg)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole.ProcessMessage()");

            ShortMessage msgResponse = null;

            try
            {
                Trace.WriteLine(String.Format("New Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));

                //
                // Resolve message locale for parsing
                //
                NotificationResourceMessages nrm = NotificationEngine.GetCurrentEngine().GetMessageLocale(msg);

                //
                // For now, create a new parser for each request based on locale
                //
                NotificationRequestParser nrp = new NotificationRequestParser(nrm);

                //
                // Run this message through the core notification proccessor
                //
                msgResponse = nrp.ProcessNewNotificationRequest(msg);


            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                msgResponse = ShortMessage.CreateShortMessage(msg.To, msg.From, "Sorry, an error occurred while processing your request");
            }

            if (msgResponse != null)
            {
                Trace.WriteLine(String.Format("Reply Message: Origination [{0}], Destination [{1}], Body [{2}]", msgResponse.From, msgResponse.To, msgResponse.Message));
                Trace.WriteLine(String.Format("Reply Message: Origination [{0}], Destination [{1}], Body [{2}]", msgResponse.From, msgResponse.To, msgResponse.Message));
            }
            else
            {
                Trace.WriteLine(String.Format("Reply Message: NULL - No Message Returned"));
                Trace.WriteLine(String.Format("Reply Message: NULL - No Message Returned"));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole.ProcessMessage()");

            return msgResponse;
        }


        private MessageNotificationLocation ResolveLocation(string szLocation, string szCulture)
        {
            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.ResolveLocation()");

            MessageNotificationLocation location = null;

            //
            // Update Bing Token if necessary
            //
            bool fExpired = UpdateBingToken(String.IsNullOrEmpty(m_szBingToken));

            try
            {
                if (!fExpired)
                {
                    //
                    // Lookup location via Bing
                    //
                    GeocodeResponse geocodeResponse = BingMapServicesHelper.CallBingMapServices(m_szBingToken, szCulture, szLocation);

                    if (geocodeResponse.Results.Count() <= 0)
                    {
                        Trace.WriteLine(String.Format("ERROR: {0}", "Location Not Found"));
                    }
                    else
                    {
                        Trace.WriteLine(String.Format("INFORMATION: {0}", String.Format("Resolved Location [{0}] ==> Name [{1}], Lat [{2}], Long [{3}]", szLocation, geocodeResponse.Results[0].DisplayName, geocodeResponse.Results[0].Locations[0].Latitude, geocodeResponse.Results[0].Locations[0].Longitude)));

                        location = new MessageNotificationLocation();
                        location.Name = geocodeResponse.Results[0].DisplayName;
                        location.Description = String.Format("<LOCATION><NAME>{0}</NAME><LATITUDE>{1}</LATITUDE><LONGITUDE>{2}</LONGITUDE></LOCATION>", geocodeResponse.Results[0].DisplayName, geocodeResponse.Results[0].Locations[0].Latitude, geocodeResponse.Results[0].Locations[0].Longitude);
                        location.Latitude = geocodeResponse.Results[0].Locations[0].Latitude;
                        location.Longitude = geocodeResponse.Results[0].Locations[0].Longitude;
                    }
                }
            }
            catch (System.ServiceModel.FaultException<dev.virtualearth.net.webservices.v1.common.ResponseSummary>)
            {
                //
                // Force Update of Bing Token
                //
                fExpired = UpdateBingToken(true);
            }

            if (fExpired)
            {
                try
                {
                    //
                    // Retry lookup location via Bing
                    //
                    GeocodeResponse geocodeResponse = BingMapServicesHelper.CallBingMapServices(m_szBingToken, szCulture, szLocation);

                    if (geocodeResponse.Results.Count() <= 0)
                    {
                        Trace.WriteLine(String.Format("ERROR: {0}", "Location Not Found"));
                    }
                    else
                    {
                        Trace.WriteLine(String.Format("INFORMATION: {0}", String.Format("Resolved Location [{0}] ==> Name [{1}], Lat [{2}], Long [{3}]", szLocation, geocodeResponse.Results[0].DisplayName, geocodeResponse.Results[0].Locations[0].Latitude, geocodeResponse.Results[0].Locations[0].Longitude)));

                        location = new MessageNotificationLocation();
                        location.Name = geocodeResponse.Results[0].DisplayName;
                        location.Description = String.Format("<LOCATION><NAME>{0}</NAME><LATITUDE>{1}</LATITUDE><LONGITUDE>{2}</LONGITUDE></LOCATION>", geocodeResponse.Results[0].DisplayName, geocodeResponse.Results[0].Locations[0].Latitude, geocodeResponse.Results[0].Locations[0].Longitude);
                        location.Latitude = geocodeResponse.Results[0].Locations[0].Latitude;
                        location.Longitude = geocodeResponse.Results[0].Locations[0].Longitude;
                    }
                }
                catch (System.ServiceModel.FaultException<dev.virtualearth.net.webservices.v1.common.ResponseSummary>)
                {
                    Trace.WriteLine(String.Format("ERROR: {0}", String.Format("Failed 2nd attempt to resolve location [{0}] with updated Bing Token", szLocation)));
                }
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.ResolveLocation()");

            return location;
        }


        private bool UpdateBingToken(bool fInvalid)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.UpdateBingToken()");

            try
            {
                if (fInvalid)
                {
                    m_szBingToken = null;
                }

                if (String.IsNullOrEmpty(m_szBingToken))
                {
                    m_szBingToken = BingMapServicesHelper.GetBingToken("127.0.0.1");

                    if (!String.IsNullOrEmpty(m_szBingToken))
                    {
                        AzureLogging.WriteToLog("Information", String.Format("Updated Bing Token Token [{0}] at [{1}]", m_szBingToken, DateTime.UtcNow.ToString()));
                    }
                    else
                    {
                        AzureLogging.WriteToLog("Error", String.Format("Unable to update Bing Token Token at [{0}]", DateTime.UtcNow.ToString()));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                m_szBingToken = null;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.UpdateBingToken()");

            return fInvalid;
        }


        /// <summary>
        /// Gets the measurement.
        /// </summary>
        /// <param name="longitude">The longitude.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="component">The component.</param>
        /// <returns>The measurement value for a component</returns>
        private decimal? GetMeasurement(decimal longitude, decimal latitude, string component)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.GetMeasurement()");

            decimal? measureToReturn = null;

            try
            {
                switch (component)
                {
                    case MEASUREMENT_AIR_QUALITY:
                        {
                            //
                            // If the data requested is air, then access the closest center Air Station
                            //
                            CentreLocation centreLocation = ServiceProvider.CenterLocationService.GetCentreLocationForPoint(longitude, latitude);

                            if (centreLocation != null)
                            {
                                measureToReturn = (decimal?)centreLocation.QualityValue;
                            }
                        }
                        break;

                    case MEASUREMENT_WATER_QUALITY:
                        {
                            //
                            // Get closest water station
                            //
                            Station s = ServiceProvider.StationService.GetClosestStation((double)longitude, (double)latitude, "Water");

                            if (s != null)
                            {
                                measureToReturn = s.QualityIndex;
                            }
                        }
                        break;

                    default:
                        {
                            //
                            // Otherwise access the component requested
                            //
                            var query = from Measurement measurement in ServiceProvider.CenterLocationService.GetIndexForPoint(longitude, latitude, component) select measurement;

                            Measurement measure = query.FirstOrDefault();

                            measureToReturn = measure == null ? null : measure.Value;
                        }
                        break;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            if (measureToReturn != null)
            {
                AzureLogging.WriteToLog("Information", String.Format("Found Measurement Type [{0}], value [{1}] for location Lat [{2}], Long [{3}]", component, measureToReturn, latitude, longitude));
                Trace.WriteLine(String.Format("Found Measurement Type [{0}], value [{1}] for location Lat [{2}], Long [{3}]", component, measureToReturn, latitude, longitude));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.GetMeasurement()");

            return measureToReturn;
        }

        
        private MessageNotificationLocation GetAirQualityIndex(decimal longitude, decimal latitude)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.GetAirQualityIndex()");

            MessageNotificationLocation measureToReturn = null;

            try
            {
                //
                // If the data requested is air, then access the closest Air Index
                //
                CentreLocation centreLocation = ServiceProvider.CenterLocationService.GetCentreLocationForPoint(longitude, latitude);

                if ((centreLocation != null) && (centreLocation.QualityIndex != null))
                {
                    measureToReturn = new MessageNotificationLocation(MEASUREMENT_AIR_QUALITY, string.Empty, (double)latitude, (double)longitude, (double)0.0, 0, null);

                    measureToReturn.QualityIndex = (int)centreLocation.QualityIndex;

                    measureToReturn.LastModified = (centreLocation.Timestamp != null) ? (DateTime)centreLocation.Timestamp : DateTime.UtcNow;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            if (measureToReturn != null)
            {
                AzureLogging.WriteToLog("Information", String.Format("Found Air Quality Index, value [{0}] for location Lat [{1}], Long [{2}]", measureToReturn.QualityIndex, latitude, longitude));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.GetAirQualityIndex()");

            return measureToReturn;
        }


        private MessageNotificationLocation GetWaterQualityIndex(decimal longitude, decimal latitude)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.GetWaterQualityIndex()");

            MessageNotificationLocation measureToReturn = null;

            try
            {
                //
                // Get closest water station
                //
                Station s = ServiceProvider.StationService.GetClosestStation((double)longitude, (double)latitude, "Water");

                if ((s != null) && (s.QualityIndex != null))
                {
                    measureToReturn = new MessageNotificationLocation(MEASUREMENT_WATER_QUALITY, string.Empty, (double)latitude, (double)longitude, (double)0.0, 0, null);

                    measureToReturn.QualityIndex = (int)s.QualityIndex;

                    measureToReturn.Name = s.Name;

                    measureToReturn.LastModified = (s.Timestamp != null) ? (DateTime)s.Timestamp : DateTime.UtcNow;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            if (measureToReturn != null)
            {
                AzureLogging.WriteToLog("Information", String.Format("Found Water Quality Index, value [{0}] for location Lat [{1}], Long [{2}]", measureToReturn.QualityIndex, latitude, longitude));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.GetWaterQualityIndex()");

            return measureToReturn;
        }


        private MessageNotificationLocation WorkerRole_LocationResolverHandler(string szLocation, string szCulture)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole_LocationResolverHandler()");

            MessageNotificationLocation location = null;

            try
            {
                location = ResolveLocation(szLocation, szCulture);
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole_LocationResolverHandler()");

            return location;
        }


        private decimal? WorkerRole_RetrieveMeasurementDataHandler(decimal longitude, decimal latitude, string szMeasurementType)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole_RetrieveMeasurementDataHandler()");

            decimal? dMeasurementValue = null;

            try
            {
                dMeasurementValue = GetMeasurement(longitude, latitude, szMeasurementType);
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole_RetrieveMeasurementDataHandler()");

            return dMeasurementValue;
        }


        private MessageNotificationLocation WorkerRole_RetrieveAirQualityIndexHandler(decimal longitude, decimal latitude)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole_RetrieveAirQualityIndexHandler()");

            MessageNotificationLocation measureToReturn = null;

            try
            {
                measureToReturn = GetAirQualityIndex(longitude, latitude);
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole_RetrieveAirQualityIndexHandler()");

            return measureToReturn;
        }


        private MessageNotificationLocation WorkerRole_RetrieveWaterQualityIndexHandler(decimal longitude, decimal latitude)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole_RetrieveWaterQualityIndexHandler()");

            MessageNotificationLocation measureToReturn = null;

            try
            {
                measureToReturn = GetWaterQualityIndex(longitude, latitude);
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole_RetrieveWaterQualityIndexHandler()");

            return measureToReturn;
        }


        private bool WorkerRole_SendMessageHandler(ShortMessage msg)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.WorkerRole_SendMessageHandler()");

            bool fSent = false;

            try
            {
                QueueMessage(m_mqOutboundMessages, msg); 

                fSent = true;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.WorkerRole_SendMessageHandler()");

            return fSent;
        }

        public bool QueueNotificationsForCountryCode(string szCountryCode)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.QueueNotificationsForCountryCode()");

            bool fSuccess = false;

            try
            {
                //
                // Queue country code for processing
                //
                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    if (fSuccess = this.m_mqNotificationCountry.PutMessage(new Message(szCountryCode), TTL_NOTIFICATION_COUNTRY_CODE))
                    {
                        AzureLogging.WriteToLog("Information", String.Format("Queued Notification Work Item: Country Code [{0}]", szCountryCode));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            if (!fSuccess)
            {
                AzureLogging.WriteToLog("Error", String.Format("Failed to Queue Notification Work Item: Country Code [{0}]", szCountryCode));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.QueueNotificationsForCountryCode()");

            return fSuccess;
        }
 
    
    
        private enum WorkerRoleState:int
        {
            Uninitialized = 0,
            Starting = 1,
            Initialized = 2,
            Running = 3,
            Stopping = 4,
            Stopped = 5
        };
    }
}
