﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Diagnostics;

using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.Azure.WorkerRole.Utilities;


using System.Mobile.Messaging;
using System.Mobile.Messaging.Web;
using Microsoft.Mobile.Messaging.Utils;
using Microsoft.AirWatch.Notifications;

namespace WebRole1
{
    /// <summary>
    /// Summary description for TextMessageService
    /// </summary>
    [WebService(Namespace = "http://schemas.microsoft.com/ws/2004/05/mws/sms")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class TextMessageService : System.Web.Services.WebService
    {
        public TextMessageService()
        {
            Trace.WriteLine(">> WebRole1.TextMessageService()");
            Trace.WriteLine("<< WebRole1.TextMessageService()");
        }

        [WebMethod]
        public void ProcessMessage(ShortMessage msg)
        {
            Trace.WriteLine(">> WebRole1.TextMessageService.ProcessMessage()");
            ProcessMessageWithResponse(msg);
            Trace.WriteLine("<< WebRole1.TextMessageService.ProcessMessage()");
        }

        [WebMethod]
        public ShortMessage ProcessMessageWithResponse(ShortMessage msg)
        {
            Trace.WriteLine(">> WebRole1.TextMessageService.ProcessMessageWithResponse()");

            ShortMessage msgResponse = null;

            #region Handle Notification Request
            try
            {
                Trace.WriteLine(String.Format("New Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));

                //
                // This will enable the application to receive messages from the Mobile Web Gateway
                //
#if ENABLE_MWG_SUPPORT
                if (!msg.GetRawSourceAddress().Equals("447786201106"))
                {
                    //
                    // Queue message for worker role
                    //
                    QueueMessage(msg);
                }
#endif
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                msgResponse = ShortMessage.CreateShortMessage(msg.To, msg.From, "[Web] - Sorry, your request could not be understood");
            }
            #endregion

            if (msgResponse != null)
            {
                Trace.WriteLine(String.Format("Reply Message: Origination [{0}], Destination [{1}], Body [{2}]", msgResponse.From, msgResponse.To, msgResponse.Message));
            }
            else
            {
                Trace.WriteLine(String.Format("Reply Message: NULL - No Message Returned"));
            }

            Trace.WriteLine("<< WebRole1.TextMessageService.ProcessMessageWithResponse()");

            return msgResponse;
        }


        #region Web Testing
        [WebMethod]
        public string TestNotificationServiceRemote(string szFrom, string szMessage)
        {
            Trace.WriteLine(">> WebRole1.TextMessageService.TestNotificationServiceRemote()");

            ShortMessage msg = ShortMessage.CreateShortMessage(szFrom, "+447624802848", szMessage);

            ShortMessage msgResponse = this.ProcessMessageWithResponse(msg);

            Trace.WriteLine("<< WebRole1.TextMessageService.TestNotificationServiceRemote()");

            return (msgResponse != null) ? msgResponse.ToString() : null;
        }
        #endregion


        #region AZURE QUEUE SUPPORT

        public void QueueMessage(ShortMessage msg)
        {
            AzureLogging.WriteToLog("Verbose", ">> WebRole1.TextMessageService.QueueMessage()");

            AzureLogging.WriteToLog("Information", String.Format("Azure Web Role Queueing Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));

            //
            // Open queue for handling inbound messages
            //
            string szQueueName = "queue-inbound-messages";
            bool fExists = false;
            bool fResult = false;
            bool fSuccess = false;

            StorageAccountInfo account = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();

            QueueStorage queueService = QueueStorage.Create(account);
            queueService.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));

            //
            // Create or Open
            //
            MessageQueue q = queueService.GetQueue(szQueueName);
            fResult = q.CreateQueue(out fExists);
            if (!fExists && fResult)
            {
                AzureLogging.WriteToLog("Information", String.Format("Queue [{0}] successfully created.", szQueueName));
            }

            //
            // Serialize text messages
            //
            ShortMessageSerializer smsSerializer = new ShortMessageSerializer();

            string szMessageXml = smsSerializer.SerializeAsString(msg);

            if (!String.IsNullOrEmpty(szMessageXml))
            {
                if (fSuccess = q.PutMessage(new Message(szMessageXml)))
                {
                    AzureLogging.WriteToLog("Information", String.Format("Message with hash [{0}] successfully put into queue", szMessageXml.GetHashCode()));
                }
            }

            if (!fSuccess)
            {
                AzureLogging.WriteToLog("Error", String.Format("Failed to Queue Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
            }

            AzureLogging.WriteToLog("Verbose", "<< WebRole1.TextMessageService.QueueMessage()");
        }
        #endregion
    }
}
