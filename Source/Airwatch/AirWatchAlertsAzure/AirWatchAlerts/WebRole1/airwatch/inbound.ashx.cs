﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Diagnostics;

using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.Azure.WorkerRole.Utilities;


using System.Mobile.Messaging;
using System.Mobile.Messaging.Web;
using Microsoft.AirWatch.Notifications;

using Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml.Responses;

namespace WebRole1.airwatch
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class inbound : IHttpHandler
    {
        public inbound()
        {
            Trace.WriteLine(">> inbound()");

            Trace.WriteLine("<< inbound()");
        }

        #region IHttpHandler

        public void ProcessRequest(HttpContext context)
        {
            Trace.WriteLine(">> MessageHandler.ProcessRequest()");

            switch (context.Request.RequestType)
            {
                case "POST":
                    HandleMessage(context);
                    break;
            }

            Trace.WriteLine("<< MessageHandler.ProcessRequest()");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Private methods

        private void HandleMessage(HttpContext context)
        {
            Trace.WriteLine(">> MessageHandler.HandleMessage()");

            try
            {
                string szEncodedRequest = context.Request.Form["XMLDATA"];

#if false || debugging
                foreach (string szKey in context.Request.Form.AllKeys)
                {
                    MobileWebGatewayClient.SendMessage(ShortMessage.CreateShortMessage("14254459010", String.Format("[{0}]=[{1}]", szKey, context.Request.Form[szKey].Substring(0, 145))));
                }
#endif


                if (!String.IsNullOrEmpty(szEncodedRequest))
                {
                    Trace.WriteLine(String.Format("Incoming encoded message Data = {0}", szEncodedRequest));

                    string szDecodedRequest = System.Web.HttpUtility.UrlDecode(szEncodedRequest);

                    Trace.WriteLine(String.Format("Incoming decoded message Data = {0}", szDecodedRequest));

                    mbloxResponseServiceSerializer ms = new mbloxResponseServiceSerializer();

                    ResponseService rs = ms.DeserializeFromString(szDecodedRequest);

                    //
                    // Process real messages
                    //
                    ProcessIncomingMessage(rs);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }



            Trace.WriteLine("<< MessageHandler.HandleMessage()");
        }

        private void ProcessIncomingMessage(ResponseService rs)
        {
            Trace.WriteLine(">> MessageHandler.ProcessIncomingMessage()");

            try
            {
                if (rs != null)
                {
                    string szUsername = "EEA";
                    string szPassword = "cG9wb09r";

                    Debug.WriteLine(String.Format("Partner [{0}]", rs.Header.Partner.Text[0]));
                    Debug.WriteLine(String.Format("ServiceID [{0}]", rs.Header.ServiceID.Text[0]));
                    Debug.WriteLine(String.Format("Password [{0}]", rs.Header.Password.Text[0]));

                    if ((szUsername != rs.Header.Partner.Text[0]) && (szPassword != rs.Header.Password.Text[0]))
                    {
                        Debug.WriteLine(String.Format("Unauthorized Request"));
                    }

                    Debug.WriteLine(String.Format("rs.ResponseList[0].Destination.Text[0] = [{0}]", rs.ResponseList[0].Destination.Text[0]));
                    Debug.WriteLine(String.Format("rs.ResponseList[0].OriginatingNumber.Text[0] = [{0}]", rs.ResponseList[0].OriginatingNumber.Text[0]));
                    Debug.WriteLine(String.Format("rs.ResponseList[0].Data.Text[0] = [{0}]", rs.ResponseList[0].Data.Text[0]));

                    ShortMessage msg = ShortMessage.CreateShortMessage(rs.ResponseList[0].OriginatingNumber.Text[0], rs.ResponseList[0].Destination.Text[0], rs.ResponseList[0].Data.Text[0]);


                    #region Handle Notification Request
                    try
                    {
                        Trace.WriteLine(String.Format("New Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));

                        //
                        // Queue message for worker role
                        //
                        QueueMessage(msg);
                    }
                    catch (System.Exception ex)
                    {
                        Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                        Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
            Trace.WriteLine("<< MessageHandler.ProcessIncomingMessage()");
        }

        #endregion


        #region AZURE QUEUE SUPPORT

        public void QueueMessage(ShortMessage msg)
        {
            AzureLogging.WriteToLog("Verbose", ">> WebRole1.TextMessageService.QueueMessage()");

            AzureLogging.WriteToLog("Information", String.Format("Azure Web Role Queueing Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));

            //
            // Open queue for handling inbound messages
            //
            string szQueueName = "queue-inbound-messages";
            bool fExists = false;
            bool fResult = false;
            bool fSuccess = false;

            StorageAccountInfo account = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();

            QueueStorage queueService = QueueStorage.Create(account);
            queueService.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));

            //
            // Create or Open
            //
            MessageQueue q = queueService.GetQueue(szQueueName);
            fResult = q.CreateQueue(out fExists);
            if (!fExists && fResult)
            {
                AzureLogging.WriteToLog("Information", String.Format("Queue [{0}] successfully created.", szQueueName));
            }

            //
            // Serialize text messages
            //
            ShortMessageSerializer smsSerializer = new ShortMessageSerializer();

            string szMessageXml = smsSerializer.SerializeAsString(msg);

            if (!String.IsNullOrEmpty(szMessageXml))
            {
                if (fSuccess = q.PutMessage(new Message(szMessageXml)))
                {
                    AzureLogging.WriteToLog("Information", String.Format("Message with hash [{0}] successfully put into queue", szMessageXml.GetHashCode()));
                }
            }

            if (!fSuccess)
            {
                AzureLogging.WriteToLog("Error", String.Format("Failed to Queue Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
            }

            AzureLogging.WriteToLog("Verbose", "<< WebRole1.TextMessageService.QueueMessage()");
        }
        #endregion

    }
}
