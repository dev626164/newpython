﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System.Data.Services.Client;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// BlockedNumber table
    /// </summary>
    public class BlockedNumberEntityTable : TableStorageDataServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlockedNumberEntityTable"/> class.
        /// </summary>
        public BlockedNumberEntityTable()
            : base(StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration())
        {
        }

        public BlockedNumberEntityTable(StorageAccountInfo accountInfo)
            : base(accountInfo)
        {
        }

        /// <summary>
        /// Gets the BlockedNumbers.
        /// </summary>
        /// <value>The BlockedNumbers.</value>
        public DataServiceQuery<BlockedNumberEntity> BlockedNumbers
        {
            get
            {
                return CreateQuery<BlockedNumberEntity>("BlockedNumbers");
            }
        }
    }
}
