﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System;
    using System.Globalization;
    using Microsoft.Samples.ServiceHosting.StorageClient;
    
    /// <summary>
    /// Notification table entity
    /// </summary>
    public class NotificationEntity : TableStorageEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationEntity"/> class.
        /// </summary>
        public NotificationEntity(string countryCode, string mobileNumber, string type, string locale, string name, string longitude, string latitude, string lastMeasurement, string lastIndex)
        {
            PartitionKey = countryCode;
            RowKey = mobileNumber;
            this.NotificationType = type;
            this.CountryCode = countryCode;
            this.MobileNumber = mobileNumber;
            this.Locale = locale;
            this.LocationName = name;
            this.Longitude = longitude;
            this.Latitude = latitude;
            this.LastIndex = lastIndex;
            this.LastMeasurement = lastMeasurement;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationEntity"/> class.
        /// </summary>
        public NotificationEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the mobile number string.
        /// </summary>
        /// <value>The mobile number.</value>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the notification type.
        /// </summary>
        /// <value>The notification type.</value>
        public string NotificationType { get; set; }

        /// <summary>
        /// Gets or sets the locale.
        /// </summary>
        /// <value>The locale.</value>
        public string Locale { get; set; }

        /// <summary>
        /// Gets or sets the LocationName.
        /// </summary>
        /// <value>The notification location name.</value>
        public string LocationName { get; set; }

        /// <summary>
        /// Gets or sets the Latitude.
        /// </summary>
        /// <value>The notification Latitude.</value>
        public string Latitude { get; set; }

        /// <summary>
        /// Gets or sets the Longitude.
        /// </summary>
        /// <value>The notification Longitude.</value>
        public string Longitude { get; set; }

        /// <summary>
        /// Gets or sets the last index value.
        /// </summary>
        /// <value>The last index value.</value>
        public string LastIndex { get; set; }

        /// <summary>
        /// Gets or sets the last measurement value.
        /// </summary>
        /// <value>The last measurement value.</value>
        public string LastMeasurement { get; set; }
    }
}
