﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Linq;
using System.Net;
using System.Text;
using System.Data.Services.Client;
using System.Data.Services.Common;
using Microsoft.ServiceHosting.ServiceRuntime;

using Microsoft.AirWatch.Common;
using Microsoft.Azure.WorkerRole.Utilities;
using Microsoft.Samples.ServiceHosting.StorageClient;


namespace Microsoft.AirWatch.Notifications.Storage
{
    public class StorageManager
    {
        private const string DB_TABLE_NAME_PERSONS = "Persons";
        private const string DB_TABLE_NAME_NOTIFICATIONS = "Notifications";
        private const string DB_TABLE_NAME_BLOCKED_NUMBERS = "BlockedNumbers";
        private const string DB_TABLE_NAME_RESOURCES = "ResourceManagerEntities";
        private const string MEASUREMENT_AIR_QUALITY = "CAQI";
        private const string MEASUREMENT_WATER_QUALITY = "CWQI";

        public StorageManager()
        {
        }

        private PersonEntityTable m_PersonsTable;
#if OLD_SCHOOL
        private NotificationEntityTable m_NotificationsTable;
#endif
        private BlockedNumberEntityTable m_BlockedNumberTable;
        private ResourceManagerEntityTable m_ResourceManagerTable;

        public void Initialize()
        {
            Initialize(true);
        }

        public void Initialize(bool fEnableEvents)
        {
            Trace.WriteLine(">> AirWatch.Notifications.Storage.StorageManager.Initialize()");

            try
            {
                //
                // Log start
                //
                AzureLogging.WriteToLog("Information", "AirWatch Notification Storage Manager entry point called");

                if (fEnableEvents)
                {
                    //
                    // Create our resource manager handler & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().LoadResourcesForLocaleHandler += new LoadResourcesForLocale(StorageManager_LoadResourcesForLocaleHandler);

                    //
                    // Create our block user handler & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().AddBlockNumberHandler += new AddBlockedNumber(StorageManager_BlockNumberHandler);

                    //
                    // Create our block user handler & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().CheckBlockNumberHandler += new CheckBlockedNumber(StorageManager_CheckBlockNumberHandler);

                    //
                    // Create our registration loader & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().LoadRegistrationHandler += new LoadRegistration(StorageManager_LoadRegistrationHandler);

                    //
                    // Create our registration saver & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().SaveRegistrationHandler += new SaveRegistration(StorageManager_SaveRegistrationHandler);

                    //
                    // Create our delete registration handler & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().DeleteRegistrationHandler += new DeleteRegistration(StorageManager_DeleteRegistrationHandler);

                    //
                    // Create our notification loader & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().LoadNotificationsHandler += new LoadNotifications(StorageManager_LoadNotificationsHandler);

                    //
                    // Create our notification saver & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().SaveNotificationHandler += new SaveNotification(StorageManager_SaveNotificationHandler);

                    //
                    // Create our clear all notification handler & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().ClearAllNotificationsHandler += new ClearAllNotifications(StorageManager_ClearAllNotificationsHandler);

                    //
                    // Create our query notifications handler & add it to the notification engine
                    //
                    NotificationEngine.GetCurrentEngine().QueryNotificationsHandler += new QueryNotifications(QueryActiveNotifications);

                }

                //
                // If in production, attempt to create database tables
                //
                bool fCreateTables = true;

                try
                {
                    Boolean.TryParse(ApplicationEnvironment.Settings["production"], out fCreateTables);
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }

                AzureLogging.WriteToLog("Information", String.Format("AirWatch Notification Worker Role Create Tables Setting [{0}]", fCreateTables.ToString()));

                //
                // Open Resource Entity Table
                //
                m_ResourceManagerTable = OpenDatabaseTable(DB_TABLE_NAME_RESOURCES, fCreateTables, false) as ResourceManagerEntityTable;

                //
                // Open Persons Table
                //
                m_PersonsTable = OpenDatabaseTable(DB_TABLE_NAME_PERSONS, fCreateTables, false) as PersonEntityTable;

#if true || OLD_SCHOOL
                //
                // Open Notifications Table
                //
                NotificationEntityTable m_NotificationsTable = OpenDatabaseTable(DB_TABLE_NAME_NOTIFICATIONS, fCreateTables, false) as NotificationEntityTable;
#endif

                //
                // Open Blocked Number Table
                //
                m_BlockedNumberTable = OpenDatabaseTable(DB_TABLE_NAME_BLOCKED_NUMBERS, fCreateTables, false) as BlockedNumberEntityTable;

            }
            catch (System.Threading.ThreadAbortException taex)
            {
                Trace.WriteLine(String.Format("THREAD ABORT: {0}", taex.Message));
                Trace.WriteLine(String.Format("THREAD ABORT DUMP:\n{0}", taex));
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.Storage.StorageManager.Initialize()");
        }



        public MessageQueue OpenMessageQueue(string szQueueName, bool fClearQueue)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.OpenMessageQueue()");

            MessageQueue q = null;

            try
            {
                //
                // Open queue for handling inbound messages
                //
                bool fExists = false;
                bool fResult = false;

                StorageAccountInfo account = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();

                QueueStorage queueService = QueueStorage.Create(account);
                queueService.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));

                //
                // Create or Open
                //
                q = queueService.GetQueue(szQueueName);
                fResult = q.CreateQueue(out fExists);
                if (!fExists && fResult)
                {
                    AzureLogging.WriteToLog("Information", String.Format("Queue [{0}] successfully created.", szQueueName));
                }

                //
                // Clear the queue on startup if requested
                //
                if (fClearQueue)
                {
                    q.Clear();
                }

                //
                // List available queues for diagnostic purposes
                //
                AzureLogging.WriteToLog("Information", String.Format("Listing Available Queues for account"));
                IEnumerable<MessageQueue> queues = queueService.ListQueues();
                foreach (MessageQueue qu in queues)
                {
                    AzureLogging.WriteToLog("Information", String.Format("Found Available Queue [{0}]", qu.Name));
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OpenMessageQueue()");

            return q;
        }


        public void DeleteMessageQueues(string szQueueName, bool fAllQueues)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.DeleteMessageQueues()");

            try
            {
                //
                // Delete specified queues
                //
                StorageAccountInfo account = StorageAccountInfo.GetDefaultQueueStorageAccountFromConfiguration();

                QueueStorage queueService = QueueStorage.Create(account);
                queueService.RetryPolicy = RetryPolicies.RetryN(2, TimeSpan.FromMilliseconds(100));

                //
                // Delete Queue(s)
                //
                IEnumerable<MessageQueue> queuesToDelete = queueService.ListQueues();
                foreach (MessageQueue qu in queuesToDelete)
                {
                    if (fAllQueues || (szQueueName == qu.Name))
                    {
                        qu.Clear();
                        qu.DeleteQueue();
                        AzureLogging.WriteToLog("Information", String.Format("Deleting Queue [{0}]", qu.Name));
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                throw ex;
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.DeleteMessageQueues()");
        }


        private TableStorageDataServiceContext OpenDatabaseTable(string szTableName)
        {
            return OpenDatabaseTable(szTableName, false, false);
        }

        private TableStorageDataServiceContext OpenDatabaseTable(string szTableName, bool fCreateTable, bool fPurgeTable)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.OpenDatabaseTable()");

            TableStorageDataServiceContext tsdsc = null;

            try
            {
                StorageAccountInfo account = StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration();

                TableManager<PersonEntityTable, PersonEntity> tmPersons = null;
                TableManager<NotificationEntityTable, NotificationEntity> tmNotifications = null;
                TableManager<BlockedNumberEntityTable, BlockedNumberEntity> tmBlockedNumbers = null;
                TableManager<ResourceManagerEntityTable, ResourceManagerEntity> tmResourceManager = null;

                switch (szTableName)
                {
                    case DB_TABLE_NAME_PERSONS:
                        tsdsc = new PersonEntityTable(account);
                        tmPersons = new TableManager<PersonEntityTable, PersonEntity>(account);
                        break;
                    case DB_TABLE_NAME_NOTIFICATIONS:
                        tsdsc = new NotificationEntityTable(account);
                        tmNotifications = new TableManager<NotificationEntityTable, NotificationEntity>(account);
                        break;
                    case DB_TABLE_NAME_BLOCKED_NUMBERS:
                        tsdsc = new BlockedNumberEntityTable(account);
                        tmBlockedNumbers = new TableManager<BlockedNumberEntityTable, BlockedNumberEntity>(account);
                        break;
                    case DB_TABLE_NAME_RESOURCES:
                        tsdsc = new ResourceManagerEntityTable(account);
                        tmResourceManager = new TableManager<ResourceManagerEntityTable, ResourceManagerEntity>(account);
                        break;
                }

                //
                // Retry policy
                //
                tsdsc.RetryPolicy = RetryPolicies.RetryN(3, TimeSpan.FromSeconds(1));

                //
                // Create tables
                //
                if (fCreateTable)
                {
                    switch (szTableName)
                    {
                        case DB_TABLE_NAME_PERSONS:
                            tmPersons.CreateStorageTable();
                            break;
                        case DB_TABLE_NAME_NOTIFICATIONS:
                            tmNotifications.CreateStorageTable();
                            break;
                        case DB_TABLE_NAME_BLOCKED_NUMBERS:
                            tmBlockedNumbers.CreateStorageTable();
                            break;
                        case DB_TABLE_NAME_RESOURCES:
                            tmResourceManager.CreateStorageTable();
                            break;
                    }
                }

                //
                // Purge tables
                //
                if (fPurgeTable)
                {
                    switch (szTableName)
                    {
                        case DB_TABLE_NAME_PERSONS:
                            tmPersons.DeleteAllTableEntries(tsdsc as PersonEntityTable, szTableName);
                            break;
                        case DB_TABLE_NAME_NOTIFICATIONS:
                            tmNotifications.DeleteAllTableEntries(tsdsc as NotificationEntityTable, szTableName);
                            break;
                        case DB_TABLE_NAME_BLOCKED_NUMBERS:
                            tmBlockedNumbers.DeleteAllTableEntries(tsdsc as BlockedNumberEntityTable, szTableName);
                            break;
                        case DB_TABLE_NAME_RESOURCES:
                            tmResourceManager.DeleteAllTableEntries(tsdsc as ResourceManagerEntityTable, szTableName);
                            break;
                    }
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.OpenDatabaseTable()");

            return tsdsc;
        }

        public IResourceManager StorageManager_LoadResourcesForLocaleHandler(string szCulture)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_LoadResourcesForLocaleHandler()");

            IResourceManager irm = null;

            try
            {
                List<ResourceManagerEntity> resourceEntities = null;

                //
                // Query for all resource manager entities for this culture
                //
                var qResult = from re in this.m_ResourceManagerTable.ResourceManagerEntities where re.PartitionKey == szCulture select re;

                TableStorageDataServiceQuery<ResourceManagerEntity> q = new TableStorageDataServiceQuery<ResourceManagerEntity>(qResult as DataServiceQuery<ResourceManagerEntity>, m_ResourceManagerTable.RetryPolicy);

                try
                {
                    //
                    // the query references the whole key and explicitly addresses one entity
                    // thus, this query can generate an exception if there are 0 results during enumeration
                    //
                    IEnumerable<ResourceManagerEntity> availableResources = q.ExecuteAllWithRetries();

                    //
                    // Create list for further processing by notification engine
                    //
                    resourceEntities = new List<ResourceManagerEntity>();

                    foreach (ResourceManagerEntity re in availableResources)
                    {
                        //
                        // Add resources to list
                        //
                        resourceEntities.Add(re);
                    }

                    if ((resourceEntities != null) && (resourceEntities.Count > 0))
                    {
                        AzureLogging.WriteToLog("Information", String.Format("Found [{0}] resources for culture [{1}] at [{2}]", resourceEntities.Count, szCulture, DateTime.UtcNow.ToString()));

                        //
                        // Create Azure resource manager from found resource entities
                        //
                        irm = new AzureResourceManager(szCulture, resourceEntities);
                    }
                    else
                    {
                        resourceEntities = null;
                    }
                }
                catch (DataServiceQueryException e)
                {
                    HttpStatusCode s;

                    if (TableStorageHelpers.EvaluateException(e, out s) && s == HttpStatusCode.NotFound)
                    {
                        //
                        // No resource entities found
                        //
                        AzureLogging.WriteToLog("Warning", String.Format("No resources found for culture [{0}] at [{1}]", szCulture, DateTime.UtcNow.ToString()));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_LoadResourcesForLocaleHandler()");

            return irm;
        }


        public bool StorageManager_SaveResourcesHandler(List<ResourceManagerEntity> resourceEntities)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_SaveResourcesHandler()");

            bool fSaved = false;

            try
            {
                //
                // Open Resources Table
                //
                ResourceManagerEntityTable tableResourceManager = OpenDatabaseTable(DB_TABLE_NAME_RESOURCES) as ResourceManagerEntityTable;

                foreach (ResourceManagerEntity resourceManagerEntity in resourceEntities)
                {

                    try
                    {
                        ResourceManagerEntity rmeExisting = (from rme in tableResourceManager.ResourceManagerEntities where ((rme.PartitionKey == resourceManagerEntity.PartitionKey) && (rme.RowKey == resourceManagerEntity.RowKey)) select rme).FirstOrDefault();

                        if ((rmeExisting != null) && (rmeExisting.RowKey == resourceManagerEntity.RowKey))
                        {
                            AzureLogging.WriteToLog("Verbose", String.Format("Updating existing resource manager entry for culture [{0}] property [{1}] ", resourceManagerEntity.PartitionKey, resourceManagerEntity.RowKey));

                            rmeExisting.ResourcePropertyValue = resourceManagerEntity.ResourcePropertyValue;
                            rmeExisting.ResourcePropertyType = resourceManagerEntity.ResourcePropertyType;

                            tableResourceManager.UpdateObject(rmeExisting);
                            tableResourceManager.SaveChangesWithRetries();
                        }
                    }
                    catch (InvalidOperationException ioe)
                    {
                        Trace.WriteLine(String.Format("IOE EXCEPTION: {0}", ioe.Message));
                        Trace.WriteLine(String.Format("IOE EXCEPTION DUMP:\n{0}", ioe));

                        try
                        {
                            AzureLogging.WriteToLog("Verbose", String.Format("Creating new resource manager entry for culture [{0}] property [{1}] ", resourceManagerEntity.PartitionKey, resourceManagerEntity.RowKey));
                            tableResourceManager.AddObject(DB_TABLE_NAME_RESOURCES, resourceManagerEntity);
                            tableResourceManager.SaveChangesWithRetries();
                        }
                        catch (InvalidOperationException ioex)
                        {
                            Trace.WriteLine(String.Format("IOE EXCEPTION: {0}", ioex.Message));
                            Trace.WriteLine(String.Format("IOE EXCEPTION DUMP:\n{0}", ioex));
                        }
                    }

                }

                fSaved = true;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_SaveResourcesHandler()");

            return fSaved;
        }


        private bool CheckActiveNotifications(string szCountryCode)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.CheckActiveNotifications()");

            AzureLogging.WriteToLog("Information", String.Format("Searching for active notifications for country code [{0}] at [{1}]", szCountryCode, DateTime.UtcNow.ToString()));

            bool fNotificationsExist = false;

            try
            {
#if !OLD_SCHOOL
                //
                // Open Notifications Table
                //
                NotificationEntityTable m_NotificationsTable = OpenDatabaseTable(DB_TABLE_NAME_NOTIFICATIONS) as NotificationEntityTable;
#endif
                //
                // Query for active notification entities
                //
                var qResult = from n in m_NotificationsTable.Notifications where n.PartitionKey == szCountryCode select n;

                try
                {
                    //
                    // Do a take so we only check for 1+ notification entities
                    //
                    foreach (NotificationEntity ne in qResult.Take(2))
                    {
                        fNotificationsExist = true;
                    }
                }
                catch (DataServiceQueryException e)
                {
                    HttpStatusCode s;

                    if (TableStorageHelpers.EvaluateException(e, out s) && s == HttpStatusCode.NotFound)
                    {
                        //
                        // No notification entity found
                        //
                        AzureLogging.WriteToLog("Warning", String.Format("No active notifications found for country code [{0}] at [{1}]", szCountryCode, DateTime.UtcNow.ToString()));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.CheckActiveNotifications()");

            return fNotificationsExist;
        }


        private List<MessageNotificationLocation> QueryActiveNotifications(string szCountryCode)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.QueryActiveNotifications()");

            List<MessageNotificationLocation> listActiveNotifications = null;

            if (CheckActiveNotifications(szCountryCode))
            {
                try
                {
                    AzureLogging.WriteToLog("Information", String.Format("Searching for active notifications for country code [{0}] at [{1}]", szCountryCode, DateTime.UtcNow.ToString()));

#if !OLD_SCHOOL
                    //
                    // Open Notifications Table
                    //
                    NotificationEntityTable m_NotificationsTable = OpenDatabaseTable(DB_TABLE_NAME_NOTIFICATIONS) as NotificationEntityTable;
#endif

                    //
                    // Query for active notification entities
                    //
                    var qResult = from n in m_NotificationsTable.Notifications where n.PartitionKey == szCountryCode select n;

                    TableStorageDataServiceQuery<NotificationEntity> q = new TableStorageDataServiceQuery<NotificationEntity>(qResult as DataServiceQuery<NotificationEntity>, m_NotificationsTable.RetryPolicy);

                    try
                    {
                        //
                        // the query references the whole key and explicitly addresses one entity
                        // thus, this query can generate an exception if there are 0 results during enumeration
                        IEnumerable<NotificationEntity> activeNotifications = q.ExecuteAllWithRetries();

                        //
                        // Create list for further processing by notification engine
                        //
                        listActiveNotifications = new List<MessageNotificationLocation>();

                        foreach (NotificationEntity ne in activeNotifications)
                        {
                            //
                            // Notification entity found
                            //
                            AzureLogging.WriteToLog("Information", String.Format("Found active notifications for mobile number [{0}] location latitude [{1}], location longitude [{2}]", ne.MobileNumber, ne.Latitude, ne.Longitude));

                            //
                            // Create the internal objects for the notification engine
                            //

                            //
                            // TBD - look up user by mobile number... to get friendly name and culture for notification message
                            //
                            MessageNotificationPerson user = new MessageNotificationPerson(ne.MobileNumber, ne.MobileNumber, ne.Locale);
                            MessageNotificationLocation location = new MessageNotificationLocation(ne.NotificationType, ne.LocationName, ne.Latitude, ne.Longitude, ne.LastMeasurement, ne.LastIndex, user);

                            //
                            // Add Notification to active list
                            //
                            listActiveNotifications.Add(location);
                        }

                        if ((listActiveNotifications != null) && (listActiveNotifications.Count > 0))
                        {
                            AzureLogging.WriteToLog("Information", String.Format("Found [{0}] active notifications for country code [{1}] at [{2}]", listActiveNotifications.Count, szCountryCode, DateTime.UtcNow.ToString()));
                        }
                        else
                        {
                            listActiveNotifications = null;
                        }
                    }
                    catch (DataServiceQueryException e)
                    {
                        HttpStatusCode s;

                        if (TableStorageHelpers.EvaluateException(e, out s) && s == HttpStatusCode.NotFound)
                        {
                            //
                            // No notification entity found
                            //
                            AzureLogging.WriteToLog("Warning", String.Format("No active notifications found for country code [{0}] at [{1}]", szCountryCode, DateTime.UtcNow.ToString()));
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }
            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.QueryActiveNotifications()");

            return listActiveNotifications;
        }

        private bool StorageManager_CheckBlockNumberHandler(string szMobileNumber)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_CheckBlockNumberHandler()");

            bool fBlocked = false;

            try
            {
                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(szMobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    BlockedNumberEntity bne = (from bn in this.m_BlockedNumberTable.BlockedNumbers where (bn.PartitionKey == szCountryCode) && (bn.RowKey == szMobileNumber) select bn).FirstOrDefault();

                    if ((bne != null) && (bne.MobileNumber == szMobileNumber))
                    {
                        fBlocked = true;
                    }
                }
            }
            catch (DataServiceQueryException e)
            {
                HttpStatusCode s;

                if (TableStorageHelpers.EvaluateException(e, out s) && s == HttpStatusCode.NotFound)
                {
                    //
                    // No blocked number entity found
                    //
                    AzureLogging.WriteToLog("Verbose", String.Format("Mobile number [{0}] not blocked at [{1}]", szMobileNumber, DateTime.UtcNow.ToString()));
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_CheckBlockNumberHandler()");

            return fBlocked;
        }

        private bool StorageManager_BlockNumberHandler(string szMobileNumber, string szBlockMessage)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_BlockNumberHandler()");

            bool fBlocked = false;

            try
            {
                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(szMobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    BlockedNumberEntity bne = new BlockedNumberEntity(szCountryCode, szMobileNumber, szBlockMessage, DateTime.UtcNow.ToString());

                    m_BlockedNumberTable.AddObject(DB_TABLE_NAME_BLOCKED_NUMBERS, bne);
                    m_BlockedNumberTable.SaveChangesWithRetries();

                    fBlocked = true;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_BlockNumberHandler()");

            return fBlocked;
        }

        private MessageNotificationPerson StorageManager_LoadRegistrationHandler(string szMobileNumber)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_LoadRegistrationHandler()");

            MessageNotificationPerson user = null;

            try
            {
                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(szMobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    PersonEntity pe = (from p in this.m_PersonsTable.Persons where (p.PartitionKey == szCountryCode) && (p.RowKey == szMobileNumber) select p).FirstOrDefault();

                    if ((pe != null) && (pe.MobileNumber == szMobileNumber))
                    {
                        user = new MessageNotificationPerson(pe.MobileNumber, pe.DisplayName, pe.DisplayName, pe.Locale, pe.TermsOfUse);
                    }
                }
            }
            catch (DataServiceQueryException e)
            {
                HttpStatusCode s;

                if (TableStorageHelpers.EvaluateException(e, out s) && s == HttpStatusCode.NotFound)
                {
                    //
                    // No registration found
                    //
                    AzureLogging.WriteToLog("Verbose", String.Format("Mobile number [{0}] not registered at [{1}]", szMobileNumber, DateTime.UtcNow.ToString()));
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_LoadRegistrationHandler()");

            return user;
        }

        private bool StorageManager_SaveRegistrationHandler(MessageNotificationPerson user)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_SaveRegistrationHandler()");

            bool fSaved = false;

            try
            {
                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(user.MobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    try
                    {
                        PersonEntity pe = new PersonEntity(szCountryCode, user.MobileNumber, user.Culture, user.Name, user.TermsOfUseAccepted.ToString());

                        m_PersonsTable.AddObject(DB_TABLE_NAME_PERSONS, pe);
                        m_PersonsTable.SaveChangesWithRetries();

                        fSaved = true;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        Trace.WriteLine(String.Format("IOE EXCEPTION: {0}", ioe.Message));
                        Trace.WriteLine(String.Format("IOE EXCEPTION DUMP:\n{0}", ioe));

                        Trace.WriteLine(String.Format("Updating existing registration entry for user [{0}]", user.MobileNumber));

                        PersonEntity pe = (from p in this.m_PersonsTable.Persons where (p.PartitionKey == szCountryCode) && (p.RowKey == user.MobileNumber) select p).FirstOrDefault();

                        if ((pe != null) && (pe.MobileNumber == user.MobileNumber))
                        {
                            pe.TermsOfUse = user.TermsOfUseAccepted.ToString();
                            m_PersonsTable.UpdateObject(pe);
                            m_PersonsTable.SaveChangesWithRetries();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_SaveRegistrationHandler()");

            return fSaved;
        }

        private List<MessageNotificationLocation> StorageManager_LoadNotificationsHandler(string szMobileNumber)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_SaveNotificationHandler()");

            List<MessageNotificationLocation> listUserNotifications = null;

            try
            {
#if !OLD_SCHOOL
                //
                // Open Notifications Table
                //
                NotificationEntityTable m_NotificationsTable = OpenDatabaseTable(DB_TABLE_NAME_NOTIFICATIONS) as NotificationEntityTable;
#endif

                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(szMobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    //
                    // Query for active notification entities
                    //
                    var qResult = from n in m_NotificationsTable.Notifications where n.PartitionKey == szCountryCode && n.RowKey == szMobileNumber select n;

                    TableStorageDataServiceQuery<NotificationEntity> q = new TableStorageDataServiceQuery<NotificationEntity>(qResult as DataServiceQuery<NotificationEntity>, m_NotificationsTable.RetryPolicy);

                    try
                    {
                        //
                        // the query references the whole key and explicitly addresses one entity
                        // thus, this query can generate an exception if there are 0 results during enumeration
                        IEnumerable<NotificationEntity> activeNotifications = q.ExecuteAllWithRetries();

                        //
                        // Create list for further processing by notification engine
                        //
                        listUserNotifications = new List<MessageNotificationLocation>();

                        foreach (NotificationEntity ne in activeNotifications)
                        {
                            //
                            // Create the internal objects for the notification engine
                            //
                            MessageNotificationPerson user = new MessageNotificationPerson(ne.MobileNumber, ne.MobileNumber, ne.Locale);
                            MessageNotificationLocation location = new MessageNotificationLocation(ne.NotificationType, ne.LocationName, ne.Latitude, ne.Longitude, ne.LastMeasurement, ne.LastIndex, user);

                            //
                            // Add Notification to active list
                            //
                            listUserNotifications.Add(location);
                        }

                        if ((listUserNotifications != null) && (listUserNotifications.Count > 0))
                        {
                            AzureLogging.WriteToLog("Information", String.Format("Found [{0}] active notifications for mobile number [{1}] at [{2}]", listUserNotifications.Count, szMobileNumber, DateTime.UtcNow.ToString()));
                        }
                        else
                        {
                            listUserNotifications = null;
                        }
                    }
                    catch (DataServiceQueryException e)
                    {
                        HttpStatusCode s;

                        if (TableStorageHelpers.EvaluateException(e, out s) && s == HttpStatusCode.NotFound)
                        {
                            //
                            // No notification entity found
                            //
                            AzureLogging.WriteToLog("Warning", String.Format("No active notifications found for mobile number [{0}] at [{1}]", szMobileNumber, DateTime.UtcNow.ToString()));
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_SaveNotificationHandler()");

            return listUserNotifications;
        }

        private bool StorageManager_SaveNotificationHandler(MessageNotificationPerson user, MessageNotificationLocation location)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_SaveNotificationHandler()");

            bool fSaved = false;

            try
            {
#if !OLD_SCHOOL
                //
                // Open Notifications Table
                //
                NotificationEntityTable m_NotificationsTable = OpenDatabaseTable(DB_TABLE_NAME_NOTIFICATIONS) as NotificationEntityTable;
#endif

                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(user.MobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    NotificationEntity ne = new NotificationEntity(szCountryCode, user.MobileNumber, MEASUREMENT_AIR_QUALITY, user.Culture, location.Name, location.Longitude.ToString(), location.Latitude.ToString(), location.MeasurementValue.ToString(), location.QualityIndex.ToString());

                    try
                    {
                        NotificationEntity neExisting = (from n in m_NotificationsTable.Notifications where ((n.PartitionKey == szCountryCode) && (n.RowKey == user.MobileNumber)) select n).FirstOrDefault();

                        if ((neExisting != null) && (neExisting.MobileNumber == user.MobileNumber))
                        {
                            Trace.WriteLine(String.Format("Updating existing notification entry for user [{0}]", user.MobileNumber));

                            neExisting.LocationName = ne.LocationName;
                            neExisting.Latitude = ne.Latitude;
                            neExisting.Longitude = ne.Longitude;
                            neExisting.LastMeasurement = ne.LastMeasurement;
                            neExisting.LastIndex = ne.LastIndex;
                            neExisting.Locale = ne.Locale;

                            m_NotificationsTable.UpdateObject(neExisting);
                            m_NotificationsTable.SaveChangesWithRetries();
                        }
                    }
                    catch (InvalidOperationException ioe)
                    {
                        Trace.WriteLine(String.Format("IOE EXCEPTION: {0}", ioe.Message));
                        Trace.WriteLine(String.Format("IOE EXCEPTION DUMP:\n{0}", ioe));

                        try
                        {
                            m_NotificationsTable.AddObject(DB_TABLE_NAME_NOTIFICATIONS, ne);
                            m_NotificationsTable.SaveChangesWithRetries();
                        }
                        catch (InvalidOperationException ioex)
                        {
                            Trace.WriteLine(String.Format("IOE EXCEPTION: {0}", ioex.Message));
                            Trace.WriteLine(String.Format("IOE EXCEPTION DUMP:\n{0}", ioex));
                        }
                    }

                    fSaved = true;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_SaveNotificationHandler()");

            return fSaved;
        }

        private bool StorageManager_DeleteRegistrationHandler(MessageNotificationPerson user)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_DeleteRegistrationHandler()");

            bool fDeleted = false;

            try
            {
                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(user.MobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    //
                    //
                    //
                    IEnumerable<PersonEntity> res;

                    StorageAccountInfo account = StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration();

                    TableStorageDataServiceQuery<PersonEntity> q;

                    var qResult = from p in this.m_PersonsTable.CreateQuery<PersonEntity>(DB_TABLE_NAME_PERSONS) where (p.PartitionKey == szCountryCode && p.RowKey == user.MobileNumber) select p;

                    q = new TableStorageDataServiceQuery<PersonEntity>(qResult as DataServiceQuery<PersonEntity>);

                    res = q.ExecuteAllWithRetries();

                    foreach (PersonEntity p in res)
                    {
                        AzureLogging.WriteToLog("Information", String.Format("Deleting user registration for mobile number [{0}]", p.MobileNumber));
                        m_PersonsTable.DeleteObject(p);
                        m_PersonsTable.SaveChangesWithRetries();
                    }

                    fDeleted = true;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_DeleteRegistrationHandler()");

            return fDeleted;
        }

        private bool StorageManager_ClearAllNotificationsHandler(string szMobileNumber)
        {
            AzureLogging.WriteToLog("Verbose", ">> AirWatch.Notifications.StorageManager_ClearAllNotificationsHandler()");

            bool fCleared = false;

            try
            {
#if !OLD_SCHOOL
                //
                // Open Notifications Table
                //
                NotificationEntityTable m_NotificationsTable = OpenDatabaseTable(DB_TABLE_NAME_NOTIFICATIONS) as NotificationEntityTable;
#endif

                //
                // Lookup country code of mobile number
                //
                string szCountryCode = NotificationEngine.GetCurrentEngine().FindCountryCodeForNumberByZone(szMobileNumber);

                if (!String.IsNullOrEmpty(szCountryCode))
                {
                    //
                    //
                    //
                    IEnumerable<NotificationEntity> res;

                    StorageAccountInfo account = StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration();

                    TableStorageDataServiceQuery<NotificationEntity> q;

                    var qResult = from ne in m_NotificationsTable.CreateQuery<NotificationEntity>(DB_TABLE_NAME_NOTIFICATIONS) where (ne.PartitionKey == szCountryCode && ne.RowKey == szMobileNumber) select ne;

                    q = new TableStorageDataServiceQuery<NotificationEntity>(qResult as DataServiceQuery<NotificationEntity>);

                    res = q.ExecuteAllWithRetries();

                    foreach (NotificationEntity ne in res)
                    {
                        AzureLogging.WriteToLog("Information", String.Format("Deleting active notifications for mobile number [{0}] location latitude [{1}], location longitude [{2}]", ne.MobileNumber, ne.Latitude, ne.Longitude));
                        m_NotificationsTable.DeleteObject(ne);
                        m_NotificationsTable.SaveChangesWithRetries();
                    }

                    fCleared = true;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            AzureLogging.WriteToLog("Verbose", "<< AirWatch.Notifications.StorageManager_ClearAllNotificationsHandler()");

            return fCleared;
        }

    }
}
