﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Resources;
using System.Globalization;

namespace Microsoft.AirWatch.Notifications.Storage
{
    /// <summary>
    ///   An azure table resource class, for looking up localized strings, etc.
    /// </summary>
    public class AzureResourceManager : IResourceManager
    {
        private Dictionary<string, ResourceManagerEntity> m_ResourceEntities;
        private global::System.Globalization.CultureInfo resourceCulture;

        public AzureResourceManager(string szCulture, List<ResourceManagerEntity> resourceEntities)
        {
            resourceCulture = new CultureInfo(szCulture, false);
            m_ResourceEntities = new Dictionary<string, ResourceManagerEntity>();

            foreach (ResourceManagerEntity rme in resourceEntities)
            {
                m_ResourceEntities.Add(rme.ResourcePropertyName, rme);
            }
        }

        #region IResourceManager Members

        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public global::System.Globalization.CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }


        public Dictionary<string, ResourceManagerEntity> GetEntities()
        {
            return m_ResourceEntities;
        }

        public string GetString(string name, CultureInfo culture)
        {
            if ((culture != null) && (culture.LCID != resourceCulture.LCID))
            {
                throw new NotSupportedException();
            }
            try
            {
                if ((m_ResourceEntities != null) && m_ResourceEntities.ContainsKey(name))
                {
                    ResourceManagerEntity rme = m_ResourceEntities[name];

                    if (rme.ResourcePropertyType == "System.String")
                    {
                        return rme.ResourcePropertyValue;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
            return null;
        }

        public object GetObject(string name, CultureInfo culture)
        {
            object value = null;

            if (culture.LCID != resourceCulture.LCID)
            {
                throw new NotSupportedException();
            }

            if ((m_ResourceEntities != null) && m_ResourceEntities.ContainsKey(name))
            {
                ResourceManagerEntity rme = m_ResourceEntities[name];

                switch (rme.ResourcePropertyType)
                {
                    case "System.String":
                        value = rme.ResourcePropertyValue;
                        break;
                    default:
                        value = rme.ResourcePropertyValue;
                        break;
                }
            }
            return value;
        }

        #endregion


    }

}

