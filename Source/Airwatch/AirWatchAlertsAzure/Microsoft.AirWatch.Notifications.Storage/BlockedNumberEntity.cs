﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System;
    using System.Globalization;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// BlockedNumber table entity
    /// </summary>
    public class BlockedNumberEntity : TableStorageEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlockedNumberEntity"/> class.
        /// </summary>
        public BlockedNumberEntity(string countryCode, string mobileNumber, string blockRequest, string blockDateTime)
        {
            PartitionKey = countryCode;
            RowKey = mobileNumber;
            this.CountryCode = countryCode;
            this.MobileNumber = mobileNumber;
            this.BlockRequest = blockRequest;
            this.BlockDateTime = blockDateTime;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BlockedNumberEntity"/> class.
        /// </summary>
        public BlockedNumberEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the mobile number string.
        /// </summary>
        /// <value>The mobile number.</value>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the original block request.
        /// </summary>
        /// <value>The original block request.</value>
        public string BlockRequest { get; set; }

        /// <summary>
        /// Gets or sets the original block request.
        /// </summary>
        /// <value>The original block request timestamp.</value>
        public string BlockDateTime { get; set; }
    }
}
