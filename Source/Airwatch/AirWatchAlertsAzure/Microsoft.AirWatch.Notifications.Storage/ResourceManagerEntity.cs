﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System;
    using System.Globalization;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// ResourceManager table entity
    /// </summary>
    public class ResourceManagerEntity : TableStorageEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceManagerEntity"/> class.
        /// </summary>
        public ResourceManagerEntity(string culture, string resourcePropertyName, string resourcePropertyValue, string resourcePropertyType)
        {
            PartitionKey = culture;
            RowKey = resourcePropertyName;
            this.Culture = culture;
            this.ResourcePropertyName = resourcePropertyName;
            this.ResourcePropertyValue = resourcePropertyValue;
            this.ResourcePropertyType = resourcePropertyType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceManagerEntity"/> class.
        /// </summary>
        public ResourceManagerEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the resourcePropertyName string.
        /// </summary>
        /// <value>The ResourcePropertyName.</value>
        public string ResourcePropertyName { get; set; }

        /// <summary>
        /// Gets or sets the resourcePropertyValue.
        /// </summary>
        /// <value>The resourcePropertyValue.</value>
        public string ResourcePropertyValue { get; set; }

        /// <summary>
        /// Gets or sets the resourcePropertyType.
        /// </summary>
        /// <value>The ResourcePropertyType.</value>
        public string ResourcePropertyType { get; set; }
    }
}
