﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data.Services.Common;
using System.Data.Services.Client;
using Microsoft.Samples.ServiceHosting.StorageClient;
using Microsoft.Azure.WorkerRole.Utilities;

namespace Microsoft.AirWatch.Notifications.Storage
{
    public class TableManager<T, V> where T : TableStorageDataServiceContext
    {
        private StorageAccountInfo m_StorageAccount;

        public TableManager(StorageAccountInfo account)
        {
            m_StorageAccount = account;
        }

        public void CreateStorageTable()
        {
            try
            {
                TableStorage.CreateTablesFromModel(typeof(T), m_StorageAccount);
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
        }

        public void DeleteAllTableEntries(T tsdsc, string tableName)
        {
            try
            {
                IEnumerable<V> res;
                TableStorageDataServiceQuery<V> q;

                var qResult = from c in tsdsc.CreateQuery<V>(tableName) select c;

                q = new TableStorageDataServiceQuery<V>(qResult as DataServiceQuery<V>);

                res = q.ExecuteAllWithRetries();

                foreach (V s in res)
                {
                    tsdsc.DeleteObject(s);
                    tsdsc.SaveChangesWithRetries();
                }
            }
            catch (System.Net.WebException we)
            {
                Trace.WriteLine(String.Format("WEB EXCEPTION: {0}", we.Message));
                Trace.WriteLine(String.Format("WEB EXCEPTION DUMP:\n{0}", we));
            }
            catch (StorageException se)
            {
                Trace.WriteLine(String.Format("STORAGE EXCEPTION: {0}", se.Message));
                Trace.WriteLine(String.Format("STORAGE EXCEPTION DUMP:\n{0}", se));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
        }

    }
}
