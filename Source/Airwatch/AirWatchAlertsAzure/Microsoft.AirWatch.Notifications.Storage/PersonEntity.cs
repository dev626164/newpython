﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System;
    using System.Globalization;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// Person table entity
    /// </summary>
    public class PersonEntity : TableStorageEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonEntity"/> class.
        /// </summary>
        public PersonEntity(string countryCode, string mobileNumber, string locale, string displayname, string touAccepted)
        {
            PartitionKey = countryCode;
            RowKey = mobileNumber;
            this.CountryCode = countryCode;
            this.MobileNumber = mobileNumber;
            this.Locale = locale;
            this.DisplayName = displayname;
            this.TermsOfUse = touAccepted;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonEntity"/> class.
        /// </summary>
        public PersonEntity()
            : base(string.Empty, string.Format(CultureInfo.InvariantCulture, "{0:d10}", DateTime.UtcNow))
        {
        }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the locale.
        /// </summary>
        /// <value>The locale.</value>
        public string Locale { get; set; }

        /// <summary>
        /// Gets or sets the mobile number string.
        /// </summary>
        /// <value>The mobile number.</value>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the Display Name.
        /// </summary>
        /// <value>The Person Display Name.</value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the TermsOfUse.
        /// </summary>
        /// <value>The Person TermsOfUse.</value>
        public string TermsOfUse { get; set; }
    }
}
