﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System.Data.Services.Client;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// ResourceManagerEntityTable
    /// </summary>
    public class ResourceManagerEntityTable : TableStorageDataServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceManagerEntityTable"/> class.
        /// </summary>
        public ResourceManagerEntityTable()
            : base(StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration())
        {
        }

        public ResourceManagerEntityTable(StorageAccountInfo accountInfo)
            : base(accountInfo)
        {
        }

        /// <summary>
        /// Gets the ResourceManagerEntity.
        /// </summary>
        /// <value>The ResourceManagerEntity.</value>
        public DataServiceQuery<ResourceManagerEntity> ResourceManagerEntities
        {
            get
            {
                return CreateQuery<ResourceManagerEntity>("ResourceManagerEntities");
            }
        }
    }
}
