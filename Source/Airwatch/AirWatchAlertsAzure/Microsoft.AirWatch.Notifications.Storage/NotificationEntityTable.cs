﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System.Data.Services.Client;
    using Microsoft.Samples.ServiceHosting.StorageClient;
    
    /// <summary>
    /// Notification table
    /// </summary>
    public class NotificationEntityTable : TableStorageDataServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationEntityTable"/> class.
        /// </summary>
        public NotificationEntityTable()
            : base(StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration())
        {
        }

        public NotificationEntityTable(StorageAccountInfo accountInfo)
            : base(accountInfo)
        {
        }

        /// <summary>
        /// Gets the notifications.
        /// </summary>
        /// <value>The notifications.</value>
        public DataServiceQuery<NotificationEntity> Notifications
        {
            get
            {
                return CreateQuery<NotificationEntity>("Notifications");
            }
        }
    }
}
