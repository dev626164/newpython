﻿namespace Microsoft.AirWatch.Notifications.Storage
{
    using System.Data.Services.Client;
    using Microsoft.Samples.ServiceHosting.StorageClient;

    /// <summary>
    /// Person table
    /// </summary>
    public class PersonEntityTable : TableStorageDataServiceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonEntityTable"/> class.
        /// </summary>
        public PersonEntityTable()
            : base(StorageAccountInfo.GetDefaultTableStorageAccountFromConfiguration())
        {
        }

        public PersonEntityTable(StorageAccountInfo accountInfo)
            : base(accountInfo)
        {
        }

        /// <summary>
        /// Gets the Persons.
        /// </summary>
        /// <value>The Persons.</value>
        public DataServiceQuery<PersonEntity> Persons
        {
            get
            {
                return CreateQuery<PersonEntity>("Persons");
            }
        }
    }
}
