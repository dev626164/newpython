﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using Microsoft.ServiceHosting.ServiceRuntime;

using Microsoft.Azure.WorkerRole.Utilities;

using WorkerRole1;

namespace AzureWorkerRoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            AzureWorkerRoleHarness<WorkerRole>.Start();
        }
    }
}
