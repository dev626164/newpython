﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Globalization;

using System.Mobile.Messaging;
using System.Mobile.Contacts;
using Microsoft.Mobile.Messaging.Utils;



namespace Microsoft.AirWatch.Notifications
{
    public class NotificationEngine
    {
        private const string MEASUREMENT_AIR_QUALITY = "CAQI";
        private const string MEASUREMENT_WATER_QUALITY = "CWQI";

        private const string APPSETTING_USE_TOD = "airwatch-notifications-use-tod";
        private const string APPSETTING_USE_INTERVAL = "airwatch-notifications-use-interval";
        private const string APPSETTING_TOD_HOUR = "airwatch-notifications-tod-hour";
        private const string APPSETTING_TOD_MINUTE = "airwatch-notifications-tod-minute";
        private const string APPSETTING_INTERVAL_MINUTES = "airwatch-notifications-interval-minutes";

        private const bool SETTINGS_USE_TOD = true;
        private const bool SETTINGS_USE_INTERVAL = false;
        private const int SETTINGS_INTERVAL_MINUTES = 240;
        private const int SETTINGS_TOD_HOUR = 9;
        private const int SETTINGS_TOD_MINUTE = 0;

        private static object s_engineLock = new object();
        private static NotificationEngine s_currentEngine = null;


        private CountryCodeTable m_CountryCodeTable;
        private Dictionary<string, NotificationResourceMessages> m_ResourceMessages;

        private DateTime m_dtLastNotificationSent;
        private TimeSpan m_tsNotificationInterval;
        private DateTime m_dtNotificationAlignment;
        private bool m_fUseNotificationInterval;
        private bool m_fUseNotificationTimeOfDay;


        public event LoadResourcesForLocale LoadResourcesForLocaleHandler;
        public event AddBlockedNumber AddBlockNumberHandler;
        public event CheckBlockedNumber CheckBlockNumberHandler;
        public event LoadRegistration LoadRegistrationHandler;
        public event SaveRegistration SaveRegistrationHandler;
        public event DeleteRegistration DeleteRegistrationHandler;
        public event LoadNotifications LoadNotificationsHandler;
        public event SaveNotification SaveNotificationHandler;
        public event ClearAllNotifications ClearAllNotificationsHandler;
        public event QueryNotifications QueryNotificationsHandler;
        public event SendMessageDelegate SendMessageHandler;
        public event LocationResolver LocationResolverHandler;

        public event RetrieveMeasurementData RetrieveMeasurementDataHandler;
        public event RetrieveAirQualityIndexDelegate RetrieveAirQualityIndexHandler;
        public event RetrieveWaterQualityIndexDelegate RetrieveWaterQualityIndexHandler;

        public NotificationEngine()
        {
            Trace.WriteLine(">> NotificationEngine()");

            //
            // Load Country Code Table
            //
            m_CountryCodeTable = CountryCodeTableFactory.LoadCountryCodeTable();

            //
            // Initialize zone lookup tables
            //
            m_CountryCodeTable.InitializeZoneTable();

            //
            // Initialize resource manager cache
            //
            m_ResourceMessages = new Dictionary<string, NotificationResourceMessages>();

            //
            // Preload internal / fallback languages
            //
            m_ResourceMessages.Add("en-GB", new NotificationResourceMessages("en-GB"));
            //m_ResourceMessages.Add("en-US", new NotificationResourceMessages("en-US"));
            //m_ResourceMessages.Add("fr-FR", new NotificationResourceMessages("fr-FR"));
            //m_ResourceMessages.Add("es-ES", new NotificationResourceMessages("es-ES"));
            //m_ResourceMessages.Add("de-DE", new NotificationResourceMessages("de-DE"));


            //
            // Set starting notification period
            //
            m_dtLastNotificationSent = DateTime.UtcNow;

            LoadSettings();

            Trace.WriteLine("<< NotificationEngine()");
        }

        public static NotificationEngine GetCurrentEngine()
        {
            lock (s_engineLock)
            {
                if (s_currentEngine == null)
                {
                    s_currentEngine = new NotificationEngine();
                }
            }
            return s_currentEngine;
        }


        private void LoadSettings()
        {
            Trace.WriteLine(">> NotificationEngine.LoadSettings()");

            //
            // Notification Engine Defaults
            //
            m_fUseNotificationInterval = SETTINGS_USE_INTERVAL;
            m_fUseNotificationTimeOfDay = SETTINGS_USE_TOD;
            m_tsNotificationInterval = new TimeSpan(0, SETTINGS_INTERVAL_MINUTES, 0);
            DateTime dtCurrentTime = DateTime.UtcNow;
            m_dtNotificationAlignment = new DateTime(dtCurrentTime.Year, dtCurrentTime.Month, dtCurrentTime.Day, SETTINGS_TOD_HOUR, SETTINGS_TOD_MINUTE, 00, DateTimeKind.Utc);

            try
            {
                int intervalMinutes = SETTINGS_INTERVAL_MINUTES;

                Boolean.TryParse(ConfigurationSettings.AppSettings[APPSETTING_USE_INTERVAL], out m_fUseNotificationInterval);

                int.TryParse(ConfigurationSettings.AppSettings[APPSETTING_INTERVAL_MINUTES], out intervalMinutes);
                
                //
                // Set notification interval
                //
                m_tsNotificationInterval = new TimeSpan(0, intervalMinutes, 0);
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                m_fUseNotificationInterval = false;
            }

            try
            {
                int todHour = SETTINGS_TOD_HOUR;
                int todMinute = SETTINGS_TOD_MINUTE;

                Boolean.TryParse(ConfigurationSettings.AppSettings[APPSETTING_USE_TOD], out m_fUseNotificationTimeOfDay);

                int.TryParse(ConfigurationSettings.AppSettings[APPSETTING_TOD_HOUR], out todHour);
                int.TryParse(ConfigurationSettings.AppSettings[APPSETTING_TOD_MINUTE], out todMinute);

                //
                // Set notification alignment
                //
                m_dtNotificationAlignment = new DateTime(dtCurrentTime.Year, dtCurrentTime.Month, dtCurrentTime.Day, todHour, todMinute, 00, DateTimeKind.Utc);
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                m_fUseNotificationTimeOfDay = false;
            }

            Trace.WriteLine("<< NotificationEngine.LoadSettings()");
        }


        public int LoadLocalizationData()
        {
            Trace.WriteLine(">> NotificationEngine.LoadLocalizationData()");

            int iResourcesLoaded = 0;

            try
            {
                Dictionary<String, CultureInfo> knownCultures = new Dictionary<String, CultureInfo>();
                NotificationResourceMessages nrm = null;
                int i = 0;

                //
                // Dump list of found cultures
                //
                Trace.WriteLine("    NUMBER CULTURE ISO ISO WIN DISPLAYNAME                              ENGLISHNAME");

                //
                // For each culture, attempt to load the resources for that specific locale
                //
                string[] rgszBaseCultures = { "bg-BG", "cs-CZ", "da-DK", "de-DE", "el-GR", "en-US", "fi-FI", "fr-FR", "hu-HU", "is-IS", "it-IT", "nl-NL", "nb-NO", "pl-PL", "ro-RO", "sk-SK", "sv-SE", "tr-TR", "sl-SI", "et-EE", "lv-LV", "lt-LT", "pt-PT", "es-ES", "ga-IE", "mt-MT" };
                CultureInfo[] rgCultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

                if (rgCultures.Length < rgszBaseCultures.Length)
                {
                    List<CultureInfo> listBaseCultures = new List<CultureInfo>();

                    foreach (string culture in rgszBaseCultures)
                    {
                        listBaseCultures.Add(new CultureInfo(culture));
                    }

                    rgCultures = listBaseCultures.ToArray();
                }

                //foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
                foreach (CultureInfo ci in rgCultures)
                {
                    try
                    {
                        string szCultureName = ci.TextInfo.CultureName;

                        if (!knownCultures.ContainsKey(szCultureName))
                        {
                            Trace.WriteLine(String.Format("    {0,-7} {1,-12} {2,-3} {3,-3} {4,-3} {5,-50} {6,-40}", i++, ci.TextInfo.CultureName, ci.TwoLetterISOLanguageName, ci.ThreeLetterISOLanguageName, ci.ThreeLetterWindowsLanguageName, ci.EnglishName, ci.DisplayName));

                            knownCultures.Add(szCultureName, ci);

                            //
                            // Load resources for this culture
                            //
                            nrm = LoadResourceMessages(szCultureName);

                            if (nrm != null)
                            {
                                Trace.WriteLine(String.Format("Found localization for Known Culture [{0}] ==> [{1}]", szCultureName, ci.EnglishName));

                                if (!m_ResourceMessages.ContainsKey(szCultureName))
                                {
                                    Trace.WriteLine(String.Format("Adding new localization for Known Culture [{0}] ==> [{1}]", szCultureName, ci.EnglishName));
                                    m_ResourceMessages.Add(szCultureName, nrm);
                                }
                                else
                                {
                                    Trace.WriteLine(String.Format("Updating localization for Known Culture [{0}] ==> [{1}]", szCultureName, ci.EnglishName));
                                    m_ResourceMessages[szCultureName] = nrm;
                                }

                                iResourcesLoaded++;
                            }
                            else
                            {
                                Trace.WriteLine(String.Format("No localization resources for Known Culture [{0}] ==> [{1}]", szCultureName, ci.EnglishName));
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                        Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< NotificationEngine.LoadLocalizationData()");

            return iResourcesLoaded;
        }

        public string FindLocaleForNumberByZone(string szMobileNumber)
        {
            return m_CountryCodeTable.FindLocaleForNumberByZone(szMobileNumber);
        }

        public string FindCountryCodeForNumberByZone(string szMobileNumber)
        {
            return m_CountryCodeTable.FindCountryCodeForNumberByZone(szMobileNumber);
        }

        public NotificationResourceMessages GetMessageLocale(ShortMessage msg)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.GetMessageLocale()");

            NotificationResourceMessages nrm = null;

            try
            {
                string szSourceAddress = msg.GetRawSourceAddress();

                //
                // Lookup locale based on inbound mobile number
                //
                string szCulture = FindLocaleForNumberByZone(szSourceAddress);

                if (!String.IsNullOrEmpty(szCulture))
                {
                    //
                    // Update message locale
                    //
                    msg.Locale = szCulture;

                    //
                    // TBD - Validate that this locale is correct based on message body text
                    //
                    nrm = LoadResourceMessages(szCulture);

                    if (nrm == null)
                    {
                        nrm = LoadResourceMessages("en-GB");
                    }

                    if (!NotificationRequestParser.IsRecognizedRequest(msg, nrm))
                    {
                        Trace.WriteLine("WARNING: GetMessageLocale received invalid command or non-default language request");

                        foreach (string szCultureKey in m_ResourceMessages.Keys)
                        {
                            if (NotificationRequestParser.IsRecognizedRequest(msg, m_ResourceMessages[szCultureKey]))
                            {
                                nrm = m_ResourceMessages[szCultureKey];
                                break;
                            }
                        }
                    }
                    else
                    {
                        Trace.WriteLine("INFORMATION: GetMessageLocale received valid command for default language");
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.GetMessageLocale()");

            return nrm;
        }


        public NotificationResourceMessages LoadResourceMessages(string szCulture)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.LoadResourceMessages()");

            NotificationResourceMessages nrm = null;

            try
            {
                if (m_ResourceMessages.ContainsKey(szCulture))
                {
                    nrm = m_ResourceMessages[szCulture];
                }
                else
                {
                    if (LoadResourcesForLocaleHandler != null)
                    {
                        IResourceManager irm = this.LoadResourcesForLocaleHandler(szCulture);

                        if (irm != null)
                        {
                            nrm = new NotificationResourceMessages(irm);

                            m_ResourceMessages.Add(szCulture, nrm);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.LoadResourceMessages()");

            return nrm;
        }


        public MessageNotificationPerson GetRegisteredUser(string szMobileNumber)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.GetRegisteredUser()");

            MessageNotificationPerson user = null;

            try
            {
                if (LoadRegistrationHandler != null)
                {
                    user = this.LoadRegistrationHandler(szMobileNumber);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.GetRegisteredUser()");

            return user;
        }

        public MessageNotificationCollection GetNotificationByMobileNumber(string szMobileNumber)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.IsMobileNumberRegistered()");

            MessageNotificationCollection mnc = new MessageNotificationCollection(new MessageNotificationPerson(szMobileNumber, szMobileNumber, string.Empty));

            try
            {
                if (LoadNotificationsHandler != null)
                {
                    mnc.NotificationLocations = this.LoadNotificationsHandler(szMobileNumber);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.IsMobileNumberRegistered()");

            return mnc;
        }

        public void ClearNotificationByMobileNumber(string szMobileNumber)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.ClearNotificationByMobileNumber()");

            try
            {
                bool fCleared = false;

                //
                // Clear all notifications
                //
                if (ClearAllNotificationsHandler != null)
                {
                    fCleared = this.ClearAllNotificationsHandler(szMobileNumber);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.ClearNotificationByMobileNumber()");
        }

        public bool IsMobileNumberRegistered(string szMobileNumber)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.IsMobileNumberRegistered()");

            bool fRegistered = false;

            try
            {
                if (LoadRegistrationHandler != null)
                {
                    fRegistered = this.LoadRegistrationHandler(szMobileNumber) != null;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.IsMobileNumberRegistered()");

            return fRegistered;
        }

        public bool IsTermsOfUseAccepted(string szMobileNumber)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.IsMobileNumberRegistered()");

            bool fTermsOfUseAccepted = false;

            try
            {
                if (LoadRegistrationHandler != null)
                {
                    MessageNotificationPerson user = this.LoadRegistrationHandler(szMobileNumber);

                    if (user != null)
                    {
                        fTermsOfUseAccepted = user.TermsOfUseAccepted;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.IsMobileNumberRegistered()");

            return fTermsOfUseAccepted;
        }

        public bool IsMobileNumberBlocked(string szMobileNumber)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.IsMobileNumberBlocked()");

            bool fBlocked = false;

            try
            {
                if (CheckBlockNumberHandler != null)
                {
                    fBlocked = this.CheckBlockNumberHandler(szMobileNumber);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.IsMobileNumberBlocked()");

            return fBlocked;
        }


        public MessageNotificationLocation AddMessageToOwnerNotification(MessageNotificationPerson mbpOwner, MessageNotificationLocation NotificationLocation)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.AddMessageToOwnerNotification()");

            MessageNotificationLocation mnl = null;

            try
            {
                bool fSaved = false;

                //
                // Save notification
                //
                if (SaveNotificationHandler != null)
                {
                    fSaved = this.SaveNotificationHandler(mbpOwner, NotificationLocation);
                }

                if (fSaved)
                {
                    mnl = NotificationLocation;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.AddMessageToOwnerNotification()");

            return mnl;
        }

        public void BlockMobileNumber(string szMobileNumber, string szBlockMessage)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.BlockMobileNumber()");

            try
            {
                //
                // Save Mobile Number to Blocked Table
                //
                if (AddBlockNumberHandler != null)
                {
                    this.AddBlockNumberHandler(szMobileNumber, szBlockMessage);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.BlockMobileNumber()");
        }


        public MessageNotificationCollection RegisterNewUser(MessageNotificationPerson mbpNotificationOwner)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.RegisterNewUser()");

            try
            {
                bool fSaved = false;

                //
                // Save registration
                //
                if (SaveRegistrationHandler != null)
                {
                    fSaved = this.SaveRegistrationHandler(mbpNotificationOwner);
                }
                else
                {
                    fSaved = true;
                }

                if (fSaved)
                {
                    //
                    // Create new Notification
                    //
                    MessageNotificationCollection msgNotification = new MessageNotificationCollection(mbpNotificationOwner);

                    return msgNotification;
                }
                else
                {
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.RegisterNewUser()");

            return null;
        }

        public bool UpdateRegistration(MessageNotificationPerson mbpNotificationOwner)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.UpdateRegistration()");

            bool fSaved = false;

            try
            {
                //
                // Save registration
                //
                if (SaveRegistrationHandler != null)
                {
                    fSaved = this.SaveRegistrationHandler(mbpNotificationOwner);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.UpdateRegistration()");

            return fSaved;
        }

        public void DeRegisterExistingUser(MessageNotificationPerson mbpNotificationOwner)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.DeRegisterExistingUser()");

            try
            {
                bool fDeleted = false;

                //
                // Delete registration
                //
                if (DeleteRegistrationHandler != null)
                {
                    fDeleted = this.DeleteRegistrationHandler(mbpNotificationOwner);
                }

                if (fDeleted)
                {
                    Trace.WriteLine("User registration deleted");
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.DeRegisterExistingUser()");
        }

        public MessageNotificationLocation ResolveLocation(string szLocation, string szCulture)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.ResolveLocation()");

            MessageNotificationLocation location = null;

            try
            {
                //
                // If a Location Resolver is available, use it to process the message data
                //
                if (LocationResolverHandler != null)
                {
                    location = this.LocationResolverHandler(szLocation, szCulture);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.ResolveLocation()");

            return location;
        }

        public decimal? RetrieveAirMeasurement(MessageNotificationLocation location)
        {
            return RetrieveMeasurement(location, MEASUREMENT_AIR_QUALITY);
        }

        public decimal? RetrieveWaterMeasurement(MessageNotificationLocation location)
        {
            return RetrieveMeasurement(location, MEASUREMENT_WATER_QUALITY);
        }

        public decimal? RetrieveMeasurement(MessageNotificationLocation location, string szMeasurementType)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.RetrieveMeasurement()");

            decimal? dMeasurementValue = null;

            try
            {
                //
                // If a Measurement Handler is available, use it to retrieve measurement data for this location and type
                //
                if (RetrieveMeasurementDataHandler != null)
                {
                    dMeasurementValue = this.RetrieveMeasurementDataHandler((decimal)location.Longitude, (decimal)location.Latitude, szMeasurementType);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.RetrieveMeasurement()");

            return dMeasurementValue;
        }

        public MessageNotificationLocation RetrieveAirQualityIndex(MessageNotificationLocation location)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.RetrieveAirQualityIndex()");

            MessageNotificationLocation measureToReturn = null;

            try
            {
                //
                // If a Air Quality Handler is available, use it to retrieve air quality index for this location
                //
                if (RetrieveAirQualityIndexHandler != null)
                {
                    measureToReturn = this.RetrieveAirQualityIndexHandler((decimal)location.Longitude, (decimal)location.Latitude);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.RetrieveAirQualityIndex()");

            return measureToReturn;
        }


        public MessageNotificationLocation RetrieveWaterQualityIndex(MessageNotificationLocation location)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.RetrieveWaterQualityIndex()");

            MessageNotificationLocation measureToReturn = null;

            try
            {
                //
                // If a Water Quality Handler is available, use it to retrieve water quality index for this location
                //
                if (RetrieveWaterQualityIndexHandler != null)
                {
                    measureToReturn = this.RetrieveWaterQualityIndexHandler((decimal)location.Longitude, (decimal)location.Latitude);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.RetrieveWaterQualityIndex()");

            return measureToReturn;
        }


        public void SendNotifications(List<MessageNotificationLocation> listActiveNotifications)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.SendNotifications()");

            foreach (MessageNotificationLocation location in listActiveNotifications)
            {
                if (!String.IsNullOrEmpty(location.Name))
                {
                    bool fUpdated = false;
                    bool fSendNotification = false;

                    //
                    // Look up current location measurement value & compare with last notification value
                    //
                    decimal? dAirMeasurementValue = this.RetrieveMeasurement(location, location.NotificationType);

                    if ((dAirMeasurementValue != null) && ((double)dAirMeasurementValue != location.MeasurementValue) && ((double)dAirMeasurementValue != 0.0))
                    {
                        //
                        // Save last measurement value for threshold calculation
                        //
                        double dLastMeasurementValue = location.MeasurementValue;

                        //
                        // Update last measurement value to new value
                        //
                        location.MeasurementValue = (double)dAirMeasurementValue;
                        fUpdated = true;

                        //
                        // Look up current location quality value & compare with last notification value
                        //
                        MessageNotificationLocation measureToReturn = this.RetrieveAirQualityIndex(location);

                        if ((measureToReturn != null) && (measureToReturn.QualityIndex != location.QualityIndex) && (measureToReturn.QualityIndex != 0))
                        {
                            //
                            // Update last quality index value
                            //
                            location.QualityIndex = measureToReturn.QualityIndex;

                            //
                            // Update timestamp
                            //
                            location.LastModified = measureToReturn.LastModified;

                            //
                            // Check threshold criteria for sending notifications
                            //
                            if (dLastMeasurementValue == 0.0)
                            {
                                fSendNotification = true;
                            }
                            else if (Math.Abs((decimal)((double)dAirMeasurementValue - dLastMeasurementValue)) >= (decimal)25.0)
                            {
                                fSendNotification = true;
                            }
                        }
                    }

                    if (fUpdated)
                    {
                        //
                        // Save updated notification (measurement & quality index)
                        //
                        if (SaveNotificationHandler != null)
                        {
                            this.SaveNotificationHandler(location.NotificationLocationRequestor, location);
                        }
                    }

                    if (fSendNotification)
                    {
                        //
                        // Load user's target locale
                        //
                        NotificationResourceMessages nrm = LoadResourceMessages(location.NotificationLocationRequestor.Culture);

                        //
                        // Send out notification to user.
                        //
                        SendMessage(ShortMessage.CreateShortMessage(location.NotificationLocationRequestor.MobileNumber, String.Format(nrm.NOTIFICATION_AIR_QUALITY, location.NotificationLocationRequestor.Name, location.Name, GetAirQualityDescription(nrm, location.QualityIndex), location.LastModified.ToString(nrm.Culture.DateTimeFormat))));

                    }
                    else
                    {
                        Trace.WriteLine(String.Format("Skipping notification to user [{0}] because new measurement was null or unchanged", location.NotificationLocationRequestor.MobileNumber));
                    }
                }
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.SendNotifications()");
        }


        public string GetAirQualityDescription(NotificationResourceMessages nrm, int iAirQualityIndex)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.GetAirQualityDescription()");

            string szAirQualityDescription = String.Format("[{0}]", iAirQualityIndex);

            switch (iAirQualityIndex)
            {
                case 0:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_0, iAirQualityIndex);
                    break;
                case 1:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_1, iAirQualityIndex);
                    break;
                case 2:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_2, iAirQualityIndex);
                    break;
                case 3:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_3, iAirQualityIndex);
                    break;
                case 4:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_4, iAirQualityIndex);
                    break;
                case 5:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_5, iAirQualityIndex);
                    break;
                case 6:
                    szAirQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_6, iAirQualityIndex);
                    break;
                default:
                    szAirQualityDescription = String.Format("[{0}]", iAirQualityIndex);
                    break;
            }
            
            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.GetAirQualityDescription()");

            return szAirQualityDescription;
        }

        public string GetWaterQualityDescription(NotificationResourceMessages nrm, int iWaterQualityIndex)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.GetWaterQualityDescription()");

            string szWaterQualityDescription = String.Format("[{0}]", iWaterQualityIndex);

            switch (iWaterQualityIndex)
            {
                case 1:
                    szWaterQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_2, iWaterQualityIndex);
                    break;
                case 2:
                    szWaterQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_3, iWaterQualityIndex);
                    break;
                case 3:
                    szWaterQualityDescription = String.Format("{0} [{1}]", nrm.AIR_QUALITY_INDEX_4, iWaterQualityIndex);
                    break;
                default:
                    szWaterQualityDescription = String.Format("[{0}]", iWaterQualityIndex);
                    break;
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.GetWaterQualityDescription()");

            return szWaterQualityDescription;
        }

        public bool SendMessage(ShortMessage msg)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.SendMessage()");

            bool fSent = false;

            try
            {
                //
                // If a Send Message Handler is available, use it to send outgoing message
                //
                if (SendMessageHandler != null)
                {
                    fSent = this.SendMessageHandler(msg);
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.SendMessage()");

            return fSent;
        }


        public bool UpdateNotificationTimeStamps(bool fForceUpdate)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.UpdateNotificationTimeStamps()");

            bool fSendNotifications = false;

            DateTime dtCurrentTime = DateTime.UtcNow;

            TimeSpan tsDelta = dtCurrentTime.Subtract(m_dtLastNotificationSent);

            try
            {
                //
                // Send Notifications at Fixed Time of Day
                //
                if (m_fUseNotificationTimeOfDay && (dtCurrentTime.CompareTo(m_dtNotificationAlignment) > 0))
                {
                    Trace.WriteLine(String.Format("EvaluateNotificationCriteria: (1) Met Fixed Time of Day Criteria for sending notifications: [{0}] TOD", m_dtNotificationAlignment.ToString()));
                    m_dtNotificationAlignment = m_dtNotificationAlignment.AddDays(1);
                    fSendNotifications = true;
                }


                //
                // Send Notifications On Periodic Basis
                //
                if (m_fUseNotificationInterval && (tsDelta.TotalMinutes >= m_tsNotificationInterval.TotalMinutes))
                {
                    Trace.WriteLine(String.Format("EvaluateNotificationCriteria: Met Interval Criteria for sending notifications: [{0}] min", tsDelta.TotalMinutes));
                    fSendNotifications = true;
                }

                if (fSendNotifications || fForceUpdate)
                {
                    m_dtLastNotificationSent = dtCurrentTime;

                    Trace.WriteLine(String.Format("Updating Last Send Notification TimeStamp to [{0}] from [{1}]", dtCurrentTime.ToString(), m_dtLastNotificationSent.ToString()));
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.UpdateNotificationTimeStamps()");

            return fSendNotifications;
        }



        public bool EvaluateNotificationCriteria()
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.EvaluateNotificationCriteria()");

            bool fSendNotifications = false;

            DateTime dtCurrentTime = DateTime.UtcNow;

            TimeSpan tsDelta = dtCurrentTime.Subtract(m_dtLastNotificationSent);

            try
            {
                //
                // Send Notifications at Fixed Time of Day
                //
                if (m_fUseNotificationTimeOfDay && (dtCurrentTime.CompareTo(m_dtNotificationAlignment) > 0))
                {
                    Trace.WriteLine(String.Format("EvaluateNotificationCriteria: (1) Met Fixed Time of Day Criteria for sending notifications: [{0}] TOD", m_dtNotificationAlignment.ToString()));
                    m_dtNotificationAlignment = m_dtNotificationAlignment.AddDays(1);
                    fSendNotifications = true;
                }


                //
                // Send Notifications On Periodic Basis
                //
                if (m_fUseNotificationInterval && (tsDelta.TotalMinutes >= m_tsNotificationInterval.TotalMinutes))
                {
                    Trace.WriteLine(String.Format("EvaluateNotificationCriteria: Met Interval Criteria for sending notifications: [{0}] min", tsDelta.TotalMinutes));
                    fSendNotifications = true;
                }

                if (fSendNotifications)
                {
                    m_dtLastNotificationSent = dtCurrentTime;

                    Trace.WriteLine(String.Format("Updating Last Send Notification TimeStamp to [{0}] from [{1}]", dtCurrentTime.ToString(), m_dtLastNotificationSent.ToString()));
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.EvaluateNotificationCriteria()");

            return fSendNotifications;
        }


        public void ProcessNotifications(ProcessNotificationsDelegate asyncProcessNotificationsHandler)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.ProcessNotifications()");

            try
            {
                Trace.WriteLine("EXAMINGING NOTIFICATIONS");

                //
                // Look for notifications by each country code (partition)
                //
                foreach (CountryCodeItem cci in m_CountryCodeTable.CountryCodes)
                {
                    foreach (string szCountryCode in cci.GetAllCountryCodes())
                    {
#if OLD_SCHOOL
                        //
                        // May need to be queueing these notifications rather than batching them if we get too many users...
                        //
                        List<MessageNotificationLocation> listActiveNotifications = null;

                        if (QueryNotificationsHandler != null)
                        {
                            listActiveNotifications = QueryNotificationsHandler(szCountryCode);
                        }

                        if ((listActiveNotifications != null) && (listActiveNotifications.Count > 0))
                        {
                            Trace.WriteLine("SENDING NOTIFICATIONS");

                            SendNotifications(listActiveNotifications);
                        }
#else
                        if (asyncProcessNotificationsHandler != null)
                        {
                            asyncProcessNotificationsHandler(szCountryCode);
                        }
                        else
                        {
                            ProcessNotificationsForCountryCode(szCountryCode);
                        }
#endif
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.ProcessNotifications()");
        }

        public bool ProcessNotificationsForCountryCode(string szCountryCode)
        {
            Trace.WriteLine(">> AirWatch.Notifications.NotificationEngine.ProcessNotificationsForCountryCode()");

            bool fSuccess = false;

            try
            {
                //
                // May need to be queueing these notifications rather than batching them if we get too many users...
                //
                List<MessageNotificationLocation> listActiveNotifications = null;

                if (QueryNotificationsHandler != null)
                {
                    listActiveNotifications = QueryNotificationsHandler(szCountryCode);
                }

                if ((listActiveNotifications != null) && (listActiveNotifications.Count > 0))
                {
                    Trace.WriteLine("SENDING NOTIFICATIONS");

                    SendNotifications(listActiveNotifications);
                }

                fSuccess = true;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            Trace.WriteLine("<< AirWatch.Notifications.NotificationEngine.ProcessNotificationsForCountryCode()");

            return fSuccess;
        }
    }




    public delegate IResourceManager LoadResourcesForLocale(string szCulture);
    public delegate bool CheckBlockedNumber(string szMobileNumber);
    public delegate bool AddBlockedNumber(string szMobileNumber, string szBlockMessage);
    public delegate MessageNotificationPerson LoadRegistration(string szMobileNumber);
    public delegate bool SaveRegistration(MessageNotificationPerson user);
    public delegate bool DeleteRegistration(MessageNotificationPerson user);
    public delegate List<MessageNotificationLocation> LoadNotifications(string szMobileNumber);
    public delegate bool SaveNotification(MessageNotificationPerson user, MessageNotificationLocation location);
    public delegate bool ClearAllNotifications(string szMobileNumber);
    public delegate List<MessageNotificationLocation> QueryNotifications(string szCountryCode);
    public delegate MessageNotificationLocation LocationResolver(string szLocation, string szCulture);
    public delegate decimal? RetrieveMeasurementData(decimal longitude, decimal latitude, string szMeasurementType);
    public delegate MessageNotificationLocation RetrieveAirQualityIndexDelegate(decimal longitude, decimal latitude);
    public delegate MessageNotificationLocation RetrieveWaterQualityIndexDelegate(decimal longitude, decimal latitude);
    public delegate bool SendMessageDelegate(ShortMessage msg);
    public delegate bool ProcessNotificationsDelegate(string szCountryCode);
}
