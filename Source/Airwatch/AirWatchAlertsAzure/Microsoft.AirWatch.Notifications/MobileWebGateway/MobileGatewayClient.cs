﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;


using System.Mobile.Messaging;
using System.Mobile.Contacts;
using Microsoft.Mobile.Messaging.Utils;

using System.Mobile.Messaging.Protocols;
using System.Mobile.Messaging.Web;


namespace Microsoft.AirWatch.Notifications
{
    public class MobileWebGatewayClient
    {
        public static bool SendMessage(ShortMessage msg)
        {
            bool fSent = false;

            if (msg.GetRawDestinationAddress().StartsWith("1"))
            {
                //
                // NOTE: Update source address for now to match account
                //
                msg.UpdateOriginationAddress("447624802848");

                fSent = SendMessageViaMWG(msg);
            }
            else
            {
                //
                // NOTE: Update source address for now to match account
                //
                msg.UpdateOriginationAddress("447786201106");

                fSent = Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml.Requests.RequestBuilder.SendMessage(msg.GetRawSourceAddress(), msg.GetRawDestinationAddress(), msg.ToString());
            }

            return fSent;
        }


        public static bool SendMessageViaMWG(ShortMessage msg)
        {
            bool fSent = false;

            try
            {
                //
                // Create Web Service Proxy
                //
                TextMessageService smsGateway = new TextMessageService();

#if true || PRODUCTION
                smsGateway.Url = "http://www.theredfort.net/MwsTextMessageService/TextMessageService.asmx";

                smsGateway.Credentials = new NetworkCredential("airwatch", "::bio9sphere::", "redfort");

                //ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(onValidateRemoteCertificate);

#else
                smsGateway.Url = "http://localhost/MwgSimulatorService/TextMessageService.asmx";
           
#endif

                //
                // Create a Short Message Submission Request
                //
                ShortMessageSubmitRequest submitRequest = new ShortMessageSubmitRequest(msg);

                //
                // Send the message to the gateway
                //
#if true || PRODUCTION
                ShortMessageSubmitResponse submitResponse = smsGateway.SubmitRequest(submitRequest);
#else
                Trace.WriteLine(String.Format("Would Send Message: Origination [{0}], Destination [{1}], Body [{2}]", msg.From, msg.To, msg.Message));
#endif

                fSent = true;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return fSent;
        }

        private static bool onValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;
        }
    }
}
