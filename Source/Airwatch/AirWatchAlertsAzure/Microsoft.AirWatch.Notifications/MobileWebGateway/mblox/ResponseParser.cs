﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.IO;

using System.Mobile.Messaging;

using Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml;

namespace Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml.Responses
{
    class ResponseParser
    {
        public static void Parse(string[] args)
        {
            //
            //
            //
            System.Xml.XmlDocument xd = new System.Xml.XmlDocument();

            xd.Load(args[1]);

            StringWriter sw = new StringWriter();

            xd.WriteContentTo(new XmlTextWriter(sw));

            string szInput = sw.ToString();

            //string szEncodedRequest = String.Format("XMLDATA={0}", System.Web.HttpUtility.UrlEncode(szInput));

            string szEncodedRequest = String.Format("{0}", System.Web.HttpUtility.UrlEncode(szInput));



            string szDecodedRequest = System.Web.HttpUtility.UrlDecode(szEncodedRequest);

            mbloxResponseServiceSerializer ms = new mbloxResponseServiceSerializer();

            ResponseService rs = ms.DeserializeFromString(szDecodedRequest);

            Debug.WriteLine(String.Format("Partner [{0}]", rs.Header.Partner.Text[0]));
            Debug.WriteLine(String.Format("ServiceID [{0}]", rs.Header.ServiceID.Text[0]));
            Debug.WriteLine(String.Format("Password [{0}]", rs.Header.Password.Text[0]));

            Debug.WriteLine(String.Format("rs.ResponseList[0].Destination.Text[0] = [{0}]", rs.ResponseList[0].Destination.Text[0]));
            Debug.WriteLine(String.Format("rs.ResponseList[0].OriginatingNumber.Text[0] = [{0}]", rs.ResponseList[0].OriginatingNumber.Text[0]));
            Debug.WriteLine(String.Format("rs.ResponseList[0].Data.Text[0] = [{0}]", rs.ResponseList[0].Data.Text[0]));

            ShortMessage msg = ShortMessage.CreateShortMessage(rs.ResponseList[0].OriginatingNumber.Text[0], rs.ResponseList[0].Destination.Text[0], rs.ResponseList[0].Data.Text[0]);
        }
    }

    public class mbloxResponseServiceSerializer : mbloxSerializerUtility<ResponseService>
    {
        public mbloxResponseServiceSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            m_knownTypes = new[] { typeof(ResponseService) };
        }
    }

}
