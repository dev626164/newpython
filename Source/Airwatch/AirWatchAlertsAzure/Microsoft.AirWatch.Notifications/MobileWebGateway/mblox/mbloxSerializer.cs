﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Reflection;

namespace Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml
{
    public class mbloxSerializerUtility<T>
    {
        private XmlSerializer m_xs;
        protected XmlSerializerNamespaces m_xn;
        protected Type[] m_knownTypes;

        public mbloxSerializerUtility()
        {
            Initialize();
        }

        private void Initialize()
        {
            m_xn = new XmlSerializerNamespaces();

            //
            // Add known configuration namespaces
            //
            RegisterNamespaces();
        }

        protected virtual void RegisterNamespaces()
        {
        }

        public T Deserialize(System.IO.Stream s)
        {
            XmlTextReader xtr = new XmlTextReader(s);

            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xtr, xde);

            xtr.Close();

            return er;
        }

        public virtual T Deserialize(XmlTextReader xtr)
        {
            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xtr, xde);

            xtr.Close();

            return er;
        }

        public virtual T Deserialize(XmlReader xr)
        {
            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xr, xde);

            xr.Close();

            return er;
        }

        protected virtual void OnUnknownElement(object o, XmlElementEventArgs xeea)
        {
            System.Diagnostics.Trace.WriteLine(String.Format("OnUnknownElement: [{0}]", xeea.Element.Name));
        }

        protected virtual void OnUnknownNode(object o, XmlNodeEventArgs xeea)
        {
            System.Diagnostics.Trace.WriteLine(String.Format("OnUnknownNode: [{0}]", xeea.Name));
        }

        public virtual void Serialize(XmlTextWriter xtw, T er)
        {
            this.Serializer.Serialize(xtw, er, m_xn);
        }

        public virtual void Serialize(XmlWriter xtw, T er)
        {
            this.Serializer.Serialize(xtw, er, m_xn);
        }

        public virtual String SerializeAsString(T msg)
        {
            String szXmlData = string.Empty;

            try
            {
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.NewLineHandling = NewLineHandling.Entitize;
                StringBuilder sb = new StringBuilder();
                XmlWriter xtw = XmlTextWriter.Create(sb, xws);
                this.Serialize(xtw, msg);
                szXmlData = sb.ToString();
                xtw.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return szXmlData;
        }


        public virtual T DeserializeFromString(String szXmlData)
        {
            T msg = default(T);

            try
            {
                StringReader sr = new StringReader(szXmlData);
                XmlTextReader xtr = new XmlTextReader(sr);
                msg = this.Deserialize(xtr);
                xtr.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return msg;
        }


        public XmlSerializerNamespaces Namespaces
        {
            get { return m_xn; }
        }

        public XmlSerializer Serializer
        {
            get
            {
                if (m_xs == null)
                {
                    m_xs = GetXmlSerializer();
                }
                return m_xs;
            }
        }


        //
        // Utility
        //
        public XmlSerializer GetXmlSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            XmlSerializer xs = new XmlSerializer(typeof(T), m_knownTypes);

            return xs;
        }

    }
}

