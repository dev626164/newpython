﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Web;
using System.Net;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

using Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml;

namespace Microsoft.AirWatch.Notifications.MobileWebGateway.mbloxClient.Xml.Requests
{
    class RequestBuilder
    {
        public static void Build(string[] args)
        {
            /*
             * 
                <?xml version="1.0"?>
                <NotificationRequest Version="3.5">
                <NotificationHeader>
                <PartnerName>UserName</PartnerName>
                <PartnerPassword>Password</PartnerPassword>
                </NotificationHeader>
                <NotificationList BatchID="1">
                <Notification SequenceNumber="1" MessageType="SMS">
                <Message>This is an MT message.</Message>
                <Profile>1234</Profile>
                <SenderID Type="Alpha">The_Sender</SenderID>
                <ExpireDate>03211100</ExpireDate>
                <Operator>99999</Operator>
                <Tariff>150</Tariff>
                <Subscriber>
                <SubscriberNumber>447900000000</SubscriberNumber>
                <SessionId>SessionID</SessionId>
                </Subscriber>
                <Tags>
                <Tag Name=”Number”>56</Tag>
                <Tag Name=”City”>Paris</Tag>
                </Tags>
                <ServiceDesc>ringtone1</ServiceDesc>
                <ContentType>3</ContentType>
                <ServiceId>5678</ServiceId>
                </Notification>
                </NotificationList>
             * 
             */

            string szUsername = "EEA";
            string szPassword = "cG9wb09r";
            string szProfileDirectPorted = "17057";
            string szProfileDirectPortedPlus = "17056";
            string szMessageText = "Test message through mblox";
            string szOriginatingNumber = "447786201106";
            string szDestinationNumber = "447624802848";


            NotificationRequest nr = new NotificationRequest();

            nr.Version = "3.5";

            //
            // Create mBLOX request header
            //
            nr.NotificationHeader = new NotificationHeader();
            nr.NotificationHeader.PartnerName = new PartnerName();
            nr.NotificationHeader.PartnerName.Text = new string[] { szUsername };
            nr.NotificationHeader.PartnerPassword = new PartnerPassword();
            nr.NotificationHeader.PartnerPassword.Text = new string[] { szPassword };



            nr.NotificationList = new NotificationList();
            nr.NotificationList.BatchID = "1";
            nr.NotificationList.Notification = new Notification[1];
            nr.NotificationList.Notification[0] = new Notification();
            nr.NotificationList.Notification[0].SequenceNumber = "1";
            nr.NotificationList.Notification[0].MessageType = NotificationMessageType.SMS;
            nr.NotificationList.Notification[0].MessageTypeSpecified = true;
            nr.NotificationList.Notification[0].Message = new Message();
            nr.NotificationList.Notification[0].Message.Text = new string[] { szMessageText };
            nr.NotificationList.Notification[0].Format = NotificationFormat.UTF8;
            nr.NotificationList.Notification[0].FormatSpecified = true;
            //nr.NotificationList.Notification[0].ContentType = new ContentType();
            //nr.NotificationList.Notification[0].ContentType.Text = new string[] { "ContentType" };
            //nr.NotificationList.Notification[0].ExpireDate = new ExpireDate();
            //nr.NotificationList.Notification[0].ExpireDate.Text = new string[] { DateTime.UtcNow.AddDays(1.0).ToString() };
            //nr.NotificationList.Notification[0].Operator = new Operator();
            //nr.NotificationList.Notification[0].Operator.Text = new string[] { "Operator" };
            nr.NotificationList.Notification[0].Profile = new Profile();
            nr.NotificationList.Notification[0].Profile.Text = new string[] { szProfileDirectPorted };
            nr.NotificationList.Notification[0].SenderID = new SenderID();
            nr.NotificationList.Notification[0].SenderID.Type = SenderIDType.Numeric;
            nr.NotificationList.Notification[0].SenderID.Text = new string[] { szOriginatingNumber };
            nr.NotificationList.Notification[0].Subscriber = new Subscriber[1];
            nr.NotificationList.Notification[0].Subscriber[0] = new Subscriber();
            //nr.NotificationList.Notification[0].Subscriber[0].SessionId = new SessionId();
            //nr.NotificationList.Notification[0].Subscriber[0].SessionId.Text = new string[] { "SessionId" };
            nr.NotificationList.Notification[0].Subscriber[0].SubscriberNumber = new SubscriberNumber();
            nr.NotificationList.Notification[0].Subscriber[0].SubscriberNumber.Text = new string[] { szDestinationNumber };
            //nr.NotificationList.Notification[0].Tags = new Tag[0];
            //nr.NotificationList.Notification[0].ServiceDesc = new ServiceDesc();
            //nr.NotificationList.Notification[0].ServiceDesc.Text = new string[] { "ServiceDesc" };
            //nr.NotificationList.Notification[0].ServiceId = new ServiceId();
            //nr.NotificationList.Notification[0].ServiceId.Text = new string[] { "ServiceId" };
            //nr.NotificationList.Notification[0].Tariff = new Tariff();
            //nr.NotificationList.Notification[0].Tariff.Text = new string[] { "Tariff" };











            mbloxNotificationRequestSerializer ms = new mbloxNotificationRequestSerializer();

            string szRequestMessage = ms.SerializeAsString(new NotificationRequestWrapper(nr));

            //string szRequestMessage = ms.SerializeAsString(nr);

            string szWrapperPattern = ".*<NotificationRequestWrapper [^>]*>(?<request>.*)</NotificationRequestWrapper>";

            string szRequest = System.Text.RegularExpressions.Regex.Replace(szRequestMessage, szWrapperPattern, "${request}", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            Debug.WriteLine(szRequestMessage);

            Debug.WriteLine(szRequest);

            Console.WriteLine(szRequestMessage);

            //
            //
            //
            string szEncodedRequest = System.Web.HttpUtility.UrlEncode(szRequest);

            Debug.WriteLine(String.Format("XMLDATA={0}", szEncodedRequest));

            //
            //
            //
            string szMbloxUri = "https://xml13.mblox.com:8443/send";


            //
            // Create a new HTTP Web Request 
            //
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(szMbloxUri);

            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(onValidateRemoteCertificate);

            req.Proxy = WebProxy.GetDefaultProxy();

            //
            // Set HTTP request parameters
            //
            req.ProtocolVersion = HttpVersion.Version11;
            req.Method = "POST";
            //req.Headers.Add("Cache-Control", szCacheControl);
            //req.Headers.Add("Accept-Encoding", szEncoding);

            //
            // Set the content type of the data being posted.
            //
            req.ContentType = "application/x-www-form-urlencoded";

            //
            // Add POST Data
            //
            byte[] rgbBuffer = new byte[4096];
            MemoryStream mStream = new MemoryStream(rgbBuffer, true);
            UTF8Encoding encoding = new UTF8Encoding();

            byte[] rgbEncodedRequest = encoding.GetBytes("XMLDATA=" + szEncodedRequest);
            mStream.Write(rgbEncodedRequest, 0, rgbEncodedRequest.Length);

            //
            // Set the content length of the string being posted.
            //
            req.ContentLength = mStream.Position;

            //
            // Write the memory strema out to the request stream
            //
            Stream reqStream = req.GetRequestStream();
            reqStream.Write(rgbBuffer, 0, (int)mStream.Position);

            Trace.WriteLine(" Closing HTTP Request stream...");

            //
            // Close the Stream object.
            //
            reqStream.Close();

            //
            // Now create the HTTP response object
            //
            HttpWebResponse resp = null;

            try
            {
                Trace.WriteLine(" Sending request and getting HTTP response...");
                resp = (HttpWebResponse)req.GetResponse();
            }
            catch (WebException e)
            {
                Trace.WriteLine(" HTTP Request failed: " + e.Response.ToString());
                resp = (HttpWebResponse)e.Response;
            }
            if (resp != null)
            {
                Trace.WriteLine("Status: " + ((int)resp.StatusCode) + "(" + resp.StatusDescription + ")");
                resp.Close();
            }
            else
            {
                Trace.WriteLine("Null response");
            }


        }

        public static bool SendMessage(string szOriginatingNumber, string szDestinationNumber, string szMessageText)
        {
            bool fSuccess = false;

            try
            {
                string szUsername = "EEA";
                string szPassword = "cG9wb09r";
                string szProfileDirectPorted = "17057";
                string szProfileDirectPortedPlus = "17056";
                string szProfile = szProfileDirectPortedPlus;

                //
                //Bosnia 
                //Croatia 
                //Montenegro 
                //Serbia 
                //Liechtenstein 
                //Albania 
                //Macedonia 
                //

                if (szDestinationNumber.StartsWith("387"))
                {
                    // Bosnia
                    szProfile = szProfileDirectPorted;
                }
                else if (szDestinationNumber.StartsWith("385"))
                {
                    // Croatia
                    szProfile = szProfileDirectPorted;
                }
                else if (szDestinationNumber.StartsWith("382"))
                {
                    // Montenegro
                    szProfile = szProfileDirectPorted;
                }
                else if (szDestinationNumber.StartsWith("381"))
                {
                    // Serbia
                    szProfile = szProfileDirectPorted;
                }
                else if (szDestinationNumber.StartsWith("423"))
                {
                    // Liechtenstein
                    szProfile = szProfileDirectPorted;
                }
                else if (szDestinationNumber.StartsWith("355"))
                {
                    // Albania
                    szProfile = szProfileDirectPorted;
                }
                else if (szDestinationNumber.StartsWith("389"))
                {
                    // Macedonia
                    szProfile = szProfileDirectPorted;
                }


                NotificationRequest nr = new NotificationRequest();

                nr.Version = "3.5";

                //
                // Create mBLOX request header
                //
                nr.NotificationHeader = new NotificationHeader();
                nr.NotificationHeader.PartnerName = new PartnerName();
                nr.NotificationHeader.PartnerName.Text = new string[] { szUsername };
                nr.NotificationHeader.PartnerPassword = new PartnerPassword();
                nr.NotificationHeader.PartnerPassword.Text = new string[] { szPassword };



                nr.NotificationList = new NotificationList();
                nr.NotificationList.BatchID = "1";
                nr.NotificationList.Notification = new Notification[1];
                nr.NotificationList.Notification[0] = new Notification();
                nr.NotificationList.Notification[0].SequenceNumber = "1";
                nr.NotificationList.Notification[0].MessageType = NotificationMessageType.SMS;
                nr.NotificationList.Notification[0].MessageTypeSpecified = true;
                nr.NotificationList.Notification[0].Message = new Message();
                nr.NotificationList.Notification[0].Message.Text = new string[] { szMessageText };
                nr.NotificationList.Notification[0].Format = NotificationFormat.UTF8;
                nr.NotificationList.Notification[0].FormatSpecified = true;
                //nr.NotificationList.Notification[0].ContentType = new ContentType();
                //nr.NotificationList.Notification[0].ContentType.Text = new string[] { "ContentType" };
                //nr.NotificationList.Notification[0].ExpireDate = new ExpireDate();
                //nr.NotificationList.Notification[0].ExpireDate.Text = new string[] { DateTime.UtcNow.AddDays(1.0).ToString() };
                //nr.NotificationList.Notification[0].Operator = new Operator();
                //nr.NotificationList.Notification[0].Operator.Text = new string[] { "Operator" };
                nr.NotificationList.Notification[0].Profile = new Profile();
                nr.NotificationList.Notification[0].Profile.Text = new string[] { szProfile };
                nr.NotificationList.Notification[0].SenderID = new SenderID();
                nr.NotificationList.Notification[0].SenderID.Type = SenderIDType.Numeric;
                nr.NotificationList.Notification[0].SenderID.Text = new string[] { szOriginatingNumber };
                nr.NotificationList.Notification[0].Subscriber = new Subscriber[1];
                nr.NotificationList.Notification[0].Subscriber[0] = new Subscriber();
                //nr.NotificationList.Notification[0].Subscriber[0].SessionId = new SessionId();
                //nr.NotificationList.Notification[0].Subscriber[0].SessionId.Text = new string[] { "SessionId" };
                nr.NotificationList.Notification[0].Subscriber[0].SubscriberNumber = new SubscriberNumber();
                nr.NotificationList.Notification[0].Subscriber[0].SubscriberNumber.Text = new string[] { szDestinationNumber };
                //nr.NotificationList.Notification[0].Tags = new Tag[0];
                //nr.NotificationList.Notification[0].ServiceDesc = new ServiceDesc();
                //nr.NotificationList.Notification[0].ServiceDesc.Text = new string[] { "ServiceDesc" };
                //nr.NotificationList.Notification[0].ServiceId = new ServiceId();
                //nr.NotificationList.Notification[0].ServiceId.Text = new string[] { "ServiceId" };
                //nr.NotificationList.Notification[0].Tariff = new Tariff();
                //nr.NotificationList.Notification[0].Tariff.Text = new string[] { "Tariff" };











                mbloxNotificationRequestSerializer ms = new mbloxNotificationRequestSerializer();

                string szRequestMessage = ms.SerializeAsString(new NotificationRequestWrapper(nr));

                //string szRequestMessage = ms.SerializeAsString(nr);

                string szWrapperPattern = ".*<NotificationRequestWrapper [^>]*>(?<request>.*)</NotificationRequestWrapper>";

                string szRequest = System.Text.RegularExpressions.Regex.Replace(szRequestMessage, szWrapperPattern, "${request}", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                Debug.WriteLine(szRequestMessage);

                Debug.WriteLine(szRequest);

                Console.WriteLine(szRequestMessage);

                //
                //
                //
                string szEncodedRequest = System.Web.HttpUtility.UrlEncode(szRequest);

                Debug.WriteLine(String.Format("XMLDATA={0}", szEncodedRequest));

                //
                //
                //
                string szMbloxUri = "https://xml13.mblox.com:8443/send";


                //
                // Create a new HTTP Web Request 
                //
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(szMbloxUri);

                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(onValidateRemoteCertificate);

                req.Proxy = WebProxy.GetDefaultProxy();

                //
                // Set HTTP request parameters
                //
                req.ProtocolVersion = HttpVersion.Version11;
                req.Method = "POST";
                //req.Headers.Add("Cache-Control", szCacheControl);
                //req.Headers.Add("Accept-Encoding", szEncoding);

                //
                // Set the content type of the data being posted.
                //
                req.ContentType = "application/x-www-form-urlencoded";

                //
                // Add POST Data
                //
                byte[] rgbBuffer = new byte[4096];
                MemoryStream mStream = new MemoryStream(rgbBuffer, true);
                UTF8Encoding encoding = new UTF8Encoding();

                byte[] rgbEncodedRequest = encoding.GetBytes("XMLDATA=" + szEncodedRequest);
                mStream.Write(rgbEncodedRequest, 0, rgbEncodedRequest.Length);

                //
                // Set the content length of the string being posted.
                //
                req.ContentLength = mStream.Position;

                //
                // Write the memory strema out to the request stream
                //
                Stream reqStream = req.GetRequestStream();
                reqStream.Write(rgbBuffer, 0, (int)mStream.Position);

                Trace.WriteLine(" Closing HTTP Request stream...");

                //
                // Close the Stream object.
                //
                reqStream.Close();

                //
                // Now create the HTTP response object
                //
                HttpWebResponse resp = null;

                try
                {
                    Trace.WriteLine(" Sending request and getting HTTP response...");
                    resp = (HttpWebResponse)req.GetResponse();
                }
                catch (WebException e)
                {
                    Trace.WriteLine(" HTTP Request failed: " + e.Response.ToString());
                    resp = (HttpWebResponse)e.Response;
                }
                if (resp != null)
                {
                    Trace.WriteLine("Status: " + ((int)resp.StatusCode) + "(" + resp.StatusDescription + ")");
                    fSuccess = (int)resp.StatusCode == 200;
                    resp.Close();
                }
                else
                {
                    Trace.WriteLine("Null response");
                }

            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return fSuccess;
        }

        private static bool onValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain,SslPolicyErrors policyErrors)
        {
#if true
            return true;
#else
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["IgnoreCertificateErrors"]))
            {
                return true;
            }
            else
            {
                return policyErrors == SslPolicyErrors.None;
            }
#endif
        }

    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = false)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class NotificationRequestWrapper
    {
        public NotificationRequest NotificationRequest { get; set; }

        public NotificationRequestWrapper()
        {
        }

        public NotificationRequestWrapper(NotificationRequest request)
        {
            this.NotificationRequest = request;
        }
    }


    public class mbloxNotificationRequestSerializer : mbloxSerializerUtility<NotificationRequestWrapper>
    {
        public mbloxNotificationRequestSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            m_knownTypes = new[] { typeof(NotificationRequestWrapper) };
        }
    }
}
