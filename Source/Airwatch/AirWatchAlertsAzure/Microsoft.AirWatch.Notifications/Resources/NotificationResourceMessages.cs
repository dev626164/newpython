﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Globalization;

namespace Microsoft.AirWatch.Notifications
{
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    public class NotificationResourceMessages
    {
        private IResourceManager m_ResourceManager;

        private global::System.Resources.ResourceManager resourceMan;

        private global::System.Globalization.CultureInfo resourceCulture;

        public NotificationResourceMessages(string szCulture)
        {
            IResourceManager temp = null;

            //
            // Internal backups
            //
            switch (szCulture)
            {
                case "en-US":
                    temp = new SystemResourceManager(Microsoft.AirWatch.Notifications.Resources.en_US.ResourceManager, new CultureInfo(szCulture));
                    break;
                case "en-GB":
                    temp = new SystemResourceManager(Microsoft.AirWatch.Notifications.Resources.en_GB.ResourceManager, new CultureInfo(szCulture));
                    break;
                case "fr-FR":
                    temp = new SystemResourceManager(Microsoft.AirWatch.Notifications.Resources.fr_FR.ResourceManager, new CultureInfo(szCulture));
                    break;
                case "de-DE":
                    temp = new SystemResourceManager(Microsoft.AirWatch.Notifications.Resources.de_DE.ResourceManager, new CultureInfo(szCulture));
                    break;
                case "es-ES":
                    temp = new SystemResourceManager(Microsoft.AirWatch.Notifications.Resources.es_ES.ResourceManager, new CultureInfo(szCulture));
                    break;
                default:
                    temp = new SystemResourceManager(Microsoft.AirWatch.Notifications.Resources.en_GB.ResourceManager, new CultureInfo("en-GB"));
                    break;
            }

            m_ResourceManager = temp;
        }

        public NotificationResourceMessages(IResourceManager resourceManager)
        {
            m_ResourceManager = resourceManager;
        }


        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    resourceMan = DefaultResourceManager.ResourceManager;
                }
                return resourceMan;
            }
        }

        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public global::System.Globalization.CultureInfo Culture
        {
            get
            {
                if (object.ReferenceEquals(resourceCulture, null))
                {
                    resourceCulture = DefaultResourceManager.Culture;
                }
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }


        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        private IResourceManager DefaultResourceManager
        {
            get { return m_ResourceManager; }
        }

        /// <summary>
        ///   Looks up a localized string similar to Very Good.
        /// </summary>
        public string AIR_QUALITY_INDEX_0
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_0", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Very Good.
        /// </summary>
        public string AIR_QUALITY_INDEX_1
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_1", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Good.
        /// </summary>
        public string AIR_QUALITY_INDEX_2
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_2", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Moderate.
        /// </summary>
        public string AIR_QUALITY_INDEX_3
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_3", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Bad.
        /// </summary>
        public string AIR_QUALITY_INDEX_4
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_4", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Very Bad.
        /// </summary>
        public string AIR_QUALITY_INDEX_5
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_5", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Very Hazardous.
        /// </summary>
        public string AIR_QUALITY_INDEX_6
        {
            get
            {
                return DefaultResourceManager.GetString("AIR_QUALITY_INDEX_6", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        public string COUNTRY_CODE_PREFIX
        {
            get
            {
                return DefaultResourceManager.GetString("COUNTRY_CODE_PREFIX", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to en-US.
        /// </summary>
        public string COUNTRY_DEFAULT_CULTURE
        {
            get
            {
                return DefaultResourceManager.GetString("COUNTRY_DEFAULT_CULTURE", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to We could not understand your air query request..
        /// </summary>
        public string ERROR_AIR_QUERY_NOT_UNDERSTOOD
        {
            get
            {
                return DefaultResourceManager.GetString("ERROR_AIR_QUERY_NOT_UNDERSTOOD", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to An error occured while processing your request..
        /// </summary>
        public string ERROR_EXCEPTION
        {
            get
            {
                return DefaultResourceManager.GetString("ERROR_EXCEPTION", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to We could not understand your water query request..
        /// </summary>
        public string ERROR_WATER_QUERY_NOT_UNDERSTOOD
        {
            get
            {
                return DefaultResourceManager.GetString("ERROR_WATER_QUERY_NOT_UNDERSTOOD", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to We could not understand your notification location request..
        /// </summary>
        public string ERROR_NOTIFY_REQUEST_NOT_UNDERSTOOD
        {
            get
            {
                return DefaultResourceManager.GetString("ERROR_NOTIFY_REQUEST_NOT_UNDERSTOOD", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Your message was not understood.
        /// </summary>
        public string ERROR_GENERIC
        {
            get
            {
                return DefaultResourceManager.GetString("ERROR_GENERIC", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^((air)|(water)|(register)|(accept)|(unsub)|(notify)|(location)|(stop)).
        /// </summary>
        public string LOCALIZED_COMMANDS
        {
            get
            {
                return DefaultResourceManager.GetString("LOCALIZED_COMMANDS", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, Estimated air quality at \&quot;{1}\&quot; was updated to the following conditions.\n\n{2}\n\n(as of {3} GMT/UTC).
        /// </summary>
        public string NOTIFICATION_AIR_QUALITY
        {
            get
            {
                return DefaultResourceManager.GetString("NOTIFICATION_AIR_QUALITY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^accept.
        /// </summary>
        public string REQUEST_ACCEPT
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_ACCEPT", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^add (?&lt;location&gt;.*).
        /// </summary>
        public string REQUEST_ADD
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_ADD", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^air (?&lt;location&gt;.*).
        /// </summary>
        public string REQUEST_AIR_QUERY
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_AIR_QUERY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^clear.*.
        /// </summary>
        public string REQUEST_CLEAR
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_CLEAR", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^unsub.*.
        /// </summary>
        public string REQUEST_DEREGISTER
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_DEREGISTER", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^help.*.
        /// </summary>
        public string REQUEST_HELP
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_HELP", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^location (?&lt;location&gt;.*).
        /// </summary>
        public string REQUEST_LOCATION
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_LOCATION", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^location.
        /// </summary>
        public string REQUEST_LOCATION_QUERY
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_LOCATION_QUERY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^notify (?&lt;location&gt;.*).
        /// </summary>
        public string REQUEST_NOTIFY
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_NOTIFY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^notify.
        /// </summary>
        public string REQUEST_NOTIFY_QUERY
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_NOTIFY_QUERY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^register (?&lt;userid&gt;[^ ]*).
        /// </summary>
        public string REQUEST_REGISTER
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_REGISTER", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^register.
        /// </summary>
        public string REQUEST_REGISTER_ANONYMOUS
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_REGISTER_ANONYMOUS", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^remove (?&lt;location&gt;.*).
        /// </summary>
        public string REQUEST_REMOVE
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_REMOVE", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^stop.*.
        /// </summary>
        public string REQUEST_STOP
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_STOP", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to ^water (?&lt;location&gt;.*).
        /// </summary>
        public string REQUEST_WATER_QUERY
        {
            get
            {
                return DefaultResourceManager.GetString("REQUEST_WATER_QUERY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your air query request for \&quot;{1}\&quot; was received..
        /// </summary>
        public string RESPONSE_AIR_QUERY
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_AIR_QUERY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your air query request was not successful.  We could not locate \&quot;{1}\&quot;..
        /// </summary>
        public string RESPONSE_AIR_QUERY_BAD_LOCATION
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_AIR_QUERY_BAD_LOCATION", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your air query request was not successful.  No air quality measurement data was found for &quot;{1}&quot;..
        /// </summary>
        public string RESPONSE_AIR_QUERY_NO_MEASUREMENT
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_AIR_QUERY_NO_MEASUREMENT", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your notification location was cleared.  You will not receive any further notifications until a new location is set..
        /// </summary>
        public string RESPONSE_CLEAR_REQUEST
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_CLEAR_REQUEST", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your current notification location is \&quot;{1}\&quot;..
        /// </summary>
        public string RESPONSE_CURRENT_NOTIFY_LOCATION
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_CURRENT_NOTIFY_LOCATION", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your mobile device is now deregistered.  You will need to register again to use this service..
        /// </summary>
        public string RESPONSE_DEREGISTERED
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_DEREGISTERED", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to The following commands are supported: air, water, register, accept, unsubscribe, notify, location, and stop.
        /// </summary>
        public string RESPONSE_HELP_MESSAGE
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_HELP_MESSAGE", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to You must first accept the terms of use before using this service.
        /// </summary>
        public string RESPONSE_MUST_ACCEPT_TERMS_OF_USE
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_MUST_ACCEPT_TERMS_OF_USE", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to You must first register to use this service.
        /// </summary>
        public string RESPONSE_MUST_FIRST_REGISTER
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_MUST_FIRST_REGISTER", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, no current notification location were found..
        /// </summary>
        public string RESPONSE_NO_CURRENT_LOCATION
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_NO_CURRENT_LOCATION", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your notification location was changed to \&quot;{1}\&quot;..
        /// </summary>
        public string RESPONSE_NOTIFY_LOCATION_CHANGED
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_NOTIFY_LOCATION_CHANGED", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your mobile device is registered.  To set a notification send \&quot;notify [location]\&quot;.  To unsubscribe, send \&quot;unsubscribe\&quot;.
        /// </summary>
        public string RESPONSE_DEFAULT_HELP_MESSAGE
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_DEFAULT_HELP_MESSAGE", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your mobile device is now registered.  To set a notification send \&quot;notify [location]\&quot;.
        /// </summary>
        public string RESPONSE_REGISTRATION_SUCCESS
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_REGISTRATION_SUCCESS", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your STOP request for this mobile device has been processed.  You not get any future messages from this service..
        /// </summary>
        public string RESPONSE_STOP_PROCESSED
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_STOP_PROCESSED", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}  The Terms and Conditions for use of this service are at {1}.
        /// </summary>
        public string RESPONSE_TERMS_AND_CONDITIONS
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_TERMS_AND_CONDITIONS", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your water query request for \&quot;{1}\&quot; was received..
        /// </summary>
        public string RESPONSE_WATER_QUERY
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_WATER_QUERY", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your water query request was not successful.  We could not locate \&quot;{1}\&quot;..
        /// </summary>
        public string RESPONSE_WATER_QUERY_BAD_LOCATION
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_WATER_QUERY_BAD_LOCATION", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, your water query request was not successful.  No water quality measurement data was found for &quot;{1}&quot;..
        /// </summary>
        public string RESPONSE_WATER_QUERY_NO_MEASUREMENT
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_WATER_QUERY_NO_MEASUREMENT", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to {0}, welcome to Eye On Earth.  Send the keyword \&quot;accept\&quot; if you agree to the terms of this service at http://eoe.eea.europa.eu/tos.
        /// </summary>
        public string RESPONSE_WELCOME_MESSAGE
        {
            get
            {
                return DefaultResourceManager.GetString("RESPONSE_WELCOME_MESSAGE", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to http://eyeonearth.cloudapp.net/tou.
        /// </summary>
        public string URL_TERMS_AND_CONDITIONS
        {
            get
            {
                return DefaultResourceManager.GetString("URL_TERMS_AND_CONDITIONS", resourceCulture);
            }
        }
    }


    public interface IResourceManager
    {
        string GetString(string name, CultureInfo culture);
        object GetObject(string name, CultureInfo culture);

        global::System.Resources.ResourceManager ResourceManager
        {
            get;
        }

        global::System.Globalization.CultureInfo Culture
        {
            get;
        }
    }

    public class SystemResourceManager : IResourceManager
    {
        private global::System.Resources.ResourceManager resourceMan;
        private global::System.Globalization.CultureInfo resourceCulture;

        public SystemResourceManager(global::System.Resources.ResourceManager rm, global::System.Globalization.CultureInfo rc)
        {
            resourceMan = rm;
            resourceCulture = rc;
        }

        #region IResourceManager Members

        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                return resourceMan;
            }
        }

        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public global::System.Globalization.CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }


        public string GetString(string name, CultureInfo culture)
        {
            return ResourceManager.GetString(name, culture);
        }

        public object GetObject(string name, CultureInfo culture)
        {
            return ResourceManager.GetObject(name, culture);
        }

        #endregion
    }

}

