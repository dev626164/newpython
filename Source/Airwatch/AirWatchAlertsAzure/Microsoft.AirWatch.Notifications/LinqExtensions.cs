﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Mobile.Messaging;

namespace Microsoft.Mobile.Messaging.Utils
{
    public static class LinqExtensions
    {
        public static string GetRawSourceAddress(this ShortMessage msg)
        {
            return msg.Header.Source.ToString(AddressFormatType.Bare);
        }

        public static string GetRawDestinationAddress(this ShortMessage msg)
        {
            return msg.Header.Destination.ToString(AddressFormatType.Bare);
        }

        public static string GetMessageTrim(this ShortMessage msg)
        {
            return msg.Message.Trim();
        }
    }
}
