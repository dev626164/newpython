﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;


using System.Mobile.Messaging;
using System.Mobile.Contacts;

namespace Microsoft.AirWatch.Notifications
{
    [Serializable]
    [DataContract(Name = "MessageNotificationLocation", Namespace = "http://schemas.microsoft.com/ws/2004/05/mws/sms")]
    [XmlType(TypeName = "MessageNotificationLocation", Namespace = "http://schemas.microsoft.com/ws/2004/05/mws/sms")]
    public class MessageNotificationLocation
    {
        private Guid m_Id;
        protected Uri m_Uri;
        protected MessageNotificationPerson m_NotificationLocRequestor;
        protected String m_szNotificationLocItemName;
        protected String m_szNotificationLocItemDescription;
        protected DateTime m_Created;
        protected DateTime m_LastModified;
        protected double m_Latitude;
        protected double m_Longitude;
        protected double m_MeasurementValue;
        protected int m_QualityIndex;
        protected String m_NotificationType;


        ShortMessage m_OriginalMessage;

        public MessageNotificationLocation()
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;
            m_szNotificationLocItemName = string.Empty;
            m_szNotificationLocItemDescription = string.Empty;
            m_Latitude = 0.0;
            m_Longitude = 0.0;
            m_MeasurementValue = 0.0;
            m_QualityIndex = 0;
            m_NotificationType = String.Empty;
        }

        public MessageNotificationLocation(string notificationType, string LocationName, double latitude, double longitude, double measurement, int qualityIndex, MessageNotificationPerson user)
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;
            m_NotificationType = notificationType;
            m_szNotificationLocItemName = LocationName;
            m_szNotificationLocItemDescription = string.Empty;
            m_Latitude = latitude;
            m_Longitude = longitude;
            m_MeasurementValue = measurement;
            m_QualityIndex = qualityIndex;
            m_NotificationLocRequestor = user;
        }

        public MessageNotificationLocation(string notificationType, string LocationName, string latitude, string longitude, string measurement, string qualityIndex, MessageNotificationPerson user)
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;
            m_NotificationType = notificationType;
            m_szNotificationLocItemName = LocationName;
            m_szNotificationLocItemDescription = string.Empty;
            m_NotificationLocRequestor = user;
            m_Latitude = 0.0;
            m_Longitude = 0.0;
            m_MeasurementValue = 0.0;
            m_QualityIndex = 0;
            Double.TryParse(latitude, out m_Latitude);
            Double.TryParse(longitude, out m_Longitude);
            Double.TryParse(measurement, out m_MeasurementValue);
            Int32.TryParse(qualityIndex, out m_QualityIndex);
        }

        public MessageNotificationLocation(ShortMessage OriginalMessage, MessageNotificationPerson bpLocRequestor)
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;


            m_OriginalMessage = OriginalMessage;
            m_NotificationLocRequestor = bpLocRequestor;

            this.m_szNotificationLocItemName = OriginalMessage.ToString();
            this.m_szNotificationLocItemDescription = OriginalMessage.ToString();

            m_Uri = m_NotificationLocRequestor.GetUri();
        }

        public MessageNotificationPerson NotificationLocationRequestor
        {
            get { return m_NotificationLocRequestor; }
            set { m_NotificationLocRequestor = value; }
        }

        public ShortMessage OriginalMessage
        {
            get { return m_OriginalMessage; }
            set { m_OriginalMessage = value; }
        }

        [DataMember(Name = "Id")]
        [XmlAttribute(AttributeName = "Id")]
        public Guid Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        [DataMember(Name = "Uri")]
        [XmlElement(ElementName = "Uri")]
        public String Uri
        {
            get { return m_Uri.AbsoluteUri; }
            set { m_Uri = new Uri(value as String); }
        }

        [DataMember(Name = "Name")]
        [XmlElement(ElementName = "Name")]
        public String Name
        {
            get { return m_szNotificationLocItemName; }
            set { m_szNotificationLocItemName = value; }
        }

        [DataMember(Name = "Description")]
        [XmlElement(ElementName = "Description")]
        public String Description
        {
            get { return m_szNotificationLocItemDescription; }
            set { m_szNotificationLocItemDescription = value; }
        }

        [DataMember(Name = "Created")]
        [XmlElement(ElementName = "Created")]
        public DateTime Created
        {
            get { return m_Created; }
            set { m_Created = value; }
        }

        [DataMember(Name = "LastModified")]
        [XmlElement(ElementName = "LastModified")]
        public DateTime LastModified
        {
            get { return m_LastModified; }
            set { m_LastModified = value; }
        }

        [DataMember(Name = "Latitude")]
        [XmlElement(ElementName = "Latitude")]
        public double Latitude
        {
            get { return m_Latitude; }
            set { m_Latitude = value; }
        }

        [DataMember(Name = "Longitude")]
        [XmlElement(ElementName = "Longitude")]
        public double Longitude
        {
            get { return m_Longitude; }
            set { m_Longitude = value; }
        }

        [DataMember(Name = "MeasurementValue")]
        [XmlElement(ElementName = "MeasurementValue")]
        public double MeasurementValue
        {
            get { return m_MeasurementValue; }
            set { m_MeasurementValue = value; }
        }

        [DataMember(Name = "QualityIndex")]
        [XmlElement(ElementName = "QualityIndex")]
        public int QualityIndex
        {
            get { return m_QualityIndex; }
            set { m_QualityIndex = value; }
        }

        [DataMember(Name = "NotificationType")]
        [XmlElement(ElementName = "NotificationType")]
        public String NotificationType
        {
            get { return m_NotificationType; }
            set { m_NotificationType = value; }
        }
        
        public Uri GetUri()
        {
            return m_Uri;
        }
    }
}
