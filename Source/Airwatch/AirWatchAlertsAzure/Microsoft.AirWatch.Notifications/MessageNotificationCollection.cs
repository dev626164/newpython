﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using System.Mobile.Messaging;
using System.Mobile.Contacts;

namespace Microsoft.AirWatch.Notifications
{
    public class MessageNotificationCollection
    {
        private MessageNotificationPerson m_NotificationOwner;
        private List<MessageNotificationLocation> m_NotificationLocations;

        public MessageNotificationCollection()
        {
            m_NotificationLocations = new List<MessageNotificationLocation>();
        }

        public MessageNotificationCollection(MessageNotificationPerson NotificationOwner)
        {
            m_NotificationOwner = NotificationOwner;
            m_NotificationLocations = new List<MessageNotificationLocation>();
        }

        public MessageNotificationPerson NotificationOwner
        {
            get { return m_NotificationOwner; }
            set { m_NotificationOwner = value; }
        }

        public List<MessageNotificationLocation> NotificationLocations
        {
            get { return m_NotificationLocations; }
            set { m_NotificationLocations = value; }
        }
    }
}
