﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Reflection;

namespace Microsoft.AirWatch.Notifications
{

    public class CountryCodeTableFactory
    {
        private const string DEFAULT_TABLE_RESOURCE_NAME = "Microsoft.AirWatch.Notifications.Resources.CountryCodes.xml";

        public CountryCodeTableFactory()
        {
            CountryCodeTableSerializer ccts = new CountryCodeTableSerializer();

            //
            // Load and deserialize country code table
            //
            this.CountryCodeTable = ccts.Deserialize(this.DefaultPropertyResourceStream);

            //
            // Sort the table
            //
            if (this.CountryCodeTable != null)
            {
                this.CountryCodeTable.SortCountryCodeTable();
            }
        }


        public static CountryCodeTable LoadCountryCodeTable()
        {
            try
            {
                CountryCodeTableFactory cctf = new CountryCodeTableFactory();

                return cctf.CountryCodeTable;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
            return null;
        }

        [DataMember(Name = "CountryCodes")]
        [XmlArray(ElementName = "CountryCodes")]
        public CountryCodeTable CountryCodeTable;

        private System.IO.Stream DefaultPropertyResourceStream
        {
            get
            {
                return this.DefaultDriverAssembly.GetManifestResourceStream(DEFAULT_TABLE_RESOURCE_NAME);
            }
        }

        private System.Reflection.Assembly DefaultDriverAssembly
        {
            get
            {
                return Assembly.GetExecutingAssembly();
            }
        }
    }


    [Serializable]
    [DataContract(Name = "CountryCodeTable")]
    [XmlType(TypeName = "CountryCodeTable")]
    public class CountryCodeTable
    {
        private CountryCodeZone[] m_CountryCodeZones;

        public CountryCodeTable()
        {
        }

        public CountryCodeItem[] CountryCodes;


        public void SortCountryCodeTable()
        {
            if (this.CountryCodes != null)
            {
                List<CountryCodeItem> listCountryCodes = new List<CountryCodeItem>(this.CountryCodes);

                //
                // Sort country code list by length, then by numeric order
                //
                listCountryCodes.Sort(CompareCountryCodeByLength);

                this.CountryCodes = listCountryCodes.ToArray();
            }
        }

        public string FindLocaleForNumber(string szMobileNumber)
        {
            string szMessageLocale = null;

            try
            {
                if (this.CountryCodes != null)
                {
                    for (int i = 0; i < this.CountryCodes.Length; i++)
                    {
                        string[] rgszCountryCodes = this.CountryCodes[i].CountryCode.Split(new char[] { ',' });

                        for (int j = 0; j < rgszCountryCodes.Length; j++)
                        {
                            if (szMobileNumber.StartsWith(rgszCountryCodes[j]))
                            {
                                szMessageLocale = this.CountryCodes[i].CountryLocale;

                                Trace.WriteLine(String.Format("Matched Mobile Number [{0}] ==> CountryCode [{1}], Country [{2}], Locale [{3}]", szMobileNumber, rgszCountryCodes[j], this.CountryCodes[i].CountryName, this.CountryCodes[i].CountryLocale));
                                break;
                            }
                        }
                        if (szMessageLocale != null)
                        {
                            break;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return szMessageLocale;
        }

        public string FindLocaleForNumberByZone(string szMobileNumber)
        {
            bool localeFound = false;
            string szMessageLocale = null;

            try
            {
                if (m_CountryCodeZones != null)
                {
                    int iZone = (int)szMobileNumber[0] - (int)'0';

                    CountryCodeZone zone = m_CountryCodeZones[iZone];

                    for (int i = 0; (!localeFound) && (i < zone.CountryCodes.Count); i++)
                    {
                        string[] rgszCountryCodes = zone.CountryCodes[i].CountryCode.Split(new char[] { ',' });

                        for (int j = 0; (!localeFound) && (j < rgszCountryCodes.Length); j++)
                        {
                            if (szMobileNumber.StartsWith(rgszCountryCodes[j]))
                            {
                                szMessageLocale = zone.CountryCodes[i].CountryLocale;

                                Trace.WriteLine(String.Format("Matched Mobile Number [{0}] ==> Zone [{1}], CountryCode [{2}], Country [{3}], Locale [{4}]", szMobileNumber, iZone.ToString(), rgszCountryCodes[j], zone.CountryCodes[i].CountryName, zone.CountryCodes[i].CountryLocale));
                                localeFound = true;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return szMessageLocale;
        }

        public string FindCountryCodeForNumberByZone(string szMobileNumber)
        {
            bool countryCodeFound = false;
            string szCountryCode = null;

            try
            {
                if (m_CountryCodeZones != null)
                {
                    int iZone = (int)szMobileNumber[0] - (int)'0';

                    CountryCodeZone zone = m_CountryCodeZones[iZone];

                    for (int i = 0; (!countryCodeFound) && (i < zone.CountryCodes.Count); i++)
                    {
                        string[] rgszCountryCodes = zone.CountryCodes[i].CountryCode.Split(new char[] { ',' });

                        for (int j = 0; (!countryCodeFound) && (j < rgszCountryCodes.Length); j++)
                        {
                            if (szMobileNumber.StartsWith(rgszCountryCodes[j]))
                            {
                                szCountryCode = rgszCountryCodes[j];

                                Trace.WriteLine(String.Format("Matched Mobile Number [{0}] ==> Zone [{1}], CountryCode [{2}], Country [{3}], Locale [{4}]", szMobileNumber, iZone.ToString(), rgszCountryCodes[j], zone.CountryCodes[i].CountryName, zone.CountryCodes[i].CountryLocale));
                                countryCodeFound = true;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return szCountryCode;
        }


        public void InitializeZoneTable()
        {
            try
            {
                if (this.CountryCodes != null)
                {
                    m_CountryCodeZones = new CountryCodeZone[10];

                    for (int i = 0; i < this.m_CountryCodeZones.Length; i++)
                    {
                        m_CountryCodeZones[i] = new CountryCodeZone(i);
                    }

                    for (int i = 0; i < this.CountryCodes.Length; i++)
                    {
                        int iZone = (int)this.CountryCodes[i].CountryCode[0] - (int)'0';

                        m_CountryCodeZones[iZone].CountryCodes.Add(this.CountryCodes[i]);
                    }




                    for (int i = 0; i < this.m_CountryCodeZones.Length; i++)
                    {
                        CountryCodeZone zone = m_CountryCodeZones[i];

#if FULL_DEBUG_TRACING
                        for (int j = 0; j < zone.CountryCodes.Count; j++)
                        {
                            Trace.WriteLine(String.Format("Zone [{0}], CountryCode [{1}], Country [{2}], Locale [{3}]", i.ToString(), zone.CountryCodes[j].CountryCode, zone.CountryCodes[j].CountryName, zone.CountryCodes[j].CountryLocale));
                        }
#endif

                        //
                        // Sort country code list for this zone by length, then by numeric order
                        //
                        m_CountryCodeZones[i].CountryCodes.Sort(CompareCountryCodeByLength);

#if true || FULL_DEBUG_TRACING
                        for (int j = 0; j < zone.CountryCodes.Count; j++)
                        {
                            Trace.WriteLine(String.Format("Zone [{0}], CountryCode [{1}], Country [{2}], Locale [{3}]", i.ToString(), zone.CountryCodes[j].CountryCode, zone.CountryCodes[j].CountryName, zone.CountryCodes[j].CountryLocale));
                        }
#endif
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
        }

        //
        // Helper Class
        //
        internal class CountryCodeZone
        {
            internal CountryCodeZone(int iZone)
            {
                Zone = iZone;
                CountryCodes = new List<CountryCodeItem>();
            }

            public int Zone;

            public List<CountryCodeItem> CountryCodes;
        }

        private static int CompareCountryCodeByLength(CountryCodeItem x, CountryCodeItem y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (y == null)
                {
                    return 1;
                }
                else
                {
                    return CompareStringByLength(x.CountryCode, y.CountryCode);
                }
            }
        }

        private static int CompareStringByLength(string x, string y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (y == null)
                {
                    return 1;
                }
                else
                {
                    int retval = x.Length.CompareTo(y.Length);

                    if (retval != 0)
                    {
                        return retval;
                    }
                    else
                    {
                        return x.CompareTo(y);
                    }
                }
            }
        }
    }



    [Serializable]
    [DataContract(Name = "Country")]
    [XmlType(TypeName = "Country")]
    public class CountryCodeItem
    {
        private String m_szCountryName;
        private String m_szCountryCode;
        private String m_szCountryLocale;

        public CountryCodeItem()
        {
        }

        [DataMember(Name = "Name")]
        [XmlElement(ElementName = "Name")]
        public String CountryName
        {
            get { return m_szCountryName; }
            set { m_szCountryName = value; }
        }

        [DataMember(Name = "CountryCode")]
        [XmlElement(ElementName = "CountryCode")]
        public String CountryCode
        {
            get { return m_szCountryCode; }
            set { m_szCountryCode = value; }
        }

        [DataMember(Name = "Locale")]
        [XmlElement(ElementName = "Locale")]
        public String CountryLocale
        {
            get { return m_szCountryLocale; }
            set { m_szCountryLocale = value; }
        }

        public String[] GetAllCountryCodes()
        {
            return this.CountryCode.Split(new char[] { ',' });
        }
    }



    public class CountryCodeTableSerializer : CountryCodeSerializer<CountryCodeTable>
    {
        public CountryCodeTableSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            m_knownTypes = new[] { typeof(CountryCodeTable) };
        }
    }


    public class CountryCodeSerializer<T>
    {
        private XmlSerializer m_xs;
        protected XmlSerializerNamespaces m_xn;
        protected Type[] m_knownTypes;

        public CountryCodeSerializer()
        {
            Initialize();
        }

        private void Initialize()
        {
            m_xn = new XmlSerializerNamespaces();

            //
            // Add known configuration namespaces
            //
            RegisterNamespaces();
        }

        protected virtual void RegisterNamespaces()
        {
            m_xn.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            m_xn.Add("smap", "http://www.smsforum.net/schemas/smap/v1.0");
            m_xn.Add("ns", "http://www.w3.org/XML/1998/namespace");
        }

        public T Deserialize(System.IO.Stream s)
        {
            XmlTextReader xtr = new XmlTextReader(s);

            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xtr, xde);

            xtr.Close();

            return er;
        }

        public virtual T Deserialize(XmlTextReader xtr)
        {
            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xtr, xde);

            xtr.Close();

            return er;
        }

        public virtual T Deserialize(XmlReader xr)
        {
            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xr, xde);

            xr.Close();

            return er;
        }

        protected virtual void OnUnknownElement(object o, XmlElementEventArgs xeea)
        {
            System.Diagnostics.Trace.WriteLine(String.Format("OnUnknownElement: [{0}]", xeea.Element.Name));
        }

        protected virtual void OnUnknownNode(object o, XmlNodeEventArgs xeea)
        {
            System.Diagnostics.Trace.WriteLine(String.Format("OnUnknownNode: [{0}]", xeea.Name));
        }

        public virtual void Serialize(XmlTextWriter xtw, T er)
        {
            this.Serializer.Serialize(xtw, er, m_xn);
        }

        public virtual void Serialize(XmlWriter xtw, T er)
        {
            this.Serializer.Serialize(xtw, er, m_xn);
        }

        public virtual String SerializeAsString(T msg)
        {
            String szXmlData = string.Empty;

            try
            {
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.NewLineHandling = NewLineHandling.Entitize;
                StringBuilder sb = new StringBuilder();
                XmlWriter xtw = XmlTextWriter.Create(sb, xws);
                this.Serialize(xtw, msg);
                szXmlData = sb.ToString();
                xtw.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return szXmlData;
        }


        public virtual T DeserializeFromString(String szXmlData)
        {
            T msg = default(T);

            try
            {
                StringReader sr = new StringReader(szXmlData);
                XmlTextReader xtr = new XmlTextReader(sr);
                msg = this.Deserialize(xtr);
                xtr.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return msg;
        }


        public XmlSerializerNamespaces Namespaces
        {
            get { return m_xn; }
        }

        public XmlSerializer Serializer
        {
            get
            {
                if (m_xs == null)
                {
                    m_xs = GetXmlSerializer();
                }
                return m_xs;
            }
        }


        //
        // Utility
        //
        public XmlSerializer GetXmlSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            XmlSerializer xs = new XmlSerializer(typeof(T), m_knownTypes);

            return xs;
        }

    }

}

