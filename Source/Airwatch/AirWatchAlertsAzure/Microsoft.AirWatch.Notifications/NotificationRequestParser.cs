﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using System.Mobile.Messaging;
using System.Mobile.Contacts;
using Microsoft.Mobile.Messaging.Utils;

namespace Microsoft.AirWatch.Notifications
{
    public class NotificationRequestParser
    {
        private NotificationResourceMessages m_ResourceMessages;

        public NotificationRequestParser(NotificationResourceMessages nrm)
        {
            m_ResourceMessages = nrm;
        }

        public ShortMessage ProcessNewNotificationRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = null;

            try
            {
                //
                // Check if STOP request
                //
                if (NotificationEngine.GetCurrentEngine().IsMobileNumberBlocked(msg.GetRawSourceAddress()))
                {
                    Trace.WriteLine(String.Format("WARNING: Received Multiple STOP messages from mobile subscriber [{0}]", msg.GetRawSourceAddress()));
                    Trace.WriteLine(String.Format("STOP MESSAGE: Origination [{0}], Destination [{1}], Body [{2}]", msg.GetRawSourceAddress(), msg.To, msg.Message));
                }
                else if (IsStopRequest(msg))
                {
                    //
                    // Deregister user if necessary
                    //
                    msgResponse = HandleStopRequest(msg);
                }
                else
                {
                    msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

                    //
                    // Check if incoming mobile number is registered before proceeding
                    //
                    if (!NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                    {
                        //
                        // Commands available for non registered users
                        //
                        if (IsAirRequest(msg))
                        {
                            msgResponse = HandleAirRequest(msg);
                        }
                        else if (IsWaterRequest(msg))
                        {
                            msgResponse = HandleWaterRequest(msg);
                        }
                        else if (IsRegisterRequest(msg))
                        {
                            msgResponse = HandleRegisterRequest(msg);
                        }
#if ENABLE_NOTIFICATION_SERVICE
                        else if (IsNotifyQueryRequest(msg) || IsNotifyQueryRequest(msg))
                        {
#if true || REGISTRATION_NOT_REQUIRED
                            msgResponse = HandleNotifyRequest(msg);
#else
                            msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.RESPONSE_MUST_FIRST_REGISTER);
#endif
                        }
                        else if (IsClearRequest(msg))
                        {
#if true || REGISTRATION_NOT_REQUIRED
                            msgResponse = HandleClearRequest(msg);
#else
                            msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.RESPONSE_MUST_FIRST_REGISTER);
#endif
                        }
#endif
                        else
                        {
                            //
                            // Default send help text
                            //
                            msgResponse = HandleDefaultRequest(msg);
                        }
                    }
                    else
                    {
                        //
                        // Check if terms of use have been accepted
                        //
                        if (!NotificationEngine.GetCurrentEngine().IsTermsOfUseAccepted(msg.GetRawSourceAddress()))
                        {
                            //
                            // Commands available for non TOU accpeted users
                            //
                            if (IsAcceptRequest(msg))
                            {
                                msgResponse = HandleAcceptRequest(msg);
                            }
                            else
                            {
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.RESPONSE_MUST_ACCEPT_TERMS_OF_USE);
                            }
                        }
                        else
                        {
                            //
                            // Check commands from registered users
                            //
                            if (IsAirRequest(msg))
                            {
                                msgResponse = HandleAirRequest(msg);
                            }
                            else if (IsWaterRequest(msg))
                            {
                                msgResponse = HandleWaterRequest(msg);
                            }
#if ENABLE_NOTIFICATION_SERVICE
                            else if (IsNotifyQueryRequest(msg) || IsNotifyQueryRequest(msg))
                            {
                                msgResponse = HandleNotifyRequest(msg);
                            }
                            else if (IsClearRequest(msg))
                            {
                                msgResponse = HandleClearRequest(msg);
                            }
#endif
                            else if (IsDeRegisterRequest(msg))
                            {
                                msgResponse = HandleDeRegisterRequest(msg);
                            }
                            else
                            {
                                //
                                // Default send help text
                                //
                                msgResponse = HandleDefaultRequest(msg);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);
            }

            return msgResponse;
        }


        #region DEFAULT REQUEST
        public  ShortMessage HandleDefaultRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            try
            {
                //
                // Lookup user to use friendly name or devicenumber
                //
                string szDisplayName = msg.GetRawSourceAddress();

                if (NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                {
                    MessageNotificationPerson mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());

                    if (mbpUser != null)
                    {
                        szDisplayName = mbpUser.Name;
                    }
                }

                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_DEFAULT_HELP_MESSAGE, szDisplayName));
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return msgResponse;
        }
#endregion

        #region REGISTER REQUEST
        public  bool IsRegisterRequest(ShortMessage msg)
        {
            return (Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_REGISTER, RegexOptions.IgnoreCase) || Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_REGISTER_ANONYMOUS, RegexOptions.IgnoreCase));
        }

        public  ShortMessage HandleRegisterRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            MessageNotificationCollection msgNotification = null;

            if (IsRegisterRequest(msg))
            {
                try
                {
                    string szUserTag = Regex.Replace(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_REGISTER, "${userid}", RegexOptions.IgnoreCase);

                    if (String.IsNullOrEmpty(szUserTag) || szUserTag.ToLower() == "register")
                    {
                        szUserTag = msg.GetRawSourceAddress();
                    }

                    string szUserDisplayName = szUserTag;

                    if (!String.IsNullOrEmpty(szUserTag))
                    {
                        if (String.IsNullOrEmpty(szUserDisplayName))
                        {
                            szUserDisplayName = szUserTag;
                        }

                        //
                        // Create new user
                        //
                        MessageNotificationPerson mbpOwner = new MessageNotificationPerson(msg.GetRawSourceAddress(), szUserTag, szUserDisplayName);

                        //
                        // Save current culture properties
                        //
                        mbpOwner.Culture = this.m_ResourceMessages.COUNTRY_DEFAULT_CULTURE;

                        //
                        // Register New User
                        //
                        msgNotification = NotificationEngine.GetCurrentEngine().RegisterNewUser(mbpOwner);

                        if (msgNotification != null)
                        {
                            msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_WELCOME_MESSAGE, msgNotification.NotificationOwner.Name));
                        }
                    }

                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
#endregion

        #region ACCEPT REQUEST
        public  bool IsAcceptRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_ACCEPT, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleAcceptRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsAcceptRequest(msg))
            {
                try
                {
                    //
                    // Currently have to have registered in order to accept TOU
                    //
                    MessageNotificationPerson mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());

                    if (mbpUser != null)
                    {
                        //
                        // Save accept TOU status
                        //
                        mbpUser.TermsOfUseAccepted = true;

                        //
                        // Save updated registration with TOU
                        //
                        NotificationEngine.GetCurrentEngine().UpdateRegistration(mbpUser);

                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_REGISTRATION_SUCCESS, mbpUser.Name));
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion

        #region DEREGISTER REQUEST
        public  bool IsDeRegisterRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_DEREGISTER, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleDeRegisterRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsDeRegisterRequest(msg))
            {
                try
                {
                    //
                    // Currently have to have registered in order to deregister
                    //
                    MessageNotificationPerson mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());

                    if (mbpUser != null)
                    {
                        //
                        // Save accept TOU status
                        //
                        mbpUser.TermsOfUseAccepted = false;

                        //
                        // Remove user
                        //
                        NotificationEngine.GetCurrentEngine().DeRegisterExistingUser(mbpUser);

                        //
                        // Return response
                        //
                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_DEREGISTERED, mbpUser.Name));
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion

        #region STOP REQUEST
        public  bool IsStopRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_STOP, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleStopRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsStopRequest(msg))
            {
                try
                {
                    //
                    // Save mobile number in blocked list
                    //
                    NotificationEngine.GetCurrentEngine().BlockMobileNumber(msg.GetRawSourceAddress(), msg.ToString());

                    //
                    // If registered, make sure we deregister user
                    //
                    if (!NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                    {
                        MessageNotificationPerson mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());

                        if (mbpUser != null)
                        {
                            //
                            // Save accept TOU status
                            //
                            mbpUser.TermsOfUseAccepted = false;

                            //
                            // Remove user
                            //
                            NotificationEngine.GetCurrentEngine().DeRegisterExistingUser(mbpUser);
                        }
                    }

                    //
                    // Return response
                    //
                    msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_STOP_PROCESSED, msg.GetRawSourceAddress()));
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion

        #region NOTIFY REQUEST
        public  bool IsNotifyRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_NOTIFY, RegexOptions.IgnoreCase);
        }
        public  bool IsNotifyQueryRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_NOTIFY_QUERY, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleNotifyRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsNotifyRequest(msg) || IsNotifyQueryRequest(msg))
            {
                try
                {
                    if (IsNotifyRequest(msg))
                    {
                        MessageNotificationPerson mbpUser = null;
                        bool fSendTermsAndConditions = false;

                        if (NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                        {
                            mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());
                        }

#if true || REGISTRATION_NOT_REQUIRED
                        if (mbpUser == null)
                        {
                            //
                            // Create new user
                            //
                            mbpUser = new MessageNotificationPerson(msg.GetRawSourceAddress(), msg.GetRawSourceAddress(), msg.GetRawSourceAddress(), m_ResourceMessages.COUNTRY_DEFAULT_CULTURE, bool.TrueString);

                            //
                            // Save current culture properties
                            //
                            mbpUser.Culture = this.m_ResourceMessages.COUNTRY_DEFAULT_CULTURE;

                            //
                            // Auto-registration, TOU accepted
                            //
                            mbpUser.TermsOfUseAccepted = true;

                            //
                            // Register New User
                            //
                            MessageNotificationCollection msgNotification = NotificationEngine.GetCurrentEngine().RegisterNewUser(mbpUser);

                            if (msgNotification != null)
                            {
                                fSendTermsAndConditions = true;
                            }
                        }
#endif

                        if (mbpUser != null)
                        {
                            string szLocation = Regex.Replace(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_NOTIFY, "${location}", RegexOptions.IgnoreCase);

                            if (!String.IsNullOrEmpty(szLocation) && !String.IsNullOrEmpty(szLocation.Trim()))
                            {
                                szLocation = szLocation.Trim();

                                //
                                // Create a new Notification Location from this Text Message
                                //
                                MessageNotificationLocation location = NotificationEngine.GetCurrentEngine().ResolveLocation(szLocation, m_ResourceMessages.COUNTRY_DEFAULT_CULTURE);

                                if (location != null)
                                {
                                    //
                                    // Now perform a lookup on the air quality index for this location to ensure we have data
                                    //
                                    MessageNotificationLocation measureToReturn = NotificationEngine.GetCurrentEngine().RetrieveAirQualityIndex(location);

                                    if (measureToReturn != null)
                                    {
                                        //
                                        // Save original message
                                        //
                                        location.OriginalMessage = msg;

                                        //
                                        // Add location to notification collection for this user
                                        //
                                        NotificationEngine.GetCurrentEngine().AddMessageToOwnerNotification(mbpUser, location);

                                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_NOTIFY_LOCATION_CHANGED, mbpUser.Name, location.Name));

                                        if (fSendTermsAndConditions)
                                        {
                                            msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_TERMS_AND_CONDITIONS, msgResponse.ToString(), m_ResourceMessages.URL_TERMS_AND_CONDITIONS));
                                        }
                                    }
                                    else
                                    {
                                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_AIR_QUERY_NO_MEASUREMENT, mbpUser.Name, location.Name));
                                    }
                                }
                                else
                                {
                                    msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.ERROR_NOTIFY_REQUEST_NOT_UNDERSTOOD));
                                }
                            }
                            else
                            {
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.ERROR_NOTIFY_REQUEST_NOT_UNDERSTOOD));
                            }
                        }
                    }
                    else if (IsNotifyQueryRequest(msg))
                    {
                        MessageNotificationPerson mbpUser = null;

                        if (NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                        {
                            mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());
                        }

#if true || REGISTRATION_NOT_REQUIRED
                        if (mbpUser == null)
                        {
                            mbpUser = new MessageNotificationPerson(msg.GetRawSourceAddress(), msg.GetRawSourceAddress(), msg.GetRawSourceAddress(), m_ResourceMessages.COUNTRY_DEFAULT_CULTURE, bool.TrueString);
                        }
#endif
                        if (mbpUser != null)
                        {
                            //
                            // Lookup current notification collection
                            //
                            MessageNotificationCollection mnc = NotificationEngine.GetCurrentEngine().GetNotificationByMobileNumber(mbpUser.MobileNumber);

                            if ((mnc != null) && (mnc.NotificationLocations.Count > 0))
                            {
                                //
                                // Return Current Location
                                //
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_CURRENT_NOTIFY_LOCATION, mbpUser.Name, mnc.NotificationLocations[0].Name));
                            }
                            else
                            {
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_NO_CURRENT_LOCATION, mbpUser.Name));
                            }
                        }

                    }

                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion

        #region CLEAR REQUEST
        public  bool IsClearRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_CLEAR, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleClearRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsClearRequest(msg))
            {
                try
                {
                    MessageNotificationPerson mbpUser = null;

                    if (NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                    {
                        mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());
                    }

#if true || REGISTRATION_NOT_REQUIRED
                    if (mbpUser == null)
                    {
                        mbpUser = new MessageNotificationPerson(msg.GetRawSourceAddress(), msg.GetRawSourceAddress(), msg.GetRawSourceAddress(), m_ResourceMessages.COUNTRY_DEFAULT_CULTURE, bool.TrueString);
                    }
#endif

                    if (mbpUser != null)
                    {
                        //
                        // Clear all Notification Locations
                        //
                        NotificationEngine.GetCurrentEngine().ClearNotificationByMobileNumber(mbpUser.MobileNumber);

                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_CLEAR_REQUEST, mbpUser.Name));
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion

        #region AIR REQUEST
        public  bool IsAirRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_AIR_QUERY, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleAirRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsAirRequest(msg))
            {
                try
                {
                    string szDisplayName = msg.GetRawSourceAddress();

                    if (NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                    {
                        MessageNotificationPerson mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());

                        if (mbpUser != null)
                        {
                            szDisplayName = mbpUser.Name;
                        }
                    }

                    string szLocation = Regex.Replace(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_AIR_QUERY, "${location}", RegexOptions.IgnoreCase);

                    if (!String.IsNullOrEmpty(szLocation) && !String.IsNullOrEmpty(szLocation.Trim()))
                    {
                        szLocation = szLocation.Trim();

                        //
                        // Create a new Notification Location from this Text Message
                        //
                        MessageNotificationLocation location = NotificationEngine.GetCurrentEngine().ResolveLocation(szLocation, m_ResourceMessages.COUNTRY_DEFAULT_CULTURE);

                        if (location != null)
                        {
                            szLocation = location.Name;

                            //
                            // Now perform a lookup on the air quality index for this location
                            //
                            MessageNotificationLocation measureToReturn = NotificationEngine.GetCurrentEngine().RetrieveAirQualityIndex(location);

                            if (measureToReturn != null)
                            {
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_AIR_QUERY, szDisplayName, szLocation, NotificationEngine.GetCurrentEngine().GetAirQualityDescription(m_ResourceMessages, measureToReturn.QualityIndex), measureToReturn.LastModified.ToString(m_ResourceMessages.Culture.DateTimeFormat)));
                            }
                            else
                            {
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_AIR_QUERY_NO_MEASUREMENT, szDisplayName, szLocation));
                            }
                        }
                        else
                        {
                            msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_AIR_QUERY_BAD_LOCATION, szDisplayName, szLocation));
                        }
                    }
                    else
                    {
                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.ERROR_AIR_QUERY_NOT_UNDERSTOOD));
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion

        #region WATER REQUEST
        public  bool IsWaterRequest(ShortMessage msg)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_WATER_QUERY, RegexOptions.IgnoreCase);
        }

        public  ShortMessage HandleWaterRequest(ShortMessage msg)
        {
            ShortMessage msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), m_ResourceMessages.ERROR_GENERIC);

            if (IsWaterRequest(msg))
            {
                try
                {
                    string szDisplayName = msg.GetRawSourceAddress();

                    if (NotificationEngine.GetCurrentEngine().IsMobileNumberRegistered(msg.GetRawSourceAddress()))
                    {
                        MessageNotificationPerson mbpUser = NotificationEngine.GetCurrentEngine().GetRegisteredUser(msg.GetRawSourceAddress());

                        if (mbpUser != null)
                        {
                            szDisplayName = mbpUser.Name;
                        }
                    }

                    string szLocation = Regex.Replace(msg.GetMessageTrim(), m_ResourceMessages.REQUEST_WATER_QUERY, "${location}", RegexOptions.IgnoreCase);

                    if (!String.IsNullOrEmpty(szLocation) && !String.IsNullOrEmpty(szLocation.Trim()))
                    {
                        szLocation = szLocation.Trim();

                        //
                        // Create a new Notification Location from this Text Message
                        //
                        MessageNotificationLocation location = NotificationEngine.GetCurrentEngine().ResolveLocation(szLocation, m_ResourceMessages.COUNTRY_DEFAULT_CULTURE);

                        if (location != null)
                        {
                            szLocation = location.Name;

                            //
                            // Now perform a lookup on the water quality index for this location
                            //
                            MessageNotificationLocation measureToReturn = NotificationEngine.GetCurrentEngine().RetrieveWaterQualityIndex(location);

                            if (measureToReturn != null)
                            {
                                //
                                // If this water station has a name, let the user know where the data is from
                                //
                                if (!String.IsNullOrEmpty(measureToReturn.Name))
                                {
                                    szLocation = measureToReturn.Name;
                                }

                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_WATER_QUERY, szDisplayName, szLocation, NotificationEngine.GetCurrentEngine().GetWaterQualityDescription(m_ResourceMessages, measureToReturn.QualityIndex), measureToReturn.LastModified.ToString(m_ResourceMessages.Culture.DateTimeFormat)));
                            }
                            else
                            {
                                msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_WATER_QUERY_NO_MEASUREMENT, szDisplayName, szLocation));
                            }
                        }
                        else
                        {
                            msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.RESPONSE_WATER_QUERY_BAD_LOCATION, szDisplayName, szLocation));
                        }
                    }
                    else
                    {
                        msgResponse = ShortMessage.CreateShortMessage(msg.GetRawSourceAddress(), String.Format(m_ResourceMessages.ERROR_WATER_QUERY_NOT_UNDERSTOOD));
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }
            }

            return msgResponse;
        }
        #endregion


        public static bool IsRecognizedRequest(ShortMessage msg, NotificationResourceMessages nrm)
        {
            return Regex.IsMatch(msg.GetMessageTrim(), nrm.LOCALIZED_COMMANDS, RegexOptions.IgnoreCase);
        }
  
    }
}
