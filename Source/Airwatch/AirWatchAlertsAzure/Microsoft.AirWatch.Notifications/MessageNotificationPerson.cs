﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Xml.Serialization;


using System.Mobile.Messaging;
using System.Mobile.Contacts;
using Microsoft.Mobile.Messaging.Utils;


namespace Microsoft.AirWatch.Notifications
{
    [Serializable]
    [DataContract(Name = "MessageNotificationPerson", Namespace = "http://schemas.microsoft.com/ws/2004/05/mws/sms")]
    [XmlType(TypeName = "MessageNotificationPerson", Namespace = "http://schemas.microsoft.com/ws/2004/05/mws/sms")]
    public class MessageNotificationPerson
    {
        private Guid m_Id;
        protected Uri m_Uri;
        private MobileContact m_NotificationPersonContact;
        protected String m_szNotificationPersonName;
        protected String m_szNotificationPersonDescription;
        protected DateTime m_Created;
        protected DateTime m_LastModified;
        protected bool m_TermsOfUseAccepted;
        protected String m_szCulture;

        public MessageNotificationPerson()
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;
            m_TermsOfUseAccepted = false;
            m_szCulture = "en-GB";
        }

        public MessageNotificationPerson(string szMobileNumber, string szDisplayName, string szCulture)
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;

            m_szCulture = szCulture;

            MobileContact mcNotificationPerson = new MobileContact(szDisplayName, szMobileNumber);
            mcNotificationPerson.EmailAddress = szMobileNumber;

            //
            // Save underlying contact details
            //
            m_NotificationPersonContact = mcNotificationPerson;
            this.m_szNotificationPersonName = mcNotificationPerson.DisplayName;
            this.m_szNotificationPersonDescription = mcNotificationPerson.DisplayName;
        }

        public MessageNotificationPerson(string szMobileNumber, string szUserTag, string szDisplayName, string szCulture)
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;

            m_szCulture = szCulture;

            MobileContact mcNotificationPerson = new MobileContact(szDisplayName, szMobileNumber);
            mcNotificationPerson.EmailAddress = szUserTag;

            //
            // Save underlying contact details
            //
            m_NotificationPersonContact = mcNotificationPerson;
            this.m_szNotificationPersonName = mcNotificationPerson.DisplayName;
            this.m_szNotificationPersonDescription = mcNotificationPerson.DisplayName;
        }

        public MessageNotificationPerson(string szMobileNumber, string szUserTag, string szDisplayName, string szCulture, string szTermsOfUse)
        {
            m_Id = Guid.NewGuid();
            m_Created = DateTime.Now;
            m_LastModified = m_Created;

            m_szCulture = szCulture;

            MobileContact mcNotificationPerson = new MobileContact(szDisplayName, szMobileNumber);
            mcNotificationPerson.EmailAddress = szUserTag;

            bool.TryParse(szTermsOfUse, out m_TermsOfUseAccepted);

            //
            // Save underlying contact details
            //
            m_NotificationPersonContact = mcNotificationPerson;
            this.m_szNotificationPersonName = mcNotificationPerson.DisplayName;
            this.m_szNotificationPersonDescription = mcNotificationPerson.DisplayName;
        }

        public string MobileNumber
        {
            get { return m_NotificationPersonContact.MobileNumber.ToString(AddressFormatType.Bare); }
            set { m_NotificationPersonContact.MobileNumber = NumberType.Parse(value); }
        }

        public string UserTag
        {
            get { return m_NotificationPersonContact.EmailAddress; }
            set { m_NotificationPersonContact.EmailAddress = value; }
        }

        public MobileContact ContactDetails
        {
            get { return m_NotificationPersonContact; }
            set { m_NotificationPersonContact = value; }
        }

        [DataMember(Name = "Id")]
        [XmlAttribute(AttributeName = "Id")]
        public Guid Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        [DataMember(Name = "Uri")]
        [XmlElement(ElementName = "Uri")]
        public String Uri
        {
            get { return m_Uri.AbsoluteUri; }
            set { m_Uri = new Uri(value as String); }
        }

        [DataMember(Name = "Name")]
        [XmlElement(ElementName = "Name")]
        public String Name
        {
            get { return m_szNotificationPersonName; }
            set { m_szNotificationPersonName = value; }
        }

        [DataMember(Name = "Description")]
        [XmlElement(ElementName = "Description")]
        public String Description
        {
            get { return m_szNotificationPersonDescription; }
            set { m_szNotificationPersonDescription = value; }
        }

        [DataMember(Name = "Created")]
        [XmlElement(ElementName = "Created")]
        public DateTime Created
        {
            get { return m_Created; }
            set { m_Created = value; }
        }

        [DataMember(Name = "LastModified")]
        [XmlElement(ElementName = "LastModified")]
        public DateTime LastModified
        {
            get { return m_LastModified; }
            set { m_LastModified = value; }
        }

        [DataMember(Name = "TermsOfUseAccepted")]
        [XmlElement(ElementName = "TermsOfUseAccepted")]
        public bool TermsOfUseAccepted
        {
            get { return m_TermsOfUseAccepted; }
            set { m_TermsOfUseAccepted = value; }
        }

        [DataMember(Name = "Culture")]
        [XmlElement(ElementName = "Culture")]
        public String Culture
        {
            get { return m_szCulture; }
            set { m_szCulture = value; }
        }

        public Uri GetUri()
        {
            return m_Uri;
        }
    }

}
