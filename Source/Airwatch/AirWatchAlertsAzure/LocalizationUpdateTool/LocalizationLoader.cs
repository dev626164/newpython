﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Reflection;
using System.Resources;
using System.Collections;
using System.Globalization;

using Microsoft.AirWatch.Notifications.Storage;

namespace LocalizationUpdateTool
{
    public class ResourceTableFactory
    {
        public ResourceTableFactory(string szResourceFile)
        {
            if (!String.IsNullOrEmpty(szResourceFile))
            {
                try
                {
                    Resources = new List<ResourceManagerEntity>();

                    ResXResourceReader rsxr = new ResXResourceReader(szResourceFile);

                    string szCulture = null;

                    foreach (DictionaryEntry d in rsxr)
                    {
                        if (d.Key.ToString() == "COUNTRY_DEFAULT_CULTURE")
                        {
                            szCulture = d.Value.ToString();
                            break;
                        }
                    }

                    if (!String.IsNullOrEmpty(szCulture) && CultureValidator.IsKnownCulture(szCulture))
                    {
                        foreach (DictionaryEntry d in rsxr)
                        {
                            ResourceManagerEntity rme = new ResourceManagerEntity(szCulture, d.Key.ToString(), d.Value.ToString(), d.Value.GetType().ToString());

                            Resources.Add(rme);
                        }
                    }

                    rsxr.Close();

                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                    Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
                }

            }
        }

        public static List<ResourceManagerEntity> LoadResourceTable(string szResourceFile)
        {
            try
            {
                ResourceTableFactory rtf = new ResourceTableFactory(szResourceFile);

                return rtf.Resources;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
            return null;
        }

        public List<ResourceManagerEntity> Resources { get; set; }

    }

    public class LocalizationTableFactory
    {
        private const string DEFAULT_TABLE_RESOURCE_NAME = "LocalizationUpdateTool.resources.template.resx.xml";

        public LocalizationTableFactory():this(null)
        {
        }

        public LocalizationTableFactory(string szLocalizationFile)
        {
            LocalizationTableSerializer ccts = new LocalizationTableSerializer();

            if (!String.IsNullOrEmpty(szLocalizationFile))
            {
                FileStream fs = new FileStream(szLocalizationFile, FileMode.Open, FileAccess.Read);

                if (fs != null)
                {
                    //
                    // Load and deserialize localization table
                    //
                    this.LocalizationTable = ccts.Deserialize(fs);
                }
            }

            if (this.LocalizationTable != null)
            {
                //
                // Resource template to use
                //
                Dictionary<string, ResourceManagerEntity> resourceTemplate = LoadResourceTemplate();

                //
                // Aggreated list of resource manager entities
                //
                Resources = new List<ResourceManagerEntity>();

                //
                // Transform (all languages) X (all resources entities)
                //
                foreach (Language l in this.LocalizationTable.Language)
                {
                    string szCulture = l.RowKey;

                    if (!String.IsNullOrEmpty(szCulture) && CultureValidator.IsKnownCulture(szCulture))
                    {
                        //Console.WriteLine(String.Format("Mapping Country Code [{0}] ==> Culture [{1}]", l.CountryCode, szCulture));
                        Resources.AddRange(MapResources(resourceTemplate, l));
                    }
                }
            }
        }

        private List<ResourceManagerEntity> MapResources(Dictionary<string, ResourceManagerEntity> resourceTemplate, Language l)
        {

            List<ResourceManagerEntity> langResources = new List<ResourceManagerEntity>();

            //
            // REGEX Templates to map EEA localization strings to required format for application...
            //
            string MAP_AIR_REPLY = "(?<begining>.*) (?<location>\\<.*\\>) (?<middle>.*) (?<quality>\\<.*\\>)(?<end>.*) (?<timestamp>\\<.*\\>) (?<post>.*)";
            string MAP_AIR_REPLY_ALT1 = "(?<location>\\<.*\\>) (?<middle>.*) (?<quality>\\<.*\\>)(?<end>.*) (?<timestamp>\\<.*\\>) (?<post>.*)";
            string MAP_WATER_REPLY = "(?<begining>.*) (?<location>\\<.*\\>) (?<middle>.*) (?<quality>\\<.*\\>)(?<end>.*) (?<timestamp>\\<.*\\>) (?<post>.*)";
            string MAP_WATER_REPLY_ALT1 = "(?<location>\\<.*\\>) (?<middle>.*) (?<quality>\\<.*\\>)(?<end>.*) (?<timestamp>\\<.*\\>) (?<post>.*)";

            string MAP_AIR_REPLY_ERROR_UNKNOWN_LOC = "(?<begining>.*) (?<location>\\<.*\\>) (?<end>.*)";
            string MAP_AIR_REPLY_ERROR_UNKNOWN_LOC_ALT1 = "(?<begining>.*) (?<location>\\<.*\\>)";
            string MAP_AIR_REPLY_ERROR_NO_MEASUREMENT = "(?<begining>.*) (?<location>\\<.*\\>) (?<end>.*)";
            string MAP_AIR_REPLY_ERROR_NO_MEASUREMENT_ALT1 = "(?<begining>.*) (?<location>\\<.*\\>)";

            string MAP_WATER_REPLY_ERROR_UNKNOWN_LOC = "(?<begining>.*) (?<location>\\<.*\\>) (?<end>.*)";
            string MAP_WATER_REPLY_ERROR_UNKNOWN_LOC_ALT1 = "(?<begining>.*) (?<location>\\<.*\\>)";
            string MAP_WATER_REPLY_ERROR_NO_MEASUREMENT = "(?<begining>.*) (?<location>\\<.*\\>) (?<end>.*)";
            string MAP_WATER_REPLY_ERROR_NO_MEASUREMENT_ALT1 = "(?<begining>.*) (?<location>\\<.*\\>)";

            //string MAP_LOCALIZED_COMMANDS = 

            //<AirReplyString>&lt;Hely&gt; levegőminőségi indexe &lt;nagyon jó&gt;[1] (&lt;időpecsét&gt; GMT/UTC időponttól kezdve)</AirReplyString>
            //<WaterReplyString>&lt;Hely&gt; vízminőségi indexe &lt;jó&gt;[1] (&lt;időpecsét&gt; GMT/UTC időponttól kezdve)</WaterReplyString>


            //<Language>
            //<PartitionKey>loc</PartitionKey>
            //<RowKey>de-DE</RowKey>
            //<CountryCode>49</CountryCode>
            //<AirReplyString>Der Luftqualitätsindex für &lt;Ort&gt;  ist &lt;sehr gut&gt;[1] (zum &lt;Zeitmarke&gt; GMT/UTC)</AirReplyString>
            //<WaterReplyString>Der Wasserqualitätsindex für &lt;Ort&gt;  ist &lt;gut&gt;[1] (zum &lt;Zeitmarke&gt; GMT/UTC)</WaterReplyString>
            //<AirName>Luft</AirName>
            //<WaterName>Wasser</WaterName>
            //<AirUnknownLocationErrorString>Ihre Luftabfrage war nicht erfolgreich. Wir konnten Ihren &lt;Ort&gt; nicht lokalisieren.</AirUnknownLocationErrorString>
            //<WaterUnknownLocationErrorString>Ihre Wasserabfrage war nicht erfolgreich. Wir konnten Ihren &lt;Ort&gt; nicht lokalisieren.</WaterUnknownLocationErrorString>
            //<AirNoMeasurementsForLocationString>Ihre Luftabfrage war nicht erfolgreich. Für &lt;Ort&gt; wurden keine gemessenen Luftqualitätsdaten gefunden.</AirNoMeasurementsForLocationString>
            //<WaterNoMeasurementsForLocationString>Ihre Wasserabfrage war nicht erfolgreich. Für &lt;Ort&gt; wurden keine gemessenen Wasserqualitätsdaten gefunden.</WaterNoMeasurementsForLocationString>
            //<AirValue1>Sehr gut</AirValue1>
            //<AirValue2>Gut</AirValue2>
            //<AirValue3>Durchschnittlich</AirValue3>
            //<AirValue4>Schlecht</AirValue4>
            //<AirValue5>Sehr schlecht</AirValue5>
            //<WaterValue1>Gut</WaterValue1>
            //<WaterValue2>Durchschnittlich</WaterValue2>
            //<WaterValue3>Schlecht</WaterValue3>
            //</Language>

            //
            // Remap old rowkey (locale) ==> new partion key
            // Remap property name ==> new row key
            //
            string szCulture = l.RowKey;



            foreach (string szPropertyName in resourceTemplate.Keys)
            {
                string szPropertyValue = resourceTemplate[szPropertyName].ResourcePropertyValue;
                string szPropertyType = resourceTemplate[szPropertyName].ResourcePropertyType;

                //
                // If required, update resource value from language
                //
                switch (szPropertyName)
                {
                    case "AIR_QUALITY_INDEX_0":
                    case "AIR_QUALITY_INDEX_1":
                        szPropertyValue = l.AirValue1;
                        break;
                    case "AIR_QUALITY_INDEX_2":
                        szPropertyValue = l.AirValue2;
                        break;
                    case "AIR_QUALITY_INDEX_3":
                        szPropertyValue = l.AirValue3;
                        break;
                    case "AIR_QUALITY_INDEX_4":
                        szPropertyValue = l.AirValue4;
                        break;
                    case "AIR_QUALITY_INDEX_5":
                        szPropertyValue = l.AirValue5;
                        break;
                    case "AIR_QUALITY_INDEX_6":
                        szPropertyValue = l.AirValue5;
                        break;
                    case "COUNTRY_CODE_PREFIX":
                        szPropertyValue = l.CountryCode.ToString();
                        break;
                    case "COUNTRY_DEFAULT_CULTURE":
                        szPropertyValue = l.RowKey;
                        break;
                    case "ERROR_AIR_QUERY_NOT_UNDERSTOOD":
                        szPropertyValue = l.AirUnknownLocationErrorString;
                        break;
                    case "ERROR_EXCEPTION":
                        break;
                    case "ERROR_GENERIC":
                        break;
                    case "ERROR_NOTIFY_REQUEST_NOT_UNDERSTOOD":
                        break;
                    case "ERROR_WATER_QUERY_NOT_UNDERSTOOD":
                        szPropertyValue = l.WaterUnknownLocationErrorString;
                        break;
                    case "LOCALIZED_COMMANDS":
                        szPropertyValue = String.Format("^(({0})|({1})|(register)|(accept)|(unsub)|(notify)|(location)|(stop))", l.AirName, l.WaterName);
                        break;
                    case "NOTIFICATION_AIR_QUALITY":
                        break;
                    case "REQUEST_ACCEPT":
                        break;
                    case "REQUEST_ADD":
                        break;
                    case "REQUEST_AIR_QUERY":
                        szPropertyValue = String.Format("^{0} (?<location>.*)", l.AirName);
                        break;
                    case "REQUEST_CLEAR":
                        break;
                    case "REQUEST_DEREGISTER":
                        break;
                    case "REQUEST_HELP":
                        break;
                    case "REQUEST_LOCATION":
                        break;
                    case "REQUEST_LOCATION_QUERY":
                        break;
                    case "REQUEST_NOTIFY":
                        break;
                    case "REQUEST_NOTIFY_QUERY":
                        break;
                    case "REQUEST_REGISTER":
                        break;
                    case "REQUEST_REGISTER_ANONYMOUS":
                        break;
                    case "REQUEST_REMOVE":
                        break;
                    case "REQUEST_STOP":
                        break;
                    case "REQUEST_WATER_QUERY":
                        szPropertyValue = String.Format("^{0} (?<location>.*)", l.WaterName);
                        break;
                    case "RESPONSE_AIR_QUERY":
                        //
                        // Map to regex expression...
                        //
                        if (Regex.IsMatch(l.AirReplyString, MAP_AIR_REPLY, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.AirReplyString);
                            szPropertyValue = Regex.Replace(l.AirReplyString, MAP_AIR_REPLY, "{0}, ${begining} \"{1}\" ${middle} {2} ${end} {3} ${post}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else if (Regex.IsMatch(l.AirReplyString, MAP_AIR_REPLY_ALT1, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.AirReplyString);
                            szPropertyValue = Regex.Replace(l.AirReplyString, MAP_AIR_REPLY_ALT1, "{0}, \"{1}\" ${middle} {2} ${end} {3} ${post}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ERROR: Unable to Map Air Query Response For Culture [{0}]", szCulture));
                        }
                        break;
                    case "RESPONSE_AIR_QUERY_BAD_LOCATION":
                        //
                        // Map to regex expression...
                        //
                        if (Regex.IsMatch(l.AirUnknownLocationErrorString, MAP_AIR_REPLY_ERROR_UNKNOWN_LOC, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.AirUnknownLocationErrorString);
                            szPropertyValue = Regex.Replace(l.AirUnknownLocationErrorString, MAP_AIR_REPLY_ERROR_UNKNOWN_LOC, "{0}, ${begining} \"{1}\" ${end}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else if (Regex.IsMatch(l.AirUnknownLocationErrorString, MAP_AIR_REPLY_ERROR_UNKNOWN_LOC_ALT1, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.AirUnknownLocationErrorString);
                            szPropertyValue = Regex.Replace(l.AirUnknownLocationErrorString, MAP_AIR_REPLY_ERROR_UNKNOWN_LOC_ALT1, "{0}, ${begining} \"{1}\"");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ERROR: Unable to Map Air Error Response (UNKNOWN LOC) For Culture [{0}]", szCulture));
                        }
                        break;
                    case "RESPONSE_AIR_QUERY_NO_MEASUREMENT":
                        //
                        // Map to regex expression...
                        //
                        if (Regex.IsMatch(l.AirNoMeasurementsForLocationString, MAP_AIR_REPLY_ERROR_NO_MEASUREMENT, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.AirNoMeasurementsForLocationString);
                            szPropertyValue = Regex.Replace(l.AirNoMeasurementsForLocationString, MAP_AIR_REPLY_ERROR_NO_MEASUREMENT, "{0}, ${begining} \"{1}\" ${end}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else if (Regex.IsMatch(l.AirNoMeasurementsForLocationString, MAP_AIR_REPLY_ERROR_NO_MEASUREMENT_ALT1, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.AirNoMeasurementsForLocationString);
                            szPropertyValue = Regex.Replace(l.AirNoMeasurementsForLocationString, MAP_AIR_REPLY_ERROR_NO_MEASUREMENT_ALT1, "{0}, ${begining} \"{1}\"");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ERROR: Unable to Map Air Error Response (NO MEAS) For Culture [{0}]", szCulture));
                        }
                        break;
                    case "RESPONSE_CLEAR_REQUEST":
                        break;
                    case "RESPONSE_CURRENT_NOTIFY_LOCATION":
                        break;
                    case "RESPONSE_DEFAULT_HELP_MESSAGE":
                        break;
                    case "RESPONSE_DEREGISTERED":
                        break;
                    case "RESPONSE_HELP_MESSAGE":
                        break;
                    case "RESPONSE_MUST_ACCEPT_TERMS_OF_USE":
                        break;
                    case "RESPONSE_MUST_FIRST_REGISTER":
                        break;
                    case "RESPONSE_NOTIFY_LOCATION_CHANGED":
                        break;
                    case "RESPONSE_NO_CURRENT_LOCATION":
                        break;
                    case "RESPONSE_REGISTRATION_SUCCESS":
                        break;
                    case "RESPONSE_STOP_PROCESSED":
                        break;
                    case "RESPONSE_TERMS_AND_CONDITIONS":
                        break;
                    case "RESPONSE_WATER_QUERY":
                        //
                        // Map to regex expression...
                        //
                        if (Regex.IsMatch(l.WaterReplyString, MAP_WATER_REPLY, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.WaterReplyString);
                            szPropertyValue = Regex.Replace(l.WaterReplyString, MAP_WATER_REPLY, "{0}, ${begining} \"{1}\" ${middle} {2} ${end} {3} ${post}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else if (Regex.IsMatch(l.WaterReplyString, MAP_WATER_REPLY_ALT1, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.WaterReplyString);
                            szPropertyValue = Regex.Replace(l.WaterReplyString, MAP_WATER_REPLY_ALT1, "{0}, \"{1}\" ${middle} {2} ${end} {3} ${post}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ERROR: Unable to Map Water Query Response For Culture [{0}]", szCulture));
                        }
                        break;
                    case "RESPONSE_WATER_QUERY_BAD_LOCATION":
                        //
                        // Map to regex expression...
                        //
                        if (Regex.IsMatch(l.WaterUnknownLocationErrorString, MAP_WATER_REPLY_ERROR_UNKNOWN_LOC, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.WaterUnknownLocationErrorString);
                            szPropertyValue = Regex.Replace(l.WaterUnknownLocationErrorString, MAP_WATER_REPLY_ERROR_UNKNOWN_LOC, "{0}, ${begining} \"{1}\" ${end}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else if (Regex.IsMatch(l.WaterUnknownLocationErrorString, MAP_WATER_REPLY_ERROR_UNKNOWN_LOC_ALT1, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.WaterUnknownLocationErrorString);
                            szPropertyValue = Regex.Replace(l.WaterUnknownLocationErrorString, MAP_WATER_REPLY_ERROR_UNKNOWN_LOC_ALT1, "{0}, ${begining} \"{1}\"");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ERROR: Unable to Map Water Error Response (UNKNOWN LOC) For Culture [{0}]", szCulture));
                        } break;
                    case "RESPONSE_WATER_QUERY_NO_MEASUREMENT":
                        //
                        // Map to regex expression...
                        //
                        if (Regex.IsMatch(l.WaterNoMeasurementsForLocationString, MAP_WATER_REPLY_ERROR_NO_MEASUREMENT, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.WaterNoMeasurementsForLocationString);
                            szPropertyValue = Regex.Replace(l.WaterNoMeasurementsForLocationString, MAP_WATER_REPLY_ERROR_NO_MEASUREMENT, "{0}, ${begining} \"{1}\" ${end}");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else if (Regex.IsMatch(l.WaterNoMeasurementsForLocationString, MAP_WATER_REPLY_ERROR_NO_MEASUREMENT_ALT1, RegexOptions.IgnoreCase))
                        {
                            Trace.WriteLine(l.WaterNoMeasurementsForLocationString);
                            szPropertyValue = Regex.Replace(l.WaterNoMeasurementsForLocationString, MAP_WATER_REPLY_ERROR_NO_MEASUREMENT_ALT1, "{0}, ${begining} \"{1}\"");
                            Trace.WriteLine(szPropertyValue);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ERROR: Unable to Map Water Error Response (NO MEAS) For Culture [{0}]", szCulture));
                        } break;
                    case "RESPONSE_WELCOME_MESSAGE":
                        break;
                    case "URL_TERMS_AND_CONDITIONS":
                        break;
                    break;

                    default:
                        break;
                }

                //
                // Create new item from template + overides using new culture string
                //
                ResourceManagerEntity rme = new ResourceManagerEntity(szCulture, szPropertyName, szPropertyValue, szPropertyType);

                langResources.Add(rme);
            }


            return langResources;
        }

        private Dictionary<string, ResourceManagerEntity> LoadResourceTemplate()
        {
            Dictionary<string, ResourceManagerEntity> resourceTemplate = new Dictionary<string, ResourceManagerEntity>();

            try
            {
                ResXResourceReader rsxr = new ResXResourceReader(this.DefaultPropertyResourceStream);

                string szCulture = null;

                foreach (DictionaryEntry d in rsxr)
                {
                    if (d.Key.ToString() == "COUNTRY_DEFAULT_CULTURE")
                    {
                        szCulture = d.Value.ToString();
                        break;
                    }
                }

                if (!String.IsNullOrEmpty(szCulture) && CultureValidator.IsKnownCulture(szCulture))
                {
                    foreach (DictionaryEntry d in rsxr)
                    {
                        ResourceManagerEntity rme = new ResourceManagerEntity(szCulture, d.Key.ToString(), d.Value.ToString(), d.Value.GetType().ToString());

                        resourceTemplate.Add(d.Key.ToString(), rme);
                    }
                }

                rsxr.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return resourceTemplate;
        }


        public static List<ResourceManagerEntity> LoadLocalizationTable(string szLocalizationFile)
        {
            try
            {
                LocalizationTableFactory cctf = new LocalizationTableFactory(szLocalizationFile);

                return cctf.Resources;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }
            return null;
        }

        public SmsLocalization LocalizationTable;

        public List<ResourceManagerEntity> Resources { get; set; }


        private System.IO.Stream DefaultPropertyResourceStream
        {
            get
            {
                return this.DefaultDriverAssembly.GetManifestResourceStream(DEFAULT_TABLE_RESOURCE_NAME);
            }
        }

        private System.Reflection.Assembly DefaultDriverAssembly
        {
            get
            {
                return Assembly.GetExecutingAssembly();
            }
        }
    }

    public class LocalizationTableSerializer : BaseXmlSerializer<SmsLocalization>
    {
        public LocalizationTableSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            m_knownTypes = new[] { typeof(SmsLocalization) };
        }
    }


    public class BaseXmlSerializer<T>
    {
        private XmlSerializer m_xs;
        protected XmlSerializerNamespaces m_xn;
        protected Type[] m_knownTypes;

        public BaseXmlSerializer()
        {
            Initialize();
        }

        private void Initialize()
        {
            m_xn = new XmlSerializerNamespaces();

            //
            // Add known configuration namespaces
            //
            RegisterNamespaces();
        }

        protected virtual void RegisterNamespaces()
        {
            m_xn.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            m_xn.Add("smap", "http://www.smsforum.net/schemas/smap/v1.0");
            m_xn.Add("ns", "http://www.w3.org/XML/1998/namespace");
        }

        public T Deserialize(System.IO.Stream s)
        {
            XmlTextReader xtr = new XmlTextReader(s);

            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xtr, xde);

            xtr.Close();

            return er;
        }

        public virtual T Deserialize(XmlTextReader xtr)
        {
            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xtr, xde);

            xtr.Close();

            return er;
        }

        public virtual T Deserialize(XmlReader xr)
        {
            //
            // Capture unknown events for logging
            //
            XmlDeserializationEvents xde = new XmlDeserializationEvents();
            xde.OnUnknownElement = new XmlElementEventHandler(OnUnknownElement);
            xde.OnUnknownNode = new XmlNodeEventHandler(OnUnknownNode);

            T er = (T)this.Serializer.Deserialize(xr, xde);

            xr.Close();

            return er;
        }

        protected virtual void OnUnknownElement(object o, XmlElementEventArgs xeea)
        {
            System.Diagnostics.Trace.WriteLine(String.Format("OnUnknownElement: [{0}]", xeea.Element.Name));
        }

        protected virtual void OnUnknownNode(object o, XmlNodeEventArgs xeea)
        {
            System.Diagnostics.Trace.WriteLine(String.Format("OnUnknownNode: [{0}]", xeea.Name));
        }

        public virtual void Serialize(XmlTextWriter xtw, T er)
        {
            this.Serializer.Serialize(xtw, er, m_xn);
        }

        public virtual void Serialize(XmlWriter xtw, T er)
        {
            this.Serializer.Serialize(xtw, er, m_xn);
        }

        public virtual String SerializeAsString(T msg)
        {
            String szXmlData = string.Empty;

            try
            {
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.NewLineHandling = NewLineHandling.Entitize;
                StringBuilder sb = new StringBuilder();
                XmlWriter xtw = XmlTextWriter.Create(sb, xws);
                this.Serialize(xtw, msg);
                szXmlData = sb.ToString();
                xtw.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return szXmlData;
        }


        public virtual T DeserializeFromString(String szXmlData)
        {
            T msg = default(T);

            try
            {
                StringReader sr = new StringReader(szXmlData);
                XmlTextReader xtr = new XmlTextReader(sr);
                msg = this.Deserialize(xtr);
                xtr.Close();
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(String.Format("EXCEPTION: {0}", ex.Message));
                Trace.WriteLine(String.Format("EXCEPTION DUMP:\n{0}", ex));
            }

            return msg;
        }


        public XmlSerializerNamespaces Namespaces
        {
            get { return m_xn; }
        }

        public XmlSerializer Serializer
        {
            get
            {
                if (m_xs == null)
                {
                    m_xs = GetXmlSerializer();
                }
                return m_xs;
            }
        }


        //
        // Utility
        //
        public XmlSerializer GetXmlSerializer()
        {
            //
            // Create XmlSerializer with all known types integrated
            //
            XmlSerializer xs = new XmlSerializer(typeof(T), m_knownTypes);

            return xs;
        }

    }


    public class CultureValidator
    {
        private static Dictionary<String, CultureInfo> knownCultures = new Dictionary<String, CultureInfo>();

        private CultureValidator()
        {
        }

        private static void Initialize()
        {
#if DEBUG
            int i = 0;
            Console.WriteLine("NUMBER CULTURE ISO ISO WIN ENGLISHNAME                              DISPLAYNAME");
#endif
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
#if DEBUG
                Console.WriteLine(String.Format("    {0,-7} {1,-12} {2,-3} {3,-3} {4,-3} {5,-50} {6,-40}", i++, ci.TextInfo.CultureName, ci.TwoLetterISOLanguageName, ci.ThreeLetterISOLanguageName, ci.ThreeLetterWindowsLanguageName, ci.EnglishName, ci.DisplayName));
#endif
                if (!knownCultures.ContainsKey(ci.TextInfo.CultureName))
                {
                    knownCultures.Add(ci.TextInfo.CultureName, ci);
                }
                else
                {
                    Console.WriteLine("Skipping Culture -- Already Discovered???");
                }

            }

#if DEBUG
            Console.WriteLine("Press Return to Continue");
            Console.ReadLine();
#endif
        }

        public static bool IsKnownCulture(string szCulture)
        {
            if (knownCultures.Count == 0)
            {
                Initialize();
            }

            if (!String.IsNullOrEmpty(szCulture))
            {
                if (knownCultures.ContainsKey(szCulture))
                {
                    Console.WriteLine(String.Format("Found Known Culture [{0}] ==> [{1}]", szCulture, knownCultures[szCulture].EnglishName));
                    return true;
                }
                else
                {
                    Console.WriteLine(String.Format("WARNING: Unknown Culture [{0}] ==> [{1}]", szCulture, "UNKNOWN"));

                    foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
                    {
                        if (szCulture.StartsWith(ci.TwoLetterISOLanguageName))
                        {
                            Console.Write("POSSIBLE MATCH  ");

                            Console.WriteLine(String.Format("    {0,-7} {1,-12} {2,-3} {3,-3} {4,-3} {5,-50} {6,-40}", 0, ci.TextInfo.CultureName, ci.TwoLetterISOLanguageName, ci.ThreeLetterISOLanguageName, ci.ThreeLetterWindowsLanguageName, ci.EnglishName, ci.DisplayName));
                        }
                    }
                }
            }

            return false;
        }
    }
}

