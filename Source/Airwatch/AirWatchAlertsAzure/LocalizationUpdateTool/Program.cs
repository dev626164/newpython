﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Resources;
using System.IO;


using Microsoft.AirWatch.Notifications.Storage;

namespace LocalizationUpdateTool
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<String, CultureInfo> knownCultures = new Dictionary<String, CultureInfo>();

            //
            // Check that a resource or xml file was specified
            //
            if (args.Length >= 1)
            {
                Console.WriteLine("Updating localization data from externally supplied resource file(s)");

                if (args[0].EndsWith(".resx", StringComparison.InvariantCultureIgnoreCase))
                {
                    List<string> listResourceFiles = GetMatchingFiles(args[0]);

                    foreach (string szResourceFile in listResourceFiles)
                    {
                        Console.WriteLine("Loading File: " + szResourceFile);

                        List<ResourceManagerEntity> listResources = ResourceTableFactory.LoadResourceTable(szResourceFile);

                        if ((listResources != null) && (listResources.Count > 0))
                        {
                            Console.WriteLine(String.Format("Found [{0}] Resources for Culture [{1}]", listResources.Count, listResources[0].Culture));

                            UpdateResources(listResources);
                        }
                    }
                }
                else
                {
                    List<ResourceManagerEntity> listResources = LocalizationTableFactory.LoadLocalizationTable(args[0]);
                    
                    if ((listResources != null) && (listResources.Count > 0))
                    {
                        Console.WriteLine(String.Format("Found [{0}] Resources for Culture [{1}]", listResources.Count, listResources[0].Culture));

                        UpdateResources(listResources);
                    }
                }
            }
            else
            {
                Console.WriteLine("Please specify either a .resx or .xml file to process for upload.");

                Console.WriteLine("Press Return to Enumerate Existing Resources");
                Console.ReadLine();
                EnumerateResources();
                return;
            }

            //
            //
            //

            Console.WriteLine("Press Return to Continue");
            Console.ReadLine();
        }


        public static void UpdateResources(List<ResourceManagerEntity> listResources)
        {
            //
            // Create the azure storage manager
            //
            StorageManager sm = new StorageManager();

            //
            // Initialize without hooking the events and starting the notification engine...
            //
            sm.Initialize(false);

            //
            // Save all localization resources that were found
            //
            sm.StorageManager_SaveResourcesHandler(listResources);

            Console.WriteLine("Resources Updated");

        }

        public static void EnumerateResources()
        {
            //
            // Create the azure storage manager
            //
            StorageManager sm = new StorageManager();

            //
            // Initialize without hooking the events and starting the notification engine...
            //
            sm.Initialize(false);

            List<string> listFoundCultures = new List<string>();

            //
            // Dump list of found cultures
            //
            Console.WriteLine("    NUMBER CULTURE ISO ISO WIN DISPLAYNAME                              ENGLISHNAME");

            int i = 0;

            //
            // For each culture, attempt to load the resources for that specific locale
            //
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                string szCultureName = ci.TextInfo.CultureName;


                //
                // Load localization resources that were found
                //
                AzureResourceManager arm = sm.StorageManager_LoadResourcesForLocaleHandler(szCultureName) as AzureResourceManager;

                if ((arm != null) && (arm.GetEntities() != null))
                {
                    if (arm.GetEntities().Keys.Count > 0)
                    {
                        Console.WriteLine(String.Format("    {0,-7} {1,-12} {2,-3} {3,-3} {4,-3} {5,-50} {6,-40}", i++, ci.TextInfo.CultureName, ci.TwoLetterISOLanguageName, ci.ThreeLetterISOLanguageName, ci.ThreeLetterWindowsLanguageName, ci.EnglishName, ci.DisplayName));
                        Console.WriteLine(String.Format("Culture [{0}] has [{1}] resources", szCultureName, arm.GetEntities().Keys.Count));
                        listFoundCultures.Add(szCultureName);
                    }
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("{ ");
            foreach (string szCulture in listFoundCultures)
            {
                sb.Append("\"" + szCulture + "\", ");
            }
            sb.Append("};");
            Console.WriteLine(sb.ToString());
        }

        static public List<string> GetMatchingFiles(string szFilePattern)
        {
            List<string> listMatchingFiles = new List<string>();

            foreach (string szPattern in szFilePattern.Split(Path.GetInvalidPathChars()))
            {
                string dir = Path.GetDirectoryName(szPattern);

                if (String.IsNullOrEmpty(dir))
                {
                    dir = Directory.GetCurrentDirectory();
                }
                listMatchingFiles.AddRange(Directory.GetFiles(Path.GetFullPath(dir), Path.GetFileName(szPattern)));
            }

            return listMatchingFiles;
        }
    }
}
