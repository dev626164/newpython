﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Azure.WorkerRole.Utilities
{
    /// <summary>
    /// WorkerRoleSimulator class.  Represents an Azure worker role simulator.
    /// </summary>
    /// <typeparam name="T">The type of the RoleEntryPoint to instantiate.</typeparam>
    /// <remarks>
    /// This simulator provides an environment outside of Azure where the functionality
    /// of a worker role can be tested.
    /// </remarks>
    public class AzureWorkerRoleHarness<T> where T : RoleEntryPoint
    {
        /// <summary>
        /// The role entry point.
        /// </summary>
        private RoleEntryPoint roleEntryPoint = Activator.CreateInstance<T>();

        /// <summary>
        /// The workerRoleThread.
        /// </summary>
        private Thread workerRoleThread;

        /// <summary>
        /// Initializes a new instance of the WorkerRoleSimulator class.
        /// </summary>
        private AzureWorkerRoleHarness()
        {
        }

        /// <summary>
        /// Starts the worker role simulator.
        /// </summary>
        public static void Start()
        {
            AzureWorkerRoleHarness<T> workerRoleSimulator = new AzureWorkerRoleHarness<T>();
            workerRoleSimulator.Run();
        }

        /// <summary>
        /// Runs the simulator.
        /// </summary>
        public void Run()
        {
            // Advise.
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Worker Role Simulator");
            Console.WriteLine("---------------------");

            // Start the worker role running in its own thread, leaving this thread to be its console.
            this.workerRoleThread = new Thread(this.WorkerRoleThreadEntryPoint);
            this.workerRoleThread.IsBackground = true;
            this.workerRoleThread.Start();

            // While running.
            bool running = true;
            do
            {
                // Prompt for command.
                Console.WriteLine("Enter your command:");
                string command = Console.ReadLine().ToUpperInvariant().Trim();

                // Process command.
                if (String.IsNullOrEmpty(command))
                {
                    this.ShowValidCommands();
                }
                else
                {
                    // Dispatch.
                    switch (command)
                    {
                        // Exit command.
                        case "BYE":
                        case "QUIT":
                        case "Q":
                        case "EXIT":
                            Console.WriteLine("Attempting to stop worker role thread...");
                            this.roleEntryPoint.Stop();

                            Thread.Sleep(2 * 1000);

                            if (this.workerRoleThread.IsAlive)
                            {
                                Console.WriteLine("Waiting...");
                                Thread.Sleep(15 * 1000);
                            }

                            if (this.workerRoleThread.IsAlive)
                            {
                                Console.WriteLine("Worker role did not respond to stop request.");
                                Console.WriteLine("Aborting worker role thread...");
                                this.workerRoleThread.Abort();
                            }

                            Console.WriteLine("Waiting for thread join...");

                            this.workerRoleThread.Join();
                            
                            Console.WriteLine("Done.");
                            running = false;
                            break;

                        // Foreign contaminant
                        default:
                            this.ShowValidCommands();
                            break;
                    }
                }
            }
            while (running);

            // Goodbye.
            Console.WriteLine("-------");
            Console.WriteLine("Goodbye");
        }

        /// <summary>
        /// Shows valid commands.
        /// </summary>
        private void ShowValidCommands()
        {
            Console.WriteLine();
            Console.WriteLine("Valid commands are:");
            Console.WriteLine();
            Console.WriteLine("(q)uit, exit, bye, - to exit.");
            Console.WriteLine();
        }

        /// <summary>
        /// The worker role thread entry point.
        /// </summary>
        private void WorkerRoleThreadEntryPoint()
        {
            // Start the worker role.
            this.roleEntryPoint.Start();
        }
    }
}
