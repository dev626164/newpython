﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.ServiceHosting.ServiceRuntime;

namespace Microsoft.Azure.WorkerRole.Utilities
{
    /// <summary>
    /// AzureHelper class.
    /// </summary>
    public class AzureLogging
    {
        public static string LoggingId = string.Empty;

        /// <summary>
        /// Writes a string value to the log.
        /// </summary>
        /// <param name="eventLogName">
        /// The type of event to be written to the log. Permitted string values
        /// are Critical, Error, Warning, Information, Verbose.
        /// </param>
        /// <param name="message">The message to write to the log. </param>
        public static void WriteToLog(string eventLogName, string message)
        {
            // When Azure RoleManager is running, write to the Azure log; otherwise, write to the console.
            if (RoleManager.IsRoleManagerRunning)
            {
                Trace.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] [{1}] {2}", LoggingId, eventLogName, message));
                RoleManager.WriteToLog(eventLogName, String.Format(CultureInfo.InvariantCulture, "[{0}] [{1}] {2}", LoggingId, eventLogName, message));
            }
            else
            {
                Console.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] [{1}] {2}", LoggingId, eventLogName, message));
                Trace.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] [{1}] {2}", LoggingId, eventLogName, message));
            }
        }
    }

    public class AzureTraceListener : TraceListener
    {
        public override void Write(string message)
        {
            throw new NotImplementedException();
        }

        public override void WriteLine(string message)
        {
            throw new NotImplementedException();
        }
    }

}
