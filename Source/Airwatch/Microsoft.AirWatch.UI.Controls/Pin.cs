﻿// <copyright file="Pin.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for the Pins </summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Pin container control
    /// </summary>
    public class Pin : Control
    {
        /// <summary>
        /// DependencyProperty as the backing store for Id.
        /// </summary>
        public static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(int), typeof(Pin), new PropertyMetadata(-1));

        /// <summary>
        /// DependencyProperty as the backing store for Latitude.
        /// </summary>
        public static readonly DependencyProperty LatitudeProperty =
            DependencyProperty.Register("Latitude", typeof(double), typeof(Pin), new PropertyMetadata(0.0));

        /// <summary>
        /// Using a DependencyProperty as the backing store for Longitude.
        /// </summary>
        public static readonly DependencyProperty LongitudeProperty =
            DependencyProperty.Register("Longitude", typeof(double), typeof(Pin), new PropertyMetadata(0.0));

        /// <summary>
        /// PinFlyoutElement string
        /// </summary>
        private const string PinFlyoutContainerElement = "PART_PinFlyoutContainer";

        /// <summary>
        /// PinIconContainerElement string
        /// </summary>
        private const string PinIconContainerElement = "PART_PinIconContainer";

        /// <summary>
        /// PinIconContainerTooltipElement string
        /// </summary>
        private const string PinIconContainerTooltipElement = "PART_PinIconContainerTooltip";
        
        /// <summary>
        /// true if flyout is fixed on
        /// </summary>
        private bool isFlyOutFixed;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pin"/> class.
        /// </summary>
        public Pin()
        {
        }

        /// <summary>
        /// Gets or sets the pin flyout.
        /// </summary>
        /// <value>The pin flyout.</value>
        public Grid PinFlyOutContainer { get; set; }

        /// <summary>
        /// Gets or sets the pin icon container.
        /// </summary>
        /// <value>The pin icon container.</value>
        public Grid PinIconContainer { get; set; }

        /// <summary>
        /// Gets or sets the pin flyout.
        /// </summary>
        /// <value>The pin flyout.</value>
        public PinFlyOut PinFlyOut { get; set; }

        /// <summary>
        /// Gets or sets the pin icon container tooltip.
        /// </summary>
        /// <value>The pin icon container tooltip.</value>
        public LocalizedTextBlock PinIconContainerTooltip { get; set; }
        
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The pushpin id.</value>
        public int Id
        {
            get { return (int)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude
        {
            get { return (double)GetValue(LatitudeProperty); }
            set { SetValue(LatitudeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude
        {
            get { return (double)GetValue(LongitudeProperty); }
            set { SetValue(LongitudeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is flyout fixed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is flyout fixed; otherwise, <c>false</c>.
        /// </value>
        public bool IsFlyOutFixed
        {
            get
            {
                return this.isFlyOutFixed;
            }

            set
            {
                this.isFlyOutFixed = value;
                this.UpdateFlyOut();
            }
        }

        /// <summary>
        /// Gets or sets the flyout on animation.
        /// </summary>
        /// <value>The flyout on animation.</value>
        private Storyboard FlyOutOnAnimation { get; set; }

        /// <summary>
        /// Gets or sets the flyout off animation.
        /// </summary>
        /// <value>The flyout off animation.</value>
        private Storyboard FlyOutOffAnimation { get; set; }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes (such as a rebuilding layout pass) call <see cref="M:System.Windows.Controls.Control.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.PinFlyOutContainer = this.GetTemplateChild(Pin.PinFlyoutContainerElement) as Grid;
            this.PinIconContainer = this.GetTemplateChild(Pin.PinIconContainerElement) as Grid;
            this.PinIconContainerTooltip = this.GetTemplateChild(Pin.PinIconContainerTooltipElement) as LocalizedTextBlock;

            Duration animationOnDuration = new Duration(TimeSpan.FromSeconds(0.5));
            Duration animationOffDuration = new Duration(TimeSpan.FromSeconds(0.5));

            DoubleAnimation flyoutOnDoubleAnimation = new DoubleAnimation { From = 0, To = 100, Duration = animationOnDuration };
            Storyboard.SetTarget(flyoutOnDoubleAnimation, this.PinFlyOutContainer);
            Storyboard.SetTargetProperty(flyoutOnDoubleAnimation, new PropertyPath("(UIElement.Opacity)"));
            this.FlyOutOnAnimation = new Storyboard { Duration = animationOnDuration };
            this.FlyOutOnAnimation.Children.Add(flyoutOnDoubleAnimation);

            DoubleAnimation flyoutOffDoubleAnimation = new DoubleAnimation { To = 0, Duration = animationOffDuration };
            Storyboard.SetTarget(flyoutOffDoubleAnimation, this.PinFlyOutContainer);
            Storyboard.SetTargetProperty(flyoutOffDoubleAnimation, new PropertyPath("(UIElement.Opacity)"));
            this.FlyOutOffAnimation = new Storyboard { Duration = animationOffDuration };
            this.FlyOutOffAnimation.Completed += new EventHandler(this.FlyOutOffAnimationCompleted);
            this.FlyOutOffAnimation.Children.Add(flyoutOffDoubleAnimation);

            this.MouseEnter += new MouseEventHandler(this.PinMouseEnter);
            this.MouseLeave += new MouseEventHandler(this.Pin_MouseLeave);

            this.MouseLeftButtonDown += new MouseButtonEventHandler(this.PinFlyoutMouseLeftButtonDown);
        }

        /// <summary>
        /// Hides this instance.
        /// </summary>
        public void Hide()
        {
            this.PinFlyOutContainer.Visibility = Visibility.Collapsed;
            this.PinIconContainerTooltip.BindingTerm = "PUSHPINFLYOUT_TOOLTIP_OPEN";
            this.IsFlyOutFixed = false;
        }

        /// <summary>
        /// Creates the pin flyout.
        /// </summary>
        protected virtual void CreatePinFlyOut()
        {
            if (this.PinFlyOutContainer.Children.Count == 0)
            {
                this.PinFlyOut = new PinFlyOut();
                this.PinFlyOut.DataContext = this.DataContext;
                this.PinFlyOut.CloseFlyOut += new EventHandler(this.PinFlyoutCloseFlyout);
                this.PinFlyOutContainer.Children.Add(this.PinFlyOut);
            }
        }

        /// <summary>
        /// Updates the flyout.
        /// </summary>
        protected void UpdateFlyOut()
        {
            if (this.isFlyOutFixed == true)
            {
                this.PinFlyOutContainer.Margin = MapHelper.GetFlyOutPosition(this.Longitude, this.Latitude, this.PinIconContainer.Width, this.PinIconContainer.Height, this.PinFlyOutContainer.Width, this.PinFlyOutContainer.Height);
                
                this.FlyOutOffAnimation.Stop();
                this.PinFlyOutContainer.Visibility = Visibility.Visible;
                this.PinIconContainerTooltip.BindingTerm = "PUSHPINFLYOUT_TOOLTIP_CLOSE";
                this.FlyOutOnAnimation.Begin();
            }
            else
            {
                this.FlyOutOnAnimation.Stop();
                this.FlyOutOffAnimation.Begin();
            }
        }

        /// <summary>
        /// Handles the CloseFlyout event of the PinFlyout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void PinFlyoutCloseFlyout(object sender, EventArgs e)
        {
            this.isFlyOutFixed = false;
            this.PinFlyOutContainer.Visibility = Visibility.Collapsed;
            this.PinIconContainerTooltip.BindingTerm = "PUSHPINFLYOUT_TOOLTIP_OPEN";
        }

        /// <summary>
        /// Handles the Completed event of the FlyOutOnAnimation control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FlyOutOffAnimationCompleted(object sender, EventArgs e)
        {
            this.PinFlyOutContainer.Visibility = Visibility.Collapsed;
            this.PinIconContainerTooltip.BindingTerm = "PUSHPINFLYOUT_TOOLTIP_OPEN";

            // reset to main view
            if (this.PinFlyOut != null && this.PinFlyOut.MainRadioButton != null)
            {
                this.PinFlyOut.MainRadioButton.IsChecked = true;
            }
        }

        /// <summary>
        /// Stops the MouseLeftButtonDown event from passing through the flyout
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PinFlyoutMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Handles the MouseLeave event of the Pin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void Pin_MouseLeave(object sender, MouseEventArgs e)
        {
            if (this.isFlyOutFixed == false)
            {
                this.FlyOutOnAnimation.Stop();
                this.FlyOutOffAnimation.Begin();
            }
        }

        /// <summary>
        /// Handles the MouseEnter event of the Pin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void PinMouseEnter(object sender, MouseEventArgs e)
        {
            if (ClientServices.Instance.MapViewModel.DragPushpinVisible == false && this.isFlyOutFixed == false)
            {
                this.CreatePinFlyOut();
                ClientServices.Instance.MapViewModel.CloseAllPins();
                this.PinFlyOutContainer.Margin = MapHelper.GetFlyOutPosition(this.Longitude, this.Latitude, this.PinIconContainer.Width, this.PinIconContainer.Height, this.PinFlyOutContainer.Width, this.PinFlyOutContainer.Height);
                this.FlyOutOffAnimation.Stop();
                this.PinFlyOutContainer.Visibility = Visibility.Visible;
                this.FlyOutOnAnimation.Begin();
            }
        }
    }
}
