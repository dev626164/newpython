﻿// <copyright file="PanControl.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>12-06-2009</date>
// <summary>Custom map panning control for VE Map</summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;

    [TemplatePart(Name = PanControl.PanSurfaceElement, Type = typeof(Button))]

    /// <summary>
    /// Custom control class for the Map pan movement control
    /// </summary>
    public class PanControl : Control
    {
        /// <summary>
        /// Pan surface
        /// </summary>
        private const string PanSurfaceElement = "PART_PanSurface";

        /// <summary>
        /// Timer used for panning
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Ratio the map should move by in X axis
        /// </summary>
        private double mapMoveX;

        /// <summary>
        /// Ratio the map should move by in Y axis
        /// </summary>
        private double mapMoveY;

        /// <summary>
        /// Initializes a new instance of the PanControl class.
        /// </summary>
        public PanControl()
            : base()
        {
            DefaultStyleKey = typeof(PanControl);
            
            this.timer = new DispatcherTimer();
            this.timer.Tick += new EventHandler(this.Timer_Tick);
            this.timer.Interval = new TimeSpan(100);
        }

        /// <summary>
        /// Occurs when map moves.
        /// </summary>
        public event EventHandler MapMove;

        /// <summary>
        /// Gets or sets pan surface
        /// </summary>
        public UIElement PanSurface { get; set; }

        /// <summary>
        /// Gets TemplateChild of elements when template is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.PanSurface = this.GetTemplateChild(PanControl.PanSurfaceElement) as UIElement;

            this.PanSurface.MouseLeftButtonDown += new MouseButtonEventHandler(this.PanSurfaceMouseLeftButtonDown);
            this.PanSurface.MouseMove += new MouseEventHandler(this.PanSurfaceMouseMove);
            this.PanSurface.MouseLeftButtonUp += new MouseButtonEventHandler(this.PanSurfaceMouseLeftButtonUp);
        }

        /// <summary>
        /// Pans the surface mouse left button down.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PanSurfaceMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.timer.Start();
            UIElement control = (UIElement)sender;
            control.CaptureMouse();
        }

        /// <summary>
        /// Pans the surface mouse move.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void PanSurfaceMouseMove(object sender, MouseEventArgs e)
        {
            UIElement control = (UIElement)sender;
            Point mousePos = e.GetPosition(control);

            double centerX = control.RenderSize.Width / 2;
            double centerY = control.RenderSize.Height / 2;

            double posRelativeToCenterX = mousePos.X - centerX;
            double posRelativeToCenterY = mousePos.Y - centerY;

            // converts absolute values to 
            this.mapMoveX = posRelativeToCenterX / centerX;
            this.mapMoveY = posRelativeToCenterY / centerY;
        }

        /// <summary>
        /// Pans the surface mouse left button up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PanSurfaceMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.timer.Stop();
            UIElement control = (UIElement)sender;
            control.ReleaseMouseCapture();
        }

        /// <summary>
        /// On timer tick, will look at the current move positions and fire move events accordingly
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.MapMove != null)
            {
                this.MapMove(this, new MapPanEventArgs(this.mapMoveX, this.mapMoveY));
            }
        }
    }
}