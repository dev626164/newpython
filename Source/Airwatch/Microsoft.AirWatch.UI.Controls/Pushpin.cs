﻿// <copyright file="Pushpin.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for the data inside the pushpin/station </summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.ViewModel;
    
    /// <summary>
    /// Custom control used to create bar charts
    /// </summary>
    public class Pushpin : Pin
   {
        /// <summary>
        /// start point for dragging
        /// </summary>
        private Point dragStartPoint;

        /// <summary>
        /// true if dragging
        /// </summary>
        private bool isDragging;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pushpin"/> class.
        /// </summary>
        public Pushpin()
        {
            this.Template = Application.Current.Resources["PushpinTemplate"] as ControlTemplate;
        }

        /// <summary>
        /// Occurs when a push pin is "picked up" (mouse down)
        /// </summary>
        public event MouseEventHandler PushpinPickedUpEvent;

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes (such as a rebuilding layout pass) call <see cref="M:System.Windows.Controls.Control.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.PinIconContainer.MouseMove += new MouseEventHandler(this.Pin_MouseMove);
            this.PinIconContainer.MouseLeftButtonDown += new MouseButtonEventHandler(this.PinIconContainerMouseLeftButtonDown);
            this.PinIconContainer.MouseLeftButtonUp += new MouseButtonEventHandler(this.PinMouseLeftButtonUp);
        }

        /// <summary>
        /// Creates the pin flyout.
        /// </summary>
        protected override void CreatePinFlyOut()
        {
            if (this.PinFlyOutContainer.Children.Count == 0)
            {
                base.CreatePinFlyOut();

                this.PinFlyOut.Template = Application.Current.Resources["PushpinFlyoutTemplate"] as ControlTemplate;
                this.PinFlyOut.DeletePushpin += new EventHandler(this.DeletePushpin);
            }
        }

        /// <summary>
        /// Handles the DeletePushpin event of the PinFlyout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DeletePushpin(object sender, EventArgs e)
        {
            ClientServices.Instance.MapViewModel.DeletePushpin(this.DataContext);
        }

        /// <summary>
        /// Handles the MouseLeftButtonDown event of the Pin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PinIconContainerMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.PinIconContainer.CaptureMouse();
            this.dragStartPoint = e.GetPosition(this);
            this.isDragging = true;
        }

        /// <summary>
        /// Handles the MouseMove event of the Pin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void Pin_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.isDragging == true)
            {
                Point newPos = e.GetPosition(this);
                if ((Math.Abs(this.dragStartPoint.X - newPos.X) > 5) || (Math.Abs(this.dragStartPoint.Y - newPos.Y) > 5))
                {
                    if (this.PushpinPickedUpEvent != null)
                    {
                        this.PinIconContainer.ReleaseMouseCapture();
                        ClientServices.Instance.MapViewModel.DeletePushpin(this.DataContext);
                        this.PushpinPickedUpEvent.Invoke(this, e);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the Pin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PinMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.PinIconContainer.ReleaseMouseCapture();
            this.IsFlyOutFixed = !this.IsFlyOutFixed;
            this.UpdateFlyOut();
            this.isDragging = false;
        }
    }
}