﻿// <copyright file="BarChartControl.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for creating bar charts</summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
 
    /// <summary>
    /// Custom control used to create bar charts
    /// </summary>
    public class BarChartControl : ItemsControl
    {
        /// <summary>
        /// Attached property for AxisText 1
        /// </summary>
        public static readonly DependencyProperty AxisText1Property = DependencyProperty.Register("AxisText1", typeof(double), typeof(BarChartControl), null);

        /// <summary>
        /// Attached property for AxisText 2
        /// </summary>
        public static readonly DependencyProperty AxisText2Property = DependencyProperty.Register("AxisText2", typeof(double), typeof(BarChartControl), null);

        /// <summary>
        /// Attached property for AxisText 3
        /// </summary>
        public static readonly DependencyProperty AxisText3Property = DependencyProperty.Register("AxisText3", typeof(double), typeof(BarChartControl), null);

        /// <summary>
        /// Attached property for AxisText 4
        /// </summary>
        public static readonly DependencyProperty AxisText4Property = DependencyProperty.Register("AxisText4", typeof(double), typeof(BarChartControl), null);

        /// <summary>
        /// Attached property for AxisText 5
        /// </summary>
        public static readonly DependencyProperty AxisText5Property = DependencyProperty.Register("AxisText5", typeof(double), typeof(BarChartControl), null);

        /// <summary>
        /// String for AxisTextBlock 1 element
        /// </summary>
        private const string AxisTextBlock1Element = "PART_AxisTextBlock1";

        /// <summary>
        /// String for AxisTextBlock 2 element
        /// </summary>
        private const string AxisTextBlock2Element = "PART_AxisTextBlock2";

        /// <summary>
        /// String for AxisTextBlock 3 element
        /// </summary>
        private const string AxisTextBlock3Element = "PART_AxisTextBlock3";

        /// <summary>
        /// String for AxisTextBlock 4 element
        /// </summary>
        private const string AxisTextBlock4Element = "PART_AxisTextBlock4";

        /// <summary>
        /// String for AxisTextBlock 5 element
        /// </summary>
        private const string AxisTextBlock5Element = "PART_AxisTextBlock5";
        
        /// <summary>
        /// Initializes a new instance of the <see cref="BarChartControl"/> class.
        /// </summary>
        public BarChartControl()
        {
            this.DefaultStyleKey = typeof(BarChartControl);
            this.Loaded += new RoutedEventHandler(this.BarChartControlLoaded);
        }

        /// <summary>
        /// Gets or sets the AxisTextBlock1.
        /// </summary>
        /// <value>The AxisTextBlock1.</value>
        public TextBlock AxisTextBlock1 { get; set; }

        /// <summary>
        /// Gets or sets the AxisTextBlock2.
        /// </summary>
        /// <value>The AxisTextBlock2.</value>
        public TextBlock AxisTextBlock2 { get; set; }

        /// <summary>
        /// Gets or sets the AxisTextBlock3.
        /// </summary>
        /// <value>The AxisTextBlock3.</value>
        public TextBlock AxisTextBlock3 { get; set; }

        /// <summary>
        /// Gets or sets the AxisTextBlock4.
        /// </summary>
        /// <value>The AxisTextBlock4.</value>
        public TextBlock AxisTextBlock4 { get; set; }

        /// <summary>
        /// Gets or sets the AxisTextBlock5.
        /// </summary>
        /// <value>The AxisTextBlock5.</value>
        public TextBlock AxisTextBlock5 { get; set; }

        /// <summary>
        /// Gets or sets the AxisText1.
        /// </summary>
        /// <value>The AxisText1.</value>
        public double AxisText1
        {
            get
            {
                return (double)GetValue(AxisText1Property);
            }

            set
            {
                SetValue(AxisText1Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the AxisText2.
        /// </summary>
        /// <value>The AxisText2.</value>
        public double AxisText2
        {
            get
            {
                return (double)GetValue(AxisText2Property);
            }

            set
            {
                SetValue(AxisText2Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the AxisText3.
        /// </summary>
        /// <value>The AxisText3.</value>
        public double AxisText3
        {
            get
            {
                return (double)GetValue(AxisText3Property);
            }

            set
            {
                SetValue(AxisText3Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the AxisText4.
        /// </summary>
        /// <value>The AxisText4.</value>
        public double AxisText4
        {
            get
            {
                return (double)GetValue(AxisText4Property);
            }

            set
            {
                SetValue(AxisText4Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the axis text5.
        /// </summary>
        /// <value>The axis text5.</value>
        public double AxisText5
        {
            get
            {
                return (double)GetValue(AxisText5Property);
            }

            set
            {
                SetValue(AxisText5Property, value);
            }
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        public void Update()
        {
            double powerFactor = 1.4;

            // the ratio between the height of the bar and the values
            double heightRatio = (this.Height - 10) / Math.Pow(Math.Log(this.AxisText5), powerFactor);

            if (this.AxisTextBlock1 != null && this.AxisTextBlock2 != null && this.AxisTextBlock3 != null && this.AxisTextBlock4 != null && this.AxisTextBlock5 != null)
            {
                if (this.AxisText1 > 0)
                {
                    this.AxisTextBlock1.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.AxisText1), powerFactor) * heightRatio));
                }

                if (this.AxisText2 > 0)
                {
                    this.AxisTextBlock2.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.AxisText2), powerFactor) * heightRatio));
                }

                if (this.AxisText3 > 0)
                {
                    this.AxisTextBlock3.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.AxisText3), powerFactor) * heightRatio));
                }

                if (this.AxisText4 > 0)
                {
                    this.AxisTextBlock4.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.AxisText4), powerFactor) * heightRatio));
                }

                if (this.AxisText5 > 0)
                {
                    this.AxisTextBlock5.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.AxisText5), powerFactor) * heightRatio));
                }

                this.AxisTextBlock1.Text = this.AxisText1.ToString(CultureInfo.CurrentCulture);
                this.AxisTextBlock2.Text = this.AxisText2.ToString(CultureInfo.CurrentCulture);
                this.AxisTextBlock3.Text = this.AxisText3.ToString(CultureInfo.CurrentCulture);
                this.AxisTextBlock4.Text = this.AxisText4.ToString(CultureInfo.CurrentCulture);
                this.AxisTextBlock5.Text = this.AxisText5.ToString(CultureInfo.CurrentCulture);
            }
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes (such as a rebuilding layout pass) call <see cref="M:System.Windows.Controls.Control.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.AxisTextBlock1 = this.GetTemplateChild(BarChartControl.AxisTextBlock1Element) as TextBlock;
            this.AxisTextBlock2 = this.GetTemplateChild(BarChartControl.AxisTextBlock2Element) as TextBlock;
            this.AxisTextBlock3 = this.GetTemplateChild(BarChartControl.AxisTextBlock3Element) as TextBlock;
            this.AxisTextBlock4 = this.GetTemplateChild(BarChartControl.AxisTextBlock4Element) as TextBlock;
            this.AxisTextBlock5 = this.GetTemplateChild(BarChartControl.AxisTextBlock5Element) as TextBlock;

            this.Update();
        }

        /// <summary>
        /// Fires on control loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void BarChartControlLoaded(object sender, RoutedEventArgs e)
        {
            this.AxisTextBlock1 = this.GetTemplateChild(BarChartControl.AxisTextBlock1Element) as TextBlock;
            this.AxisTextBlock2 = this.GetTemplateChild(BarChartControl.AxisTextBlock2Element) as TextBlock;
            this.AxisTextBlock3 = this.GetTemplateChild(BarChartControl.AxisTextBlock3Element) as TextBlock;
            this.AxisTextBlock4 = this.GetTemplateChild(BarChartControl.AxisTextBlock4Element) as TextBlock;
            this.AxisTextBlock5 = this.GetTemplateChild(BarChartControl.AxisTextBlock5Element) as TextBlock;

            this.Update();
        }
    }
}