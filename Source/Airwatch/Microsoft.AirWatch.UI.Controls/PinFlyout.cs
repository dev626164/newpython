﻿// <copyright file="PinFlyout.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for the PinFlyout</summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    using Microsoft.AirWatch.ViewModel;

    /// <summary>
    /// Pin container control
    /// </summary>
    public class PinFlyOut : Control
    {
        /// <summary>
        /// CloseButtonElement string
        /// </summary>
        private const string CloseButtonElement = "PART_CloseButton";

        /// <summary>
        /// ShareRadioButtonElement string
        /// </summary>
        private const string ShareRadioButtonElement = "PART_ShareRadioButton";

        /// <summary>
        /// RateRadioButtonElement string
        /// </summary>
        private const string RateRadioButtonElement = "PART_RateRadioButton";

        /// <summary>
        /// MainRadioButtonElement string
        /// </summary>
        private const string MainRadioButtonElement = "PART_MainRadioButton";

        /// <summary>
        /// MainContentElement string
        /// </summary>
        private const string MainContentElement = "PART_MainContent";

        /// <summary>
        /// ShareContentElement string
        /// </summary>
        private const string ShareContentElement = "PART_ShareContent";

        /// <summary>
        /// RateContentElement string
        /// </summary>
        private const string RateContentElement = "PART_RateContent";

        /// <summary>
        /// DeleteButtonElement string
        /// </summary>
        private const string DeleteButtonElement = "PART_DeleteButton";

        /// <summary>
        /// HisoryRadioButtonElement string
        /// </summary>
        private const string HistoryRadioButtonElement = "PART_HistoryRadioButton";

        /// <summary>
        /// HistoryContentElement string
        /// </summary>
        private const string HistoryContentElement = "PART_HistoryContent";

        /// <summary>
        /// Initializes a new instance of the <see cref="PinFlyOut"/> class.
        /// </summary>
        public PinFlyOut()
        {
            this.MouseLeftButtonUp += new MouseButtonEventHandler(this.PinFlyOut_MouseLeftButtonUp);
        }

        /// <summary>
        /// Occurs when flyout is closed
        /// </summary>
        public event EventHandler CloseFlyOut;

        /// <summary>
        /// Occurs when delete pushpin is clicked
        /// </summary>
        public event EventHandler DeletePushpin;

        /// <summary>
        /// Gets or sets the close button.
        /// </summary>
        /// <value>The Close Button.</value>
        public Button CloseButton { get; set; }

        /// <summary>
        /// Gets or sets the share radio button.
        /// </summary>
        /// <value>The share radio button.</value>
        public RadioButton ShareRadioButton { get; set; }

        /// <summary>
        /// Gets or sets the rate radio button.
        /// </summary>
        /// <value>The rate radio button.</value>
        public RadioButton RateRadioButton { get; set; }

        /// <summary>
        /// Gets or sets the main radio button.
        /// </summary>
        /// <value>The main radio button.</value>
        public RadioButton MainRadioButton { get; set; }

        /// <summary>
        /// Gets or sets the content of the main.
        /// </summary>
        /// <value>The content of the main.</value>
        public Grid MainContent { get; set; }

        /// <summary>
        /// Gets or sets the content of the share.
        /// </summary>
        /// <value>The content of the share.</value>
        public Grid ShareContent { get; set; }

        /// <summary>
        /// Gets or sets the content of the rate.
        /// </summary>
        /// <value>The content of the rate.</value>
        public Grid RateContent { get; set; }

         /// <summary>
        /// Gets or sets the delete button.
        /// </summary>
        /// <value>The delete button.</value>
        public Button DeleteButton { get; set; }

        /// <summary>
        /// Gets or sets the history radio button.
        /// </summary>
        /// <value>The share history button.</value>
        public RadioButton HistoryRadioButton { get; set; }

        /// <summary>
        /// Gets or sets the content of the history.
        /// </summary>
        /// <value>The content of the history.</value>
        public Grid HistoryContent { get; set; }
  
        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes (such as a rebuilding layout pass) call <see cref="M:System.Windows.Controls.Control.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.ShareRadioButton = this.GetTemplateChild(PinFlyOut.ShareRadioButtonElement) as RadioButton;
            this.RateRadioButton = this.GetTemplateChild(PinFlyOut.RateRadioButtonElement) as RadioButton;
            this.MainRadioButton = this.GetTemplateChild(PinFlyOut.MainRadioButtonElement) as RadioButton;
            this.MainContent = this.GetTemplateChild(PinFlyOut.MainContentElement) as Grid;
            this.ShareContent = this.GetTemplateChild(PinFlyOut.ShareContentElement) as Grid;
            this.RateContent = this.GetTemplateChild(PinFlyOut.RateContentElement) as Grid;
            this.CloseButton = this.GetTemplateChild(PinFlyOut.CloseButtonElement) as Button;
            this.DeleteButton = this.GetTemplateChild(PinFlyOut.DeleteButtonElement) as Button;
            this.HistoryContent = this.GetTemplateChild(PinFlyOut.HistoryContentElement) as Grid;
            this.HistoryRadioButton = this.GetTemplateChild(PinFlyOut.HistoryRadioButtonElement) as RadioButton;

            this.ShareRadioButton.Checked += new RoutedEventHandler(this.ShareRadioButtonChecked);
            this.RateRadioButton.Checked += new RoutedEventHandler(this.RateRadioButtonChecked);
            this.MainRadioButton.Checked += new RoutedEventHandler(this.MainRadioButtonChecked);
            this.CloseButton.Click += new RoutedEventHandler(this.CloseButtonClick);

            if (this.DeleteButton != null)
            {
                this.DeleteButton.Click += new RoutedEventHandler(this.DeleteButtonClick);
            }

            if (this.HistoryRadioButton != null)
            {
                this.HistoryRadioButton.Checked += new RoutedEventHandler(this.HistoryRadioButtonChecked);
            }

            ClientServices.Instance.MapViewModel.CancelFlyOutEvent += new EventHandler(this.MapViewModelCancelEvent);
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the PinFlyOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PinFlyOut_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Hides all the content panels
        /// </summary>
        private void HideAllPanels()
        {
            this.ShareContent.Visibility = Visibility.Collapsed;
            this.RateContent.Visibility = Visibility.Collapsed;
            this.MainContent.Visibility = Visibility.Collapsed;

            if (this.HistoryContent != null)
            {
                this.HistoryContent.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Handles the CloseButton event of the Pin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.CloseFlyOut != null)
            {
                this.CloseFlyOut.Invoke(this, new EventArgs());
            }

            if (this.MainRadioButton != null)
            {
                this.MainRadioButton.IsChecked = true;
            }
        }

        /// <summary>
        /// Fires on radio button checked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MainRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.HideAllPanels();
            this.MainContent.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Fires on radio button checked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void RateRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.HideAllPanels();
            this.RateContent.Visibility = Visibility.Visible;
            ClientServices.Instance.MapViewModel.DoneRatingEvent += new EventHandler(this.MapViewModelDoneRatingEvent);
        }

        /// <summary>
        /// Fires on radio button checked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ShareRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.HideAllPanels();
            this.ShareContent.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Fires on radio button checked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HistoryRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.HideAllPanels();
            this.HistoryContent.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Handles the event to cancel
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        private void MapViewModelCancelEvent(object sender, EventArgs e)
        {
            if (this.MainRadioButton != null)
            {
                this.MainRadioButton.IsChecked = true;
            }

            ClientServices.Instance.MapViewModel.DoneRatingEvent -= new EventHandler(this.MapViewModelDoneRatingEvent);
        }

        /// <summary>
        /// Handles the event to "finish" the rating process
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        private void MapViewModelDoneRatingEvent(object sender, EventArgs e)
        {
            if (this.MainRadioButton != null)
            {
                this.MainRadioButton.IsChecked = true;
            }

            ClientServices.Instance.MapViewModel.DoneRatingEvent -= new EventHandler(this.MapViewModelDoneRatingEvent);
        }

        /// <summary>
        /// Handles the Click event of the DeleteButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.DeletePushpin != null)
            {
                this.DeletePushpin.Invoke(this, new EventArgs());
            }
        }
    }
}
