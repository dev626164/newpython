﻿// <copyright file="EuropeanEnvironmentAgencyRating.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>13-07-2009</date>
// <summary>Custom control for single rating icon with label</summary>
namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Globalization;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Ink;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;

    [TemplatePart(Name = EuropeanEnvironmentAgencyRating.ElementBackground, Type = typeof(Shape))]
    [TemplatePart(Name = EuropeanEnvironmentAgencyRating.ElementRatingValue, Type = typeof(TextBlock))]

    /// <summary>
    /// Public class for Eea rating
    /// </summary>
    public class EuropeanEnvironmentAgencyRating : Control
    {
        /// <summary>
        /// Rating value provided by the European Environment Agency
        /// </summary>
        public static readonly DependencyProperty AgencyValueProperty =
            DependencyProperty.Register("AgencyValue", typeof(int), typeof(EuropeanEnvironmentAgencyRating), new PropertyMetadata(new PropertyChangedCallback(OnValuePropertyChanged)));

        /// <summary>
        /// Air or Water data being shown
        /// </summary>
        public static readonly DependencyProperty StationTypeProperty =
            DependencyProperty.Register("StationType", typeof(string), typeof(EuropeanEnvironmentAgencyRating), new PropertyMetadata(new PropertyChangedCallback(OnTypeValuePropertyChanged)));

        /// <summary>
        /// Background colour of control
        /// </summary>
        private const string ElementBackground = "PART_Background";

        /// <summary>
        /// Rating value the Eea have assigned
        /// </summary>
        private const string ElementRatingValue = "PART_RatingValue";

        /// <summary>
        /// Initializes a new instance of the EuropeanEnvironmentAgencyRating class
        /// </summary>
        public EuropeanEnvironmentAgencyRating()
            : base()
        {
            DefaultStyleKey = typeof(EuropeanEnvironmentAgencyRating);
        }

        /// <summary>
        /// Gets or sets the rating value part
        /// </summary>
        public TextBlock PartRatingValue { get; set; }

        /// <summary>
        /// Gets or sets the background value part
        /// </summary>
        public Shape PartBackground { get; set; }

        /// <summary>
        /// Gets or sets the rating value accepted by the control
        /// </summary>
        public int AgencyValue
        {
            get
            {
                return (int)GetValue(AgencyValueProperty);
            }

            set
            {
                SetValue(AgencyValueProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the station type accepted by the control
        /// </summary>
        public string StationType
        {
            get
            {
                return (string)GetValue(StationTypeProperty);
            }

            set
            {
                SetValue(StationTypeProperty, value);
            }
        }

        /// <summary>
        /// Gets TemplateChild of elements when template is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.PartBackground = this.GetTemplateChild(EuropeanEnvironmentAgencyRating.ElementBackground) as Shape;
            this.PartRatingValue = this.GetTemplateChild(EuropeanEnvironmentAgencyRating.ElementRatingValue) as TextBlock;
            Update(this);
        }

        /// <summary>
        /// Called when [rating value property changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            EuropeanEnvironmentAgencyRating.Update(sender);
        }

        /// <summary>
        /// Called when [type value property changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnTypeValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            EuropeanEnvironmentAgencyRating rating = sender as EuropeanEnvironmentAgencyRating;

            if (rating.StationType != null)
            {
                switch (rating.StationType)
                {
                    case "Air":
                        rating.Template = Application.Current.Resources["EEARatingAir"] as ControlTemplate;
                        break;
                    case "Water":
                        rating.Template = Application.Current.Resources["EEARatingWater"] as ControlTemplate;
                        break;
                }
            }

            EuropeanEnvironmentAgencyRating.Update(sender);
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        /// <param name="sender">The sender.</param>
        private static void Update(DependencyObject sender)
        {
            EuropeanEnvironmentAgencyRating rating = sender as EuropeanEnvironmentAgencyRating;

            if (rating.PartRatingValue != null)
            {
                if (rating.AgencyValue > 0)
                {
                    rating.PartRatingValue.Text = rating.AgencyValue.ToString(CultureInfo.CurrentCulture);
                }
                else
                {
                    rating.PartRatingValue.Text = string.Empty;
                }
            }

            if (rating.PartBackground != null)
            {
                rating.PartBackground.Fill = GetRatingColor(rating.StationType, rating.AgencyValue);
            }     
        }

        /// <summary>
        /// Gets the color of the rating.
        /// </summary>
        /// <param name="type">The station type.</param>
        /// <param name="rating">The rating .</param>
        /// <returns>EEA colored brush</returns>
        private static Brush GetRatingColor(string type, int rating)
        {
            LinearGradientBrush brush = new LinearGradientBrush();
            brush.StartPoint = new Point(0.5, 0);
            brush.EndPoint = new Point(0.5, 1);
            
            // convert the reading from water to air for colour matching
            if (type != null && type == "Water")
            {
                rating = (rating * 2) - 1;
            }

            switch (rating)
            {
                case 1:
                    GradientStop oneBottomGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 18, 80, 0),
                        Offset = 1
                    };
                    GradientStop oneTopGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 91, 219, 1),
                        Offset = 0.0
                    };

                    brush.GradientStops.Add(oneTopGS);
                    brush.GradientStops.Add(oneBottomGS);
                    break;
                case 2:
                    GradientStop twoBottomGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 37, 190, 0),
                        Offset = 1
                    };
                    GradientStop twoTopGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 175, 233, 9),
                        Offset = 0.0
                    };

                    brush.GradientStops.Add(twoTopGS);
                    brush.GradientStops.Add(twoBottomGS);
                    break;
                case 3:
                    GradientStop threeBottomGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 201, 202, 5),
                        Offset = 1
                    };
                    GradientStop threeTopGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 254, 255, 3),
                        Offset = 0.0
                    };

                    brush.GradientStops.Add(threeTopGS);
                    brush.GradientStops.Add(threeBottomGS);
                    break;
                case 4:
                    GradientStop fourBottomGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 180, 90, 0),

                        Offset = 1
                    };
                    GradientStop fourTopGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 254, 150, 45),
                        Offset = 0.0
                    };

                    brush.GradientStops.Add(fourTopGS);
                    brush.GradientStops.Add(fourBottomGS);
                    break;
                case 5:
                    GradientStop fiveBottomGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 129, 0, 0),
                        Offset = 1
                    };
                    GradientStop fiveTopGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 255, 4, 4),
                        Offset = 0.0
                    };

                    brush.GradientStops.Add(fiveTopGS);
                    brush.GradientStops.Add(fiveBottomGS);
                    break;
                case 0:
                    GradientStop zeroBottomGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 40, 40, 40),
                        Offset = 1
                    };
                    GradientStop zeroTopGS = new GradientStop()
                    {
                        Color = Color.FromArgb(255, 140, 140, 140),
                        Offset = 0.0
                    };

                    brush.GradientStops.Add(zeroTopGS);
                    brush.GradientStops.Add(zeroBottomGS);
                    break;
            }

            return brush;
        }
    }
}
