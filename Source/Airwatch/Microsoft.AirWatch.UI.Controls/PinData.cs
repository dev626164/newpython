﻿// <copyright file="PinData.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for the data inside the pushpin/station </summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Custom control used to create bar charts
    /// </summary>
    public class PinData : Control
    {
        /// <summary>
        /// Dependency Property indicating if the push pin is showing air or water data
        /// </summary>
        public static readonly DependencyProperty TargetTypeProperty =
            DependencyProperty.Register("TargetType", typeof(string), typeof(PinData), new PropertyMetadata(new PropertyChangedCallback(TargetTypePropertyChanged)));

        /// <summary>
        /// Gets or sets the type of the target.
        /// </summary>
        /// <value>The type of the target.</value>
        public string TargetType
        {
            get
            { 
                return (string)GetValue(TargetTypeProperty); 
            }

            set 
            { 
                SetValue(TargetTypeProperty, value); 
            }
        }

        /// <summary>
        /// Targets the type property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void TargetTypePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            PinData pinData = sender as PinData;

            switch (pinData.TargetType)
            {
                case "Air":
                    {
                        pinData.Template = Application.Current.Resources["StationDataTemplate"] as ControlTemplate;
                        break;
                    }

                case "Water":
                    {
                        pinData.Template = Application.Current.Resources["WaterStationDataTemplate"] as ControlTemplate;
                        break;
                    }
            }
        }
    }
}
