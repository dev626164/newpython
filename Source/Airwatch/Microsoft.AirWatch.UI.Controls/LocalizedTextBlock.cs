﻿//-----------------------------------------------------------------------
// <copyright file="LocalizedTextBlock.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>01-10-2009</date>
// <summary>Localised TextBlock.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.UI.Controls
{
    #region Using

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;

    #endregion

    /// <summary>
    /// Localised TextBlock.
    /// </summary>
    [TemplatePart(Name = LocalizedTextBlock.TextBlockElement, Type = typeof(TextBlock))]
    public class LocalizedTextBlock : Control
    {        
        /// <summary>
        /// Dependency Property for the Binding Term
        /// </summary>
        public static readonly DependencyProperty BindingTermProperty = DependencyProperty.Register("BindingTerm", typeof(string), typeof(LocalizedTextBlock), new PropertyMetadata(string.Empty, new PropertyChangedCallback(LocalizedTextBlock.BindingTermChanged)));

        /// <summary>
        /// Identifies the System.Windows.Controls.TextBlock.TextWrapping dependency property.
        /// </summary>
        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register("TextWrapping", typeof(TextWrapping), typeof(LocalizedTextBlock), new PropertyMetadata(TextWrapping.NoWrap));

        /// <summary>
        /// Identifies the System.Windows.Controls.TextBlock.TextAlignment dependency property.
        /// </summary>
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(LocalizedTextBlock), null);
               
        /// <summary>
        /// Identifies the TranslatedTermProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty TranslatedTermProperty = DependencyProperty.Register("TranslatedTerm", typeof(string), typeof(LocalizedTextBlock), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Identifies the CaseProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty CaseProperty = DependencyProperty.Register("Case", typeof(string), typeof(LocalizedTextBlock), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Identifies the CaseProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty ParameterProperty = DependencyProperty.Register("Parameter", typeof(string), typeof(LocalizedTextBlock), new PropertyMetadata(null));

        /// <summary>
        /// TextBlockElement string definition.
        /// </summary>
        private const string TextBlockElement = "PART_TextBlock";   

        /// <summary>
        /// Backing field for CurrentLanguageDictionary property.
        /// </summary>
        private static Dictionary<string, string> currentLanguageDictionary;             
      
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedTextBlock"/> class.
        /// </summary>
        public LocalizedTextBlock()
        {
            this.DefaultStyleKey = typeof(LocalizedTextBlock);
            LocalizedTextBlock.CultureChanged += new EventHandler(this.CultureChangedEventHandler);
        }

        /// <summary>
        /// Occurs when [culture changed].
        /// </summary>
        public static event EventHandler CultureChanged;       

        /// <summary>
        /// Gets or sets the current language dictionary.
        /// </summary>
        /// <value>The current language dictionary.</value>
        public static Dictionary<string, string> CurrentLanguageDictionary
        {
            get
            {
                return LocalizedTextBlock.currentLanguageDictionary;
            }

            set
            {
                LocalizedTextBlock.currentLanguageDictionary = value;
                if (CultureChanged != null)
                {
                    LocalizedTextBlock.CultureChanged(null, null);
                }
            }
        }         

        /// <summary>
        /// Gets or sets the binding term.
        /// </summary>
        /// <value>The binding term.</value>
        public string BindingTerm
        {
            get
            {
                return (string)GetValue(BindingTermProperty);
            }

            set
            {
                SetValue(BindingTermProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the case of the translated term.
        /// </summary>
        /// <value>The case of the translated term.  'Upper', 'Lower', or 'Camel'.</value>
        public string Case
        {
            get
            {
                return (string)GetValue(CaseProperty);
            }

            set
            {
                SetValue(CaseProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the parameter.
        /// </summary>
        /// <value>The parameter to replace in the translated term.</value>
        public string Parameter
        {
            get
            {
                return (string)GetValue(ParameterProperty);
            }

            set
            {
                SetValue(ParameterProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the text wrapping.
        /// </summary>
        /// <value>The text wrapping.</value>
        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text alignment.
        /// </summary>
        /// <value>The text alignment.</value>
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }        

        /// <summary>
        /// Gets or sets the translated term.
        /// </summary>
        /// <value>The translated term.</value>
        public string TranslatedTerm
        {
            get { return (string)GetValue(TranslatedTermProperty); }
            set { SetValue(TranslatedTermProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text block we are wrapping.
        /// </summary>
        /// <value>The text block.</value>
        public TextBlock TextBlock 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Bindings the term changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        public static void BindingTermChanged(DependencyObject sender, DependencyPropertyChangedEventArgs eventArgs)
        {
            LocalizedTextBlock localisedTextBlock = sender as LocalizedTextBlock;

            if (System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                localisedTextBlock.TranslatedTerm = eventArgs.NewValue.ToString();
            }
            else if (localisedTextBlock != null && LocalizedTextBlock.CurrentLanguageDictionary != null && eventArgs.NewValue != null)
            {
                if (LocalizedTextBlock.CurrentLanguageDictionary.ContainsKey(eventArgs.NewValue.ToString()))
                {
                    localisedTextBlock.TranslatedTerm = SetCase(localisedTextBlock.Case, LocalizedTextBlock.CurrentLanguageDictionary[eventArgs.NewValue.ToString()]);
                }
                else
                {
                    localisedTextBlock.TranslatedTerm = SetCase(localisedTextBlock.Case, eventArgs.NewValue.ToString());
                }

                if (!string.IsNullOrEmpty(localisedTextBlock.Parameter))
                {
                    localisedTextBlock.TranslatedTerm = string.Format(CultureInfo.InvariantCulture, localisedTextBlock.TranslatedTerm, localisedTextBlock.Parameter);
                }
            }
        }        

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes (such as a rebuilding layout pass) call <see cref="M:System.Windows.Controls.Control.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.TextBlock = this.GetTemplateChild(LocalizedTextBlock.TextBlockElement) as TextBlock;
        }

        /// <summary>
        /// Sets the case.
        /// </summary>
        /// <param name="textCase">The text case.</param>
        /// <param name="term">The term to turn to lower case, upper case or 'camel' case.</param>
        /// <returns>A string in the required case.</returns>
        private static string SetCase(string textCase, string term)
        {
            string finalString = term;

            switch (textCase.ToLower(CultureInfo.CurrentCulture))
            {
                case "upper":
                    finalString = term.ToUpper(CultureInfo.CurrentCulture);
                    break;
                case "lower":
                    finalString = term.ToLower(CultureInfo.CurrentCulture);
                    break;
                case "camel":
                    string[] wordArray = term.Split(new char[] { ' ' });

                    StringBuilder stringBuilder = new StringBuilder();

                    foreach (string s in wordArray)
                    {
                        stringBuilder.Append(s[0].ToString().ToUpper(CultureInfo.CurrentCulture));
                        stringBuilder.Append(s.Substring(1, s.Length - 1));
                        stringBuilder.Append(" ");
                    }

                    break;
                default:
                    break;
            }

            return finalString;
        }

        /// <summary>
        /// Handles the CultureChanged event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.UI.Controls.LanguageChangedEventArgs"/> instance containing the event data.</param>
        private void CultureChangedEventHandler(object sender, EventArgs e)
        {
            if (this.BindingTerm != null && LocalizedTextBlock.CurrentLanguageDictionary.ContainsKey(this.BindingTerm))
            {
                if (string.IsNullOrEmpty(this.Parameter))
                {
                    this.TranslatedTerm = SetCase(this.Case, LocalizedTextBlock.CurrentLanguageDictionary[this.BindingTerm]);
                }
                else
                {
                    this.TranslatedTerm = string.Format(CultureInfo.InvariantCulture, LocalizedTextBlock.CurrentLanguageDictionary[this.BindingTerm], this.Parameter);
                }
            }
        }        
    }
}
