﻿// <copyright file="BarControl.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for creating bar charts</summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Ink;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    
    /// <summary>
    /// Custom control used to create bar charts
    /// </summary>
    public class BarControl : Control
    {
        /// <summary>
        /// Dependency Property for units of graph.
        /// </summary>
        public static readonly DependencyProperty UnitsProperty = DependencyProperty.Register("Units", typeof(string), typeof(BarControl), null);

        /// <summary>
        /// Attached property for value of child
        /// </summary>
        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register("Label", typeof(string), typeof(BarControl), null);

        /// <summary>
        /// Attached property for value of child
        /// </summary>
        public static readonly DependencyProperty HoverProperty = DependencyProperty.Register("Hover", typeof(string), typeof(BarControl), null);

        /// <summary>
        /// Attached property for value of child
        /// </summary>
        public static readonly DependencyProperty BarValueProperty = DependencyProperty.Register("BarValue", typeof(double), typeof(BarControl), null);

        /// <summary>
        /// Attached property for value of child
        /// </summary>
        public static readonly DependencyProperty BarValueStringProperty = DependencyProperty.Register("BarValueString", typeof(string), typeof(BarControl), null);

        /// <summary>
        /// Attached property for threshold 1
        /// </summary>
        public static readonly DependencyProperty Threshold1Property = DependencyProperty.Register("Threshold1", typeof(double), typeof(BarControl), null);

        /// <summary>
        /// Attached property for threshold 2
        /// </summary>
        public static readonly DependencyProperty Threshold2Property = DependencyProperty.Register("Threshold2", typeof(double), typeof(BarControl), null);

        /// <summary>
        /// Attached property for threshold 3
        /// </summary>
        public static readonly DependencyProperty Threshold3Property = DependencyProperty.Register("Threshold3", typeof(double), typeof(BarControl), null);

        /// <summary>
        /// Attached property for threshold 4
        /// </summary>
        public static readonly DependencyProperty Threshold4Property = DependencyProperty.Register("Threshold4", typeof(double), typeof(BarControl), null);

        /// <summary>
        /// Greatest value the chart can show
        /// </summary>
        public static readonly DependencyProperty MaxValueProperty = DependencyProperty.Register("BarMaxValue", typeof(double), typeof(BarControl), null);

        /// <summary>
        /// String for BarLabel element
        /// </summary>
        private const string BarLabelElement = "PART_BarLabel";

        /// <summary>
        /// String for BarLabel element
        /// </summary>
        private const string BarHoverElement = "PART_BarHover";

        /// <summary>
        /// String for Bar element
        /// </summary>
        private const string BarElement = "PART_Bar";

        /// <summary>
        /// String for Path 1 element
        /// </summary>
        private const string Path1Element = "PART_Path1";

        /// <summary>
        /// String for Path 2 element
        /// </summary>
        private const string Path2Element = "PART_Path2";

        /// <summary>
        /// String for Path 3 element
        /// </summary>
        private const string Path3Element = "PART_Path3";

        /// <summary>
        /// String for Path 4 element
        /// </summary>
        private const string Path4Element = "PART_Path4";

        /// <summary>
        /// Initializes a new instance of the <see cref="BarControl"/> class.
        /// </summary>
        public BarControl()
        {
            this.DefaultStyleKey = typeof(BarControl);
            this.Loaded += new RoutedEventHandler(this.BarControlLoaded);
        }

        /// <summary>
        /// Gets or sets the Path1.
        /// </summary>
        /// <value>The Path1.</value>
        public Path Path1 { get; set; }

        /// <summary>
        /// Gets or sets the Path2.
        /// </summary>
        /// <value>The Path2.</value>
        public Path Path2 { get; set; }

        /// <summary>
        /// Gets or sets the Path3.
        /// </summary>
        /// <value>The Path3.</value>
        public Path Path3 { get; set; }

        /// <summary>
        /// Gets or sets the Path4.
        /// </summary>
        /// <value>The Path4.</value>
        public Path Path4 { get; set; }

        /// <summary>
        /// Gets or sets the bar label.
        /// </summary>
        /// <value>The bar label.</value>
        public TextBlock BarLabel { get; set; }

        /// <summary>
        /// Gets or sets the bar.
        /// </summary>
        /// <value>The bar element.</value>
        public Border Bar { get; set; }

        /// <summary>
        /// Gets or sets the threshold1.
        /// </summary>
        /// <value>The threshold1.</value>
        public double Threshold1
        {
            get
            {
                return (double)GetValue(Threshold1Property);
            }

            set
            {
                SetValue(Threshold1Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the threshold2.
        /// </summary>
        /// <value>The threshold2.</value>
        public double Threshold2
        {
            get
            {
                return (double)GetValue(Threshold2Property);
            }

            set
            {
                SetValue(Threshold2Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the threshold3.
        /// </summary>
        /// <value>The threshold3.</value>
        public double Threshold3
        {
            get
            {
                return (double)GetValue(Threshold3Property);
            }

            set
            {
                SetValue(Threshold3Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the threshold4.
        /// </summary>
        /// <value>The threshold4.</value>
        public double Threshold4
        {
            get
            {
                return (double)GetValue(Threshold4Property);
            }

            set
            {
                SetValue(Threshold4Property, value);
            }
        }

        /// <summary>
        /// Gets or sets the max value.
        /// </summary>
        /// <value>The max value.</value>
        public double BarMaxValue
        {
            get
            {
                return (double)GetValue(MaxValueProperty);
            }

            set
            {
                SetValue(MaxValueProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the hover.
        /// </summary>
        /// <value>The hover.</value>
        public string Hover
        {
            get
            {
                return (string)GetValue(HoverProperty);
            }

            set
            {
                SetValue(HoverProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public double BarValue
        {
            get
            {
                return (double)GetValue(BarValueProperty);
            }

            set
            {
                SetValue(BarValueProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public string BarValueString
        {
            get
            {
                return (string)GetValue(BarValueProperty);
            }

            set
            {
                SetValue(BarValueProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>The label.</value>
        public string Label
        {
            get
            {
                return (string)GetValue(LabelProperty);
            }
            
            set
            {
                SetValue(LabelProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the units.
        /// </summary>
        /// <value>The label.</value>
        public string Units
        {
            get
            {
                return (string)GetValue(UnitsProperty);
            }

            set
            {
                SetValue(UnitsProperty, value);
            }
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        public void Update()
        {
            if (this.BarLabel == null || this.Bar == null)
            {
                this.ApplyTemplate();
            }

            this.BarLabel.Text = this.Label;            

            if (this.BarMaxValue == 0)
            {
                this.BarMaxValue = 500;
            }

            double power = 1.4;

            // the ratio between the height of the bar and the values
            double heightRatio = this.Height / Math.Pow(Math.Log(this.BarMaxValue), power);

            // percentage of the max value that the value is, so we can scale the thresholds if needed
            double percentageHeight = this.BarValue / this.BarMaxValue;            

            if (percentageHeight <= 1)
            {
                if (this.BarValue >= 1)
                {
                    this.Bar.Height = Math.Pow(Math.Log(this.BarValue + 1), power) * heightRatio;
                }

                if (this.Threshold1 >= 1)
                {
                    this.Path1.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.Threshold1 + 1), power) * heightRatio) - (this.Path1.Height / 2));
                }

                if (this.Threshold2 >= 1)
                {
                    this.Path2.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.Threshold2 + 1), power) * heightRatio) - (this.Path2.Height / 2));
                }

                if (this.Threshold3 >= 1)
                {
                    this.Path3.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.Threshold3 + 1), power) * heightRatio) - (this.Path3.Height / 2));
                }

                if (this.Threshold4 >= 1)
                {
                    this.Path4.Margin = new Thickness(0, 0, 0, (Math.Pow(Math.Log(this.Threshold4 + 1), power) * heightRatio) - (this.Path4.Height / 2));
                }
            }
            else
            {
                this.Bar.Height = this.Height;

                if (this.Threshold1 >= 1)
                {
                    this.Path1.Margin = new Thickness(0, 0, 0, Math.Log(this.Threshold1 * percentageHeight) * heightRatio);
                }

                if (this.Threshold2 >= 1)
                {
                    this.Path2.Margin = new Thickness(0, 0, 0, Math.Log(this.Threshold2 * percentageHeight) * heightRatio);
                }

                if (this.Threshold3 >= 1)
                {
                    this.Path3.Margin = new Thickness(0, 0, 0, Math.Log(this.Threshold3 * percentageHeight) * heightRatio);
                }

                if (this.Threshold4 >= 1)
                {
                    this.Path4.Margin = new Thickness(0, 0, 0, Math.Log(this.Threshold4 * percentageHeight) * heightRatio);
                }
            }

            if (this.BarValue < this.Threshold1)
            {
                this.Bar.Background = new SolidColorBrush(new Color { A = 255, B = 0, G = 95, R = 31 });
            } 
            else if (this.BarValue < this.Threshold2)
            {
                this.Bar.Background = new SolidColorBrush(new Color { A = 255, B = 4, G = 191, R = 113 });
            } 
            else if (this.BarValue < this.Threshold3)
            {
                this.Bar.Background = new SolidColorBrush(new Color { A = 255, B = 4, G = 207, R = 206 });
            }
            else if (this.BarValue < this.Threshold4)
            {
                this.Bar.Background = new SolidColorBrush(new Color { A = 255, B = 0, G = 128, R = 255 });
            }
            else
            {
                this.Bar.Background = new SolidColorBrush(new Color { A = 255, B = 0, G = 0, R = 147 });
            }
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes (such as a rebuilding layout pass) call <see cref="M:System.Windows.Controls.Control.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.BarLabel = this.GetTemplateChild(BarControl.BarLabelElement) as TextBlock;           
            this.Bar = this.GetTemplateChild(BarControl.BarElement) as Border;
            this.Path1 = this.GetTemplateChild(BarControl.Path1Element) as Path;
            this.Path2 = this.GetTemplateChild(BarControl.Path2Element) as Path;
            this.Path3 = this.GetTemplateChild(BarControl.Path3Element) as Path;
            this.Path4 = this.GetTemplateChild(BarControl.Path4Element) as Path;
        }

        /// <summary>
        /// Fires on control loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void BarControlLoaded(object sender, RoutedEventArgs e)
        {
            this.BarLabel = this.GetTemplateChild(BarControl.BarLabelElement) as TextBlock;            
            this.Bar = this.GetTemplateChild(BarControl.BarElement) as Border;
            this.Path1 = this.GetTemplateChild(BarControl.Path1Element) as Path;
            this.Path2 = this.GetTemplateChild(BarControl.Path2Element) as Path;
            this.Path3 = this.GetTemplateChild(BarControl.Path3Element) as Path;
            this.Path4 = this.GetTemplateChild(BarControl.Path4Element) as Path;

            this.Update();
        }
    }
}