﻿//-----------------------------------------------------------------------
// <copyright file="LanguageChangedEventArgs.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Simon Middlemiss</author>
// <email>smiddle@microsoft.com</email>
// <date>26-09-2009</date>
// <summary>Language Changed Event Args.</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.UI.Controls
{
    #region Usings

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// Language Changed Event Args.
    /// </summary>
    public class LanguageChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Language dictionary
        /// </summary>
        private Dictionary<string, string> languageDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageChangedEventArgs"/> class.
        /// </summary>
        /// <param name="stringDictionary">The string dictionary.</param>
        public LanguageChangedEventArgs(Dictionary<string, string> stringDictionary)
        {
            this.languageDictionary = stringDictionary;
        }

        /// <summary>
        /// Gets the language dictionary.
        /// </summary>
        /// <value>The language dictionary.</value>
        public Dictionary<string, string> LanguageDictionary
        {
            get
            {
                return this.languageDictionary;
            }
        }
    }
}
