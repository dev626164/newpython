﻿// <copyright file="MapPanEventArgs.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>16-06-2009</date>
// <summary>Custom event args to ascertain zoom level mode change</summary>
namespace Microsoft.AirWatch.UI.Controls
{
    using System;

    /// <summary>
    /// Custom event args for zoom level change events in MapNavigationBar.cs
    /// </summary>
    public class MapPanEventArgs : EventArgs
    {
        /// <summary>
        /// The ratio which shows how far the map should move with 0 being a small ammount and 1 being large.
        /// </summary>
        private double moveRatioX;

        /// <summary>
        /// The ratio which shows how far the map should move with 0 being a small ammount and 1 being large.
        /// </summary>
        private double moveRatioY;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapPanEventArgs"/> class.
        /// </summary>
        /// <param name="moveRatioX">The move ratio X.</param>
        /// <param name="moveRatioY">The move ratio Y.</param>
        public MapPanEventArgs(double moveRatioX, double moveRatioY)
        {
            this.moveRatioX = moveRatioX;
            this.YMoveRatio = moveRatioY;
        }

        /// <summary>
        /// Gets or sets the X move ratio.
        /// </summary>
        /// <value>The X move ratio.</value>
       public double XMoveRatio
        {
            get { return this.moveRatioX; }
            set { this.moveRatioX = value; }
        }

       /// <summary>
       /// Gets or sets the Y move ratio.
       /// </summary>
       /// <value>The Y move ratio.</value>
       public double YMoveRatio
       {
           get { return this.moveRatioY; }
           set { this.moveRatioY = value; }
       }
    }
}
