﻿// <copyright file="Station.cs" company="Microsoft">
// Copyright (c) 2009 All Right Reserved
// </copyright>
// <date>13-09-2009</date>
// <summary>Custom control for the data inside the station </summary>

namespace Microsoft.AirWatch.UI.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Microsoft.AirWatch.ViewModel;
    
    /// <summary>
    /// Custom control for the data inside the station 
    /// </summary>
    public class Station : Pin
    {
        /// <summary>
        /// DependencyProperty as the backing store for EuropeanCode.
        /// </summary>
        public static readonly DependencyProperty EuropeanCodeProperty =
            DependencyProperty.Register("EuropeanCode", typeof(string), typeof(Station), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Initializes a new instance of the <see cref="Station"/> class.
        /// </summary>
        public Station()
        {
            this.Template = Application.Current.Resources["StationTemplate"] as ControlTemplate;
            this.MouseEnter += new MouseEventHandler(this.StationMouseEnter);
            this.MouseLeftButtonUp += new MouseButtonEventHandler(this.StationMouseLeftButtonUp);
        }

        /// <summary>
        /// Gets or sets the european code.
        /// </summary>
        /// <value>The european code.</value>
        public string EuropeanCode
        {
            get { return (string)GetValue(EuropeanCodeProperty); }
            set { SetValue(EuropeanCodeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the type of the station.
        /// </summary>
        /// <value>The type of the station.</value>
        public string StationType { get; set; }

        /// <summary>
        /// Creates the pin flyout.
        /// </summary>
        protected override void CreatePinFlyOut()
        {
            if (this.PinFlyOutContainer.Children.Count == 0)
            {
                base.CreatePinFlyOut();

                this.PinFlyOut.Template = Application.Current.Resources["StationFlyoutTemplate"] as ControlTemplate;
            }
        }

        /// <summary>
        /// Handles the MouseEnter event of the Station control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void StationMouseEnter(object sender, MouseEventArgs e)
        {       
            if (String.IsNullOrEmpty(this.EuropeanCode))
            {
                MapViewModel.GetStationById(this.Id);
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the Station control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void StationMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.IsFlyOutFixed = !this.IsFlyOutFixed;
            this.UpdateFlyOut();
        }
    }
}