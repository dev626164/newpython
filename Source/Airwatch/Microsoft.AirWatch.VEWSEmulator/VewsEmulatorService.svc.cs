﻿// <copyright file="VewsEmulatorService.svc.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-04-2009</date>
// <summary>Virtual Earth Web Services Emulator</summary>
namespace Microsoft.AirWatch.VewsEmulator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;

    /// <summary>
    /// Virtual Earth Web Services Emulator
    /// </summary>
    public class VewsEmulatorService : IVewsEmulatorService
    {
        #region IVewsEmulatorService Members

        /// <summary>
        /// Resolves a location to geolocation
        /// </summary>
        /// <param name="location">location string</param>
        /// <returns>the resolved location</returns>
        public string Resolve(string location)
        {
            return "0.000000, 50.000000";
        }

        #endregion
    }
}
