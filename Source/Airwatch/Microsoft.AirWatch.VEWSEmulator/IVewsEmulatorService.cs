﻿// <copyright file="IVewsEmulatorService.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>29-04-2009</date>
// <summary>Eee</summary>
namespace Microsoft.AirWatch.VewsEmulator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;

    /// <summary>
    /// Virtual Earth Web Services Emulator
    /// </summary>
    [ServiceContract]
    public interface IVewsEmulatorService
    {
        /// <summary>
        /// Resolve geolocation
        /// </summary>
        /// <param name="location">location string</param>
        /// <returns>resolved location</returns>
        [OperationContract]
        string Resolve(string location);
    }
}
