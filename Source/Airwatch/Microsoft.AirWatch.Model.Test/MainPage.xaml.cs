﻿//-----------------------------------------------------------------------
// <copyright file="MainPage.xaml.cs" company="Microsoft Corporation and EEA">
// Copyright (c) Microsoft Corporation and The European Environment Agency 2009.
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Fil Karnicki</author>
// <email>t-dikarn@microsoft.com</email>
// <date>22/06/09</date>
// <summary>Main page</summary>
//----------------------------------------------------------------------- 
namespace Microsoft.AirWatch.Model.Test
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using dev.virtualearth.net.webservices.v1.common;

    /// <summary>
    /// Main test page
    /// </summary>
    public partial class MainPage : UserControl
    {
        /// <summary>
        /// AirWatch Model
        /// </summary>
        private AirWatchModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();
            this.model = AirWatchModel.Instance;
            this.model.MapTokenRetrieved += new EventHandler<TokenRetrievedEventArgs>(this.ModelTokenRetrieved);
            this.model.SearchResultsRetrieved += new EventHandler<SearchResultsEventArgs>(this.ModelSearchResultsRetrieved);
        }

        /// <summary>
        /// Handles the Click event of the TestButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void TestButtonClick(object sender, RoutedEventArgs e)
        {
            this.model.GetLanguage("en-GB");
        }        

        /// <summary>
        /// Models the search results retrieved.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.SearchResultsEventArgs"/> instance containing the event data.</param>
        private void ModelSearchResultsRetrieved(object sender, SearchResultsEventArgs e)
        {
            foreach (GeocodeResult res in e.Results)
            {
                this.TestTextBlock.Text += res.DisplayName + Environment.NewLine + res.Locations[0].Longitude + Environment.NewLine + res.Locations[0].Latitude + Environment.NewLine + Environment.NewLine;
            }
        }

        /// <summary>
        /// Models the token retrieved.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.AirWatch.Model.TokenRetrievedEventArgs"/> instance containing the event data.</param>
        private void ModelTokenRetrieved(object sender, TokenRetrievedEventArgs e)
        {
            this.model.GetSearchResults(this.InputTextBlock.Text, e.BingToken, "en-GB");
        }
    }
}
