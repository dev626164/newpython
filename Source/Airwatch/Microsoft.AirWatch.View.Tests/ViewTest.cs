﻿// <copyright file="ViewTest.cs" company="Microsoft">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <date>30-04-2009</date>
// <summary>Unit tests for the AirWatch View</summary>
namespace Microsoft.AirWatch.View.Tests
{
    using VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the AirWatch View 
    /// </summary>
    [TestClass]
    public class ViewTests
    {
        /// <summary>
        /// The context the test is run in
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the ViewTests class
        /// </summary>
        public ViewTests()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// You can use the following additional attributes as you write your tests:
        ////
        //// Use ClassInitialize to run code before running the first test in the class
        //// [ClassInitialize()]
        //// public static void MyClassInitialize(TestContext testContext) { }
        ////
        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }
        ////
        #endregion

        /// <summary>
        /// Sample test method
        /// </summary>
        [TestMethod]
        public void TestMethod()
        {
        }
    }
}